﻿<?php
function publish_action($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE base_fields SET `bool` = b\'1\' WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}
function unpublish_action($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE base_fields SET `bool` = b\'0\' WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}
function change_status($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE '.$xcrud->get('table').' SET '.$xcrud->get('field').' = '.$xcrud->get('value').' WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}

function getExpiredDate($value, $fieldname='', $primary_key='', $row='', $xcrud=''){
	if(!empty($value)){
		$list = explode("-",$value);
		if(intval($list[1]) < 10 || (intval($list[1]) == 10 && intval($list[2]) == 1))
			$year = $list[0]+60;
		else
			$year = $list[0]+61;
		return '1 ตุลาคม'.' '.($year+543);
	}
	
}
function getAge($value, $fieldname, $primary_key, $row, $xcrud){
	if(!empty($value)){
		$db = Xcrud_db::get_instance();
        $query = 'SELECT staffBirthday FROM tbl_staff WHERE staffID = ' . $value;
        $db->query($query);
        $row = $db->result();
        $bday = $row[0]['staffBirthday'];
        if(!empty($bday)){
        	list($year,$month,$day) = explode("-",$bday);
			$year_diff  = date("Y") - $year;
			$month_diff = date("m") - $month;
			$day_diff   = date("d") - $day;
			if ($day_diff < 0 || $month_diff < 0)
			  $year_diff--;
			if($year_diff < 150)
				return $year_diff.' ปี';
        }
        
	}
	
}
function getAgeChild($value, $fieldname, $primary_key, $row, $xcrud){
	if(!empty($value)){
		
		list($year,$month,$day) = explode("-",$value);
		$year_diff  = date("Y") - $year;
		$month_diff = date("m") - $month;
		$day_diff   = date("d") - $day;
		if ($day_diff < 0 || $month_diff < 0)
		  $year_diff--;
		if($year_diff < 150)
			return $year_diff.' ปี';
       
        
	}
	
}
function show_seminar_date($value, $fieldname, $primary_key, $row, $xcrud){

  
   $seminarID = $row['tbl_staff_seminar.seminarID'];

   $db = Xcrud_db::get_instance();
   $query = 'SELECT * FROM tbl_seminar WHERE seminarID= ' . $seminarID;
        
   $db->query($query);
   $row = $db->result();
   
   if(!empty($row)){

   	   list($syear,$smonth,$sday) = explode("-", $row[0]['seminarStartDate']);
	   list($eyear,$emonth,$eday) = explode("-", $row[0]['seminarEndDate']);
	   return $sday.'/'.$smonth.'/'.($syear+543).' - '.$eday.'/'.$emonth.'/'.($eyear+543);
   }

}

function show_feat_date($value, $fieldname, $primary_key, $row, $xcrud){

  
  list($syear,$smonth,$sday) = explode("-", $value);
  $End_date = $row['tbl_project.endDate'];
  list($eyear,$emonth,$eday) = explode("-", $End_date);
  return $sday.'/'.$smonth.'/'.($syear+543).' - '.$eday.'/'.$emonth.'/'.($eyear+543);

}
function show_mistake_date($value, $fieldname, $primary_key, $row, $xcrud){

  
  list($syear,$smonth,$sday) = explode("-", $value);
  $End_date = $row['tbl_staff_mistake.mistakeEndDate'];
  list($eyear,$emonth,$eday) = explode("-", $End_date);
  return $sday.'/'.$smonth.'/'.($syear+543).' - '.$eday.'/'.$emonth.'/'.($eyear+543);

}

function show_contract_date($value, $fieldname, $primary_key, $row, $xcrud){

  
  list($syear,$smonth,$sday) = explode("-", $value);
  $End_date = $row['tbl_staff_contract.contractEndDate'];
  list($eyear,$emonth,$eday) = explode("-", $End_date);
  return $sday.'/'.$smonth.'/'.($syear+543).' - '.$eday.'/'.$emonth.'/'.($eyear+543);

}

function getOrgID1($value, $fieldname, $primary_key, $row, $xcrud){
	 
	 	$id = $row['tbl_staff_work.orgID'];

		$db = Xcrud_db::get_instance();

        $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
    	$db->query($sql);
    	$row = $db->result();
    	$assignID = (!empty($row)) ? $row[0]['assignID'] : "1";

        $query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$id.' AND c.assignID = '.$assignID ;
    
    
        $db->query($query);
        $row = $db->result();
        if(!empty($row)){
        	if($row[0]['upperOrgID'] == '0' || $row[0]['upperOrgID'] == '1')
        		return $row[0]['orgName'];
        	else{
        		$query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$row[0]['upperOrgID'].' AND c.assignID='.$row[0]['assignID'];
		        $db->query($query);
		        $row2 = $db->result();
		        if(!empty($row2)){
		        	if($row2[0]['upperOrgID'] == '0' || $row2[0]['upperOrgID'] == '1')
		        		return $row2[0]['orgName'];
		        	else{
		        		$query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$row2[0]['upperOrgID'].' AND c.assignID='.$row2[0]['assignID'];
				        $db->query($query);
				        $row3 = $db->result();
				        if(!empty($row3))
			        		return $row3[0]['orgName'];
		        	}
		        	
		        }
        	}
        }
       
        
	
}
function getOrgID2($value, $fieldname, $primary_key, $row, $xcrud){
	 
	 	$id = $row['tbl_staff_work.orgID'];

		$db = Xcrud_db::get_instance();
        $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
    	$db->query($sql);
    	$row = $db->result();
    	$assignID = (!empty($row)) ? $row[0]['assignID'] : "1";

        $query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$id.' AND c.assignID = '.$assignID ;
    
        $db->query($query);
        $row = $db->result();
        if(!empty($row)){
        	
        	if($row[0]['upperOrgID'] != '0' && $row[0]['upperOrgID'] != '1'){
        		$query = 'SELECT * FROM tbl_org_chart c WHERE c.upperOrgID = '.$row[0]['orgID'].' AND c.assignID='.$row[0]['assignID'];
		        $db->query($query);
		        $row2 = $db->result();
		        if(!empty($row2)){
		        	return $row[0]['orgName'];
		        }else{
		        	$query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$row[0]['upperOrgID'].' AND c.assignID='.$row[0]['assignID'];
			        $db->query($query);
			        $row1 = $db->result();
			        if(!empty($row1)){
			        	return $row1[0]['orgName'];
			        }
		        }
        		
        	}
        }
       
	
}
function getOrgID($value, $fieldname, $primary_key, $row, $xcrud){
	 
        $id = $row['tbl_staff_work.orgID'];

		$db = Xcrud_db::get_instance();
        $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
    	$db->query($sql);
    	$row = $db->result();
    	$assignID = (!empty($row)) ? $row[0]['assignID'] : "1";

        $query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$id.' AND c.assignID = '.$assignID ;
    
        $db->query($query);
        $row = $db->result();
        if(!empty($row)){
        	$query = 'SELECT * FROM tbl_org_chart c WHERE c.upperOrgID = '.$row[0]['orgID'].' AND c.assignID='.$row[0]['assignID'];
	        $db->query($query);
	        $row2 = $db->result();
	        if(empty($row2)){
	        	return $row[0]['orgName'];
	        }
        }
       
	
}


function toBDYear($value, $fieldname, $primary_key, $row, $xcrud){
	return $value+543;
}
function toBDDate($value, $fieldname, $primary_key, $row, $xcrud){
	list($year,$month,$day) = explode("-",$value);
	return $day.'/'.$month.'/'.($year+543);
}

function calAgeWork($value, $fieldname, $primary_key, $row, $xcrud){

	$dateWork = $row['tbl_staff.staffDateWork'];
	
	list($year,$month,$day) = explode("-",$dateWork);
		$year_diff  = date("Y") - $year;
		$month_diff = date("m") - $month;
		$day_diff   = date("d") - $day;
		if ($day_diff < 0 || $month_diff < 0)
		  $year_diff--;
		if($year_diff < 62)
			return $year_diff.' ปี';
}
function calPension($value, $fieldname, $primary_key, $row, $xcrud){

	$dateWork = $row['tbl_staff.staffDateWork'];
	$salary = $row['tbl_staff_pension.pensionSalary'];
	list($year,$month,$day) = explode("-",$dateWork);
	$year_diff  = date("Y") - $year;
	$month_diff = date("m") - $month;
	$day_diff   = date("d") - $day;
	if ($day_diff < 0 || $month_diff < 0)
	  $year_diff--;

	return number_format($salary * $year_diff);

}
function showNumberFormat($value){
	return number_format($value);
}
function getLinkChangeName($value, $fieldname, $primary_key, $row, $xcrud){
	return '<button class="btn btn-primary" onclick="window.location=\'./staffname/?token='.(int)$xcrud->get('primary').'\';">ประวัติการเปลี่ยนชื่อ</button>';
}


function DateThai($value, $fieldname, $primary_key, $row, $xcrud) {
	 
	$status = $value;
	$strYear = date("Y",strtotime($status))+543;
	$strMonth= date("n",strtotime($status));
	$strDay= date("j",strtotime($status));
			
	$strMonthCut = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	$strMonthThai=$strMonthCut[$strMonth];
  	
	return "$strDay $strMonthThai $strYear";
}

function delete_user_data($primary, $xcrud){
    $db = Xcrud_db::get_instance();
    $db->query('DELETE FROM tbl_org_chart WHERE assignID = ' . $db->escape($primary));
}

function calDateWork($value, $fieldname, $primary_key, $row, $xcrud){

	$db = Xcrud_db::get_instance();
    $query = 'SELECT s.workStartDate FROM tbl_staff_work s  WHERE staffID = ' . $row['tbl_staff_work.staffID'].' AND workID > '.$primary_key.' ORDER BY workID ASC';

    $db->query($query);
 	$row = $db->result();
 	if(!empty($row[0]['workStartDate'])){
       list($year,$month,$day) = explode("-",$value);
       list($year_e,$month_e,$day_e) = explode("-",date("Y-m-d",strtotime("-1day",strtotime($row[0]['workStartDate']))));
		return $day.'/'.$month.'/'.($year+543).' - '.$day_e.'/'.$month_e.'/'.($year_e+543);
 	}else{
   		list($year,$month,$day) = explode("-",$value);
		return $day.'/'.$month.'/'.($year+543);
   }
	
}


function calAgeContract($value, $fieldname, $primary_key, $row, $xcrud){

	if(!empty($value)){
		$contractType = $row['tbl_staff_contract.contractType'];
		$db = Xcrud_db::get_instance();
	    $query = 'SELECT contracttypeYear FROM tbl_contract_type WHERE contracttypeID = ' . $contractType;
	    $db->query($query);
	 	$row = $db->result();
	 	return (!empty($row[0]['contracttypeYear'])) ? $row[0]['contracttypeYear'] : "";
		
	}
	
}

function calRankWork($value, $fieldname, $primary_key, $row, $xcrud){

	
	$db = Xcrud_db::get_instance();
    //$query = 'SELECT r.rankName FROM tbl_staff_work s JOIN tbl_rank r ON s.rankID = r.rankID WHERE s.staffID = ' . $primary_key.' ORDER BY s.workStartDate DESC';
    $query = 'SELECT r.emp_level_name FROM tbl_staff_work s JOIN tbl_emp_level r ON s.rankID = r.emp_level_id WHERE s.staffID = ' . $primary_key.' ORDER BY s.workStartDate DESC';
    
    $db->query($query);
 	$row = $db->result();
 	return (!empty($row[0]['emp_level_name'])) ? $row[0]['emp_level_name'] : $value;
		
}

function calPositionWork($value, $fieldname, $primary_key, $row, $xcrud){

		$db = Xcrud_db::get_instance();
	    $query = 'SELECT p.positionName FROM tbl_staff_work s JOIN tbl_position p ON s.positionID = p.positionID WHERE s.staffID = ' . $primary_key.' ORDER BY s.workStartDate DESC';
	    $db->query($query);
	 	$row = $db->result();
	 	return (!empty($row[0]['positionName'])) ? $row[0]['positionName'] : $value;

}

function calOrgIDWork($value, $fieldname, $primary_key, $row, $xcrud){

		$db = Xcrud_db::get_instance();

		
	    $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
    	$db->query($sql);
    	$row = $db->result();
    	$assignID = (!empty($row)) ? $row[0]['assignID'] : "1";

    	/*
	    $query = 'SELECT * FROM tbl_staff_work s JOIN tbl_org_chart o ON s.orgID = o.orgID  AND o.assignID = '.$assignID.' WHERE s.staffID = ' . $primary_key.' ORDER BY s.workStartDate DESC';
	    */

	    $query = 'SELECT o.* FROM tbl_staff s JOIN tbl_org_chart o ON s.orgID = o.orgID  AND o.assignID = '.$assignID.' WHERE s.ID = ' . $primary_key;

	    $db->query($query);
	 	$row = $db->result();

	 	if(!empty($row)){
	 		return $row[0]['orgName'];
	 		/*
	 		if($row[0]['upperOrgID'] == '0' || $row[0]['upperOrgID'] == '1')
        		return $row[0]['orgName'];
        	else{
        		$query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$row[0]['upperOrgID'].' AND c.assignID='.$row[0]['assignID'];
		        $db->query($query);
		        $row2 = $db->result();
		        if(!empty($row2)){
		        	if($row2[0]['upperOrgID'] == '0' || $row2[0]['upperOrgID'] == '1')
		        		return $row2[0]['orgName'];
		        	else{
		        		$query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$row2[0]['upperOrgID'].' AND c.assignID='.$row2[0]['assignID'];
				        $db->query($query);
				        $row3 = $db->result();
				        if(!empty($row3))
			        		return $row3[0]['orgName'];
		        	}
		        	
		        }
        	}
        	*/

	 	}
	 	
	
}


function calTimeContract($value, $fieldname, $primary_key, $row, $xcrud){

	/*if(!empty($value)){
		$contractType = $row['tbl_staff_contract.contractType'];
		$contractDate = $row['tbl_staff_contract.contractDate'];
		$db = Xcrud_db::get_instance();
	    $query = 'SELECT contracttypeYear FROM tbl_contract_type WHERE contracttypeID = ' . $contractType;
	    $db->query($query);
	 	$row = $db->result();
	 	if(!empty($row[0]['contracttypeYear']) && !empty($contractDate)){
	 		list($year,$month,$day) = explode("-",$contractDate);
	 		return $day.'/'.$month.'/'.($year+543).' - '.$day.'/'.$month.'/'.($year+543+$row[0]['contracttypeYear']);

	 	}
		
	}*/
	if(!empty($value)){
		$arr_field = json_decode($value);
		$text = "";
		foreach($arr_field as $rowf){
			if($rowf->id == '22' || $rowf->id == '23'){
				$list = explode('/',$rowf->value);
				if(!empty($list[0]) && !empty($list[1]) && !empty($list[2]))
					$text.= $list[0].'/'.$list[1].'/'.($list[2]+543).' - ';
			}

		}

		return (!empty($text)) ? substr($text,0,-2) : "";
	}
	
}

function getStaffID($value, $fieldname, $primary_key, $row, $xcrud){

	if(!empty($value)){
		$staffID = $row['tbl_staff_cerificate.staffID'];
		
		$db = Xcrud_db::get_instance();
	    $query = 'SELECT staffID FROM tbl_staff WHERE ID = ' . $staffID;
	    $db->query($query);
	 	$row = $db->result();
	 	if(!empty($row[0]['staffID'])){	 		
	 		return $row[0]['staffID'];
	 	}		
	}
	
}

function getContractStatus($value, $fieldname, $primary_key, $row, $xcrud){

	if(!empty($value)){
		$contractFile = $row['tbl_staff_contract.contractFile'];
		
		return (empty($contractFile)) ? 'รอปิดงาน' : 'เรียบร้อย';		
	}
	
}

function getStaffStatus($value, $fieldname, $primary_key, $row, $xcrud){
	
	$staffID = $row['tbl_staff.ID'];
	
	if(!empty($staffID)){
		$db = Xcrud_db::get_instance();
	    $query = 'SELECT personStatus FROM tbl_staff_person WHERE staffID = ' . $staffID.' AND personType="1" ORDER BY marryDate DESC';
	   
	    $db->query($query);
	 	$row = $db->result();
	 	if(!empty($row)){
	 		if($row[0]['personStatus'] == '1'){
		 		return "สมรส";
		 	}else if($row[0]['personStatus'] == '0'){
		 		return "หย่า";
		 	}else if($row[0]['personStatus'] == '2'){
		 		return "เสียชีวิต";
		 	}else{
		 		return "ไม่มีข้อมูลคู่สมรส";
		 	}
		 }else{
		 		return "ไม่มีข้อมูลคู่สมรส";
		 }
	}
	
 	
	
	
}

function hash_assigndate1($primary, $xcrud){
   $db = Xcrud_db::get_instance();
   echo $postdata->get('assignDate');
   $db->query(' SELECT MAX(assignDate) FROM `tbl_orgchart_assigndate` where assignDate >= ' . $postdata->get('assignDate'));
   return "ok";
}



function hash_assigndate($postdata, $xcrud){
    
   $db = Xcrud_db::get_instance();
   //echo $postdata->get('assignDate');
   $sql     = 'SELECT MAX(assignDate) as assignDate ,assignID FROM `tbl_orgchart_assigndate`  where assignDate  >= "'.$postdata->get('assignDate').'" ' ;
   $db->query($sql);
  // echo  $sql; exit(); 
   //echo $postdata->get('assignID');
   $row     =  $db->result();
   $assignDate  =   $row[0]['assignDate'];
   $myUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']),array('off','no'))) ? 'https' : 'http';
    
    // Get domain portion
   $myUrl .= '://'.$_SERVER['HTTP_HOST'];
   
   $assignID        = $postdata->get('assignID') -1;
   $assignID_new    = $postdata->get('assignID');
   //echo  $assignID.'<br>';
        if($row[0]['assignDate'] == ''){
             ///  MAX date //
            $sql_max_date   = ' SELECT assignCreateDate  , assignID  ,  assignDate   FROM `tbl_orgchart_assigndate` ORDER BY assignCreateDate desc limit 1 ' ;
            $db->query($sql_max_date);
            $row_max_date = $db->result();
            $assignID_max_date  =   $row_max_date[0]['assignID'] ;
            //echo $assignID_max_date;
            
            ///  MAX OrgID //
            $sql2   = ' SELECT MAX(orgID) as orgID    FROM `tbl_org_chart`    ' ;
            $db->query($sql2);
            $row1 = $db->result();
            $orgID1 =   $row1[0]['orgID'] ;
            ///  Check OrgChart , assignID  มีกี่ record โดย count //
                $sql1   = 'SELECT * FROM `tbl_org_chart`  where assignID  = "'.$assignID_max_date.'" ' ;
                $db->query($sql1);
                $row3 = $db->result();
                //echo  $sql1.'<br>';
                //exit();
                //echo count($row3);
                if(count($row3)) {
                 $orgID     = $row3[0]['orgID'] ;                       
                    //for ($i=1;$i <= count($row3);$i++) {
                        $sql5   = 'SELECT * FROM `tbl_org_chart`  where assignID  = "'.$assignID_max_date.'" ' ;
                        $db->query($sql5);
                        $row5 = $db->result();
                        $i  = 1;
                        $num_upper  =   0;
                        $assignID   = $postdata->get('assignID') +1;
                         foreach ($row5 as $fitem){
                             $pri_org   =   $orgID1 + $i;
                             
                             $orgID         = $fitem['orgID']  ;
                             $upperOrgID    = $fitem['upperOrgID']  ; 
                             $orgShortName  = $fitem['orgShortName']  ; 
                             $orgName       = $fitem['orgName']  ; 
							 
							 $sequence       = $fitem['sequence']  ; 
							 $position       = $fitem['position']  ; 
							 $rows       	 = $fitem['rows']  ; 
                             
                             $db->query("  INSERT INTO tbl_org_chart (orgID, assignID ,orgShortName , orgName , upperOrgID , sequence ,position ,  rows) 
                             VALUES ('$orgID','$assignID_new','$orgShortName','$orgName', $upperOrgID ,'$sequence' ,'$position' ,'$rows' )  ");
                             $i++;
                         }
                    //}           
        }
         //exit();  
   }else{
        echo '<div class="table-responsive">'
              .'<table class="table">'
                .'<tbody>'
                  .'<tr>'
                    .'<th colspan="7" ><center> <br><br><br><br>กรุณาเลือกวันที่เอกสารใหม่อีกครั้ง  </center></th>'
                  .'</tr>'
                  .'<tr>'
                    .'<th colspan="7" scope="row"> <meta http-equiv="refresh" content="1;URL="'. $myUrl.'/appoint'.'"" /> </th>'
                  .'</tr>'
                .'</tbody>'
              .'</table>'
            .'</div>' ;
         
        exit(); 
   }
   
}

function hash_retrospect($postdata, $xcrud){
    
   $db = Xcrud_db::get_instance();
   //echo $postdata->get('assignDate');
   $sql     = 'SELECT MIN(assignDate) as assignDate , MAX(assignID) as assignID FROM `tbl_orgchart_assigndate`  ' ;
   $db->query($sql);
   
   //echo $postdata->get('assignID');
   $row     =  $db->result();
   $assignDate  =   $row[0]['assignDate'];
   
   $myUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']),array('off','no'))) ? 'https' : 'http';
    
    // Get domain portion
   $myUrl .= '://'.$_SERVER['HTTP_HOST'];
   
   $assignID        = $row[0]['assignID']; //$postdata->get('assignID')-1;
   $assignID_new    = $postdata->get('assignID');
   
        if($row[0]['assignDate'] != ''){
             ///  MAX date //
            $sql_max_date   = ' SELECT assignCreateDate  , assignID  ,  assignDate   FROM `tbl_orgchart_assigndate` ORDER BY assignCreateDate asc limit 1 ' ;
            $db->query($sql_max_date);
            $row_max_date = $db->result();
            $assignID_max_date  =   $row_max_date[0]['assignID'] ;
            
            ///  MAX OrgID //
            $sql2   = 'SELECT MAX(orgID) as orgID  FROM `tbl_org_chart`  ' ;
            $db->query($sql2);
            $row1   = $db->result();
            $orgID1 =   $row1[0]['orgID'] ;
            ///  Check OrgChart , assignID  มีกี่ record โดย count //
                $sql1   = 'SELECT * FROM `tbl_org_chart`  where assignID  = "'.$assignID_max_date.'" ' ;
                $db->query($sql1);
                $row3 = $db->result();
                //echo count($row3);
                if(count($row3)) {
                 $orgID     = $row3[0]['orgID'] ;                       
                    //for ($i=1;$i <= count($row3);$i++) {
                        $sql5   = 'SELECT * FROM `tbl_org_chart`  where assignID  = "'.$assignID_max_date.'"    ' ;
                        $db->query($sql5);
                        $row5 = $db->result();
                        $i  = 1;
                        $num_upper  =   0;
                        $assignID   = $postdata->get('assignID') +1;
                         foreach ($row5 as $fitem){
                             $pri_org   =   $orgID1 + $i;
                             
                             $orgID         = $fitem['orgID']  ;
                             $upperOrgID    = $fitem['upperOrgID']  ; 
                             $orgShortName  = $fitem['orgShortName']  ; 
                             $orgName       = $fitem['orgName']  ; 
                             
							 $sequence       = $fitem['sequence']  ; 
							 $position       = $fitem['position']  ; 
							 $rows       	 = $fitem['rows']  ; 
							 
                             $db->query("  INSERT INTO tbl_org_chart (orgID, assignID ,orgShortName , orgName , upperOrgID , sequence ,position ,  rows ) 
                             VALUES ('$orgID','$assignID_new','$orgShortName','$orgName', $upperOrgID  ,'$sequence' ,'$position' ,'$rows')  ");
                             $i++;
                         }
                    //}           
        }
         //exit();  
   }else{
        echo '<div class="table-responsive">'
              .'<table class="table">'
                .'<tbody>'
                  .'<tr>'
                    .'<th colspan="7" ><center> <br><br><br><br>กรุณาเลือกวันที่เอกสารใหม่อีกครั้ง  </center></th>'
                  .'</tr>'
                  .'<tr>'
                    .'<th colspan="7" scope="row"> <meta http-equiv="refresh" content="1;URL="'. $myUrl.'/appoint'.'"" /> </th>'
                  .'</tr>'
                .'</tbody>'
              .'</table>'
            .'</div>' ;
         
        exit(); 
   }
   
}

function delete_position_line_data($primary, $xcrud){
    $db = Xcrud_db::get_instance();
    $db->query('DELETE FROM tbl_job_desc WHERE line_id = ' . $db->escape($primary));
}

