$(document).ready(function() {
  var url = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PLKBh3M0A9l8j4Xtg3S3gnNXF4X6p3aS2w&key=AIzaSyAqVGNQExGhMtPk4fbP4M1AAwJjhRrMq68";
  var video;
  var thumb;
  var container = $('.youtube-container');
  $.getJSON(url,
      function(response) {
        for (var i = response.items.length - 1; i >= 0; i--) {
          thumb = response.items[i].snippet.thumbnails.medium.url;
          video = response.items[i].snippet.resourceId.videoId;
          container.append('<article class="video-placeholder" data-video="' + video + '"><img class="youtube-icon" src="http://www.youtube.com/yt/brand/media/image/YouTube-icon-dark.png"><img class="youtube-thumb" src="' + thumb + '"></article>');
        }
      }); //end getJSON reposnse
  $(document).on('click', '.video-placeholder', function() {
    var videoID = $(this).attr('data-video');
    var videoiframe = '<article class="video-placeholder"><iframe width="257" height="146" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1" frameborder="0" allowfullscreen></iframe></article>';
    $(this).replaceWith(videoiframe);
  });
}); //end doc ready