SMMMS PeopleFinder
==================

PeopleFinder attach to a text input field a generic JQuery Combo with pagination.

Require:
- https://fortawesome.github.io/Font-Awesome/

AJAX Get Call:
url.php?key=abc&rows=5&page=2

rows 5 page 1 ==> limit 0,5
rows 5 page 2 ==> limit 5,5

AJAX Return:
{
  "total":34,
  "items":[ {"field1":"", "field2":"", ... }, ... ]
}

Call

  $('#comboA').smmmsPeopleFinder({
    url: 'test/server.php',           //==> url for the Get AJAX Call
    imageFolderUrl: '',               //==> In case if need to add a path to the pictures
    rows: 10,                         //==> Default 10 rows / page
                                      //==> JSON returned from the GET
    idColumn: 'uid',                  //==> id used for the field <input type="hidden" value="" name="comboA_id">
    textColumn: 'name',               //==> text to be used inside the text input
    displayColumns: [                 //==> map 1 to N JSON fields to a column.
      {'map': 'image', 'image': true},
      [
        {
          'map': 'name'
        },
        {
          'map': 'position'
        }
      ],
      {
        'map': 'department'
      }
    ]
              
  });
  
  ....
  
  <input size="30" id="comboA" name="comboA"></input>
  
  
HTML Generated next to the input field:
  
  <input type="hidden" value="" name="comboA_id">                               //==> input hidden field with the selected item's id, name="comboA_id"
  <input type="button" class="pf-search-button" value="" >
  <div class="pf-popup" id="comboA_popup" style="min-width: 318px;"> ... </div>  //==>  popup window with an id="comboA_popup" (if input field has an id)
                                                                                        This id allows custom css for the popup .
                                                                                        
Custom CSS Example:

  #comboA_popup .pf-popup-content-table-col2{
    width: 500px;
  }
  #comboA_popup .pf-popup-content-table-col2 p:nth-child(1) {
    font-weight: bold;
  }
  #comboA_popup .pf-popup-content-table-col2 p:nth-child(2) {
    font-style: italic;
    font-size: 90%;
  }
