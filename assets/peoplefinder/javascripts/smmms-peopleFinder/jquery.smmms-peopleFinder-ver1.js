(function($) {
  $.fn.smmmsPeopleFinder = function( options ) {
    // Default Settings
    var settings = $.extend({ 
      url:     'test/server.php',
      delay: 300,
      rows : 10,
      imageFolderUrl: "",
      maxpages: 5,
      debug: false,
      imageField: "image",
      row1Fields: ["name","position"],
      row2Fields: ["department"]
    }, options);

    //Main Container
    return this.each( function() {
      $(this).addClass("pf-input");
      var self = $( this )[0];
      var searchButton = $('<input type="button" class="pf-search-button" value="" />');
        searchButton.height($( this ).outerHeight(true));
        searchButton.width($( this ).outerHeight(true));
        searchButton.click(function() {
          showResults(self.value);
        });
        $( this ).after(searchButton);
      var imageField = settings.imageField;
      var row1Fields = settings.row1Fields;
      var row2Fields = settings.row2Fields;
      var rowsPerPage = settings.rows;
      var currentPage = 1;
      var lastPage = 1;
      var selectedItem = -1;
      var lastItem = 0;
      var tempMeVal = '';
      var popupContent = $("<div class='pf-popup-content'>");
      var popupControls = $("<div class='pf-popup-control-controls'>");
        var backFirstPage = $("<i class='fa fa-fast-backward'>");
          backFirstPage.on('mousedown', function(e) {
            if(!backFirstPage.hasClass("control-disabled")){
              showResults(self.value);
            }
          });
          popupControls.append(backFirstPage);
        var backOnePage = $("<i class='fa fa-step-backward'>");
          backOnePage.on('mousedown', function(e) {
            if(!backOnePage.hasClass("control-disabled")){
              showResults(self.value, --currentPage);
            }
          });
          popupControls.append(backOnePage);
        var pagination = $('<span>').addClass("pf-popup-control-pagination");
          popupControls.append(pagination);
        var goNextPage = $("<i class='fa fa-step-forward'>");
          goNextPage.on('mousedown', function(e) {
            if(!goNextPage.hasClass("control-disabled")){
              showResults(self.value, ++currentPage);
            }
          });        
          popupControls.append(goNextPage);        

        var goLastPage = $("<i class='fa fa-fast-forward'>");
        goLastPage.on('mousedown', function(e) {
          if(!goLastPage.hasClass("control-disabled")){
            showResults(self.value, lastPage);
          }
        });
        popupControls.append(goLastPage);
        var displayRows = $("<select><option value='5'>5</option><option value='10'>10</option><option value='20'>20</option></selected>");
        popupControls.append(displayRows.addClass("pf-popup-control-display-rows"));
        displayRows.children('option[value="'+rowsPerPage+'"]').prop('selected',true);
        displayRows.change(function() {
          var optionSelected = $("option:selected", this);
          var valueSelected = this.value;
          rowsPerPage = valueSelected;
          showResults(self.value, 1);
          self.focus();
        });
        popupControls.append($("<span>").addClass("pf-popup-control-display-rows-span").text("items per page"));
        var countResults = $("<span>");
        countResults.addClass("pf-popup-control-display-count-span");
        popupControls.append(countResults);

        var popup = $("<div class='pf-popup'></div>");
        popup.append(popupContent);
        popup.append($("<div class='pf-popup-control'>").append(popupControls));
        popup.css({display: 'none'});
        popup.position().top = $( this ).position().top + $( this ).outerHeight(true);
        popup.css({minWidth: $( this ).outerWidth(true)});
        searchButton.after(popup);

      var lastTimeout = null;  
      $(this).bind('keyup', function(e){
        var code = e.keyCode || e.which;
        if(settings.debug)console.log("keyup", code);
        switch(code) {
          case 40: //down arrow
              move_keyboard("next");
              break;
          case 38: //up arrow
              move_keyboard("previous");
              break;
          case 13:
              selectRow_keyboard();
              break;    
          case 37: //left arrow
          case 39: //right arrow
              break;
          default:
            tempMeVal = self.value;
            if(self.value.length >= 3 ){
              if(settings.debug)console.log("value before timeout",self.value);
              if(lastTimeout !== null)window.clearTimeout(lastTimeout);
              lastTimeout = window.setTimeout(showResults(self.value), settings.delay);  
            }else{
              popup.hide();  
            }
          }
      });
      $(document).mouseup(function (e)
      {
        if(settings.debug)console.log("mouseup",e.target);
          if (!popup.is(e.target)
                  && popup.has(e.target).length === 0)
              {
                  if(document.activeElement != self){
                    //for some reason we have a target problem when we click on the edge of an icon
                    popup.hide();
                  }
              }
      });
      function move_keyboard(direction) {
        if(popup.is(":visible") && !popupContent.hasClass("control-disabled")){
          if(/^next/.test(direction)){
            if(selectedItem < rowsPerPage - 1 && selectedItem + ((currentPage-1)*rowsPerPage) < lastItem -1 ){
              highlightRow_keyboard(++selectedItem);
            } 
          }
          if(/^previous/.test(direction)){
            if(selectedItem > -1){--selectedItem;}
              highlightRow_keyboard(selectedItem);
          }
        }
      }

      function highlightRow_keyboard(item){
        if(settings.debug)console.log("item",item);
        popupContent.find(".selected").removeClass("selected");
        if(item >= 0){
          itemToSelect = popupContent.find(".pf-popup-content-table-row:eq("+item+")");
          itemToSelect.addClass("selected");
          if(itemToSelect.position().top + itemToSelect.outerHeight(true) > popupContent.height() ){
              popupContent.scrollTop(popupContent.scrollTop() + (itemToSelect.position().top + itemToSelect.outerHeight(true) - popupContent.height()) );
          }
          if(itemToSelect.position().top < 0){
            popupContent.scrollTop(popupContent.scrollTop() + itemToSelect.position().top);
          }
          self.value = popupContent.find(".selected").find("p.pf-popup-content-name").text();
        }else{
          self.value = tempMeVal;
        }
      }

      function selectRow_keyboard(item){
        tempMeVal = self.value;
        popup.hide();
      }

      function showResults(keyword, page) {
        var page = page || 1;
        pagination.addClass("control-disabled");
        popupContent.addClass("control-disabled");
        popupControls.children().addClass("control-disabled");
        if(settings.debug)console.log("getJSON", keyword + " " + rowsPerPage + " " + page);
        var listPeople = $.getJSON( "test/server.php", { name: keyword, rows: rowsPerPage, page: page})
        .done(function( data ) {
          popupContent.html(formatResults(data.people));
          currentPage = page //Math.ceil(data.first / rowsPerPage);
          lastPage = Math.ceil(data.total / rowsPerPage);
          lastItem = data.total;
          selectedItem = -1;
          updateControls(data.total, currentPage, lastPage);
          popupContent.removeClass("control-disabled");
          if(data.total>0){
            popup.show();
            popupContent.scrollTop(0);
            self.focus();
          }else{
            popup.hide();
          }
        })
      }
      function updateControls(total, currentPage, lastPage){
        if(settings.debug)console.log("updateControls", total + " " + currentPage + " " + lastPage);
        pagination.removeClass("control-disabled");
        displayRows.removeClass("control-disabled");
        if(settings.debug)console.log("current page", currentPage);
        if(currentPage > 1){
          backFirstPage.removeClass("control-disabled");
          backOnePage.removeClass("control-disabled");
        }
        if(currentPage < lastPage){
          goNextPage.removeClass("control-disabled");
          goLastPage.removeClass("control-disabled");
        } 
        
        var middlePagination = Math.ceil(settings.maxpages/2);
        var startPagination = 1;
        var endPagination = 1;
        if(currentPage <= middlePagination){
          var endPagination = settings.maxpages;
        }else if(currentPage >= lastPage - middlePagination){
          endPagination = lastPage;
          startPagination = lastPage - settings.maxpages; 
        }else{
          startPagination = currentPage - middlePagination;
          endPagination = startPagination + settings.maxpages - 1;
        }
        if(endPagination > lastPage)endPagination = lastPage;
        if(startPagination < 1)startPagination = 1;
        pagination.html('');
        for(i=startPagination; i<=endPagination; i++){
          var page = $('<span>');
          page.text(i);
          page.on('mousedown', function(e) {
            if(!pagination.hasClass("control-disabled")){
              showResults(self.value,$( this ).text());
            }
          });
          if(i == currentPage){
              page.addClass('selected')
          };        
          pagination.append(page);
        }
        var el1 = (rowsPerPage * (currentPage-1)) + 1;
        var el2 = (rowsPerPage * (currentPage+1));
        if(el2 > total)el2 = total;
        countResults.text(el1 + "-" + el2 + " of " + total + " items");
           
      }

      function formatResults(data) {
        output = $("<table>");
        if(settings.debug)console.log("data.length" , data.length);
        if(data != undefined && data != null && data.length > 0){
          $.each(data, function (index, value) {
            var line = $('<tr>').addClass('pf-popup-content-table-row');
            line.on('mousedown', function(e) {
              e.preventDefault();
              e.stopPropagation();
              if(row1Fields.length>0)self.value=value[row1Fields[0]];
              popup.hide();
            });
            var img = $('<div>').addClass('pf-popup-content-img');
            if(value[imageField] !== ""){
              img.css({'background-image':'url("'+settings.imageFolderUrl+value[imageField]+'")'});
            }
            line.append($("<td>").addClass('pf-popup-content-table-col').addClass('pf-popup-content-table-col1')
              .append($("<p class='pf-popup-content-imgBox'>").append(img)));
            var name = $("<p class='pf-popup-content-name'>");
            if(row1Fields.length>0)name.text(value[row1Fields[0]]);
            else name.text("\xA0");
            var position = $("<p class='pf-popup-content-position'>");
            if(row1Fields.length>1)position.text(value[row1Fields[1]]);
            else position.text("\xA0");
            line.append($("<td>").addClass('pf-popup-content-table-col').addClass('pf-popup-content-table-col2')
              .append(name).append(position));
            var department = $("<p class='pf-popup-content-department'>");
            if(row2Fields.length>0)department.text(value[row2Fields[0]]);
            else department.text("\xA0");
            var position2 = $("<p class='pf-popup-content-position'>");
            if(row2Fields.length>1)position2.text(value[row2Fields[1]]);
            else position2.text("\xA0");
            line.append($("<td>").addClass('pf-popup-content-table-col').addClass('pf-popup-content-table-col3')
              .append(department).append(position2));
            output.append(line);
            if(settings.debug)console.log("image path", img);
          });
        }
        return output;
      }
    });





    }

}(jQuery));