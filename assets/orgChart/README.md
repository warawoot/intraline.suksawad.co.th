SMMMS OrgChart
==============
0.7

OrgChart is a plugin to generate organization charts.


init:
    
  <script src="javascripts/smmms-orgChart/jquery.smmms-orgChart-ver7.min.js"></script>
  <script type="text/javascript">
    $(document).ready( function() {
        $('#orgChart').smmmsOrgChart({
          defaultPicture: 'javascripts/smmms-orgChart/manager-default.jpg',
          imageFolderUrl: 'http://localhost/orgChart/test/',
          data:     'test/multiExpand1.json'
        });
    });
    