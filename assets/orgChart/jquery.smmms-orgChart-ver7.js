(function($) {

    $.fn.smmmsOrgChart = function( options ) {

        //Shortcut to redraw all the connexions
        if(options == 'connectTheNodes') {
            return connectTheNodes();
        }
        // Default Settings
        var settings = $.extend({
              defaultPicture: 'manager-default.jpg',
              imageFolderUrl: '',
              data:     'url_to_data.json'
        }, options);  
        //reset
        var debug = false;
        var zoomState=1;
        var arrayToLink = [];
        var arrayChildrenAlreadyLinked = [];
        var forId = 1;
        var arrayDepartmentToDisplay = [];
        //Main Container
        return this.each( function() {
            var container = $(this);
            container.addClass('orgChart-container');
            //reset
            container.html("");


            var canvas = $('<div>').addClass('orgChart-canvas');
            //Create canvas
            
            var containerTable = $('<table>').addClass('orgChart-table');

            //Load The Data
            $.getJSON(settings.data, {}, function(data){
              canvas.width('600000px');
              buildNode(containerTable, data[0]);
              canvas.append(containerTable);
              container.append(canvas);

              //resize all the left/right tables
              $($('.orgChart-table-right')).each(function() {
                if($(this).parents('tr').first().children('td').first().width() < $(this).parents('td').first().width()){
                  var minwidth = $(this).parents('td').first().width();
                  $(this).parents('tr').first().children('td').first().width(minwidth);
                }else{
                  var minwidth = $(this).parents('tr').first().children('td').first().width();
                  $(this).parents('td').first().width(minwidth);
                }
              });
              
              $('.orgChart-row-tree').each(function() {
                $(this).children('td').attr('align','left');
                //$(this).width($(this).children('td').width());
                var desiredWidth = $(this).children().children('.orgChart-cell').first().width();
                //console.log($(this).children('.departmentBox'));
                desiredWidth = desiredWidth * 0.7;
                //console.log(desiredWidth);
                //resize all the tree childrens if play with 1 column (new Method)
                //$(this).next().children('td').css({'padding-left': desiredWidth+'px' });
                $(this).parent().children('tr').slice(1).children('td').css({'width': desiredWidth+'px' });
                $(this).parent().children('tr').slice(1).children('td').css({'min-width': desiredWidth+'px' });
              });
              
              canvas.width(containerTable.width());
              //$(".orgChart-table").css({'width':'100%'});

            if(container.next().attr('class') !== "orgChart-button-group"){


            $('<div>').addClass('orgChart-button-group');
              var zoomIn = $('<input type="button" class="orgChart-zoom-button orgChart-zoom-button-in" value="+" />');
              var zoomOut = $('<input type="button" class="orgChart-zoom-button orgChart-zoom-button-out" value="-" />');
              var print = $('<input type="button" class="orgChart-print-button" value="" />');
              container.after($('<div>').addClass('orgChart-button-group').append(zoomIn).append(zoomOut).append(print));

              $('.orgChart-zoom-button-in').click(function(){zoomState+=.1; zoomAction()});
              $('.orgChart-zoom-button-out').click(function(){zoomState>.55&&(zoomState-=.1); zoomAction()});
              //$('.orgChart-print-button').click(function(){$('.orgChart-canvas').print();});
            }

            //Escape the array to fix elements on resize
            $('.orgChart-ul-bullet').each(function() {
              $( this ).css({'top' : $( this ).position().top});
              $( this ).css({'left' : $( this ).position().left});
            });            
            $('.departmentBox').each(function() {
              $( this ).css({'top' : $( this ).position().top});
              $( this ).css({'left' : $( this ).position().left});
              
            });
            if(!debug){
              $('.departmentBox').css({'position':'absolute'}).appendTo( canvas );
              $('.orgChart-ul-bullet').css({'position':'absolute'}).appendTo( canvas );
              $('.orgChart-table').remove();
              var lefts = $('.departmentBox').map(function(){
                if($(this).position().top > 150){
                  return $(this).position().left;
                }else{
                  return $(this).position().left - 50;
                } 
              }).get();
              var minLeft = Math.min.apply(Math, lefts)-10;
              $('.departmentBox').each(function() {
                $( this ).css({'left' : $( this ).position().left - minLeft});
              }); 
              $('.orgChart-ul-bullet').each(function() {
                $( this ).css({'left' : $( this ).position().left - minLeft});
              }); 
            }else{
              //$('.departmentBox').css({'position':'absolute'});
            }
            canvas.append($('<div>').attr('id', 'connectNode'));
            connectTheNodes();
            
            container.after($('<div>').addClass('departmentDetails-background'));
            /*
            $('.departmentDetails-background').click( function(){
              $(this).html('').hide();
            });
            */
            addDepartmentClick();

            }).error(function(e) {
              //Or STOP if any problem in the JSON
              console.log('getJSON error: ' + JSON.stringify(e)); 
            });


        });

        //---------------------Build Functions---------------------------------------------------------------------------------
        /*
        *  We are in a TABLE
        *  buildNode: Build TOP Element and children
        *  @remark: Here is a 2 rows, all the children are splitted in two rows if expand typed
        */
        function buildNode(container, department, optionalNodeClass){
          if (typeof forId === 'undefined') { forId = '0'; }
          var colspan = 2;

          if("name" in department){
            var topNode = newOrgChartRow();

            var topElement = newOrgChartDepartment(department);
            switch(typeof department.type !== "undefined" && department.type.toLowerCase()) {
              case "tree":
                            var colspan = 2;
                            topNode.addClass('orgChart-row-tree');
                            break;
              case "bullet":
                            var colspan = 1;
                            topNode.addClass('orgChart-row-bullet');
                            break;
            }
            if(optionalNodeClass !== "undefined"){
              topElement.addClass(optionalNodeClass); 
            }

            //1. Build Top Node
            topNode.append(
              newOrgChartCol(1,colspan)
              .append(
                newOrgChartCell()
                .append(
                  topElement
                )
              )
            );
            container.append(topNode);
            //2. Build childs Node
            buildNodeChildren(container, department);

          }else{
            //Empty Element, used to have a left or right Column
             var topNode = 
                  newOrgChartRow()
                  .append(
                    newOrgChartCol(1,colspan)
                    .append(
                      ""
                    )
                  );
            container.append(topNode);           
          }
        }

        function buildNodeChildren(container, department){
          //console.log(department.type);
          var parentId = forId -1;
          switch(typeof department.type !== "undefined" && department.type.toLowerCase()) {
              case "expand":
                if("departments" in department){
                  var subNodes = department.departments;
                  if(typeof subNodes !== "undefined" && subNodes.constructor === Array && subNodes.length > 0 && subNodes[0].constructor === Array) {
                    buildNodeChildrenMultipleExpand(container, subNodes);
                  }else{
                    if(subNodes.length % 2 == 0){
                      buildNodeChildrenExpand(container, subNodes, true);
                    }else{
                      buildNodeChildrenExpand(container, subNodes);
                    }
                  }
                }
                break;
              case "tree":
                var subNodes = department.departments;
                buildNodeChildrenTree(container, subNodes);
                break;
              case "bullet":
                var subNodes = department.departments;
                buildNodeChildrenBullet(container, subNodes);
                break;
          }

          var lastChildId = forId -1;
          if(lastChildId > parentId){
            var childId = [];  //Will be the array with only the subElements N-1 ID
            
            for(i=(parentId+1);i<=lastChildId;i++){
              if($.inArray(i, arrayChildrenAlreadyLinked) == -1){
                arrayChildrenAlreadyLinked.push(i);
                childId.push(i); 
              }
            }
            var nodeToLink = {"type" : department.type, "parent" : parentId, "children": childId};
            //console.log(nodeToLink);
            arrayToLink.push(nodeToLink);
          }
                   
        }

        function buildNodeChildrenExpand(container, subNodes, optionalSplit, optionalRowTopMargin, optionalFloat, optionalNodeClass){
          if (typeof optionalRowTopMargin === 'undefined') { optionalRowTopMargin = '40'; }
          if (typeof optionalSplit === 'undefined') { optionalSplit = false; }
          if (typeof optionalFloat === 'undefined') { optionalFloat = true; }
          /*
          * Expand Is Tricky, First have to split in Two Column each of them with 50% of the children
          * whenever happens we need minimum 2 columns
          */
          if(typeof subNodes !== "undefined" && subNodes.constructor === Array && subNodes.length > 0) {
            var currentRow = newOrgChartRow();
            if(optionalSplit){
              var half_length = Math.ceil(subNodes.length / 2);
              var leftSide = subNodes.splice(0,half_length);
              //The LEFT SIDE (WE WILL NEED INSIDE A NEW TABLE WITH ONE ROW AND N COLUMNS)
              var NodeLeftChilElements = $('<table>').addClass('orgChart-table');
              if(optionalFloat) NodeLeftChilElements.addClass('orgChart-table-left');
              var NodeLeftChilElementsROW = newOrgChartRow();
              $.each(leftSide, function(key, sdepartment){
                var NodeLeftChilOneElement = $('<table>').addClass('orgChart-table');
                buildNode(NodeLeftChilOneElement, sdepartment, optionalNodeClass);
                var NodeLeftChilElementsCOL = newOrgChartCol(leftSide.length,1);
                NodeLeftChilElementsCOL.append(NodeLeftChilOneElement);
                NodeLeftChilElementsROW.append(NodeLeftChilElementsCOL);
              });
              NodeLeftChilElements.append(NodeLeftChilElementsROW);
              currentRow.append(newOrgChartCol(2,1).css({'padding-top':  optionalRowTopMargin + 'px'}).append(NodeLeftChilElements));
            }
            var rightSide = subNodes;
            var NodeRightChilElements = $('<table>').addClass('orgChart-table');
            if(optionalSplit && optionalFloat) NodeRightChilElements.addClass('orgChart-table-right')
            var NodeRightChilElementsROW = newOrgChartRow();                  
            $.each(rightSide, function(key, sdepartment){
              var NodeRightChilOneElement = $('<table>').addClass('orgChart-table');
              buildNode(NodeRightChilOneElement, sdepartment, optionalNodeClass);
              var NodeRightChilElementsCOL = newOrgChartCol(rightSide.length,1);
              NodeRightChilElementsCOL.append(NodeRightChilOneElement);
              NodeRightChilElementsROW.append(NodeRightChilElementsCOL);
            });
            NodeRightChilElements.append(NodeRightChilElementsROW);

            
            currentRow.append(newOrgChartCol(2,1).css({'padding-top':  optionalRowTopMargin + 'px'}).append(NodeRightChilElements));
            container.append(currentRow);
          }
        }
        function buildNodeChildrenMultipleExpand(container, subNodesArr){
          if(typeof subNodesArr !== "undefined" && subNodesArr.constructor === Array && subNodesArr.length > 0) {
            $.each(subNodesArr, function(key, subNodes){
              
              if(typeof subNodes !== "undefined" && subNodes.constructor === Array && subNodes.length > 0 && subNodes.length < 3 && "position" in subNodes[0]) {
                
                if(subNodes.length === 1){
                  subNodes.push({});
                }
                if(subNodes[0].position.toLowerCase() === "right"){
                  subNodes.reverse();
                }
                buildNodeChildrenExpand(container, subNodes, true, 0, true, "orgChart-expand-side");
              }else{
                buildNodeChildrenExpand(container, subNodes, true);
              }
            });  
          }
        }
        function buildNodeChildrenTree(container, subNodesArr){
          if(typeof subNodesArr !== "undefined" && subNodesArr.constructor === Array && subNodesArr.length > 0) {
            $.each(subNodesArr, function(key, subNodes){
              var tempArray = [];
              tempArray.push({});
              tempArray.push(subNodes);
              buildNodeChildrenExpand(container, tempArray, true, 0);
            });  
          }
        }
        /*
        function buildNodeChildrenTreeNew(container, subNodesArr){
          if(typeof subNodesArr !== "undefined" && subNodesArr.constructor === Array && subNodesArr.length > 0) {
            var bulletList = $('<ul>').addClass('orgChart-ul-tree');
            $.each(subNodesArr, function(key, department){
              var bulletElement = newOrgChartDepartment(department.name,department.manager,department.assistants);
              bulletElement.addClass('orgChart-cell-bullet');
              bulletList.append($('<li>').addClass('orgChart-li-tree').append(bulletElement));
            });
            container.append($('<tr>').append($('<td>').append(bulletList)));  
          }
        } */       
        /*
        * Bullet is a special case because we a re at the end
        */
        function buildNodeChildrenBullet(container, subNodesArr){
          if(typeof subNodesArr !== "undefined" && subNodesArr.constructor === Array && subNodesArr.length > 0) {
            var bulletList = $('<ul>').addClass('orgChart-ul-bullet');
            $.each(subNodesArr, function(key, department){
              var bulletElement = newOrgChartDepartment(department, '0');
              bulletElement.addClass('orgChart-cell-bullet');
              bulletList.append($('<li>').addClass('orgChart-li-bullet').append(bulletElement));
            });
            container.append($('<tr>').append($('<td>').append(bulletList)));  
          }
        }
        //Row Element
        function newOrgChartRow(){
          return $('<tr>')
          .css({})
          .addClass( 'orgChart-row' );
        }
        //Col Element
        function newOrgChartCol(colCount, colSpan){
          return $('<td colspan=' + colSpan + ' align="center">')
          .css({
            'width':   100/colCount + '%',
          })
          .addClass( 'orgChart-col' );
        }        
        //Cell Element
        function newOrgChartCell(){
          return $('<div>')
          .css({})
          .addClass( 'orgChart-cell' );
        }
        //Inside Cell Element
        function newOrgChartDepartment(department, border){
          var name = "", manager = {}, assistants = [];
          if(department.name)name = department.name; 
          if(department.hasOwnProperty('manager'))manager = department.manager;
          if(department.assistants)assistants = department.assistants;

          department = $('<div>');
          
          //Case 1 Box with picture
          if(!border){
            var departmentId = 'departmentBox_' + forId++;
            department.addClass( 'departmentBox' ).attr('id', departmentId);  
            arrayDepartmentToDisplay.push({'id':departmentId,'name':name,'manager':manager, "assistants" : assistants});
            if(typeof manager == {} || !manager.name){
              department.addClass( 'departmentBox-empty' );  
            }

            department.append(
              $('<div>').addClass('departmentBox-header').append(
                $('<p style="white-space: nowrap;">').text(name)
              )
            );
            var body = $('<div>').addClass('departmentBox-body');
            var bodyLeft = $('<div>').addClass('departmentBox-body-left').append(
              $('<img src="'+settings.defaultPicture+'" />')
            );
            if(manager.image){
              bodyLeft.html($('<img src="'+settings.imageFolderUrl+manager.image+'" />'));  
            }
            var bodyRight = $('<div>').addClass('departmentBox-body-right');
            if(manager.name)
              bodyRight.append($('<p style="white-space: nowrap;">').addClass('departmentBox-body-right-name').text(manager.name));
            if(manager.position) bodyRight.append($('<p style="white-space: nowrap;">').addClass('departmentBox-body-right-position').text(manager.position));
            department.append(
              $('<div>').addClass('departmentBox-body').append(bodyLeft).append(bodyRight)
            );

          }else{
            department.append($('<p style="white-space: nowrap;">').text(name)).addClass( 'departmentBullet' );           
            if(typeof manager !== {}){
              if(manager.name)
                department.append($('<p style="white-space: nowrap;">>').text(manager.name));
              if(manager.position) department.append($('<p style="white-space: nowrap;">>').text(manager.position));
            }
            if(assistants.length > 0) {
              $.each(assistants, function(key, assistant){
                var assistantPosition = (assistant.position) ? assistant.position : '';
                department
                  .append($('<p style="white-space: nowrap;">>').text('(' + assistant.name + ') ' + assistantPosition))
              });
            }
          }

          return department;
        }        
   
        function connectTheNodes(){
          $.each(arrayToLink, function(key, node){ 
            switch(typeof node.type !== "undefined" && node.type.toLowerCase()) {
              case "expand":
                    if(typeof node.children !== "undefined" && node.children.constructor === Array && node.children.length > 0) {
                      //get the last child to draw the trunc
                      var lastChild = node.children[node.children.length-1];
                      var truncX = $('#departmentBox_'+ node.parent).position().left + ($('#departmentBox_'+ node.parent).outerWidth() / 2);
                      var truncBuffer = 20;
                      if($( '#departmentBox_' + lastChild).hasClass('orgChart-expand-side')){
                        truncBuffer = 0;  
                      }
                      $('#connectNode').append(
                        drawVerticalLine2(
                            truncX, 
                            $('#departmentBox_'+ node.parent).position().top + $('#departmentBox_'+ node.parent).outerHeight(),
                            $( '#departmentBox_' + lastChild).position().top - truncBuffer
                        ));
                      //The tricky part, for multiple ones, split / level
                      var arrayOfRows = [];
                      var arrayCurrentRow = [];
                      arrayCurrentRow.push(node.children[0]);
                      for(var i=1;i<node.children.length;i++){
                        //console.log( arrayCurrentRow[arrayCurrentRow.length-1]);
                        var lastChildXPos = $('#departmentBox_'+ arrayCurrentRow[arrayCurrentRow.length-1]).position().top;
                        var currentChildXPos = $('#departmentBox_'+ node.children[i]).position().top;
                        //10px error max
                        if(lastChildXPos <= currentChildXPos + 10 && lastChildXPos >= currentChildXPos - 10 ){
                          arrayCurrentRow.push(node.children[i]);  
                        }else{
                          arrayOfRows.push(arrayCurrentRow);
                          arrayCurrentRow = [];
                          arrayCurrentRow.push(node.children[i]);  
                        }
                      }
                      arrayOfRows.push(arrayCurrentRow);
                      //console.log(arrayOfRows);
                      $.each(arrayOfRows, function(key, row){
                        //case LEFT / RIGHT
                        //console.log(row);
                        if(row.length <= 2 && $('#departmentBox_'+ row[0]).hasClass('orgChart-expand-side')){
                          var xstart = 0;
                          var xstop = 0;
                          if(row.length == 2){
                             xstart = $('#departmentBox_'+ row[0]).position().left + ($('#departmentBox_'+ node.parent).outerWidth());
                             xstop = $('#departmentBox_'+ row[1]).position().left;  
                          }else{
                            if($('#departmentBox_'+ row[0]).position().left > truncX){
                              xstart = truncX;
                              xstop = $('#departmentBox_'+ row[0]).position().left;
                              //console.log(xstart + " " + xstop);
                            }else{
                              xstart = $('#departmentBox_'+ row[0]).position().left + ($('#departmentBox_'+ node.parent).outerWidth());
                              xstop = truncX;
                            }
                          }
                          $('#connectNode').append(
                            drawHorizontalLine(
                                $('#departmentBox_'+ row[0]).position().top + ($('#departmentBox_'+ row[0]).outerHeight() / 2), 
                                xstart,
                                xstop
                            ));
                        }else{
                          xstart = $('#departmentBox_'+ row[0]).position().left + ($('#departmentBox_'+ row[0]).outerWidth()/2);
                          xstop = $('#departmentBox_'+ row[row.length-1]).position().left + ($('#departmentBox_'+ row[row.length-1]).outerWidth()/2);
                          yBuffer = $('#departmentBox_'+ row[0]).position().top - 20;
                          $('#connectNode').append(
                            drawHorizontalLine(
                              yBuffer,
                              xstart,
                              xstop
                            ));
                          $.each(row, function(key, rowDepartment){
                            $('#connectNode').append(
                              drawVerticalLine2(
                                $('#departmentBox_'+ rowDepartment).position().left + ($('#departmentBox_'+ rowDepartment).outerWidth()/2),
                                yBuffer,
                                $('#departmentBox_'+ rowDepartment).position().top
                              ));
                          });
                        }

                      });
                      

                    }
                    break;

              case "tree":
                    var lastChild = node.children[node.children.length-1];
                    $('#connectNode').append(
                      drawVerticalLine2(
                          $('#departmentBox_'+ node.parent).position().left + ($('#departmentBox_'+ node.parent).outerWidth() / 2), 
                          $('#departmentBox_'+ node.parent).position().top + $('#departmentBox_'+ node.parent).outerHeight(),
                          $( '#departmentBox_' + lastChild).position().top + ($('#departmentBox_'+ lastChild).outerHeight() / 2)
                      ));
                    $.each(node.children, function(key, subnode){
                      $('#connectNode').append(
                        drawHorizontalLine(
                            $('#departmentBox_'+ subnode).position().top + ($('#departmentBox_'+ subnode).outerHeight() / 2), 
                            $('#departmentBox_'+ node.parent).position().left + ($('#departmentBox_'+ node.parent).outerWidth() / 2),
                            $( '#departmentBox_' + subnode).position().left
                        ));
                    });
                    break;              
            }
          });
        }
        function drawVerticalLine(x, ystart, size){
          return $('<div>')
          .addClass( 'chartLine' )
          .css({
            'height': size,
            'top': ystart,
            'left': x
          })
          .addClass( 'vertical-chartLine' );
        }
        function drawVerticalLine2(x, ystart, ystop){
          return $('<div>')
          .addClass( 'chartLine' )
          .css({
            'height': ystop - ystart,
            'top': ystart,
            'left': x
          })
          .addClass( 'vertical-chartLine' );
        }
        function drawHorizontalLine(y, xstart, xstop){
          return $('<div>')
          .css({
            'top': y,
            'left': xstart,
            'width': xstop - xstart,
          })
          .addClass( 'chartLine' )
          .addClass( 'horizontal-chartLine' );
        }
        function drawHorizontalLine2(y, xstart, size){
          return $('<div>')
          .css({
            'top': y,
            'left': xstart,
            'width': size,
          })
          .addClass( 'chartLine' )
          .addClass( 'horizontal-chartLine' );
        }

        function zoomAction(){
          var a="undefined"!=typeof InstallTrigger;
          a?$(".orgChart-canvas").css({MozTransform:"scale("+zoomState+","+zoomState+")","transform-origin":"0 0"}):$(".orgChart-canvas").css({zoom:zoomState})
        }
         
        function addDepartmentClick(){
          $('.departmentBox').not('.departmentBox-empty').click(function(){
            var self= $(this);
            var departmentToDisplay = $.grep(arrayDepartmentToDisplay, function(e){return e.id == self.attr('id'); });       
            if(departmentToDisplay.length > 0){
              var department = departmentToDisplay[0];
              $('.departmentDetails-background').show();
              var departmentDetailsContainer = $('<div>').addClass('departmentDetails-container').click( function(event){
                event.stopPropagation();
              });
              departmentDetailsContainer.append($('<div>').addClass('departmentDetails-header').text(department.name));
              var departmentDetailsBody = $('<div>').addClass('departmentDetails-body');
              var assistants = department.assistants.slice();
              assistants.unshift(department.manager);

              $.each(assistants, function(key, manager){
                var bodyLeft = $('<div>').addClass('departmentDetails-body-row-left').append(
                  $('<img src="'+settings.defaultPicture+'" />')
                );
                if(manager.image){
                  bodyLeft.html($('<img src="'+settings.imageFolderUrl+manager.image+'" />'));  
                }
                var bodyRight = $('<div>').addClass('departmentDetails-body-row-right');
                if(manager.name)
                  bodyRight.append($('<p style="white-space: nowrap;">').addClass('departmentDetails-body-row-right-name').text(manager.name));
                if(manager.position) bodyRight.append($('<p style="white-space: nowrap;">').addClass('departmentDetails-body-row-right-position').text(manager.position));
                departmentDetailsBody.append(
                  $('<div>').addClass('departmentDetails-body-row').append(bodyLeft).append(bodyRight)
                );                
              });
              var departmentDetailsClose = $('<div>').addClass('departmentDetails-body-close').click( function(event){
                departmentDetailsContainer.css('top');
                departmentDetailsContainer.css('top', '-50%');
                setTimeout(function(){$('.departmentDetails-background').hide().html('')}, 600);
              });

              departmentDetailsBody.append(departmentDetailsClose);
              departmentDetailsContainer.append(departmentDetailsBody);
              $('.departmentDetails-background').append(departmentDetailsContainer);
              departmentDetailsContainer.css('top');
              departmentDetailsContainer.css('top', '50%');
            }
          });
        }
  }
}(jQuery));