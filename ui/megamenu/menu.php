    <!-- Main Navigation -->
	    <a class="sr-only sr-only-focusable" href="#content">Skip to main content</a>
        <nav class="navbar navbar-blue-dark no-border-radius xs-height75 navbar-static-top" id="main_navbar" role="navigation">
        	<div class="container-fluid">
        		<!-- Brand and toggle get grouped for better mobile display -->
        		<div class="navbar-header">
        			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#MegaNavbarID">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
        			</button>
        		</div>
        		<!-- Collect the nav links, forms, and other content for toggling -->
        		<div class="collapse navbar-collapse" id="MegaNavbarID">
        			<!-- regular link -->
        			<ul class="nav navbar-nav navbar-left">

        				<li class="dropdown-grid">
        					<a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-sitemap"></i>&nbsp;<span class="hidden-sm">องค์กร</span><span class="caret"></span></a>
        					<div class="dropdown-grid-wrapper" role="menu">
        						<ul class="dropdown-menu col-xs-12 col-sm-10 col-md-6 col-lg-6">
        							<li>
        								<div id="carousel-eg">
        									<div class="row">
        										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
        											<ol class="carousel-indicators navbar-carousel-indicators h-divided">
        												<li data-target="#carousel-eg" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">โครงสร้างองค์กร<span class="hidden-xs desc">กำหนดโครงสร้าง และผังองค์กร</span></a></li>
        												<li data-target="#carousel-eg" data-slide-to="1" class=""><a href="javascript:void(0);" class="">ตำแหน่งงาน<span class="hidden-xs desc">ตำแหน่งงาน อัตรากำลัง</span></a></li>
        												<li data-target="#carousel-eg" data-slide-to="2" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐาน<span class="hidden-xs desc">ข้อมูลพื้นฐานที่เกี่ยวข้อง</span></a></li>
        											</ol>
        										</div>
        										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        											<div class="carousel-inner">
        												<div class="item active">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
                                                                	<li><a href="/appoint"><i class="fa fa-file"></i>&nbsp; จัดการโครงสร้างองค์กร</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
                                                                	<li><a href="/structure"><i class="fa fa-file"></i>&nbsp; จัดการตำแหน่งงาน</a></li>
                                                                    <li><a href="/sequence"><i class="fa fa-file"></i>&nbsp; อัตรากำลัง</a></li>
						                        					<li><a href="/reportposition"><i class="fa fa-file"></i>&nbsp; ตำแหน่งว่าง</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
                                                                	<li><a href="/position"><i class="fa fa-file"></i>&nbsp; ชื่อตำแหน่ง</a></li>
                                                                    <li><a href="/level"><i class="fa fa-file"></i>&nbsp; ชื่อระดับ</a></li>
                                                                	<li><a href="/workplace"><i class="fa fa-file"></i>&nbsp; สถานที่ทำงาน</a></li>
                                                                	<li><a href="/workstatus"><i class="fa fa-file"></i>&nbsp; สถานะพนักงาน</a></li>
                                                                </ul>
        													</div>
        												</div>
        											</div>
        										</div>
        									</div>
        								</div>
        							</li>
        						</ul>
        					</div>
        				</li>

        				<li class="dropdown-grid">
        					<a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-user"></i>&nbsp;<span class="hidden-sm">พนักงาน</span><span class="caret"></span></a>
        					<div class="dropdown-grid-wrapper" role="menu">
        						<ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-7">
        							<li>
        								<div id="carousel-eg2">
        									<div class="row">
        										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
        											<ol class="carousel-indicators navbar-carousel-indicators h-divided">
        												<li data-target="#carousel-eg2" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">ทะเบียนประวัติ<span class="hidden-xs desc">ข้อมูลทะเบียนประวัติของพนักงาน</span></a></li>
        												<li data-target="#carousel-eg2" data-slide-to="1" class=""><a href="javascript:void(0);" class="">เอกสารสัญญา<span class="hidden-xs desc">สัญญาจ้าง ใบรับรอง</span></a></li>
        												<li data-target="#carousel-eg2" data-slide-to="2" class=""><a href="javascript:void(0);" class="">รายงาน<span class="hidden-xs desc">รายงานที่เกี่ยวข้อง</span></a></li>
        												<li data-target="#carousel-eg2" data-slide-to="3" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐาน<span class="hidden-xs desc">ข้อมูลพื้นฐานที่เกี่ยวข้อง</span></a></li>
        											</ol>
        										</div>
        										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        											<div class="carousel-inner">
        												<div class="item active">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
                                                                	<li><a href="/reg"><i class="fa fa-file"></i>&nbsp; จัดการทะเบียนประวัติ</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
                                                                	<li><a href="/contracttype"><i class="fa fa-file"></i>&nbsp; จัดการสัญญาจ้าง</a></li>
											                        <li><a href="/cerificatework"><i class="fa fa-file"></i>&nbsp; ใบรับรองการทำงาน</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-4by3">
        														<ul>
                        <li><a href="/reportbirthday"><i class="fa fa-file"></i>&nbsp; วันเกิดพนักงาน</a></li>
                        <li><a href="/reportcontract"><i class="fa fa-file"></i>&nbsp; พนักงานครบกำหนดสัญญาจ้าง</a></li>
                        <li><a href="/reportretired"><i class="fa fa-file"></i>&nbsp; พนักงานที่เกษียณอายุ</a></li>
                        <li><a href="/reportstatusemployees"><i class="fa fa-file"></i>&nbsp; พนักงานที่พ้นสภาพ</a></li>
                        <li><a href="/reportrank"><i class="fa fa-file"></i>&nbsp; ผู้มีสิทธิ์เลื่อนระดับขั้น</a></li>
                        <li><a href="/reportstatage"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ อายุงาน</a></li>
                        <li><a href="/reportstatcontract"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ สัญญาจ้าง</a></li>
                        <li><a href="/reportstatretired"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ เกษียณอายุ</a></li>
						<li><a href="/reportstatstatus"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ พ้นสภาพ</a></li>
                        <li><a href="/reportstatrank"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ เลื่อนระดับ</a></li>
																</ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
                                                                	<li><a href="/stafftype"><i class="fa fa-file"></i>&nbsp; ประเภทพนักงาน</a></li>
											                        <li><a href="/mistaketype"><i class="fa fa-file"></i>&nbsp; ประเภทการลงโทษ</a></li>
                                                                </ul>
        													</div>
        												</div>

        											</div>
        										</div>
        									</div>
        								</div>
        							</li>
        						</ul>
        					</div>
        				</li>

        				<li class="dropdown-grid">
        					<a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-heartbeat"></i>&nbsp;<span class="hidden-sm">สวัสดิการ</span><span class="caret"></span></a>
        					<div class="dropdown-grid-wrapper" role="menu">
        						<ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-8">
        							<li>
        								<div id="carousel-eg3">
        									<div class="row">
        										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
        											<ol class="carousel-indicators navbar-carousel-indicators h-divided">
        												<li data-target="#carousel-eg3" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">เบิกสวัสดิการ<span class="hidden-xs desc">เบิกจ่ายและตรวจสอบสิทธิ์สวัสดิการ</span></a></li>
        												<li data-target="#carousel-eg3" data-slide-to="1" class=""><a href="javascript:void(0);" class="">การตรวจสุขภาพ<span class="hidden-xs desc">ข้อมูลการตรวจสุขภาพ</span></a></li>
        												<li data-target="#carousel-eg3" data-slide-to="2" class=""><a href="javascript:void(0);" class="">รายงาน<span class="hidden-xs desc">รายงานที่เกี่ยวข้อง</span></a></li>
        												<li data-target="#carousel-eg3" data-slide-to="3" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐาน<span class="hidden-xs desc">ข้อมูลพื้นฐานที่เกี่ยวข้อง</span></a></li>
        											</ol>
        										</div>
        										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        											<div class="carousel-inner">
        												<div class="item active">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
																	<li><a href="/staffwelfareright"><i class="fa fa-file"></i>&nbsp; กำหนดสิทธิ์สวัสดิการ</a></li>
                                                                    <li><a href="/staffwelfaretake"><i class="fa fa-file"></i>&nbsp; เบิกสวัสดิการ</a></li>
																	<li><a href="/staffwelfarecheck"><i class="fa fa-file"></i>&nbsp; ตรวจสอบ/เรียกดูสิทธิ์สวัสดิการ</a></li>   
                                                                    <li><a href="/cerificatewelfare"><i class="fa fa-file"></i>&nbsp; แบบฟอร์ม / หนังสือรับรอง</a></li>                                                             
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
																	<li><a href="/reporthealthcheck"><i class="fa fa-file"></i>&nbsp; รายงานตรวจสุขภาพ</a></li>
																	<li><a href="/reportstaffnothealthchecklist"><i class="fa fa-file"></i>&nbsp; รายชื่อพนักงานที่ไม่ตรวจสุขภาพ</a></li>
																	<li><a href="/import"><i class="fa fa-file"></i>&nbsp; Import รายงานตรวจสุขภาพ</a></li>
																	<li><a href="/export"><i class="fa fa-file"></i>&nbsp; Export รายงานตรวจสุขภาพ</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
																	<li><a href="/reportmedicalfeehistory"><i class="fa fa-file"></i>&nbsp; ประวัติการเบิกค่ารักษาพยาบาล</a></li>
                                                                    <li><a href="/reportsicknessrank"><i class="fa fa-file"></i>&nbsp; สถิติการเจ็บป่วยของพนักงาน</a></li>
																	<li><a href="/reportmedicalfeerank"><i class="fa fa-file"></i>&nbsp; สถิติการเบิกค่ารักษาพยาบาล</a></li>
																	<li><a href="/reportmunstaffrank"><i class="fa fa-file"></i>&nbsp; จำนวนพนักงานตามเชิงสถิติ</a></li>
																	<li><a href="/reportmedicalfeestaff"><i class="fa fa-file"></i>&nbsp; สรุปการเบิกค่ารักษาพยาบาล</a></li>
																	<li><a href="/reportmedicalfeeall"><i class="fa fa-file"></i>&nbsp; ยอดการเบิกค่ารักษาพยาบาล</a></li>
                         											<li><a href="/reporthealthcheckfeestatement"><i class="fa fa-file"></i>&nbsp; ยอดการเบิกค่าตรวจสุขภาพ</a></li>
																	<li><a href="/reportduemedicalfee"><i class="fa fa-file"></i>&nbsp; สรุปเงินค้างชำระค่ารักษาพยาบาล</a></li>
                                                                </ul>
        													</div>
        												</div>
       													<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
                      												<li><a href="/hospitaltype"><i class="fa fa-file"></i>&nbsp; ประเภทสถานพยาบาล</a></li>
																	<li><a href="/hospital"><i class="fa fa-file"></i>&nbsp; สถานพยาบาล</a></li> 
																	<li><a href="/patienttype"><i class="fa fa-file"></i>&nbsp; ประเภทผู้ป่วย</a></li>
                      												<li><a href="/welfaretype"><i class="fa fa-file"></i>&nbsp; ประเภทสวัสดิการ</a></li> 
																	<li><a href="/welfarelistfee"><i class="fa fa-file"></i>&nbsp; รายการเบิกค่ารักษาพยาบาล</a></li>
																	<li><a href="/welfareright"><i class="fa fa-file"></i>&nbsp; สิทธิ์สวัสดิการ</a></li> 
                      												<li><a href="/welfarepay"><i class="fa fa-file"></i>&nbsp; ช่องทางการรับเงิน</a></li>
                      												<li><a href="/welfarecalendar"><i class="fa fa-file"></i>&nbsp; ปฏิทินการจ่ายเงิน</a></li> 
                      												<li><a href="/welfarefundlife"><i class="fa fa-file"></i>&nbsp; ข้อมูลกองทุนสำรองเลี้ยงชีพ</a></li> 
																	<li><a href="/welfarefundpension"><i class="fa fa-file"></i>&nbsp; ข้อมูลกองทุนบำเหน็จ</a></li> 

                                                                </ul>
        													</div>
        												</div>                                                        
        											</div>
        										</div>
        									</div>
        								</div>
        							</li>
        						</ul>
        					</div>
        				</li>

        				<li class="dropdown-grid">
        					<a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-check-square"></i>&nbsp;<span class="hidden-sm">ประเมิน/ขึ้นเงินเดือน</span><span class="caret"></span></a>
        					<div class="dropdown-grid-wrapper" role="menu">
        						<ul class="dropdown-menu col-xs-12 col-sm-10 col-md-6col-lg-6">
        							<li>
        								<div id="carousel-eg4">
        									<div class="row">
        										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
        											<ol class="carousel-indicators navbar-carousel-indicators h-divided">
        												<li data-target="#carousel-eg4" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">ประเมินพฤติกรรม/KPI<span class="hidden-xs desc">แบบประเมินพฤติกรรม/KPI</span></a></li>
        												<li data-target="#carousel-eg4" data-slide-to="1" class=""><a href="javascript:void(0);" class="">ประเมิน Competency<span class="hidden-xs desc">แบบประเมิน Competency</span></a></li>
        												<li data-target="#carousel-eg4" data-slide-to="2" class=""><a href="javascript:void(0);" class="">พิจารณาขึ้นเงินเดือน<span class="hidden-xs desc">ข้อมูลการพิจารณาขึ้นเงินเดือน</span></a></li>
        											</ol>
        										</div>
        										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        											<div class="carousel-inner">
        												<div class="item active">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
              <li><a href="/evaluation"><i class="fa fa-file"></i>&nbsp;จัดการแบบประเมิน </a></li>  
              <li><a href="/rate/recent"><i class="fa fa-file"></i>&nbsp; ทำการประเมิน</a></li>
              <li><a href="/rate/tscore"><i class="fa fa-file"></i>&nbsp; คะแนนการประเมิน</a></li>
              <li><a href="/competencys/subjectkpi"><i class="fa fa-file"></i>&nbsp; จัดการข้อมูล KPI</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
              <li><a href="/competency"><i class="fa fa-file"></i>&nbsp;จัดการแบบประเมิน </a></li> 
              <li><a href="/competencys/recent"><i class="fa fa-file"></i>&nbsp; ทำการประเมิน</a> </li>
              <li><a href="/competencys/subject"><i class="fa fa-file"></i>&nbsp; จัดการข้อมูล Competency</a></li> 
              <li><a href="/competencys/repost"><i class="fa fa-file"></i>&nbsp; รายชื่อผู้ที่แก้ Gap</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
               <li><a href="/salarys"><i class="fa fa-file"></i>&nbsp; ภาพรวมการขึ้นเงินเดือน</a></li>
               <li><a href="/salarys/dept"><i class="fa fa-file"></i>&nbsp; จัดการขึ้นเงินเดือน</a> 
                                                                </ul>
        													</div>
        												</div>
        											</div>
        										</div>
        									</div>
        								</div>
        							</li>
        						</ul>
        					</div>
        				</li>

        				<li class="dropdown-grid">
        					<a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-graduation-cap"></i>&nbsp;<span class="hidden-sm">พัฒนาบุคลากร</span><span class="caret"></span></a>
        					<div class="dropdown-grid-wrapper" role="menu">
        						<ul class="dropdown-menu col-xs-12 col-sm-10 col-md-6 col-lg-6">
        							<li>
        								<div id="carousel-eg5">
        									<div class="row">
        										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
        											<ol class="carousel-indicators navbar-carousel-indicators h-divided">
        												<li data-target="#carousel-eg5" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">ศึกษาต่อ<span class="hidden-xs desc">ข้อมูลการลาศึกษาต่อ</span></a></li>
        												<li data-target="#carousel-eg5" data-slide-to="1" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐานการศึกษา<span class="hidden-xs desc">ข้อมูลพื้นฐานด้านการศึกษา</span></a></li>
        												<li data-target="#carousel-eg5" data-slide-to="2" class=""><a href="javascript:void(0);" class="">อบรม<span class="hidden-xs desc">ข้อมูลการอบรม</span></a></li>
        												<li data-target="#carousel-eg5" data-slide-to="3" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐานอบรม<span class="hidden-xs desc">ข้อมูลพื้นฐานด้านการอบรม</span></a></li>
        											</ol>
        										</div>
        										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        											<div class="carousel-inner">
        												<div class="item active">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
                                                                	<li><a href="/conedu"><i class="fa fa-file"></i>&nbsp; ลาศึกษาต่อ</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
       														<div class="embed-responsive embed-responsive-16by9">
        														<ul>
			<li><a href="/education"><i class="fa fa-file"></i>&nbsp; ระดับการศึกษา</a></li>
			<li><a href="/eduplace"><i class="fa fa-file"></i>&nbsp; สถานศึกษา</a></li>
			<li><a href="/edutype"><i class="fa fa-file"></i>&nbsp; ประเภทสถานศึกษา</a></li>
			<li><a href="/edufaculty"><i class="fa fa-file"></i>&nbsp; คณะ / สายการเรียน</a></li>
            <li><a href="/edudepartment"><i class="fa fa-file"></i>&nbsp; สาขา</a></li>
			<li><a href="/edudegree"><i class="fa fa-file"></i>&nbsp; วุฒิการศึกษา</a></li>
			<li><a href="/country"><i class="fa fa-file"></i>&nbsp; ประเทศ</a></li>
                                                                </ul>
        													</div>
         												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
           	<li><a href="/traintype"><i class="fa fa-file"></i>&nbsp; ประเภทหลักสูตร</a></li>
           	<li><a href="/trainer"><i class="fa fa-file"></i>&nbsp; วิทยากร</a></li>
           	<li><a href="/trainyear"><i class="fa fa-file"></i>&nbsp; วางแผนงบประมาณ</a></li>
           	<li><a href="/traincourse"><i class="fa fa-file"></i>&nbsp; จัดอบรม</a></li>
           	<li><a href="/trainevaluate"><i class="fa fa-file"></i>&nbsp; ประเมินผลการอบรม</a></li>
			<li><a href="/traineecourse"><i class="fa fa-file"></i>&nbsp; กำหนดผู้เข้าร่วมอบรม</a></li>
			<li><a href="/trainprice"><i class="fa fa-file"></i>&nbsp; บันทึกค่าใช้จ่าย</a></li>
                                                                </ul>
        													</div>
        												</div>
        												<div class="item">
        													<div class="embed-responsive embed-responsive-16by9">
        														<ul>
           	<li><a href="/traintype"><i class="fa fa-file"></i>&nbsp; ประเภทหลักสูตร</a></li>
           	<li><a href="/trainer"><i class="fa fa-file"></i>&nbsp; วิทยากร</a></li>
           	<li><a href="/trainyear"><i class="fa fa-file"></i>&nbsp; วางแผนงบประมาณ</a></li>
           	<li><a href="/traincourse"><i class="fa fa-file"></i>&nbsp; จัดอบรม</a></li>
           	<li><a href="/trainevaluate"><i class="fa fa-file"></i>&nbsp; ประเมินผลการอบรม</a></li>
			<li><a href="/traineecourse"><i class="fa fa-file"></i>&nbsp; กำหนดผู้เข้าร่วมอบรม</a></li>
			<li><a href="/trainprice"><i class="fa fa-file"></i>&nbsp; บันทึกค่าใช้จ่าย</a></li>
                                                                </ul>
        													</div>
        												</div>
        											</div>
        										</div>
        									</div>
        								</div>
        							</li>
        						</ul>
        					</div>
        				</li>
        				<li>
        					<a href="#"><i class="fa fa-video-camera"></i> ข้อมูลการลา</a>
						</li>


        			</ul>

        		</div>
        	</div>
        </nav>
        
        <div id="responsive-menu-container"></div>
