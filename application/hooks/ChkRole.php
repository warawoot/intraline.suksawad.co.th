<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ChkRole {
	protected $CI;

	public function __construct() {
	    $this->CI = & get_instance();

	}


  	public function check_login(){

	  	$CurrentClass = $this->CI->router->fetch_class();
	  	$RoleID = $this->CI->session->userdata('roleID');
	  	$userID = $this->CI->session->userdata('userID');

	  	$ClassRole1 = array(
	  		'dashboard',
	  		'passwordreset',
	  		'auth',
	  		'logout',
	  		'absence'
	  	);

	  	$ClassRole2 = array(
	  		'dashboard',
	  		'absence',
	  		'rate',
	  		'passwordreset',
	  		'auth',
	  		'logout'
	  	);
	  	
	  	$ClassRole3 = array(
	  		'reg',
	  		'reportbirthday',
	  		'passwordreset',
	  		'appoint',
	  		'structure',
	  		'sequence',
	  		'reportposition',
	  		'position',
	  		'level',
	  		'workplace',
	  		'workstatus',
	  		'reportturnover'
	  	);

	  	if($userID){

		  	if($RoleID==1 && !in_array($CurrentClass,$ClassRole1)){
				redirect(site_url().'dashboard');
		  	}else if($RoleID==2 && !in_array($CurrentClass,$ClassRole2)){
				redirect(site_url().'dashboard');
		  	}
		}else if($CurrentClass != 'auth'){
			redirect(site_url().'logout');
		}


	  	//else is RoleID=3
  	}

  	

}
?>