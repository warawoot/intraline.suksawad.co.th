<?php
class MY_Model extends CI_Model {

    public $_table = '';
    public $_pk = 'id';
    public $_filter = 'intval';
    public $_order_by = '';
    public $_sort_by = 'asc';
    public $_timestamps = false;

    function __construct() {    	
       parent::__construct();
    }


    /**
    * Insert or Update Datas
    * 
    * @param array $data
    * @return bool
    */
    public function save($data, $id = null)
    {
 
        // Set timestamps
        if ($this->_timestamps == true) {
            $now = date('Y-m-d H:i:s');
            $id || $data['createDateTime'] = $now;
            $id || $data['CreateBy'] = $this->session->userdata('accountID');

            $data['updateDateTime'] = $now;
            $data['UpdateBy'] = $this->session->userdata('accountID');
        }
 
        // Insert
        if (empty($id)) {
            !isset($data[$this->_pk]) || $data[$this->_pk] = null;
            $this->db->set($data);
            $this->db->insert($this->_table);
            $id = $this->db->insert_id();
        }
        // Update
        else {
            $filter = $this->_filter;
            $id = $filter($id);
            $this->db->set($data);
            $this->db->where($this->_pk, $id);
            $this->db->update($this->_table);
        }
 
        return $id;
    }

    /**
    * Delete datas
    * 
    * @param $id
    * @return bool
    */
    public function delete($id)
    {
        $filter = $this->_filter;
        $id = $filter($id);
 
        if (empty($id)) {
            return false;
        }
        $this->db->where($this->_pk, $id);
        $this->db->limit(1);
        return $this->db->delete($this->_table);
    }

    /**
    * Delete datas
    * 
    * @param $where
    * @return bool
    */
    public function delete_by($arr)
    {
        
        if (empty($arr)) {
            return false;
        }
        $this->db->where($arr);
        $this->db->delete($this->_table);
    }



    /**
    * Get Lastest ID [Where by Condition]
    * 
    * @param $pk,array $arrWhere
    * @return Lastest ID
    */
     function getLastestID($pk='',$arrWhere=array()){

        if(!empty($arrWhere)){
            $this->db->select_max($pk);
            $r = $this->db->get_where($this->_table,$arrWhere);	
            return ($r->row()->$pk != NULL) ? $r->row()->$pk : 0;
        }else {
            if(empty($pk)){
                $this->db->select_max($this->_pk);
                $primary = $this->_pk;
            }else{
                $this->db->select_max($pk);
                $primary = $pk;
            }
            $r = $this->db->get($this->_table);
            
            return ($r->row()->$primary != NULL) ? $r->row()->$primary : 0;	
        }

    }

    /**
    * Get All data or Get Data Where by PK
    * 
    * @param $id
    * @return result
    */
    public function get($id = null, $single = false){
 
        if ($id != null) {
            $filter = $this->_filter;
      
            $id = $filter($id);
            $this->db->where($this->_pk, $id);
          
            $method = 'row';
        } elseif ($single == true) {
            $method = 'row';
        } else {
            $method = 'result';
        }
 
        /*if (!count($this->db->ar_orderby) && !empty($this->_order_by)) {
            $this->db->order_by($this->_order_by,$this->_sort_by);
        }*/
        if (!empty($this->_order_by)){
            $this->db->order_by($this->_order_by,$this->_sort_by);
        }
        return $this->db->get($this->_table)->$method();
    }

     public function get_by($where, $single = false,$limit = false,$offset = false)
    {
        $this->db->where($where);
        
        if($limit)
        $this->db->limit($limit,$offset);
        
        return $this->get(null, $single);
    }

    public function get_join($table_join,$condition,$where, $single = false,$limit = false,$offset = false)
    {
        if(!empty($table_join) && !empty($condition)){
            $this->db->join($table_join,$condition);
        }
        if(!empty($where))
            $this->db->where($where);
        
        if($limit)
            $this->db->limit($limit,$offset);
        
        return $this->get(null, $single);
    }

    
    /**
    * Get PK
    * 
    * @param 
    * @return PK
    */
    function getPK(){
        return $this->_pk;
    }
    
    /**
    * Get Number of Record
    * 
    * @param 
    * @return Number of Record
    */
    function getCount(){
        $count = $this->db->get($this->_table)
                          ->num_rows();
        return $count;
        
    }

    /**
    * Get Table Name
    * 
    * @param 
    * @return Table Name
    */
    function getTableName(){
        return $this->_table;
    }
}



class Default_Model extends CI_Model {

    public $_table = '';
    public $_pk = 'id';
    public $_filter = 'intval';
    public $_order_by = '';
    public $_sort_by = 'asc';
    public $_timestamps = true;

    function __construct() {        
       parent::__construct();
    }


    /**
    * Insert or Update Datas
    * 
    * @param array $data
    * @return bool
    */
    public function save($data, $id = null,$auto = true,$arrwhere = array(),$arr_upd = array())
    {
 
        // Set timestamps
        if ($this->_timestamps == true) {
            $now = date('Y-m-d H:i:s');
            $id || $data['createDateTime'] = $now;
            $id || $data['CreateBy'] = $this->session->userdata('userID');

            $data['updateDateTime'] = $now;
            $data['UpdateBy'] = $this->session->userdata('userID');
        }
 
        // Insert
        if (empty($id)) {
            //!isset($data[$this->_pk]) || $data[$this->_pk] = null;

            if(!$auto){
                 if(empty($arrwhere))
                    $r = $this->db->select_max($this->_pk)->get($this->_table);
                 else
                    $r = $this->db->select_max($this->_pk)->where($arrwhere)->get($this->_table);
                 $primary = $this->_pk;
                 $data[$this->_pk] = $r->row()->$primary+1;
                 $id = $r->row()->$primary+1;
                
                 $this->db->set($data);
                 $this->db->insert($this->_table);
                 

            }else{
                $this->db->set($data);
                
                $this->db->insert($this->_table);
                $id = $this->db->insert_id();
            }

            
        }
        // Update
        else {
            $filter = $this->_filter;
            $id = $filter($id);
            $this->db->set($data);
            if(empty($arr_upd))
                $this->db->where($this->_pk, $id);
            else
                $this->db->where($arr_upd);
            
            $this->db->update($this->_table);
        }
 
        return $id;
    }

    /**
    * Delete datas
    * 
    * @param $id
    * @return bool
    */
    public function delete($id)
    {
        $filter = $this->_filter;
        $id = $filter($id);
 
        if (empty($id)) {
            return false;
        }
        $this->db->where($this->_pk, $id);
        $this->db->limit(1);
        return $this->db->delete($this->_table);
    }

    /**
    * Delete datas
    * 
    * @param $where
    * @return bool
    */
    public function delete_by($arr)
    {
        
        if (empty($arr)) {
            return false;
        }
        $this->db->where($arr);
        return $this->db->delete($this->_table);
    }



    /**
    * Get Lastest ID [Where by Condition]
    * 
    * @param $pk,array $arrWhere
    * @return Lastest ID
    */
     function getLastestID($pk='',$arrWhere=array()){

        if(!empty($arrWhere)){
            $this->db->select_max($pk);
            $r = $this->db->get_where($this->_table,$arrWhere); 
            return ($r->row()->$pk != NULL) ? $r->row()->$pk : 0;
        }else {
            if(empty($pk)){
                $this->db->select_max($this->_pk);
                $primary = $this->_pk;
            }else{
                $this->db->select_max($pk);
                $primary = $pk;
            }
            $r = $this->db->get($this->_table);
            
            return ($r->row()->$primary != NULL) ? $r->row()->$primary : 0; 
        }

    }

    /**
    * Get All data or Get Data Where by PK
    * 
    * @param $id
    * @return result
    */
    public function get($id = null, $single = false){
 
        if ($id != null) {
            $filter = $this->_filter;
      
            $id = $filter($id);
            $this->db->where($this->_pk, $id);
          
            $method = 'row';
        } elseif ($single == true) {
            $method = 'row';
        } else {
            $method = 'result';
        }
 
        /*if (!count($this->db->ar_orderby) && !empty($this->_order_by)) {
            $this->db->order_by($this->_order_by,$this->_sort_by);
        }*/
        if (!empty($this->_order_by)){
            $this->db->order_by($this->_order_by,$this->_sort_by);
        }
        return $this->db->get($this->_table)->$method();
    }

     public function get_by($where, $single = false,$limit = false,$offset = false)
    {
        $this->db->where($where);
        
        if($limit)
        $this->db->limit($limit,$offset);
        
        return $this->get(null, $single);
    }

    public function get_join($table_join,$condition,$where, $single = false,$limit = false,$offset = false)
    {
        if(!empty($table_join) && !empty($condition)){
            $this->db->join($table_join,$condition);
        }
        if(!empty($where))
            $this->db->where($where);
        
        if($limit)
            $this->db->limit($limit,$offset);
        
        return $this->get(null, $single);
    }

    
    /**
    * Get PK
    * 
    * @param 
    * @return PK
    */
    function getPK(){
        return $this->_pk;
    }
    
    /**
    * Get Number of Record
    * 
    * @param 
    * @return Number of Record
    */
    function getCount(){
        $count = $this->db->get($this->_table)
                          ->num_rows();
        return $count;
        
    }

    /**
    * Get Table Name
    * 
    * @param 
    * @return Table Name
    */
    function getTableName(){
        return $this->_table;
    }
}
?>