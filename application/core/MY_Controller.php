<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if( ! ini_get('date.timezone') )
{
   date_default_timezone_set("Asia/Bangkok");
}
class MY_Controller extends MX_Controller {

	private $_model;
	
	function __construct(){
		parent::__construct();
   		if (!$this->my_usession->logged_in){         
		   redirect('auth','refresh');
		}
	}

	/**
	* Set Model
	* 
	* @param $t
	* @return 
	*/
	public function setModel($m){
            $this->_model = $m;
            $this->load->model($this->_model);
	}

	public function getModel(){
            return $this->_model;
            
	}

	/**
	* First Controller
	* 
	* @param 
	* @return 
	*/
	public function index(){
		$this->load->view("layout");
	}

	/**
	* Show List Page
	* 
	* @param 
	* @return 
	*/
	public function page_list(){

        $this->load->view("list");
    }

    /**
	* Get All Data
	* 
	* @param 
	* @return 
	*/
    public function get_list(){
    	$model = $this->_model;
    	$data = $this->$model->get();
    	$this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
	* Show Form Page
	* 
	* @param 
	* @return 
	*/
     public function create(){
        $this->load->view("form");
    }

    /**
	* Insert or Update Datas
	* 
	* @param array data
	* @return 
	*/
    public function save($arr,$id){
        
    	$model = $this->_model;
        /*$data = file_get_contents("php://input");

        $objData = json_decode($data);

        $id = isset($objData->id) ? $objData->id : "";
       
        $arr['name'] = $objData->name;*/
        
        if(empty($id))
            return $this->$model->save($arr);
        else
            return $this->$model->save($arr,$id);

    }

    /**
	* Delete Data
	* 
	* @param  $id
	* @return 
	*/
    public function delete(){
    	$model = $this->_model;
        $id = $this->input->get('id');
        $this->$model->delete($id);

    }

    /**
	* Get Data By ID
	* 
	* @param $id
	* @return 
	*/
    public function view(){
    	$model = $this->_model;
        $id = $this->input->get('id');
        echo '['.json_encode($this->$model->get($id,true)).']';

    }

    public function getJsonData(){
	
    	$model = $this->_model;
    	$data = $this->$model->get();
    	
		echo json_encode($data);
	}

}
class Backend_Controller extends MY_Controller {
	function __construct(){
        parent::__construct(); 
        $this->isLogin();
		
	}
	
	/**
	* Authen Login
	* 
	* @param 
	* @return bool
	*/
	protected function isLogin(){
		if (!$this->my_usession->logged_in){         
		   redirect('auth','refresh');
		}
	}
}
class Frontend_Controller extends MY_Controller {
	function __construct(){
            parent::__construct();
            $this->session->set_flashdata('url_back',$this->uri->segment(1)); 
            $this->isLogin();
	}
        /**
	* Authen Login
	* 
	* @param 
	* @return bool
	*/
	protected function isLogin(){
            if (!$this->my_usession->logged_in_user){         
               redirect('login','refresh');
            }
	}
}


/* End of file MY_Controller.php */
/* Location: ./application/controllers/my_controller.php */
?>