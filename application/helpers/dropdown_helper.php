<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 if (!function_exists('listData')) {
    function listData($table,$name,$value,$orderBy='ASC') {
        $items = array();
        $CI = & get_instance();
        if($orderBy) {
          $CI->db->order_by($value,$orderBy);
        }
        $query = $CI->db->select("$name,$value")->from($table)->get();
        if ($query->num_rows() > 0) {
           $combo = '';
          foreach($query->result() as $data) {
            $list = explode(',',$value);
            $tmp_value = "";
            for($i=0; $i<count($list); $i++){
              $tmp_value .= $data->$list[$i].' ';
            }
           if(!empty($data->$name))
              $items[$data->$name] = $tmp_value;
          }
          $query->free_result();
          
        }
        return $items;
    }
  }


  if (!function_exists('listDataJoin')) {
    function listDataJoin($table,$name,$value,$orderBy='ASC',$table_join,$condition,$arrwhere) {
        $items = array();
        $CI = & get_instance();
        if($orderBy) {
          $CI->db->order_by($value,$orderBy);
        }
        
          $query = $CI->db->select("$name,$value")->from($table)
                    ->join($table_join,$condition)
                    ->where($arrwhere)
                    ->get();
        
        if ($query->num_rows() > 0) {
           $combo = '';
          foreach($query->result() as $data) {
            $list = explode(',',$value);
            $tmp_value = "";
            for($i=0; $i<count($list); $i++){
              $tmp_value .= $data->$list[$i].' ';
            }
            
           if(!empty($data->$name))
              $items[$data->$name] = $tmp_value;
          }
          $query->free_result();
          
        }
        return $items;
    }
  }

  if (!function_exists('listDataSql')) {
    function listDataSql($sql,$name,$value) {
        $items = array();
        $CI = & get_instance();
        

        
          $query = $CI->db->query($sql);
        
        if ($query->num_rows() > 0) {
           $combo = '';
          foreach($query->result() as $data) {
            $list = explode(',',$value);
            $tmp_value = "";
            for($i=0; $i<count($list); $i++){
              $tmp_value .= $data->$list[$i].' ';
            }
           if(!empty($data->$name))
              $items[$data->$name] = $tmp_value;
          }
          $query->free_result();
          
        }
        return $items;
    }
  }

  

  if (!function_exists('getDropdown')) {
    function getDropdown($listdata,$name = '', $selected = '', $det = '') {
          $obj = & get_instance();
          
            $combo = '';
            if ($name != '') {
                $combo .= '<select name="' . $name . '" id="' . $name . '" ' . $det . ' >';
                $combo .= '<option value="">-- กรุณาเลือก--</option>';
                if(!empty($listdata)){
                  foreach($listdata as $key=>$value){
                      $vt = trim($value);
                      if(!empty($vt)){
                          if(is_array($selected)){

                            foreach($selected as $val){
                              if($val == $key){
                                $sel = ' selected '; break; 
                              }else
                                $sel = '';
                            }
                          }else{
                            $sel = ($selected == $key) ? ' selected ' : '';
                          }
                         
                         $combo .= '<option value="' . $key . '"' . $sel . '>' . $value . '</option>';
                      }
                         
                  }
                }
                $combo .= '</select>';
            }
            return $combo;
          
    }
  }

  if (!function_exists('getRadio')) {
    function getRadio($listdata,$name = '', $selected = '', $det = '') {
          $obj = & get_instance();
          
            $radio = '';
            if ($name != '') {

                if(!empty($listdata)){

                  foreach($listdata as $key=>$value){
                      $vt = trim($value);
                      if(!empty($vt)){
                         $sel = ($selected == $key) ? ' checked ' : '';
                         //$radio .= '<option value="' . $key . '"' . $sel . '>' . $value . '</option>';
                         $radio .= '<input type="radio" name="'.$name.'" value="'.$key.'" ' . $sel . '>&nbsp;&nbsp;&nbsp;'. $value.'<br>';
                      }            
                  }

                }
            }
            return $radio;
          
    }
  }

  if (!function_exists('getCheckbox')) {
    function getCheckbox($listdata,$name = '', $selected = '', $det = '') {
          $obj = & get_instance();
          
            $radio = '';
            if ($name != '') {

                if(!empty($listdata)){

                  foreach($listdata as $key=>$value){
                      $vt = trim($value);
                      if(!empty($vt)){
                        if(is_array($selected)){

                            foreach($selected as $val){

                              if($val == $key){

                                $sel = ' checked ="checked"'; break; 
                              }else
                                $sel = '';
                            }
                          }else{
                            $sel = ($selected == $key) ? ' checked ="checked"' : '';
                          }
                         //$radio .= '<option value="' . $key . '"' . $sel . '>' . $value . '</option>';
                         $radio .= '<input type="checkbox" name="'.$name.'[]" value="'.$key.'" ' . $sel . '>&nbsp;&nbsp;&nbsp;'. $value.'<br>';
                      }            
                  }

                }
            }
            return $radio;
          
    }
  }

  
/* End of file dropdwon_helper.php */
/* Location: ./application/helper/dropdown_helper.php */