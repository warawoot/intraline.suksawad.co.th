<?php
	if (!defined('BASEPATH'))
    	exit('No direct script access allowed');
		
	/************** General DDL *****************/
	if (!function_exists('hour_ddl')) {
		function hour_ddl($name = '', $selected = '', $det = '') {
			$obj = & get_instance();
			$combo = '';
			if ($name != '') {
				$combo .= '<select name="' . $name . '" id="' . $name . '" ' . $det . ' >';
				
				for ($i=0; $i<=23; $i++) {
					$sel = (intval($selected) == intval($i)) ? ' selected ' : '';
					$val = (strlen($i) == 1) ? '0'.$i : $i;
					$combo .= '<option value="' . $i . '"' . $sel . '>' . $val . '</option>';
				}
				$combo .= '</select>';
			}
			return $combo;
		}	
	}
	
	if (!function_exists('minute_ddl')) {	
		function minute_ddl($name = '', $selected = '', $det = '') {
			$obj = & get_instance();
			$combo = '';
			if ($name != '') {
				$combo .= '<select name="' . $name . '" id="' . $name . '" ' . $det . ' >';
				
				for ($i=0; $i<=45; $i=$i+15) {
					$sel = (intval($selected) == intval($i)) ? ' selected ' : '';
					$val = (strlen($i) == 1) ? '0'.$i : $i;
					$combo .= '<option value="' . $i . '"' . $sel . '>' . $val . '</option>';
				}
				$combo .= '</select>';
			}
			return $combo;
		}	
	}
	
	if (!function_exists('branch_ddl')) {
		function branch_ddl($name = '', $selected = '', $det = '') {
			$obj = & get_instance();
			$combo = '';
			if ($name != '') {
				$combo .= '<select name="' . $name . '" id="' . $name . '" ' . $det . ' >';
				$combo .= '<option value="">-- Please Select --</option>';
				$dept = 0;
				$ida = array();
				getChildBranch('0',$ida,$dept);
				if(count($ida) > 0){
					for($i=0; $i<count($ida); $i++){
					
						$list = explode(' ',$ida[$i]);
						$r = $obj->db->get_where('tblbranch',array('BranchID'=>$list[0]));
						//if($r->row()->BranchID != '-1'){
							$t = '';
							for($j=1; $j<=$list[1];$j++){
									$t.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
							$sel = ($selected == $r->row()->BranchID) ? ' selected ' : '';
							$combo .= '<option value="' . $r->row()->BranchID . '"' . $sel . '>' . $t.' '.htmlspecialchars($r->row()->BranchName) . '</option>';
						//}     
					}
				}

				$combo .= '</select>';
			}
			return $combo;
		}
	}
	
	function getChildBranch($num,&$ida,&$dept) {
        $obj = & get_instance();
        $r = $obj->db->get_where('tblbranch',array('ParentBranch'=>$num));

        if ($r!=NULL) {
            foreach ($r->result() as $row) {
                
                if(count($ida) > 0){
                    if($row->ParentBranch == 0)
                        $dept = 0;
                    else{
                        for($i=0; $i<count($ida); $i++){
                            $list = explode(' ',$ida[$i]);
                            if($list[0] == $row->ParentBranch){
                                $dept = $list[1]+1;
                                break;
                            }
                        }
                    }
                }
                $ida[] = $row->BranchID.' '.$dept;

                getChildBranch($row->BranchID,$ida,$dept);
            }
        } else {
            if($dept>0)
                $dept--;
        }
    }

    if (!function_exists('getDropdownTree')) {
	    function getDropdownTree($level = 0 , $prefix = '' , $assignID=0 ,$upperOrgID='') {
			$obj = & get_instance();
			
			$rows = $obj->db
	 			->select('t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t1.upperOrgID')
				 
				->where('t1.upperOrgID', $level)
				->where('t1.assignID', $assignID)
				 
				->order_by('orgID','asc')
				->get('tbl_org_chart as t1')
	 			->result();
			 ;
			$tree = '';
			if (count($rows) > 0) {
	 			$i = 1;
	  			foreach ($rows as $key  =>$row) {//<i class="fa fa-caret-right"></i>
	  				 	if($upperOrgID == $row->orgID){ 
							$selected =  "selected='selected'" ;
						}else{
							$selected = "";	
						}
						// Append subcategories
						if($upperOrgID == ''){
	 						$tree  .=  "<option value='".$row->orgID."' >";
						}else{
							$tree  .=  "<option value='".$row->orgID."' ".$selected.">";	
						}
						$tree  .= $prefix .$row->orgName;
						$tree  .=  "</option>";
	 						 
	 					$tree .= getDropdownTree($row->orgID, $prefix . '--',$row->assignID , $upperOrgID);
	  			}
	 		}
			return $tree;
		}
	}
    
    
  
	
	function your_transform_func($value){
		return date('d/m/Y', strtotime($value));
	}
   
   	function str_replaces($string)
	{
		return  str_replace("\'", "&nbsp;", $string) ;
	}
	
	function str_replaces_fpdf($string)
	{
		return  str_replace("\'", " ", $string) ;
	}
	
	function conv($string)
	{
		return iconv('UTF-8', 'TIS-620', str_replaces_fpdf($string));
	}
 
   
   if (!function_exists('contractType_ddl')) {
		function contractType_ddl($name = '', $selected = '', $det = '') {
			$obj = & get_instance();
            $combo = '';
            if ($name != '') {
                $combo .= '<select name="' . $name . '" id="' . $name . '" ' . $det . ' >';
                $combo .= '<option value="">-- Please Select --</option>';
               
               $r = $obj->db->get('tbl_contract_type');
			   $sel = ($selected == '0') ? ' selected ' : '';
			    
                foreach($r->result() as $row){
                        $sel = ($selected == $row->contracttypeID) ? ' selected ' : '';
                        $combo .= '<option value="' . $row->contracttypeID . '"' . $sel . '>' . $row->contracttypeType . '</option>';
                }
                $combo .= '</select>';
            }
            return $combo;
		}
	}
    
	 if (!function_exists('DateThaiHelper')) {
		 function DateThaiHelper($strDate)
		{
			$strYear = date("Y",strtotime($strDate))+543;
			$strMonth= date("n",strtotime($strDate));
			$strDay= date("j",strtotime($strDate));
					
			$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
			$strMonthThai=$strMonthCut[$strMonth];
			return "$strDay $strMonthThai $strYear";
		}
	}
/* End of file ddl_helper.php */
/* Location: ./application/helpers/ddl_helper.php */