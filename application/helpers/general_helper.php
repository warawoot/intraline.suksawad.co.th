<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
    /* 
        Function     : toCEDate
        Description  : convert d/m/Y format to Y-m-d
    */
    if (!function_exists('toCEDate')) {

        function toCEDate($dateStr) {
            if(!empty($dateStr)){
                $date = explode("/", $dateStr);
                $datetime = new DateTime();
                $datetime->setDate($date[2], $date[1], $date[0]);
                return $datetime->format('Y-m-d');
            }else
                return NULL;
        }
    }

    /* 
       Function     : toDBDate
       Description  : convert d/m/Y to Y-m-d include culture
    */
    if (!function_exists('toDBDate')) {

       function toDBDate($dateStr, $culture) {

           if(!empty($dateStr)){
               $date        = explode("/", $dateStr);
               $dayStr      = $date[0];
               $monthStr    = $date[1];
               $yearStr     = $date[2];
               if($culture  = "TH-TH" && $yearStr != "") {
                   $yearStr -= 543;
               }
               $datetime    = new DateTime();
               $datetime->setDate( $yearStr,$monthStr,$dayStr);
               return $datetime->format('Y-m-d');
           }else
               return NULL;
       }
    }

    /* 
       Function     : toBEDate
       Description  : convert Y-m-d format to d/m/Y
     */
   if (!function_exists('toBEDate')) {

       function toBEDate($dateStr) {

           if(!empty($dateStr)){
               $date = explode("-", $dateStr);
               $datetime = new DateTime();
               $datetime->setDate($date[0], $date[1], $date[2]);
               return $datetime->format('d/m/Y');
           }else
               return NULL;

       }

   }

   /* 
       Function     : toBEDateThai
       Description  : convert Y-m-d format to d/m/Y
     */
   if (!function_exists('toBEDateThai')) {

       function toBEDateThai($dateStr) {

           if(!empty($dateStr)){
               $date = explode("-", $dateStr);
               $datetime = new DateTime();
               $datetime->setDate($date[0]+543, $date[1], $date[2]);
               return $datetime->format('d/m/Y');
           }else
               return NULL;

       }

   }

   /* 
       Function     : toBETime
       Description  : convert Y-m-d H:i:s format to H:i
     */
   if (!function_exists('toBETime')) {

       function toBETime($dateStr) {
          if(!empty($dateStr)){
            return $newDate = date("H:i", strtotime($dateStr));
          }else{
            return NULL;
          }

       }

   }

   /* 
      Function     : toFullDate
      Description  : convert Y-m-d format to จันทร์ d มกราคม 2556
         w = Numeric representation of the day of the week :0 => for Sunday through 6=> Saturday
         n = Numeric representation of a month, without leading zeros:1 through 12
    */
  if (!function_exists('toFullDate')) {
  
        function toFullDate($dateStr,$language='th',$m='') {       
              $th_day = array( 'อาทิตย์','จันทร์', 'อังคาร', 'พุธ', 'พฤหัส', 'ศุกร์', 'เสาร์');
              $th_month = array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
              $th_month_alias = array('','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
              $en_day = array( 'Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
              $en_month = array('','January','Febuary','March','April','May','June','July','August','September','October','November','December');
                if(!empty($dateStr)){           
                    $date = explode("-", $dateStr);
                   
                    $day = date("w", strtotime($dateStr));
                    $month = intval($date[1]);
                    $obj = & get_instance();
                    if($language == 'th'){
                      if(empty($m))
                        return intval($date[2]).' '.$th_month_alias[$month].' '.($date[0]+543);
                      else
                        return intval($date[2]).' '.$th_month[$month].' '.($date[0]+543);
                    }
                    else if($language == 'en')
                            return intval($date[2]).' '.$en_month[$month].' '.($date[0]);
                }else
                    return NULL;
                
       }
  
  }

  if (!function_exists('toFullDateContract')) {
  
        function toFullDateContract($dateStr,$language='th') {       
              $th_day = array( 'อาทิตย์','จันทร์', 'อังคาร', 'พุธ', 'พฤหัส', 'ศุกร์', 'เสาร์');
              $th_month = array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
              $th_month_alias = array('','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
              $en_day = array( 'Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
              $en_month = array('','January','Febuary','March','April','May','June','July','August','September','October','November','December');
                if(!empty($dateStr)){           
                    $date = explode("-", $dateStr);
                   
                    $day = date("w", strtotime($dateStr));
                    $month = intval($date[1]);
                    $obj = & get_instance();
                    if($language == 'th')
                            return intval($date[2]).' เดือน'.$th_month[$month].' พ.ศ.'.($date[0]+543);
                    else if($language == 'en')
                            return intval($date[2]).' '.$en_month[$month].' '.($date[0]);
                }else
                    return NULL;
                
       }
  
  }
   
 if (!function_exists('getStaffName')) {
        function getStaffName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_staff',array('ID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->staffPreName.' '.$r->row()->staffFName.' '.$r->row()->staffLName : "";
            }else
                return '';

        }
    }

    if (!function_exists('getStaffByID')) {
        function getStaffByID($id,$field) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_staff',array('ID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->$field : "";
            }else
                return '';

        }
    }

    if (!function_exists('getContractTypeName')) {
        function getContractTypeName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_contract_type',array('contracttypeID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->contracttypeType : "";
            }else
                return '';

        }
    }

    if (!function_exists('getContractTypeAge')) {
        function getContractTypeAge($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_contract_type',array('contracttypeID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->contracttypeYear : "";
            }else
                return '';

        }
    }

    
    if (!function_exists('getExpiredDateByWork')) {
        function getExpiredDateByWork($value,$CE=false){
            if(!empty($value)){
                $list = explode("-",$value);
                if(intval($list[1]) < 10 || (intval($list[1]) == 10 && intval($list[2]) == 1))
                    $year = $list[0]+60;
                else
                    $year = $list[0]+61;
                return (!$CE) ? '1 ตุลาคม'.' '.($year+543) : $year.'-10-01';
            }
           
        }
    }

     if (!function_exists('getWorkStatusByWork')) {
        function getWorkStatusByWork($id){
            if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_work s')
                            ->join('tbl_work_status w','s.workStatusID = w.workStatusID')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0)
                    return $r->row()->workStatusName;
                else
                    return '-';
            }
        }
    }

    if (!function_exists('getExpiredDateByContract')) {
        function getExpiredDateByContract($id){
            if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_contract s')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.contractID','DESC')
                            ->get();

                if($r->num_rows() > 0){
                  $arr_f = json_decode($r->row()->contractField);
                  foreach($arr_f as $row){
                    if($row->id == '23'){
                      $list = explode("/",$row->value);
                      return toFullDate($list[2].'-'.$list[1].'-'.$list[0],'th','full');
                      exit;
                    }
                  }
                  return '-';
                } else
                    return '-';
            }
        }
    }

    if(!function_exists('getAgePositionByWork')) {
        function getAgePositionByWork($id){
          if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_work s')
                            ->join('tbl_position p','s.positionID = p.positionID')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0){
                  list($year,$month,$day) = explode("-",$r->row()->workStartDate);
                  $year_diff  = date("Y") - $year;
                  $month_diff = date("m") - $month;
                  $day_diff   = date("d") - $day;
                  if ($day_diff < 0 || $month_diff < 0)
                    $year_diff--;
                  if($year_diff < 62){
                    $year_diff = ($year_diff > 0) ? $year_diff.' ปี ' : "";
                    $month_diff = ($month_diff > 0) ? $month_diff.' เดือน ' : "";
                    $day_diff = ($day_diff > 0) ? $day_diff.' วัน ' : "";
                    return $year_diff;
                  }
                }else
                    return '';
            }
        }
      }

      if(!function_exists('getSegByWork')) {
        function getSegByWork($id,$name=true){
          if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_work s')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0)
                  return $r->row()->segID;
                else
                  return '-';
            }
        }
      }

    if(!function_exists('getPositionByWork')) {
        function getPositionByWork($id,$name=true){
          if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_work s')
                            ->join('tbl_position p','s.positionID = p.positionID')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0)
                  if($name)
                    return $r->row()->positionName;
                  else
                    return $r->row()->positionID;
                else
                    return '-';
            }
        }
      }

      if(!function_exists('getStaffTypeByWork')) {
        function getStaffTypeByWork($id,$name=true){
          if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_work s')
                            ->join('tbl_staff_type t','s.staffTypeID = t.typeID')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0)
                  if($name)
                    return $r->row()->typeName;
                  else
                    return $r->row()->typeID;
                else
                    return '-';
            }
        }
      }

      if(!function_exists('getRankByWork')) {
        function getRankByWork($id,$name = true){
          if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_work s')
                            ->join('tbl_rank r','s.rankID = r.rankID')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0)
                    if($name)
                    return $r->row()->rankName;
                  else
                    return $r->row()->rankID;
                else
                    return '-';
            }
        }
      }

      if(!function_exists('getOrgByID')) { // แผนก
        function getOrgByID($orgID){
          if(!empty($orgID)){
            $obj = & get_instance();
            $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
            $r = $obj->db->query($sql);
            $assignID = ($r->num_rows() > 0) ? $r->row()->assignID : "1";
            $r = $obj->db->from('tbl_org_chart o')
                        ->where(array('o.assignID'=>$assignID, 'o.orgID'=>trim($orgID)))
                        ->get();
            if($r->num_rows() > 0){
                return $r->row()->orgName;
            }else
                return '-';

            /*


            */
            }
            else {
                    return 'N/A';                
            }
        }
      }

      if(!function_exists('getOrgByWork')) { // แผนก
        function getOrgByWork($id){
          if(!empty($id)){
                $obj = & get_instance();

               
                $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
               $r = $obj->db->query($sql);
               $assignID = ($r->num_rows() > 0) ? $r->row()->assignID : "1";

                $r = $obj->db->from('tbl_staff_work s')
                            ->join('tbl_org_chart o','s.orgID = o.orgID and o.assignID ='.$assignID)
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0){
                  $query = 'SELECT * FROM tbl_org_chart c WHERE c.upperOrgID = '.$r->row()->orgID.' AND c.assignID='.$r->row()->assignID;
                  $r2 = $obj->db->query($query);
                  if($r2->num_rows() == 0){
                    return $r->row()->orgName;
                  }else
                    return '-';

                  
                }else
                    return '-';
            }
        }
      }

      if(!function_exists('getOrg2ByWork')) { // กอง
        function getOrg2ByWork($id){
          if(!empty($id)){
                $obj = & get_instance();

               
                $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
               $r = $obj->db->query($sql);
               $assignID = ($r->num_rows() > 0) ? $r->row()->assignID : "1";

                $r = $obj->db->from('tbl_staff_work s')
                            ->join('tbl_org_chart o','s.orgID = o.orgID and o.assignID ='.$assignID)
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0){
                    if($r->row()->upperOrgID != '0' && $r->row()->upperOrgID != '1'){
                      $query = 'SELECT * FROM tbl_org_chart c WHERE c.upperOrgID = '.$r->row()->orgID.' AND c.assignID='.$r->row()->assignID;
                      $r2 = $obj->db->query($query);
                      if($r2->num_rows() > 0){
                        return $r->row()->orgName;
                      }else{
                        $query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$r->row()->upperOrgID.' AND c.assignID='.$r->row()->assignID;
                        $r1 = $obj->db->query($query);
                        if($r1->num_rows() > 0){
                          return $r1->row()->orgName;
                        }else
                          return '-';
                      }
                      
                    }else
                          return '-';
                }else
                    return '-';
            }
        }
      }

      if(!function_exists('getOrg1ByWork')) { // ฝ่าย
        function getOrg1ByWork($id){
          if(!empty($id)){
                $obj = & get_instance();

               
               $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
               $r = $obj->db->query($sql);
               $assignID = ($r->num_rows() > 0) ? $r->row()->assignID : "1";

                $r = $obj->db->from('tbl_staff_work s')
                            ->join('tbl_org_chart o','s.orgID = o.orgID and o.assignID ='.$assignID)
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','DESC')
                            ->get();

                if($r->num_rows() > 0){
                  if($r->row()->upperOrgID == '0' || $r->row()->upperOrgID == '1')
                    return $r->row()->orgName;
                  else{
                    $query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$r->row()->upperOrgID.' AND c.assignID='.$r->row()->assignID;
                    $r2 = $obj->db->query($query);
                    if($r2->num_rows() > 0){
                      if($r2->row()->upperOrgID == '0' || $r2->row()->upperOrgID == '1')
                        return $r2->row()->orgName;
                      else{
                        $query = 'SELECT * FROM tbl_org_chart c WHERE c.orgID = '.$r2->row()->upperOrgID.' AND c.assignID='.$r2->row()->assignID;
                        $r3 = $obj->db->query($query);
                        if($r3->num_rows() > 0)
                          return $r3->row()->orgName;
                      }
                      
                    }
                  }
                }else
                    return '-';
            }
        }
      }

      if(!function_exists('getWorkDateByWork')) { // ฝ่าย
        function getWorkDateByWork($id){
          if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_work s')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','ASC')
                            ->get();

                if($r->num_rows() > 0)
                    return $r->row()->workStartDate;
                else
                    return '-';
            }
        }
      }

      if(!function_exists('getWorkAgeByWork')) { // ฝ่าย
        function getWorkAgeByWork($id,$syear=false){
          if(!empty($id)){
                $obj = & get_instance();

               
                $r = $obj->db->from('tbl_staff_work s')
                            ->where(array('s.staffID'=>trim($id)))
                            ->order_by('s.workStartDate','ASC')
                            ->get();

                if($r->num_rows() > 0){

                  
                  list($year,$month,$day) = explode("-",$r->row()->workStartDate);
                  $year_diff  = date("Y") - $year;
                  $month_diff = date("m") - $month;
                  $day_diff   = date("d") - $day;
                  if ($day_diff < 0 || $month_diff < 0)
                    $year_diff--;
                  if($year_diff < 62){
                    $year_diff = ($year_diff > 0) ? $year_diff.' ปี ' : "";
                    $month_diff = ($month_diff > 0) ? $month_diff.' เดือน ' : "";
                    $day_diff = ($day_diff > 0) ? $day_diff.' วัน ' : "";
                    if(!$syear)
                      return $year_diff.$month_diff.$day_diff;
                    else
                      return $year_diff;
                  }

                }else
                    return '-';
            }
        }
      }

      if(!function_exists('getWorkAge')) { 
        function getWorkAge($value){
            if(!empty($value)){
                $obj = & get_instance();
                list($year,$month,$day) = explode("-",$value);
                $year_diff  = date("Y") - $year;
                $month_diff = date("m") - $month;
                $day_diff   = date("d") - $day;
                if ($day_diff < 0 || $month_diff < 0)
                  $year_diff--;
                if($year_diff < 150)
                  return $year_diff;
            }
           
        }
      }

    if (!function_exists('getPK')) {
        function getPK($table){
            if(!empty($table)){
                $obj = & get_instance();
                $fields =  $obj->db->field_data($table);
                foreach ($fields as $field)
                {
                  if($field->primary_key == 1){
                     return $field->name;
                     exit;
                  }
                } 
            }
           
        }
    }

    if (!function_exists('calAge')) {
        function calAge($value){
            if(!empty($value)){
                $obj = & get_instance();
                list($year,$month,$day) = explode("-",$value);
                $year_diff  = date("Y") - $year;
                $month_diff = date("m") - $month;
                $day_diff   = date("d") - $day;
                if ($day_diff < 0 || $month_diff < 0)
                  $year_diff--;
                if($year_diff < 150)
                  return $year_diff;
            }
           
        }
  }

  if(!function_exists('getAssignLasted')) { 
      function getAssignLasted(){      
          $obj = & get_instance();
    
          $sql = "select assignID from tbl_orgchart_assigndate order by assignDate DESC LIMIT 1";
          $r = $obj->db->query($sql);
          return ($r->num_rows() > 0) ? $r->row()->assignID : "1";  
      }
    }

    if (!function_exists('getWelfareTypeName')) {
        function getWelfareTypeName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_welfare_type',array('welTypeID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->welTypeName : "";
            }else
                return '';

        }
    }
    if (!function_exists('getFixName')) {
        function getFixName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $query = 'SELECT f.welFixName FROM tbl_welfare_type t 
                          JOIN tbl_welfare_fixed f ON t.welFixID = f.welFixID
                          WHERE t.welTypeID = ' . $id;

                $r = $obj->db->query($query);
                return ($r->num_rows() > 0) ? $r->row()->welFixName : "";
            }else
                return '';

        }
    }
    if (!function_exists('getStaffTypeName')) {
        function getStaffTypeName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_staff_type',array('typeID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->typeName : "";
            }else
                return '';

        }
    }

    if (!function_exists('getEduFactName')) {
        function getEduFactName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_edu_faculty',array('eduFactID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->eduFactName : "";
            }else
                return '';

        }
    }
     if (!function_exists('getEduDepartmentName')) {
        function getEduDepartmentName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_edu_department',array('eduDepartmentID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->eduDepartmentName : "";
            }else
                return '';

        }
    }
    if (!function_exists('getEduName')) {
        function getEduName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_education',array('eduID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->eduName : "";
            }else
                return '';

        }
    }
    if (!function_exists('getEduTypeName')) {
        function getEduTypeName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_edu_type',array('eduTypeID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->eduTypeName : "";
            }else
                return '';

        }
    }
    if (!function_exists('getEduPlaceName')) {
        function getEduPlaceName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_edu_place',array('eduPlaceID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->eduPlaceName : "";
            }else
                return '';

        }
    }
    if (!function_exists('getEduTypeNameByPlace')) {
        function getEduTypeNameByPlace($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->from('tbl_edu_place p')
                            ->join('tbl_edu_type t','p.eduTypeID=t.eduTypeID')
                            ->where(array('eduPlaceID'=>  trim($id)))
                            ->get();
                return ($r->num_rows() > 0) ? $r->row()->eduTypeName : "";
            }else
                return '';

        }
    }

    if (!function_exists('getFeeName')) {
        function getFeeName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_welfare_list_fee',array('lfID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->lfName : "";
            }else
                return '';

        }
    }

    if (!function_exists('getPatientTypeName')) {
        function getPatientTypeName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_patient_type',array('patTypeID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->patTypeName : "";
            }else
                return '';

        }
    }
    if (!function_exists('getHosTypeName')) {
        function getHosTypeName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_hospital_type',array('hosTypeID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->hosTypeName : "";
            }else
                return '';

        }
    }

    if (!function_exists('getHosName')) {
        function getHosName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_hospital',array('hosID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->hosName : "";
            }else
                return '';

        }
    }

    if (!function_exists('getPersonName')) {
        function getPersonName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_staff_person',array('personID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->personFName.' '.$r->row()->personLName : "";
            }else
                return '';

        }
    }

     if (!function_exists('image_thumb')) {
            
            function image_thumb($fullPath) {
                $obj = & get_instance();
                
                $config['source_image'] = $fullPath;  
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                //$config['height'] = 200;
                
                $obj->load->library('image_lib', $config);
                if(!$obj->image_lib->resize()) return $obj->image_lib->display_errors();
                $obj->image_lib->clear(); 
               
            }     
    }
    
    if (!function_exists('getUserName')) {
        function getUserName($id) {

            if(!empty($id)){
                $obj = & get_instance();
                $r = $obj->db->get_where('tbl_user',array('userID'=>  trim($id)));
                return ($r->num_rows() > 0) ? $r->row()->userName.' '.$r->row()->userName : "";
            }else
                return '';

        }
    }

    if (!function_exists('getEmployeeIn')) {
        function getEmployeeIn($org,$month,$year){
            
                $obj = & get_instance();
                $year-=543;
                $month = (strlen($month) == 2) ? $month : '0'.$month;
                $date1 = $year.'-'.$month; 

                $timestamp=strtotime($date1);
                $d=date('Y-m', $timestamp);                
                //$d = date_create_from_format('Y-m',$date1); 

                $d = new DateTime( $d.'-1' ); 
                //$last_day = date_format($d, 't');
                // find last day of month
                $last_day = $d->format( 'Y-m-t' );
                // find first day of month
                $first_day = $d->format( 'Y-m-1' );
              
               if(!empty($org)){
                  $sql = "SELECT  count(s.ID) as count_emp
                              FROM tbl_staff_work c 
                              JOIN tbl_staff s ON c.staffID = s.ID
                              WHERE c.workStartDate = (SELECT max(workStartDate) from tbl_staff_work WHERE staffID=c.staffID  AND workStatusID = '1' AND workStartDate between '$first_day' AND '$last_day') AND c.orgID = $org"; 
                  
                  $r = $obj->db->query($sql);
                 
                  return ($r->num_rows() > 0) ? $r->row()->count_emp : '0';
                }   

        }
    }

    if (!function_exists('getEmployeeOut')) {
        function getEmployeeOut($org,$month,$year){
            
               $obj = & get_instance();
               $year-=543;
                $month = (strlen($month) == 2) ? $month : '0'.$month;
                $date1 = $year.'-'.$month; 
                $timestamp=strtotime($date1);
                $d=date('Y-m', $timestamp);
                //$d = date_create_from_format('Y-m',$date1); 
               
                $d = new DateTime( $d.'-1' ); 
                //$last_day = date_format($d, 't');
                // find last day of month
                $last_day = $d->format( 'Y-m-t' );
                // find first day of month
                $first_day = $d->format( 'Y-m-1' );

               if(!empty($org)){
                
                  $sql = "SELECT  count(s.ID) as count_emp
                              FROM tbl_staff_work c 
                              JOIN tbl_staff s ON c.staffID = s.ID
                              WHERE c.workStartDate = (SELECT max(workStartDate) from tbl_staff_work WHERE staffID=c.staffID  AND workStatusID != '1' AND workStartDate between '$first_day' AND '$last_day') AND c.orgID = $org"; 
                  
                  $r = $obj->db->query($sql);
                  return ($r->num_rows() > 0) ? $r->row()->count_emp : '0';
                }   

        }
    }


    

/* End of file general_helper.php */
/* Location: ./application/helper/general_helper.php */