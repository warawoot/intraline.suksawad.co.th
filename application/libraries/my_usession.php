<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Usession{

    public $logged_in = FALSE;
    
    public function  __construct() {
        
        $this->is_logged_in();
   
    }

    public function is_logged_in()
    {
        $obj = & get_instance();
        $logged = $obj->session->userdata('login');
        $this->logged_in = ($logged) ? TRUE : FALSE;
    }
    
    
}
/* End of file MY_Usession.php */
/* Location: ./system/application/libraries/MY_Usession.php */