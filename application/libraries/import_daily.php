<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CI_Import_daily{

    
    public function  __construct() {
         
    }


   public function do_after_import(){

      $CI =& get_instance();
      //--------------
      $CI->load->model('import_daily_model'); 
      //--------------
      $r = $CI->import_daily_model->getDailyData();
      
      if($r->num_rows() > 0){
        foreach ($r->result() as $row) {
           
           $scan_min = explode(" ",$row->min_dated);
           $scan_max = explode(" ",$row->max_dated);

           if(!empty($scan_min[1]) && !empty($scan_max[1])){
               $time_in = explode(":",$scan_min[1]);
               $time_out = explode(":",$scan_max[1]);

               // morning //
               $flg = strtotime($scan_min[1])<strtotime($CI->config->item('lanch_start_hour').':'.$CI->config->item('lanch_start_minute').':'.'00');
               $flglate = strtotime($scan_min[1]) > strtotime($CI->config->item('start_hour').':'.$CI->config->item('start_minute').':'.'00');
               // afternoon //
               $flgaf = strtotime($scan_max[1]) > strtotime($CI->config->item('lanch_end_hour').':'.$CI->config->item('lanch_end_minute').':'.'00');
               $flgaflate = strtotime($scan_max[1]) < strtotime($CI->config->item('end_hour').':'.$CI->config->item('end_minute').':'.'00');

               $late_minute = 0;
               $af_late_minute = 0;
               if($flg && $flglate){
                      $late_minute = round(abs(strtotime($scan_min[1]) - strtotime($CI->config->item('start_hour').':'.$CI->config->item('start_minute').':'.'00'))/60,2);
                      
                     

                }

                if($flgaf && $flgaflate){
                  
                  $af_late_minute = round(abs(strtotime($CI->config->item('end_hour').':'.$CI->config->item('end_minute').':'.'00') - $scan_max[1])/60,2);
                }

                if(!empty($late_minute) || !empty($af_late_minute)){
                    $arr = array(
                        'staffID'  => $row->ID,
                        'dateDaily' => $scan_min[0],
                        'amtStart' => $late_minute,
                        'amtEnd' => $af_late_minute);
                    $CI->db->insert('tbl_staff_work_late',$arr);
                }
                  

           }
           
        }
     }   

   }
    
    
}
/* End of file Import_daily.php */
/* Location: ./system/application/libraries/Import_daily.php */