<?php
class Import_daily_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff_work_late';
            $this->_pk = 'amtID';
        }
        


        public function getDailyData(){
          //$startDate_s = date("Y-m").'-01';
          //$endDate_s = date("Y-m-d");
          $sql = "SELECT s.ID,s.staffFName,s.staffLName,s.staffIDCardOff,d.dailyID,min(d.dateDaily) as min_dated,max(d.dateDaily) as max_dated,d.staffID as staffIDD FROM tbl_staff_work_daily d right JOIN tbl_staff s ON s.staffIDCardOff = d.staffID ";

          if(!empty($startDate_s) && !empty($endDate_s)){
            $sql .= " WHERE DATE(d.dateDaily) BETWEEN '$startDate_s' AND '$endDate_s' ";
          }
         
          $sql.="group by  s.ID,YEAR(d.dateDaily),MONTH(d.dateDaily),DAY(d.dateDaily) ";
          
          $sql.="ORDER BY s.staffFName ASC,s.staffIDCardOff";
        
          return $this->db->query($sql);

     }
     
     
	
}
/* End of file import_daily_model.php */
/* Location: ./application/module/user/models/import_daily_model.php */