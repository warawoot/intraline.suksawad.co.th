<!DOCTYPE html>
<html lang="en-US"><!--<![endif]-->
<head>
    <!-- META TAGS -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Title -->
    <title>IntraLINE-HR : Suksawad Group</title>

    <!-- Define a view port to mobile devices to use - telling the browser to assume that
    the page is as wide as the device (width=device-width)
     and setting the initial page zoom level to be 1 (initial-scale=1.0) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <!-- favicon -->
    <!--link rel="shortcut icon" href="assets/images/icon/favicon.ico"-->

    <!-- Google Web Font -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,100,500,600,700,800,900,300,200" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- bootstrap Style Sheet (caution ! - Do not edit this stylesheet) -->
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Include the site main stylesheet -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/main.css" type="text/css" media="all">
    <!-- Include the site responsive  stylesheet -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/custom-responsive.css" type="text/css" media="all">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/MegaNavbar.css"/>
    <!--link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/animation/animation.css" title="inverse"-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/skins/navbar-custom.css" title="inverse">
    <!--link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/flattable.css" title="inverse"-->

    <!-- JQUERY -->
    <script type="text/javascript" id="quick-js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!--script type="text/javascript" src="<?=base_url()?>scripts/jqueryui/jquery-ui-1.11.4.custom.js"></script-->
    <!--
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css">
    -->

    <script type="text/javascript" src="<?=base_url()?>assets/jqueryUI/jquery-ui-1.11.4.custom.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>assets/jqueryUI/jquery-ui-1.11.4.custom.css" />
     

    <!--script type='text/javascript' src='<?=base_url()?>assets/js/jquery-1.11.1.min.js'></script-->
    <script type='text/javascript' src='<?=base_url()?>assets/js/bootstrap.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery.isotope.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery.appear.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery.ui.core.min.js'></script>
    <!--script type='text/javascript' src='<?=base_url()?>assets/js/jquery.ui.datepicker.min.js'></script-->
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery.validate.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery.form.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery.autosize.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery.velocity.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/respond.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery-migrate-1.2.1.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/custom.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/plugins/bootstrap-dropdown-hover/jquery.bootstrap-dropdown-hover.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/angular.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/angular-resource.min.js'></script> 
    <script type='text/javascript' src='<?=base_url()?>assets/js/angular-route.min.js'></script>

    <!--
    <script src="<?=base_url()?>assets/js/dhtmlxGrid/dhtmlxcommon.js"></script>
    <script src="<?=base_url()?>assets/js/dhtmlxGrid/dhtmlxgrid.js"></script>
    <script src="<?=base_url()?>assets/js/dhtmlxGrid/dhtmlxgridcell.js"></script>
    <script src="<?=base_url()?>assets/js/dhtmlxGrid/dhtmlxgrid_excell_cntr.js"></script>
    <script src="<?=base_url()?>assets/js/dhtmlxGrid/dhtmlxtreegrid.js"></script>
    <script src="<?=base_url()?>assets/js/dhtmlxGrid/dhtmlxgrid_drag.js"></script>
    <link href="<?=base_url()?>assets/css/dhtmlxGrid/dhtmlxgrid.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/css/dhtmlxGrid/dhtmlxgrid_dhx_skyblue.css" rel="stylesheet" />
    -->
    
    <!--dhtmlxDataView-->
    <!--
    <script src="<?=base_url()?>assets/js/dhtmlxDataView/dhtmlxdataview.js"></script>
    <link href="<?=base_url()?>assets/css/dhtmlxGrid/dhtmlxdataview.css" rel="stylesheet" />
    -->
</head>
<body>
    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="<php echo 'http://www.google.com/chromeframe/?redirect=true'; ?>">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <style>
        .show-grid [class^=col-] {padding-top: 10px;padding-bottom: 10px;background-color: transparent;text-align:center;}
        .show-grid { clear: both;}
        .show-grid:not(:first-child) { border-top: 1px solid #4c6d7c;}
        .show-grid [class^=col-]:not(:last-child) { border-right: 1px solid #4c6d7c;}
        .iconmenu a img {margin-bottom:10px;}
        .thumbnail {
        position: relative;
        width: 40px;
        height: 40px;
        overflow: hidden;
        }
        .thumbnail img {
        position: absolute;
        left: 50%;
        top: 50%;
        height: 100%;
        width: auto;
        -webkit-transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
                transform: translate(-50%,-50%);
        }
        .thumbnail img.portrait {
        width: 100%;
        height: auto;
        }        
    </style>

    <header id="header">
        <div class="header-top clearfix text-center">
            <a href="<?php echo site_url(); ?>">            
            <img src="<?=base_url()?>assets/images/logo.png" alt="Company Logo" style="height:100px">
            </a>
        </div>

        <div class="container">
        <!-- Main Navigation -->
        <nav class="navbar navbar-blue-dark no-border-radius xs-height75 navbar-static-top" id="main_navbar" role="navigation">
        	<div class="container-fluid">
                <?php echo modules::run("menu");?>
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown ">                            
                            <div class="thumbnail" style="float:left">
                            <?php
                                $filename = site_url().'assets/staff/'.$this->session->userdata('userID').'.jpg';
                                $file_headers = @get_headers($filename);
                                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                    echo '<img src="'.base_url().'./assets/staff/default-user.png" class="portrait">';   
                                } else {
                                    echo '<img src="'.$filename.' " class="portrait">';
                                }
                            ?>
                            </div>
                            <b class="caret"></b>
                        <ul class="dropdown-menu extended logout">
                            <li><a href="#"><i class=" fa fa-suitcase"></i> <?=$this->session->userdata('fullName')?></a></li>
                            <li><a href="<?php echo site_url();?>logout"><i class="fa fa-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                </ul>

        		</div>
        	</div>
        </nav>
        
        <div id="responsive-menu-container"></div>
        </div>
    </header>

<div class="page-top clearfix">
    <!--page main heading-->
    <div class="container-fluid">
		<?php echo $contents; ?>               
    </div>
</div>



<!--page footer-->
<section>
<div class="footer-bottom animated fadeInDown clearfix ae-animation-fadeInDown">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p>© 2017 - Suksawad Group - All Rights Reserved - Powered by IntraLINE-HR</p>
            </div>
        </div>
	</div>
</div>
</section>

<a href="#top" id="scroll-top"></a>

<script>
	//Start navbar toggle fix top bottom
	$(document).on('click', '.toggle_fixing', function(e) {
		e.preventDefault();
		if ($('#main_navbar').hasClass('navbar-fixed-top')) {
			$('#main_navbar').toggleClass('navbar-fixed-bottom navbar-fixed-top');
			$(this).children('i').toggleClass('fa-chevron-down fa-chevron-up');
		} else {
			$('#main_navbar').toggleClass('navbar-fixed-bottom');
			$(this).children('i').toggleClass('fa-chevron-down fa-chevron-up');
			if ($('#main_navbar').parent('div').hasClass('container')) $('#main_navbar').children('div').addClass('container').removeClass('container-fluid');
			else if ($('#main_navbar').parent('div').hasClass('container-fluid')) $('#main_navbar').children('div').addClass('container-fluid').removeClass('container');
			FixMegaNavbar(navHeight);
		}
		if ($('#main_navbar').hasClass('navbar-fixed-top')) {$('body').css({'margin-top': $('#main_navbar').height()+'px', 'margin-bottom': ''});}
		else if ($('#main_navbar').hasClass('navbar-fixed-bottom')) {$('body').css({'margin-bottom': $('#main_navbar').height()+'px', 'margin-top': ''});}
	})
	//End navbar toggle fix top bottom

	//Start Fix MegaNavbar on scroll page
	var navHeight = $('#main_navbar').offset().top;
	FixMegaNavbar(navHeight);
	$(window).bind('scroll', function() {FixMegaNavbar(navHeight);});

	function FixMegaNavbar(navHeight) {
		if (!$('#main_navbar').hasClass('navbar-fixed-bottom')) {
			if ($(window).scrollTop() > navHeight) {
				$('#main_navbar').addClass('navbar-fixed-top')
				$('body').css({'margin-top': $('#main_navbar').height()+'px'});
				if ($('#main_navbar').parent('div').hasClass('container')) $('#main_navbar').children('div').addClass('container').removeClass('container-fluid');
				else if ($('#main_navbar').parent('div').hasClass('container-fluid')) $('#main_navbar').children('div').addClass('container-fluid').removeClass('container');
			}
			else {
				$('#main_navbar').removeClass('navbar-fixed-top');
				$('#main_navbar').children('div').addClass('container-fluid').removeClass('container');
				$('body').css({'margin-top': ''});
			}
		}
	}
	//End Fix MegaNavbar on scroll page

	//Next code used to prevent unexpected menu close when using some components (like accordion, tabs, forms, etc), please add the next JavaScript to your page
	$( window ).on('load', function() {
		$(document).on('click', '.navbar .dropdown-menu', function(e) {e.stopPropagation();});
	});
		
</script>

<script>
	//https://github.com/istvan-ujjmeszaros/bootstrap-dropdown-hover
	//$('[data-toggle="dropdown"]').bootstrapDropdownHover();
	//$.fn.bootstrapDropdownHover();
	$('.navbar [data-toggle="dropdown"]').bootstrapDropdownHover();
</script>   
<!-- Include the site custom stylesheet -->
<link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css" type="text/css" media="all">

<script src="<?=base_url()?>assets/js/jquery.roundimage.js"></script>

<script>
$(document).ready(function() {
    $('.img-round').roundImage();
});
</script>

</body>
</html>