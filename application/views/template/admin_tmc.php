<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
	<title>IntraLINE-HR : Suksawad Group</title>
	
	<!-- ######### CSS STYLES ######### -->
	<!--Core CSS -->
    <!--<link href="<?php echo base_url() ?>assets/css/bootstrap.min1.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/docs.min1.css" rel="stylesheet">-->
    <link href="<?php echo base_url() ?>assets/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/bootstrap/assets/css/docs.min.css" rel="stylesheet">
    
    <link href="<?php echo base_url() ?>assets/css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style-responsive.css" rel="stylesheet" />
    
    <link href="<?php echo base_url() ?>assets/css/table-style.css" rel="stylesheet">
    
    <link href="<?php echo base_url() ?>xcrud/plugins/bootstrap/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    
    <link href="<?php echo base_url() ?>assets/css/jquery.dataTables.css" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="<?php echo base_url() ?>assets/bootstrap/dist/css/bootstrap-theme-modify.css" rel="stylesheet">
	<!-- ######### SCRIPT ######### -->
	<!--Core js-->
	<script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
    
	<script src="<?php echo base_url() ?>assets/js/3.2.0.bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.dcjqaccordion.2.7.js" class="include" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.nicescroll.js"></script>
    
	<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.dataTables.min.js"></script>
	<!--Chart JS-->
	<script src="<?php echo base_url() ?>assets/js/chart-js/Chart.js"></script>
	<script src="<?php echo base_url() ?>assets/js/chartjs.init.js"></script>

	<!--common script init for all pages-->
	<script src="<?php echo base_url() ?>assets/js/scripts.js"></script>
      
 	
</head>
<body>	
    <section id="container">
        <!--header start-->
        <header class="header fixed-top clearfix">
        <!--logo start-->
        <div class="brand">
            <a href="#" class="logo">
                    <img src="<?php echo base_url() ?>assets/images/logo.png" alt="Company Logo" style="height:100px">
            </a>
            <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
            </div>
        </div>
        <!--logo end-->
        <!--top_menu--><!--top_menu end-->
        <div class="top-nav clearfix">
            <!--search & user info start-->
            <div class="col-sm-6 col-md-6 pull-left" style="margin-top:3px; margin-left:30px;">
                            <form id="attributeForm" class="form-horizontal" role="form" action="<?php echo site_url();?>doctor/doctor_form_search" method="post">
                                <div class="input-group">
                                    <input type="text" class="form-control"   name="search" id="search" placeholder="ค้นหาแพทย์" style=" border-radius: 0px;">
                                    <div class="input-group-btn">
                                        <button type="submit" id="check_license" class="btn btn-default" style="border-left: 0; border-radius: 0px;"/*onclick="modal_research()"*/>ค้นหา</button>
                                    </div>
                                </div>
                            </form>
            </div>
            <ul class="nav pull-right top-menu">
                <!-- user login dropdown start-->
                <li> 
                        	<div style="padding:0px;">
                                 <div class="img-circle " style="zoom:1.5; margin-top:3px; opacity:0.8;  filter:alpha(opacity=40); ">
                                 <?php if($this->session->userdata('roleID') == '3'){?>
                                     <a href="<?php echo site_url();?>hospital" title="หน้าแรก" class="text-muted">
                                     <i class="glyphicon glyphicon-home "></i></a> 
                                  <?php }else if($this->session->userdata('roleID') == '1'){?>
                                      <a href="<?php echo site_url();?>main/overview" title="หน้าแรก" class="text-muted">
                                     <i class="glyphicon glyphicon-home "></i></a> 
                                  <?php } ?>
                                  </div>
                             </div>  
                        </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        
						<?php 
						if ($this->session->userdata('roleID') == 3 ) {
							echo "<img  src=".site_url()."assets/images/hospital_logo/".$this->session->userdata('hospitalLogo').">";
						}else if ($this->session->userdata('roleID') == 2 ) {
							echo "<img  src=".site_url()."assets/images/network_logo/".$this->session->userdata('networkLogo').">";
						}else if($this->session->userdata('roleID') == 1){
							echo "<img  src=".site_url()."assets/images/avatar2_small.jpg".">";
						}else{
							echo "<img  src=".site_url()."assets/images/avatar2_small.jpg".">";
						} ?>
                        <span class="username"><?php echo $this->session->userdata('userName'); ?></span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                        <!--<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>-->
                        <li><a href="<?php echo site_url();?>logout"><i class="fa fa-key"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <!--search & user info end-->
        </div>
        </header>
        <!--header end-->


        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse">
                    <!-- sidebar menu start-->            
                    <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <!--### 1 = Admin CME ###-->
                            <?php if($this->session->userdata('roleID') == '1' ){ //แพทย์สภา ?>
                            <!--/*<li>
                                    <a href="<?php echo site_url();?>main/overview" class="active" id="menu_home">
                                            <i class="fa fa-dashboard"></i>
                                            <span>ภาพรวม</span>
                                    </a>
                            </li>*/-->
                            <li>
                                    <a href="<?php echo site_url();?>group" id="menu_netword">
                                            <i class="fa fa-group"></i>
                                            <span>เครือข่ายโรงพยาบาล</span>
                                    </a>
                            </li> 
                            <!-- <li>
                                    <a href="<?php echo site_url();?>hospital" id="menu_netword">
                                            <i class="fa fa-h-square"></i>
                                            <span>โรงพยาบาล</span>
                                    </a>
                            </li> -->
                            <!--<li>
                                    <a href="<?php echo site_url();?>doctor/doctor_search" id="menu_netword">
                                            <i class="fa fa fa-search"></i>
                                            <span>ค้นหาแพทย์</span>
                                    </a>
                            </li>-->
                            <!-- <li><a href="<?php echo site_url();?>report_graduate" id="menu_result">
                                            <i class="fa fa-check"></i>
                                            <span>ดูผลประเมินแพทย์ผ่าน/ไม่ผ่าน</span>
                                    </a></li>-->
							<!--<li>
                                    <a href="<?php echo site_url();?>gen_user" id="menu_user">
                                            <i class="fa fa-key"></i>
                                            <span>สร้าง Username/Password</span>
                                    </a>
                            </li>
							<li>
                                    <a href="<?php echo site_url();?>certificate" id="menu_user">
                                            <i class="fa fa-trophy"></i>
                                            <span>พิมพ์ประกาศนียบัตร</span>
                                    </a>
                            </li> -->
                            <li class="sub-menu">
                                    <a href="javascript:;" id="menu_report">
                                            <i class="fa fa-book"></i>
                                            <span>รายงาน</span>
                                    </a>
                                    <ul class="sub">
                                            <!--<li><a href="<?php echo site_url();?>approve_quota">จำนวนที่อนุมัติโรงพยาบาลที่ได้รับการรับรอง</a></li>-->
                                            <li><a href="<?php echo site_url();?>report_quota"><i class="glyphicon glyphicon-th"></i> สรุปโรงพยาบาลที่ได้รับการรับรอง</a></li>
											<li><a href="<?php echo site_url();?>report_graduate"><i class="glyphicon glyphicon-th"></i> สรุปแพทย์ที่ผ่าน/ไม่ผ่าน</a></li>
                                            <li><a href="<?php echo site_url();?>report_eval"><i class="glyphicon glyphicon-th"></i> สรุปแพทย์ที่ส่งผ่านการประเมิน</a></li>
                                            <li><a href="<?php echo site_url();?>report_rate"><i class="glyphicon glyphicon-th"></i> รายงานค่าเฉลี่ยผลการประเมิน</a></li>
                                            <li><a href="<?php echo site_url();?>reportdoctor"><i class="glyphicon glyphicon-th"></i> รายงานสรุปข้อมูลแพทย์ที่เข้าโครงการแพทย์เพิ่มพูนทักษะ</a></li>
                                             <li><a href="<?php echo site_url();?>reportworking_doctor"><i class="glyphicon glyphicon-th"></i> รายงานสรุปผลการปฏิบัติงานของแพทย์</a></li>
                                            <!--<li><a href="<?php echo site_url();?>report_graduate">รายงานสรุปแพทย์ที่ผ่านการประเมินประจำปี</a></li>
                                            <li><a href="#">รายงานสรุปตามสถาบันที่จบ</a></li>
                                            <li><a href="<?php //echo site_url();?>ask_cer">รายการใบคำขอของแพทย์</a></li>-->
                                    </ul>
                            </li>
                            <li>
                                    <a href="javascript:;" id="menu_data">
                                            <i class="fa fa-list"></i>
                                            <span>ข้อมูลตั้งต้น</span>
                                    </a>
                                    <ul class="sub">
                                    	<li><a href="<?php echo site_url();?>physician">เพิ่มปีการศึกษา</a></li>
                                       <!-- <li><a href="<?php //echo site_url();?>future">อนุมัติปีการศึกษาอนาคต</a></li>
                                        <li><a href="<?php //echo site_url();?>approval">อนุมัติประเมิน</a></li>-->
                                        <li><a href="<?php echo site_url();?>manage">ผู้ใช้งาน</a></li>
                                        <li><a href="<?php echo site_url();?>network">เครือข่าย</a></li>
                                        <li><a href="<?php echo site_url();?>province">จังหวัด</a></li>
                                        <li><a href="<?php echo site_url();?>belong">สังกัด</a></li>
                                        <li><a href="<?php echo site_url();?>education">สถาบันที่จบ</a></li>		
                                        <li><a href="<?php echo site_url();?>hospitals">โรงพยาบาล</a></li>	
                                        						
                                    </ul>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>download/download_upload" id="menu_doc">
                                            <i class="fa fa-download"></i>
                                            <span>อัพโหลดเอกสาร</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>signature" id="menu_doc">
                                            <i class="fa fa-file"></i>
                                            <span>เพิ่มลายมือชื่อ</span>
                                    </a>
                            </li>
                            <!--<li>
                                    <a href="<?php echo site_url();?>download/download_upload" id="menu_doc">
                                            <i class="fa fa-download"></i>
                                            <span>เอกสารดาวน์โหลด</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="#" id="menu_user">
                                            <i class="fa fa-file"></i>
                                            <span>Log File</span>
                                    </a>
                            </li>-->
                            <li>
                                    <a href="<?php echo site_url();?>logout">
                                            <i class="fa fa-sign-out"></i>
                                            <span>Logout</span>
                                    </a>
                            </li>
                            
                            <!--### 2 = เครือข่าย  ###-->
                            <?php } else  if($this->session->userdata('roleID') == '2'){ // เครือข่าย ?>
                            <!--<li>
                                    <a href="<?php echo site_url();?>main/overview" class="active" id="menu_home">
                                            <i class="fa fa-dashboard"></i>
                                            <span>ภาพรวม</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>hospital/hospital_network" id="menu_netword">
                                            <i class="fa fa-h-square"></i>
                                            <span>โรงพยาบาล</span>
                                    </a>
                            </li>-->	
                            <li><!--<a href="<?php echo site_url();?>approve_quota/network_approve_quota" id="menu_hospital">-->
                                    <a href="<?php echo site_url();?>main/overview" id="menu_hospital">
                                            <i class="fa fa-clipboard"></i>
                                            <span>ตรวจสอบโรงพยาบาล</span>
                                    </a>
                            </li>
                            <!--<li>
                                    <a href="<?php echo site_url();?>doctor/doctor_search" id="menu_netword">
                                            <i class="fa fa fa-search"></i>
                                            <span>ค้นหาแพทย์</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>report_quota/network_report_quota" id="menu_hospital">
                                            <i class="fa fa-envelope"></i>
                                            <span>โควตาที่ได้รับอนุมัติ</span>
                                    </a>
                            </li>-->
                            <!-- <li>
                                    <a href="<?php echo site_url();?>report_graduate" id="menu_result">
                                            <i class="fa fa-bolt"></i>
                                            <span>ดูผลประเมินแพทย์ผ่าน/ไม่ผ่าน</span>
                                    </a>
                            </li> -->
                            <!--<li>
                                    <a href="<?php echo site_url();?>report_graduate/report_network" id="menu_result">
                                            <i class="fa fa-book"></i>
                                            <span>สรุปผลประเมินแพทย์</span>
                                    </a>
                            </li>-->
                            <li>
                                    <a href="<?php echo site_url();?>logout">
                                            <i class="fa fa-sign-out"></i>
                                            <span>Logout</span>
                                    </a>
                            </li>

                            <!--### 3 = โรงพยาบาล  ###-->
                            <?php } else if($this->session->userdata('roleID') == '3'){  ?>
<!--                            <li>
                                    <a href="<?php echo site_url();?>main/overview" class="active" id="menu_home">
                                            <i class="fa fa-dashboard"></i>
                                            <span>ภาพรวม</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>regist" id="menu_hospital">
                                            <i class="fa fa-check-square-o"></i>
                                            <span>ขออนุมัติโควต้า</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>send_dr" id="menu_doctorname">
                                            <i class="fa fa-stethoscope"></i>
                                            <span>ส่งรายชื่อแพทย์</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>resignation" id="menu_doctorname">
                                            <i class="fa fa-bullhorn"></i>
                                            <span>แจ้งลาออก / รับโอนแพทย์</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>questionnaire" id="menu_questionnaire">
                                            <i class="fa fa-gavel"></i>
                                            <span>ประเมินแพทย์</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>report_graduate/report_hospital" id="menu_result">
                                            <i class="fa fa-book"></i>
                                            <span>สรุปผลประเมินแพทย์</span>
                                    </a>
                            </li>
                            <li>
                                    <a href="<?php echo site_url();?>download" id="menu_doc">
                                            <i class="fa fa-download"></i>
                                            <span>เอกสารดาวน์โหลด</span>
                                    </a>
                            </li> 
                            <li>
                                    <a href="javascript:;" id="menu_data">
                                            <i class="fa fa-list"></i>
                                            <span>ข้อมูลตั้งต้น</span>
                                    </a>
                                    <ul class="sub">
                                            <li><a href="<?php echo site_url();?>#">แพทย์ผู้ดูแล</a></li>
                                            <li><a href="<?php echo site_url();?>#">แพทย์ผู้รับการฝึก</a></li>
                                    </ul>
                            </li> -->

                            
                            <?php } ?>				
                        </ul>
                    </div>        
            <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->


        <!--main content start-->
        <section id="main-content">
                <section class="wrapper">
                        <?php echo $contents; ?>
                </section>
        </section>
        <!--main content end-->

	  <style type="text/css">
			    @import url("<?php echo base_url() ?>assets/bootstrap/js/bootstrap-select.min.css");
			   
				.dropdown-toggle-menu-left { 
				  background: #cc181e;
				  color: #fff;
				}
				 
				.dropdown-menu {
					position: absolute;
					top: 100%;
					/*left: 70px;*/
					left: 0px;
					z-index: 1000;
					display: none;
					float: left;
					min-width: 160px;
					padding: 5px 0;
					margin: 2px 0 0;
					font-size: 14px;
					text-align: left;
					list-style: none;
					background-color: #fff;
					-webkit-background-clip: padding-box;
					background-clip: padding-box;
					border: 1px solid #ccc;
					border: 1px solid rgba(0, 0, 0, .15);
					border-radius: 4px;
					-webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
					box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
				}
				.yt-uix-button-content {
					position: absolute;
					/*top: -2px;
					left: 14px;*/
					top: 5px;
					left: 30px;
					z-index: 100;
					min-width: 15px;
					border-bottom: 1px solid #fff;
					border-left: 1px solid #fff;
					background: #cb4437;
					color: #fff;
					line-height: 15px;
					text-align: center;
					opacity: 1;
					border-radius: 2px;
				}
				.timeline {
				  list-style: none;
				  padding: 20px 0 20px;
				  position: relative;
				}
				.timeline:before {
				  top: 0;
				  bottom: 0;
				  position: absolute;
				  content: " ";
				  width: 3px;
				  background-color: #eeeeee;
				  left: 25px;
				  margin-right: -1.5px;
				}
				.timeline > li {
				  margin-bottom: 20px;
				  position: relative;
				}
				.timeline > li:before,
				.timeline > li:after {
				  content: " ";
				  display: table;
				}
				.timeline > li:after {
				  clear: both;
				}
				.timeline > li:before,
				.timeline > li:after {
				  content: " ";
				  display: table;
				}
				.timeline > li:after {
				  clear: both;
				}
				.timeline > li > .timeline-panel {
				  width: calc( 100% - 75px );
				  float: right;
				  border: 1px solid #d4d4d4;
				  border-radius: 2px;
				  padding: 20px;
				  position: relative;
				  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
				  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
				}
				.timeline > li > .timeline-panel:before {
				  position: absolute;
				  top: 26px;
				  left: -15px;
				  display: inline-block;
				  border-top: 15px solid transparent;
				  border-right: 15px solid #ccc;
				  border-left: 0 solid #ccc;
				  border-bottom: 15px solid transparent;
				  content: " ";
				}
				.timeline > li > .timeline-panel:after {
				  position: absolute;
				  top: 27px;
				  left: -14px;
				  display: inline-block;
				  border-top: 14px solid transparent;
				  border-right: 14px solid #fff;
				  border-left: 0 solid #fff;
				  border-bottom: 14px solid transparent;
				  content: " ";
				}
				.timeline > li > .timeline-badge {
				  color: #fff;
				  width: 50px;
				  height: 50px;
				  line-height: 50px;
				  font-size: 1.4em;
				  text-align: center;
				  position: absolute;
				  top: 16px;
				  left: 0px;
				  margin-right: -25px;
				  /*background-color: #999999;*/
				  z-index: 100;
				  border-top-right-radius: 50%;
				  border-top-left-radius: 50%;
				  border-bottom-right-radius: 50%;
				  border-bottom-left-radius: 50%;
				}
				.timeline > li.timeline-inverted > .timeline-panel {
				  float: left;
				}
				.timeline > li.timeline-inverted > .timeline-panel:before {
				  border-right-width: 0;
				  border-left-width: 15px;
				  right: -15px;
				  left: auto;
				}
				.timeline > li.timeline-inverted > .timeline-panel:after {
				  border-right-width: 0;
				  border-left-width: 14px;
				  right: -14px;
				  left: auto;
				}
				.timeline-badge.primary {
				  background-color: #2e6da4 !important;
				}
				.timeline-badge.success {
				  background-color: #3f903f !important;
				}
				.timeline-badge.warning {
				  background-color: #f0ad4e !important;
				}
				.timeline-badge.danger {
				  background-color: #d9534f !important;
				}
				.timeline-badge.info {
				  background-color: #5bc0de !important;
				}
				.timeline-title {
				  margin-top: 0;
				  color: inherit;
				}
				.timeline-body > p,
				.timeline-body > ul {
				  margin-bottom: 0;
				}
				.timeline-body > p + p {
				  margin-top: 5px;
				}
				
				.timeline-body {
					/*display: none;*/
				}
		 </style>
		 <script>
			window.onload=function(){
			  $('.selectpicker').selectpicker();
			  $('.rm-mustard').click(function() {
				$('.remove-example').find('[value=Mustard]').remove();
				$('.remove-example').selectpicker('refresh');
			  });
			  $('.rm-ketchup').click(function() {
				$('.remove-example').find('[value=Ketchup]').remove();
				$('.remove-example').selectpicker('refresh');
			  });
			  $('.rm-relish').click(function() {
				$('.remove-example').find('[value=Relish]').remove();
				$('.remove-example').selectpicker('refresh');
			  });
			  $('.ex-disable').click(function() {
				  $('.disable-example').prop('disabled',true);
				  $('.disable-example').selectpicker('refresh');
			  });
			  $('.ex-enable').click(function() {
				  $('.disable-example').prop('disabled',false);
				  $('.disable-example').selectpicker('refresh');
			  });
		
			  // scrollYou
			  $('.scrollMe .dropdown-menu').scrollyou();
		
			  prettyPrint();
			  };
			  
			  
			  /*$('.timeline-panel').click(function() {
					$('.timeline-body', this).toggle(); // p00f
					 
			  });*/
		 </script>
		 <script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap-select.js" /></script>
    </section>	
</body>
</html>
<script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
	