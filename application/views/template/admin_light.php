<!DOCTYPE html>
<html lang="en-US"><!--<![endif]-->
<head>
    <!-- META TAGS -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Title -->
    <title>IntraLINE-HR : Suksawad Group</title>

    <!-- Define a view port to mobile devices to use - telling the browser to assume that
    the page is as wide as the device (width=device-width)
     and setting the initial page zoom level to be 1 (initial-scale=1.0) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/jqueryUI/jquery-ui-1.11.4.custom.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>assets/jqueryUI/jquery-ui-1.11.4.custom.css" />
     
</head>
<body>
	<?php echo $contents; ?>               
</body>
</html>