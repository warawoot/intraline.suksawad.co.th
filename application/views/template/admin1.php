<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>IntraLINE-HR : Suksawad Group</title>
    
    <!-- ######### CSS STYLES ######### -->
    <!--Core CSS -->
 
    <link href="<?= base_url() ?>assets/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/bootstrap/assets/css/docs.min.css" rel="stylesheet">
    
    <link href="<?= base_url() ?>assets/css/bootstrap-reset.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/style-responsive.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
    
    <link href="<?= base_url() ?>assets/css/table-style.css" rel="stylesheet">
    
    <link href="<?= base_url() ?>assets/css/jquery.dataTables.css" rel="stylesheet" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link href="<?= base_url() ?>assets/bootstrap/dist/css/bootstrap-theme-modify.css" rel="stylesheet">
    <!-- ######### SCRIPT ######### -->
    <!--Core js-->
    <script src="<?= base_url() ?>assets/js/jquery-2.1.3.min.js"></script>
    
    <script src="<?= base_url() ?>assets/js/3.2.0.bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.dcjqaccordion.2.7.js" class="include" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.nicescroll.js"></script>
    
    <script src="<?= base_url() ?>assets/js/jquery.validate.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.dataTables.min.js"></script>
    
    <!--common script init for all pages-->
    <script src="<?= base_url() ?>assets/js/scripts.js"></script>
  
    <script src="<?= base_url() ?>assets/js/angular.min.js"></script>
    <script src="<?= base_url() ?>assets/js/angular-resource.min.js"></script> 
    <script src="<?= base_url() ?>assets/js/angular-route.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style type="text/css">
        .form-control {
          color: #343232;
        }
    </style>
</head>
<body>  
    <section id="container" ng-app="baseApp" >
        <!--header start-->
        <header class="header fixed-top clearfix">
        <!--logo start-->
        <div class="brand">
            <a href="<?= site_url();?>" class="logo">
                    <img src="<?= base_url() ?>assets/images/logo.png" alt="Company Logo" style="height:80px">
            </a>
            <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
            </div>
        </div>
        <!--logo end-->
        <!--top_menu--><!--top_menu end-->
        <div class="top-nav clearfix">
            <!--search & user info start-->
            <div class="col-sm-6 col-md-6 pull-left" style="margin-top:3px; margin-left:30px;">
                           <!-- <form id="attributeForm" class="form-horizontal" role="form" action="<?= site_url();?>dashboard" method="post">
                                <div class="input-group">
                                    <input type="text" class="form-control"   name="search" id="search" placeholder="ค้นหา" style=" border-radius: 0px;">
                                    <div class="input-group-btn">
                                        <button type="submit" id="check_license" class="btn btn-default" style="border-left: 0; border-radius: 0px;"/*onclick="modal_research()"*/>ค้นหา</button>
                                    </div>
                                </div>
                            </form>-->
            </div>
            <ul class="nav pull-right top-menu">
                <!-- user login dropdown start-->
                <li>                
            <div style="padding:0px;">
                    <div class="img-circle " style="zoom:1.5; margin-top:3px; opacity:0.8;  filter:alpha(opacity=40); ">
                    <a href="<?= site_url();?>dashboard" title="หน้าแรก" class="text-muted">
                    <img src="<?=base_url() ?>assets/staff/<?=$this->session->userdata('userID')?>.jpg" class="img-circle" alt="User Image" width="20"></a> 
                    </div>
                </div>  
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        
                        
                        <span class="username"><?= $this->session->userdata('fullName'); ?></span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <li><a href="/reg/editStaff?id=6"><i class=" fa fa-suitcase"></i>Profile</a></li>
                        <!--<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>-->
                        <li><a href="<?= site_url();?>logout"><i class="fa fa-key"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <!--search & user info end-->
        </div>
        </header>
        <!--header end-->


        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse" ng-controller="menuCtrl">
                    <!-- sidebar menu start-->            
                    <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <?php echo modules::run("menu");?>      
                        </ul>
                    </div>        
            <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->


        <!--main content start-->
        <section id="main-content" >
            <section class="wrapper">  
                <?php echo $contents; ?>                        
            </section>
        </section>
        <!--main content end-->

    </section>  

    
<script type="text/javascript">
   jQuery(function($) {
   var path = window.location.href; // because the 'href' property of the DOM element is the absolute path

   $('ul a').each(function() { 
      if (this.href === path) {
          //$(this).addClass('sub-menu active');
          $(this).parent().parent().closest("li").find("a").addClass('active');
          $(this).css("color", "#FFFF00");
          $(this).parent().parent().closest("ul").show();
          $(this).parent().closest("ul").show();
          $(this).parent().parent().parent().closest("ul").show();

      }
   });
}); 
</script>
</body>
</html>