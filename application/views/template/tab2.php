<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="images/favicon.png">
	<title>DPO</title>
	
	<!-- ######### CSS STYLES ######### -->
	<!--Core CSS -->
 
    <link href="<?php echo base_url() ?>assets/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/bootstrap/assets/css/docs.min.css" rel="stylesheet">
    
    <link href="<?php echo base_url() ?>assets/css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style-responsive.css" rel="stylesheet" />
    
    <link href="<?php echo base_url() ?>assets/css/table-style.css" rel="stylesheet">
    
    <link href="<?php echo base_url() ?>assets/css/jquery.dataTables.css" rel="stylesheet" />


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="<?php echo base_url() ?>assets/bootstrap/dist/css/bootstrap-theme-modify.css" rel="stylesheet">
	<!-- ######### SCRIPT ######### -->
	<!--Core js-->
	<script src="<?php echo base_url() ?>assets/js/jquery-2.1.3.min.js"></script>
    
	<script src="<?php echo base_url() ?>assets/js/3.2.0.bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.dcjqaccordion.2.7.js" class="include" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.nicescroll.js"></script>
    
	<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.dataTables.min.js"></script>
	
	<!--common script init for all pages-->
	<script src="<?php echo base_url() ?>assets/js/scripts.js"></script>
  
    <script src="<?php echo base_url() ?>assets/js/angular.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/angular-resource.min.js"></script> 
    <script src="<?php echo base_url() ?>assets/js/angular-route.min.js"></script>

     <style>
    
    .form-control {
        color:#000;
    }
    </style>
</head>
<body>
    <section id="container" ng-app="baseApp" >
                     
          <?php echo $contents; ?>    
       
	</section>
</body>
</html>
	