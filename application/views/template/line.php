<!DOCTYPE html>
<html lang="en-US"><!--<![endif]-->
<head>
    <!-- META TAGS -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Title -->
    <title>IntraLINE-HR</title>

    <!-- Define a view port to mobile devices to use - telling the browser to assume that
    the page is as wide as the device (width=device-width)
     and setting the initial page zoom level to be 1 (initial-scale=1.0) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <!-- favicon -->
    <!--link rel="shortcut icon" href="assets/images/icon/favicon.ico"-->

    <!-- Google Web Font -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,100,500,600,700,800,900,300,200" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- bootstrap Style Sheet (caution ! - Do not edit this stylesheet) -->
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Include the site main stylesheet -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/main.css" type="text/css" media="all">
    <!-- Include the site responsive  stylesheet -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/custom-responsive.css" type="text/css" media="all">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- JQUERY -->
    <script type="text/javascript" id="quick-js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/jqueryUI/jquery-ui-1.11.4.custom.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>assets/jqueryUI/jquery-ui-1.11.4.custom.css" />
     

    <!--script type='text/javascript' src='<?=base_url()?>assets/js/jquery-1.11.1.min.js'></script-->
    <script type='text/javascript' src='<?=base_url()?>assets/js/bootstrap.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery.ui.core.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>assets/js/jquery-migrate-1.2.1.min.js'></script>

</head>
<body>
    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="<php echo 'http://www.google.com/chromeframe/?redirect=true'; ?>">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->

    <header id="header">
        <div class="header-top clearfix text-center">
            <a href="<?php echo site_url(); ?>">            
            <img src="<?=base_url()?>assets/images/logo.png" alt="Company Logo" style="height:50px">
            </a>
        </div>
    </header>

<div class="page-top clearfix">
    <div class="container-fluid">
		<?php echo $contents; ?>               
    </div>
</div>

<!-- Include the site custom stylesheet -->
<link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css" type="text/css" media="all">
<script src="<?=base_url()?>assets/js/jquery.roundimage.js"></script>
<script>
$(document).ready(function() {
    $('.img-round').roundImage();
});
</script>

</body>
</html>