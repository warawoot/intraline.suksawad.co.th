<?php

class Employment_model extends MY_Model {
   
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	}
    
	function max_id(){
	    $this->db->select_max('id');
		$this->db->from('tbl_project');
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->id; 
 		}else{
			$data 		=  '1'; //  hot เท่ากับ login แต่ status ไม่เท่า 2
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
   function max_data($id){
	    $this->db->select('*');
		$this->db->from('`tbl_project`');
		$this->db->where('`id`' ,$id);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
	function distinct($id){
		$this->db->select('*');
		$this->db->from('`tbl_project`');
		$this->db->where('`id`' ,$id);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
    }
	
	function get_date_edit($id){
		
		$this->db->select('*');
		$this->db->from('tbl_project');
		$this->db->where('id' ,$id); 
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
         
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	function insert_data($data){
		
		$id		= $data['id'];
		$name		= $data['name'];
		 
		$this->db->select('id , name');
		$this->db->from('tbl_project as t1');
		$this->db->where('t1.id' ,$id); 
		$this->db->where('t1.name' ,$name); 
  		$query 		= $this->db->get();
 		 
		if($query->num_rows() == 0){
				$sql	=	$this->db->insert("tbl_project",$data);
				//exit();	 
				if($this->db->affected_rows() > 0){
						 return 'succesfully';
				}else{
						 return 'failed';
				}
		}else{
				return 'failed';
		}
 			
 	}
	
	
	function update_data($evalYear,$evalRound,$data){ 
  			
			$this->db->where('evalYear', $evalYear);
			$this->db->where('evalRound', $evalRound);
			$this->db->update('tbl_eval_date',$data); 	
			 
			//if($this->db->affected_rows() > 0){
					 return 'succesfully';
			//}else{
					 //return 'failed';
			//}
  		}
	
}
/* End of file hospital_model.php */
/* Location: ./application/module/hospital/hospital_model.php */