<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employment extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('employment_model');
      }
 

    public function index() {
  		
		$xcrud	=	xcrud_get_instance();
 		 
        $xcrud->table('tbl_project'); 
        $arr		=	array("id"=>"เลขที่คำสั่ง","name"=>"ชื่อโครงการ","startDate"=>"วันที่เริ่มต้น","endDate"=>"วันที่สิ้นสุด" );
    		
		// -- validation_required -- 
		$xcrud->validation_required('id')->validation_required('name')->validation_required('startDate')->validation_required('endDate') ;
   		$xcrud->button(site_url().'workinggroup/?token={id}','คณะทำงานโครงการ','fa fa-folder-open'); 
        $xcrud->label($arr);
        $xcrud->columns($arr); 
   		
 		$xcrud->label($arr);
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
		$data['id']	=   '';
        $this->template->load('template/admin', 'employment',$data);
	} 
	
	public function add(){
		 
		
		$data['date_start']	= '';
		$data['date_end']	= '';
		
 		$data['token']	 	= '';
		$data['data']	 	= '';
		$evalYear	= $this->input->get('token');
		$evalRound	= $this->input->get('token1');
		$data['actions'] 	= 'add';
		
		
		
		$data['token']		= $evalYear; 
		$data['token1']		= $evalRound; 
		$data['actions']	= 'add';
		 
  		//$data['get_data'] = $this->project_model->get_date_edit($evalYear,$evalRound); 
        $this->template->load('template/admin', 'employment_form',$data);
		 
	}
	
	public function check_add() {
		
		$id				= $this->input->get_post("id");
		$name 			= $this->input->get_post("name");
		$startDate		= $this->input->get_post("startDate");
		$endDate		= $this->input->get_post("endDate"); 
		$action 		= $this->input->get_post('action');
		
		// format date : 2015-05-01
		$date_start 		= explode('/',$startDate);  
		$date_end 	  		= explode('/',$endDate);
		  
		$strYear_s 			= $date_start[2];
		$strMonth_s			= $date_start[1];
		$strDay_s			= $date_start[0];
		
		$strYear_e 			= $date_end[2];
		$strMonth_e			= $date_end[1];
		$strDay_e			= $date_end[0];
		
		$date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s;
		$date_end			= $strYear_e."-".$strMonth_e."-".$strDay_e;
		
  		$data	=	array( 
		
 				"id"=>$id,
				"name"=>$name ,
 				"startDate"=>$date_start,
				"endDate"=>$date_end  
		
		);
		
		$data2	=	array( 
		
  				"startDate"=>$date_start,
				"endDate"=>$date_end  
		
		);
		
  
		if($action == 'edit'){
			//$insert_data = $this->evaluation_model->update_data($evalYear,$evalRound,$data2); 
 		}elseif($action == 'add'){
			$insert_data = $this->employment_model->insert_data($data); 	
 		}
  		 
 		 
		if($insert_data == 'succesfully'){
				echo  'succesfully';
		}else{
				echo 'failed';
		}									
         
	}  
   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>