<style type="text/css">
	.form-control {
 	  color: #343232;
	}
    #ui-datepicker-div{  
		font-size: 0.9em;
 	}  
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
 <div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ชื่องานโครงการ</h3>
            </header>
            <div class="panel-body">
                <link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                     <div class="xcrud">
                        <div class="xcrud-container">
                            <div class="xcrud-ajax">
                             
                                <div class="xcrud-view">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">เลขที่คำสั่ง*</label>
                                            <div class="col-sm-9">
                                                <input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="int" value="" name="dGJsX3Byb2plY3QuaWQ-" id="dGJsX3Byb2plY3QuaWQ-" data-pattern="integer" maxlength="11"></div>
                                        </div>
                                        <div class="form-group"><label class="control-label col-sm-3">ชื่อโครงการ*</label>
                                            <div class="col-sm-9"><input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="" name="dGJsX3Byb2plY3QubmFtZQ--" id="dGJsX3Byb2plY3QubmFtZQ--" maxlength="255"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">วันที่เริ่มต้น*</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control" data-required="1" type="text" data-type="date" value="" name="dp1433735797502" id="dp1433735797502">
                                                </div>
                                       </div>
                                       <div class="form-group">
                                            <label class="control-label col-sm-3">วันที่สิ้นสุด*</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" data-required="1" type="text" data-type="date" value="" name="dp1433735797503" id="dp1433735797503">
                                            </div>
                                       </div>
                                    </div>
                                 </div>
                                <div class="xcrud-top-actions btn-group">
                                    <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action" onclick="modal_ck()">บันทึกและย้อนกลับ</a><a href="#" data-task="list" class="btn btn-warning xcrud-action" onclick="modal_back()">ย้อนกลับ</a>
                                </div>
                            </div>
                     </div>
                </div> 
             </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 

 $(document).ready(function () {
	 var dateBefore=null;  
	  $("#dp1433735797502").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  });
	  
	  $("#dp1433735797503").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  }); 
 }); 
 
 function modal_back(){
	location.replace('<?php echo base_url().'employment'; ?>');
 }
 
 function modal_ck(){
	 
	var id	= $("#dGJsX3Byb2plY3QuaWQ-").val();
	var name	= $("#dGJsX3Byb2plY3QubmFtZQ--").val();
	var startDate	= $("#dp1433735797502").val();
	var endDate		= $("#dp1433735797503").val();
	
	if(id == ''){
		  alert('กรุณาระบุ เลขที่คำสั่ง ด้วยค่ะ');
		  location.href = '<?php echo base_url().'employment/add?token='.$token.'&token1='.$token1; ?>';
	}else if(name == ''){
		  alert('กรุณาระบุ ชื่อโครงการ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'employment/add?token='.$token.'&token1='.$token1; ?>';
	}else if(startDate ==''){
		  alert('กรุณาระบุ วันที่เริ่มต้น ด้วยค่ะ');
		  location.href = '<?php echo base_url().'employment/add?token='.$token.'&token1='.$token1; ?>';
	}else if(endDate == ''){	
		  alert('กรุณาระบุ วันที่สิ้นสุด ด้วยค่ะ');
		  location.href = '<?php echo base_url().'employment/add?token='.$token.'&token1='.$token1; ?>';
	}else{
		 
		  $.ajax({
				url: "<?php echo base_url('employment/check_add?action='.$actions) ?>",
				type: 'POST',
				data: {
						id: id,
						name: name,
						startDate: startDate,
						endDate: endDate
				}, 
				success: function(response) {
					//Do Something 
						if(response == 'succesfully'){
 							location.replace('<?php echo base_url().'employment'?>');
						}else{
 							//var obj = jQuery.parseJSON(response); 
							alert('กรุณาลองใหม่อีกครั้ง');
							//location.replace('<?php echo base_url().'employment/add' ?>');
						}
				},
				error: function(xhr) {
					//Do Something to handle error
					alert('กรุณาลองใหม่อีกครั้ง');
					//location.replace('<?php echo base_url().'employment' ?>');
				}
				
		 }); //end $.ajax
		  
	} //end if	
 }
</script>