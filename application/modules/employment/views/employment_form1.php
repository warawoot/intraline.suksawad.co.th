<style type="text/css">
	.form-control {
 	  color: #343232;
	}
    #ui-datepicker-div{  
		font-size: 0.9em;
 	}  
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>   
 <div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ชื่องานโครงการ</h3>
            </header>
            <div class="panel-body">
            	<link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                 <div class="xcrud">
                       <div class="xcrud-container">
                            <div class="xcrud-ajax">
                            	 
                                <div class="xcrud-view">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">เลขที่คำสั่ง*</label>
                                            <div class="col-sm-9">
                                                <input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="int" value="" name="dGJsX3Byb2plY3QuaWQ-" data-pattern="integer" maxlength="11"></div>
                                        </div>
                                        <div class="form-group"><label class="control-label col-sm-3">ชื่อโครงการ*</label>
                                            <div class="col-sm-9"><input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="" name="dGJsX3Byb2plY3QubmFtZQ--" maxlength="255"></div>
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label col-sm-3">วันที่เริ่มต้น*</label>
                                                <div class="col-sm-9"><input class="xcrud-input xcrud-datepicker form-control hasDatepicker" data-required="1" type="text" data-type="date" value="" name="dGJsX3Byb2plY3Quc3RhcnREYXRl" id="dp1434102827487"></div>
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label col-sm-3">วันที่สิ้นสุด*</label>
                                                    <div class="col-sm-9"><input class="xcrud-input xcrud-datepicker form-control hasDatepicker" data-required="1" type="text" data-type="date" value="" name="dGJsX3Byb2plY3QuZW5kRGF0ZQ--" id="dp1434102827488"></div>
                                        </div>
                                    </div>
                                </div>
                            <div class="xcrud-top-actions btn-group">
                                <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action">บันทึกและย้อนกลับ</a><a href="#" data-task="list" class="btn btn-warning xcrud-action" onclick="modal_back()">ย้อนกลับ</a></div>
                            </div>
                           <div class="xcrud-overlay" style="width: 1330px; opacity: 0.6; display: none;"></div>
                      </div>
                </div>
          </div>
        </section>
    </div>
</div>
<script type="text/javascript"> 

 $(document).ready(function () {
	 var dateBefore=null;  
	  $("#dp1434102827487").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  });
	  
	  $("#dp1434102827488").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  }); 
 }); 
 
 function modal_back(){
	location.replace('<?php echo base_url().'evaluation'; ?>');
 }
 
 function modal_ck(){
	 
	var evalYear	= $("#dGJsX2V2YWxfZGF0ZS5ldmFsWWVhcg--").val();
	var evalRound	= $("#dGJsX2V2YWxfZGF0ZS5ldmFsUm91bmQ-").val();
	var startDate	= $("#dp1433735797502").val();
	var endDate		= $("#dp1433735797503").val();
	
	if(evalYear == ''){
		  alert('กรุณาระบุ การประเมินประจำปี ด้วยค่ะ');
		  location.href = '<?php echo base_url().'evaluation/add?token='.$token.'&token1='.$token1; ?>';
	}else if(evalRound == ''){
		  alert('กรุณาระบุ ครั้งที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'evaluation/add?token='.$token.'&token1='.$token1; ?>';
	}else if(startDate ==''){
		  alert('กรุณาระบุ ตั้งแต่วันที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'evaluation/add?token='.$token.'&token1='.$token1; ?>';
	}else if(endDate == ''){	
		  alert('กรุณาระบุ ถึงวันที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'evaluation/add?token='.$token.'&token1='.$token1; ?>';
	}else{
		 
		  $.ajax({
				url: "<?php echo base_url('evaluation/check_add?action='.$actions) ?>",
				type: 'POST',
				data: {
						evalYear: evalYear,
						evalRound: evalRound,
						startDate: startDate,
						endDate: endDate
				}, 
				success: function(response) {
					//Do Something 
						if(response == 'succesfully'){
 							location.replace('<?php echo base_url().'evaluation'?>');
						}else{
 							//var obj = jQuery.parseJSON(response); 
							alert('กรุณาลองใหม่อีกครั้ง');
							location.replace('<?php echo base_url().'evaluation/add' ?>');
						}
				},
				error: function(xhr) {
					//Do Something to handle error
					alert('กรุณาลองใหม่อีกครั้ง');
					location.replace('<?php echo base_url().'evaluation' ?>');
				}
				
		 }); //end $.ajax
		  
	} //end if	
 }
</script>