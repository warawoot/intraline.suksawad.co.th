<?php

class Stafffeat extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_project_member')->where('person_id =', $id);
		$xcrud->join('project_id','tbl_project','id');
		$xcrud->join('position_id','tbl_project_position','id');
		
		//// List /////
		$col_name = array(
			'tbl_project.startDate' => 'วันที่',
			'tbl_project.name' => ' หัวข้อ / โครงการ',
			'tbl_project_position.name' => 'ตำแหน่ง'

		);

		$xcrud->columns('tbl_project.startDate,tbl_project.name,tbl_project_position.name');
		$xcrud->label($col_name);
		$xcrud->column_pattern('tbl_project.startDate',' {value} - {tbl_project.endDate}');
		$xcrud->column_callback('tbl_project.startDate','show_feat_date');
		$xcrud->unset_numbers()->unset_pagination()->unset_limitlist();
		// End List//

		//// Form //////

		//$xcrud->pass_var(array('staffID'=>$id));
		//$xcrud->fields('featNo,featStartDate,featDetail');
		
		$xcrud->unset_add()->unset_edit()->unset_remove();

		$data['html'] = $xcrud->render();
		

		$data['id'] = $id;
		$data['title'] = "ความดีความชอบ";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

}
/* End of file stafffeat.php */
/* Location: ./application/module/stafffeat/stafffeat.php */