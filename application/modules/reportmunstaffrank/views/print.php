
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h4 style="text-align:center;">
		<br>
			รายงานพนักงานที่เกิด <?php echo (!empty($day)) ? 'วันที่ '.$day : ""; ?> <?php echo (!empty($month)) ? 'ประจำเดือน '.$th_month[$month] : ""; ?> <?php echo (!empty($year)) ? 'ปี '.$year : ""; ?>
		
	</h4>
	
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ลำดับ</th>
			<th >รหัสพนักงาน</th>
			<th >อัตราที่</th>
			<th >ชื่อ-นามสกุล</th>
			<th >ตำแหน่ง</th>
			<th >ระดับ</th>
			<th >แผนก</th>
			<th >กอง</th>
			<th >ฝ่าย</th>
			<th>วันเกิด</th>
			
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
			foreach($r->result() as  $row){ 
					$i++;


				?>
				<tr ><!-- td 10 ตัว-->
					<td><?php echo $i; ?></td>
					<td><?php echo $row->staffID;?></td>
					<td><?php echo getSegByWork($row->ID);?></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo getPositionByWork($row->ID);?></td>
					<td><?php echo getRankByWork($row->ID);?></td>
					<td><?php echo getOrgByWork($row->ID); ?></td>
					<td><?php echo getOrg2ByWork($row->ID);?></td>
					<td><?php echo getOrg1ByWork($row->ID);?></td>
					<td><?php echo toFullDate($row->staffBirthday,'th','full'); ?></td>
				</tr>
		<?php  } }else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>