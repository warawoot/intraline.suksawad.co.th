<?php

class reportmunstaffrank extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_model->getByBirthday($data['day'],$data['month'],$data['year']);
		
		$data['title'] = "รายงานพนักงานครบกำหนดสัญญาจ้าง";
        $this->template->load("template/admin",'reportmunstaffrank', $data);

	}

	public function print_pdf(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_model->getByBirthday($data['day'],$data['month'],$data['year']);


		$data_r['html'] = $this->load->view('print',$data,true);
		
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_model->getByBirthday($data['day'],$data['month'],$data['year']);

		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportbirthday.php */
/* Location: ./application/module/reportbirthday/reportbirthday.php */