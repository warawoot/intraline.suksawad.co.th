<?php

class Report_work extends MY_Controller {
	
  function __construct() {
    parent::__construct();

    $this->load->model('staff_work_daily_model');
    $this->load->model('staff_model');
    $this->load->model('orgchart_assigndate_model');
    $this->load->model('staff_work_daily_approve_model');

    $this->load->module('pagination_module');
    $this->load->model('staff_absence_model');
  }
  	 
  public function index() {
    $time_start = microtime(true); 	  
    $page           = (!empty($_POST['page']))       ? $this->input->get_post('page')      : 1;
    $orgID          = (!empty($_POST['orgID']))      ? $this->input->get_post('orgID')     : "";
    $empID          = (!empty($_POST['empID']))      ? $this->input->get_post('empID')     : "";
    $problem        = (!empty($_POST['problem']))    ? $this->input->get_post('problem')   : "";
    $startDate      = (!empty($_POST['start_date'])) ? $this->input->get_post('start_date'): date('d/m/').(date(Y)+543);
    $endDate        = (!empty($_POST['end_date']))   ? $this->input->get_post('end_date')  : date('d/m/').(date(Y)+543);


    $pagesize = 25;
    $start = ($page-1)*$pagesize;
    $data['assignID']   = $this->orgchart_assigndate_model->getLastedDate();
    
    $data['mode']       = (!empty($_GET['mode'])) ? $this->input->get_post('mode') : "report";
    if(empty($_POST['empID'])){
      $empID          = (!empty($_GET['empID'])) ? $this->input->get_post('empID') : "";
    }
    //query 
    $r = $this->staff_work_daily_model->getDailyData($startDate,$endDate,$orgID,$empID,$problem);
    echo "<!--";
    echo "page=".$page."<br>";
    echo "startDate=".$startDate."<br>";
    echo "endDate=".$endDate."<br>";
    echo "orgID=".$orgID."<br>";
    echo "empID=".$empID."<br>";
    echo "problem=".$problem."<br>";
    echo "assignID=".$data['assignID']."<br>";
    echo "mode=".$data['mode']."<br>";
    echo "empID=".$empID."<br>";
    echo "-->";
    $tmp = 1;
    
    if(!empty($r)){
      foreach($r as $row){
        if($tmp > $start && $tmp <= ($pagesize+$start)) {
          $data['r'][] = $row;
        }
        $tmp++;
      }
    }
    $tmp = 1;
    if($data['mode'] =="view_late"){
    $staff = $this->staff_model->get_by(array('staffID'=>$empID));
        foreach ($staff as $key => $re) {
          $data['name'] = $re->staffPreName.$re->staffFName." ".$re->staffLName;
          $data['ID'] = $re->staffID; //$empID;
          $data['org1']=getOrg1ByWork($re->ID);
          $data['org2']=getOrgByWork($re->ID);
          $data['year']=$year+543;
          $data['month']=$month;
        }
    }

    $data['startDate'] = $startDate;
    $data['endDate'] = $endDate;
    $data['orgID'] = $orgID;
    $data['empID'] = $empID;
    $data['problem'] = $problem;

    $count = count($r);
    if($count > 0)
        $data['viewpage'] = $this->pagination_module->index($pagesize,$page,$count);
    else
        $data['viewpage'] = '';

    $data['absence_type']   = $this->staff_work_daily_model->get_dropdown_all('absTypeID' , 'absName' ,'tbl_absence_type');
    $data['start'] = $start;
    $data['title'] = "รายงานสรุปการทำงาน"; 
    $time_end = microtime(true);

    //dividing with 60 will give the execution time in minutes other wise seconds
    $data['execution_time'] = round(($time_end - $time_start)/60,2);
    $this->template->load('template/admin', 'main',$data);

      /*
      

      if(!empty($rc)){
        foreach ($rc as $row) {
          if($tmp > $start && $tmp <= ($pagesize+$start))
            $data['rc'][] = $row;
          $tmp++;
        }
      }
      //select for label
      */      
 		 
     }

  /*
  public function submitApprove(){

    //$start_time			= 	mysql_real_escape_string(trim($this->input->post('start_time',true)));
    $time_in				= 	mysql_real_escape_string(trim($this->input->post('time_in',true)));
    $detail					= 	mysql_real_escape_string(trim($this->input->post('detail',true)));
    $daily_date     =   mysql_real_escape_string(trim($this->input->post('daily_date',true)));
    $staff_id				=   mysql_real_escape_string(trim($this->input->post('staff_id',true)));
    $period_type   = mysql_real_escape_string(trim($this->input->post('period_type',true)));
    $minute   = mysql_real_escape_string(trim($this->input->post('minute',true)));

    $minute2   = (!empty($_POST['minute2']))?$this->input->post('minute2',true):"";


    $start_date_pop   = mysql_real_escape_string(trim($this->input->post('start_date_pop',true)));
    $end_date_pop   = mysql_real_escape_string(trim($this->input->post('end_date_pop',true)));
    $orgID_pop   = mysql_real_escape_string(trim($this->input->post('orgID_pop',true)));
    $empID_pop   = mysql_real_escape_string(trim($this->input->post('empID_pop',true)));
    $problem_pop   = mysql_real_escape_string(trim($this->input->post('problem_pop',true)));
    $pagination   = mysql_real_escape_string(trim($this->input->post('pagination',true)));

    $approvedTime = explode(':',$time_in);

    //นับนาทีจากเช้าห่างกันกี่นาที
    if($period_type ==1){
      $late_minute = round(abs(strtotime($time_in) - strtotime('8:00:00'))/60,2);
    /// กลางวัน
    }else if($period_type ==2){
      $late_minute = round(abs(strtotime($time_in) - strtotime('11:00:00'))/60,2);
    //เย็น
    }else if($period_type == 3){
      $late_minute = round(abs(strtotime('17:00:00')-strtotime($time_in) )/60,2);
    }
    /// f($this->input->post('absence')!=1){
    $arr = array(
      'staffID'				=> $staff_id,
      'dateDaily'			=> $daily_date,
      'approveBy'			=> $this->session->userdata('userID'),
      'approveDetail' => $detail,
      'createDate'    => date("Y-m-d"),
      'periodType'    => $period_type,
      'approvedTime'  => $minute,
      'approvedTime2'=>$minute2

    );
    $this->staff_work_daily_approve_model->save($arr);
  redirect('report_work?start_date='.$start_date_pop.'&end_date='.$end_date_pop.'&orgID='.$orgID_pop.'&empID='.$empID_pop.'&problem='.$problem_pop.'&page='.$pagination);
  }
  */

  public function genbDate(){
    $sql = "SELECT ID,datebtext FROM tbl_staff where ID >= 416 ";
    $r = $this->db->query($sql);
    foreach($r->result() as $row){

      $datebtext = $row->datebtext;
      if(!empty($datebtext)){
        $list = explode('/',$datebtext);
        if(count($list) > 0){
          $arr = array(
            'staffBirthday'  => $list[2].'-'.$list[1].'-'.$list[0]
          );
          $this->staff_model->save($arr,$row->ID);
        }
      }            
    }
  }

  public function geneDate(){
    $sql = "SELECT ID,dateetext FROM tbl_staff where ID >= 416";
    $r = $this->db->query($sql);
    foreach($r->result() as $row){

      $dateetext = $row->dateetext;
      if(!empty($dateetext)){
        $list = explode('/',$dateetext);
        if(count($list) > 0){
          $arr = array(
            'staffIDCardExpire'  => $list[2].'-'.$list[1].'-'.$list[0]
          );
          $this->staff_model->save($arr,$row->ID);
        }
      }
    }
  }

  public function getDaily(){
    $sql = "SELECT dailyID,timetext FROM tbl_staff_work_daily";
    $r = $this->db->query($sql);
    foreach($r->result() as $row){

      $dateetext = $row->timetext;
      
      $list = explode(' ',$dateetext);
      if(count($list) > 0){
        $listd = explode('/',$list[0]);
        $listt = explode(':',$list[1]);
        $start = '';
        $end = '';
        if($listt[0] >= 12)
          $start = $list[1];
        else
          $end = $list[1];

          $arr = array(
            'dateDaily'  => $listd[2].'-'.$listd[1].'-'.$listd[0],
            'startTime' => $start,
            'endTIme'	=> $end

          );
          $this->staff_work_daily_model->save($arr,$row->dailyID);
      }
    }
  }

   public function removeDupEmp(){
      $sql = "SELECT *,MAX(ID),count(*) as num FROM tbl_staff GROUP BY staffID having count(*) > 1";
      $r = $this->db->query($sql);
      foreach($r->result() as $row){
        $staffID = $row->staffID;
        $sql = "SELECT MAX(ID) as max_id FROM tbl_staff WHERE staffID = '$staffID'";
        $rs = $this->db->query($sql);
        $id = $rs->row()->max_id;
        $sql = "DELETE FROM tbl_staff WHERE ID = '$id'";
        $rd = $this->db->query($sql);

      }
   }
	 
   public function print_pdf(){

    $page =  (!empty($_GET['page'])) ? $this->input->get('page') : 1;
      $pagesize = 25;
      $start = ($page-1)*$pagesize;

      $startDate   = (!empty($_GET['start_date'])) ? $this->input->get('start_date') : "";
      $endDate       = (!empty($_GET['end_date'])) ? $this->input->get('end_date') : "";
      $orgID         = (!empty($_GET['orgID'])) ? $this->input->get('orgID') : "";
      $empID         = (!empty($_GET['empID'])) ? $this->input->get('empID') : "";
       if($_GET['problem']!="undefined"){
           $problem  = (!empty($_GET['problem'])) ? $this->input->get('problem') : "";
     }else{
          $problem = "";
     }
       // for detail file staff_work_late
      $data['mode'] = (!empty($_GET['mode'])) ? $this->input->get_post('mode') : "report";
      $year         = (!empty($_GET['year'])) ? $this->input->get_post('year') : "";
      $month       = (!empty($_GET['month'])) ? $this->input->get_post('month') : "";
      $data['assignID'] = $this->orgchart_assigndate_model->getLastedDate();
      $r = $this->staff_work_daily_model->getDailyData($startDate,$endDate,$orgID,$empID,$problem,$month,$year);

      $tmp = 1;
      if(!empty($r)){
        foreach($r as $row){
          if($tmp > $start && $tmp <= ($pagesize+$start))
            $data['r'][] = $row;
          $tmp++;
        }
      }
    
      
     $data['startDate'] = $startDate;
      $data['endDate'] = $endDate;
      $data['orgID'] = $orgID;
      $data['empID'] = $empID;
      $data['problem'] = $problem;

      //$count = count($r);
      //if($count > 0)
      //    $data['viewpage'] = $this->pagination_module->index($pagesize,$page,$count);
      //else
      //    $data['viewpage'] = '';
      $data['start'] = $start;
      $data['title'] = "รายงานสรุปการทำงาน"; 

    $data_r['html'] = $this->load->view('print',$data,true);
    $this->load->view('print_pdf',$data_r);

  }

  public function print_excel(){

    
    $page =  (!empty($_GET['page'])) ? $this->input->get('page') : 1;
      $pagesize = 25;
      $start = ($page-1)*$pagesize;

      $startDate   = (!empty($_GET['start_date'])) ? $this->input->get('start_date') : "";
      $endDate       = (!empty($_GET['end_date'])) ? $this->input->get('end_date') : "";
      $orgID         = (!empty($_GET['orgID'])) ? $this->input->get('orgID') : "";
      $empID         = (!empty($_GET['empID'])) ? $this->input->get('empID') : "";
       if($_GET['problem']!="undefined"){
           $problem  = (!empty($_GET['problem'])) ? $this->input->get('problem') : "";
     }else{
          $problem = "";
     }
        // for detail file staff_work_late
      $data['mode'] = (!empty($_GET['mode'])) ? $this->input->get_post('mode') : "report";
      $year         = (!empty($_GET['year'])) ? $this->input->get_post('year') : "";
      $month       = (!empty($_GET['month'])) ? $this->input->get_post('month') : "";
      $data['assignID'] = $this->orgchart_assigndate_model->getLastedDate();
      $r = $this->staff_work_daily_model->getDailyData($startDate,$endDate,$orgID,$empID,$problem,$month,$year);

      $tmp = 1;
      if(!empty($r)){
        foreach($r as $row){
          if($tmp > $start && $tmp <= ($pagesize+$start))
            $data['r'][] = $row;
          $tmp++;
        }
      }
    
      
      $data['startDate'] = $startDate;
      $data['endDate'] = $endDate;
      $data['orgID'] = $orgID;
      $data['empID'] = $empID;
      $data['problem'] = $problem;
      /* $count = count($r);
      if($count > 0)
          $data['viewpage'] = $this->pagination_module->index($pagesize,$page,$count);
      else
          $data['viewpage'] = '';
         */ 
      $data['start'] = $start;
      $data['title'] = "รายงานสรุปการทำงาน"; 
    
    $this->load->view('print_excel',$data);

  }
}

?>