<style type="text/css">
	.form-control {
 	  color: #343232;
	}

</style>
<style>
  #add_trainee{
    position:absolute;
    top:140px;
    left:80px;
  }
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
<link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3><?php echo !empty($title) ? $title : "ข้อมูลการศึกษา"; ?></h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('report_work') ?>" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label class="control-label col-sm-3">วันที่เริ่มต้น</label>
                          <div class="col-sm-9">
                              <input class="form-control" type="text" data-type="date" value="" name="start_date" id="start_date">
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="control-label col-sm-3">วันที่สิ้นสุด</label>
                          <div class="col-sm-9">
                              <input class="form-control" type="text" data-type="date" value="" name="end_date" id="end_date">
                          </div>
                       </div>
                    		<div class="form-group">
                                <label class="control-label col-sm-3">หน่วยงาน</label>
                                <div class="col-sm-9">
                                    <select class="xcrud-input form-control form-control" data-type="select" name="orgID" id="orgID"> 
                                    <?php 
                                        echo getDropdownTree('0','',$assignID,'');
                                    ?>
                                    </select>
                                </div>
                             </div>
                            
                            <div class="form-group">
                                  <button type="button" class="btn btn-warning">ค้นหา</button>
                            </div>
                    </form>
                    
            </div>
         </section>
    </div>
</div> 
<br /><br />
<table class="xcrud-list table table-striped table-hover table-bordered">
    <thead>
        <tr class="xcrud-th">
            <th class="xcrud-num">#</th>
            <th data-order="asc" data-orderby="tbl_staff.staffFName" class="xcrud-column xcrud-action">สถานะ</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> พนักงาน </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> วันที่ </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> เวลาเริ่มงาน </th>
             <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> เวลาออกงาน </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> Approve By </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> รายละเอียด </th>
         </tr>   
    </thead>
    <tbody>
        <?php 
         $num = 1 ;
         
         if($r != NULL){
            foreach ($r->result() as $row) {
                # code...
      				 //$orgID				=	$item->orgID;
      				 //$completeDate		=	$item->completeDate;
      				 //$recent_submit		=	$this->recent_model->get_recent_submit($recent_year,$recent_evalRound,$orgID);
       				 
              ?>
            <tr class="xcrud-row xcrud-row-0" > 
                <td class="xcrud-current xcrud-num"><?php echo $num;?></td>
                <td> <?php echo $row->staffID; ?>   </td>
                <td><?php echo $row->staffFName.' '.$row->staffLName; ?></td>
                <td><?php echo getOrg1ByWork($row->ID); ?></td>
                <td><?php echo getOrg2ByWork($row->ID);?></td>
                <td><?php echo getOrgByWork($row->ID); ?></td>
             </tr>
         <?php $num++; }
            }else{
         ?>	   
<style>
.red1
{
  background: #F5A9A9;
}

.red2
{
  background: #F78181;
}

.yellow1
{
  background:#F3E2A9;
}

.yellow2
{
  background:#F5DA81;
}

.green1
{
  background:#BCF5A9;
}

.green2
{
  background:#81F781;
}
</style>
            <tr class="xcrud-row xcrud-row-0 green1"> 
                <!--<td colspan="7" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>-->

                  <td class="xcrud-current xcrud-num">1</td>
                  <td style="text-align:center;"></td>
                  <td>โชคชัย ชัยมงคล</td>
                  <td>1 ธันวาคม 2558</td>
                  <td>08.30</td>
                  <td>17.30</td>
                  <td></td>
                  <td></td>
            </tr>
            <tr class="xcrud-row xcrud-row-0 green1"> 
                <!--<td colspan="7" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>-->

                  <td class="xcrud-current xcrud-num">2</td>
                  <td style="text-align:center;"></td>
                  <td>ไพลิน จันทร</td>
                  <td>1 ธันวาคม 2558</td>
                  <td>08.30</td>
                  <td></td>
                  <td>จารุวรรณ ณ ลำปาง</td>
                  <td>ลากิจช่วงบ่าย</td>
            </tr>
            <tr class="xcrud-row xcrud-row-0"> 
                <!--<td colspan="7" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>-->

                  <td class="xcrud-current xcrud-num">3</td>
                  <td style="text-align:center;"></td>
                  <td>โชคชัย ชัยมงคล</td>
                  <td>2 ธันวาคม 2558</td>
                  <td>08.45</td>
                  <td>17.40</td>
                  <td></td>
                  <td></td>
            </tr>
             <tr class="xcrud-row xcrud-row-0"> 
                <!--<td colspan="7" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>-->

                  <td class="xcrud-current xcrud-num">4</td>
                  <td style="text-align:center;" ><a href="#" onclick="openModal()">Approve</a></td>
                  <td>ไพลิน จันทร</td>
                  <td>2 ธันวาคม 2558</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
            </tr>
        <?php } ?>          
       </tbody>
    <tfoot>
   </tfoot>
</table>
  

  <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Approve</h4>
      </div>
      <div class="modal-body">
        

          <form action="<?php echo base_url('traineecourse/submitForm') ?>" method="post" enctype="multipart/form-data" id="staffForm">
              <input type='hidden' name="tcid_select" id="tcid_select">
              <input type='hidden' name="noid_select" id="noid_select">
                <div class="form-group">
                        <label class="control-label col-sm-3">เวลาเริ่มงาน</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" data-type="date" value="" name="start_time" id="start_time">
                        </div>
                     </div>
                     <div class="form-group">
                      <label class="control-label col-sm-3">เวลาสิ้นสุด</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" value="" name="end_time" id="end_time">
                         </div>
                    </div>
                   <div class="form-group">
                      <label class="control-label col-sm-3">รายละเอียด</label>
                        <div class="col-sm-9">
                          <textarea name="detail" id="detail" class="form-control"></textarea>
                         </div>
                    </div>
           
              </form>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="submitForm()" data-dismiss="modal">บันทึก</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript"> 

    

    function openModal(){
      $('#myModal').modal('show');
    }

    /*function submitForm(){
      $('#staffForm').submit();
    }*/
    
	

    $(document).ready(function () {
     var dateBefore=null;  
      $("#start_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      });
      
      $("#end_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      }); 

     
   });
  
</script>
 