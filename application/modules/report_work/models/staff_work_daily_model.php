<?php
class Staff_work_daily_model extends Default_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff_work_daily';
            $this->_pk = 'dailyID';
            $this->_timestamps = false;
        }

       
		 public function getStaff($org,$staff=''){
            if(!empty($org)){
                $sql = "SELECT  *
                        FROM tbl_staff_work c 
                        JOIN tbl_staff s ON c.staffID = s.ID 
                             
                        WHERE c.workStartDate = (SELECT max(workStartDate) from tbl_staff_work WHERE staffID=c.staffID) AND c.orgID = $org";
                 if(!empty($staff))
                 	$sql.=" AND s.ID = '$staff'"; 
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r->result() : NULL;
            } 
        }

        private function getLowerOrg($orgID){
            $strOrg = "";
            $sql = "SELECT orgID FROM tbl_org_chart WHERE upperOrgID=$orgID";
            $r = $this->db->query($sql);
            $result = $r->result();
            foreach ($result as $key => $row) {
                $strOrg .= ",".$row->orgID;
                $strOrg .= $this->getLowerOrg($row->orgID);
            }
            return $strOrg;  
        }

        public function getDailyData($startDate='',$endDate='',$orgID='',$empID='',$problem=''){
            if($startDate !="") {$startDate = toDBDate($startDate,"TH-TH");}
            if($endDate   !="") {$endDate   = toDBDate($endDate,"TH-TH");}
            // report query
            $sql = "SELECT 
              s.ID,
              s.staffID,
              s.staffIDCardOff, 
              s.staffFName, 
              s.staffLName,
              s.orgID, 
              d.dateDaily,  
              MIN(d.dateDaily) as min_dated,
              MAX(d.dateDaily) as max_dated,
              (SELECT DISTINCT approvedTime
                FROM tbl_staff_work_daily_approve da 
                WHERE 
                  da.staffID = d.staffID 
                  AND DATE(d.dateDaily) = DATE(da.dateDaily)
                  AND da.periodType=1
                )AS time_late1,
              (SELECT DISTINCT approvedTime 
                FROM tbl_staff_work_daily_approve da 
                WHERE 
                  da.staffID = d.staffID 
                  AND DATE(d.dateDaily) = DATE(da.dateDaily)
                  AND da.periodType=3
                ) AS time_late3,
              (SELECT DISTINCT approvedTime 
                FROM tbl_staff_work_daily_approve da 
                WHERE 
                  da.staffID = d.staffID 
                  AND DATE(d.dateDaily) = DATE(da.dateDaily)
                  AND da.periodType=2
              ) AS time_late2_1,
              (SELECT DISTINCT approvedTime2 
                FROM tbl_staff_work_daily_approve da 
                WHERE 
                  da.staffID = d.staffID 
                  AND DATE(d.dateDaily) = DATE(da.dateDaily)
                  AND da.periodType=2
              ) AS time_late2_2              
            FROM (SELECT * FROM tbl_staff WHERE status <> 2) s
            LEFT JOIN (SELECT * FROM tbl_staff_work_daily WHERE 1";
            
            if(!empty($startDate) && !empty($endDate)){
              $sql .= " AND DATE(dateDaily) BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' ";
            }                 
            
            $sql .= ") d
            ON d.staffID = s.staffID 
            GROUP BY s.ID,YEAR(d.dateDaily),MONTH(d.dateDaily),DAY(d.dateDaily),d.dateDaily
            HAVING 1 "; /* GROUP BY s.ID,YEAR(d.dateDaily),MONTH(d.dateDaily),DAY(d.dateDaily)
            HAVING 1 => server production error*/
            if($problem==1){
              $sql.=" AND (TIME(min_dated) > '08:00:00' OR  TIME(max_dated) < '17:00:00') ";
            }
            if(!empty($empID)){
              $sql .= " AND s.staffID='$empID'";
            }       
            if(!empty($orgID)){
              $sql .= " AND s.orgID IN ($orgID".$this->getLowerOrg($orgID).")";
            }           
            $sql.=" ORDER BY s.staffID ASC, DATE(d.dateDaily) ASC";  
            echo "<!--".$sql."-->";
            $r = $this->db->query($sql);
            $result = $r->result();
            return  $result;
          }
          
        public function getCelendarData($startDate='',$endDate='',$orgID='',$empID='',$problem=''){
            // report query
            $sql = "SELECT s.ID,
                        s.staffIDCardOff, 
                        s.staffFName, 
                        s.staffLName,
                        s.orgID
            FROM tbl_staff s WHERE 1 ";
            if(!empty($empID)){
              $sql .= " AND s.staffIDCardOff='$empID'";
            }       
            if(!empty($orgID)){
              $sql .= " AND s.orgID IN ($orgID".$this->getLowerOrg($orgID).")";
            }                          
            $sql.=" ORDER BY s.staffIDCardOff ASC";  
            $r = $this->db->query($sql);
            $result = $r->result();
            return  $result;
          }
        public function getAfternoon($staffID,$date,$moning,$evening){
           
          //if(strtotime($moning) < strtotime("12:00:00")&& strtotime($evening) > strtotime("13:00:00")){
               
               $sql = "SELECT Time(dateDaily) as re_time FROM tbl_staff_work_daily WHERE staffID = '$staffID' 
               and DATE(dateDaily) = '$date' and Time(dateDaily) >= '11:00:00' and Time(dateDaily) <= '14:00:00'  order by Time(dateDaily) asc ";
               $r = $this->db->query($sql);
               $num = $r->num_rows();
               $rs = $r->result();
               $i = 1;
                
                  if($num > 1){
                       foreach ($rs as $key => $row) {
                          if($i==1){
                            $result_time[0] = $row->re_time;
                          // เวลาเที่ยงครั้งสุดท้าย
                          }else if($i==$num){
                            $result_time[1] = $row->re_time;
                          }
                         /*echo $row->dateDaily." "."เวลาเข้า".$moning;
                         echo "<br>";
                         */
                         $i++;
                       }
                   /// ถ้ามี recode เดียว    
                  }else if($num==1){
                     foreach ($rs as $key => $row) {
                        $result_time[0] = $row->re_time;
                        $result_time[1] = "-";
                     }
                  }else{
                    $result_time[0] = "-";
                    $result_time[1] = "-"; 
                  }

           //}else{
             //  $result_time[0] = "-";
               //$result_time[1] = "-";

           //}
           /* echo "<pre>";
           print_r($result_time);
           echo "</pre>";
           echo $num;
           //echo $sql;
           exit; 
           */
           return $result_time;
        }
         public function getAbsence($id,$year=''){
               $getDate = explode("-",$year);
               $getYear =  $getDate[0];
               //YEAR(NOW())
               $sql = "SELECT * FROM tbl_staff_absence WHERE staffID = '$id' AND YEAR(absStartDate) = '$getYear'";
               $r = $this->db->query($sql);
               $num = $r->num_rows();
               //echo $sql;
               $rs = $r->result();
               if($num >= '1'){
                   foreach ($rs as $key => $re) {
                          $startDate = $re->absStartDate;
                          $endDate = $re->absEndDate;
                         while (strtotime($startDate) <= strtotime($endDate)) {
                                $arr[] = $startDate;
                                $startDate = date ("Y-m-d", strtotime("+1 day", strtotime($startDate)));
                             
                         }
                   }
                }else{
                       $arr[] = NULL;
                }
              // echo "<pre>";
              //print_r($arr);
               //echo "</pre>";
           return array($r->num_rows(),$arr);
        }
      function get_dropdown_all($id , $name ,$table){
        $this->db->order_by($name, "asc");
        $sql    = $this->db->get($table);
        $dropdowns  = $sql->result();
        
        if ($sql->num_rows() > 0){
          foreach($dropdowns as $dropdown){
          //echo $fruit = array_shift($dropdown);
          $dropdownlist[''] = "- none -";
            $dropdownlist[$dropdown->$id] = $dropdown->$name;
        }
        }else{
            $dropdownlist[''] = "- none -";
        } 
        
        $finaldropdown  = $dropdownlist;
        return $finaldropdown;
  }
                    function getSundays($y,$m){ 
                        $date = "$y-$m-01";
                        $first_day = date('N',strtotime($date));
                        $first_day = 7 - $first_day + 1;
                        $last_day =  date('t',strtotime($date));
                        $days = array();
                        for($i=$first_day; $i<=$last_day; $i=$i+7 ){
                            $days[] = $i;
                        }
                        return  $days;
                    }   
}            
