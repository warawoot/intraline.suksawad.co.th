<?php  
function duration($begin,$end){  
    $remain=intval(strtotime($end)-strtotime($begin));  
    $wan=floor($remain/86400);  
    $l_wan=$remain%86400;  
    $hour=floor($l_wan/3600);  
    $l_hour=$l_wan%3600;  
    $minute=floor($l_hour/60);  
    $second=$l_hour%60; 
    $mun = str_pad($minute,2, "0", STR_PAD_LEFT);
    //return "ผ่านมาแล้ว ".$wan." วัน ".$hour." ชั่วโมง ".$minute." นาที ".str_pad($second ,2, "0", STR_PAD_LEFT)." วินาที";  
    return array($hour,$mun);

}
?>  
<?php

$year = (!empty($year)) ? $year : (date("Y")+543);

header('Content-type: application/excel');
$filename = 'report_turnover.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        <style>
			table,th,td{
				border: 1px solid black;
				border-collapse:collapse;
				text-align:center;
			}


		</style>
    </head>
	<body>

	
		<h3 style="text-align:center;">
		<br>
			รายงานสรุปการทำงาน
		
		</h5>
		
		<br>
		
		
		<div class="xcrud-list-container">
		<table class="xcrud-list table table-hover table-bordered">
    <thead>
        <tr class="xcrud-th">
            <th class="xcrud-num">#</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="5%"> รหัสพนักงาน </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="5%"> ชื่อ-นามสกุล </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="20%"> วันที่ </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="10%"> เวลาเริ่มงาน </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="5%"><?php if($mode=="view_late"){ echo "กลับก่อน(นาที)"; }else{ echo "Approve By";  }?></th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="20%"> พักกลางวัน </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="5%"><?php if($mode=="view_late"){ echo "กลับก่อน(นาที)"; }else{ echo "Approve By";  }?></th></th>
             <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="10%"> เวลาออกงาน </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="5%"> <?php if($mode=="view_late"){ echo "กลับก่อน(นาที)"; }else{ echo "Approve By";  }?></th></th>
            
         </tr>   
    </thead>
    <tbody>
        <?php 
         $num = $start+1 ;
         $tmp_id = '';
         if(!empty($r)){
            foreach ($r as $row) {
               //$recent_submit   = $this->recent_model->get_recent_submit($recent_year,$recent_evalRound,$orgID);

          // ตรวจสอบว่าถ้าเวลาไม่เท่ากับค่าว่าง
          if($row->min_dated !="" && $row->max_dated !=""){
                   $scan_min = explode(" ",$row->min_dated);
                   $scan_max = explode(" ",$row->max_dated);

                  $time_in = explode(":",$scan_min[1]);
                  $time_out = explode(":",$scan_max[1]);
                  //print_r($row); exit;
                  $date_app = toFullDate($scan_min[0]);
      // check time afternoon
                  $afternoon = $this->staff_work_daily_model->getAfternoon($row->staffIDCardOff,$scan_min[0],$scan_min[1],$scan_max[1]);
                  if($afternoon[0]=="-" && $afternoon[1]="-"){

                          //check mode from file link staffwork_late
                          if($mode=="view_late"){
                                  $checknoon_Approve ="";
                          }else{ 
                                  $checknoon_Approve = "<button type='button' class='btn btn-warning' 
                                    onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                    \"$date_app\",\"$scan_min[0]\",
                                            \"$afternoon[0]\",2,\"$afternoon[1]\")' >Approve</button>";
                          } 
                  }else{
                     $checknoon_Approve = "";
                  }
                  
                  //ตรวจสอบชั่วโมง // check color เที่ยง
                  $hour = duration($afternoon[0],$afternoon[1]);
                  if($hour[0] <= 1){
                     if($hour[0]==1 && $hour[1] >str_pad(0,2, "0", STR_PAD_LEFT)){
                       $class_color_af = "yellow";
                        //check mode
                        if($mode=='view_late'){
                              $checknoon_Approve = "";
                             
                        }else{
                               $checknoon_Approve = "<button type='button' class='btn btn-warning' 
                              onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                              \"$date_app\",\"$scan_min[0]\",
                              \"$afternoon[0]\",2,\"$afternoon[1]\")'>Approve</button>";
                        }
                     }else{
                       $class_color_af = "green2";
                     }
                  }else{
                    $class_color_af ="yellow";
                     
                      if($mode == 'view_late'){
                          $checknoon_Approve = "";
                      }else{
                          $checknoon_Approve = "<button type='button' class='btn btn-warning' 
                        onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                        \"$date_app\",\"$scan_min[0]\",
                        \"$afternoon[0]\",2,\"$afternoon[1]\")'>Approve</button>";
                      }

                  }
                   if($afternoon[0]=="-" && $afternoon[1]="-"){
                       $class_color_af ="red1";
                   }
      // end check afternoon               


       //check เข้างาน
                        if(strtotime($scan_min[1])>=strtotime("12:00:00")){
                             $check_in_re =  "ไม่ scan เข้างาน";
                              $class_color = "red1";
                              
                              //check mode
                              if($mode =="view_late"){
                                     $checkin_Approve ="";
                              }else{
                                     $checkin_Approve  = "<button type='button' class='btn btn-warning' 
                                    onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                    \"$date_app\",\"$scan_min[0]\",
                                    \"$check_in_re\",1,\"\")'>Approve</button>";
                              }

                        // if late
                        }else if(strtotime($scan_min[1]) > strtotime("8:00:00")){
                              //late === 8.30 - 8.30
                              if(strtotime($scan_min[1]) <= strtotime("8:30:00")){
                               $come_normal = "มาสาย";
                               $class_color = "yellow";
                             /// late === มากกว่า 8.30
                              }else{
                               $class_color = "yellow1";
                              }
                              $check_in_re = $scan_min[1];
                              
                              /// check mode
                              if($mode =="view_late"){
                                   $checkin_Approve = round(abs(strtotime($scan_min[1]) - strtotime('8:00:00'))/60,2)." นาที";
                                       
                              }else{
                                      $checkin_Approve ="<button type='button' class='btn btn-warning' 
                                        onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                        \"$date_app\",\"$scan_min[0]\",
                                  \"$check_in_re\",1,\"\")'>Approve</button>";
                              }
                        // normal
                        }else{
                              $come_normal = "มาตรงเวลา";
                              $class_color = "green2";
                               $check_in_re = $scan_min[1];
                               $checkin_Approve = "";
                        }
        // จบ check การเข้างาน

        // check ออกงาน
                        if(strtotime($scan_max[1])<=strtotime("14:00:00")){
                          $check_out_re = "ไม่ scan ออกงาน";
                          $class_color2 = "red1";

                            if($mode=="view_late"){
                                $checkout_Approve = "";
                            }else{
                                $checkout_Approve = "<button type='button' class='btn btn-warning' 
                                onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                \"$date_app\",\"$scan_min[0]\",
                              \"$check_out_re\",3,\"\")'>Approve</button>";
                            }
                        // if non-normal
                        }else if(strtotime($scan_max[1])<strtotime("17:00:00")){
                          
                          //late === < 16.30 yellow
                              if(strtotime($scan_max[1]) < strtotime("16:30:00")){
                                  $class_color2 = "yellow";
                              }else{
                                  $class_color2 = "yellow1";
                              }
                             /// late === มากกว่า 8.30
                           $check_out_re = $scan_max[1];
                           
                           if($mode=="view_late"){
                                  $checkout_Approve = round(abs(strtotime('17:00:00')-strtotime($scan_max[1]))/60,2)." นาที";
                           
                           }else{
                                  $checkout_Approve = "<button type='button' class='btn btn-warning'
                                 onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                 \"$date_app\",\"$scan_min[0]\",
                              \"$check_out_re\",3,\"\")'>Approve</button>";
                            }
                        // if normal
                        }else{
                          $check_out_re = $scan_max[1];
                           $class_color2 = "green2";
                           $checkout_Approve = "";
                        }
          // จบ check การออกงาน           
                        // ออกตรงเวลา //
                       // $cond3 = (intval($liste[0]) > intval($this->config->item('end_hour')));
                        //$cond4 = (intval($liste[0]) == intval($this->config->item('end_hour')) && intval($liste[1]) >= intval($this->config->item('end_minute')) );
            // ปิดการตรวจสอบเวลา
            }else{
             
              $scan_min[0]=$startDate;
               // การลา
              $absence = $this->staff_work_daily_model->getAbsence($row->ID,$startDate);
              if($absence >='1'){
                  $class_color = "blue";
                  $class_color_af = "blue";
                  $class_color2 = "blue";

              }else{
                 $class_color = "";
                 $class_color_af = "";
                 $class_color2 = ""; 

              }
              //checking
              $check_in_re = "";
              
              $checkin_Approve = "";

              //afternoon
              $afternoon[0] = "";
              $afternoon[1] = "";
             
              $checknoon_Approve = "";

              //check out
              $check_out_re = "";
             
              $checkout_Approve = "";

            }
              ?>
          
                <tr class="xcrud-row xcrud-row-0 <?php //echo $class; ?>" > 
                    <td class="xcrud-current xcrud-num"><?php echo $num;?></td>
                    <td><?php echo $row->staffIDCardOff;?></td>
                    <td style="white-space: nowrap;"><?php echo $row->staffFName." ".$row->staffLName;?></td>
                    <td style="white-space: nowrap;"><?php echo toFullDate($scan_min[0]);?></td>
                    <td class="<?=$class_color;?>"><?php echo $check_in_re; ?></td>
                    <td><?php echo $checkin_Approve;?></td>
                    <td style="white-space: nowrap;" class="<?=$class_color_af;?>"><?php echo $afternoon[0]."-".$afternoon[1];?></td>
                    <td><?php echo $checknoon_Approve; ?></td>
                    <td class="<?=$class_color2;?>"><?php echo $check_out_re; ?></td>
                    <td><?php echo $checkout_Approve; ?></td>
                 </tr>
           <?php $num++; 
               
              }
            }else{
         ?>    

            <tr class="xcrud-row xcrud-row-0 green1"> 
                
                  <td colspan="10" style="text-align:center;">ไม่พบข้อมูล</td>
                  
            </tr>
           
        <?php } ?>          
       </tbody>
    <tfoot>
   </tfoot>
</table>
		</div>

	</body>
</html>
