<link type="text/css" rel="stylesheet" href="<?=base_url()?>xcrud/themes/bootstrap/xcrud.css">
<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/css/checkbox/style-checkbox.css"/>

<style type="text/css">
	.form-control {
 	  color: #343232;
	}
  #add_trainee{
    position:absolute;
    top:140px;
    left:80px;
  }
.red1
{
  background: #F5A9A9;
}

.red2
{
  background: #F78181;
}
.yellow{
  background:#FFFF00;
}
.yellow1
{
  background:#F3E2A9;
}
.yellow2
{
  background:#F5DA81;
}
.green1
{
  background:#BCF5A9;
}
.green2
{
  background:#81F781;
}
.blue{
   background:#00FFFF;
}
select{
  margin-bottom:20px;
}
.color_prov{
text-decoration:line-through ;
color: red;
}
.cal-head
{
  background-color: #455A64;
  color: #FFF;
  width: 30px !important;
  padding:5px !important;
  font-size:8px;
  font-weight: bold;
}

.cal-norm
{
  padding:5px !important;
  width: 30px !important;    
}

.cal-holiday
{
  background-color: #FFCC80;
  padding:5px !important;
  width: 30px !important;      
}.viewbt
{
  background-color: #4CAF50;
}

.report_table td {
  padding   : 10px !important;
  font-size : 12px;
  height    : 80px;
}

.ui-datepicker-trigger {
  float:left;  
  margin-left:5px;
  margin-top:10px;
}

.hasDatepicker {
  float:left;
}

</style>

<?php  
function duration($begin,$end){  
    $remain=intval(strtotime($end)-strtotime($begin));  
    $wan=floor($remain/86400);  
    $l_wan=$remain%86400;  
    $hour=floor($l_wan/3600);  
    $l_hour=$l_wan%3600;  
    $minute=floor($l_hour/60);  
    $second=$l_hour%60; 
    $mun = str_pad($minute,2, "0", STR_PAD_LEFT);
    //return "ผ่านมาแล้ว ".$wan." วัน ".$hour." ชั่วโมง ".$minute." นาที ".str_pad($second ,2, "0", STR_PAD_LEFT)." วินาที";  
    return array($hour,$mun);

}
$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
?>  
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3><?php echo !empty($title) ? $title : "ข้อมูลการทำงาน"; ?></h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('report_work') ?>" method="post" id="searchForm" enctype="multipart/form-data">
                      <input type="hidden" name="page" id="page">

                      <input type="hidden" name="mode" id="mode" value="<?php echo $mode;?>">
                     
                <?php 
                if($mode=="view_late"){
                ?>
                  <div class="form-group">
                          <label class="control-label col-sm-3">รหัสพนักงาน:</label>
                          <div class="col-sm-9">
                            <input type="hidden" name="month" id="month" value="<?php echo $month;?>">
                             <input type="hidden" name="year" id="year" value="<?php echo $year-543;?>">
                             <label class="form-label" id="empID"><?=$ID;?></label>
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="control-label col-sm-3">ชื่อ-นามสกุล:</label>
                          <div class="col-sm-9">
                               <label class="form-label"><?=$name;?></label>
                          </div>
                       </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">ฝ่าย:</label>
                            <div class="col-sm-9">
                                 <label class="form-label"><?=$org1;?></label>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="control-label col-sm-3">แผนก:</label>
                            <div class="col-sm-9">
                                <label class="form-label"><?=$org2;?></label>
                               
                            </div>
                         </div>
                         <?php 
                            foreach ($th_month as $key => $val) {
                                if($key==$month){
                                    $result_month = $val;

                                }
                            }
                         ?>
                          <div class="form-group">
                            <label class="control-label col-sm-3">วันที่เริ่มต้น :</label>
                            <div class="col-sm-9">
                                <label class="form-label"><?php echo toFullDate($startDate); ?></label>
                               
                            </div>
                         </div>
                          <div class="form-group">
                            <label class="control-label col-sm-3">วันที่สิ้นสุด :</label>
                            <div class="col-sm-9">
                                <label class="form-label"><?php echo toFullDate($endDate); ?></label>
                               
                            </div>
                         </div>
                         
                <?php
                }else{
                  ?>
                      <div class="form-group">
                          <label class="control-label col-sm-3">วันที่</label>
                          <div class="col-sm-9">
                            <input  
                                data-required="1" 
                                type="text" 
                                style="width:250px;background-color:#FFF" 
                                data-type="date" 
                                name="start_date" 
                                id="start_date"
                                readonly=true
                                class="form-control input-small" 
                                value="<?=$startDate?>">
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="control-label col-sm-3">วันที่สิ้นสุด</label>
                          <div class="col-sm-9">
                            <input  
                                data-required="1" 
                                type="text" 
                                style="width:250px;background-color:#FFF" 
                                data-type="date" 
                                name="end_date" 
                                id="end_date"
                                readonly=true
                                class="form-control input-small" 
                                value="<?=$endDate?>">                            
                          </div>
                       </div>
                    		<div class="form-group">
                            <label class="control-label col-sm-3">หน่วยงาน</label>
                            <div class="col-sm-9">
                                <select class="xcrud-input form-control" data-type="select" name="orgID" id="orgID"> 
                                  <option value="">----กรุณาเลือก-----</option>
                                <?php 
                                    echo getDropdownTree('0','',$assignID,$orgID);
                                ?>
                                </select>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="control-label col-sm-3"> พนักงาน</label>
                            <div class="col-sm-9">
                               
                                <?php 
                                    echo getDropdown(listData('tbl_staff','staffIDCardOff','staffFName,staffLName'),'empID', $empID, "class='xcrud-input  form-control'");
                                ?>
                               
                            </div>
                         </div>
                          <div class="form-group">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-9">
                          
                                <input type="checkbox" name="problem" id="problem" value="1" <?php echo ('1'== $problem) ? "checked" : ""; ?>>แสดงเฉพาะรายงานที่มีปัญหา<br>
                               
                            </div>
                         </div>
                            
                            <div class="form-group">
                                  <button type="submit" id="search" class="btn btn-warning">ค้นหา</button>

                            </div>
              <?php }  ?>
                    </form>
                    
            </div>
         </section>
    </div>
</div> 

<div>
<a class="btn btn-success"  href="javascript:<?php if($mode=="view_late"){ echo "print_excel1()";}else{ echo "print_excel()";}?>" style="float:right;"><i class="fa fa-table"></i> Excel </a>
<a class="btn btn-info"  href="javascript:<?php if($mode=="view_late"){ echo "print_pdf1()";}else{ echo "print_pdf()";}?>;" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>
</div>
<br><br>
<table class="xcrud-list table table-hover table-bordered">
  <thead>
    <tr class="xcrud-th">
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="5%">รหัสพนักงาน </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="5%">ชื่อ-นามสกุล </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="20%">วันที่ </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="10%">เวลาเริ่มงาน </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="20%">พักกลางวัน </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="10%">เวลาออกงาน </th>
    </tr>
  </thead>
  <tbody>
  <script>
    function setLate(id)
    {
      document.getElementById(id).style.backgroundColor = "#F00";
    }
  </script>
  <?php 
   $latestr = '';
   $checkin_Approve_ = array();
   $checknoon_Approve_ = array();
   $checkout_Approve_ = array();
   $tmp_id = '';
   if(!empty($r)){
      foreach ($r as $row) {
          $color_app1 ="";
          $color_app2 = "";
          $color_app3 = "";
          $class_color = "";
          $class_color_af = "";
          $class_color3 = ""; 
          $show_date = toFullDate($scan_min[0]);
         // btn apporve
         // สีการลา
          $class_color_absence ="";          
          // ตรวจสอบว่าถ้าเวลาไม่เท่ากับค่าว่าง
          if($row->min_dated =="" && $row->max_dated ==""){
            //ไม่มีเวลาเข้างานในวันนั้น
              $class_color_absence ="red1";

              $scan_min[0]=$startDate;
               // การลา
              $absence = $this->staff_work_daily_model->getAbsence($row->ID,$startDate);
              $status_absence = 0;
              if($absence[0] >='1'){
             
                  foreach ($absence[1] as $key => $val) {
                      //echo $val." ".$absence[0]."/".$startDate."<br>";
                      /// เวลาเปรียบเทียบลา
                      $date_query =date('Y-m-d', strtotime($val));
                      // เวลาดึงจากลิสต์
                      $date_list = date('Y-m-d', strtotime($startDate));
                      if($date_query == $date_list){
                        $class_color = "blue";
                        $class_color_af = "blue";
                        $class_color3 = "blue";

                        //btn apporve
                        $checkin_Approve = "";
                        $checknoon_Approve = "";
                        $checkout_Approve = "";

                          // สีการลา
                        $class_color_absence ="blue";

                        $status_absence =1;
                      }else{
                        /*
                          if($status_absence !=1){
                            $status_absence = 2;
                            $class_color = "";
                            $class_color_af = "";
                            $class_color3 = ""; 
                             // btn apporve
                          }
                        */  
                       
                      }

                  }
              }else if($absence[0]<'1'){
                $class_color = "";
                $class_color_af = "";
                $class_color3 = ""; 
                $show_date = "";//toFullDate($scan_min[0]);
                 // btn apporve
                $checkin_Approve = "";
                $checknoon_Approve = "";
                $checkout_Approve = "";

                /*
                $checkin_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffID,\"$row->staffFName\",\"$row->staffLName\",\"$show_date\",\"$scan_min[0]\",
                              \"$check_out_re\",1,\"\")'>Approve</button>";
                $checknoon_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffID,\"$row->staffFName\",\"$row->staffLName\",\"$show_date\",\"$scan_min[0]\",
                              \"$check_out_re\",2,\"\")'>Approve</button>";
                $checkout_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffID,\"$row->staffFName\",\"$row->staffLName\",\"$show_date\",\"$scan_min[0]\",
                              \"$check_out_re\",1,\"\")'>Approve</button>";

                */
                 // สีการลา
              $class_color_absence ="red1";

              }
              //checking
              $check_in_re = "";
              
              //$checkin_Approve = "";

              //afternoon
              $afternoon[0] = "";
              $afternoon[1] = "";

              //check out
              $check_out_re = "";
              // สีการลา

          }
          else {
            $scan_min = explode(" ",$row->min_dated);
            $scan_max = explode(" ",$row->max_dated);
            $time_in = explode(":",$scan_min[1]);
            $time_out = explode(":",$scan_max[1]);
            $date_app = toFullDate($scan_min[0]);

/************************************************************************check เข้างาน *************************************************************/
          if(strtotime($scan_min[1]) > strtotime("11:00:00")){
            $check_in_re =  "ไม่ scan เข้างาน";
            $class_color = "red1";    
          }else if(strtotime($scan_min[1]) > strtotime("8:00:00")){
              //late === 8.30 - 8.30
              if(strtotime($scan_min[1]) <= strtotime("8:30:00")){
                $come_normal = "มาสาย";
                $class_color = "yellow";
              /// late === มากกว่า 8.30
              }else{
                $class_color = "yellow1";
              }
              $check_in_re = $scan_min[1];
          // normal
          }else{
              $come_normal = "มาตรงเวลา";
              $class_color = ""; //green2";
              $check_in_re = $scan_min[1];
          }
          //check aprove 
          if($row->time_late1 !=""){
              $class_color = "";
              $color_app1 ="color_prov";
          }
/************************************************************************ end check เข้างาน *************************************************************/

/************************************************************************ check พักกลางวัน *************************************************************/
             $afternoon = $this->staff_work_daily_model->getAfternoon($row->staffID,$scan_min[0],$scan_min[1],$scan_max[1]);
             if($afternoon[0]=="-" || $afternoon[1]=="-"){
                //ไม่ Scan เวลาพัก ออก หรือ เข้า
                $class_color_af ="red1";              
             }   
             else {
                //ตรวจสอบชั่วโมง // check color เที่ยง
                $hour = duration($afternoon[0],$afternoon[1]);
                if($hour[0] <= 1){
                  if($hour[0]==1 && $hour[1] >str_pad(0,2, "0", STR_PAD_LEFT)){
                    $class_color_af = "yellow";
                    $latestr .= "setLate('".$row->staffID."_".date('j',strtotime($row->min_dated))."_1');";  
                    //$checknoon_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffID,\"$row->staffFName\",\"$row->staffLName\",\"$date_app\",\"$scan_min[0]\",\"$afternoon[0]\",2,\"$afternoon[1]\")' >Approve</button>";
                  }else{
                    $class_color_af = "";
                    $checknoon_Approve = "";
                  }
                }else{
                  $class_color_af ="yellow";
                  $latestr .= "setLate('".$row->staffID."_".date('j',strtotime($row->min_dated))."_1');";
                  //$checknoon_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffID,\"$row->staffFName\",\"$row->staffLName\",\"$date_app\",\"$scan_min[0]\",\"$afternoon[0]\",2,\"$afternoon[1]\")' >Approve</button>";
                }

                //check approve 
                if($row->time_late2_1 !=""){
                  $class_color_af = "";
                  $color_app2 ="color_prov";
                }
             }         
/********************************************************************* end check พักกลางวัน *************************************************************/

/************************************************************************ check เวลาออกงาน ***************************************************************/
            if(strtotime($scan_max[1])<=strtotime("14:00:00")){
              $check_out_re = "ไม่ scan ออกงาน";
              $class_color3 = "red1";
            // if non-normal
            }else if(strtotime($scan_max[1])<strtotime("17:00:00")){
              //late === < 16.30 yellow
              if(strtotime($scan_max[1]) < strtotime("16:30:00")){
                $class_color3 = "yellow";
              }else{
                $class_color3 = "yellow1";
              }
              /// late === มากกว่า 8.30
                $check_out_re = $scan_max[1];
                $latestr .= "setLate('".$row->staffID."_".date('j',strtotime($row->max_dated))."_3');";                                
            }else{
              $check_out_re = $scan_max[1];
              $class_color3 = "";//green2";
              $checkout_Approve = "";
            }
            // aprove
            if($row->time_late3 !=""){
              $class_color3 = "";
              $color_app3 = "color_prov";
            }

/************************************************************************ check เวลาออกงาน ***************************************************************/
          } //มีเวลาเข้างาน  
  ?>
    <tr class="report_table <?php echo $class_color_absence;?>" > 
        <td ><?php echo $row->staffID;?></td> 
        <td style="white-space: nowrap;"><?php echo $row->staffFName." ".$row->staffLName;?></td>
        <td style="text-align:center" style="white-space: nowrap;"><b><?php echo $show_date;?></b></td>
        <td style="text-align:center" class="<?=$class_color;?>"><span class="<?=$color_app1;?>"><?php echo $check_in_re;?></span><?php echo (!empty($row->time_late1)) ? "<BR>".$row->time_late1 : "";?></td>
        <td style="text-align:center" style="white-space: nowrap;" class="<?=$class_color_af;?>"><span class="<?=$color_app2;?>"><?php echo $afternoon[0]."-".$afternoon[1];?></span><?php echo (!empty($row->time_late2_1)) ? "<BR>".$row->time_late2_1."-".$row->time_late2_2 : "";?></td>
        <td style="text-align:center" class="<?=$class_color3;?>"><span class="<?=$color_app3;?>"><?php echo $check_out_re;?></span><?php echo (!empty($row->time_late3)) ? "<BR>".$row->time_late3 : "";?></td>
     </tr>
  <?php 
    }
  }else{
  ?>    
    <tr class="report_table green1">                 
      <td colspan="10" style="text-align:center;">ไม่พบข้อมูล</td>                  
    </tr>          
<?php } ?>          
</tbody>
</table>
<br>
<b>Total Execution Time:</b> <?=$execution_time?> Mins<br><br>      

<!--input type="button" title="SetLate" onclick="<?php echo $latestr?>"-->
<?php echo $viewpage; ?>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">แก้ไขเวลาทำงาน</h4>
      </div>
      <div class="modal-body">
        
          <form action="<?php echo base_url('report_work/submitApprove') ?>" method="post" enctype="multipart/form-data" id="approveForm" class="form-horizontal" role="form">
              <input type='hidden' name="staff_id" id="staff_id">
              <input type='hidden' name="daily_date" id="daily_date">
              <input type='hidden' name="time_in" id="time_in">
              <input type="hidden" name="period_type" id="period_type">

              <input type="hidden" name="start_date_pop" id="start_date_pop">
              <input type="hidden" name="end_date_pop" id="end_date_pop">
              <input type="hidden" name="orgID_pop" id="orgID_pop">
              <input type="hidden" name="empID_pop" id="empID_pop">
              <input type="hidden" name="problem_pop" id="problem_pop">
              <input type="hidden" name="pagination" id="pagination">
                
                    <div class="form-group">
                      <label class="control-label col-sm-3">พนักงาน</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" readonly="readonly" value="" name="staff" id="staff">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">วันที่</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" value="" readonly="readonly"  name="date" id="date">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">เวลาทำงาน</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="time" value="" readonly="readonly"  name="time" id="time">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">รายละเอียด</label>
                        <div class="col-sm-9">
                          <textarea name="detail" id="detail" class="form-control"></textarea>
                         </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">เวลาที่เปลี่ยนใหม่</label>
                        <div class="col-sm-4">
                            <div class="input-group bootstrap-timepicker timepicker datepicker">
                              <input id="minute" name="minute" type="text" class="form-control input-small ">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div> 
                        </div>
                        <div class="col-sm-4" id="hiddinTime2">
                              <div class="input-group bootstrap-timepicker timepicker datepicker">
                                <input id="minute2" name="minute2" type="text" class="form-control input-small ">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                              </div> 
                        </div>
                    </div>
                    
              </form>
              </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" onclick="window.open('/absence/add');" data-dismiss="modal" id="btnLeave">ใบลา</button>
        <button type="button" class="btn btn-primary" onclick="submitForm()" data-dismiss="modal">บันทึก</button>
      </div>
    </div>
  </div>
  </div>
  <div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View</h4>
      </div>
      <div class="modal-body">
      <form class="form-horizontal" role="form">
                    <div class="form-group" >
                      <label class="control-label col-sm-3">พนักงาน</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" readonly="readonly" value="" name="staff2" id="staff2">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">วันที่</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" value="" readonly="readonly"  name="date2" id="date2">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">เวลาทำงาน</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="time" value="" readonly="readonly"  name="time2" id="time2">
                         </div>
                    </div>
      </form>
      </div>
    </div>
  </div>
  </div>

  <div id="myModalLeave" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">บันทึกการลา</h4>
      </div>
      <div class="modal-body">
      <form class="form-horizontal" role="form">
                    <div class="form-group" >
                      <label class="control-label col-sm-3">พนักงาน</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" readonly="readonly" value="" name="staff2" id="staff2">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">วันที่</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" value="" readonly="readonly"  name="date2" id="date2">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">เวลาทำงาน</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="time" value="" readonly="readonly"  name="time2" id="time2">
                         </div>
                    </div>
      </form>
      </div>
    </div>
  </div>
  </div>

<style type="text/css">
.datepicker{z-index:1151 !important;}
</style>
<script type="text/javascript">
/* function time_pic(id){
  if(id==1){
    $('#minute').timepicker({
          showMeridian : false,
          defaultTime: '08:00'  
    });
  }else if(id==2){
    $('#minute').timepicker({
          showMeridian : false,
          defaultTime: '17:00'  
    });
  }
}
*/
  $('#minute').timepicker({
          showMeridian : false,
          defaultTime: '08:00'  
    });
  $('#minute2').timepicker({
          showMeridian : false,
          defaultTime: '13:00'  
    });
</script>

<script type="text/javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
<script>
$.datepicker.regional['th'] ={
  changeMonth: true,
  changeYear: true,
  //defaultDate: GetFxupdateDate(FxRateDateAndUpdate.d[0].Day),
  yearOffSet: 543,
  showOn: "button",
  buttonImage: '<?=base_url()?>assets/images/common_calendar_month_-20.png',
  buttonImageOnly: true,
  dateFormat: 'dd/mm/yy',
  dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
  dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
  monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
  monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
  constrainInput: true,

  prevText: 'ก่อนหน้า',
  nextText: 'ถัดไป',
  yearRange: '-20:+0',
  buttonText: 'เลือก',

};

$.datepicker.setDefaults($.datepicker.regional['th']);

$(function() {
  $( "#start_date" ).datepicker( $.datepicker.regional["th"] ); 
  $( "#end_date" ).datepicker( $.datepicker.regional["th"] ); 
  <?php if($startDate == '') {?> $( "#start_date" ).datepicker("setDate", new Date()); <?php }?>
  <?php if($endDate   == '') {?> $( "#end_date"   ).datepicker("setDate", new Date()); <?php }?>
});
    
</script>  
<script type="text/javascript"> 

    function openModalLeave(staff_id,staff_name,staff_lastname,date_in,date_in_db,time_in,period,time_out){
      var staffID =staff_id.toString();
      var staffName =staff_name;
      var date= date_in.toString() ;
      var time_out_chek;

      /// status coming monirng afternoon evening
      var period_type = period;
      if(time_out===''){
        time_out_chek = ""; 
      }else{
        time_out_chek = "-"+time_out.toString();
      }
      var time = time_in+time_out_chek;
      var staffLastname = staff_lastname;
      var name = staffID+"-".concat(staffName)+"   "+staffLastname;
      $('#staff2').val(name);
      $('#date2').val(date);
      $('#time2').val(time);
      //hidden
      //$('#daily_date').val(daily_date);
      //alert(staff_id);
      $('#myModalLeave').modal('show');

    }
    
    function openModal2(staff_id,staff_name,staff_lastname,date_in,date_in_db,time_in,period,time_out){
      var staffID =staff_id.toString();
      var staffName =staff_name;
      var date= date_in.toString() ;
      var time_out_chek;

      /// status coming monirng afternoon evening
      var period_type = period;
      if(time_out===''){
        time_out_chek = ""; 
      }else{
        time_out_chek = "-"+time_out.toString();
      }
      var time = time_in+time_out_chek;
      var staffLastname = staff_lastname;
      var name = staffID+"-".concat(staffName)+"   "+staffLastname;
      $('#staff2').val(name);
      $('#date2').val(date);
      $('#time2').val(time);
      //hidden
      //$('#daily_date').val(daily_date);
      //alert(staff_id);
      $('#myModal2').modal('show');

    }

    function openModal(staff_id,staff_name,staff_lastname,date_in,date_in_db,time_in,period,time_out){
      //var staffID = staff_id.toString();
      var staffID =staff_id.toString();
      var staffName =staff_name;
      var date= date_in.toString() ;
      var time_out_chek;

      /// status coming monirng afternoon evening
      var period_type = period;
      if(time_out===''){
        time_out_chek = ""; 
      }else{
        time_out_chek = "-"+time_out.toString();
      }
      var time = time_in+time_out_chek;
      var staffLastname = staff_lastname;
      var name = staffID+"-".concat(staffName)+"   "+staffLastname;
      $('#staff').val(name);
      $('#date').val(date);
      $('#time').val(time);
      //hidden
      $('#staff_id').val(staff_id);
      $('#daily_date').val(date_in_db);
      $('#time_in').val(time_in);
      $('#period_type').val(period_type);
       
      $('#start_date_pop').val($('#start_date').val());
      $('#end_date_pop').val($('#end_date').val());
      $('#orgID_pop').val($('#orgID').val());
      $('#empID_pop').val($('#empID').val());
      $('#pagination').val($('#page_selected').html());
      if($('#problem').is(':checked')){
        $('#problem_pop').val($('#problem').val());
      }
      if(period_type==1) {
        $('#minute').val('08:00');  
        $('#hiddinTime2').hide();     
      } 
      else if(period_type==2) {
        $('#minute').val('12:00'); 
      }
       else {
          $('#minute').val('17:00');
          $('#hiddinTime2').hide();                        
       }
      $('#myModal').modal('show');

    }

    /*function submitForm(){
      $('#staffForm').submit();
    }*/
    
	

    $(document).ready(function () {
     var dateBefore=null;  
      $("#start_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      });
      
      $("#end_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      }); 

     
   });

   function approve(period, staffID, dateApprove){
     alert(period);
     alert(staffID);
     alert(dateApprove);
   }

   function submitForm(){
    $('#approveForm').submit();
   }

   function getPaging(pageID){
      $('#page').val(pageID);
      $('#searchForm').submit();
   }
    function print_pdf(){
      window.open('<?php echo site_url();?>report_work/print_pdf?start_date='+$('#start_date1').val()+'&end_date='+$('#end_date1').val()+'&orgID='+$('#orgID').val()+'&empID='+$('#empID').val()+'&problem='+$('#problem:checked').val()+'&page='+$('#page_selected').text());
    }
    function print_excel(){
      window.open('<?php echo site_url();?>report_work/print_excel?start_date='+$('#start_date1').val()+'&end_date='+$('#end_date1').val()+'&orgID='+$('#orgID').val()+'&empID='+$('#empID').val()+'&problem='+$('#problem:checked').val()+'&page='+$('#page_selected').text());
    }
   
  
</script>
<script type="text/javascript">
     function print_pdf1(){
          window.open('<?php echo site_url();?>report_work/print_pdf?empID='+$('#empID').text()+'&month='+$('#month').val()+'&year='+$('#year').val()+'&page='+$('#page_selected').text()+'&mode=view_late');
        }

</script>
<script type="text/javascript">
  function print_excel1(){
           window.open('<?php echo site_url();?>report_work/print_excel?empID='+$('#empID').text()+'&month='+$('#month').val()+'&year='+$('#year').val()+'&page='+$('#page_selected').text()+'&mode=view_late');
        }
</script>