<style type="text/css">
	.form-control {
 	  color: #343232;
	}

</style>
<style>
  #add_trainee{
    position:absolute;
    top:140px;
    left:80px;
  }
</style>
<style>
.red1
{
  background: #F5A9A9;
}

.red2
{
  background: #F78181;
}
.yellow{
  background:#FFFF00;
}
.yellow1
{
  background:#F3E2A9;
}

.yellow2
{
  background:#F5DA81;
}

.green1
{
  background:#BCF5A9;
}

.green2
{
  background:#81F781;
}
.blue{
   background:#00FFFF;
}
select{
  margin-bottom:20px;
}
.color_prov{
text-decoration:line-through ;
color: red;
}
</style>
<?php  
function duration($begin,$end){  
    $remain=intval(strtotime($end)-strtotime($begin));  
    $wan=floor($remain/86400);  
    $l_wan=$remain%86400;  
    $hour=floor($l_wan/3600);  
    $l_hour=$l_wan%3600;  
    $minute=floor($l_hour/60);  
    $second=$l_hour%60; 
    $mun = str_pad($minute,2, "0", STR_PAD_LEFT);
    //return "ผ่านมาแล้ว ".$wan." วัน ".$hour." ชั่วโมง ".$minute." นาที ".str_pad($second ,2, "0", STR_PAD_LEFT)." วินาที";  
    return array($hour,$mun);

}
$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
?>  
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
<link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
 <link type="text/css" href="<?php echo base_url()?>assets/bootstrap/timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"/>
 <script type="text/javascript"  src="<?php echo base_url()?>assets/bootstrap/timepicker/js/bootstrap-timepicker.min.js"></script>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3><?php echo !empty($title) ? $title : "ข้อมูลการศึกษา"; ?></h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('report_work') ?>" method="post" id="searchForm" enctype="multipart/form-data">
                      <input type="hidden" name="page" id="page">

                      <input type="hidden" name="mode" id="mode" value="<?php echo $mode;?>">
                     
                <?php 
                if($mode=="view_late"){
                ?>
                  <div class="form-group">
                          <label class="control-label col-sm-3">รหัสพนักงาน:</label>
                          <div class="col-sm-9">
                            <input type="hidden" name="month" id="month" value="<?php echo $month;?>">
                             <input type="hidden" name="year" id="year" value="<?php echo $year-543;?>">
                             <label class="form-label" id="empID"><?=$ID;?></label>
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="control-label col-sm-3">ชื่อ-นามสกุล:</label>
                          <div class="col-sm-9">
                               <label class="form-label"><?=$name;?></label>
                          </div>
                       </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">ฝ่าย:</label>
                            <div class="col-sm-9">
                                 <label class="form-label"><?=$org1;?></label>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="control-label col-sm-3">แผนก:</label>
                            <div class="col-sm-9">
                                <label class="form-label"><?=$org2;?></label>
                               
                            </div>
                         </div>
                         <?php 
                            foreach ($th_month as $key => $val) {
                                if($key==$month){
                                    $result_month = $val;

                                }
                            }
                         ?>
                          <div class="form-group">
                            <label class="control-label col-sm-3">วันที่เริ่มต้น :</label>
                            <div class="col-sm-9">
                                <label class="form-label"><?php echo toFullDate($startDate); ?></label>
                               
                            </div>
                         </div>
                          <div class="form-group">
                            <label class="control-label col-sm-3">วันที่สิ้นสุด :</label>
                            <div class="col-sm-9">
                                <label class="form-label"><?php echo toFullDate($endDate); ?></label>
                               
                            </div>
                         </div>
                         
                <?php
                }else{
                  ?>
                      <div class="form-group">
                          <label class="control-label col-sm-3">วันที่</label>
                          <div class="col-sm-9">
                              <input class="form-control" type="text" data-type="date"  name="start_date" id="start_date" value="<?php echo toBEDate($startDate); ?>">
                               <input type="hidden"  name="start_date1" id="start_date1" value="<?php echo $startDate; ?>">
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="control-label col-sm-3">วันที่สิ้นสุด</label>
                          <div class="col-sm-9">
                              <input class="form-control" type="text" data-type="date"  name="end_date" id="end_date" value="<?php echo toBEDate($endDate); ?>">
                              <input type="hidden"  name="end_date1" id="end_date1" value="<?php echo $endDate; ?>">
                          </div>
                       </div>
                    		<div class="form-group">
                            <label class="control-label col-sm-3">หน่วยงาน</label>
                            <div class="col-sm-9">
                                <select class="xcrud-input form-control" data-type="select" name="orgID" id="orgID"> 
                                  <option value="">----กรุณาเลือก-----</option>
                                <?php 
                                    echo getDropdownTree('0','',$assignID,$orgID);
                                ?>
                                </select>
                            </div>
                         </div>

                         <div class="form-group">
                            <label class="control-label col-sm-3"> พนักงาน</label>
                            <div class="col-sm-9">
                               
                                <?php 
                                    echo getDropdown(listData('tbl_staff','staffIDCardOff','staffFName,staffLName'),'empID', $empID, "class='xcrud-input  form-control'");
                                ?>
                               
                            </div>
                         </div>
                          <div class="form-group">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-9">
                          
                                <input type="checkbox" name="problem" id="problem" value="1" <?php echo ('1'== $problem) ? "checked" : ""; ?>>แสดงเฉพาะรายงานที่มีปัญหา<br>
                               
                            </div>
                         </div>
                            
                            <div class="form-group">
                                  <button type="submit" id="search" class="btn btn-warning">ค้นหา</button>

                            </div>
              <?php }  ?>
                    </form>
                    
            </div>
         </section>
    </div>
</div> 

<a class="btn btn-danger"  href="javascript:<?php if($mode=="view_late"){ echo "print_excel1()";}else{ echo "print_excel()";}?>" style="float:right;"><i class="fa fa-table"></i> Excel </a>
    <a class="btn btn-success"  href="javascript:<?php if($mode=="view_late"){ echo "print_pdf1()";}else{ echo "print_pdf()";}?>;" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>
<style>
  .cal-head
  {
    background-color: #455A64;
    color: #FFF;
    width: 30px !important;
    padding:5px !important;
    font-size:8px;
    font-weight: bold;
  }

  .cal-norm
  {
    padding:5px !important;
    width: 30px !important;    
  }

  .cal-holiday
  {
    background-color: #FFCC80;
    padding:5px !important;
    width: 30px !important;      
  }
</style> 
<table class="xcrud-list table table-hover table-bordered">
  <thead>
    <tr class="xcrud-th">
        <th class="xcrud-num">#</th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="5%"> รหัสพนักงาน </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="5%"> ชื่อ-นามสกุล </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="20%"> วันที่ </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="10%"> เวลาเริ่มงาน </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="5%"><?php if($mode=="view_late"){ echo "มาสาย(นาที)"; }else{ echo "Approve By";  }?></th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="20%"> พักกลางวัน </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="5%"><?php if($mode=="view_late"){ echo "พักเกิน(นาที)"; }else{ echo "Approve By";  }?></th></th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="10%"> เวลาออกงาน </th>
        <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"  width="5%"> <?php if($mode=="view_late"){ echo "กลับก่อน(นาที)"; }else{ echo "Approve By";  }?></th></th>
    </tr>
  </thead>
  <tbody>
  <script>
    function setLate(id)
    {
      document.getElementById(id).style.backgroundColor = "#F00";
    }
  </script>
        <?php 
         $latestr = '';
         $num = $start+1 ;
         $tmp_id = '';
         if(!empty($r)){
            foreach ($r as $row) {
                $color_app1 ="";
                $color_app2 = "";
                $color_app3 = "";
                // ตรวจสอบว่าถ้าเวลาไม่เท่ากับค่าว่าง
                if($row->min_dated !="" && $row->max_dated !=""){
                   $scan_min = explode(" ",$row->min_dated);
                   $scan_max = explode(" ",$row->max_dated);
                   $time_in = explode(":",$scan_min[1]);
                   $time_out = explode(":",$scan_max[1]);
                   $date_app = toFullDate($scan_min[0]);
                  // check time afternoon
                  $afternoon = $this->staff_work_daily_model->getAfternoon($row->staffIDCardOff,$scan_min[0],$scan_min[1],$scan_max[1]);
                      
                        if($afternoon[0]=="-" && $afternoon[1]="-"){
                            //check mode from file link staffwork_late
                            if($mode=="view_late"){
                                    $checknoon_Approve ="";
                            }else{ 
                                    $checknoon_Approve = "<button type='button' class='btn btn-warning' 
                                      onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                      \"$date_app\",\"$scan_min[0]\",
                                              \"$afternoon[0]\",2,\"$afternoon[1]\")' >Approve</button>";
                            } 
                        }else{
                          $checknoon_Approve = "";
                        }
                        //ตรวจสอบชั่วโมง // check color เที่ยง
                        $hour = duration($afternoon[0],$afternoon[1]);
                        if($hour[0] <= 1){
                              if($hour[0]==1 && $hour[1] >str_pad(0,2, "0", STR_PAD_LEFT)){
                                  
                                    $class_color_af = "yellow";
                                    //check mode
                                    if($mode=='view_late'){
                                          $checknoon_Approve = "";
                             
                                    }else{
                                           $checknoon_Approve = "<button type='button' class='btn btn-warning' 
                                          onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                          \"$date_app\",\"$scan_min[0]\",
                                          \"$afternoon[0]\",2,\"$afternoon[1]\")'>Approve</button>";
                                    }
                              }else{
                                  $class_color_af = "";;
                              }
                        }else{
                            $class_color_af ="yellow";
                            if($mode == 'view_late'){
                                  $checknoon_Approve = "";
                            }else{
                                  $checknoon_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",\"$date_app\",\"$scan_min[0]\",\"$afternoon[0]\",2,\"$afternoon[1]\")'>Approve</button>";
                            }
                        }
                        if($afternoon[0]=="-" && $afternoon[1]="-"){
                              $class_color_af ="red1";
                        }
                        //check aprove 
                        if($row->time_late2_1 !=""){
                            $class_color_af = "";
                            $checknoon_Approve = "";
                            $color_app2 ="color_prov";
                        }
      // end check afternoon               


       //check เข้างาน
                        if(strtotime($scan_min[1])> strtotime("11:00:00")){
                              $check_in_re =  "ไม่ scan เข้างาน";
                              $class_color = "red1";    
                              //check mode
                              if($mode =="view_late"){
                                     $checkin_Approve ="";
                              }else{
                                     $checkin_Approve  = "<button type='button' class='btn btn-warning' 
                                    onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                    \"$date_app\",\"$scan_min[0]\",
                                    \"$check_in_re\",1,\"\")'>Approve</button>";
                              }
                        // if late
                        }else if(strtotime($scan_min[1]) > strtotime("8:00:00")){
                              //late === 8.30 - 8.30
                              if(strtotime($scan_min[1]) <= strtotime("8:30:00")){
                                $come_normal = "มาสาย";
                                $class_color = "yellow";
                             /// late === มากกว่า 8.30
                              }else{
                                $class_color = "yellow1";
                              }
                              $check_in_re = $scan_min[1];
                              /// check mode
                              if($mode =="view_late"){
                                   $checkin_Approve = round(abs(strtotime($scan_min[1]) - strtotime('8:00:00'))/60,2)." นาที";
                                       
                              }else{
                                      //Show late on calendar
                                      $latestr .= "setLate('".$row->staffIDCardOff."_".date('j',strtotime($row->min_dated))."_1');";
                                      $checkin_Approve ="<button type='button' class='btn btn-warning' 
                                        onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                        \"$date_app\",\"$scan_min[0]\",
                                  \"$check_in_re\",1,\"\")'>Approve</button>";
                              }
                        // normal
                        }else{
                               $come_normal = "มาตรงเวลา";
                               $class_color = ""; //green2";
                               $check_in_re = $scan_min[1];
                               $checkin_Approve = "";
                        }
                        //check aprove 
                        if($row->time_late1 !=""){
                            $class_color = "";
                            $checkin_Approve = "";
                            $color_app1 ="color_prov";
                        }
        // จบ check การเข้างาน

        // check ออกงาน
            
                        if(strtotime($scan_max[1])<=strtotime("14:00:00")){
                            $check_out_re = "ไม่ scan ออกงาน";
                            $class_color3 = "red1";
                            if($mode=="view_late"){
                                $checkout_Approve = "";
                            }else{
                                $checkout_Approve = "<button type='button' class='btn btn-warning' 
                                onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                \"$date_app\",\"$scan_min[0]\",
                              \"$check_out_re\",3,\"\")'>Approve</button>";
                            }
                        // if non-normal
                        }else if(strtotime($scan_max[1])<strtotime("17:00:00")){
                              //late === < 16.30 yellow
                              if(strtotime($scan_max[1]) < strtotime("16:30:00")){
                                  $class_color3 = "yellow";
                              }else{
                                  $class_color3 = "yellow1";
                              }
                             /// late === มากกว่า 8.30
                              $check_out_re = $scan_max[1];
                              if($mode=="view_late"){
                                  $checkout_Approve = round(abs(strtotime('17:00:00')-strtotime($scan_max[1]))/60,2)." นาที";
                           
                              }else{
                                  //Show late on calendar
                                  $latestr .= "setLate('".$row->staffIDCardOff."_".date('j',strtotime($row->max_dated))."_3');";                                
                                  $checkout_Approve = "<button type='button' class='btn btn-warning'
                                  onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                 \"$date_app\",\"$scan_min[0]\",
                              \"$check_out_re\",3,\"\")'>Approve</button>";
                              }
                        // if normal
                        }else{
                          $check_out_re = $scan_max[1];
                           $class_color3 = "";//green2";
                           $checkout_Approve = "";
                        }
                          // aprove
                        if($row->time_late3 !=""){
                           $checkout_Approve ="";
                           $class_color3 = "";
                           $color_app3 = "color_prov";
                        }
          // จบ check การออกงาน           
                    
                        // ปิดการตรวจสอบเวลา
                        $absence="";
                        // สีการลา
                        $class_color_absence ="";
            }else{
             
              $scan_min[0]=$startDate;
               // การลา
              $absence = $this->staff_work_daily_model->getAbsence($row->ID,$startDate);
              $status_absence = 0;
              if($absence[0] >='1'){
             
                  foreach ($absence[1] as $key => $val) {
                      //echo $val." ".$absence[0]."/".$startDate."<br>";
                      /// เวลาเปรียบเทียบลา
                      $date_query =date('Y-m-d', strtotime($val));
                      // เวลาดึงจากลิสต์
                      $date_list = date('Y-m-d', strtotime($startDate));
                      if($date_query == $date_list){
                        $class_color = "blue";
                        $class_color_af = "blue";
                        $class_color3 = "blue";

                        //btn apporve
                        $checkin_Approve = "";
                        $checknoon_Approve = "";
                        $checkout_Approve = "";

                          // สีการลา
                        $class_color_absence ="blue";

                        $status_absence =1;
                      }else{
                          if($status_absence !=1){
                             $status_absence = 2;
                             $class_color = "";
                             $class_color_af = "";
                             $class_color3 = ""; 

                             // btn apporve
                             $checkin_Approve = "<button type='button' class='btn btn-warning'>Approve1</button>";
                             $checknoon_Approve = "<button type='button' class='btn btn-warning'>Approve1</button>";
                             $checkout_Approve = "<button type='button' class='btn btn-warning'>Approve1</button>";
                             // สีการลา
                            $class_color_absence ="";
                          }
                       
                      }

                  }
              }else if($absence[0]<='1'){
                 $class_color = "";
                 $class_color_af = "";
                 $class_color3 = ""; 
                $show_date = toFullDate($scan_min[0]);
                 // btn apporve
                 $checkin_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",\"$show_date\",\"$scan_min[0]\",
                              \"$check_out_re\",1,\"\")'>Approve2</button>";
                 $checknoon_Approve = "<button type='button' class='btn btn-warning'>Approve2</button>";
                 $checkout_Approve = "<button type='button' class='btn btn-warning'>Approve2</button>";
                 // สีการลา
                  $class_color_absence ="";

              }
              //checking
              $check_in_re = "";
              
              //$checkin_Approve = "";

              //afternoon
              $afternoon[0] = "";
              $afternoon[1] = "";

              //check out
              $check_out_re = "";
              // สีการลา

            }
              ?>
          
                <tr class="xcrud-row xcrud-row-0 <?php echo $class_color_absence;?>" > 
                    <td class="xcrud-current xcrud-num"><?php echo $num;?></td>
                    <td ><?php echo $row->staffIDCardOff;?></td> 
                    <td style="white-space: nowrap;"><?php echo $row->staffFName." ".$row->staffLName;?></td>
                    <td style="white-space: nowrap;"><?php echo toFullDate($scan_min[0]);?></td>
                    <td class="<?=$class_color;?>"><p class="<?=$color_app1;?>"><?php echo $check_in_re;?></p><?php echo (!empty($row->time_late1)) ? "<BR>".$row->time_late1 : "";?></td>
                    <td><?php echo $checkin_Approve;?></td>
                    <td style="white-space: nowrap;" class="<?=$class_color_af;?>"><p class="<?=$color_app2;?>"><?php echo $afternoon[0]."-".$afternoon[1];?></p><?php echo (!empty($row->time_late2_1)) ? "<BR>".$row->time_late2_1."-".$row->time_late2_2 : "";?></td>
                    <td><?php echo $checknoon_Approve; ?></td>
                    <td class="<?=$class_color3;?>"><p class="<?=$color_app3;?>"><?php echo $check_out_re;?></p><?php echo (!empty($row->time_late3)) ? "<BR>".$row->time_late3 : "";?></td>
                    <td><?php echo $checkout_Approve; ?></td>

                 </tr>
           <?php $num++; 
               
              }
            }else{
         ?>    

            <tr class="xcrud-row xcrud-row-0 green1"> 
                
                  <td colspan="10" style="text-align:center;">ไม่พบข้อมูล</td>
                  
            </tr>
           
        <?php } ?>          
       </tbody>
</table>
<input type="button" title="SetLate" onclick="<?php echo $latestr?>">
<table class="xcrud-list table table-hover table-bordered">
    <thead>
        <tr class="xcrud-th">
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="5%"> รหัสพนักงาน </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID" width="5%"> ชื่อ-นามสกุล </th>
            <?php 
              $year = date("Y");
              $month = date("m");
              $number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
              for($i=0;$i<$number;$i++){
                $d = $i +1 ;
                echo '<th class="cal-head">'.$d.'</th>';
              }
            ?>
         </tr>   
    </thead>
    <tbody>
        <?php 
         $num = $start+1 ;
         $tmp_id = '';
         if(!empty($rc)){
            foreach ($rc as $row) {
                $color_app1 ="";
                $color_app2 = "";
                $color_app3 = "";
                // ตรวจสอบว่าถ้าเวลาไม่เท่ากับค่าว่าง
                if($row->min_dated !="" && $row->max_dated !=""){
                   $scan_min = explode(" ",$row->min_dated);
                   $scan_max = explode(" ",$row->max_dated);
                   $time_in = explode(":",$scan_min[1]);
                   $time_out = explode(":",$scan_max[1]);
                   $date_app = toFullDate($scan_min[0]);
                  // check time afternoon
                  $afternoon = $this->staff_work_daily_model->getAfternoon($row->staffIDCardOff,$scan_min[0],$scan_min[1],$scan_max[1]);
                      
                        if($afternoon[0]=="-" && $afternoon[1]="-"){
                            //check mode from file link staffwork_late
                            if($mode=="view_late"){
                                    $checknoon_Approve ="";
                            }else{ 
                                    $checknoon_Approve = "<button type='button' class='btn btn-warning' 
                                      onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                      \"$date_app\",\"$scan_min[0]\",
                                              \"$afternoon[0]\",2,\"$afternoon[1]\")' >Approve</button>";
                            } 
                        }else{
                          $checknoon_Approve = "";
                        }
                        //ตรวจสอบชั่วโมง // check color เที่ยง
                        $hour = duration($afternoon[0],$afternoon[1]);
                        if($hour[0] <= 1){
                              if($hour[0]==1 && $hour[1] >str_pad(0,2, "0", STR_PAD_LEFT)){
                                  
                                    $class_color_af = "yellow";
                                    //check mode
                                    if($mode=='view_late'){
                                          $checknoon_Approve = "";
                             
                                    }else{
                                           $checknoon_Approve = "<button type='button' class='btn btn-warning' 
                                          onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                          \"$date_app\",\"$scan_min[0]\",
                                          \"$afternoon[0]\",2,\"$afternoon[1]\")'>Approve</button>";
                                    }
                              }else{
                                  $class_color_af = "";;
                              }
                        }else{
                            $class_color_af ="yellow";
                            if($mode == 'view_late'){
                                  $checknoon_Approve = "";
                            }else{
                                  $checknoon_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",\"$date_app\",\"$scan_min[0]\",\"$afternoon[0]\",2,\"$afternoon[1]\")'>Approve</button>";
                            }
                        }
                        if($afternoon[0]=="-" && $afternoon[1]="-"){
                              $class_color_af ="red1";
                        }
                        //check aprove 
                        if($row->time_late2_1 !=""){
                            $class_color_af = "";
                            $checknoon_Approve = "";
                            $color_app2 ="color_prov";
                        }
      // end check afternoon               


       //check เข้างาน
                        if(strtotime($scan_min[1])> strtotime("11:00:00")){
                              $check_in_re =  "ไม่ scan เข้างาน";
                              $class_color = "red1";    
                              //check mode
                              if($mode =="view_late"){
                                     $checkin_Approve ="";
                              }else{
                                     $checkin_Approve  = "<button type='button' class='btn btn-warning' 
                                    onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                    \"$date_app\",\"$scan_min[0]\",
                                    \"$check_in_re\",1,\"\")'>Approve</button>";
                              }
                        // if late
                        }else if(strtotime($scan_min[1]) > strtotime("8:00:00")){
                              //late === 8.30 - 8.30
                              if(strtotime($scan_min[1]) <= strtotime("8:30:00")){
                                $come_normal = "มาสาย";
                                $class_color = "yellow";
                             /// late === มากกว่า 8.30
                              }else{
                                $class_color = "yellow1";
                              }
                              $check_in_re = $scan_min[1];
                              /// check mode
                              if($mode =="view_late"){
                                   $checkin_Approve = round(abs(strtotime($scan_min[1]) - strtotime('8:00:00'))/60,2)." นาที";
                                       
                              }else{
                                      $checkin_Approve ="<button type='button' class='btn btn-warning' 
                                        onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                        \"$date_app\",\"$scan_min[0]\",
                                  \"$check_in_re\",1,\"\")'>Approve</button>";
                              }
                        // normal
                        }else{
                               $come_normal = "มาตรงเวลา";
                               $class_color = ""; //green2";
                               $check_in_re = $scan_min[1];
                               $checkin_Approve = "";
                        }
                        //check aprove 
                        if($row->time_late1 !=""){
                            $class_color = "";
                            $checkin_Approve = "";
                            $color_app1 ="color_prov";
                        }
        // จบ check การเข้างาน

        // check ออกงาน
            
                        if(strtotime($scan_max[1])<=strtotime("14:00:00")){
                            $check_out_re = "ไม่ scan ออกงาน";
                            $class_color3 = "red1";
                            if($mode=="view_late"){
                                $checkout_Approve = "";
                            }else{
                                $checkout_Approve = "<button type='button' class='btn btn-warning' 
                                onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                \"$date_app\",\"$scan_min[0]\",
                              \"$check_out_re\",3,\"\")'>Approve</button>";
                            }
                        // if non-normal
                        }else if(strtotime($scan_max[1])<strtotime("17:00:00")){
                              //late === < 16.30 yellow
                              if(strtotime($scan_max[1]) < strtotime("16:30:00")){
                                  $class_color3 = "yellow";
                              }else{
                                  $class_color3 = "yellow1";
                              }
                             /// late === มากกว่า 8.30
                              $check_out_re = $scan_max[1];
                              if($mode=="view_late"){
                                  $checkout_Approve = round(abs(strtotime('17:00:00')-strtotime($scan_max[1]))/60,2)." นาที";
                           
                              }else{
                                  $checkout_Approve = "<button type='button' class='btn btn-warning'
                                 onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",
                                 \"$date_app\",\"$scan_min[0]\",
                              \"$check_out_re\",3,\"\")'>Approve</button>";
                              }
                        // if normal
                        }else{
                          $check_out_re = $scan_max[1];
                           $class_color3 = "";//green2";
                           $checkout_Approve = "";
                        }
                          // aprove
                        if($row->time_late3 !=""){
                           $checkout_Approve ="";
                           $class_color3 = "";
                           $color_app3 = "color_prov";
                        }
          // จบ check การออกงาน           
                    
                        // ปิดการตรวจสอบเวลา
                        $absence="";
                        // สีการลา
                        $class_color_absence ="";
            }else{
             
              $scan_min[0]=$startDate;
               // การลา
              $absence = $this->staff_work_daily_model->getAbsence($row->ID,$startDate);
              $status_absence = 0;
              if($absence[0] >='1'){
             
                  foreach ($absence[1] as $key => $val) {
                      //echo $val." ".$absence[0]."/".$startDate."<br>";
                      /// เวลาเปรียบเทียบลา
                      $date_query =date('Y-m-d', strtotime($val));
                      // เวลาดึงจากลิสต์
                      $date_list = date('Y-m-d', strtotime($startDate));
                      if($date_query == $date_list){
                        $class_color = "blue";
                        $class_color_af = "blue";
                        $class_color3 = "blue";

                        //btn apporve
                        $checkin_Approve = "";
                        $checknoon_Approve = "";
                        $checkout_Approve = "";

                          // สีการลา
                        $class_color_absence ="blue";

                        $status_absence =1;
                      }else{
                          if($status_absence !=1){
                             $status_absence = 2;
                             $class_color = "";
                             $class_color_af = "";
                             $class_color3 = ""; 

                             // btn apporve
                             $checkin_Approve = "<button type='button' class='btn btn-warning'>Approve1</button>";
                             $checknoon_Approve = "<button type='button' class='btn btn-warning'>Approve1</button>";
                             $checkout_Approve = "<button type='button' class='btn btn-warning'>Approve1</button>";
                             // สีการลา
                            $class_color_absence ="";
                          }
                       
                      }

                  }
              }else if($absence[0]<='1'){
                 $class_color = "";
                 $class_color_af = "";
                 $class_color3 = ""; 
                $show_date = toFullDate($scan_min[0]);
                 // btn apporve
                 $checkin_Approve = "<button type='button' class='btn btn-warning' onclick='openModal($row->staffIDCardOff,\"$row->staffFName\",\"$row->staffLName\",\"$show_date\",\"$scan_min[0]\",
                              \"$check_out_re\",1,\"\")'>Approve2</button>";
                 $checknoon_Approve = "<button type='button' class='btn btn-warning'>Approve2</button>";
                 $checkout_Approve = "<button type='button' class='btn btn-warning'>Approve2</button>";
                 // สีการลา
                  $class_color_absence ="";

              }
              //checking
              $check_in_re = "";
              
              //$checkin_Approve = "";

              //afternoon
              $afternoon[0] = "";
              $afternoon[1] = "";

              //check out
              $check_out_re = "";
              // สีการลา

            }
              ?>
          
                <tr class="xcrud-row xcrud-row-0 <?php echo $class_color_absence;?>" > 
                    <!-- <td class="xcrud-current xcrud-num"><?php echo $num;?></td> -->
                    <td rowspan="3"><?php echo $row->staffIDCardOff;?></td> 
                    <td rowspan="3" style="white-space: nowrap;"><?php echo $row->staffFName." ".$row->staffLName;?></td>
                    <!-- <td style="white-space: nowrap;"><?php echo toFullDate($scan_min[0]);?></td>
                    <td class="<?=$class_color;?>"><p class="<?=$color_app1;?>"><?php echo $check_in_re;?></p><?php echo (!empty($row->time_late1)) ? "<BR>".$row->time_late1 : "";?></td>
                    <td><?php echo $checkin_Approve;?></td>
                    <td style="white-space: nowrap;" class="<?=$class_color_af;?>"><p class="<?=$color_app2;?>"><?php echo $afternoon[0]."-".$afternoon[1];?></p><?php echo (!empty($row->time_late2_1)) ? "<BR>".$row->time_late2_1."-".$row->time_late2_2 : "";?></td>
                    <td><?php echo $checknoon_Approve; ?></td>
                    <td class="<?=$class_color3;?>"><p class="<?=$color_app3;?>"><?php echo $check_out_re;?></p><?php echo (!empty($row->time_late3)) ? "<BR>".$row->time_late3 : "";?></td>
                    <td><?php echo $checkout_Approve; ?></td> -->
                    <?php 


                    $holiday = $this->staff_work_daily_model->getSundays($year,$month);

                      $year = date("Y");
                      $month = date("m");
                      $number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                      for($i=1;$i<=$number;$i++){
                          $clsstr = (in_array($i, $holiday))? 'cal-holiday':'calc-norm'; 
                          echo '<td class="'.$clsstr.'" id="'.$row->staffIDCardOff."_".$i.'_1"></td>';
                      }
                      echo '</tr><tr>';
                      for($i=1;$i<=$number;$i++){
                          $clsstr = (in_array($i, $holiday))? 'cal-holiday':'calc-norm'; 
                          echo '<td class="'.$clsstr.'" id="'.$row->staffIDCardOff."_".$i.'_2"></td>';
                      }
                      echo '</tr><tr>';
                      for($i=1;$i<=$number;$i++){
                          $clsstr = (in_array($i, $holiday))? 'cal-holiday':'calc-norm';                           
                          echo '<td class="'.$clsstr.'" id="'.$row->staffIDCardOff."_".$i.'_3"></td>';
                      }                                            
                    ?>
                 </tr>
           <?php $num++; 
               
              }
            }else{
         ?>	   

            <tr class="xcrud-row xcrud-row-0 green1"> 
                
                  <td colspan="10" style="text-align:center;">ไม่พบข้อมูล</td>
                  
            </tr>
           
        <?php } ?>          
       </tbody>
    <tfoot>
   </tfoot>
</table>
  
  <?php echo $viewpage; ?>

  <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Approve</h4>
      </div>
      <div class="modal-body">
        

          <form action="<?php echo base_url('report_work/submitApprove') ?>" method="post" enctype="multipart/form-data" id="approveForm" class="form-horizontal" role="form">
              <input type='hidden' name="staff_id" id="staff_id">
              <input type='hidden' name="daily_date" id="daily_date">
              <input type='hidden' name="time_in" id="time_in">
              <input type="hidden" name="period_type" id="period_type">

              <input type="hidden" name="start_date_pop" id="start_date_pop">
              <input type="hidden" name="end_date_pop" id="end_date_pop">
              <input type="hidden" name="orgID_pop" id="orgID_pop">
              <input type="hidden" name="empID_pop" id="empID_pop">
              <input type="hidden" name="problem_pop" id="problem_pop">
              <input type="hidden" name="pagination" id="pagination">
                
                    <div class="form-group">
                      <label class="control-label col-sm-3">พนักงาน</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" readonly="readonly" value="" name="staff" id="staff">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">วันที่</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="date" value="" readonly="readonly"  name="date" id="date">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">เวลาทำงาน</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="time" value="" readonly="readonly"  name="time" id="time">
                         </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-sm-3">รายละเอียด</label>
                        <div class="col-sm-9">
                          <textarea name="detail" id="detail" class="form-control"></textarea>
                         </div>
                    </div>

                    <!-- <div class="form-group">
                      <label class="control-label col-sm-3">จำนวนนาทีที่หักลบ(นาที)</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="text" data-type="" value="" onkeypress="return isNumberKey(event)" name="minute" id="minute">
                         </div>
                    </div> -->

                    <div class="form-group">
                        <label class="control-label col-sm-3">เวลาที่เปลี่ยนใหม่</label>
                        <div class="col-sm-4">
                            <div class="input-group bootstrap-timepicker timepicker datepicker">
                              <input id="minute" name="minute" type="text" class="form-control input-small ">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div> 
                        </div>
                        <div class="col-sm-4" id="hiddinTime2">
                              <div class="input-group bootstrap-timepicker timepicker datepicker">
                                <input id="minute2" name="minute2" type="text" class="form-control input-small ">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                              </div> 
                        </div>
                    </div>
                    
              </form>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="submitForm()" data-dismiss="modal">บันทึก</button>
      </div>
    </div>

  </div>
</div>
<style type="text/css">
.datepicker{z-index:1151 !important;}
</style>
<script type="text/javascript">
/* function time_pic(id){
  if(id==1){
    $('#minute').timepicker({
          showMeridian : false,
          defaultTime: '08:00'  
    });
  }else if(id==2){
    $('#minute').timepicker({
          showMeridian : false,
          defaultTime: '17:00'  
    });
  }
}
*/
  $('#minute').timepicker({
          showMeridian : false,
          defaultTime: '08:00'  
    });
  $('#minute2').timepicker({
          showMeridian : false,
          defaultTime: '13:00'  
    });
</script>

<script type="text/javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
<script type="text/javascript"> 

    

    function openModal(staff_id,staff_name,staff_lastname,date_in,date_in_db,time_in,period,time_out){
      //var staffID = staff_id.toString();
      var staffID =staff_id.toString();
      var staffName =staff_name;
      var date= date_in.toString() ;
      var time_out_chek;

      /// status coming monirng afternoon evening
      var period_type = period;
      if(time_out===''){
        time_out_chek = ""; 
      }else{
        time_out_chek = "-"+time_out.toString();
      }
      var time = time_in+time_out_chek;
      var staffLastname = staff_lastname;
      var name = staffID+"-".concat(staffName)+"   "+staffLastname;
      $('#staff').val(name);
      $('#date').val(date);
      $('#time').val(time);
      //hidden
      $('#staff_id').val(staff_id);
       $('#daily_date').val(date_in_db);
       $('#time_in').val(time_in);
       $('#period_type').val(period_type);
       
       $('#start_date_pop').val($('#start_date').val());
       $('#end_date_pop').val($('#end_date').val());
       $('#orgID_pop').val($('#orgID').val());
       $('#empID_pop').val($('#empID').val());
       $('#pagination').val($('#page_selected').html());
       if($('#problem').is(':checked')){
        $('#problem_pop').val($('#problem').val());
       }
       if(period_type==1) {
        $('#minute').val('08:00');  
        $('#hiddinTime2').hide();     
       } 
       else if(period_type==2) {
          $('#minute').val('12:00'); 

       }
       else {
          $('#minute').val('17:00');
          $('#hiddinTime2').hide();                        
       }
      //$('#daily_date').val(daily_date);
      //alert(staff_id);
      $('#myModal').modal('show');

    }

    /*function submitForm(){
      $('#staffForm').submit();
    }*/
    
	

    $(document).ready(function () {
     var dateBefore=null;  
      $("#start_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      });
      
      $("#end_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      }); 

     
   });

   function submitForm(){
    $('#approveForm').submit();
   }

   function getPaging(pageID){
      $('#page').val(pageID);
      $('#searchForm').submit();
   }
    function print_pdf(){
          window.open('<?php echo site_url();?>report_work/print_pdf?start_date='+$('#start_date1').val()+'&end_date='+$('#end_date1').val()+'&orgID='+$('#orgID').val()+'&empID='+$('#empID').val()+'&problem='+$('#problem:checked').val()+'&page='+$('#page_selected').text());
        }

        function print_excel(){
          window.open('<?php echo site_url();?>report_work/print_excel?start_date='+$('#start_date1').val()+'&end_date='+$('#end_date1').val()+'&orgID='+$('#orgID').val()+'&empID='+$('#empID').val()+'&problem='+$('#problem:checked').val()+'&page='+$('#page_selected').text());
        }
   
  
</script>
<script type="text/javascript">
     function print_pdf1(){
          window.open('<?php echo site_url();?>report_work/print_pdf?empID='+$('#empID').text()+'&month='+$('#month').val()+'&year='+$('#year').val()+'&page='+$('#page_selected').text()+'&mode=view_late');
        }

</script>
<script type="text/javascript">
  function print_excel1(){
           window.open('<?php echo site_url();?>report_work/print_excel?empID='+$('#empID').text()+'&month='+$('#month').val()+'&year='+$('#year').val()+'&page='+$('#page_selected').text()+'&mode=view_late');
        }
</script>
 