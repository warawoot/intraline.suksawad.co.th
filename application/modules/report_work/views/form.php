<style type="text/css">
	.form-control {
 	  color: #343232;
	}

</style>
<style>
  #add_trainee{
    position:absolute;
    top:140px;
    left:80px;
  }
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3><?php echo !empty($title) ? $title : "ข้อมูลการศึกษา"; ?></h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('traineecourse') ?>" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">หลักสูตร</label>
                                <div class="col-sm-9">
                                    <?php 
                                        echo getDropdown(listData("tbl_train_course","tcID","tcName"),"tcID", $tcID, 'class="xcrud-input form-control"');
                                    ?>
                                </div>
                             </div>
                             <div class="form-group">
                              <label class="control-label col-sm-3">รุ่นที่</label>
                              	<div class="col-sm-9">
                                <?php if($tcID ==''){?>
                                	
                                         <select name="noID" id="noID" class="form-control">
                                            <option value="" selected="selected">-- กรุณาเลือก --</option>
                                         </select>
                                    
                                    <?php
									}else{
                                    	 echo getDropdown(listDataSql("SELECT * FROM tbl_course_no WHERE tcID = '$tcID'","noID","courseNo"),"noID", $noID, 'class="xcrud-input form-control"');
                                    
									}
									?>
                                 </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-warning">ค้นหา</button>
                            </div>
                    </form>
                    
            </div>
         </section>
    </div>
</div> 
<br /><br />
<table class="xcrud-list table table-striped table-hover table-bordered">
    <thead>
        <tr class="xcrud-th">
            <th class="xcrud-num">#</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.staffID"> รหัส </th>
            <th data-order="asc" data-orderby="tbl_staff.staffFName" class="xcrud-column xcrud-action">ชื่อ - สกุล</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> ฝ่าย </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> กอง </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> แผนก </th>
         </tr>   
    </thead>
    <tbody>
        <?php 
         $num = 1 ;
         
         if($r != NULL){
            foreach ($r->result() as $row) {
                # code...
      				 //$orgID				=	$item->orgID;
      				 //$completeDate		=	$item->completeDate;
      				 //$recent_submit		=	$this->recent_model->get_recent_submit($recent_year,$recent_evalRound,$orgID);
       				 
              ?>
            <tr class="xcrud-row xcrud-row-0" > 
                <td class="xcrud-current xcrud-num"><?php echo $num;?></td>
                <td> <?php echo $row->staffID; ?>   </td>
                <td><?php echo $row->staffFName.' '.$row->staffLName; ?></td>
                <td></td>
                <td></td>
                <td></td>
             </tr>
         <?php $num++; }
            }else{
         ?>	   
            <tr class="xcrud-row xcrud-row-0"> 
                <td colspan="6" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
            </tr>
        <?php } ?>          
       </tbody>
    <tfoot>
   </tfoot>
</table>
  
<script type="text/javascript"> 

    $('#tcID').change(function(){
        $.ajax({
                url: "<?php echo base_url('traineecourse/courseNoDdl') ?>",
                type: 'POST',
                data: {
                        tc_id: $('#tcID :selected').val()
                },
                success: function(response) {
                    //Do Something 
                   var obj = jQuery.parseJSON(response); 
                   //alert(response);
                   //console.log(obj); 
                    $("#noID").html(obj);
                    //alert(obj.assignID);
                    //var dp1433735797502 = $("#dp1433735797502").val('');
                },
                error: function(xhr) {
                    //Do Something to handle error
                    //alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
                    location.replace('<?php echo base_url().'traineecourse' ?>');
                }
        }); 
    })
	/*function changeCourse(obj){
    	//alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('traineecourse/courseNoDdl') ?>",
				type: 'POST',
				data: {
						tc_id: x 
				},
				success: function(response) {
					//Do Something 
				   var obj = jQuery.parseJSON(response); 
				   //alert(response);
				   //console.log(obj); 
					$("#noID").html(obj);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
				    location.replace('<?php echo base_url().'traineecourse' ?>');
				}
		});	
  	}*/
  
</script>
 