<?php

class Staffname extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		
		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));


		$xcrud->table('tbl_staff_name')->where('staffID =',$id);
		

		
		//// List /////
		$col_name = array(
			'dateChange' => 'วันที่เปลี่ยน',
			'staffPreName' => 'คำนำหน้าชื่อ',
			'staffFName' => 'ชื่อ',
			'staffLName' => 'นามสกุล',
			'staffPreNameEN'=>'Pre',
			'staffFNameEN'=>'Name',
			'staffLNameEN'=>'Surename',
			'staffPreNameEtc'=>'อื่นๆ',
			'staffPreNameENEtc'=>'ETC'
			

		);

		$xcrud->pass_var('staffID',$id);
		
		
		$xcrud->columns('dateChange,staffFName,staffFNameEN');
		$xcrud->label($col_name);
		$xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
		$xcrud->column_pattern('staffFNameEN','{staffPreNameEN} {value} {staffLNameEN}');
		$xcrud->column_callback("dateChange","toBDDate");

		//// Form //////

		$xcrud->fields('dateChange,staffPreName,staffPreNameEtc,staffFName,staffLName,staffPreNameEN,staffPreNameENEtc,staffFNameEN,staffLNameEN');
		$xcrud->change_type('staffPreName', 'select', '', array('นาย'=>'นาย','นาง'=>'นาง','นางสาว'=>'นางสาว','อื่นๆ'=>'อื่นๆ'));
		$xcrud->change_type('staffPreNameEN', 'select', '', array('Mr.'=>'Mr.','Mrs.'=>'Mrs.','Miss.'=>'Miss','Etc.'=>'Etc'));
		
		
		$xcrud->validation_required('dateChange,staffPreName,staffFName,staffLName');

		
		
		$data['html'] = $xcrud->render();
		$data['title'] = "ประวัติการเปลี่ยนชื่อ";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file staffname.php */
/* Location: ./application/module/staffname/staffname.php */