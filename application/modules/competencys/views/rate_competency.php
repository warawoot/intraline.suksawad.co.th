<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3><?php echo $title;?></h3>
            </header>
            <div class="panel-body">
                <?php echo $data; ?>
            </div>
        </section>
    </div>
</div>
 
 