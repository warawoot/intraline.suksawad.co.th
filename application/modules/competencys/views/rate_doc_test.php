<style type="text/css">
	.form-control {
 	  color: #343232;
	}
  
  .table-evaluation tr td{
    padding:7px !important;
    vertical-align: middle !important;
  }
  .table-evaluation thead th{
  border-top: none; 
  border-bottom: none;
  border: 0 !important; 
  }  
    input[type=number] {
      float: left;
      width: 70px;
      height: 35px;
      padding: 0;
      font-size: 1.2em;
      text-transform: uppercase;
      text-align: center;
      color: #93504C;
      border: 2px #93504C solid;
      background: none;
      outline: none;
      pointer-events: none;
    }
    
    span.spinner {
      position: absolute;
      height: 40px;
      margin-top: -9px;
      user-select: none;
      -ms-user-select: none;
      -moz-user-select: none;
      -webkit-user-select: none;
      -webkit-touch-callout: none;
    }
    
    span.spinner > .sub,
    span.spinner > .add {
      float: left;
      display: block;
      width: 35px;
      height: 35px;
      text-align: center;
      font-family: Lato;
      font-weight: 700;
      font-size: 1.2em;
      line-height: 33px;
      color: #93504C;
      border: 2px #93504C solid;
      border-right: 0;
      border-radius: 2px 0 0 2px;
      cursor: pointer;
      transition: 0.1s linear;
      -o-transition: 0.1s linear;
      -ms-transition: 0.1s linear;
      -moz-transition: 0.1s linear;
      -webkit-transition: 0.1s linear;
    }
    
    span.spinner > .add {
      top: 0;
      border: 2px #93504C solid;
      border-left: 0;
      border-radius: 0 2px 2px 0;
    }
    
    span.spinner > .sub:hover,
    span.spinner > .add:hover {
      background: #93504C;
      color: #25323B;
    }
     input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
     -webkit-appearance: none;
    }

</style>
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading"> 
          <h3>ข้อมูลการประเมินผล</h3>
      </header>
      <div class="panel-body">
        <form  id="myForm" method="post" action="<?php echo base_url() ?>rate/docs" enctype="multipart/form-data">
          <table class="table table-striped" style="background-color: #fff; font-size:14px;">
            <tr>
              <td>
                <table width="100%" border="0">
                  <tr>
                    <td colspan="7" align="center" style=" font-size:16px;">
                      <strong> แบบประเมินผลการปฏิบัติงานประจำปี </strong> 
						          <?php echo $get_evalYear;?> <strong> ครั้งที่ </strong>
						          <?php echo $get_evalRound;?> ( <strong>วันที่</strong> <?php echo $get_startDate;?> - <?php echo $get_endDate;?>) 
                      <input name="evalType" id="evalType" type="hidden" value="1" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7" align="center"><strong> [ </strong><?php echo $get_evalNameText;?><strong> ] </strong></td>
                  </tr>
                  <tr>
                    <td><strong>ชื่อ-นามสกุล</strong> <?php  echo $get_staffPreName . $get_staffFName .'&nbsp;&nbsp;'. $get_staffLName;?></td>
                    <td></td>
                    <td>&nbsp;</td>
                    <td><strong>ตำแหน่ง<?php echo $get_positionName;?></strong></td>
                    <td></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php
                  		 $party		=	$this->rate_model->structural_work($var_agencies,'party',6);
						 
						 $staffIDz	=	$this->rate_model->structural_staff($staffID);
						 //echo "<pre>";
						//	print_r($staffIDz);
						 $department		=	$this->rate_model->structural_work($staffIDz['orgID'],'department',6);
 						 $pile		=	$this->rate_model->structural_work($department['upperOrgID'],'pile',6);
				  ?>
                  <tr>
                    <td><strong><!--แผนก-->
                    <?php echo $department['orgName']; ?></strong></td>
                    <td>&nbsp; </td>
                    <td>&nbsp;</td>
                    <td><strong><!--กอง-->
                    <?php echo $pile['orgName']; ?></strong></td>
                    <td>&nbsp;</td>
                    <td><strong><!--ฝ่าย--></strong></td>
                    <td>&nbsp;<strong><?php echo $party['orgName']; ?></strong></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" class="table table-condensed table-striped table-evaluation">
                  <thead>
                  <tr>
                    <th colspan="2"><strong>โปรดวงกลมล้อมรอบตัวเลขคะแนนที่ประเมินในแต่ละปัจจัย</strong></th>
                    <th colspan="1"><strong>ระดับคะแนนที่ให้</strong></th>
                  </tr>
                  <tr>
                    <th colspan="2"><strong>หัวข้อปัจจัยที่ประเมิน</strong></th>
                    <th colspan="1"><strong>คะแนน</strong>&nbsp;</th>
                  </tr>
                  </thead>
                  <tbody>
              <?php 
              if(is_array($get_eval_group_subject)){ 
					      $num = 1;
                foreach ($get_eval_group_subject as $key => $value) { # code...  tbl_eval_group_subject
							     $nums	=	$num++;
							     $sum_eval_subject = $this->rate_model->get_sum_eval_subject($value->evalGroupSubjectID);# code...  tbl_eval_subject
						  ?>
                  <tr>
                    <th colspan="2"><strong><?php echo $value->evalGroupSubjectText;?></strong></th>
                    <th><strong>น้ำหนักของปัจจัยที่ <?php echo $nums;?> = [ </strong><?php echo $sum_eval_subject['maxScore']; ?><strong> ] คะแนน</strong>&nbsp;</th>
                  </tr>
              <?php 
							$get_apen_name = $this->rate_model->get_eval_subject($value->evalGroupSubjectID);
							//echo "<pre>";
							//print_r($get_apen_name);
               				$num_score	=	1;
							  foreach ($get_apen_name as $key => $item) {  
							   $ddl = score_max_min_.$item->evalSubjectID ;
							   // echo $ddl;
							   $get_eval_nedit	=	$this->rate_model->get_eval_edit($staffID,$get_evalFormID,$item->evalSubjectID);
							    
                 # code...
             ?>
                  <input name="ddl[]" id="ddl[]" type="hidden" value="<?php echo $ddl;?>" />
                  <tr style="font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item->evalSubjectText; ?></td><!--onChange="changeTest_<?php //echo $item->evalSubjectID; ?>(this)"-->
                  <td>&nbsp;</td>
                  <td >
                    <table width="50%" border="0">
                      <tr>
                        <td><input type="number" min="<?php echo $item->minScore;?>" max="<?php echo $item->maxScore;?>" value="<?php echo ($get_eval_nedit['evalScore'] !=''?$get_eval_nedit['evalScore']:0);?>"  name="score_max_min_<?php echo $item->evalSubjectID; ?>" id="score_max_min_<?php echo $item->evalSubjectID; ?>"   onChange="changeTest_<?php echo $item->evalSubjectID; ?>(this)"/></td>
                      </tr>
                    </table> 
                    <table width="50%" border="0" style="float:right;text-align:right;margin-top:-14px;">
                    	<tr>
                       	<td> ( <?php echo $item->minScore;?> - <?php echo $item->maxScore;?> ) </td>
                      </tr>
                 	  </table>
           				  <div class="list_score">
                      <span id="list_score_<?php echo $nums;?>_<?php echo $item->evalSubjectID; ?>"></span>
                    </div>
                    <div style="display:none">
                     	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="<?php echo $item->evalSubjectID;?>" />
                    </div>
                  </td>
                </tr>
              <?php     
                  }
       			   ?>
              <div class="list_count">
                <span id="list_count_<?php echo $nums;?>"></span>
              </div>
              <span id="list_count_test_<?php echo $nums;?>"></span>
                
           		<?php
                }
              ?>  
              <div class="score_max" style="display:none;">
              	<span id="get_score_max"><?php echo $nums;?></span>
                  
              </div>
                        
              <?php }else{
              ?>
              <tr>
                <td colspan="3" align="center">ไม่พบข้อมูล</td>
              </tr>
              <?php  }   ?>

            </table></td>
          </tr>
          <tr>
            <td align="right">
              <div class="ge_score_test"><h4>รวมคะแนนประเมิน = <span id="totalScore"></span> คะแนน</h4></div>
            </td>
          </tr>                   
          <tr>
            <td align="right">
            <!--<table width="100%" border="0">
              <tbody><tr>
                <td><strong>จำนวนวันลา &nbsp;&nbsp;</strong></td>
                <td>?? ลาป่วย 12 วัน / ลากิจ 3 วัน / ขาดงาน 1 วัน</td>
              </tr>
              <tr>
                <td><strong>การลงโทษทางวินัย</strong></td>
                <td>?? ไม่มี</td>
              </tr>
              <tr>
                <td><strong>การลาศึกษาต่อ</strong></td>
                <td>?? ไม่มี</td>
              </tr>
              <tr>
                <td><strong>การอบรม- สัมมนา</strong></td>
                <td>?? 1) อบรมการใช้ระบบสารสนเทศสำหรับรัฐวิสากิ</td>
              </tr>
            </tbody></table>-->
            </td>
          </tr>

          <tr>
            <td align="right">
            <table width="100%" border="0">
              <tbody><tr>
                <td><strong>1.ระดับผลการประเมิน &nbsp;&nbsp;</strong></td>
                <td id="evalResults"></td>
              </tr>
              <tr>
                <td><strong>2.ควรได้รับการพัฒนาเพิ่มเติมในเรื่อง  </strong></td>
                <td>
                  <textarea name="evalNote1" id="evalNote1" cols="45" rows="5"><?php echo ($get_eval_notes['evalNote1'] !=''? $get_eval_notes['evalNote1']: ''  )?></textarea>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><strong>3. ข้อคิดเห็นเพิ่มเติม </strong></td>
                <td>
                  <textarea name="evalNote2" id="evalNote2" cols="45" rows="5"><?php echo ($get_eval_notes['evalNote2'] !=''? $get_eval_notes['evalNote2']: ''  )?></textarea>
                </td>
              </tr>
            </tbody></table>
            </td>
          </tr>

        </table>
       <div style="display:none">
            <input type="text" name="eval" id="eval" value="<?php echo $eval;?>" />
            <input type="text" name="staffID" id="staffID" value="<?php echo $staffID;?>" />
            <input type="text" name="get_evalFormID" id="get_evalFormID" value="<?php echo $get_evalFormID;?>" /> 
            <input type="text" name="var_agencies" id="var_agencies" value="<?php echo $var_agencies;?>" />
        </div>

        <button type="submit" class="btn btn-success">บันทึก</button>
        &nbsp;
        <button type="button" class="btn btn-warning">ยกเลิก</button>


      </form>
    </div>
     
  </section>
  </div>
</div>
<script type="text/javascript"> 
  

  function calculateTotalScore(){
      var totalScore = 0; 
       $(".spinner").find('input[type=number]').map(function(){
          totalScore += parseInt(this.value);
      }).get();
      $('#totalScore').html(totalScore);
      updateLevel(totalScore);
  }

  var nextLevelScore = -1;
  var prevLevelScore = 0;
  var lookingForLevel = false;
  function updateLevel(totalScore){
    if((totalScore > nextLevelScore || totalScore < prevLevelScore) && !lookingForLevel){
      $("#evalResults").html('loading ...');
      lookingForLevel = true;
      $.getJSON( "<?php echo base_url(); ?>rate/getScoreAjax", {total:totalScore}, function( data ) {
        prevLevelScore = parseInt(data.minScore);
        nextLevelScore = parseInt(data.maxScore);
        $("#evalResults").html('<strong>ระดับ ' + data.levelText + ' &nbsp;&nbsp;&nbsp;&nbsp;( </strong>' + data.minScore + ' - ' + data.maxScore + ' <strong>)</strong>');
      })
      .fail(function(jqXHR, textStatus, errorThrown) { $("#evalResults").html("Error getting results"); })
      .always(function() { lookingForLevel = false; });
    }

  }



(function($) {
    $.fn.spinner = function() {
		this.each(function() {
		var el = $(this);
		
		// add elements
		el.wrap('<span class="spinner"></span>');     
		el.before('<span class="sub">-</span>');
		el.after('<span class="add">+</span>');
		
		// substract
		el.parent().on('click', '.sub', function () {
		if (el.val() > parseInt(el.attr('min')))
		  el.val( function(i, oldval) { return --oldval; });
      	  calculateTotalScore();
		});
		
		// increment
		el.parent().on('click', '.add', function () {
		if (el.val() < parseInt(el.attr('max')))
		  el.val( function(i, oldval) { return ++oldval; });
      calculateTotalScore();
		});
			});
		};
		})(jQuery);
		
	 $('input[type=number]').spinner();
   calculateTotalScore();

  //The only script I found to scroll with offset on html5 error
  var delay = 0;
  var offset = 80;

  document.addEventListener('invalid', function(e){
     $(e.target).addClass("invalid");
     $('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset }, delay);
  }, true);
  document.addEventListener('change', function(e){
     $(e.target).removeClass("invalid")
  }, true);


</script>
 