<style type="text/css">
	.form-control {
 	  color: #343232;
	 
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล</h3>
            </header>
            <div class="panel-body">
            	<table class="table" style="background-color: #fff; font-size:16px;">
                  <tr>
                    <td colspan="3"><table width="100%" border="0">
                      <tr>
                        <td colspan="7" align="center"><strong> แบบประเมินผลการปฏิบัติงานประจำปี </strong>
						<?php echo $get_evalYear;?> <strong> ครั้งที่  </strong><?php echo $get_evalRound;?> ( <strong>วันที่</strong> <?php echo $get_startDate;?> - <?php echo $get_endDate;?>) </td>
                      </tr>
                      <tr>
                        <td colspan="7" align="center"><strong> [<?php echo $get_evalNameText;?>] </strong> *** <?php echo $orgID;?></td>
                      </tr>
                      <tr>
                        <td>ชื่อ-นามสกุล</td>
                        <td><?php  echo $get_staffPreName . $get_staffFName .'&nbsp;&nbsp;'. $get_staffLName;?></td>
                        <td>&nbsp;</td>
                        <td>ตำแหน่ง</td>
                        <td><?php echo $get_positionName;?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>แผนก</td>
                        <td>xx</td>
                        <td>&nbsp;</td>
                        <td>กอง</td>
                        <td>&nbsp;</td>
                        <td>ฝ่าย</td>
                        <td>xxx</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="3"><table width="100%" border="0">
                      <tr>
                        <td>โปรดวงกลมล้อมรอบตัวเลขคะแนนที่ประเมินในแต่ละปัจจัย</td>
                        <td>&nbsp;</td>
                        <td>ระดับคะแนนที่ให้</td>
                      </tr>
                      <tr>
                        <td>หัวข้อปัจจัยที่ประเมิน</td>
                        <td>&nbsp;</td>
                        <td>คะแนน&nbsp;</td>
                      </tr>
                        
                      <?php 
          					  		//echo $list_htmll;
          					  		//foreach ($list_html as $rows) {
          								//echo $rows->html;
          			  ?>
                    </table></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  
                </table>
               	 

             </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 
	
	$(".xcrud-top-actions").find('.btn-success').attr("onClick","save_Job();");
	//$(".btn-group").find('.btn-warning').attr("onClick","edit_Job();");
	
	function save_Job(){
		 location.replace('<?php echo base_url('evaluation/add'); ?>');
	}
	function edit_Job(){
		 location.replace('<?php echo base_url('evaluation/edit'); ?>');
	}
	jQuery(document).on("xcrudafterrequest",function(event,container){
		if(Xcrud.current_task == 'save') {
		   // Xcrud.show_message(container,'WOW!','success');
		   //alert(Xcrud.current_task);
		   location.replace('<?php echo base_url(); ?>evaluation');
		}
		if(Xcrud.current_task == 'list')  {
		  location.replace('<?php echo base_url(); ?>evaluation');
		}
		 
	});
 
</script>
 