<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล</h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('rate') ?>" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">เลือกปีงบประมาณ/รอบ</label>
                                <div class="col-sm-9">
                                    <?php 
                                        echo form_dropdown('eval_date',$eval_date,$var_eval_date,'class="form-control" ');
                                    ?>
                                </div>
                             </div>
                             <div class="form-group">
                                <label class="control-label col-sm-3">หน่วยงาน</label>
                                <div class="col-sm-9">
                                    <!--<select class="xcrud-input form-control form-control" data-required="1" data-type="select" name="agencies" id="agencies" maxlength="11" >
                                         <?php  
                                            //echo $dropdown_org_chart;
                                         ?>
                                    </select>-->
                                    <?php  
 											echo form_dropdown('agencies',$dropdown_org_chart,$var_agencies,'class="form-control"  id="agencies"');
                                     ?>
                                </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">ค้นหา</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3> </h3>
            </header>
            <div class="panel-body">
                <link href="<?php echo base_url();?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url();?>xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url();?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url();?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css"><div class="xcrud">
        <div class="xcrud-container">
        <div class="xcrud-ajax">
            <input type="hidden" class="xcrud-data" name="key" value="3d1723c130b51cdaa6bea59b828151d5cfaa2f0d">
            <input type="hidden" class="xcrud-data" name="orderby" value="">
            <input type="hidden" class="xcrud-data" name="order" value="asc">
            <input type="hidden" class="xcrud-data" name="start" value="0">
            <input type="hidden" class="xcrud-data" name="limit" value="50">
            <input type="hidden" class="xcrud-data" name="instance" value="22f06f4663b5db21c775244fb2e37e51372c3cff">
            <input type="hidden" class="xcrud-data" name="task" value="list">
            <!--<div class="xcrud-top-actions">
            <div class="btn-group pull-right">
                            </div>
            <a href="javascript:;" data-task="create" class="btn btn-success xcrud-action" onclick="save_Job();"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</a>	    <a class="xcrud-search-toggle btn btn-warning" href="javascript:;">ค้นหา</a><span class="xcrud-search form-inline" style="display:none;"><input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value=""><select class="xcrud-daterange xcrud-searchdata input-small form-control" name="range" data-fieldtype="date" style="display:none"><option value="">- กรุณาเลือกช่วง -</option><option value="next_year" data-from="1451602800" data-to="1483225199">ปีหน้า</option><option value="next_month" data-from="1441058400" data-to="1443563999">เดือนหน้า</option><option value="today" data-from="1440367200" data-to="1440453599">วันนี้</option><option value="this_week_today" data-from="1440367200" data-to="1440453599">สัปดาห์นี้จนถึงวันนี้</option><option value="this_week_full" data-from="1440367200" data-to="1440971999">ทั้งสัปดาห์นี้</option><option value="last_week" data-from="1439762400" data-to="1440367199">สัปดาห์ที่แล้ว</option><option value="last_2weeks" data-from="1439157600" data-to="1439762399">2 สัปดาห์ที่แล้ว</option><option value="this_month" data-from="1438380000" data-to="1440453599">เดือนนี้</option><option value="last_month" data-from="1435701600" data-to="1440367199">เดือนที่แล้ว</option><option value="last_3months" data-from="1430431200" data-to="1440367199">3 เดือนที่แล้ว</option><option value="last_6months" data-from="1422745200" data-to="1440367199">6 เดือนที่แล้ว</option><option value="this_year" data-from="1420066800" data-to="1440453599">ภายในปีนี้</option><option value="last_year" data-from="1388530800" data-to="1420066799">ปีที่แล้ว</option></select><input class="xcrud-searchdata xcrud-datepicker-from  input-small form-control" name="phrase][from" style="display:none" data-type="date" data-fieldtype="date" type="text" value=""><input class="xcrud-searchdata xcrud-datepicker-to  input-small form-control" name="phrase][to" style="display:none" data-type="date" data-fieldtype="date" type="text" value=""><select class="xcrud-data xcrud-columns-select input-small form-control" name="column"><option value="">[ค้นจากทุกฟิลด์]</option><option value="tbl_eval_date.evalYear" data-type="int">การประเมินประจำปี</option><option value="tbl_eval_date.evalRound" data-type="int">ครั้งที่</option><option value="tbl_eval_date.startDate" data-type="date">ตั้งแต่วันที่</option><option value="tbl_eval_date.endDate" data-type="date">ถึงวันที่</option></select><span class="btn-group"><a class="xcrud-action btn btn-primary" href="javascript:;" data-search="1">ตกลง</a></span></span>            <div class="clearfix"></div>
        </div>-->
        <div class="xcrud-list-container">
        
         ﻿<table class="xcrud-list table table-striped table-hover table-bordered">
            <thead>
                <tr class="xcrud-th">
                	<th class="xcrud-num">#</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.evalYear" class="xcrud-column xcrud-action">รหัสพนักงาน</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.evalRound" class="xcrud-column xcrud-action">ชื่อ-นามสกุล</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.startDate" class="xcrud-column xcrud-action">ประเมินผลปฏิบัติงาน</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ประเมิน Competency</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ประเมิน kpi </th>
                    
            </thead>
            <tbody>
            	<?php 
				$num = 1 ;
				    //for($i = 0 ; $i<=10;$i++){
				 if(count($eval_get)>0){
                    foreach ($eval_get as $key => $item) {
                        # code...
                ?>
                    <tr class="xcrud-row xcrud-row-0"> 
                        <td class="xcrud-current xcrud-num"><?php echo $num++;?></td>
                        <td><?php echo $item->staffID; ?></td>
                        <td><?php echo $item->staffPreName . $item->staffFName.'&nbsp;'.$item->staffLName; ?></td>
                        <td>
                        <?php
                        		$get_eval_notes	=	$this->rate_model->get_eval_notes($item->staffID);
								if($get_eval_notes	== NULL){
						?>
                        <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->staffID ?>" title="">ประเมิน</a>
                        <?php }else{
								$get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
								 
						?>
                        	<a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $get_eval_nedit['empID'] ?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
                        <?php } ?>
                        </td>
                        <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->staffID ?>" title="">ประเมิน</a></td>
                        <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/profile');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->staffID ?>" title="">ดู</a></td>
                     </tr>
                <?php }
				 }else{
				
				?>	   
                	<tr class="xcrud-row xcrud-row-0"> 
                        <td colspan="6" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
                     </tr>
                <?php } ?>          
               </tbody>
            <tfoot>
           </tfoot>
        </table>
        </div>
        <div class="xcrud-nav">
            <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio"><button type="button" class="btn btn-default xcrud-action" data-limit="10">10</button><button type="button" class="btn btn-default active xcrud-action" data-limit="50">50</button><button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button><button type="button" class="btn btn-default xcrud-action" data-limit="all">ทั้งหมด</button></div>                        
                    </div>
        </div>
        <div class="xcrud-overlay" style="display: none;"></div>
    </div>
</div>
<script src="<?php echo base_url();?>xcrud/plugins/jquery.min.js"></script>
<script src="<?php echo base_url();?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>xcrud/plugins/jcrop/jquery.Jcrop.min.js"></script>
<script src="<?php echo base_url();?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>editors/tinymce/tinymce.min.js"></script>
<script src="//maps.google.com/maps/api/js?sensor=false&amp;language=th"></script>
<script src="http://maps.gstatic.com/maps-api-v3/api/js/21/9a/intl/th_ALL/main.js"></script>
<script src="<?php echo base_url();?>xcrud/plugins/xcrud.js"></script>
            <script type="text/javascript">
            <!--
            
           	var xcrud_config = {"url":"http:\/\/dpo-ehr.smmms.com\/xcrud\/xcrud_ajax.php","editor_url":"http:\/\/dpo-ehr.smmms.com\/editors\/tinymce\/tinymce.min.js","editor_init_url":false,"force_editor":false,"date_first_day":1,"date_format":"dd\/mm\/yy","time_format":"HH:mm:ss","lang":{"add":"\u0e40\u0e1e\u0e34\u0e48\u0e21","edit":"\u0e41\u0e01\u0e49\u0e44\u0e02","view":"\u0e14\u0e39\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25","remove":"\u0e25\u0e1a","duplicate":"Duplicate","print":"\u0e1e\u0e34\u0e21\u0e1e\u0e4c","export_csv":"\u0e19\u0e33\u0e2d\u0e2d\u0e01\u0e40\u0e1b\u0e47\u0e19\u0e44\u0e1f\u0e25\u0e4c CSV","search":"\u0e04\u0e49\u0e19\u0e2b\u0e32","go":"\u0e15\u0e01\u0e25\u0e07","reset":"\u0e23\u0e35\u0e40\u0e0b\u0e47\u0e15","save":"\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01","save_return":"\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01\u0e41\u0e25\u0e30\u0e22\u0e49\u0e2d\u0e19\u0e01\u0e25\u0e31\u0e1a","save_new":"\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01\u0e41\u0e25\u0e30\u0e40\u0e1e\u0e34\u0e48\u0e21\u0e43\u0e2b\u0e21\u0e48","save_edit":"\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01\u0e41\u0e25\u0e30\u0e41\u0e01\u0e49\u0e44\u0e02","return":"\u0e22\u0e49\u0e2d\u0e19\u0e01\u0e25\u0e31\u0e1a","modal_dismiss":"\u0e1b\u0e34\u0e14","add_image":"\u0e40\u0e1e\u0e34\u0e48\u0e21\u0e23\u0e39\u0e1b\u0e20\u0e32\u0e1e","add_file":"\u0e40\u0e1e\u0e34\u0e48\u0e21\u0e44\u0e1f\u0e25\u0e4c","exec_time":"Execution time:","memory_usage":"Memory usage:","bool_on":"Yes","bool_off":"No","no_file":"no file","no_image":"no image","null_option":"- none -","total_entries":"\u0e08\u0e33\u0e19\u0e27\u0e19\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25 :","table_empty":"\u0e44\u0e21\u0e48\u0e1e\u0e1a\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25","all":"\u0e17\u0e31\u0e49\u0e07\u0e2b\u0e21\u0e14","deleting_confirm":"\u0e15\u0e49\u0e2d\u0e07\u0e01\u0e32\u0e23\u0e25\u0e1a\u0e43\u0e0a\u0e48\u0e2b\u0e23\u0e37\u0e2d\u0e44\u0e21\u0e48?","undefined_error":"\u0e40\u0e01\u0e34\u0e14\u0e02\u0e49\u0e2d\u0e1c\u0e34\u0e14\u0e1e\u0e25\u0e32\u0e14\u0e1a\u0e32\u0e07\u0e2d\u0e22\u0e48\u0e32\u0e07...","validation_error":"\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25\u0e1a\u0e32\u0e07\u0e1f\u0e34\u0e25\u0e14\u0e4c\u0e44\u0e21\u0e48\u0e16\u0e39\u0e01\u0e15\u0e49\u0e2d\u0e07","image_type_error":"\u0e44\u0e21\u0e48\u0e23\u0e2d\u0e07\u0e23\u0e31\u0e1a\u0e44\u0e1f\u0e25\u0e4c\u0e20\u0e32\u0e1e\u0e19\u0e32\u0e21\u0e2a\u0e01\u0e38\u0e25\u0e19\u0e35\u0e49","unique_error":"Some fields are not unique.","your_position":"Your position","search_here":"Search here...","all_fields":"[\u0e04\u0e49\u0e19\u0e08\u0e32\u0e01\u0e17\u0e38\u0e01\u0e1f\u0e34\u0e25\u0e14\u0e4c]","choose_range":"- \u0e01\u0e23\u0e38\u0e13\u0e32\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e0a\u0e48\u0e27\u0e07 -","next_year":"\u0e1b\u0e35\u0e2b\u0e19\u0e49\u0e32","next_month":"\u0e40\u0e14\u0e37\u0e2d\u0e19\u0e2b\u0e19\u0e49\u0e32","today":"\u0e27\u0e31\u0e19\u0e19\u0e35\u0e49","this_week_today":"\u0e2a\u0e31\u0e1b\u0e14\u0e32\u0e2b\u0e4c\u0e19\u0e35\u0e49\u0e08\u0e19\u0e16\u0e36\u0e07\u0e27\u0e31\u0e19\u0e19\u0e35\u0e49","this_week_full":"\u0e17\u0e31\u0e49\u0e07\u0e2a\u0e31\u0e1b\u0e14\u0e32\u0e2b\u0e4c\u0e19\u0e35\u0e49","last_week":"\u0e2a\u0e31\u0e1b\u0e14\u0e32\u0e2b\u0e4c\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","last_2weeks":"2 \u0e2a\u0e31\u0e1b\u0e14\u0e32\u0e2b\u0e4c\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","this_month":"\u0e40\u0e14\u0e37\u0e2d\u0e19\u0e19\u0e35\u0e49","last_month":"\u0e40\u0e14\u0e37\u0e2d\u0e19\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","last_3months":"3 \u0e40\u0e14\u0e37\u0e2d\u0e19\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","last_6months":"6 \u0e40\u0e14\u0e37\u0e2d\u0e19\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","this_year":"\u0e20\u0e32\u0e22\u0e43\u0e19\u0e1b\u0e35\u0e19\u0e35\u0e49","last_year":"\u0e1b\u0e35\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27"},"rtl":0};
                            
            -->
            </script><script src="<?php echo base_url();?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js">
            </script><script src="<?php echo base_url();?>xcrud/languages/timepicker/jquery-ui-timepicker-th.js"></script> 
             </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 
	function changeRate(obj){
    	alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/xx') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   // var obj = jQuery.parseJSON(response); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(response);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
					location.replace('<?php echo base_url().'rate/' ?>');
				}
		});	
  	}
 
 
</script>
 