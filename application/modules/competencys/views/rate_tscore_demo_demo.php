<style type="text/css">
	.form-control {
 	  color: #343232;
	}
 
	.bs-example:after {
			position: absolute;
			top: 15px;
			left: 15px;
			font-size: 12px;
			font-weight: 700;
			color: #959595;
			text-transform: uppercase;
			letter-spacing: 1px;
			content: " ";
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล </h3>
            </header>
        
            	<div class="panel-body">  
                     <form action="<?php echo base_url('rate/tscore') ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">เลือกปีงบประมาณ <?php echo $var_evalYear; ?></label>
                                    <div class="col-sm-9">
                                        <?php 
                                            echo form_dropdown('evalYear',$eval_date_year, $var_evalYear,'class="form-control" onchange="changeRate(this);" id="evalYear"  ');
                                        ?>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                  <label class="control-label col-sm-3">รอบที่</label>
                                    <div class="col-sm-9">
                                    <?php if($roundRound ==''){?>
                                        <span id="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE">
                                             <select name="evalRound" id="evalRound" class="form-control">
                                                <option value="" selected="selected">กรุณาระบุรอบด้วย</option>
                                             </select>
                                        </span>
                                        <?php
                                        }else{
                                            echo $roundRound;
                                        }
                                        ?>
                                     </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">หน่วยงาน</label>
                                    <div class="col-sm-9">
                                         <?php  
                                                echo form_dropdown('agencies',$dropdown_org_chart,$var_agencies,'class="form-control"  id="agencies"');
                                         ?>
                                      </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">ระดับเริ่มต้น</label>
                                    <div class="col-sm-9">
                                            <select name="eval_date" class="form-control" >
                                                    <option value="">กรุณาระบุด้วย</option>
                                                    <option value="1" selected="selected">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                            </select>
                                      </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">ระดับสิ้นสุด</label>
                                    <div class="col-sm-9">
                                            <select name="eval_date" class="form-control" >
                                                <option value="">กรุณาระบุด้วย</option>
                                                <option value="1" selected="selected">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>           
                                      </div>
                                </div>
                                <div class="form-group">
                                      <button type="submit" class="btn btn-success">ค้นหา</button>
								  	  <button type="button" class="btn btn-info" onclick="window.open('EvalTScore-Export.xls')">Export to Excel</button>
                                </div>
                        </form>
                </div>
            
         </section>
    </div>
</div> 
 
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            
          <div class="panel-body">  
           
       		<table class="xcrud-list table table-striped table-hover table-bordered">
				<tr class="xcrud-th">
					<th style="width:150px">จำนวนคน</th>
					<td align="left"><div id="number_people"></div> </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">คะแนนเฉลี่ย (xi) </th>
					<td align="left"><div id="average"></div> </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">(&Sigma;(xi-x)<sup>2</sup>) / N </th>
					<td align="left">
                    	<div class="xcrud-overlay-sigma" >
                            	<span id="squareAll">&nbsp;</span>
                        </div>
                    </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">SD </th>
					<td align="left"> 
                    	<div class="xcrud-overlay-sd">
                            	<span id="sd">&nbsp;</span>
                        </div>
                    </td>
				</tr>
			 </table>
           
          </div>
            <br /> 
            <div class="panel-body">  
                <form id="myForm" name="myForm">
                  <table class="xcrud-list table table-striped table-hover table-bordered">
                        <thead>
                            <tr class="xcrud-th">
                                <th class="xcrud-num">#</th>
                                <th class="xcrud-column xcrud-action">รหัสพนักงาน</th>
                                <th class="xcrud-column xcrud-action">ชื่อ-นามสกุล</th>
                                <th class="xcrud-column xcrud-action">คะแนน</th>
                                <th class="xcrud-column xcrud-action">เกรด</th>
                                <th class="xcrud-column xcrud-action">xi-x</th>
                                <th class="xcrud-column xcrud-action">(xi-x)2</th>
                                <th class="xcrud-column xcrud-action">T-Score</th>
                            </tr>    
                        </thead>
                        <tbody>
               			    <?php 
								 $num = 1 ;
								 $eval_get	= $this->rate_tsocre->rate_emp_party($var_agencies,$assingID); 
								 if(count($eval_get)>0){
									foreach ($eval_get as $key => $item) {
										 # code...
										//$eval	= $this->rate_model->get_eval($item->person_id,$round);
										 										
										if($item->person_id != ''){
											 $eval		= $this->rate_tsocre->tScoreSumScore($item->person_id);
											 $Sumscore	= ($eval['SumEvalScore'] !=''?$eval['SumEvalScore']:'-');
											 $var_sum1	= $eval['SumEvalScore'];
											  
											 	 
                              ?> 
                                         	<tr class="xcrud-row xcrud-row-0"> 
                                            <td class="xcrud-current xcrud-num">
                                            <input name="employee_id[]" id="employee_id[]" type="hidden" value="<?php echo $item->person_id;?>" />
                                            </td>
                                            <td><?php echo $item->person_id;?></td>
                                            <td>&nbsp;<?php echo $item->person;?>&nbsp;<?php echo $item->personLName;?></td>
                                            <td>
                                                <?php 
                                                      echo $Sumscore;
                                                      if($Sumscore != '-'){
                                                        $eval_grade			= $this->rate_tsocre->tScoreSumGrade($eval['SumEvalScore'],$eval['evalFormID']);
                                                        $eval_grade_text	= $eval_grade['levelText'];
                                                      }else{
                                                        $eval_grade_text = '-';
                                                      }
                                                 ?>
                                            </td>
                                            <td>
                                                <?php
                                                    echo  $eval_grade_text;
                                                ?>
                                            </td>
                                            <td>
												<?php 
												 	if($Sumscore != '-'){
														$xideletex1	=	 $Sumscore - $average; 
														echo $xideletex1 ;
													}else{
													   echo '-' ;
													}
												?>
                                            </td>
                                            <td>
                                            	<?php
													if($Sumscore != '-'){												
														$square1	=	pow($xideletex1,2);
														echo $square1;
														$sd1	    += $square1;
													}else{
														echo '-';
														$sd1	    += 0;
													}
												?>
                                            </td>
                                            <td>
                                            	<?php
													if($Sumscore != '-'){	
                                                		echo number_format((($xideletex1/$sd*10)+50), 4, '.', ''); 
													}else{
														echo '-';	
													}
												?>
                                            </td>
                                        </tr>
                                 <?php 
                                           $eval_get_tsocre	= $this->rate_tsocre->rate_emp_division($item->orgID,$assingID); 
                                           foreach ($eval_get_tsocre as $key => $item2) {
                                              
                                               if($item2->group_id != ''){
												   $eval	= $this->rate_tsocre->tScoreSumScore($item2->group_id);
                                                   $Sumscore	= ($eval['SumEvalScore'] !=''?$eval['SumEvalScore']:'-');
												   $var_sum2		+= $eval['SumEvalScore'];
                                 ?> 
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">
                                                    <input name="employee_id[]" id="employee_id[]" type="hidden" value="<?php echo $item2->group_id;?>" />
                                                     
                                                    </td>
                                                    <td><?php echo $item2->group_id;?></td>
                                                    <td>&nbsp;<?php echo $item2->groups;?>&nbsp;<?php echo $item2->groupsLName;?></td>
                                                    <td>
                                                        <?php 
                                                              
                                                              echo $Sumscore;
                                                              if($Sumscore != '-'){
                                                                $eval_grade	= $this->rate_tsocre->tScoreSumGrade($eval['SumEvalScore'],$eval['evalFormID']);
                                                                $eval_grade_text	=	$eval_grade['levelText'];
                                                              }else{
                                                                $eval_grade_text = '-';
                                                              }
                                                         ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                                echo $eval_grade_text;
                                                         ?>
                                                    </td>
                                                    <td><?php 
															 if($Sumscore != '-'){
																$xideletex2	=	 $Sumscore - $average; 
																echo $xideletex2;
															 }else{
															   	echo "-";	 
															 }
														?> 
                                                	</td>
                                                    <td>
                                                    	<?php
															if($Sumscore != '-'){
																$square2	=	pow($xideletex2,2);
																echo $square2;
																$sd2	    += $square2;
															}else{
																echo '-';
																$sd2	    +=0;
															}
														?>
                                                    </td>
                                                    <td>
                                                    	<?php
															if($Sumscore != '-'){
																echo number_format((($xideletex2/$sd*10)+50), 4, '.', ''); 
															}else{
																echo "-";	
															}
														?>
                                                    </td>
                                                </tr>
                                                <?php }else{?>
                                                <tr class="xcrud-row xcrud-row-0">  
                                                    <td colspan="8" class="xcrud-current xcrud-num"> ไม่พบข้อมูล หัวหน้ากอง</td>
                                                </tr>
                                                <?php
                                                      }
                                                ?>
                                                 <?php 
                                                        $eval_get_tsocress	= $this->rate_tsocre->rate_emp_department($item2->orgID,$assingID); 
                                                         
                                                        foreach ($eval_get_tsocress as $key => $item4) {
                                                  ?> 
                                                     <?php 
                                                            $eval_get_tsocress44	= $this->rate_tsocre->rate_emp_department_person($item4->orgID,$assingID); 
                                                            
                                                            foreach ($eval_get_tsocress44 as $key => $item5) {
                                                                 
                                                                if($item5->department_id !=''){
																	
																	 $eval		= $this->rate_tsocre->tScoreSumScore($item5->department_id);
                                                                     $Sumscore	= ($eval['SumEvalScore'] !=''?$eval['SumEvalScore']:'-');
																	 $var_sum3	+= $eval['SumEvalScore'];
                                                      ?>
                                                                    <tr class="xcrud-row xcrud-row-0"> 
                                                                        <td class="xcrud-current xcrud-num">
                                                                            <input name="employee_id[]" id="employee_id[]" type="hidden" value="<?php echo $item5->department_id;?>" />
                                                                             
                                                                        </td>
                                                                        <td><?php echo $item5->department_id;?></td>
                                                                        <td>&nbsp;<?php echo $item5->department;?>&nbsp;<?php echo $item5->departmentLName;?></td>
                                                                        <td>  
                                                                            <?php                                                                                      
                                                                                      echo $Sumscore;
                                                                                      if($Sumscore != '-'){
                                                                                        $eval_grade	= $this->rate_tsocre->tScoreSumGrade($eval['SumEvalScore'],$eval['evalFormID']);
                                                                                        $eval_grade_text	=	$eval_grade['levelText'];
                                                                                      }else{
                                                                                        $eval_grade_text = '-';
                                                                                      }
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                    echo $eval_grade_text;
                                                                             ?>
                                                                        </td>
                                                                        <td>
																			<?php 
																				 if($Sumscore != '-'){
																					$xideletex3	= $Sumscore - $average; 
																					echo $xideletex3;
																				 }else{
																					echo "-"; 
																				  }
																			?>
                                                                        </td>
                                                                        <td>
                                                                        	<?php
																				 if($Sumscore != '-'){
																					$square3	=	pow($xideletex3,2);
																					echo $square3;
																					$sd3	    += $square3;
																				 }else{
																					echo '-';
																					$sd3	    += 0;
																				 }
																			?>
                                                                        </td>
                                                                        <td>
                                                                        	<?php
																				 if($Sumscore != '-'){
																					echo number_format((($xideletex3/$sd*10)+50), 4, '.', ''); 
																				 }else{
																					echo '-'; 
																				  }
																			?>
                                                                        </td>
                                                                    </tr>
                                                         
                                                                 <?php }else{?>
                                                                    <tr class="xcrud-row xcrud-row-0">  
                                                                        <td colspan="8" class="xcrud-current xcrud-num"> ไม่พบข้อมูล หัวหน้าแผนก</td>
                                                                    </tr>
                                                                 <?php }?>     
                                                          <?php }?>       
                                                          <?php 
                                                            $eval_get_tsocress5	= $this->rate_tsocre->rate_employee_org($item5->orgID,$assingID); 
                                                            foreach ($eval_get_tsocress5 as $key => $item6) {
                                                          ?> 
                                                         <?php 
                                                                $eval_get_tsocress55	= $this->rate_tsocre->rate_employee_person($item6->orgID,$assingID); 
                                                                if(count($eval_get_tsocress55)>0){
                                                                    foreach ($eval_get_tsocress55 as $key => $item7) {
                                                                         if($item7->employee_id !=''){
																			$eval	= $this->rate_tsocre->tScoreSumScore($item7->employee_id);
                                                                            $Sumscore	= ($eval['SumEvalScore'] !=''?$eval['SumEvalScore']:'-');
																			
																			$var_sum4	+= $eval['SumEvalScore'];
                                                         ?>
                                                                          <tr class="xcrud-row xcrud-row-0"> 
                                                                                <td class="xcrud-current xcrud-num">
                                                                                    <input name="employee_id[]" id="employee_id[]" type="hidden" value="<?php echo $item7->employee_id;?>" />
                                                                                    
                                                                                </td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;
																				<?php echo $item7->staffPreName; ?><?php echo $item7->staffFName;?> <?php echo $item7->staffLName;?>&nbsp; 
                                                                                </td>
                                                                                <td>
                                                                                    <?php 
                                                                                               echo $Sumscore;
                                                                                              if($Sumscore != '-'){
                                                                                                $eval_grade	= $this->rate_tsocre->tScoreSumGrade($eval['SumEvalScore'],$eval['evalFormID']);
                                                                                                $eval_grade_text	=	$eval_grade['levelText'];
                                                                                              }else{
                                                                                                $eval_grade_text = '-';
                                                                                              }
                                                                                    ?>
                                                                                </td> 
                                                                                <td>
                                                                                    <?php
                                                                                            echo $eval_grade_text;
                                                                                     ?>
                                                                                </td>
                                                                                <td>
																					<?php 
																						if($Sumscore != '-'){
																							$xideletex4	= $Sumscore - $average; 
																							echo $xideletex4;
																						}else{
																							echo "-";	
																						}
																					?>
                                                                                </td>
                                                                                <td>
                                                                                	<?php
																						if($Sumscore != '-'){
																							$square4	=	pow($xideletex4,2);
																							echo $square4;
																							$sd4	    += $square4;	
																						}else{
																							echo '-';
																							$sd4	    += 0;
																						}
																					?>
                                                                                </td>
                                                                                <td>
                                                                                	<?php
																						if($Sumscore != '-'){
																							echo number_format((($xideletex4/$sd*10)+50), 4, '.', ''); 
																						}else{
																							echo "-";	
																						}
																					?>
                                                                                </td>
                                                                           </tr>
                                                                                  
                                                                    <?php }else{?>
                                                                        <tr class="xcrud-row xcrud-row-0">  
                                                                            <td colspan="8" class="xcrud-current xcrud-num"> ไม่พบข้อมูล พนักงานในแผนก</td>
                                                                        </tr>
                                                                    <?php }?>
                                                                 <?php }?>
                                                       <?php }else{
                                                    ?> 
                                                    <?php   
                                                        }
                                                    ?>
                                                    <?php } ?></tr>
                                                   <?php } ?>
                                             <?php } ?>
                                        <?php
                                          }else{?>
                                            <tr class="xcrud-row xcrud-row-0">  
                                                <td colspan="8" class="xcrud-current xcrud-num"> ไม่พบข้อมูล หัวหน้าฝ่าย</td>
                                            </tr>
                                     <?php
                                        }
                                    }
                                 }else{
                                ?>	   
                             <tr class="xcrud-row xcrud-row-0"> 
                                <td colspan="8" class="xcrud-current xcrud-num">ไม่พบข้อมูล ฝ่าย</td>
                             </tr>
                        <?php } 
						
								$var_sums_score	= $var_sum1+$var_sum2+$var_sum3+$var_sum4 ;
								 
								if( $var_sums_score == ''){
									 $var_sums_score = 0; 
									 $squareAll		= 0;
								}else{
									 $var_sums_score	=  $var_sums_score;
									 $squareAll			= $sd1+$sd2+$sd3+$sd4;
 								}
						?>          
                       </tbody>
                     <tfoot>
                   </tfoot>
                </table>
           		</form>
       </div> 
        </section>
   </div>
<div>

<script type="application/javascript">

	function changeRate(obj){
    	//alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/recentdll') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   var obj = jQuery.parseJSON(response); 
				   //alert(response);
				   //console.log(obj); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(obj);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
				    location.replace('<?php echo base_url().'rate/recent' ?>');
				}
		});	
  	}
 	 
 
	var form 	= document.getElementById("myForm"),
		inputs  = form.getElementsByTagName("input"),
		arr 	= [];
		  
	  for(var i=0, len=inputs.length; i<len; i++){
		if(inputs[i].type === "hidden"){
		  arr.push(inputs[i].value);
		}
	  }
  
    var n		=	arr.length;
	  
 	if(n !=''){
		   
		  var score		= <?php echo $var_sums_score;?> 
		  var average	= score / n ;	
		 
		  $('#number_people').html(n);  
		  $('#average').html(average); 
 		  
		  var square	= <?php echo $squareAll ;?> / n;
		  var sds		= Math.sqrt(square).toFixed(6);
		  
		  $('#squareAll').html(square.toFixed(4)); 
		  $('#sd').html(sds);  
		  console.log(arr.length);
 		  
		  var XcrudAverage = "<?php echo $average;?>";
		  var aaaa = 1;
		  if (XcrudAverage == '') {
				 //alert(square );
				location.href = '<?php echo base_url() ;?>rate/tscore?n='+n+'&sd='+sds+'&average='+average+'&evalYear=<?php echo $var_evalYear; ?>&agencies=<?php echo $var_agencies;?>&evalRound=<?php echo $var_eval_date; ?>';
				
		  }	else{
			    
			 if(sds != "<?php echo $sd;?>"){ 	 
				location.href = '<?php echo base_url() ;?>rate/tscore?n='+n+'&sd='+sds+'&average='+average+'&evalYear=<?php echo $var_evalYear; ?>&agencies=<?php echo $var_agencies;?>&evalRound=<?php echo $var_eval_date; ?>';
			 }
		  }
		   
	 }
	 // alert(sds);
	 
		   
	  
</script>

 
 
 