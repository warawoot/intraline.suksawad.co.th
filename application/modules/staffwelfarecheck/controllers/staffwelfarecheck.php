<?php

class Staffwelfarecheck extends MY_Controller {
   
    function __construct() {
        parent::__construct();

        $this->setModel('staff_welfare_take_model');

        $this->load->model('staff_model');
         $this->load->model('welfare_take_image_model');
        $this->load->model('staff_welfare_right_model');
        $this->load->model('staff_welfare_fund_model');
        $this->load->model('staff_welfare_pension_model');
    }
 

    public function index() {
         
        $this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();

        $xcrud->table('tbl_staff');
        $xcrud->relation('positionID','tbl_position','positionID','positionName');

        $col_name = array(
            'staffID' => 'รหัสพนักงาน',  
            'staffFName' => 'ชื่อ',
            'rankID' => 'ระดับ',
            'positionID' => 'ตำแหน่ง',
            'orgID' => 'ฝ่าย / สำนักงาน'
           

        );
        $xcrud->columns('staffID,staffFName,rankID,positionID,orgID');
        $xcrud->label($col_name);
        $xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
        $xcrud->column_callback('rankID','calRankWork');
        $xcrud->column_callback('positionID','calPositionWork');
        $xcrud->column_callback('orgID','calOrgIDWork');

        $xcrud->column_width('staffID','5%');

        $xcrud->unset_add()->unset_view()->unset_edit()->unset_remove();
        $xcrud->button(site_url().'staffwelfarecheck/welfare/?token={ID}','ตรวจสอบ/เรียกดูสิทธิ์','fa fa-plus-square','btn-success');
        $xcrud->button(site_url().'staffwelfarecheck/fund/?token={ID}','กองทุน','fa fa-book','btn-warning');

        $data['html'] = $xcrud->render();
        $data['title'] = "ตรวจสอบ / เรียกดูสิทธิ์สวัสดิการ";
        $this->template->load('template/admin', 'main',$data);
    }

    public function welfare(){

        $data['staffID'] = $this->input->get('token',true);

        $this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();
        $xcrud->table('tbl_staff_welfare_right')->where('staffID =',$data['staffID']);
        $xcrud->relation('welTypeID','tbl_welfare_type','welTypeID','welTypeName');
        $col_name = array(
            'welTypeID' => 'ประเภทสวัสดิการ',
            'rightDate'  => 'วันที่เบิก',
            'rightPersonType'   => 'ผู้รับสิทธิ์',
            'personID'  => 'ชื่อ-สกุล ผู้รับสิทธิ์',
            'staffID'	=> 'ยอดเงิน'
        );

        $xcrud->columns('rightDate,rightPersonType,personID,welTypeID,staffID');
        $xcrud->label($col_name);
        $xcrud->column_callback('rightDate','toBDDate');
        $xcrud->column_callback('personID','getStaffPerson');
        $xcrud->column_callback('rightPersonType','show_person_type');
        $xcrud->column_callback('staffID','check_price_welfare');

        $xcrud->unset_add()->unset_view()->unset_edit()->unset_remove();

        //$xcrud->button(site_url().'staffwelfarecheck/view/?token={rightID}&staff='.$data['staffID'],'ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
        //$xcrud->button(site_url().'staffwelfareright/edit/?token={rightID}&staff='.$data['staffID'],'แก้ไข','glyphicon glyphicon-edit','btn-warning');

        $data['html'] = $xcrud->render();
        $data['staffName'] = getStaffName($data['staffID']);
        $data['title'] = "ตรวจสอบ / เรียกดูสิทธิ์สวัสดิการ";
        $this->template->load("template/admin",'main', $data);
    }

    public function fund(){

        $data['staffID'] = $this->input->get('token',true);

        $data['rs'] = $this->staff_model->get_by(array('ID'=>$data['staffID']),true);
        $data['rp'] = $this->staff_welfare_pension_model->get_by(array('staffID'=>$data['staffID'],'isApply'=>'1'),true);
        
        $data['rf'] = $this->staff_welfare_fund_model->get_by(array('staffID'=>$data['staffID']),true);

        $data['staffName'] = getStaffName($data['staffID']);
        $data['title'] = "ตรวจสอบ / เรียกดูสิทธิ์สวัสดิการ";
        $this->template->load("template/admin",'main_fund', $data);
    }

    public function add(){

        $data['staffID'] = $this->input->get('token',true);
        $data['title'] = "กำหนดสิทธิ์สวัสดิการ";
        $this->template->load("template/admin",'form', $data);
    }

    public function submit(){

        if(!empty($_POST['welfareType'])){

            $welfareType            =   mysql_real_escape_string(trim($this->input->post('welfareType',true)));
            $rightID                =   mysql_real_escape_string(trim($this->input->post('rightID',true)));
            $staffID                =   mysql_real_escape_string(trim($this->input->post('staffID',true)));
            $rightDate              =   mysql_real_escape_string(trim($this->input->post('rightDate',true)));    
            $rightPersonType        =   mysql_real_escape_string(trim($this->input->post('rightPersonType',true)));
            $personID                =   (isset($_POST['personID'])) ? mysql_real_escape_string(trim($this->input->post('personID',true))) : $staffID;
            
            ////////start transaction///////////
           // $this->db->trans_begin();

            $arr = array(
                'welTypeID'         => $welfareType,
                'rightDate'         => toCEDate($rightDate),
                'rightPersonType'   => $rightPersonType,
                'staffID'           => $staffID,
                'personID'          => $personID
            ); 
            if($this->save($arr,$rightID))
                redirect('staffwelfareright/welfare/?token='.$staffID,'refresh');

   
        }
    }

    public function view(){
        if(!empty($_GET['token'])){

            $takeID = mysql_real_escape_string(trim($this->input->get('token',true)));
            $data['staffID'] = mysql_real_escape_string(trim($this->input->get('staff',true)));
            $data['r'] = $this->staff_welfare_take_model->get_by(array('takeID'=>$takeID),true);
            
            $data['rp'] =  $this->welfare_take_image_model->get_by(array('takeID'=>$takeID));
            

            $data['title'] = "ตรวจสอบ / เรียกดูสิทธิ์สวัสดิการ";
            
            $data['takeID'] = $takeID;
            $this->template->load("template/admin",'form', $data);
        }   
    }

    public function edit(){
        if(!empty($_GET['token'])){

            $rightID = mysql_real_escape_string(trim($this->input->get('token',true)));
            $data['staffID'] = mysql_real_escape_string(trim($this->input->get('staff',true)));
            $data['r'] = $this->staff_welfare_right_model->get_by(array('rightID'=>$rightID),true);
            
            $data['title'] = "กำหนดสิทธิ์สวัสดิการ";
            
            $data['rightID'] = $rightID;
            $this->template->load("template/admin",'form', $data);
        }   
    }
     
   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>