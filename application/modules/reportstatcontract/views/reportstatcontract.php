
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	
	<h5 style="text-align:center;">
		<br>
			โครงสร้างสัญญาจ้างพนักงาน ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?>
		
	</h5>
	<br><br>

	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th width="50%">ฝ่าย / สำนัก</th>
			<th>จำนวน</th>
			
			
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 
					$i++; 
						$count = $this->staff_model->getContractByOrg($row->orgID);

					?>
					<tr>
						<td style="text-align:left;"><?php echo $row->orgName; ?></td>
						<td><?php echo $count; ?></td>
						
					</tr>
				
				
		<?php }}else{
			echo '<tr><td colspan="2">ไม่พบข้อมูล</td></tr>';
		}

		
		 ?>
		
	</table>
	</div>


	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportstatcontract/print_pdf');
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportstatcontract/print_excel');
        }

</script>
