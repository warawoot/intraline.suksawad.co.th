<h4 style="text-align:center;">
		<br>
			โครงสร้างสัญญาจ้างพนักงาน ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?>
		
	</h4>
	<br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th width="50%">ฝ่าย / สำนัก</th>
			<th>จำนวน</th>
			
			
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 
					$i++; 
						$count = $this->staff_model->getContractByOrg($row->orgID);

					?>
					<tr>
						<td ><?php echo $row->orgName; ?></td>
						<td style="text-align:center;"><?php echo $count; ?></td>
						
					</tr>
				
				
		<?php }}else{
			echo '<tr><td colspan="2">ไม่พบข้อมูล</td></tr>';
		}

		
		 ?>
		
	</table>
	</div>
