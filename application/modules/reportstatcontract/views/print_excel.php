<?php

$year = (!empty($year)) ? $year : (date("Y")+543);

header('Content-type: application/excel');
$filename = 'โครงสร้างสัญญาจ้างพนักงาน.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        <style>
			table,th,td{
				border: 1px solid black;
				border-collapse:collapse;
				text-align:center;
			}


		</style>
    </head>
	<body>

	
		<h5 style="text-align:center;">
			</h5>
		<h5 style="text-align:center;">โครงสร้างสัญญาจ้างพนักงาน ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?>
			
		</h5><br>

		<br><br>

		<p style="font-size:11px;">ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></p>
		
		<div class="xcrud-list-container">
		<table class="xcrud-list table table-striped table-hover table-bordered" style="font-size:11px;">
		
			<tr >
			<th width="50%">ฝ่าย / สำนัก</th>
			<th>จำนวน</th>
			
			
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 
					$i++; 
						$count = $this->staff_model->getContractByOrg($row->orgID);

					?>
					<tr>
						<td ><?php echo $row->orgName; ?></td>
						<td style="text-align:center;"><?php echo $count; ?></td>
						
					</tr>
				
				
		<?php }}else{
			echo '<tr><td colspan="2">ไม่พบข้อมูล</td></tr>';
		}

		
		 ?>
			
		</table>
		</div>


		
	</body>
</html>
