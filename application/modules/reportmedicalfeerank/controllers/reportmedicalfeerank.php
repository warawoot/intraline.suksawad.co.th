<?php

class reportmedicalfeerank extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_welfare_take_model');
		
	}
	
	public function index(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_welfare_take_model->getAll($data['day'],$data['month'],$data['year']);
		
        $this->template->load("template/admin",'reportmedicalfeerank', $data);

	}

	public function print_pdf(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_welfare_take_model->getAll($data['day'],$data['month'],$data['year']);


		$data_r['html'] = $this->load->view('print',$data,true);
		
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_welfare_take_model->getAll($data['day'],$data['month'],$data['year']);

		
		$this->load->view('print_excel',$data);

	}

	public function detail(){
		//$takeID = (isset($_GET['id'])) ? $this->input->get('id') : "";
        $data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
        $data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
        $data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);


        $data['r'] = $this->staff_welfare_take_model->getDetail($data['month'] ,$data['year']);
        //echo $this->db->last_query(); exit;
        
        $this->template->load("template/admin",'detail', $data);
	}

	public function detail_print_pdf(){
      
        $data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
        $data['month'] = (isset($_GET['month'])) ? intval($this->input->get('month')) : date("m");
        $data['year'] = (isset($_GET['year'])) ? $this->input->get('year')-543 : (date("Y"));
        
        $data['r'] = $this->staff_welfare_take_model->getDetail($data['month'] ,$data['year']);
        
        $data_r['html'] = $this->load->view('detail_print',$data,true);
        
        $this->load->view('print_pdf',$data_r);

    }

    public function detail_print_excel(){

        $data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
        $data['month'] = (isset($_GET['month'])) ? intval($this->input->get('month')) : date("m");
        $data['year'] = (isset($_GET['year'])) ? $this->input->get('year')-543 : (date("Y"));
        
        $data['r'] = $this->staff_welfare_take_model->getDetail($data['month'] ,$data['year']);

        
        $this->load->view('detail_print_excel',$data);

    }

}
/* End of file reportmedicalfeerank.php */
/* Location: ./application/module/reportmedicalfeerank/reportmedicalfeerank.php */