<?php

$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
      

header('Content-type: application/excel');
$filename = 'รายงานพนักงานที่เกิด.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        

    </head>
	<body>

	
		<h5 style="text-align:center;"></h5>
		<h5 style="text-align:center;">รายงานสถานภาพการทำงานของพนักงาน ประจำเดือน <?php echo $th_month[$month]; ?></h5>
	<br>
	
	<br><br>
	<p style="font-size:11px;">ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered" style="font-size:11px;">
	
		<tr >
			<th >เดือน</th>
			<th >ปี</th>
			<th >จำนวนรายการ</th>
			<th >ยอดการเบิก</th>
			
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
			$mtemp = "";
			$myear = "";
			$count = 0;
			$price = 0;
			foreach($r->result() as  $row){ 

					$count++;
					$arrp = json_decode($row->takePrice);
					foreach($arrp as $p){
						$price+=$p->request;
						
					}

					if($mtemp != $row->tmonth || $myear != $row->tyear){
						
						$mtemp = $row->tmonth;
						$myear = $row->tyear;
						
						if($cout>1){
								echo '<td>'.$count.'</td>';
								echo '<td>'.number_format($price).'</td>';			
								echo '</tr>';
								$count = 1;
								$price=0;
						}

				?>

					<tr>
						<?php $month = (strlen($row->tmonth) == 1) ? '0'.$row->tmonth : $row->tmonth; ?>
						<td><?php echo $th_month[$month]; ?></td>
						<td><?php echo $row->tyear+543;?></td>
					

		<?php  	}
		
			} 
			echo '<td>'.$count.'</td>';
			echo '<td>'.number_format($price).'</td>';			
			echo '</tr>'; 
		}else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>
	</body>
</html>
