
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h5 style="text-align:center;">
		<br>
			สถิติการเบิกค่ารักษาพยาบาล <?php echo ' ปี '.$year; ?>
		
	</h5>
	<br>
	ค้นหา 
	วัน
	<select style="width:80px" id="day">
		<option value="" <?php echo ($day == "") ? "selected" : ""; ?>>-- กรุณาเลือก --</option>
		<?php $count = $day_arr[intval($month)];

		for($i=1; $i<=$count; $i++){ ?>
			<option value="<?php echo $i; ?>" <?php echo ($i == $day) ? "selected" : ""; ?>><?php echo $i; ?></option>
		<?php } ?>
		
	</select>


	&nbsp;&nbsp;&nbsp;เดือน
	
	<select style="width:80px" id="month">
		<?php foreach($th_month as $key=>$val){ ?>
		<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;
	<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>

	
	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ลำดับ</th>
			<th>วันที่เบิก</th>
			<th >ผู้เบิก</th>
			<th >เบิกให้</th>
			<th >ความสัมพันธ์</th>
			<th >ประเภทสวัสดิการ</th>
			<th >โรค</th>
			<th >ผู้ป่วยใน / นอก</th>
			<th >วันที่รักษา</th>
			<th >จำนวนเงินที่เบิก</th>
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
			foreach($r->result() as  $row){ 
					$i++;


				?>
				 <tr >
					<td><?php echo $i; ?></td>
					<td><?php echo toFullDate($row->takeDate,'th','full');?></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo ($row->takePersonType == '1') ? $row->staffFName.' '.$row->staffLName : $row->personFName.' '.$row->personLName; ?></td>
					<td>
					<?php 
					switch($row->takePersonType){
						case '1' :  echo 'ตนเอง'; break;
						case '2' :  echo 'บิดา-มารดา'; break;
						case '3' :  echo 'คู่สมรส'; break;
						case '4' :  echo 'บุตร'; break;
					}
					?>
					</td>
					<td><?php echo $row->welTypeName; ?></td>
					<td><?php echo $row->diseaseName; ?></td>
					<td><?php echo $row->patTypeName; ?></td>
					<td><?php echo toFullDate($row->takeStartDate,'th','full').' - '.toFullDate($row->takeEndDate,'th','full'); ?></td>
					<?php
					$price=0;
					$arrp = json_decode($row->takePrice);
					foreach($arrp as $rowp){
						$price+=$rowp->request;
					}
					?>
					<td><?php echo number_format($price); ?></td>
				</tr>
		<?php  } 
		}else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function search(){
        	window.location='<?php echo site_url();?>reportmedicalfeerank/detail?day='+$('#day :selected').val()+'&month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
        }


        $('#month').change(function(){
        	day_arr = ["","31","29","31","30","31","30","31","31","30","31","30","31"];  
   			day = $('#day :selected').val();
        	month = $('#month :selected').val();
        	count = day_arr[parseInt(month)];
        	sel = (day == "") ? 'selected' : '';
        	html = "<option value='' "+sel+">-- กรุณาเลือก --</option>";

			for(i=1; i<=count; i++){ 

				sel = (day == i) ? 'selected' : '';
				html+='<option value="'+i+'" '+sel+'>'+i+'</option>';
			 } 
			 $("#day").html(html);
        })
        

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportmedicalfeerank/detail_print_pdf?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&day='+$('#day :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportmedicalfeerank/detail_print_excel?imonth='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&day='+$('#day :selected').val());
        }

</script>