
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h5 style="text-align:center;">
		<br>
			สถิติการเบิกค่ารักษาพยาบาล  <?php echo 'ปี'.$year; ?>
		
	</h5>
	<br>
	ค้นหา 
	วัน
	<select style="width:80px" id="day">
		<option value="" <?php echo ($day == "") ? "selected" : ""; ?>>-- กรุณาเลือก --</option>
		<?php $count = $day_arr[intval($month)];

		for($i=1; $i<=$count; $i++){ ?>
			<option value="<?php echo $i; ?>" <?php echo ($i == $day) ? "selected" : ""; ?>><?php echo $i; ?></option>
		<?php } ?>
		
	</select>


	&nbsp;&nbsp;&nbsp;เดือน
	
	<select style="width:80px" id="month">
		<?php foreach($th_month as $key=>$val){ ?>
		<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;
		ถึง 
	วัน
	<select style="width:80px" id="day">
		<option value="" <?php echo ($day == "") ? "selected" : ""; ?>>-- กรุณาเลือก --</option>
		<?php $count = $day_arr[intval($month)];

		for($i=1; $i<=$count; $i++){ ?>
			<option value="<?php echo $i; ?>" <?php echo ($i == $day) ? "selected" : ""; ?>><?php echo $i; ?></option>
		<?php } ?>
		
	</select>


	&nbsp;&nbsp;&nbsp;เดือน
	
	<select style="width:80px" id="month">
		<?php foreach($th_month as $key=>$val){ ?>
		<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;
	<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>

	
	 <a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >เดือน</th>
			<th >ปี</th>
			<th >จำนวนรายการ</th>
			<th >ยอดการเบิก</th>
			
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
			$mtemp = "";
			$myear = "";
			$count = 0;
			$price = 0;
			foreach($r->result() as  $row){ 

					$count++;
					$arrp = json_decode($row->takePrice);
					foreach($arrp as $p){
						$price+=$p->request;
						
					}

					if($mtemp != $row->tmonth || $myear != $row->tyear){
						
						$mtemp = $row->tmonth;
						$myear = $row->tyear;
						
						if($cout>1){
								echo '<td>'.$count.'</td>';
								echo '<td><a href="'.site_url().'reportmedicalfeerank/detail?month='.$row->tmonth.'&year='.$row->tyear.'">'.number_format($price).'</a></td>';			
								echo '</tr>';
								$count = 1;
								$price=0;
						}

				?>

					<tr>
						<?php $month = (strlen($row->tmonth) == 1) ? '0'.$row->tmonth : $row->tmonth; ?>
						<td><?php echo $th_month[$month]; ?></td>
						<td><?php echo $row->tyear+543;?></td>
					

		<?php  	}
		
			} 
			echo '<td>'.$count.'</td>';
			echo '<td><a href="'.site_url().'reportmedicalfeerank/detail?month='.$row->tmonth.'&year='.$row->tyear.'">'.number_format($price).'</a></td>';			
			echo '</tr>';
		}else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function search(){
        	window.location='<?php echo site_url();?>reportmedicalfeerank?day='+$('#day :selected').val()+'&month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
        }


        $('#month').change(function(){
        	day_arr = ["","31","29","31","30","31","30","31","31","30","31","30","31"];  
   			day = $('#day :selected').val();
        	month = $('#month :selected').val();
        	count = day_arr[parseInt(month)];
        	sel = (day == "") ? 'selected' : '';
        	html = "<option value='' "+sel+">-- กรุณาเลือก --</option>";

			for(i=1; i<=count; i++){ 

				sel = (day == i) ? 'selected' : '';
				html+='<option value="'+i+'" '+sel+'>'+i+'</option>';
			 } 
			 $("#day").html(html);
        })
        

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportmedicalfeerank/print_pdf?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&day='+$('#day :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportmedicalfeerank/print_excel?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&day='+$('#day :selected').val());
        }

</script>