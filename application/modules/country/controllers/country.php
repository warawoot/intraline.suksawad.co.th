<?php

class Country extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		
		// select table//
		$xcrud->table('tbl_country');
		
		
		//// List /////
		$col_name = array(
			'countryName' => 'ประเทศ'

		);

		$xcrud->columns('countryName');
		$xcrud->label($col_name);
		// End List//

		//// Form //////

		$xcrud->fields('countryName');

		$data['html'] = $xcrud->render();
		

		$data['title'] = "ประเทศ";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file country.php */
/* Location: ./application/module/country/country.php */