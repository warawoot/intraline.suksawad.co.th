<?php
class Staff_check_yearly_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff_check_yearly';
            $this->_pk = 'checkID';
        }

        public function getAll($day,$month,$year){
             if(!empty($day) || !empty($month) || !empty($year)){

                $year -= 543;
            
                 $sql = "SELECT c.*,s.StaffPreName,s.StaffFName,s.StaffLName
                        FROM tbl_staff_check_yearly c
                        JOIN tbl_staff s on c.staffID = s.staffID
                        WHERE ";
                if(!empty($day)){
                    $sql.=" DAY(c.checkDate) = '$day' AND ";
                }
                if(!empty($month)){
                     $sql.=" MONTH(c.checkDate) = '$month' AND ";
                  
                }
                if(!empty($year)){
                    $sql.=" YEAR(c.checkDate) = '$year' AND ";
                }
                
                $sql = substr($sql,0,-4);

                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r : NULL;
            }
            
        }
        

	
}
/* End of file Staff_check_yearly_model.php */
/* Location: ./application/module/reporthealthcheck/models/Staff_check_yearly_model.php */