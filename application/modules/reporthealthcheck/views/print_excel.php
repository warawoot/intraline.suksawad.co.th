<?php

$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
      

header('Content-type: application/excel');
$filename = 'รายงานตรวจสุขภาพ.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        

    </head>
	<body>

	
		<h5 style="text-align:center;"></h5>
		<h5 style="text-align:center;">รายงานตรวจสุขภาพ ประจำเดือน <?php echo $th_month[$month]; ?></h5>
	<br>
	
	<br><br>
	<p style="font-size:11px;">ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered" style="font-size:11px;">
	
		<tr >
			<th >ลำดับ</th>
			<th >รหัสพนักงาน</th>
			<th >ชื่อ-นามสกุล</th>
			
			<th >วันที่ตรวจ</th>
			<th >Film Chest</th>
			<th >Mass Chest</th>
			<th >Urine Exam</th>
			<th >Stool Exam</th>
			<th >CBC</th>
			<th >Glucose</th>
			<th >Cholesteral</th>
			<th >Triglyceride</th>
			<th >BUN</th>
			<th >Creatinine</th>
			<th >SGOT</th>
			<th >SGPT</th>
			<th >Alkaline</th>
			<th >Uric</th>
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){ echo '<tr>';
			foreach($r->result() as  $row){ 
					$i++;


				?>
				<th ><?php $i; ?></th>
				<th ><?php echo $row->staffID; ?></th>
				<th ><?php echo $row->StaffPreName.' '.$row->StaffFName.' '.$row->StaffLName; ?></th>
				
				<th ><?php echo toBDDate($row->checkDate); ?></th>
				<th ><?php echo $row->filmChest; ?></th>
				<th ><?php echo $row->massChest; ?></th>
				<th ><?php echo $row->urineExam; ?></th>
				<th ><?php echo $row->stoolExam; ?></th>
				<th ><?php echo $row->cbc; ?></th>
				<th ><?php echo $row->glucose; ?></th>
				<th ><?php echo $row->cholesteral; ?></th>
				<th ><?php echo $row->triglyceride; ?></th>
				<th ><?php echo $row->bun; ?></th>
				<th ><?php echo $row->creatinine; ?></th>
				<th ><?php echo $row->sgot; ?></th>
				<th ><?php echo $row->sgpt; ?></th>
				<th ><?php echo $row->alkaline; ?></th>
				<th ><?php echo $row->uric; ?></th>
		<?php  } 
			echo '</tr>';
		}else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>
	</body>
</html>
