
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h4 style="text-align:center;">
		<br>
			รายงานตรวจสุขภาพ <?php echo (!empty($day)) ? 'วันที่ '.$day : ""; ?> <?php echo (!empty($month)) ? 'ประจำเดือน '.$th_month[$month] : ""; ?> <?php echo (!empty($year)) ? 'ปี '.$year : ""; ?>
		
	</h4>
	
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ลำดับ</th>
			<th >รหัสพนักงาน</th>
			<th >ชื่อ-นามสกุล</th>
			
			<th >วันที่ตรวจ</th>
			<th >Film Chest</th>
			<th >Mass Chest</th>
			<th >Urine Exam</th>
			<th >Stool Exam</th>
			<th >CBC</th>
			<th >Glucose</th>
			<th >Cholesteral</th>
			<th >Triglyceride</th>
			<th >BUN</th>
			<th >Creatinine</th>
			<th >SGOT</th>
			<th >SGPT</th>
			<th >Alkaline</th>
			<th >Uric</th>
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){ echo '<tr>';
			foreach($r->result() as  $row){ 
					$i++;


				?>
				<th ><?php $i; ?></th>
				<th ><?php echo $row->staffID; ?></th>
				<th ><?php echo $row->StaffPreName.' '.$row->StaffFName.' '.$row->StaffLName; ?></th>
				
				<th ><?php echo toBDDate($row->checkDate); ?></th>
				<th ><?php echo $row->filmChest; ?></th>
				<th ><?php echo $row->massChest; ?></th>
				<th ><?php echo $row->urineExam; ?></th>
				<th ><?php echo $row->stoolExam; ?></th>
				<th ><?php echo $row->cbc; ?></th>
				<th ><?php echo $row->glucose; ?></th>
				<th ><?php echo $row->cholesteral; ?></th>
				<th ><?php echo $row->triglyceride; ?></th>
				<th ><?php echo $row->bun; ?></th>
				<th ><?php echo $row->creatinine; ?></th>
				<th ><?php echo $row->sgot; ?></th>
				<th ><?php echo $row->sgpt; ?></th>
				<th ><?php echo $row->alkaline; ?></th>
				<th ><?php echo $row->uric; ?></th>
		<?php  } 
			echo '</tr>';
	}else{
		
			echo '<tr><td colspan="19" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>