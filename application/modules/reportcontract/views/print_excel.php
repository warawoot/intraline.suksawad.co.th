<?php

$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
      

header('Content-type: application/excel');
$filename = 'รายงานพนักงานครบกำหนดสัญญาจ้าง ประจำเดือน '.$th_month[$month].'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        

    </head>
	<body>

	
		<h5 style="text-align:center;"></h5>
		<h5 style="text-align:center;">รายงานพนักงานครบกำหนดสัญญาจ้าง ประจำเดือน <?php echo $th_month[$month]; ?></h5>

	<br>
	<br>
	<p style="font-size:11px;">ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered" style="font-size:11px;">
		<thead>
			<tr class="success">
					<th >ลำดับ</th>
					<th >รหัสพนักงาน</th>
					<th >อัตราที่</th>
					<th >ชื่อ-นามสกุล</th>
					<th >ตำแหน่ง</th>
					<th >ระดับ</th>
					<th >แผนก</th>
					<th >กอง</th>
					<th >ฝ่าย</th>
					<th>วันที่ครบกำหนดสัญญาจ้าง</th>
					
				</tr>
		</thead>
		<tbody>
		<?php 
		$i=0;
		if($r != NULL){
			foreach($r->result() as  $row){ 
					
					$dateend = "";
				  $arr_f = json_decode($row->contractField);

	              foreach($arr_f as $rowf){
	                if($rowf->id == '23' && !empty($rowf->value)){
	                  $list = explode("/",$rowf->value);

	                 
	                  if(($list[2]+543) == $year && intval($list[1]) == intval($month) ){
	                  	$dateend = toFullDate($list[2].'-'.$list[1].'-'.$list[0],'th','full');
		                  $i++;
		                  break;
	                  }
	                  
	                }
	              }
	              if(!empty($dateend)){


				?>
				<tr ><!-- td 10 ตัว-->
					<td><?php echo $i; ?></td>
					<td><?php echo $row->staffID;?></td>
					<td><?php echo getSegByWork($row->ID);?></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo getPositionByWork($row->ID);?></td>
					<td><?php echo getRankByWork($row->ID);?></td>
					<td><?php echo getOrgByWork($row->ID); ?></td>
					<td><?php echo getOrg2ByWork($row->ID);?></td>
					<td><?php echo getOrg1ByWork($row->ID);?></td>
					<td><?php echo $dateend; ?></td>
				</tr>
		<?php  } }
		} 

		if($i == 0){
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		</tbody>
	</table>
	</div>
	</body>
</html>
