
<style>
table {
    border-collapse: collapse;
    width:100%;
}

table, th, td {
    border: 1px solid #ddd;

}
th{
  background: #efefef;
}
</style>
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
          ?>
	<h5 style="text-align:center;">
		<br>
			รายงานพนักงานครบกำหนดสัญญาจ้าง ประจำเดือน <?php echo $th_month[$month]; ?>
		
	</h5>
	<br>
	ค้นหา 
	เดือน
	
	<select style="width:80px" id="month">
		<?php foreach($th_month as $key=>$val){ ?>
		<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;
	<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>
	
	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ลำดับ</th>
			<th >รหัสพนักงาน</th>
			<th >อัตราที่</th>
			<th >ชื่อ-นามสกุล</th>
			<th >ตำแหน่ง</th>
			<th >ระดับ</th>
			<th >แผนก</th>
			<th >กอง</th>
			<th >ฝ่าย</th>
			<th>วันที่ครบกำหนดสัญญาจ้าง</th>
			
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
			foreach($r->result() as  $row){ 
					
					$dateend = "";
				  $arr_f = json_decode($row->contractField);

	              foreach($arr_f as $rowf){
	                if($rowf->id == '23' && !empty($rowf->value)){
	                  $list = explode("/",$rowf->value);

	                 
	                  if(($list[2]+543) == $year && intval($list[1]) == intval($month) ){
	                  	$dateend = toFullDate($list[2].'-'.$list[1].'-'.$list[0],'th','full');
		                  $i++;
		                  break;
	                  }
	                  
	                }
	              }
	              if(!empty($dateend)){


				?>
				<tr ><!-- td 10 ตัว-->
					<td><?php echo $i; ?></td>
					<td><?php echo $row->staffID;?></td>
					<td><?php echo getSegByWork($row->ID);?></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo getPositionByWork($row->ID);?></td>
					<td><?php echo getRankByWork($row->ID);?></td>
					<td><?php echo getOrgByWork($row->ID); ?></td>
					<td><?php echo getOrg2ByWork($row->ID);?></td>
					<td><?php echo getOrg1ByWork($row->ID);?></td>
					<td><?php echo $dateend; ?></td>
				</tr>
		<?php  } }
		} 

		if($i == 0){
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);
        /*$('#month').change(function(){
        	window.location='<?php echo site_url();?>reportcontract?month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
        })

        $('#year').change(function(){
        	window.location='<?php echo site_url();?>reportcontract?month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
        })*/
		function search(){
			window.location='<?php echo site_url();?>reportcontract?month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
		}

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportcontract/print_pdf?month='+$('#month :selected').val()+'&year='+$('#year :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportcontract/print_excel?month='+$('#month :selected').val()+'&year='+$('#year :selected').val());
        }

</script>