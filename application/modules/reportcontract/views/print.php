
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
          ?>
<h3 style="text-align:center;">
		<br>
			รายงานพนักงานครบกำหนดสัญญาจ้าง ประจำเดือน <?php echo $th_month[$month]; ?>
		
	</h3>
<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>


<div class="xcrud-list-container">
	<table>
		<thead>
			<tr class="success">
				<th >ลำดับ</th>
				<th >รหัสพนักงาน</th>
				<th >อัตราที่</th>
				<th >ชื่อ-นามสกุล</th>
				<th >ตำแหน่ง</th>
				<th >ระดับ</th>
				<th >แผนก</th>
				<th >กอง</th>
				<th >ฝ่าย</th>
				<th>วันที่ครบกำหนดสัญญาจ้าง</th>
				
			</tr>
		</thead>
		<tbody>
		<?php 
		$i=0;
		if($r != NULL){
			foreach($r->result() as  $row){ 
					
					$dateend = "";
				  $arr_f = json_decode($row->contractField);

	              foreach($arr_f as $rowf){
	                if($rowf->id == '23' && !empty($rowf->value)){
	                  $list = explode("/",$rowf->value);

	                 
	                  if(($list[2]+543) == $year && intval($list[1]) == intval($month) ){
	                  	$dateend = toFullDate($list[2].'-'.$list[1].'-'.$list[0],'th','full');
		                  $i++;
		                  break;
	                  }
	                  
	                }
	              }
	              if(!empty($dateend)){


				?>
				<tr ><!-- td 10 ตัว-->
					<td><?php echo $i; ?></td>
					<td><?php echo $row->staffID;?></td>
					<td><?php echo getSegByWork($row->ID);?></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo getPositionByWork($row->ID);?></td>
					<td><?php echo getRankByWork($row->ID);?></td>
					<td><?php echo getOrgByWork($row->ID); ?></td>
					<td><?php echo getOrg2ByWork($row->ID);?></td>
					<td><?php echo getOrg1ByWork($row->ID);?></td>
					<td><?php echo $dateend; ?></td>
				</tr>
		<?php  } }
		} 

		if($i == 0){
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		</tbody>
		
	</table>
	</div>