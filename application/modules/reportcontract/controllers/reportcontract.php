<?php

class Reportcontract extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_model->getByContractDate($data['month'],$data['year']);

		$data['title'] = "รายงานพนักงานครบกำหนดสัญญาจ้าง";
        $this->template->load("template/admin",'reportcontract', $data);

	}

	public function print_pdf(){

		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_model->getByContractDate($data['month'],$data['year']);

		$data_r['html'] = $this->load->view('print',$data,true);
		//echo $data_r['html']; exit;
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_model->getByContractDate($data['month'],$data['year']);

		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportcontract.php */
/* Location: ./application/module/reportcontract/reportcontract.php */