<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Backend_Controller {

    private $portalID;

    function __construct(){
        parent::__construct();
        
        $this->load->model('users_model');
        $this->load->model('usersroles_model');
        
	   $this->load->model('roles_model');
        
    
    }
    
    public function index(){

       
        
        $data['r'] = $this->users_model->get();
        $data['title'] = "Users";
        $this->template->load("template/admin2",'main', $data);

    }

    

    public function add(){

        
        $data['title'] = "Add Users";
        
        $this->template->load("template/admin2",'form', $data);
    }

    public function check_username(){	
			$result = $this->users_model->check_username(htmlspecialchars($this->input->post('username',true)));
			echo json_encode(array('flag'=> $result));			
	}

    public function submitUsers(){

    	
        if(!empty($_POST['userName']) && !empty($_POST['password']) && !empty($_POST['pwd_confirm'])){

            $userID    		=   $this->input->post('userID',true);
            $userName 		=   $this->input->post('userName',true);
            //$firstName 		= 	$this->input->post('firstName',true);
            //$lastName 		= 	$this->input->post('lastName',true);
            //$email 			= $this->input->post('email',true);
            $password 		= $this->input->post('password',true);
             $arr = array(
                //'UserID'            => $this->users_model->getLastestID()+1,	
                'UserName'          => $userName,
                'UserCredential'    => hash('sha256',$password)
                //'UserEmail'         => $email,
                //'FirstName'         => $firstName ,
                //'LastName'         	=> $lastName
              );

            ////////start transaction///////////
            $this->db->trans_begin();
                    

           $this->users_model->save($arr,$userID,false);
           
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();  
                redirect('users');
           }
           else{
                $this->db->trans_commit();
                redirect('users');
           }


        }else{
        	$userID    		=   $this->input->post('userID',true);
            $userName 		=   $this->input->post('userName',true);
            //$firstName 		= 	$this->input->post('firstName',true);
            //$lastName 		= 	$this->input->post('lastName',true);
            //$email 			= $this->input->post('email',true);

            $arr = array(
                
                'UserName'          => $userName
                //'UserEmail'         => $email,
                //'FirstName'         => $firstName,
                //'LastName'         	=> $lastName
              );

            ////////start transaction///////////
            $this->db->trans_begin();
                    

           $this->users_model->save($arr,$userID,false);
           
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();  
                redirect('users');
           }
           else{
                $this->db->trans_commit();
                redirect('users');
           }

        }
    }

    public function submitUsersPassword(){
    	if(!empty($_POST['password']) && !empty($_POST['pwd_confirm'])){

            $userID    		=   $this->input->post('userID',true);
            $password 		= $this->input->post('password',true);
             $arr = array(
               'UserCredential'    => hash('sha256',$password)
              );

            ////////start transaction///////////
            $this->db->trans_begin();
                    

           $this->users_model->save($arr,$userID,false);
           
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();  
                redirect('users');
           }
           else{
                $this->db->trans_commit();
                redirect('users');
           }


        }
    }


     public function edit(){

        if(!empty($_GET['token'])){

            $userID = mysql_real_escape_string(trim($this->input->get('token',true)));

            $data['r'] = $this->users_model->get_by(array('UserID'=>$userID),true);
            
            $data['title'] = "Edit User";
            
            $this->template->load("template/admin2",'form', $data);
        }

       
    }

    public function editPassword(){

        if(!empty($_GET['token'])){

            $userID = mysql_real_escape_string(trim($this->input->get('token',true)));

            $data['r'] = $this->users_model->get_by(array('UserID'=>$userID),true);
            
            $data['title'] = "Edit Password";
            
            $this->template->load("template/admin",'form_password', $data);
        }

       
    }

    public function userrole(){
        if(!empty($_GET['token'])){
            $data['id'] = htmlspecialchars($this->input->get('token',true));
            $data['r'] = $this->users_model->get_by(array('UserID'=>$data['id']),true);
            
            $data['title'] = 'User Roles';
            $this->template->load('template/admin2','form_userinroles',$data);

        } else 
            show_404();
    }

    public function listAlluserRoles(){
            if(!empty($_POST['id'])){
                $r = $this->usersroles_model->get_by(array('userID'=>htmlspecialchars($this->input->post('id',true))));
               
                if(!empty($r)){

                    $json = "rows:[";
                    foreach($r as $row){
                        $rR = $this->roles_model->get_by(array('roleID'=>htmlspecialchars($row->roleID)),true);

                        $json .= '{id:"' . $row->roleID . '",
                               data:["' . htmlspecialchars($rR->roleName) . '",
                               "' . base_url() . 'assets/images/icon/delete_action.png^Delete^javascript:deleteRole(' . $row->roleID . ')^_self"]},';
                    }
                    echo "{" . substr($json, 0, -1) . "]}";
                }
            }
	}
        
        public function listAllRoles(){
            if(!empty($_POST['id'])){
                $r = $this->usersroles_model->get_by(array('userID'=>htmlspecialchars($this->input->post('id',true))));
                $not_in = '';
                if(!empty($r)){
                    foreach($r as $row){
                        $not_in .= $row->roleID.',';
                    }
                    $not_in = substr($not_in,0,-1);
                }   
                
                $r = $this->roles_model->getNotRoleByUser($not_in);
                $json = "rows:[";
                foreach($r->result() as $row){ 
                    $json .= '{id:"' . $row->roleID . '",
                           data:["' . htmlspecialchars($row->roleName) . '",
                           "' . base_url() . 'assets/images/icon/delete_action.png^Delete^javascript:deleteRole(' . $row->roleID . ')^_self"]},';
                }
                echo "{" . substr($json, 0, -1) . "]}";
   
            }
	}

	 public function submitUsersRoles(){	
        if(!empty($_POST['user_role']) && !empty($_POST['user_id'])){    
	        $arr = array(
	                'RoleID' 	=> htmlspecialchars($this->input->post('user_role',true)),	
	                'UserID' 	=> htmlspecialchars($this->input->post('user_id',true)) );

	        	$this->usersroles_model->save($arr,null);
	        
	           echo json_encode(array('flag'=>true)); 
        }else
           echo json_encode(array('flag'=>false)); 		
	}

	public function deleteRole(){
            if(!empty($_POST['id']) && !empty($_POST['user_id'])){   
            	$user_role = mysql_real_escape_string($this->input->post('id',true));
                $user_id = mysql_real_escape_string($this->input->post('user_id',true));
                if($this->usersroles_model->delete_by( array('UserID'=>$user_id,'RoleID'=>$user_role)) ){
                      echo json_encode(array('flag'=>true));
                }else
                      echo json_encode(array('flag'=>false));
            }else
                  echo json_encode(array('flag'=>false));
	}


    public function delete(){
        if(!empty($_POST['token'])){
                
            ////////start transaction///////////
            $this->db->trans_begin();
            
            $userID = mysql_real_escape_string($this->input->post('token',true)); 
            
            $this->users_model->delete($userID);
            $this->usersroles_model->delete_by(array('UserID'=>$userID));
            
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();  
                echo json_encode(array('flag'=>FALSE));
           }else{
                $this->db->trans_commit();
                echo json_encode(array('flag'=>TRUE));
            }
           
        }else
            echo json_encode(array('flag'=>false));
    }

    

}

/* End of file users.php */
/* Location: ./application/controllers/users.php */
?>