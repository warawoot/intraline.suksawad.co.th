<style>

/*#warnning_same label.error {
    color: #FF0000;
}*/

.error {
  color: #FF0000;
    float: right;
    font-size: 11px;
    width: 300px;
}
/* Password Strength */
#passwordStrength
{
  height:5px;
  display:block;
  float:left;
}
#passwordDescription {
  font-size:12px;
}
.strength0
{
  width:250px;
  background:#cccccc;
}
.strength1
{
  width:50px;
  background:#ff0000;
}
.strength2
{
  width:100px;  
  background:#ff5f5f;
}
.strength3
{
  width:150px;
  background:#49a0fc;
}
.strength4
{
  background:#4dcd00;
  width:200px;
}
.strength5
{
  background:#399800;
  width:250px;
}
</style>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
<div class="col-md-12" >
  <div class="col-separator box col-separator-first col-unscrollable">
    <div class="col-table">
        <div class="row">
            <div class="col-sm-12">
              <section class="panel">

                 <header class="panel-heading">
                    <h3><?php echo !empty($title) ? $title : "DPO"; ?></h3>
                </header>
              
                <div class="panel-body">
                     <form class="form-horizontal " id="usersForm" name="usersForm" method="post" action="<?php echo site_url(); ?>users/submitUsersPassword">
                          
                          
                          <input type="hidden" name="userID" id="userID" value="<?php echo !empty($r) ? $r->userID : ""; ?>">
                          <input type="hidden" name="pwd_strength" id="pwd_strength" value="" />

                          <div class="xcrud">
                              <div class="xcrud-container">
                                <div class="xcrud-view">
                                  <div class="form-horizontal">
                                      
                                     
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">Username</label>
                                            <div class="col-sm-9">
                                            <?php echo $r->userName; ?>
                                            </div>
                                          </div>


                                          <div class="form-group">
                                              <label class="control-label col-sm-3">Password *</label>
                                              <div class="col-sm-9">
                                              
                                               <input type="password"  name="password" id="password"  data-type="text" size="20" onkeyup="passwordStrength(this.value)" class="xcrud-input form-control">
                                                <div id="warnning_pwd" class="error" ></div>
                                              
                                              </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3"></label>
                                            <div class="col-sm-9"><font size="1" color="#999999">- ความยาว 7 ตัวอักษรขึ้นไป</font></div>
                                         </div>
                                         <div class="form-group">
                                            <label class="control-label col-sm-3"></label>
                                            <div class="col-sm-9"><font size="1" color="#999999">- ประกอบด้วยตัวพิมพ์เล็ก และตัวพิมพ์ใหญ่ และตัวเลข และอักขระพิเศษ</font></div>
                                         </div>
                                         <div class="form-group">
                                            <label class="control-label col-sm-3"></label>
                                            <div class="col-sm-9"><font size="1" color="#999999">Ex. Pass@1234</font></div>
                                         </div>
                                         <div class="form-group">
                                              <label class="control-label col-sm-3">Re-Password</label>
                                              <div class="col-sm-9">
                                              
                                               <input type="password"  name="pwd_confirm" id="pwd_confirm"  data-type="text" size="20" class="xcrud-input form-control">
                                                <div id="warnning_same" ></div>
                                              
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-sm-3"></label>
                                              <div class="col-sm-9">
                                              
                                                  <div id="passwordDescription">Password not entered</div>
                                                  <div id="passwordStrength" class="strength0"></div>
                                              </div>
                                          </div>
                                         
                                  </div>
                                </div>
                          </div>
                          
                          <div class="xcrud-top-actions btn-group">
                              <?php if($this->uri->segment('2') != 'view'){ ?>
                              <a class="btn btn-primary"  href="javascript:submitForm();">บันทึกและย้อนกลับ</a>
                              <?php } ?>
                              <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>users'">ย้อนกลับ</a>
                          </div>

                     </form>         
                </div>

              </section>
            </div>
        </div>
    </div>
  </div>
</div>

<script>
  

  /*function submitForm(){
    if($('#pagesForm').valid())
      $('#pagesForm').submit();
  }*/
  function submitForm(){  
    //submit
    
      if($('#usersForm').valid() && $('#pwd_strength').val()>=4 ) { 
        if($('#pwd_strength').val()>=4 ) {    
          $('#usersForm').submit();
        } else if($('#pwd_strength').val() < 4){ 
          $('#password').attr('style','border-color:#FF0000;');
          $('#warnning_pwd').html('ไม่ผ่านเงื่อนไขการกำหนดรหัสผ่าน');
        }       
      } else if($('#userName').val() != "" && $('#password').val() != "" && $('#pwd_confirm').val() != "" && $('#pwd_strength').val() < 4){ 
        $('#password').attr('style','border-color:#FF0000;');
        $('#warnning_pwd').html('ไม่ผ่านเงื่อนไขการกำหนดรหัสผ่าน');
      } else if($('#password').val() != "" && $('#pwd_confirm').val() != "" && $('#password').val() != $('#pwd_confirm').val() != "" && $('#pwd_strength').val() >= 4){ 
        $('#password').attr('style','border-color:none;');
        $('#warnning_pwd').html('');
        
        $('#pwd_confirm').attr('style','border-color:#FF0000;');
        $('#warnning_same').addClass('error');
        $('#warnning_same').html('รหัสผ่านไม่ตรงกัน');
      } 
    
    
      
  }

  $("#usersForm").validate({
            errorClass: "error_lbl",
            rules: {
                    
                    password:"required",
                    pwd_confirm:{required:true,equalTo:"#password"}
            }, messages: {
                    
                    password:"",
                    pwd_confirm:{required:"",equalTo:"รหัสผ่านไม่ตรงกัน"}
            }
    });

  

  function passwordStrength(password)
  {
    var desc = new Array();
    desc[0] = "Very Weak";
    desc[1] = "Weak";
    desc[2] = "Better";
    desc[3] = "Medium";
    desc[4] = "Strong";
    desc[5] = "Strongest";

    var score   = 0;

    //if password bigger than 6 give 1 point
    if (password.length > 6) {
      score++;
    }

    //if password has both lower and uppercase characters give 1 point  
    if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) {
      score++;
    }

    //if password has at least one number give 1 point
    if (password.match(/\d+/)) {
      score++;
    }

    //if password has at least one special caracther give 1 point
    if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) {
      score++;
    }

    //if password bigger than 12 give another 1 point
    if (password.length > 12) {
      score++;
    }

     document.getElementById("passwordDescription").innerHTML = desc[score];
     document.getElementById("passwordStrength").className = "strength" + score;
     $('#pwd_strength').val(score);
  }
    
</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/users'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        

</script>
