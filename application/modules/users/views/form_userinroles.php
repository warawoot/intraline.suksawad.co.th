
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "DPO"; ?></h3>

            </header>
            <div class="panel-body">
                
               
                    
                    <div class="xcrud">
                          <div class="xcrud-container">
                            <div class="xcrud-view">
                              <div class="form-horizontal">

                                    <input type="hidden" name="userID" id="userID" value="<?php echo $r->userID; ?>" /> 

                                    <div class="form-group">
                                        <label class="control-label col-sm-12" style="text-align:center;"><strong>Username : <?php echo $r->userName; ?></strong></label>
                                       
                                    </div>


                                    <div class="form-group">
                                        <div class="control-label col-sm-12">
                                            <div style="width:80%;margin:auto;">
                                                <input type='hidden' name='delID' id='delID' >  
                                                <div id="roleuserGrid" style="width:370px;height:400px;margin:5px 0;float:left;"></div>
                                                <div id="userGrid" style="width:370px;height:400px;margin:5px 0;float:right;"></div>
                                                 <div style="clear:both;"></div>
                                               
                                            </div> 
                                        </div>
                                       
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-12" style="text-align:center;"> <input type='button' value='Back' onclick="window.location='<?php echo site_url(); ?>users'"></label>
                                       
                                    </div>


                              </div>
                            </div>
                        </div>
                    </div>
                   
                     
                
               

            </div>
        </section>
    </div>
</div>

<script>
        
        function addUserRole(r1, r2){
            $.ajax({
                url: "<?php echo site_url(); ?>users/submitUsersRoles",
                type:"POST",
                data:{user_role:r1,
                      user_id: $('#userID').val()
                },
                async:false,
                dataType:"json",
                success:function(data){
                    if(data.flag===true){
                       loadUserGridData();
                       loadRoleuserGridData();   
                    }else{
                        alert("เกิดข้อผิดพลาด");
                    }
                }
           });
        }
        
        function removeUserRole(r1, r2){
            $.ajax({
                url: "<?php echo site_url(); ?>users/deleteRole",
                type:"POST",
                data:{id:r1,
                        user_id:$('#userID').val()
                       },
                async:false,
                dataType:"json",
                success:function(data){
                    if(data.flag===true){
                       loadUserGridData();
                       loadRoleuserGridData();
                    }else{
                        alert("เกิดข้อผิดพลาด");
                    }
                }
           });  
        }
        
	var roleuserGrid = new dhtmlXGridObject('roleuserGrid');
        roleuserGrid.setImagePath("<? echo base_url() ?>assets/images/dhtmlxGrid/imgs/");
        roleuserGrid.setHeader("Selected Role");
        roleuserGrid.setInitWidths("350");
        roleuserGrid.setColAlign("center");
        roleuserGrid.setColTypes("ro");
        roleuserGrid.setColSorting("str");
        roleuserGrid.setSkin("dhx_skyblue");
        roleuserGrid.enableDragAndDrop(true);
        roleuserGrid.init();
        roleuserGrid.attachEvent("onDrag", addUserRole);
	
	loadRoleuserGridData();
	
        var userGrid = new dhtmlXGridObject('userGrid');
        userGrid.setImagePath("<? echo base_url() ?>assets/images/dhtmlxGrid/imgs/");
        userGrid.setHeader("Role");
        userGrid.setInitWidths("350");
        userGrid.setColAlign("center");
        userGrid.setColTypes("ro");
        userGrid.setColSorting("str");
        userGrid.setSkin("dhx_skyblue");
        userGrid.enableDragAndDrop(true);
        userGrid.init();
        userGrid.attachEvent("onDrag", removeUserRole);
        
        loadUserGridData();
        
	function loadRoleuserGridData()
        { 
            roleuserGrid.clearAll();
            try{
                var responseData = $.ajax({    
                    url: "<?php echo site_url(); ?>users/listAlluserRoles",
                    data : {id: $('#userID').val(),csrf_mict: '<?php echo $this->security->get_csrf_hash(); ?>'},
                    type : 'POST',
                    dataType : "json",
                    async:false
                }).responseText ;
                if(responseData != "" && responseData != null){
                    roleuserGrid.parse(eval('(' + responseData + ')'),"json");	
                }
            }catch(e){} 		
       
        } 	
        
        function loadUserGridData()
        { 
            userGrid.clearAll();
            try{
                var responseData = $.ajax({    
                    url: "<?php echo site_url(); ?>users/listAllRoles",
                    data : {id: $('#userID').val(),csrf_mict: '<?php echo $this->security->get_csrf_hash(); ?>'},
                    type : 'POST',
                    dataType : "json",
                    async:false
                }).responseText ;
                if(responseData != "" && responseData != null){
                    userGrid.parse(eval('(' + responseData + ')'),"json");	
                }
            }catch(e){} 		
       
        }
        
	
	
	function deleteRole(id){
        if(confirm("ต้องการลบข้อมูล ?")){
            $.ajax({
                url: "<?php echo site_url(); ?>roles/deleteRole",
                type:"POST",
                data:{user_id:id,
                          role_id:$('#role_id').val(),
                          csrf_mict: '<?php echo $this->security->get_csrf_hash(); ?>'
                         },
                async:false,
                dataType:"json",
                success:function(data){
                    if(data.flag===true){
                        
                        window.location='<?php echo base_url()?>roles/manageUserRoles?id='+$('#role_id').val();
                    }else{
                        alert("เกิดข้อผิดพลาด");
                    }
                }

           });  
        }
		
	}
	
</script>
