<style>
.xcrud-search-toggle{
    position:absolute;
    top:-35px;
    left:70px;
  }
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "DPO"; ?></h3>

            </header>
            <div class="panel-body">
                <a class="btn btn-success" href="javascript:window.location='<?php echo site_url();?>users/add'"><i class="fa fa-plus"></i> เพิ่ม</a>
                   
            </div>
        </section>
    </div>
</div>

<br /><br />
<table class="xcrud-list table table-striped table-hover table-bordered">
    <thead>
        <tr class="xcrud-th">
            <th class="xcrud-num">#</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.staffID" width="50%"> Username </th>
            <!--<th data-order="asc" data-orderby="tbl_staff.staffFName" class="xcrud-column xcrud-action" width="40%">ชื่อ - นามสกุล</th>-->
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"></th>
         </tr>   
    </thead>
    <tbody>
        <?php 
         $num = 1 ;
         
         if(!empty($r)){
            foreach($r as $row){ ?>
              
              <tr class="xcrud-row xcrud-row-0" > 

                <td class="xcrud-current xcrud-num"><?php echo $num;?></td>
                <td> <?php echo $row->userName; ?>   </td>
               
                <!--<td style="text-align:center;"><?php //echo $row->FirstName.' '.$row->LastName; ?></td>-->
                
                <td>
                <a href="<?php echo site_url(); ?>users/userrole?token=<?php echo $row->userID; ?>" title="Role" class="xcrud-action btn btn-default btn-sm"><i class="fa fa-users"></i></a>
                <a href="<?php echo site_url(); ?>users/edit?token=<?php echo $row->userID; ?>" title="แก้ไข" class="xcrud-action btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                <a href="<?php echo site_url(); ?>users/editPassword?token=<?php echo $row->userID; ?>" title="แก้ไขรหัสผ่าน" class="xcrud-action btn btn-info btn-sm"><i class="fa fa-unlock-alt"></i></a>
                <a  href="javascript:deleteRow('<?php echo $row->userID; ?>')" title="ลบ" class="xcrud-action btn btn-danger btn-sm"><i class="fa fa-remove"></i></a></td>
             </tr>

            <?php $num++; }
            /*foreach ($r->result() as $row) {
                # code...
               //$orgID       = $item->orgID;
               //$completeDate    = $item->completeDate;
               //$recent_submit   = $this->recent_model->get_recent_submit($recent_year,$recent_evalRound,$orgID);
               
              ?>
            <tr class="xcrud-row xcrud-row-0" > 
                <td class="xcrud-current xcrud-num"><?php echo $num;?></td>
                <td> <?php echo $row->staffID; ?>   </td>
                <td><?php echo $row->staffFName.' '.$row->staffLName; ?></td>
                <td><?php echo getOrg1ByWork($row->ID); ?></td>
                <td><?php echo getOrg2ByWork($row->ID);?></td>
                <td><?php echo getOrgByWork($row->ID); ?></td>
             </tr>
         <?php $num++; }*/
            }else{
         ?>    
            <tr class="xcrud-row xcrud-row-0"> 
                <td colspan="8" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
            </tr>
        <?php } ?>          
       </tbody>
    <tfoot>
   </tfoot>
</table>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/category'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);


</script>

<script>
function deleteRow(userID){
    if(confirm("ต้องการลบข้อมูล ?")){
      $.ajax({
          url: "<?php echo site_url(); ?>users/delete",
          type:"POST",
          data:{token:userID,
                csrf_mict: '<?php echo $this->security->get_csrf_hash(); ?>'
           },
          async:false,
          dataType:"json",
          success:function(data){
              if(data.flag===true){
                 window.location='<?php echo site_url()?>users';
              }else
                 alert('เกิดข้อผิดพลาด');
              
          }
      });
    }
  }

  
</script>