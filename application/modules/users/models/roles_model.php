<?php
class Roles_model extends Default_Model{

   function __construct() {
        parent::__construct();
        $this->_table = 'tbl_roles';
        $this->_pk = 'roleID';

    }
	
    function getNotRoleByUser($not_in){
            if(!empty($not_in)){
                $sql = "SELECT * FROM tbl_roles WHERE roleID NOT IN ($not_in)";
                $r = $this->db->query($sql);
            }else{
                $r = $this->db->get('tbl_roles');
            }
            
            return ($r->num_rows() > 0) ? $r : NULL;
        }
}
?>