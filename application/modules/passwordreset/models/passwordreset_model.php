<?php
class Passwordreset_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = '';
            $this->_pk = '';
        }    


		public function checkuserpassword($userID, $current_pass){
			$sql = "SELECT 
                    u.userID,
                    u.userCredential, 
                    u.roleID
                FROM `tbl_user`u 
                WHERE u.userID='$userID'
                    AND u.userCredential='".hash('sha256',$current_pass)."'";
            $r = $this->db->query($sql);
            return ($r->num_rows() > 0) ? $r : NULL;
		} 



        public function updatepassword($userID,$data){
            $this->db->set($data);
            $this->db->where('userID',$userID);
            $query = $this->db->update('tbl_user');
            return $query;
        }
}

/* End of file staff_model.php */
/* Location: ./application/module/Reportstatusemployees/models/staff_model.php */
?>