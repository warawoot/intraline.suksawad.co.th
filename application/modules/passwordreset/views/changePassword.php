	<div style="margin-left: 100px; margin-top: 10px;">
		<?php echo form_open('passwordreset/updatePwd'); ?>
		<div class="form-group"> 
			<label>Current Password</label>
			<?php echo form_password('current_password','','id="current_password" class="form-control" placeholder="รหัสผ่านปัจจุบัน"');?>
			<?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
		</div>
		<div class="form-group"> 
			<label>New Password</label>
			<?php echo form_password('new_password','','id="current_password" class="form-control" placeholder="รหัสผ่านใหม่"');?>
			<?php echo form_error('newpass', '<div class="text-danger">', '</div>'); ?>
		</div>
		<div class="form-group"> 
			<label>Confirm Password</label>
			<?php echo form_password('new_password2','','id="current_password" class="form-control" placeholder="ยืนยันรหัสผ่าน"');?>
			<?php echo form_error('confpassword', '<div class="text-danger">', '</div>'); ?>
		</div>
		<div class="form-group"> 
		<?php echo form_submit(['name'=>'submit' , 'value'=>'Update Password']); ?>
		</div>
		<?php echo form_close(); ?>
	</div>
