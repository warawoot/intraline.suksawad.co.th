<br>
<div class="xcrud-container" align="right" >
  <div class="xcrud-ajax">
        <div class="xcrud-view">
            <form id="formMain" action="<?=base_url('passwordreset/savePassword') ?>" method="post"  enctype="multipart/form-data" class="form-horizontal">       			 
                <div class="form-group">                   
					<div class="col-sm-4" align="right">
					 	<h5><strong style="color:red">*</strong>รหัสผ่านเก่า :</h5>
					</div>
                    <div class="col-sm-4">
				
						<input 
                            data-required="1" 
                            type="password" 
                            style="width:400px;background-color:#FFF" 
                            name="pw" 
                            id="pw"
                             placeholder="กรุณาป้อนรหัสเก่า..."
							class="form-control input-small" value="">			 
                    </div>
				</div>
				 <div class="form-group">                   
					<div class="col-sm-4" align="right">
					 	<h5><strong style="color:red">*</strong>รหัสผ่านใหม่ :</h5>
					</div>
                    <div class="col-sm-4">
				
						<input 
                            data-required="1" 
                            type="password" 
                            style="width:400px;background-color:#FFF" 
                            name="pw_new" 
                            id="pw_new"
                            placeholder="กรุณาป้อนรหัสใหม่..."
							class="form-control input-small" value="">			 
                    </div>
				</div>
				 <div class="form-group">                   
					<div class="col-sm-4" align="right">
					 	<h5><strong style="color:red">*</strong>ยืนยันรหัสผ่าน :</h5>
					</div>
                    <div class="col-sm-4">
				
						<input 
                            data-required="1" 
                            type="password" 
                            style="width:400px;background-color:#FFF" 
                            name="pw_rel_new" 
                            id="pw_rel_new"
                            placeholder="ยืนยันรหัสใหม่..."
							class="form-control input-small" value="">			 
                    </div>

				</div>
					<div class="col-sm-6" align="right">
						    &nbsp
							<button class="btn btn-success xcrud-action" 
								type="submit" >บันทึก
							</button> 
							&nbsp&nbsp
							<button class="btn btn-Danger xcrud-action" 
								type="submit" >ยกเลิก
							</button> 
                    </div>
        	</form>
    	</div>
	</div>
</div>



