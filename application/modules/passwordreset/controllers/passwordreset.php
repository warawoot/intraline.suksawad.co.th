<?php

class Passwordreset extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('passwordreset_model');
		$this->load->model('dashboard/dashboard_model');	 
    $this->chk_login();
		
	}
	
	public function index(){
     $this->template->load("template/admin",'passwordreset_form');  
	}

	


  public function savePassword(){
    if($this->input->post('current_pass')){
        $current_pass = $this->input->post('current_pass');
        $new_pass     = $this->input->post('new_pass');
        $new_pass2    = $this->input->post('new_pass2');
        $userID       = $this->session->userdata('userID');

          $chk_pass = $this->passwordreset_model->checkuserpassword($userID,$current_pass);
          if($chk_pass){
             
              if($new_pass == $new_pass2){
                      $data =  array(
                        'userCredential' => hash('sha256', $new_pass)
                      );

                      $updatepass = $this->passwordreset_model->updatepassword($userID,$data);
                      if($updatepass){
                          echo "<script>alert('เปลี่ยนรหัสผ่านสำเร็จ');window.location='passwordreset';</script>";
                      }else{
                          echo "<script>alert('คำสั่งผิดพลาด กรุณาทำรายการใหม่อีกครั้ง');history.back();</script>";
                      }

              }else{
                      echo "<script>alert('รหัสผ่านใหม่ของท่านไม่ตรงกัน กรุณาตรวจสอบใหม่อีกครั้ง');history.back();</script>";
              }

          }else{
              echo "<script>alert('รหัสผ่านของท่านไม่ถูกต้อง กรุณาตรวจสอบใหม่อีกครั้ง');history.back();</script>";
          }
        
    }
       
  }

  public function chk_login(){
      if(empty($this->session->userdata('login')) && empty($this->session->userdata('userID'))){
          redirect(base_url());
      }
  }




        
        

     


    


}
