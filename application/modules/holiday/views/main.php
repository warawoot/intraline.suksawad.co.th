<script type='text/javascript' src='<?php echo base_url() ?>assets/js/jquery.ui.core.min.js'></script>
<script type='text/javascript' src='<?php echo base_url() ?>assets/js/jquery.ui.datepicker.min.js'></script>
<script type='text/javascript' src='<?php echo base_url() ?>assets/js/jquery.validate.min.js'></script>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?></h3>
            </header>
            <div class="panel-body">
                <?php echo $html; ?>
            </div>
        </section>
    </div>
</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
           
          }
        }]);


</script>