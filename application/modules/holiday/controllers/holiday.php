<?php

class Holiday extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		

		$xcrud->table('tbl_holiday');
		
		
		//// List /////
		$col_name = array(
			'holiday_date' => 'วันเดือนปี',
			'holiday_name' => 'ชื่อวันหยุด'
		);
	
		$xcrud->columns('holiday_date,holiday_name');
		$xcrud->label($col_name);
		

		//// Form //////
		 //$xcrud->change_type('holiday_date','date','2012-10-22');
		//$xcrud->change_type('holiday_date','date','',array('rang_end'=>'end_date'));
		 //$xcrud->change_type('holiday_date','text');


		$xcrud->fields('holiday_date,holiday_name');

		$xcrud->validation_required('holiday_date,holiday_name');
		// echo "<pre>";
		// var_dump($xcrud);
		
		
		
		
		$data['html'] = $xcrud->render();
		$data['title'] = "ข้อมูลวันหยุด";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file mistaketype.php */
/* Location: ./application/module/mistaketype/mistaketype.php */