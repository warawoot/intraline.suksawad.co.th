<?php

class Reportstatage extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		
		$data['r'] = $this->staff_model->getAllOrg();
		
        $this->template->load("template/admin",'reportstatage', $data);

	}

	public function print_pdf(){

		$data['r'] = $this->staff_model->getAllOrg();
		$data_r['html'] = $this->load->view('print',$data,true);
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		
		$data['r'] = $this->staff_model->getAllOrg();
		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportstatage.php */
/* Location: ./application/module/reportstatage/reportstatage.php */