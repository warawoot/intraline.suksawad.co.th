
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	
	<h5 style="text-align:center;">
		<br>
			โครงสร้างอายุงานพนักงาน ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?>
		
	</h5>
	<br><br>

	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="2">ฝ่าย / สำนัก</th>
			<th colspan="6">ช่วงอายุงาน</th>
			<th rowspan="2">อายุเฉลีย</th>
			
			
		</tr>
		
		
		<tr >
			
			<td><=1</td>
			<td>1-5</td>
			<td>6-10</td>
			<td>11-15</td>
			<td>16-20</td>
			<td>>20</td>
		</tr>
		<?php 
		
		if(!empty($r)){
			foreach($r as $row){ 
					 
					$avg = 0;
					
					$sum = 0;
					
						$ro = $this->staff_model->getAgeByOrg($row->orgID);
						if(!empty($ro)){
							$count1 = 0;
							$count5 = 0;
							$count10 = 0;
							$count15 = 0;
							$count20 = 0;
							$count = 0;
							$i=0;
							foreach($ro as $rowo){
								$age = getWorkAge($rowo->workStartDate);
								$sum+= $age;
								$i++;
								if($age <= 1)
									$count1++;
								else if($age > 1 && $age <= 5)
									$count5++;
								else if($age > 5 && $age <= 10)
									$count10++;
								else if($age > 10 && $age <= 15)
									$count15++;
								else if($age > 15 && $age <= 20)
									$count20++;
								else
									$count++;

							}
							$avg = ($sum > 0 && $i > 0) ? $sum/$i : 0;
						}
					?>
					<tr>
						<td style="text-align:left;"><?php echo $row->orgName; ?></td>
						<td><?php echo $count1;?></td>
						<td><?php echo $count5;?></td>
						<td><?php echo $count10;?></td>
						<td><?php echo $count15;?></td>
						<td><?php echo $count20;?></td>
						<td><?php echo $count;?></td>
						<td><?php echo number_format($avg); ?></td>
						
					</tr>
				
				
		<?php }}

		 ?>
		
	</table>
	</div>


	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportstatage/print_pdf');
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportstatage/print_excel');
        }

        
</script>
