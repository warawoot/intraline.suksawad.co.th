<h4 style="text-align:center;">
		<br>
			โครงสร้างอายุงานพนักงาน ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?>
		
	</h4>
	<br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="2">ฝ่าย / สำนัก</th>
			<th colspan="6">ช่วงอายุงาน</th>
			<th rowspan="2">อายุเฉลีย</th>
			
			
		</tr>
		
		
		<tr >
			
			<td style="text-align:center;"><=1</td>
			<td style="text-align:center;">1-5</td>
			<td style="text-align:center;">6-10</td>
			<td style="text-align:center;">11-15</td>
			<td style="text-align:center;">16-20</td>
			<td style="text-align:center;">20</td>
		</tr>
		<?php 
		
		if(!empty($r)){
			foreach($r as $row){ 
					 
					$avg = 0;
					$count1 = 0;
					$count5 = 0;
					$count10 = 0;
					$count15 = 0;
					$count20 = 0;
					$count = 0;
					$sum = 0;
						$ro = $this->staff_model->getAgeByOrg($row->orgID);
						if(!empty($ro)){
							
							$i=0;
							
							foreach($ro as $rowo){
								$age = getWorkAge($rowo->workStartDate);
								$sum+= $age;
								$i++;
								if($age <= 1)
									$count1++;
								else if($age > 1 && $age <= 5)
									$count5++;
								else if($age > 5 && $age <= 10)
									$count10++;
								else if($age > 10 && $age <= 15)
									$count15++;
								else if($age > 15 && $age <= 20)
									$count20++;
								else
									$count++;
							}
							$avg = ($sum > 0 && $i > 0) ? $sum/$i : 0;
						}
					?>
					<tr>
						<td style="text-align:left;"><?php echo $row->orgName; ?></td>
						<td style="text-align:center;"><?php echo $count1;?></td>
						<td style="text-align:center;"><?php echo $count5;?></td>
						<td style="text-align:center;"><?php echo $count10;?></td>
						<td style="text-align:center;"><?php echo $count15;?></td>
						<td style="text-align:center;"><?php echo $count20;?></td>
						<td style="text-align:center;"><?php echo $count;?></td>
						<td style="text-align:center;"><?php echo number_format($avg); ?></td>
						
					</tr>
				
				
		<?php }}

		 ?>
		
	</table>
	</div>
