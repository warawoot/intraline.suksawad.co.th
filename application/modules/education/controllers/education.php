<?php

class Education extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		
		// select table//
		$xcrud->table('tbl_education');
		
		
		//// List /////
		$col_name = array(
			'eduName' => 'ระดับการศึกษา'

		);

		$xcrud->columns('eduName');
		$xcrud->label($col_name);
		// End List//

		//// Form //////

		$xcrud->fields('eduName');

		$data['html'] = $xcrud->render();
		

		$data['title'] = "ระดับการศึกษา";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file education.php */
/* Location: ./application/module/education/education.php */