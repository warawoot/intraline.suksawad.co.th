
<h4 style="text-align:center;">
		<br>
			ผู้มีสิทธิได้รับเครื่องราช
		
	</h4>
	<br>
	<!--ระดับ-->
	<?php //echo getDropdown(listData("tbl_rank","rankID","rankName"),"rankID", $rankID); ?>
	<br>

	
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="2">ลำดับ</th>
			<th rowspan="2">รหัสพนักงาน</th>
			<th rowspan="2">อัตราที่</th>
			<th rowspan="2">ชื่อ - นามสกุล</th>
			<th rowspan="2">ตำแหน่ง</th>
			<th rowspan="2">ระดับ</th>
			<th rowspan="2">แผนก</th>
			<th rowspan="2">กอง/กลุ่มงาน</th>
			<th rowspan="2">ฝ่าย/สำนักงาน</th>
			
			
				<?php 
					$arr_insig = array();
					$arr_insigniaalias = array();
					$rs = $this->db->get('tbl_insignia');
					echo '<th colspan="'.$rs->num_rows().'">ปีที่ได้รับเครื่องราชฯ/ชนิดของเครื่องราช</th>
					<th rowspan="2">เครื่องราชฯ ชั้นปัจจุบันที่ได้รับ</th>
					<th rowspan="2">เครื่องราชฯ ชั้นที่มีสิทธิ์ขอรับ</th>';
					if($rs->num_rows() > 0){
						echo '<tr>';
						foreach($rs->result() as $rows){
							echo '<td>'.$rows->insigniaAlias.'</td>';
							$arr_insig[] = $rows->insigniaID;
							$arr_insigniaalias[] = $rows->insigniaAlias;
						}
						echo '</tr>';
					}
				?>
			
		</tr>
		
		<?php 
		$i=0;
		$numshow=0;
		if(!empty($r)){
			foreach($r as $row){ 

				
				$i++; 

				$htmlsub = "";
				$j=0;
				$lasted_i = "";
				$lasted_y = 0;
				foreach($arr_insig as $arr){
					$rs = $this->db->get_where('tbl_staff_insignia',array('staffID'=>$row->ID,'insigniaType'=>$arr));
					if(($rs->num_rows() > 0)){
						$year = $rs->row()->insigniaYear;
						if($year > $lasted_y){
							$lasted_i = $arr_insigniaalias[$j].' ปี '.$year;
							$lasted_y = $year;
						}
						
					}else{
						$year = "-";
					}
					
					$j++;
					$htmlsub.='<td style="text-align:center;">'.$year.'</td>';
				}

				$flg_p = false;
				$flg_w = false;
				$flg_r = false;
				$flg_mp = false;
				$flg_i = false;

				if(!empty($year)){
								
						$rc = $this->staff_model->getInsigniaNext($arr_insig[$j-1]);
						if($rc != NULL){

							

							// position //
							if($rc->positionID == getPositionByWork($row->ID,false))
								$flg_p = true;

							// min work year //
							if(!empty($rc->minWorkYear)){
								if($rc->minWorkYear == getWorkAgeByWork($row->ID,true))
									$flg_w = true;
							}else
								$flg_w = true;

							// min position year //
							if(!empty($rc->minPositionYear)){

								if($rc->minPositionYear == getPositionByWork($row->ID))
									$flg_mp = true;
							}else
								$flg_mp = true;


							// min insignia year //
							if(!empty($rc->minRankYear)){
								$diff = date("Y")-$year;
								if($rc->minRankYear == $diff)
									$flg_i = true;
							}else
								$flg_i = true;


							// rank //
							$rsID = getRankByWork($row->ID,false);
							if(!empty($rc->rankID1) && !empty($rc->rankID2)){									
								if($rc->rankID ==  $rsID || $rc->rankID1 == $rsID || $rc->rankID2 == $rsID)
									$flg_r = true;
							}else if(!empty($rc->rankID1)){
								if($rc->rankID ==  $rsID || $rc->rankID1 == $rsID)
									$flg_r = true;	
							}else if(!empty($rc->rankID2)){
								if($rc->rankID ==  $rsID || $rc->rankID2 == $rsID)
									$flg_r = true;
							}else{
								if($rc->rankID ==  $rsID)
									$flg_r = true;
							}

						}
				}

				if($flg_p && $flg_w && $flg_r && $flg_mp && $flg_i){

				

				?>
				<tr>
					<td style="text-align:center;"><?php echo $i; ?></td>
					<td style="text-align:center;"><?php echo $row->staffID; ?></td>
					<td style="text-align:center;"></td>
					<td style="text-align:center;"><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo getPositionByWork($row->ID); ?></td>
					<td style="text-align:center;"><?php echo getRankByWork($row->ID);?></td>
					<td><?php echo getOrgByWork($row->ID);?></td>
					<td><?php echo getOrg2ByWork($row->ID);?></td>
					<td><?php echo getOrg1ByWork($row->ID); ?></td>
					<?php echo $htmlsub; ?>
					<td style="text-align:center;"><?php echo $lasted_i; ?></td>
					<td style="text-align:center;"><?php echo $rc->insigniaAlias; ?></td>
						
				
				</tr>		
		<?php }
			}}

		if($numshow == 0){
			$col =11+count($arr_insigniaalias);
			echo '<tr><td colspan="'.$col.'" style="text-align:center;">ไม่พบข้อมูล</td></tr>';
		}
			
		 ?>
		
	</table>
	</div>
