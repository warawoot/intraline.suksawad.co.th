
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	
	<h5 style="text-align:center;">
		<br>
			ผู้มีสิทธิได้รับเครื่องราช
		
	</h5>
	<br>
	<!--ระดับ-->
	<?php //echo getDropdown(listData("tbl_rank","rankID","rankName"),"rankID", $rankID); ?>
	<br>

	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>
	
	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="2">ลำดับ</th>
			<th rowspan="2">รหัสพนักงาน</th>
			<th rowspan="2">อัตราที่</th>
			<th rowspan="2">ชื่อ - นามสกุล</th>
			<th rowspan="2">ตำแหน่ง</th>
			<th rowspan="2">ระดับ</th>
			<th rowspan="2">แผนก</th>
			<th rowspan="2">กอง/กลุ่มงาน</th>
			<th rowspan="2">ฝ่าย/สำนักงาน</th>
			
			
				<?php 
					$arr_insig = array();
					$arr_insigniaalias = array();
					$rs = $this->db->get('tbl_insignia');
					echo '<th colspan="'.$rs->num_rows().'">ปีที่ได้รับเครื่องราชฯ/ชนิดของเครื่องราช</th>
					<th rowspan="2">เครื่องราชฯ ชั้นปัจจุบันที่ได้รับ</th>
					<th rowspan="2">เครื่องราชฯ ชั้นที่มีสิทธิ์ขอรับ</th>';
					if($rs->num_rows() > 0){
						echo '<tr>';
						foreach($rs->result() as $rows){
							echo '<td>'.$rows->insigniaAlias.'</td>';
							$arr_insig[] = $rows->insigniaID;
							$arr_insigniaalias[] = $rows->insigniaAlias;
						}
						echo '</tr>';
					}
				?>
			
		</tr>
		
		<?php 
		$i=0;
		$numshow=0;
		if(!empty($r)){
			foreach($r as $row){ 

				
				$i++; 

				$htmlsub = "";
				$j=0;
				$lasted_i = "";
				$lasted_y = 0;
				foreach($arr_insig as $arr){
					$rs = $this->db->get_where('tbl_staff_insignia',array('staffID'=>$row->ID,'insigniaType'=>$arr));
					if(($rs->num_rows() > 0)){
						$year = $rs->row()->insigniaYear;
						if($year > $lasted_y){
							$lasted_i = $arr_insigniaalias[$j].' ปี '.$year;
							$lasted_y = $year;
						}
						
					}else{
						$year = "-";
					}
					
					$j++;
					$htmlsub.='<td>'.$year.'</td>';
				}

				$flg_p = false;
				$flg_w = false;
				$flg_r = false;
				$flg_mp = false;
				$flg_i = false;

				if(!empty($year)){
								
						$rc = $this->staff_model->getInsigniaNext($arr_insig[$j-1]);
						if($rc != NULL){

							

							// position //
							if($rc->positionID == getPositionByWork($row->ID,false))
								$flg_p = true;

							// min work year //
							if(!empty($rc->minWorkYear)){
								if($rc->minWorkYear == getWorkAgeByWork($row->ID,true))
									$flg_w = true;
							}else
								$flg_w = true;

							// min position year //
							if(!empty($rc->minPositionYear)){

								if($rc->minPositionYear == getPositionByWork($row->ID))
									$flg_mp = true;
							}else
								$flg_mp = true;


							// min insignia year //
							if(!empty($rc->minRankYear)){
								$diff = date("Y")-$year;
								if($rc->minRankYear == $diff)
									$flg_i = true;
							}else
								$flg_i = true;


							// rank //
							$rsID = getRankByWork($row->ID,false);
							if(!empty($rc->rankID1) && !empty($rc->rankID2)){									
								if($rc->rankID ==  $rsID || $rc->rankID1 == $rsID || $rc->rankID2 == $rsID)
									$flg_r = true;
							}else if(!empty($rc->rankID1)){
								if($rc->rankID ==  $rsID || $rc->rankID1 == $rsID)
									$flg_r = true;	
							}else if(!empty($rc->rankID2)){
								if($rc->rankID ==  $rsID || $rc->rankID2 == $rsID)
									$flg_r = true;
							}else{
								if($rc->rankID ==  $rsID)
									$flg_r = true;
							}

						}
				}

				if($flg_p && $flg_w && $flg_r && $flg_mp && $flg_i){

					$numshow++;

				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $row->staffID; ?></td>
					<td></td>
					<td style="text-align:left;"><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td style="text-align:left;"><?php echo getPositionByWork($row->ID); ?></td>
					<td><?php echo getRankByWork($row->ID);?></td>
					<td style="text-align:left;"><?php echo getOrgByWork($row->ID);?></td>
					<td style="text-align:left;"><?php echo getOrg2ByWork($row->ID);?></td>
					<td style="text-align:left;"><?php echo getOrg1ByWork($row->ID); ?></td>
					<?php echo $htmlsub; ?>
					<td><?php echo $lasted_i; ?></td>
					<td><?php echo $rc->insigniaAlias; ?></td>
						
				
				</tr>		
		<?php }
			}}

		if($numshow == 0){
			$col =11+count($arr_insigniaalias);
			echo '<tr><td colspan="'.$col.'">ไม่พบข้อมูล</td></tr>';
		}
			
		 ?>
		
	</table>
	</div>

	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        
        function print_pdf(){
        	window.open('<?php echo site_url();?>reportinsignia/print_pdf');
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportinsignia/print_excel');
        }
</script>
