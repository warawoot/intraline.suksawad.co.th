<?php

class Reportinsignia extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		

		$data['rankID'] = (!empty($_GET['rankID'])) ? $this->input->get('rankID') : "";
		
		$data['r'] = $this->staff_model->get();
		
		$data['title'] = "ผู้มีสิทธิได้รับเครื่องราช";

        $this->template->load("template/admin",'reportinsignia', $data);

	}

	public function print_pdf(){

		
		$data['rankID'] = (!empty($_GET['rankID'])) ? $this->input->get('rankID') : "";

		$data['r'] = $this->staff_model->get();
		$data_r['html'] = $this->load->view('print',$data,true);

		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		
		$data['rankID'] = (!empty($_GET['rankID'])) ? $this->input->get('rankID') : "";

		$data['r'] = $this->staff_model->getByAgeExpried();
		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportinsignia.php */
/* Location: ./application/module/reportinsignia/reportinsignia.php */