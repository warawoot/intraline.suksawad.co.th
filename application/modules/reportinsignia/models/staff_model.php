<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

        public function getByAgeExpried(){
        	$sql = "SELECT s.*
        			FROM tbl_staff s";
        	$r = $this->db->query($sql);
        	return $r->result();
        }

        public function getInsigniaNext($id){
            $sql = "SELECT * FROM tbl_insignia_condition c
                    JOIN tbl_insignia_condition c2 ON c.insigniaID2 = c2.insigniaID
                    JOIN tbl_insignia i on c2.insigniaID = i.insigniaID
                    WHERE c.insigniaID = $id";
            $r = $this->db->query($sql);
            return ($r->num_rows() > 0) ? $r->row() : NULL;
        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/reg/models/staff_model.php */