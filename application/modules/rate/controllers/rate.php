<?php

class Rate extends MY_Controller {
	
     function __construct() {
    	parent::__construct();
     	$this->load->model('rate_model');
		$this->load->model('rate_tscore');
		$this->load->model('recent_model');
		$this->load->model('rate_tscore_calculate');
		$this->load->model('report/report_model');
		$this->load->model('sequence/sequence_model');
		$this->load->model('absence/absence_model');
		$this->load->model('organization/organization_model');
		$this->assignID	= $this->sequence_model->get_last_assignID();
		 
    }
  	 
    public function index() {
		
		## Recent ##
		$token				= $this->input->get('token');	
		$eval = $this->input->get('eval');
		if(!empty($eval)){
			$data['eval'] = $eval;
		}else{
			$lastest_eval = $this->rate_model->getEvalForm('','',1);
			$data['eval'] = $lastest_eval->evalRound.','.$lastest_eval->evalYear;
		}

		
		$recentz			= $data['eval'];
 		
	

		
		$agencies			= $this->input->get('agencies'); 
		 
		// Redirect  : [ eval=1,2558&staffID=161&var_agencies=82 ]
		$staffID			= $this->input->get('staffID'); 
		
		// End Redirect
		$var_eval_date				 		= ($eval_date !=''?$eval_date:($eval !=''?$eval:($recentz !=''?$recentz:'')));
		
		## mode
		
		//$data['eval_date']					= $this->rate_model->get_dropdown_all_new('evalRound' , 'evalYear' ,'tbl_eval_date',$var_eval_date);#Dropdown 
		$data['dropdown_org_chart'] 			= $this->organization_model->getDropdownManager($this->session->userdata('userID'), $prefix = '', $token ,$agencies);#Dropdown
 		//$data['get_dropdown_org_option']  	= $this->rate_model->get_dropdown_org_option('orgID' , 'orgName' ,'tbl_org_chart',$this->assignID);#Dropdown
		
		if($eval_date !=''){
			$rate_eval_dates	= $this->rate_model->rate_eval_dates($eval_date); 
			 $data['dateDue']	= DateThaiHelper($rate_eval_dates['dateDue']);
		 
		}elseif($recentz !=''){
			$rate_eval_dates	= $this->rate_model->rate_eval_dates($recentz); 
			 $data['dateDue']	= DateThaiHelper($rate_eval_dates['dateDue']);
		 
		}
		
        $data['data']			= '';
		$data['var_eval_date']	= $var_eval_date; //evalRound
		$data['var_evalYear']	= $evalYear;
		$data['var_agencies']	= $agencies;
		$data['assignID']		= $this->assignID; //assignID
		$data['orgName'] 		= $this->organization_model->getOrgName($agencies);
		
		// recent_submit($evalYear,$evalRound,$orgID) 
 		$data['evalFormID']		=	'';
		$data['evalType']		=	'';
		## Recent ##
		$data['year']		=	$year;
		$data['round']		=	$round;

		
		
 		
        $this->template->load('template/admin', 'appraisal_list',$data);	
 		
     }
	 
	 public function kpi(){
  		
		$data['title']				= 'Job Profile';
		$eval						= $this->input->get_post('eval'); // evalRound
		$staffID					= $this->input->get_post('staffID'); 
		$var_agencies	    		= $this->input->get_post('var_agencies'); 
 		 
		if($staffID  == '' || $eval  == ''){
			redirect(site_url().'rate', 'refresh');
			exit();
 		}
		 
		## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 
		$eval_doc					= $this->rate_model->rate_eval_doc($eval,2); 
		
		 
		$data['get_evalYear']		= $eval_doc['evalYear'];
		$data['get_evalRound']		= $eval_doc['evalRound'];
		$data['get_startDate']		= DateThaiHelper($eval_doc['startDate']);
		$data['get_endDate']		= DateThaiHelper($eval_doc['endDate']);
		$data['get_evalNameText']	= $eval_doc['evalNameText'];
		$data['get_evalFormID']		= $eval_doc['evalFormID']; 
		## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		$eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		$data['get_staffPreName']	= $eval_emp['staffPreName'];
		$data['get_staffFName']		= $eval_emp['staffFName'];
		$data['get_staffLName']		= $eval_emp['staffLName'];
		$data['get_positionName']	= $eval_emp['positionName'];  
		$data['orgID']				= $eval_emp['orgID'];
		 
		## แสดงส่วนของประเมิน กลุ่ม ##
		 
		$data['get_eval_group_subject'] = $this->rate_model->get_eval_group_subject($data['get_evalFormID'],2);  
		//echo "<pre>";
		//print_r( $data['get_eval_group_subject']);
		## Sum 
		$exp_eval = explode(',', $eval);
        $evalRound = $exp_eval[0];
      	$evalYear = $exp_eval[1];
		$kpi_form = $this->rate_model->getEvalForm($evalYear,$evalRound,2);

        $data['ddl_kpi'] 			= $this->rate_model->get_dropdown_kpi($var_agencies);
		$data['ddl_edit'] 			= $this->rate_model->get_eval_data($staffID,$kpi_form->evalFormID,"");
		//print_r($data['ddl_edit']);
		/*
	  	foreach ($ddl_edit as $key => $item) {  
			echo $item->evalSubjectID."/".$item->evalScore."/".$item->evalVariable."<br>";
		}
		echo "5=".$ddl_edit[5]."<br>";
		*/		  
		// echo "<pre>";
		// print_r($data['get_eval_notes']);
		 
		$data['eval']				= $eval; 
		$data['staffID']			= $staffID; 
		$data['var_agencies']		= $var_agencies; 
		$data['data']				= '';
		## Note
		//$data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID,$eval_doc['evalFormID'],2);
 
		$this->template->load('template/admin', 'appraisal_kpi',$data);
	 }

	 public function competency(){
  		 
		 $data['title']		=	'Competency';
		 $eval				= $this->input->get_post('eval'); // evalRound
		 $staffID			= $this->input->get_post('staffID'); 
		 $var_agencies	    = $this->input->get_post('var_agencies'); 
 		 
		 if($staffID  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 if($eval  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval,3); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 
		 ## แสดงส่วนของรายชื่อพนักงาน ## 
		 ## ดึงข้อมูลพนักงานมาแสดง น่าจะย้ายไปไว้ staff_model ##

		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 $data['rankID'] 			= $eval_emp['rankID'];
		 
		 ## แสดงส่วนของประเมิน กลุ่ม ##
		 
		 //$data['get_eval_group_subject'] = $this->rate_model->get_eval_group_subject($data['get_evalFormID'],3);  
		 $data['ddl_subject'] 		= $this->rate_model->get_dropdown_competency($var_agencies,2);
		 $data['ddl_edit'] 			= $this->rate_model->get_competency_data($staffID,$eval_doc['evalFormID'],1);
		 //Managirial Competency 
		 $data['ddl_subjectM'] 		= $this->rate_model->get_dropdown_competency(0,3);
		 $data['ddl_editM'] 		= $this->rate_model->get_competency_data($staffID,$eval_doc['evalFormID'],2); 
		 //print_r($data['ddl_edit']);
		 //echo $this->db->last_query();
		 //echo "<pre>";
		 //print_r( $data['get_eval_group_subject']);
		## Sum 
		  
		// echo "<pre>";
		// print_r($data['get_eval_notes']);
		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID; 
		 $data['var_agencies']	=	$var_agencies; 
		 $data['data']		=	'';
		 $data['data']		=	'';
		## Note
		 $data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID,$eval_doc['evalFormID'],3);
 
		 $this->template->load('template/admin', 'appraisal_competency',$data);
		 //$this->load->view("eval-kpi");
	 }
	 
	 public function assessment(){
	 	show_404();
	 	exit();
	 	//ตัดฟอร์ม love ออก18/06/2018


	 	
		//แสดงแบบประเมิน Performance		 
		$eval				= $this->input->get_post('eval'); // evalRound
		$staffID			= $this->input->get_post('staffID'); 
		$var_agencies	    = $this->input->get_post('var_agencies'); 
 		 
		if($staffID  == ''){
			redirect(site_url().'rate', 'refresh');
		exit();
		}

		if($eval  == ''){
			redirect(site_url().'rate', 'refresh');
		exit();
		}

		## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 
		$eval_doc			= $this->rate_model->rate_eval_doc($eval,1); 

		$data['get_evalYear']		= $eval_doc['evalYear'];
		$data['get_evalRound']		= $eval_doc['evalRound'];
		$data['get_startDate']		= DateThaiHelper($eval_doc['startDate']);
		$data['get_endDate']		= DateThaiHelper($eval_doc['endDate']);
		$data['get_evalNameText']	= $eval_doc['evalNameText'];
		$data['get_evalFormID']		= $eval_doc['evalFormID']; 
		## แสดงส่วนของรายชื่อพนักงงาน ## 

		$eval_emp	= $this->rate_model->rate_emp_doc($staffID); 

		$data['get_staffPreName']	= $eval_emp['staffPreName'];
		$data['get_staffFName']		= $eval_emp['staffFName'];
		$data['get_staffLName']		= $eval_emp['staffLName'];
		$data['get_positionName']	= $eval_emp['positionName'];  
		$data['orgID']				= $eval_emp['orgID'];
		$empID       				= $eval_emp['ID'];

		$data['ac1'] = $this->absence_model->getAbsenceQuota(1);
		$data['ac2'] = $this->absence_model->getAbsenceQuota(2);
		$data['ac7'] = $this->absence_model->getAbsenceQuota(7);
		$sa1 = $this->absence_model->getStaffAbsence(date('Y'),$empID,1);
		$sa2 = $this->absence_model->getStaffAbsence(date('Y'),$empID,2);
		$sa7 = $this->absence_model->getStaffAbsence(date('Y'),$empID,7);
		$sao = $this->absence_model->getStaffAbsence(date('Y'),$empID);
		$data['sa1'] = is_null($sa1)?0:$sa1;
		$data['sa2'] = is_null($sa2)?0:$sa2;
		$data['sa7'] = is_null($sa7)?0:$sa7;
		$data['sao'] = is_null($sao)?0:$sao;		 
		 
		 ## แสดงส่วนของประเมิน กลุ่ม ##
		 
		 $data['get_eval_group_subject'] = $this->rate_model->get_eval_group_subject($data['get_evalFormID'],1);  

		 ## Note
		 //$data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID);
		 $data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID,$eval_doc['evalFormID'],1);
		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID; 
		 $data['var_agencies']	=	$var_agencies; 
		 //$data['data']		=	'';
		 $this->template->load('template/admin', 'appraisal_performance',$data);
	 }

	public function getScoreAjax(){
	 	 $total			=	$this->input->get_post('total');
	 	 if(empty($total))$total = 0; //if not defined
	 	 # score 
 		 $maxScore	= $this->rate_model->tbl_eval_form_level(intval($total));
		 echo json_encode($maxScore);
	}

	public function kpisubmit()
	{
		$evalType				= 2; //$this->input->get_post('evalType'); // evalRound
		$eval					= $this->input->get_post('eval'); // evalRound
		$staffID				= $this->input->get_post('staffID');
		$evalFormID				= $this->input->get_post('get_evalFormID');	
		$var_agencies			= $this->input->get_post('var_agencies');	
		$this->rate_model->delete_eval($staffID,$evalFormID); //ลบข้อมูลเดิมก่อนบันทึกใหม่
		for($i=1;$i<=10;$i++){
			$evalSubjectID 		= $this->input->get_post('ddl_kpi_'.$i);
			$evalScore 			= $this->input->get_post('score_'.$i);
			$evalVariable 		= $this->input->get_post('weight_'.$i);
			if($evalSubjectID !== '')
			{
			//echo $i."-".$this->input->get_post('ddl_kpi_'.$i)."-".$this->input->get_post('weight_'.$i)."-".$this->input->get_post('score_'.$i)."<br>";
				$inputArray  = 	array(
								'empID' => $staffID, 
								'evalFormID'=>$evalFormID,
								'evalSubjectID'=>$evalSubjectID,
								'evalScore'=>$evalScore,
								'evalType'=>$evalType,
								'evalVariable'=>$evalVariable
						);
	 			$this->rate_model->insert_eval($inputArray);
				//echo $this->db->last_query();
			}
		}
		 $this->template->load('template/admin', 'appraisal_success',$data);
	}
	 
	public function competencysubmit()
	{
		$eval					= $this->input->get_post('eval'); // evalRound
		$staffID				= $this->input->get_post('staffID');
		$evalFormID				= $this->input->get_post('get_evalFormID');	
		$var_agencies			= $this->input->get_post('var_agencies');	
		$rankID					= $this->input->get_post('rankID');	
		$this->rate_model->delete_eval($staffID,$evalFormID); //ลบข้อมูลเดิมก่อนบันทึกใหม่
		$evalType				= 1; //$this->input->get_post('evalType');
		for($i=1;$i<=15;$i++)
		{
			$evalSubjectID 		= $this->input->get_post('ddl_subject_'.$i);
			$evalScore 			= $this->input->get_post('score_'.$i);
			$evalVariable 		= $this->input->get_post('weight_'.$i);
			if($evalSubjectID !== '')
			{
				$inputArray  = 	array(
								'empID' => $staffID, 
								'evalFormID'=>$evalFormID,
								'evalSubjectID'=>$evalSubjectID,
								'evalScore'=>$evalScore,
								'evalType'=>$evalType,
								'evalVariable'=>$evalVariable
						);
	 			$this->rate_model->insert_eval($inputArray);
			}
		}

		if($rankID != 0)
		{
			$evalType				= 2; //Managirial Competency
			for($i=1;$i<=4;$i++)
			{
				$evalSubjectID 		= $this->input->get_post('ddl_subjectM_'.$i);
				$evalScore 			= $this->input->get_post('scoreM_'.$i);
				$evalVariable 		= $this->input->get_post('weightM_'.$i);
				if($evalSubjectID !== '')
				{
					$inputArray  = 	array(
									'empID' => $staffID, 
									'evalFormID'=>$evalFormID,
									'evalSubjectID'=>$evalSubjectID,
									'evalScore'=>$evalScore,
									'evalType'=>$evalType,
									'evalVariable'=>$evalVariable
							);
					$this->rate_model->insert_eval($inputArray);
				}
			}
		}

		$this->template->load('template/admin', 'appraisal_success',$data);
	}
	 
	public function docs(){
		 
 		$evalType				=	1; //$this->input->get_post('evalType'); // evalRound
		$eval					=	$this->input->get_post('eval'); // evalRound
		$staffID				=	$this->input->get_post('staffID');
		//$eval_doc				=	$this->rate_model->rate_eval_doc($eval); 
		$ddl_count		    	=	count($this->input->get_post('ddl')) -1;	
		$ddl					=	$this->input->get_post('ddl')  ;	
		$evalSubjectID_count	=	count($this->input->get_post('evalSubjectID'))  ;	
		$evalSubjectID			=	$this->input->get_post('evalSubjectID');
		$evalFormID			=	$this->input->get_post('get_evalFormID');	
		$var_agencies			=	$this->input->get_post('var_agencies');	
		 
		$total = 0;

		$get_eval 		=  $this->rate_model->get_eval($staffID,$evalFormID);
		for($i=0;$i<=$ddl_count;$i++){
			$total		 	+= $this->input->get_post($ddl[$i]);
			$ddl_value		=	$this->input->get_post($ddl[$i]); 
			
			$subject 	=  	$evalSubjectID[$i] ;
			$arrayName  = 	array('empID' => $staffID, 
								'evalFormID'=>$evalFormID,
								'evalSubjectID'=>$subject,
								'evalScore'=>$ddl_value,
								'evalType'=>$evalType
						);
			$insert_eval	=	$this->rate_model->insert_eval($arrayName);
		}

		/*
		if($get_eval == NULL){
			 for($i=0;$i<=$ddl_count;$i++){
	 			$total		 	+= $this->input->get_post($ddl[$i]);
	 			$ddl_value		=	$this->input->get_post($ddl[$i]); 
	 		  
	 			$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array('empID' => $staffID, 
	 								'evalFormID'=>$evalFormID,
	 								'evalSubjectID'=>$subject,
	 								'evalScore'=>$ddl_value,
									'evalType'=>$evalType
	 						);
	 			$insert_eval	=	$this->rate_model->insert_eval($arrayName);
	 		 }
	 	}else{
	 		for($i=0;$i<=$ddl_count;$i++){
	 			$total		 	+= $this->input->get_post($ddl[$i]);
	 			$ddl_value		=	$this->input->get_post($ddl[$i]); 
	 		  
	 			$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array( 
	 								 
	 								'evalScore'=>$ddl_value
	 						);
	 			$update_eval	=	$this->rate_model->update_eval($staffID,$evalFormID,$subject,$arrayName);
	 		 }
	 	}
		*/

		$evalNote1			= $this->input->get_post('evalNote1');
		$evalNote2			= $this->input->get_post('evalNote2');
		$data['data']		=	'';
		$evalType				=	$this->input->get_post('evalType'); // evalRound
		
		$arrayName = array( 'empID' => $staffID ,
							'evalFormID'=> $evalFormID,
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
					
		$arrayNameUp = array(  
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
				
		$get_eval_notes	= $this->rate_model->get_eval_notes($staffID,$evalFormID,$evalType);//get_eval_notes($staffID);			
		
		if($get_eval_notes == NULL){	
 			$data['insert_eval_note']	=	$this->rate_model->insert_eval_note($arrayName);
		}else{
			$data['insert_eval_note']	=	$this->rate_model->update_eval_note($staffID,$evalFormID,$arrayNameUp);
		}
		
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['var_agencies']	=$var_agencies;
		 $this->template->load('template/admin', 'appraisal_success',$data);
	}
	
	public function kpis(){
 		 $evalType				=	$this->input->get_post('evalType'); // evalRound
		 $eval					=	$this->input->get_post('eval'); // evalRound
		 $staffID				=	$this->input->get_post('staffID');
		 $ddl_count		    	=	count($this->input->get_post('ddl')) -1;	
		 $ddl					=	$this->input->get_post('ddl')  ;	
		 $evalSubjectID_count	=	count($this->input->get_post('evalSubjectID'))  ;	
		 $evalSubjectID			=	$this->input->get_post('evalSubjectID');
		 $evalFormID			=	$this->input->get_post('get_evalFormID');	
		 $var_agencies			=	$this->input->get_post('var_agencies');	
		 
		 $total = 0;

		 $get_eval 		=  $this->rate_model->get_eval($staffID,$evalFormID);
		if($get_eval == NULL){
			 for($i=0;$i<=$ddl_count;$i++){
	 			$total		 	+= $this->input->get_post($ddl[$i]);
	 			$ddl_value		=	$this->input->get_post($ddl[$i]); 
	 		  
	 			$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array('empID' => $staffID, 
	 								'evalFormID'=>$evalFormID,
	 								'evalSubjectID'=>$subject,
	 								'evalScore'=>$ddl_value,
									'evalType'=>$evalType
	 						);
	 			$insert_eval	=	$this->rate_model->insert_eval($arrayName);
	 		 }
	 	}else{
	 		for($i=0;$i<=$ddl_count;$i++){
	 			$total		 	+= $this->input->get_post($ddl[$i]);
	 			$ddl_value		=	$this->input->get_post($ddl[$i]); 
	 		  
	 			$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array( 
	 								 
	 								'evalScore'=>$ddl_value
	 						);
	 			$update_eval	=	$this->rate_model->update_eval($staffID,$evalFormID,$subject,$arrayName);
	 		 }
	 	}

		$evalNote1			= $this->input->get_post('evalNote1');
		$evalNote2			= $this->input->get_post('evalNote2');
		$data['data']		=	'';
		$evalType				=	$this->input->get_post('evalType'); // evalRound
		
		$arrayName = array( 'empID' => $staffID ,
							'evalFormID'=> $evalFormID,
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
					
		$arrayNameUp = array(  
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
				
		$get_eval_notes	= $this->rate_model->get_eval_notes($staffID,$evalFormID,$evalType);//get_eval_notes($staffID);			
		
		if($get_eval_notes == NULL){	
 			$data['insert_eval_note']	=	$this->rate_model->insert_eval_note($arrayName);
		}else{
			$data['insert_eval_note']	=	$this->rate_model->update_eval_note($staffID,$evalFormID,$arrayNameUp);
		}
		
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['var_agencies']	=$var_agencies;
		$this->template->load('template/admin', 'rate_doc_test_ok',$data);
	}

	public function additional(){
 		 
		 $eval				= $this->input->get_post('eval'); // evalRound
		 $staffID			= $this->input->get_post('staffID');
		 $total				= $this->input->get_post('total');
		 $evalFormID		= $this->input->get_post('evalFormID');
		 $evalNote1			= $this->input->get_post('evalNote1');
		 $evalNote2			= $this->input->get_post('evalNote2');
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		 # score 
 		 $maxScore	= $this->rate_model->tbl_eval_form_level($total);
		 $data['evalFormID']	=	$maxScore['evalFormID'];
		 $data['levelID']	=	$maxScore['levelID'];
		 $data['levelText']	=	$maxScore['levelText'];
		 $data['minScore']	=	$maxScore['minScore'];
		 $data['maxScore']	=	$maxScore['maxScore']; 
 		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['total']		=	$total;
		 
		 $data['data']		=	'';
		 $this->template->load('template/admin', 'rate_doc_test_level',$data); 
	}
	
	public function sum(){ 
		$eval				= $this->input->get_post('eval'); // evalRound
		$staffID			= $this->input->get_post('staffID');
		$total				= $this->input->get_post('total');
		$evalFormID			= $this->input->get_post('evalFormID');
		$evalNote1			= $this->input->get_post('evalNote1');
		$evalNote2			= $this->input->get_post('evalNote2');
		$data['data']		=	'';
		$var_agencies			= $this->input->get_post('var_agencies');
		$evalType				=	$this->input->get_post('evalType'); // evalRound
		
		$arrayName = array( 'empID' => $staffID ,
							'evalFormID'=> $evalFormID,
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
					
		$arrayNameUp = array(  
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
				
		$get_eval_notes	= $this->rate_model->get_eval_notes($staffID,$evalFormID,$evalType);//get_eval_notes($staffID);			
		
		if($get_eval_notes == NULL){	
 			$data['insert_eval_note']	=	$this->rate_model->insert_eval_note($arrayName);
		}else{
			$data['insert_eval_note']	=	$this->rate_model->update_eval_note($staffID,$evalFormID,$arrayNameUp);
		}
		
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['var_agencies']	=$var_agencies;
		$this->template->load('template/admin', 'rate_doc_test_ok',$data);
	}

	
	 
	public function tscore1(){
		
  		$varsd				= $this->input->get_post('sd');
		//echo  'varsd : '.$varsd;
		/// 2 - 09 - 2558 ///
		
		$eval_date			= $this->input->get_post('eval_date');//get evalRound
 		$evalYear			= $this->input->post('evalYear');//get evalYear
		$agencies			= $this->input->get_post('agencies'); 
 		/*$org_upper			= $this->rate_model->get_org_upper($agencies,$this->assignID);
 		foreach ($org_upper as $key => $value) {
			# code...
 			$org_array			=  $value ;
 		}
		echo $org_array;*/
		// $org_array			= $org_upper;
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
		//$data['eval_get']	= $this->rate_model->rate_emp($org_array,$eval_date); 
 		/// 1 - 09 - 2558 ///
  		$data['eval_date']				= $this->rate_model->get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown
		//$data['dropdown_org_chart'] = $this->organization_model->getDropdownTreeTscore($level = 0, $prefix = '' , $this->assignID ,$upperOrgID='' );#Dropdown  
		
		$data['dropdown_org_chart']  	= $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assignID);#Dropdown
		$data['get_evalScore'] 			= $this->rate_model->get_evalScore($agencies,$this->assignID); 
		
		$data['get_count_evalPerson']	= $this->rate_model->get_count_evalPerson($agencies); 
		 
		$data['get_count_evalScore']	= $this->rate_model->get_count_evalScore($agencies); 
		
		$data['varsd']			= $varsd;
		$data['data']			= '';
		$data['var_eval_date']	= $eval_date; //evalRound
		$data['var_evalYear']	= $evalYear;
		$data['var_agencies']	= $agencies; //agencies
		$this->template->load('template/admin', 'rate_tscore_test',$data);
		
  	} 
	
	public function sd(){
		 $sd						= $this->input->get_post('sd1');
		  
		 $data['data']		=	$sd ;
		
		$this->template->load('template/admin', 'rate_doc_test_sum1',$data);
		 //echo $type;
	}
	
	public function evaluation(){
		
		$data['data']		=	'';
		$this->template->load('template/admin', 'rate_doc_test_bomb',$data);
		
	}
	
	public function test(){
		
		$eval_date			= $this->input->post('eval_date');//get evalRound
		 
		$evalYear			= $this->input->post('evalYear');//get evalYear
		$agencies			= $this->input->post('agencies'); 
		 
		$data['eval_date']	= $this->rate_model->get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown
		 
		//$data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , $this->assignID ,$upperOrgID='' );#Dropdown  
  		$data['dropdown_org_chart']  = $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assignID);#Dropdown
		
		 
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
        $data['data']		=	'';
		$data['var_eval_date']	=	$eval_date; //evalRound
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:0); //agencies
		$data['assignID']		=	$this->assignID; //assignID
        $this->template->load('template/admin', 'test',$data);	
		
	}
	
	public function recent(){
		
		
		$eval_date				= $this->input->post('eval_date');//get evalRound
 		$evalYear				= $this->input->post('evalYear');//get evalYear
		$agencies				= $this->input->post('agencies'); 
 		$eval					= $this->input->get('eval'); 
		$staffID				= $this->input->get('staffID'); 
		$var_agencies			= $this->input->get('var_agencies'); 
		
		//parameter : recent
 		$recent_year		= $this->input->get_post('eval_date_year');
		$recent_evalRound	= $this->input->get_post('evalRound');
		// End Redirect
 		$data['eval_date_year']			= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown
 		
		$data['eval_date_roundRound']	= $this->recent_model->recent_get_dropdown_roundRound('evalYear' , 'evalRound' ,'tbl_eval_date',$recent_year);#Dropdown
   		if($recent_year != '' and $recent_evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assignID);#Dropdown
		    $data['roundRound']	= $this->recent_model->recent_get_dropdown_roundRound($recent_year,$recent_evalRound);#Dropdown
		}else{
			$data['org_faction']	=	'';
			$data['roundRound']		=	'';
		}
        $data['data']		=	'';
		$data['var_eval_date']	=	($eval_date !=''?$eval_date:($eval !=''?$eval:''));  
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:($var_agencies!=''?$var_agencies:0));  
		$data['assignID']		=	$this->assignID;  
		
		$data['recent_year']		=	$recent_year;  
		$data['recent_evalRound']	=	$recent_evalRound;  
       
		$data['data']		=	'';  
        $this->template->load('template/admin', 'recent',$data);
		
   }
   
   public function recentdll(){
	   
	   $recent_year		= $this->input->get_post('assignDate');
	   $roundRound		= $this->recent_model->recent_get_dropdown_roundRound($recent_year,'');#Dropdown
   		
	    echo json_encode($roundRound);
   
   }
   
   public function submit(){
	   
	   $year			= $this->input->post('year');
	   $round			= $this->input->post('round');
	   $var_agencies	= $this->input->post('var_agencies');
	   
	   $data			= array(
	   							"evalYear"=>$year,
								"evalRound"=>$round ,
								"orgID"=>$var_agencies,
								"completeDate"=>date('Y-m-d H:i:s')
	   					  );
	   $roundRound		= $this->recent_model->insert_submit($data);#Dropdown
	   if( $roundRound== 'succesfully'){
	   		redirect(site_url().'rate/recent?eval_date_year='.$year.'&evalRound='.$round.'&var_agencies='.$var_agencies.'', 'refresh');
	  		exit();
	   }
  }
  
  public function demo(){
	    $eval				= $this->input->get_post('eval'); // evalRound
		$staffID			= $this->input->get_post('staffID');
		$total				= $this->input->get_post('total');
		$evalFormID			= $this->input->get_post('evalFormID');
		$evalNote1			= $this->input->get_post('evalNote1');
		$evalNote2			= $this->input->get_post('evalNote2');
		$var_agencies		= $this->input->get_post('var_agencies');
		$data['data']		=	'';
		$data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['var_agencies']	=$var_agencies;
		$this->template->load('template/admin', 'rate_doc_test_ok',$data);  
  }
	
  public function xx1() {
		
		## Recent ##
   		$evalYear			= $this->input->post('evalYear'); 
		$agencies			= $this->input->post('agencies'); 
  	  	$evalRound			= $this->input->post('evalRound'); 
   		if($evalYear != '' and $evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assignID);#Dropdown
			$data['roundRound']		= $this->recent_model->recent_get_dropdown_roundRound($evalYear,$evalRound);#Dropdown
 		}else{
			$data['roundRound']		= '';	
 		}
 		
		## mode
		$data['eval_date_year']		= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown เลือกปีงบประมาณ
    	$data['dropdown_org_chart']	= $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assignID);#Dropdown หน่วยงาน
 		$data['eval_eval_submit']	= $this->rate_tscore_calculate->eval_submit($agencies,$evalYear,$evalRound);
 		$data['var_agencies']		= $agencies;
		$data['var_evalYear']		= $evalYear;
 		$data['var_assignID']		 = $this->assignID;
        $this->template->load('template/admin', 'rate_tscore_demo',$data);	 
 		 
     }   
	 
	public function xx2() {
		
		$year				= $this->input->get_post('year'); 
		$round				= $this->input->get_post('round'); 
		//$recentz			= $round.','.$year;
 		
		//$eval_date			= $this->input->post('eval_date');//get evalRound
 		
		
		//$evalYear			= $this->input->post('evalYear');//get evalYear
		//$agencies			= $this->input->post('agencies'); 
		 
		// Redirect  : [ eval=1,2558&staffID=161&var_agencies=82 ]
		$eval				= $this->input->get('eval'); 
		$staffID			= $this->input->get('staffID'); 
		$var_agencies		= $this->input->get('var_agencies'); 
	 	## Recent ##
   		$evalYear			= $this->input->post('evalYear'); 
		$agencies			= $this->input->post('agencies'); 
  	  	$evalRound			= $this->input->post('evalRound'); 
   		if($evalYear != '' and $evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assignID);#Dropdown
			$data['roundRound']		= $this->recent_model->recent_get_dropdown_roundRound($evalYear,$evalRound);#Dropdown
 		}else{
			$data['roundRound']		= '';	
			$data['org_faction']	= '';
 		}
 		
		## mode
		$data['eval_date_year']		= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown เลือกปีงบประมาณ 
   	    $data['dropdown_org_chart']  = $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assignID);#Dropdown
 		
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
        $data['data']		=	'';
		$data['var_eval_date']	=	$evalRound; //evalRound
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:($var_agencies!=''?$var_agencies:0)); //agencies
		$data['assignID']		=	$this->assignID; //assignID
		
		// recent_submit($evalYear,$evalRound,$orgID) 
		
		
		 
		
		## Recent ##
		$data['year']		=	$year;
		$data['round']		=	$round;
 		
        $this->template->load('template/admin', 'rate_tscore_demo',$data);	
		
 	}
	
 	public function tscore() {
	 
		$eval				= $this->input->get_post('eval'); 
		$staffID			= $this->input->get_post('staffID'); 
		$var_agencies		= $this->input->get_post('var_agencies'); 
	 	## Recent ##.
		$n					= $this->input->get_post('n'); 
		$sd					= $this->input->get_post('sd'); 
   		$evalYear			= $this->input->get_post('evalYear'); 
		$agencies			= $this->input->get_post('agencies'); 
  	  	$evalRound			= $this->input->get_post('evalRound');
		$average			= $this->input->get_post('average');
 		$squareAll			= $this->input->get_post('squareAll'); 
   		if($evalYear != '' and $evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assignID);#Dropdown
			$data['roundRound']		= $this->recent_model->recent_get_dropdown_roundRound($evalYear,$evalRound);#Dropdown
 		}else{
			$data['roundRound']		= '';	
			$data['org_faction']	= '';
 		}
 		
		## mode
		$data['eval_date_year']			= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown เลือกปีงบประมาณ 
   	    $data['dropdown_org_chart']  	= $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assignID);#Dropdown
 		
		$data['get_evalScore'] 			= $this->rate_model->get_evalScore($agencies,$this->assignID); 
		
		$data['get_count_evalPerson']	= $this->rate_model->get_count_evalPerson($agencies); 
		//echo "<pre>";
		//print_r($data['get_count_evalPerson']); 
		 
		 
		$data['get_count_evalScore']	= $this->rate_model->get_count_evalScore($agencies); 
		
		
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
        $data['data']		=	'';
		$data['var_eval_date']	=	$evalRound; //evalRound
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:($var_agencies!=''?$var_agencies:0)); //agencies
		$data['assignID']		=	$this->assignID; //assignID 
 		$data['average']		=	$average;
		$data['sd']				=	$sd	;
		$data['squareAll']		=	$squareAll;
		$data['n']				=	$n;
		## Recent ##
		//$data['year']		=	$year;
		//$data['round']		=	$round;
 		
        $this->template->load('template/admin', 'rate_tscore_demo',$data);	
		
		 
	}
	
	public function  tscore_sum(){
		
		$employee_id_count		    	=	count($this->input->post('employee_id')) ;	
		$employee_id					=	$this->input->post('employee_id')  ; 	
		
		//for($i=0;$i<=$employee_id_count;$i++){
			//$xx	= array_push($employee_id[$i]); 
		//}
		//echo "<pre>";
		//print_r($xx);
		//$var							=	$employee_id[ ];
		//  $employee 		= $this->rate_tscore->tScoreGetEmployee(array_push($employee_id)); 
		//$secret 		= $_POST['secret'];
		// $ar				= explode(',',$employee_id);
		
		//echo "<pre>";
		//print_r($ar);
	   // echo json_encode($employee_id_count);
	}
	
	public function ekpi(){
		$data['title']		=	'Job Profile';
		 $eval				= $this->input->get_post('eval'); // evalRound
		 $staffID			= $this->input->get_post('staffID'); 
		 $var_agencies	    = $this->input->get_post('var_agencies'); 
 		 
		 if($staffID  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 if($eval  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval,2); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		 ## แสดงส่วนของประเมิน กลุ่ม ##
		 
		 $data['get_eval_group_subject'] = $this->rate_model->get_eval_group_subject($data['get_evalFormID'],2);  
		 //echo "<pre>";
		 //print_r( $data['get_eval_group_subject']);
		## Sum 
		  
		// echo "<pre>";
		// print_r($data['get_eval_notes']);
		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID; 
		 $data['var_agencies']	=	$var_agencies; 
		 $data['data']		=	'';
		 $data['data']		=	'';
		## Note
		 $data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID,$eval_doc['evalFormID'],2);
 
		 $this->template->load('template/admin', 'EvalFormKPI-Edit',$data);
		 //$this->load->view("eval-kpi");
	
	}

}

?>