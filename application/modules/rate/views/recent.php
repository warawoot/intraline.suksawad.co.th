<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผลล่าสุด </h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('rate/recent') ?>" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">เลือกปีงบประมาณ</label>
                                <div class="col-sm-9">
                                    <?php 
                                        echo form_dropdown('eval_date_year',$eval_date_year,$recent_year,'class="form-control" onchange="changeRate(this);"  ');
                                    ?>
                                </div>
                             </div>
                             <div class="form-group">
                              <label class="control-label col-sm-3">เลือกรอบ</label>
                              	<div class="col-sm-9">
                                <?php if($roundRound ==''){?>
                                	<span id="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE">
                                         <select name="evalRound" id="evalRound" class="form-control">
                                            <option value="" selected="selected">กรุณาระบุรอบด้วย</option>
                                         </select>
                                    </span>
                                    <?php
									}else{
                                    	echo $roundRound;
									}
									?>
                                 </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">ค้นหา</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div> 
<br /><br />
<table class="xcrud-list table table-striped table-hover table-bordered">
    <thead>
        <tr class="xcrud-th">
            <th class="xcrud-num">#</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_eval_date.evalYear"> ฝ่าย </th>
            <th data-order="asc" data-orderby="tbl_eval_date.startDate" class="xcrud-column xcrud-action">ประเมินผลปฏิบัติงาน</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_eval_date.endDate"> ประเมินเสร็จเมื่อ </th>
         </tr>   
    </thead>
    <tbody>
        <?php 
         $num = 1 ;
         //$eval_get	= $this->rate_tsocre->rate_emp3($var_agencies,$assingID); 
         if(is_array($org_faction)){
            foreach ($org_faction as $key => $item) {
                # code...
				 $orgID				=	$item->orgID;
				 $completeDate		=	$item->completeDate;
				 $recent_submit		=	$this->recent_model->get_recent_submit($recent_year,$recent_evalRound,$orgID);
 				 
        ?>
            <tr class="xcrud-row xcrud-row-0" > 
                <td class="xcrud-current xcrud-num"><?php echo $key;?></td>
                <td> <?php echo $item->orgName; ?>   </td>
                <td>
                <?php if($recent_submit == false) {?>
                <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate');?>?year=<?php echo $recent_year;?>&round=<?php  echo $recent_evalRound ;?>&var_agencies=<?php echo $orgID;?>" title="">ประเมิน</a>
                <?php }else{?>
               		<a class="btn btn-default btn-sm btn-success" href="#" title=""> ประเมินแล้ว </a>
                <?php } ?>
                </td>
                <td> <?php echo ($completeDate != NULL?DateThaiHelper($completeDate):'-') ; ?></td>
             </tr>
         <?php }
            }else{
         ?>	   
            <tr class="xcrud-row xcrud-row-0"> 
                <td colspan="4" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
            </tr>
        <?php } ?>          
       </tbody>
    <tfoot>
   </tfoot>
</table>
  
<script type="text/javascript"> 

	function changeRate(obj){
    	//alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/recentdll') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   var obj = jQuery.parseJSON(response); 
				   //alert(response);
				   //console.log(obj); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(obj);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
				    location.replace('<?php echo base_url().'rate/recent' ?>');
				}
		});	
  	}
  
</script>
 