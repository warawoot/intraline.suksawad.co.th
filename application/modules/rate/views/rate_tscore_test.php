<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>รายงานการประเมินผล Grade / T-Score</h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('rate/tscore') ?>" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">เลือกปีงบประมาณ/รอบ</label>
                                <div class="col-sm-9">
                                    <?php 
                                        echo form_dropdown('eval_date',$eval_date,$var_eval_date,'class="form-control" ');
                                    ?>
                                </div>
                             </div><br />
                             <div class="form-group">
                                <label class="control-label col-sm-3">หน่วยงาน</label>
                                <div class="col-sm-9">
 									<!--<select class="xcrud-input form-control form-control" data-required="1" data-type="select" name="agencies" id="agencies" maxlength="11" >-->
                                         <?php  
                                            //echo $dropdown_org_chart;
											echo form_dropdown('agencies',$dropdown_org_chart,$var_agencies,'class="form-control"  id="agencies"');
                                         ?>
                                    <!--</select>-->
                                 </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">ค้นหา</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3> </h3>
            </header>
			   ﻿<table class="xcrud-list table table-striped table-hover table-bordered">
				<tr class="xcrud-th">
					<th style="width:150px">จำนวนคน</th>
					<td align="left">
						<?php 
							$n		=	 $get_count_evalPerson['person'];
							echo $n	; 
					   ?>
                    </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">คะแนนเฉลี่ย (xi) </th>
					<td align="left">
						<?php 
							$score		= $get_count_evalScore['score']; 
							$xiAverage	= $score / $n; 
							echo number_format($xiAverage , 0, '.', '')  ;
							
						?>                          
                       </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">(&Sigma;(xi-x)<sup>2</sup>) / N </th>
					<td align="left">
                    	<div class="xcrud-overlay-sigma" >
                            	<span id="sum_squ_sigma">&nbsp;</span>
                        </div>
                    </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">SD </th>
					<td align="left"> 
                    	<div class="xcrud-overlay-sd">
                            	<span id="sum_ad_x">&nbsp;</span>
                        </div>
                    </td>
				</tr>
			   </table>
	    	<br/><br/>
            <div class="panel-body">
                <link href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url(); ?>xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                        <div class="xcrud">
                            <div class="xcrud-container">
                                <div class="xcrud-ajax">
                                    
                                    <div class="xcrud-list-container">
                                    
                                     ﻿<table class="xcrud-list table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr class="xcrud-th">
                                                <th class="xcrud-num">#</th>
                                                <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_eval_date.evalYear">รหัสพนักงาน </th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.evalRound" class="xcrud-column xcrud-action">ชื่อ-นามสกุล</th>
                                                <th align="center">คะแนน </th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">เกรด</th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">xi-x</th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">(xi-x)<sup>2</sup></th>
                                                <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_eval_date.endDate"> T-Score</th>
                                        </thead>
                                        <tbody>
                                        <?php 
										 
										      foreach ($get_evalScore as $key => $item) {
                                                  # code...
                                        ?>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num"><?php echo ++$key; ?></td>
                                                    <td><?php echo $item->empID; ?> </td>
                                                    <td><?php echo $item->staffPreName; ?><?php echo $item->staffFName; ?>&nbsp;<?php echo $item->staffLName; ?></td>
                                                    <td align="right">
														<?php 
																$x	=	$item->s_evalScore  ;
																//echo 	$x ;//.'   ( สูตร : '.$item->s_evalScore .'/'. $n .')';
																echo number_format($x , 0, '.', '');
																$sum_x	 +=	 $x;
  														?>
                                                    </td>
                                                    <td align="center"><?php echo $item->grade; ?></td>
                                                    <td align="right">
                                                    	<?php
                                                        	$xi_x	= $x - $xiAverage;
															
															echo number_format($xi_x, 2, '.', '');
														?>
                                                    </td>
                                                    <td align="right">
                                                    	<?php
                                                        	$squares	=	($xi_x)*($xi_x);
															echo number_format($squares, 2, '.', '');
															$sum_squ	 +=	 $squares;
															
														?>
                                                    </td>
                                                    <td align="right"> 
                                                      <!--(((xi-x)/sd*10)+50)-->
                                                      <?php
                                                         		$tScore = $xi_x / $varsd * 10;
																echo number_format($tScore + 50, 2, '.', '');
														 ?>
                                                     </td>
                                                 </tr>
                                                
                                        <?php
                                              }
										 
										?>  
                                        		 
                                           </tbody>
                                        <tfoot>
                                       </tfoot>
                                    </table>
                                  </div>
                                </div>
                            <div class="xcrud-overlay" style="display: none;">
                            	<span id="sum_squ">
									<?php //echo $sum_squ ;
										 //$sum_squ = ($sum_squ > 0) ? $sum_squ : '0'; 
										 if($sum_squ ==''){
											 echo  '';
										 }else{
											 echo  $sum_squ;
										 }
										 
									?></span>
                            </div>
                         </div>
       				</div>
              </div>
         </section>
    </div>
</div>
 
<script type="application/javascript">
	
	var squares       = $('.xcrud-overlay').find('#sum_squ').text();	
	var sq_sum_all	  = squares / <?php echo $n;?>;
	var squares_sum	  = $('#sum_squ_sigma').html(sq_sum_all.toFixed(2));	
	 
	var sd			  = <?php echo $sum_squ;?> / <?php echo $n;?>;
 	var sd_sum		  = $('#sum_ad_x').html(Math.sqrt(sd).toFixed(2));
    var sd_sum_x	  = Math.sqrt(sd).toFixed(2);
	// Get Parameter SD 
	var XcrudSD  = "<?php echo $varsd;?>";
    if (XcrudSD == '') {
		location.href = '<?php echo base_url() ;?>rate/tscore?sd='+sd_sum_x+'&eval_date=<?php echo $var_eval_date; ?>&agencies=<?php echo $var_agencies;?>';
    }
				 
</script>
 