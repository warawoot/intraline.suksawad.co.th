<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading"> 
        <h3>ข้อมูลการประเมินผล </h3>
      </header>
      <div class="panel-body">
        <form action="<?php echo base_url('rate') ?>" method="get" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-sm-3">เลือกปีงบประมาณ/รอบ</label>
              <div class="col-sm-9">
              <?php 
                echo form_dropdown('eval_date',$eval_date,$var_eval_date,'class="form-control" ');
              ?>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">หน่วยงาน</label>
            <div class="col-sm-9">
              <select class="xcrud-input form-control form-control" data-type="select" name="agencies" id="agencies" maxlength="11"> 
              <?php 
              echo $dropdown_org_chart;
              ?>
              </select>   
            </div>
          </div>
          <div class="form-group" ><label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-9"> &nbsp; </div>
          </div>
       		<div class="form-group" ><label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-9"><button type="submit" class="btn btn-success">ค้นหา</button></div>
          </div>
          <div class="form-group" ><label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-9"> 
              <br/> 
              <table border="0" class="table" style="border:0px;border-top: 0px solid #ddd;" width="100%">
                <tr style="border:0px;border-top: 0px solid #ddd;">
                  <td width="33%" style="border:0px;border-top: 0px solid #ddd;"> วันที่ครบกำหนดการประเมิน </td>
                  <td width="67%" style="border:0px;border-top: 0px solid #ddd;"> <?php echo $dateDue;?> </td>
                </tr>
              </table>
            </div>
          </div> 
        </form>
      </div>
    </section>
  </div>
</div> 
 
<form action="<?php echo base_url('rate/submit') ?>" method="post" enctype="multipart/form-data">
  <table class="xcrud-list table table-striped table-hover table-bordered">
    <thead>
      <tr class="xcrud-th">
        <th class="xcrud-num">#</th>
        <th class="xcrud-column xcrud-action" style="width:10%">รหัสพนักงาน</th>
        <th class="xcrud-column xcrud-action" style="width:45%">หน่วยงาน / ชื่อ-นามสกุล</th>
        <th class="xcrud-column xcrud-action" style="width:15%;text-align:center">ประเมิน LOVE</th>
        <th class="xcrud-column xcrud-action" style="width:15%;text-align:center">ประเมินการทำงาน </th>
        <th class="xcrud-column xcrud-action" style="width:15%;text-align:center">ประเมิน Competency </th>
                    
    </thead>
    <tbody>
      <?php 
				 $num = 1 ;	 
				 $eval_get	= $this->rate_tsocre->rate_emp_party($var_agencies,$assingID); 
				 if(count($eval_get)>0){
            foreach ($eval_get as $key => $item) {
            ?>
            <tr class="xcrud-row xcrud-row-0"> 
              <td class="xcrud-current xcrud-num"><i class="fa fa-users"></i> </td>
              <td><?php echo $item->staffID; ?></td>
              <td><?php echo $item->staffPreName.$item->staffPreNameEtc." ".$item->staffFName." ".$item->staffLName;?></td>
              <td style="text-align:center">
                <?php
                  $get_eval_notes               = $this->rate_model->get_eval_notes($item->staffID,$evalFormID,$evalType);//
                  $eval_person                  = $this->rate_model->get_eval_count_people($item->staffID,$get_eval_notes['evalFormID'],1);
                  $count_proples_person         = $eval_person['count_proples'];
                  $eval_subject_group_subject   = $this->rate_model->get_eval_count_subject_group_subject($get_eval_notes['evalFormID']);
                  $subject_group_subject        = $eval_subject_group_subject['subject_group_subject'];

                  if($eval_person['count_proples'] == 0){
                  ?>
                  <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->staffID;?>&var_agencies=<?php echo $var_agencies;?>" title="" style="width:100px">ประเมิน</a>
                    <?php 
                  }elseif($eval_person > 0){ 
                    $get_eval_nedit = $this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
                  ?>
                  <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/eassessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->staffID?>&var_agencies=<?php echo $var_agencies;?>" title="" style="width:100px"><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
                  <?php }//if($eval_person['count_proples'] == 0) ?>                  
              </td>
              <td style="text-align:center">
                <?php
                  $eval_person         = $this->rate_model->get_eval_count_people($item->staffID,$get_eval_notes['evalFormID'],2);
                  if($eval_person['count_proples'] == 0){
                ?>
                  <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/kpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->staffID ?>&var_agencies=<?php echo $var_agencies;?>" title="" style="width:100px">ประเมิน</a>
                <?php }else{?>
                  <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/ekpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->staffID ?>&var_agencies=<?php echo $var_agencies;?>" title="" style="width:100px"><i class="fa fa-pencil-square-o"></i>ประเมินแล้ว</a>
                <?php  
                  }//if($eval_person['count_proples'] == 0)?>                
              </td>
              <td style="text-align:center">
                <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->staffID ?>&var_agencies=<?php echo $var_agencies;?>" title="" style="width:100px">ประเมิน</a>                
              </td>
            </tr> 
            <?php }//foreach ($eval_get as $key => $item)
          }else{//if(count($eval_get)>0)
          ?>	   
            <tr class="xcrud-row xcrud-row-0"> 
              <td colspan="6" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
            </tr>
          <?php 
          } //if(count($eval_get)>0)?>          
          </tbody>
        </table>
 <div class="form-group" style="text-align: right;">
 <strong style="color:red">หากยืนยันการส่งข้อมูลแล้วจะไม่สามารถแก้ไขการประเมินได้</strong><br/>
 <?php 
 
		if($var_eval_date !=''){
				$var_eval_date	=	explode(',',$var_eval_date);	
				$get_eval_date		=	$var_eval_date[1];
				$get_eval_round		=	$var_eval_date[0];
		}
		$year 				= ($year !=''?$year:$get_eval_date);
		$round 				= ($round !=''?$round:$get_eval_round);
		$recent_submit		= $this->recent_model->recent_submit($year,$round,$var_agencies);	
		
		if($year !='' and $round !='' and $var_agencies !='') {
 			if($recent_submit == false){
			
 ?>
                  <input name="year" id="year" type="hidden" value="<?php echo  $year ;?>" />
                  <input name="round" id="round" type="hidden" value="<?php echo $round ;?>" />
                  <input name="var_agencies" id="var_agencies" type="hidden" value="<?php echo $var_agencies;?>" />
                  <button type="submit" class="btn btn-warning"  ><i class="fa fa-plus-circle"></i> ส่งข้อมูลประเมิน</button>
  <?php 	}//else{
	  
  		}
	    //}
	//}
  ?>
 </div>
</form>
 

<script type="text/javascript"> 
	function changeRate(obj){
    	alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/xx') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   // var obj = jQuery.parseJSON(response); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(response);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
					location.replace('<?php echo base_url().'rate/' ?>');
				}
		});	
  	}
 
 
</script>
 