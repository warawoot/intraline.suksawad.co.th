<style type="text/css">
	.form-control {
 	  color: #343232;
	 
	}
  .table-results tr td{
    padding:5px !important;
    vertical-align: middle !important;
  }
  .table-results thead th{
    border-top: none; 
    border-bottom: none;
    border: 0 !important; 
  }
    
  span.spinner {
    position: absolute;
    height: 40px;
    margin-top: -9px;
    user-select: none;
    -ms-user-select: none;
    -moz-user-select: none;
    -webkit-user-select: none;
    -webkit-touch-callout: none;
  }
    
  span.spinner > .sub,
  span.spinner > .add {
    float: left;
    display: block;
    width: 20px;
    height: 25px;
    text-align: center;
    font-family: Lato;
    font-weight: 900;
    font-size: 1.2em;
    line-height: 25px;
    cursor: pointer;
    transition: 0.1s linear;
    -o-transition: 0.1s linear;
    -ms-transition: 0.1s linear;
    -moz-transition: 0.1s linear;
    -webkit-transition: 0.1s linear;
  }
    
  span.spinner > .add {
    top: 0;
    color: #000;
    font-weight:bold;
    background-color:#CEF6CE;
    border : solid #FFF 1px;
  }

  span.spinner > .add:hover {
    background: #088A08;
    color: #FFF;
  }

  span.spinner > .sub {
    top: 0;
    color: #000;
    font-weight:bold;
    background-color:#F6CECE;
    border : solid #FFF 1px;
  }

  span.spinner > .sub:hover {
    background: #B40404;
    color: #FFF;
  }

  input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }

  th{
    color:#FFF !important;
    text-align:center;
  }

  .excel-style {
    padding : 0px !important;
  }

  .excel-form {
    width : 100% !important;
    line-height : 50px !important;
    height : 50px !important;
    border : none !important; 
    background-color: transparent !important; 
  }

  select.excel-form {
    width : 700px !important;
    line-height : 50px !important;
    height : 50px !important;
    border : none !important; 
    background-color: transparent !important; 
  }

  input[type=number].excel-form {
    margin-top : 15px !important;
    width : 120px !important;
    text-align:right;
    font-size:24px;
    font-weight:bold;
    display:table-cell;
  }

  .summary {
    padding-right : 10px !important;
    text-align:right;
    font-size:24px;
    font-weight:bold;
  }

</style>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
      <header class="panel-heading"> 
          <h3>แบบประเมินผลการปฏิบัติงานประจำปี : <?=$get_evalNameText;?></h3>
      </header>
      <div class="panel-body">
        <form  id="myForm" method="post" action="<?=base_url() ?>rate/competencysubmit" enctype="multipart/form-data">
          <table class="table table-striped" style="background-color: #fff; font-size:14px;">
            <tr>
              <td>
                <table width="100%" border="0">
                  <tr>
                    <td colspan="7" align="center" style=" font-size:16px;">
                      <strong>  </strong> 
						          <?php echo $get_evalYear;?> <strong> ครั้งที่ </strong>
						          <?php echo $get_evalRound;?> ( <strong>วันที่</strong> <?php echo $get_startDate;?> - <?php echo $get_endDate;?>) 
                      <input name="evalType" id="evalType" type="hidden" value="1" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7"><strong>ชื่อ-นามสกุล</strong> <?php  echo $get_staffPreName . $get_staffFName .'&nbsp;&nbsp;'. $get_staffLName;?></td>
                  </tr>
                  <?php
                    $party		    =	$this->rate_model->structural_work($var_agencies,'party',6);						 
                    $staffIDz	    =	$this->rate_model->structural_staff($staffID);
                    $department		=	$this->rate_model->structural_work($staffIDz['orgID'],'department',6);
                    $pile		      =	$this->rate_model->structural_work($department['upperOrgID'],'pile',6);
				          ?>
                </table>
                <br>

                <div class="row">
                  <div class="col-md-3">
                    <div class="pastel-pink" style="padding:20px">
                      <h4>ลาป่วย : <?=$sa1["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?=$ac1["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-medkit"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-blue" style="padding:20px">
                      <h4>ลากิจ : <?=$sa2["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?=$ac2["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-briefcase"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-green" style="padding:20px">
                      <h4>ลาพักผ่อน : <?=$sa7["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?=$ac7["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-ios-partlysunny-outline"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-yellow" style="padding:20px">
                      <h4>ลาอื่นๆ : <?=$sao["numDay"]?> วัน</h4>
                      <h6>&nbsp;</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-email"></i>
                    </div>
                  </div>
                </div> 
                
              </td>
            </tr>
            <tr>
              <td>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:left"><strong>โปรดให้คะแนนที่ประเมินในแต่ละปัจจัย</strong></th>
                      <th colspan="3"><strong>ระดับคะแนนที่ให้</strong></th>
                    </tr>
                    <tr style="font-weight:bold;">
                      <th style="text-align:left" colspan="4">หัวข้อปัจจัยที่ประเมิน</th>
                    </tr>
                    <?php
                    if($rankID > 0) {
                    ?>
                    <tr>
                      <th style="text-align:left"><strong>Managerial Competency and Leadership ความสามารถด้านการบริหารและภาวะผู้นำ</strong></th>
                      <th style="width:120px !important;">ระดับที่คาดหวัง (A)</th>
                      <th style="width:120px !important;">คะแนน (B)</th>
                      <th style="width:120px !important;">GAP (A - B)</th>
                    </tr>
                    <?php
                      for($i=1;$i<=4;$i++)
                      {
                        $weightDataM = 0;
                        $scoreDataM = 0;
                        if($i <= sizeof($ddl_editM)) {
                          $weightDataM  = $ddl_editM[$i-1]->evalVariable;
                          $scoreDataM   = $ddl_editM[$i-1]->evalScore;
                        }
                    ?>
                    <tr>
                      <td class="excel-style"> 
                        <?php
                            echo form_dropdown('ddl_subjectM_'.$i,$ddl_subjectM,$ddl_editM[$i-1]->evalSubjectID,' class="excel-form"');
                        ?>
                      </td>  
                      <td class="excel-style">
                          <input type="number" 
                              min="0" 
                              max="10" 
                              value="<?=$weightDataM?>"  
                              name="weightM_<?=$i?>" 
                              id="weightM_<?=$i?>" 
                              class="excel-form"
                              onKeyUp="setvallengthM(this,0,10,<?=$i?>);"
                              />
                      </td>
                      <td class="excel-style">
                          <input type="number" 
                              min="0" 
                              max="10" 
                              placeholder="0.00"  
                              step="0.01" 
                              value="<?=$scoreDataM?>"  
                              name="scoreM_<?=$i?>" 
                              id="scoreM_<?=$i?>" 
                              class="excel-form"  
                              onKeyUp="setvallengthM(this,0,10,<?=$i?>);"
                              />
                      </td>
                      <td class="summary"><span id="calScoreM_<?=$i?>">0</span></td>
                    </tr>
                    <?php    
                      }
                    ?>
                    <tr style="font-weight:bold;">
                        <td style="text-align:right"><h4 class="ge_score_test">น้ำหนักรวม/คะแนนรวม</h4></td>
                        <td class="summary"><span id="totalWeightM">0</span></td>
                        <td class="summary"><span id="totalScoreM">0</span></td>
                        <td style="text-align:center">-</td>
                    </tr>
                    <?php
                    }
                    ?>
                    <tr>
                      <th style="text-align:left"><strong>Knowledge and Skill ความรู้และทักษะที่จำเป็นในงาน(Functional Competency)</strong></th>
                      <th style="width:120px !important;">ระดับที่คาดหวัง (A)</th>
                      <th style="width:120px !important;">คะแนน (B)</th>
                      <th style="width:120px !important;">GAP (A - B)</th>
                    </tr>
                  </thead>
                  <?php
                    for($i=1;$i<=15;$i++)
                    {
                      $weightData = 0;
                      $scoreData = 0;
                      if($i <= sizeof($ddl_edit)) {
                        $weightData = $ddl_edit[$i-1]->evalVariable;
                        $scoreData = $ddl_edit[$i-1]->evalScore;
                      }
                  ?>
                    <tr>
                      <td class="excel-style"> 
                        <?php
                            echo form_dropdown('ddl_subject_'.$i,$ddl_subject,$ddl_edit[$i-1]->evalSubjectID,' class="excel-form"');
                        ?>
                      </td>  
                      <td class="excel-style">
                          <input type="number" 
                              min="0" 
                              max="10" 
                              value="<?=$weightData?>"  
                              name="weight_<?=$i?>" 
                              id="weight_<?=$i?>" 
                              class="excel-form"
                              onKeyUp="setvallength(this,0,10,<?=$i?>);"
                              />
                      </td>
                      <td class="excel-style">
                          <input type="number" 
                              min="0" 
                              max="10" 
                              placeholder="0.00"  
                              step="0.01" 
                              value="<?=$scoreData?>"  
                              name="score_<?=$i?>" 
                              id="score_<?=$i?>" 
                              class="excel-form"  
                              onKeyUp="setvallength(this,0,10,<?=$i?>);"
                              />
                      </td>
                      <td class="summary"><span id="calScore_<?=$i?>">0</span></td>
                    </tr>
                    <?php    
                      }
                    ?>
                    <tr style="font-weight:bold;">
                        <td style="text-align:right"><h4 class="ge_score_test">น้ำหนักรวม/คะแนนรวม</h4></td>
                        <td class="summary"><span id="totalWeight">0</span></td>
                        <td class="summary"><span id="totalScore">0</span></td>
                        <td style="text-align:center">-</td>
                    </tr>
                    <?php 
                        $get_apen_name = $this->rate_model->get_eval_subject($value->evalGroupSubjectID);
                        
                        $num_score	=	1;
                        foreach ($get_apen_name as $key => $item) {  
                        $ddl = 'score_max_min_'.$item->evalSubjectID ;
                            // echo $ddl;
                            $get_eval_nedit	=	$this->rate_model->get_eval_edit($staffID,$get_evalFormID,$item->evalSubjectID);
                            $evalWeight		=	$item->evalWeight;
                            $evalScore		=	($get_eval_nedit['evalScore'] !=''?$get_eval_nedit['evalScore']:0);
                    ?>
                    <?php     
                        }
                    ?> 
                    <div class="score_max" style="display:none;">
                        <span id="get_score_max"><?php echo $nums;?></span>
                        
                    </div>
      
                </table></td>
                </tr>
        </table>
        <div style="display:none">
            <input type="text" name="eval" id="eval" value="<?=$eval?>" />
            <input type="text" name="staffID" id="staffID" value="<?=$staffID?>" />
            <input type="text" name="rankID" id="rankID" value="<?=$rankID?>" />
            <input type="text" name="get_evalFormID" id="get_evalFormID" value="<?=$get_evalFormID?>" /> 
            <input type="text" name="var_agencies" id="var_agencies" value="<?=$var_agencies?>" />
        </div>
        <button type="submit" class="btn btn-success">บันทึก</button>
        <button type="button" class="btn btn-warning">ยกเลิก</button>
    </form>
    </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 

function setvallengthM(obj,minval,maxval,i) 
{
  if(obj.value>maxval){obj.value=maxval;} else if(obj.value<minval){obj.value=minval;}
  calculateScoreM(i);
  calculateTotalWeightM();
  calculateTotalScoreM();
}

function calculateScoreM(i)
{
  var weightX = 0;
  var scoreX = 0;
  var calScore = 0;
  weightX = parseInt($('#weightM_'+i).val());
  scoreX  = parseFloat($('#scoreM_'+i).val());
  calScore = weightX - scoreX;
  $('#calScoreM_'+i).html(calScore.toFixed(2));
}

function calculateTotalWeightM()
{
  var weightX = 0;
  for(i=1;i<=4;i++) {
    weightX += parseInt($('#weightM_'+i).val());
  }
  $('#totalWeightM').html(weightX);
}

function calculateTotalScoreM()
{
  var scoreX = 0;
  for(i=1;i<=4;i++) {
    scoreX += parseFloat($('#scoreM_'+i).val());
  }
  $('#totalScoreM').html(scoreX.toFixed(2));  
}

function setvallength(obj,minval,maxval,i) 
{
  if(obj.value>maxval){obj.value=maxval;} else if(obj.value<minval){obj.value=minval;}
  calculateScore(i);
  calculateTotalWeight();
  calculateTotalScore();
}

function calculateScore(i)
{
  var weightX = 0;
  var scoreX = 0;
  var calScore = 0;
  weightX = parseInt($('#weight_'+i).val());
  scoreX  = parseFloat($('#score_'+i).val());
  calScore = weightX - scoreX;
  $('#calScore_'+i).html(calScore.toFixed(2));
}

function calculateTotalWeight()
{
  var weightX = 0;
  for(i=1;i<=15;i++) {
    weightX += parseInt($('#weight_'+i).val());
  }
  $('#totalWeight').html(weightX);
}

function calculateTotalScore()
{
  var scoreX = 0;
  for(i=1;i<=15;i++) {
    scoreX += parseFloat($('#score_'+i).val());
  }
  $('#totalScore').html(scoreX.toFixed(2));  
}

var delay = 0;
var offset = 80;

document.addEventListener('invalid', function(e){
    $(e.target).addClass("invalid");
    $('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset }, delay);
}, true);
document.addEventListener('change', function(e){
    $(e.target).removeClass("invalid")
}, true);

//ตรวจสอบก่อนส่งค่าไปบันทึก
/*
$('form').submit(function () {
  var weightX = 0;
  weightX = parseInt($('#totalWeight').html());
  if (weightX  != 100) {
      alert('กรุณาทำให้ผลรวมน้ำหนักคะแนนเป็น 100');
      return false;
  } 
});
*/

$(function() {
  for(i=1;i<=15;i++) {
    calculateScore(i);
  }
  calculateTotalWeight();
  calculateTotalScore();

  for(i=1;i<=4;i++) {
    calculateScoreM(i);
  }
  calculateTotalWeightM();
  calculateTotalScoreM();
});

</script>