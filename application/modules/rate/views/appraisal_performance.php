<style type="text/css">
	.form-control {
 	  color: #343232;
	}
  
  .table-evaluation tr td{
    padding:7px !important;
    vertical-align: middle !important;
  }
  .table-evaluation thead th{
  border-top: none; 
  border-bottom: none;
  border: 0 !important; 
  }  

  input[type=number] {
    float: left;
    width: 30px !important;
    height: 25px !important;
    padding: 0;
    font-size: 1.2em;
    text-transform: uppercase;
    text-align: center;
    color: #93504C;
    border: 2px #93504C solid;
    background: none;
    outline: none;
    pointer-events: none;
  }
    
  span.spinner {
    position: absolute;
    height: 40px;
    margin-top: -9px;
    user-select: none;
    -ms-user-select: none;
    -moz-user-select: none;
    -webkit-user-select: none;
    -webkit-touch-callout: none;
  }
    
  span.spinner > .sub,
  span.spinner > .add {
    float: left;
    display: block;
    width: 20px;
    height: 25px;
    text-align: center;
    font-family: Lato;
    font-weight: 900;
    font-size: 1.2em;
    line-height: 25px;
    cursor: pointer;
    transition: 0.1s linear;
    -o-transition: 0.1s linear;
    -ms-transition: 0.1s linear;
    -moz-transition: 0.1s linear;
    -webkit-transition: 0.1s linear;
  }
    
  span.spinner > .add {
    top: 0;
    color: #000;
    font-weight:bold;
    background-color:#CEF6CE;
    border : solid #FFF 1px;
  }

  span.spinner > .add:hover {
    background: #088A08;
    color: #FFF;
  }

  span.spinner > .sub {
    top: 0;
    color: #000;
    font-weight:bold;
    background-color:#F6CECE;
    border : solid #FFF 1px;
  }

  span.spinner > .sub:hover {
    background: #B40404;
    color: #FFF;
  }

  input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }

  th{
    color:#FFF !important;
  }

</style>
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading"> 
          <h3>แบบประเมินผลการปฏิบัติงานประจำปี (<?=$get_evalNameText;?>)</h3>
      </header>
      <div class="panel-body">
        <form  id="myForm" method="post" action="<?=base_url() ?>rate/docs" enctype="multipart/form-data">
          <table class="table table-striped" style="background-color: #fff; font-size:14px;">
            <tr>
              <td>
                <table width="100%" border="0">
                  <tr>
                    <td colspan="7" align="center" style=" font-size:16px;">
                      <strong>  </strong> 
						          <?php echo $get_evalYear;?> <strong> ครั้งที่ </strong>
						          <?php echo $get_evalRound;?> ( <strong>วันที่</strong> <?php echo $get_startDate;?> - <?php echo $get_endDate;?>) 
                      <input name="evalType" id="evalType" type="hidden" value="1" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7"><strong>ชื่อ-นามสกุล</strong> <?php  echo $get_staffPreName . $get_staffFName .'&nbsp;&nbsp;'. $get_staffLName;?></td>
                  </tr>
                  <?php
                    $party		    =	$this->rate_model->structural_work($var_agencies,'party',6);						 
                    $staffIDz	    =	$this->rate_model->structural_staff($staffID);
                    $department		=	$this->rate_model->structural_work($staffIDz['orgID'],'department',6);
                    $pile		      =	$this->rate_model->structural_work($department['upperOrgID'],'pile',6);
				          ?>
                  <!--
                  <tr>
                    <td><strong><!--แผนก--
                    <?php echo $department['orgName']; ?></strong></td>
                    <td>&nbsp; </td>
                    <td>&nbsp;</td>
                    <td><strong><!--กอง--
                    <?php echo $pile['orgName']; ?></strong></td>
                    <td>&nbsp;</td>
                    <td><strong><!--ฝ่าย--</strong></td>
                    <td>&nbsp;<strong><?php echo $party['orgName']; ?></strong></td>
                  </tr>
                  -->
                </table>
                <br>

                <div class="row">
                  <div class="col-md-3">
                    <div class="pastel-pink" style="padding:20px">
                      <h4>ลาป่วย : <?=$sa1["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?=$ac1["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-medkit"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-blue" style="padding:20px">
                      <h4>ลากิจ : <?=$sa2["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?=$ac2["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-briefcase"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-green" style="padding:20px">
                      <h4>ลาพักผ่อน : <?=$sa7["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?=$ac7["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-ios-partlysunny-outline"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-yellow" style="padding:20px">
                      <h4>ลาอื่นๆ : <?=$sao["numDay"]?> วัน</h4>
                      <h6>&nbsp;</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-email"></i>
                    </div>
                  </div>
                </div> 
                
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" class="table table-condensed table-evaluation">
                  <thead>
                  <tr>
                    <th colspan="2"><strong>โปรดเลือกคะแนนที่ประเมินในแต่ละปัจจัย</strong></th>
                    <th><strong>ระดับคะแนนที่ให้</strong></th>
                  </tr>
                  <tr>
                    <th colspan="2"><strong>หัวข้อปัจจัยที่ประเมิน</strong></th>
                    <th style="width:200px !important"><strong>คะแนน</strong>&nbsp;</th>
                  </tr>
                  </thead>
                  <tbody>
              <?php 
              if(is_array($get_eval_group_subject)){ 
						      $num = 1;
               		foreach ($get_eval_group_subject as $key => $value) { # code...  tbl_eval_group_subject
							     $nums	=	$num++;
							     $sum_eval_subject = $this->rate_model->get_sum_eval_subject($value->evalGroupSubjectID);# code...  tbl_eval_subject
		          ?>
                  <tr>
                    <th colspan="2"><strong><?php echo $value->evalGroupSubjectText;?></strong></th>
                    <th><!--<strong>น้ำหนักของปัจจัยที่ <?php echo $nums;?> = [ </strong><?php echo $sum_eval_subject['maxScore']; ?><strong> ] คะแนน</strong>&nbsp;--></th>
                  </tr>
              <?php 
							$get_apen_name = $this->rate_model->get_eval_subject($value->evalGroupSubjectID);
							//echo "<pre>";
							//print_r($get_apen_name);
               				$num_score	=	1;
							  foreach ($get_apen_name as $key => $item) {  
							   $ddl = 'score_max_min_'.$item->evalSubjectID ;
							   // echo $ddl;
							   $get_eval_nedit	=	$this->rate_model->get_eval_edit($staffID,$get_evalFormID,$item->evalSubjectID);
							    
                 # code...
             ?>
                  <input name="ddl[]" id="ddl[]" type="hidden" value="<?php echo $ddl;?>" />
                  <tr style="font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                  <td colspan="2" style="padding:20px !important;">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item->evalSubjectText; ?></td><!--onChange="changeTest_<?php //echo $item->evalSubjectID; ?>(this)"-->
                  <td>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="number" 
                          min="<?=$item->minScore;?>" 
                          max="<?=$item->maxScore;?>" 
                          value="<?php echo ($get_eval_nedit['evalScore'] !=''?$get_eval_nedit['evalScore']:0);?>" 
                          name="score_max_min_<?php echo $item->evalSubjectID; ?>" 
                          id="score_max_min_<?php echo $item->evalSubjectID; ?>" 
                          onChange="changeTest_<?php echo $item->evalSubjectID; ?>(this)"/>
                      </div>
                      <div class="col-md-6">
                        ( <?=$item->minScore;?> - <?=$item->maxScore;?> )
                      </div>
                    </div>
           				  <div class="list_score">
                      <span id="list_score_<?php echo $nums;?>_<?php echo $item->evalSubjectID; ?>"></span>
                    </div>
                    <div style="display:none">
                     	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="<?php echo $item->evalSubjectID;?>" />
                    </div>
                  </td>
                </tr>
              <?php     
                  }
       			   ?>
              <div class="list_count">
                <span id="list_count_<?php echo $nums;?>"></span>
              </div>
              <span id="list_count_test_<?php echo $nums;?>"></span>
                
           		<?php
                }
              ?>  
              <div class="score_max" style="display:none;">
              	<span id="get_score_max"><?php echo $nums;?></span>
                  
              </div>
                        
              <?php }else{
              ?>
              <tr>
                <td colspan="3" align="center">ไม่พบข้อมูล</td>
              </tr>
              <?php  }   ?>

            </table></td>
          </tr>
          <tr>
            <td align="right">
              <div class="ge_score_test"><h4>รวมคะแนนประเมิน = <span id="totalScore"></span></h4></div>
            </td>
          </tr>                   
          <tr>
            <td align="right">
            <table width="100%" border="0">
              <tbody>
              <!--
              <tr>
                <td><strong>1.ระดับผลการประเมิน &nbsp;&nbsp;</strong></td>
                <td id="evalResults"></td>
              </tr>
              -->
              <tr>
                <td><strong>ควรได้รับการพัฒนาเพิ่มเติมในเรื่อง  </strong></td>
                <td>
                  <textarea name="evalNote1" id="evalNote1" cols="45" rows="5"><?php echo ($get_eval_notes['evalNote1'] !=''? $get_eval_notes['evalNote1']: ''  )?></textarea>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><strong>ข้อคิดเห็นเพิ่มเติม </strong></td>
                <td>
                  <textarea name="evalNote2" id="evalNote2" cols="45" rows="5"><?php echo ($get_eval_notes['evalNote2'] !=''? $get_eval_notes['evalNote2']: ''  )?></textarea>
                </td>
              </tr>
            </tbody></table>
            </td>
          </tr>

        </table>
       <div style="display:none">
            <input type="text" name="eval" id="eval" value="<?php echo $eval;?>" />
            <input type="text" name="staffID" id="staffID" value="<?php echo $staffID;?>" />
            <input type="text" name="get_evalFormID" id="get_evalFormID" value="<?php echo $get_evalFormID;?>" /> 
            <input type="text" name="var_agencies" id="var_agencies" value="<?php echo $var_agencies;?>" />
        </div>

        <button type="submit" class="btn btn-success">บันทึก</button>
        &nbsp;
        <button type="button" class="btn btn-warning">ยกเลิก</button>


      </form>
    </div>
     
  </section>
  </div>
</div>
<script type="text/javascript"> 
  
function calculateTotalScore(){
    var totalScore = 0; 
    var percentScore = 0;
      $(".spinner").find('input[type=number]').map(function(){
        totalScore += parseInt(this.value);
    }).get();
    percentScore = totalScore / 2;
    $('#totalScore').html(totalScore + " คะแนน (" + percentScore + "%)");
    //updateLevel(totalScore);
}

var nextLevelScore = -1;
var prevLevelScore = 0;
var lookingForLevel = false;

function updateLevel(totalScore){
  if((totalScore > nextLevelScore || totalScore < prevLevelScore) && !lookingForLevel){
    $("#evalResults").html('loading ...');
    lookingForLevel = true;
    $.getJSON( "<?php echo base_url(); ?>rate/getScoreAjax", {total:totalScore}, function( data ) {
      prevLevelScore = parseInt(data.minScore);
      nextLevelScore = parseInt(data.maxScore);
      $("#evalResults").html('<strong>ระดับ ' + data.levelText + ' &nbsp;&nbsp;&nbsp;&nbsp;( </strong>' + data.minScore + ' - ' + data.maxScore + ' <strong>)</strong>');
    })
    .fail(function(jqXHR, textStatus, errorThrown) { $("#evalResults").html("Error getting results"); })
    .always(function() { lookingForLevel = false; });
  }

}

(function($) {
  $.fn.spinner = function() {
    this.each(function() {
    var el = $(this);

    // add elements
    el.wrap('<span class="spinner"></span>');     
    el.before('<span class="sub">-</span>');
    el.after('<span class="add">+</span>');

    // substract
    el.parent().on('click', '.sub', function () {
    if (el.val() > parseInt(el.attr('min')))
      el.val( function(i, oldval) { return --oldval; });
      calculateTotalScore();
    });

    // increment
    el.parent().on('click', '.add', function () {
    if (el.val() < parseInt(el.attr('max')))
      el.val( function(i, oldval) { return ++oldval; });
      calculateTotalScore();
    });

    });
  };
})(jQuery);
		
	 $('input[type=number]').spinner();
   calculateTotalScore();

  //The only script I found to scroll with offset on html5 error
  var delay = 0;
  var offset = 80;

  document.addEventListener('invalid', function(e){
     $(e.target).addClass("invalid");
     $('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset }, delay);
  }, true);
  document.addEventListener('change', function(e){
     $(e.target).removeClass("invalid")
  }, true);

</script>