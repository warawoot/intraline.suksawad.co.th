<style type="text/css">
	.form-control {
 	  color: #343232;
	 
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล</h3>
            </header>
            <div class="panel-body">
              <form  id="myForm" method="post" action="<?php echo base_url() ?>rate/docs" enctype="multipart/form-data">
                <table class="table" style="background-color: #fff; font-size:14px;">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td colspan="7" align="center" style=" font-size:16px;"><strong> แบบประเมินผลการปฏิบัติงานประจำปี </strong> <?php echo $get_evalYear;?> <strong> ครั้งที่ </strong><?php echo $get_evalRound;?> ( <strong>วันที่</strong> <?php echo $get_startDate;?> - <?php echo $get_endDate;?>) </td>
                      </tr>
                      <tr>
                        <td colspan="7" align="center"><strong> [ </strong><?php echo $get_evalNameText;?><strong> ] </strong></td>
                      </tr>
                      <tr>
                        <td><strong>ชื่อ-นามสกุล</strong></td>
                        <td><?php  echo $get_staffPreName . $get_staffFName .'&nbsp;&nbsp;'. $get_staffLName;?></td>
                        <td>&nbsp;</td>
                        <td><strong>ตำแหน่ง</strong></td>
                        <td><?php echo $get_positionName;?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>แผนก</strong></td>
                        <td>xx</td>
                        <td>&nbsp;</td>
                        <td><strong>กอง</strong></td>
                        <td>xx</td>
                        <td><strong>ฝ่าย</strong></td>
                        <td>xxx</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0">
                      <?php 
                      if(is_array($get_eval_group_subject)){ 
         			           $num = 1;
                         foreach ($get_eval_group_subject as $key => $value) {      # code... 
        					        $nums	=	$num++;
        					        $sum_eval_subject = $this->rate_model->get_sum_eval_subject($value->evalGroupSubjectID);
                      ?>
                      <?php 
          						  $get_apen_name = $this->rate_model->get_eval_subject($value->evalGroupSubjectID);
          						  $num_score	=	1;
          						   foreach ($get_apen_name as $key => $item) {  
								   $ddl = score_max_min_.$item->evalSubjectID ;
								   echo $ddl;
								 
                             # code...
                      ?>
                      <input name="ddl[]" id="ddl[]" type="hidden" value="<?php echo $ddl;?>" />
                       <?php     
                          }
          					   ?>
                               <div class="list_count">
                                <span id="list_count_<?php echo $nums;?>"></span>
                              </div>
                              <span id="list_count_test_<?php echo $nums;?>"></span>
           					   <?php
                            }
          						  ?>  
                        <div class="score_max">
                        	<span id="get_score_max"><?php echo $nums;?></span>
                            
                        </div>
                        
                      <?php }else{
                      ?>
                      <?php  }   ?>
                      <tr>
                        <td align="right">
                           <div class="ge_score_test">
                              รวมคะแนนประเมิน = <?php echo $total;?> คะแนน
                          </div>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td><strong>1.ระดับผลการประเมิน &nbsp;&nbsp;</strong></td>
                        <td><strong>ระดับ X &nbsp;&nbsp;&nbsp;&nbsp;( </strong><?php echo $item->minScore; ?> - <?php echo $item->maxScore; ?> <strong>)</strong></td>
                      </tr>
                      <tr>
                        <td><strong>2.ควรได้รับการพัฒนาเพิ่มเติมในเรื่อง  </strong></td>
                        <td>
                          <textarea name="evalNote1" id="evalNote1" cols="45" rows="5"></textarea></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>3. ข้อคิดเห็นเพิ่มเติม </strong></td>
                        <td>
                          <textarea name="evalNote2" id="evalNote2" cols="45" rows="5"></textarea></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td align="right">
                    	<button type="submit" class="btn btn-success">บันทึก</button>
                      	&nbsp;
                      	<button type="button" class="btn btn-warning">ยกเลิก</button>
                     	<!--<input name="eval" id="eval" type="hidden" value="<?php //echo $eval;?>" /> -->
                     </td>
                  </tr>
                </table>
              </form>
            </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 

 	
  
   var score_max     = $('.score_max').find('#get_score_max').text();	
   //alert(score_max);
	function test(sum_now){
		var form = document.getElementById("myForm"),
      	  inputs = form.getElementsByTagName("input"),
		  arr = [];
			test  = 0;  
		  for(var i=0, len=inputs.length; i<len; i++){
			if(inputs[i].type === "hidden"){
			  arr.push(inputs[i].value);
			  xx =  inputs[i].value;
			  test += $('#'+xx).val();
				//alert(test);
			}
		  }
		  
		  console.log(arr);
		
		 //alert(sum_now);
		 //var test = new Array(sum_now)
	  var put_sum_score 		= $('.ge_score').find('#put_sum_score').text();

     // Max Gruop  //
		// var score_max     = $('.score_max').find('#get_score_max').text();
		 //alert(score_max);
		 //result =  (parseFloat(put_sum_score)+parseFloat(sum_now)); 
      /*for (var i = 1; i <= score_max; i++) {
		alert(i);
        var xx     = $('.list_score').find('#list_score_'+score_max+'_'+sum_now).text();//list_score_1_10
        var total     = $('.list_score').find('#list_score_'+score_max+'_'+sum_now).text();//list_score_1_10
		var list_count_test_<?php echo $nums;?>      = $('.list_count').find('#list_count_'+i).text();//list_count_1
        //alert(score_max);alert(sum_now);
		var somthing	 = list_count_test_<?php echo $nums;?> ;	
		alert(xx); 
		$('#put_sum_score_test').html(list_score_test); 
        $('#list_count_test_<?php echo $nums;?>').html(somthing); 
     };*/
		
	}

  function test_test(inputName,inputValue){
      //$.ajax({
        
      $.ajax({
            url:"<?php echo base_url('rate/sum') ?>",
            type:'POST',
            data:{
                  GetinputName :inputName,
                  GetinputValue :inputValue,
                  eval:<?php echo $eval;?>

            },
            error:function(data){

            },
            success:function(data){
                console.log(data); 
            }
      });


  }
 
 
</script>
 