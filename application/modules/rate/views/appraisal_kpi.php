<style type="text/css">
	.form-control {
 	  color: #343232;
	 
	}
  .table-results tr td{
    padding:5px !important;
    vertical-align: middle !important;
  }
  .table-results thead th{
    border-top: none; 
    border-bottom: none;
    border: 0 !important; 
  }

  span.spinner {
    position: absolute;
    height: 40px;
    margin-top: -9px;
    user-select: none;
    -ms-user-select: none;
    -moz-user-select: none;
    -webkit-user-select: none;
    -webkit-touch-callout: none;
  }
    
  span.spinner > .sub,
  span.spinner > .add {
    float: left;
    display: block;
    width: 20px;
    height: 25px;
    text-align: center;
    font-family: Lato;
    font-weight: 900;
    font-size: 1.2em;
    line-height: 25px;
    cursor: pointer;
    transition: 0.1s linear;
    -o-transition: 0.1s linear;
    -ms-transition: 0.1s linear;
    -moz-transition: 0.1s linear;
    -webkit-transition: 0.1s linear;
  }
    
  span.spinner > .add {
    top: 0;
    color: #000;
    font-weight:bold;
    background-color:#CEF6CE;
    border : solid #FFF 1px;
  }

  span.spinner > .add:hover {
    background: #088A08;
    color: #FFF;
  }

  span.spinner > .sub {
    top: 0;
    color: #000;
    font-weight:bold;
    background-color:#F6CECE;
    border : solid #FFF 1px;
  }

  span.spinner > .sub:hover {
    background: #B40404;
    color: #FFF;
  }

  input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }

  th{
    color:#FFF !important;
    text-align:center;
  }

  .excel-style {
    padding : 0px !important;
  }

  .excel-form {
    width : 100% !important;
    line-height : 50px !important;
    height : 50px !important;
    border : none !important; 
    background-color: transparent !important; 
  }

  select.excel-form {
    width : 720px !important;
    line-height : 50px !important;
    height : 50px !important;
    border : none !important; 
    background-color: transparent !important; 
  }

  input[type=number].excel-form {
    margin-top : 15px !important;
    width : 120px !important;
    text-align:right;
    font-size:24px;
    font-weight:bold;
    display:table-cell;
  }

  .summary {
    padding-right : 10px !important;
    text-align:right;
    font-size:24px;
    font-weight:bold;
  }

</style>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
      <header class="panel-heading"> 
          <h3>แบบประเมินผลการปฏิบัติงานประจำปี (<?php echo $get_evalNameText;?>)</h3>
      </header>
      <div class="panel-body">
        <form  id="myForm" method="post" action="<?php echo base_url() ?>rate/kpisubmit" enctype="multipart/form-data">
          <table class="table table-striped" style="background-color: #fff; font-size:14px;">
            <tr>
              <td>
                <table width="100%" border="0">
                  <tr>
                    <td colspan="7" align="center" style=" font-size:16px;">
                      <strong>  </strong> 
						          <?php echo $get_evalYear;?> <strong> ครั้งที่ </strong>
						          <?php echo $get_evalRound;?> ( <strong>วันที่</strong> <?php echo $get_startDate;?> - <?php echo $get_endDate;?>) 
                      <input name="evalType" id="evalType" type="hidden" value="1" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7"><strong>ชื่อ-นามสกุล</strong> <?php  echo $get_staffPreName . $get_staffFName .'&nbsp;&nbsp;'. $get_staffLName;?></td>
                  </tr>
                  <?php
                  
                    $party		    =	$this->rate_model->structural_work($var_agencies,'party',6);						 
                    $staffIDz	    =	$this->rate_model->structural_staff($staffID);
                    $department		=	$this->rate_model->structural_work($staffIDz['orgID'],'department',6);
                    $pile		      =	$this->rate_model->structural_work($department['upperOrgID'],'pile',6);
				          ?>
                </table>
                <br>

                <div class="row">
                  <div class="col-md-3">
                    <div class="pastel-pink" style="padding:20px">
                      <h4>ลาป่วย : <?php echo $sa1["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?php echo $ac1["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-medkit"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-blue" style="padding:20px">
                      <h4>ลากิจ : <?php echo $sa2["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?php echo $ac2["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-briefcase"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-green" style="padding:20px">
                      <h4>ลาพักผ่อน : <?php echo $sa7["numDay"]?> วัน</h4>
                      <h6>จากสิทธิ <?php echo $ac7["numDay"]?> วัน</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-ios-partlysunny-outline"></i>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="pastel-yellow" style="padding:20px">
                      <h4>ลาอื่นๆ : <?php echo $sao["numDay"]?> วัน</h4>
                      <h6>&nbsp;</h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-email"></i>
                    </div>
                  </div>
                </div> 
                
              </td>
            </tr>
            <tr>
              <td>
                <table class="table table-striped">
                  <thead>
                    <tr>
                        <th style="text-align:left"><strong>โปรดให้คะแนนที่ประเมินในแต่ละปัจจัย</strong></th>
                        <th colspan="3"><strong>ระดับคะแนนที่ให้</strong></th>
                    </tr>
                    <tr style="font-weight:bold;">
                        <th style="text-align:left">หัวข้อปัจจัยที่ประเมิน</th>
                        <th>น้ำหนักคะแนน (A)</th>
                        <th>คะแนน (B)</th>
                        <th rowspan="2">คะแนนที่ได้ (AxB)</th>
                    </tr>
                    <tr>
                        <th style="text-align:left"><strong>ผลงานตาม Job Description</strong></th>
                        <th>ผลรวม = 100</th>
                        <th>(0-100)</th>
                    </tr>
                    </thead>
                    <?php
                      for($i=1;$i<=1;$i++)
                      {
                        $weightData = 0;
                        $scoreData = 0;
                        if($i <= sizeof($ddl_edit)) {
                          $weightData = $ddl_edit[$i-1]->evalVariable;
                          $scoreData = $ddl_edit[$i-1]->evalScore;
                        }
                    ?>
                    <tr <?php if ($i > 1) {?> style="hidden" <?php }?>>
                      <td class="excel-style"> 
                        <?php
                          echo form_dropdown('ddl_kpi_'.$i,$ddl_kpi,689,' class="excel-form"');
                        ?>
                      </td>  
                      <td class="excel-style">
                        <input type="number" 
                          value="1"  
                          name="weight_<?php echo $i?>" 
                          id="weight_<?php echo $i?>" 
                          class="excel-form"
                          readonly="true"
                          />
                      </td>
                      <td class="excel-style">
                        <input type="number" 
                          min="0" 
                          max="100" 
                          placeholder="0.00" 
                          step="0.01"
                          value="<?php echo $scoreData?>"  
                          name="score_<?php echo $i?>" 
                          id="score_<?php echo $i?>" 
                          class="excel-form"  
                          onKeyUp="setvallength(this,0,100,<?php echo $i?>);"
                          />
                      </td>
                      <td class="summary"><span id="calScore_<?php echo $i?>">0</span></td>
                    </tr>
                    <?php    
                      }//foreach ddl
                    ?>
                    <tr style="font-weight:bold;">
                      <td style="text-align:right"><h4 class="ge_score_test">น้ำหนักรวม/คะแนนรวม</h4></td>
                      <td class="summary"><span id="totalWeight">0</span></td>
                      <td style="text-align:center">-</td>
                      <td class="summary"><span id="totalScore">0</span></td>
                    </tr>
                    <tr style="font-weight:bold;">
                      <td style="text-align:right"><h4 class="ge_score_test">คะแนนคิดเป็นร้อยละ</h4></td>
                      <td class="summary"><span id="percentScore">0</span></td>
                    </tr>
                </table>
              </td>
            </tr>
          </table>

          <div style="display:none">
              <input type="text" name="eval" id="eval" value="<?php echo $eval;?>" />
              <input type="text" name="staffID" id="staffID" value="<?php echo $staffID;?>" />
              <input type="text" name="get_evalFormID" id="get_evalFormID" value="<?php echo $get_evalFormID;?>" /> 
              <input type="text" name="var_agencies" id="var_agencies" value="<?php echo $var_agencies;?>" />
          </div>
          <button type="submit" class="btn btn-success">บันทึก</button>
          <button type="button" class="btn btn-warning">ยกเลิก</button>
        </form>
      </div>
             
      </section>
    </div>
</div>
<script type="text/javascript"> 

function setvallength(obj,minval,maxval,i) 
{
  if(obj.value>maxval){obj.value=maxval;} else if(obj.value<minval){obj.value=minval;}
  calculateScore(i);
  calculateTotalWeight();
  calculateTotalScore();
  calculatePercent();
}

function calculateScore(i)
{
  var weightX = 0;
  var scoreX = 0;
  var calScore = 0;
  weightX = parseFloat($('#weight_'+i).val());
  scoreX  = parseFloat($('#score_'+i).val());
  calScore = (weightX * scoreX).toFixed(2);
  $('#calScore_'+i).html(calScore);
}

function calculateTotalWeight()
{
  var weightX = 0;
  weightX += parseFloat($('#weight_1').val());
  $('#totalWeight').html(weightX);
}

function calculateTotalScore()
{
  var scoreX = 0;
  scoreX += parseFloat($('#calScore_1').html());
  $('#totalScore').html(scoreX);  
}

function calculatePercent()
{
  var weightX = 0;
  var scoreX = 0;
  var percentScore = 0;
  weightX = parseFloat($('#totalWeight').html());
  scoreX = parseFloat($('#totalScore').html());
  if(weightX != 0) {
    percentScore = (scoreX/weightX).toFixed(2);
  }
  $('#percentScore').html(percentScore);  
}

var nextLevelScore = -1;
var prevLevelScore = 0;
var lookingForLevel = false;

function updateLevel(totalScore){
  if((totalScore > nextLevelScore || totalScore < prevLevelScore) && !lookingForLevel){
    $("#evalResults").html('loading ...');
    lookingForLevel = true;
    $.getJSON( "<?php echo base_url(); ?>rate/getScoreAjax", {total:totalScore}, function( data ) {
      prevLevelScore = parseFloat(data.minScore);
      nextLevelScore = parseFloat(data.maxScore);
      $("#evalResults").html('<strong>ระดับ ' + data.levelText + ' &nbsp;&nbsp;&nbsp;&nbsp;( </strong>' + data.minScore + ' - ' + data.maxScore + ' <strong>)</strong>');
    })
    .fail(function(jqXHR, textStatus, errorThrown) { $("#evalResults").html("Error getting results"); })
    .always(function() { lookingForLevel = false; });
  }
}

//calculateTotalScore();

//The only script I found to scroll with offset on html5 error
var delay = 0;
var offset = 80;

document.addEventListener('invalid', function(e){
    $(e.target).addClass("invalid");
    $('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset }, delay);
}, true);
document.addEventListener('change', function(e){
    $(e.target).removeClass("invalid")
}, true);

//ตรวจสอบก่อนส่งค่าไปบันทึก
$('form').submit(function () {
  var weightX = 0;
  weightX = parseFloat($('#totalWeight').html());
  /*
  if (weightX  != 100) {
      alert('กรุณาทำให้ผลรวมน้ำหนักคะแนนเป็น 100');
      return false;
  } 
  */
});

$(function() {
  for(i=1;i<=1;i++) {
    calculateScore(i);
  }
  calculateTotalWeight();
  calculateTotalScore();
  calculatePercent();
});
</script>