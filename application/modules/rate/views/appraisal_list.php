<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<?php
        
         $exp_eval = explode(',', $eval);
         $evalRound = $exp_eval[0];
         $evalYear = $exp_eval[1];
?>
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading"> 
        <h3>ข้อมูลการประเมินผล (รอบที่ <?=$evalRound;?>)</h3>
      </header>
      <div class="panel-body">
        <form action="<?=base_url('rate') ?>" method="get" enctype="multipart/form-data">
          <!--div class="form-group">
            <label class="control-label col-sm-3">เลือกปีงบประมาณ/รอบ</label>
              <div class="col-sm-9">
              <?php 
                //echo form_dropdown('eval_date',$eval_date,$var_eval_date,'class="form-control" ');
              ?>
              </div>
          </div-->
          <!--div class="form-group" ><label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-9"> &nbsp; </div>
          </div>
       		<div class="form-group" ><label class="control-label col-sm-3">&nbsp;</label>
            <div class="col-sm-9"><button type="submit" class="btn btn-success">ค้นหา</button></div>
          </div-->
          <div class="form-group" >
            <div class="row" style="padding:10px">
              <div class="col-md-3"><b>หน่วยงาน</b></div>
              <div class="col-md-9">
                    <select class="xcrud-input form-control form-control" data-type="select" name="agencies" id="agencies" maxlength="11" onchange="javascript:location.href = 'rate?eval=<?=$this->input->get("eval");?>&agencies='+this.value;"> 
                      <option value='0'>กรุณาเลือก</option>
                        <?php 
                        echo $dropdown_org_chart;
                        ?>
                    </select>
              </div>
            </div>
            <div class="row" style="padding:10px">
              <div class="col-md-3"><b>วันที่ครบกำหนดการประเมิน</b></div>
              <div class="col-md-9"><?php echo $dateDue;?></div>
            </div>
          </div> 
        </form>
      </div>
    </section>
  </div>
</div> 
 
<form action="<?=base_url('rate/submit') ?>" method="post" enctype="multipart/form-data">
  <div class="container">
    <div class="col-md-6"><h4><?php echo $orgName;?></h4></div>
		<?php if($this->session->userdata('roleID')==3) {?>
    <div class="col-md-6" style="text-align:right"><a class="btn btn-sm btn-success" href="<?=base_url('reportAppraisal')?>?agencies=<?=$var_agencies?>" target="_blank">รายงานผลการประเมิน</a></div>
    <?php }?>
  </div>
  <br><br>
  <?php
  if($var_agencies != "0")
  {
  ?>
  <table class="xcrud-list table table-striped table-hover table-bordered">
    <thead>
      <tr class="xcrud-th">
        <th class="xcrud-num">#</th>
        <th class="xcrud-column xcrud-action" style="width:10%">รหัสพนักงาน</th>
        <th class="xcrud-column xcrud-action" style="width:60%">หน่วยงาน / ชื่อ-นามสกุล</th>
        <th class="xcrud-column xcrud-action" style="width:15%;text-align:center">ประเมิน KPI </th>
        <th class="xcrud-column xcrud-action" style="width:15%;text-align:center">ประเมิน Competency </th>
                    
    </thead>
    <tbody>
      <?php 
				 $num = 1 ;	 


         $love_form = $this->rate_model->getEvalForm($evalYear,$evalRound,1);
         $kpi_form = $this->rate_model->getEvalForm($evalYear,$evalRound,2);
         $competency_form = $this->rate_model->getEvalForm($evalYear,$evalRound,3);

        $var_eval_date = $eval;


				 $eval_get	= $this->rate_tscore->rate_emp_party($var_agencies,$assignID); 
				 if(count($eval_get)>0){
            foreach ($eval_get as $key => $item) {
            ?>
            <tr class="xcrud-row xcrud-row-0"> 
              <td class="xcrud-current xcrud-num"><i class="fa fa-users"></i> </td>
              <td><?php echo $item->staffID; ?></td>
              <td><?php echo $item->staffPreName.$item->staffPreNameEtc." ".$item->staffFName." ".$item->staffLName;?></td>

              <td style="text-align:center">
                <?php
                  $statusKPI = $this->rate_model->get_eval_staff($item->staffID,$kpi_form->evalFormID); //4 - 7
                  if($statusKPI==-2){ //ค่าเดิม -1
                ?>
                  <a class="btn btn-sm btn-warning" 
                      href="<?=base_url('rate/kpi');?>?eval=<?=$var_eval_date;?>&staffID=<?=$item->staffID ?>&var_agencies=<?=$var_agencies;?>" title="" target="_blank" style="width:130px">กำลังเตรียมข้อมูล</a>
                <?php }elseif($statusKPI==-1){ //ค่าเดิม 0?> 
                  <a class="btn btn-sm btn-info" 
                      href="<?=base_url('rate/kpi');?>?eval=<?=$var_eval_date;?>&staffID=<?=$item->staffID ?>&var_agencies=<?=$var_agencies;?>" title="" target="_blank" style="width:130px">ประเมิน</a>            
                <?php }else{?>
                  <a class="btn btn-sm btn-success" 
                      href="<?=base_url('rate/kpi');?>?eval=<?=$var_eval_date;?>&staffID=<?=$item->staffID ?>&var_agencies=<?=$var_agencies;?>" title="" target="_blank" style="width:130px"><i class="fa fa-pencil-square-o"></i>ประเมินแล้ว</a>
                <?php  
                  }
                  //echo $this->db->last_query()."<br>";//if($this->rate_model->get_eval_staff($item->staffID,4,1)){    
                ?>             
              </td>
              <td style="text-align:center">
                <?php
                  $statusCompetency = $this->rate_model->get_eval_staff($item->staffID,$competency_form->evalFormID); //2 - 6
                  if($statusCompetency==-1){
                ?>              
                  <a class="btn btn-sm btn-warning" 
                      href="<?=base_url('rate/competency');?>?eval=<?=$var_eval_date;?>&staffID=<?=$item->staffID ?>&var_agencies=<?=$var_agencies;?>" title="" target="_blank" style="width:130px">กำลังเตรียมข้อมูล</a>                
                <?php }elseif($statusCompetency==0){?>
                  <a class="btn btn-sm btn-info" 
                      href="<?=base_url('rate/competency');?>?eval=<?=$var_eval_date;?>&staffID=<?=$item->staffID ?>&var_agencies=<?=$var_agencies;?>" title="" target="_blank" style="width:130px">ประเมิน</a>            
                <?php }else{?>
                  <a class="btn btn-sm btn-success" 
                      href="<?=base_url('rate/competency');?>?eval=<?=$var_eval_date;?>&staffID=<?=$item->staffID ?>&var_agencies=<?=$var_agencies;?>" title="" target="_blank" style="width:130px"><i class="fa fa-pencil-square-o"></i>ประเมินแล้ว</a>
                <?php  
                  }
                  //echo $this->db->last_query()."<br>";
                  //if($this->rate_model->get_eval_staff($item->staffID,2,1)){    
                ?>             
              </td>
            </tr> 
            <?php }//foreach ($eval_get as $key => $item)
          }else{//if(count($eval_get)>0)
          ?>	   
            <tr class="xcrud-row xcrud-row-0"> 
              <td colspan="6" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
            </tr>
          <?php 
          } //if(count($eval_get)>0)?>          
          </tbody>
        </table>
      <?php
      }
      ?>
 <!--       
 <div class="form-group" style="text-align: right;">
 <strong style="color:red">หากยืนยันการส่งข้อมูลแล้วจะไม่สามารถแก้ไขการประเมินได้</strong><br/>
 <?php 
 
		if($var_eval_date !=''){
				$var_eval_date	=	explode(',',$var_eval_date);	
				$get_eval_date		=	$var_eval_date[1];
				$get_eval_round		=	$var_eval_date[0];
		}
		$year 				= ($year !=''?$year:$get_eval_date);
		$round 				= ($round !=''?$round:$get_eval_round);
		$recent_submit		= $this->recent_model->recent_submit($year,$round,$var_agencies);	
		
		if($year !='' and $round !='' and $var_agencies !='') {
 			if($recent_submit == false){
			
 ?>
                  <input name="year" id="year" type="hidden" value="<?php echo  $year ;?>" />
                  <input name="round" id="round" type="hidden" value="<?php echo $round ;?>" />
                  <input name="var_agencies" id="var_agencies" type="hidden" value="<?php echo $var_agencies;?>" />
                  <button type="submit" class="btn btn-warning"  ><i class="fa fa-plus-circle"></i> ส่งข้อมูลประเมิน</button>
  <?php 	}//else{
	  
  		}
	    //}
	//}
  ?>
 </div-->
</form>
 

<script type="text/javascript"> 
	function changeRate(obj){
    	alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/xx') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   // var obj = jQuery.parseJSON(response); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(response);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
					location.replace('<?php echo base_url().'rate/' ?>');
				}
		});	
  	}
</script>
 