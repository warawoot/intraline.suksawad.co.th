    
 <style type="text/css">
	.form-control {
 	  color: #343232;
	 
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล</h3>
            </header>
            <div class="panel-body">
              <form  id="myForm" method="post" action="<?php echo base_url(); ?>rate/kpis" enctype="multipart/form-data">
                <table class="table" style="background-color: #fff; font-size:14px;">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td colspan="7" align="center" style=" font-size:16px;"><strong> แบบประเมินผลการปฏิบัติงานประจำปี </strong> 2558 <strong> ครั้งที่ </strong>1 ( <strong>วันที่</strong> 1 กรกฎาคม 2559 - 31 ธันวาคม 2559) </td>
                      </tr>
                      <tr>
                        <td colspan="7" align="center"><strong> [ </strong>ปผ1 (ระดับ 1-5)<strong> ] </strong></td>
                      </tr>
                      <tr>
                        <td><strong>ชื่อ-นามสกุล</strong></td>
                        <td>นายธีระ&nbsp;&nbsp;เพลินสินธุ์</td>
                        <td>&nbsp;</td>
                        <td><strong>ตำแหน่ง</strong></td>
                        <td>นักบริหาร</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>แผนก</strong></td>
                        <td>&nbsp; </td>
                        <td>&nbsp;</td>
                        <td><strong>กอง</strong></td>
                        <td>&nbsp; </td>
                        <td><strong>ฝ่าย</strong></td>
                        <td>&nbsp; </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td><strong>โปรดให้คะแนนที่ประเมินในแต่ละปัจจัย</strong></td>
                        <td>&nbsp;</td>
                        <td><strong>ระดับคะแนนที่ให้</strong></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>หัวข้อปัจจัยที่ประเมิน</strong></td>
                        <td><strong>น้ำหนักคะแนน (A)</strong></td>
                        <td colspan="2"><strong>คะแนน (B)</strong></td>
                        <td><strong>คะแนนเต็ม (Ax5)</strong></td>
                        <td><strong>คะแนนที่ได้ (AxB)</strong></td>
                      </tr>
                                            <tr style="background-color:#99CCFF">
                        <td><strong> ผลลัพธ์ของงาน
						(KPI)
						</strong></td>
                        <td>&nbsp;</td>
                        <td colspan="4"><strong>น้ำหนักของปัจจัยที่ 1 = [ </strong>100<strong> ] คะแนน</strong>&nbsp;</td>
                      </tr>
                        
					<input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_10" />
                      <tr style="background-color:#EEE;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>
						<input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="int" value="">
						</td><!--onChange="changeTest_(this)"-->
                        <td align="center">
						<input class="xcrud-input form-control" style="width:80px" data-required="1" data-unique="" type="text" data-type="int" value="">		</td>
                        <td><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="5" value="2"  name="score_max_min_10" id="score_max_min_10"   onChange="changeTest_10(this)"/></td>
                                </tr>
                            </table>
 
               				  <div class="list_score">
                                <span id="list_score_1_10"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="10" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						   <td align="center">100</td>
							<td>&nbsp;</td>
                      </tr>

                      <tr style="background-color:#F9F9F9;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>
						<input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="int" value="">
						</td><!--onChange="changeTest_(this)"-->
                        <td align="center">
						<input class="xcrud-input form-control" style="width:80px" data-required="1" data-unique="" type="text" data-type="int" value="">		</td>
                        <td><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="5" value="2"  name="score_max_min_10" id="score_max_min_10"   onChange="changeTest_10(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_10"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="10" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						   <td align="center">100</td>
							<td>&nbsp;</td>
                      </tr>

					   <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_11" />
                                                     <div class="list_count">
                                <span id="list_count_1"></span>
                              </div>
                              <span id="list_count_test_1"></span>
                              
           			                       <tr style="background-color:#99CCFF">
                        <td><strong>ผลของสมรรถนะ (Competencies)</strong></td>
                        <td>&nbsp;</td>
                        <td colspan="4"><strong>น้ำหนักของปัจจัยที่ 2 = [ </strong>400<strong> ] คะแนน</strong>&nbsp;</td>
                      </tr>
                      <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_15" />
                      <tr style="background-color:#EEE;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2. ความรู้ความเข้าใจในหน้าที่งานที่รับผิดชอบ</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="5" value="2"  name="score_max_min_15" id="score_max_min_15"   onChange="changeTest_15(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						   <td align="center">50</td>
							<td>&nbsp;</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_16" />
                      <tr style="background-color:#F9F9F9;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;3. การแก้ไขปัญหาและการตัดสินใจ</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="10" value="5"  name="score_max_min_16" id="score_max_min_16"   onChange="changeTest_16(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						    <td align="center">50</td>
 							<td>&nbsp;</td>
                     </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_17" />
                      <tr style="background-color:#EEE;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;4. การวางแผนงาน</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="10" value="6"  name="score_max_min_17" id="score_max_min_17"   onChange="changeTest_17(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						    <td align="center">50</td>
							<td>&nbsp;</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_18" />
                      <tr style="background-color:#F9F9F9;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;5. การวินิจจัยสั่งการ</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="10" value="4"  name="score_max_min_18" id="score_max_min_18"   onChange="changeTest_18(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						    <td align="center">50</td>
							<td>&nbsp;</td>
                      </tr>
                                                     <div class="list_count">
                                <span id="list_count_2"></span>
                              </div>
                              <span id="list_count_test_2"></span>
                              
                      <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_19" />
                      <tr style="background-color:#EEE;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;6. การพัฒนาตนเอง</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="10" value="2"  name="score_max_min_19" id="score_max_min_19"   onChange="changeTest_19(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						    <td align="center">50</td>
 							<td>&nbsp;</td>
                     </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_20" />
                      <tr style="background-color:#F9F9F9;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;7. ความคิดริเริ่มสร้างสรรค์</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="10" value="4"  name="score_max_min_20" id="score_max_min_20"   onChange="changeTest_20(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						    <td align="center">50</td>
							<td>&nbsp;</td>
                      </tr>
                                                     <div class="list_count">
                                <span id="list_count_3"></span>
                              </div>
                              <span id="list_count_test_3"></span>
                              

</div>                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_21" />
                      <tr style="background-color:#EEE;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;8. มนุษย์สัมพันธ์และการประสานงาน</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="10" value="10"  name="score_max_min_21" id="score_max_min_21"   onChange="changeTest_21(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						    <td align="center">50</td>
							<td>&nbsp;</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_22" />
                      <tr style="background-color:#F9F9F9;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;9.  ความร่วมมือต่อทั้งผู้บังคับบัญชาและองค์กร</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="5" value="5"  name="score_max_min_22" id="score_max_min_22"   onChange="changeTest_22(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						    <td align="center">50</td>
							<td>&nbsp;</td>
                      </tr>
                                                     <div class="list_count">
                                <span id="list_count_3"></span>
                              </div>
                              <span id="list_count_test_3"></span>
                              

</div>                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_21" />
                      <tr style="background-color:#EEE;font-size:12px; text-align:left; padding:5px; margin-bottom:25px" >
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;10. การมาทำงานสม่ำเสมอ</td><!--onChange="changeTest_(this)"-->
                        <td align="center">10</td>
                        <td ><table width="50%" border="0" style="margin-top:-28px">
                              <tr>
                                <td><input type="number" min="1" max="10" value="10"  name="score_max_min_21" id="score_max_min_21"   onChange="changeTest_21(this)"/></td>
                                </tr>
                            </table>

                             <!--<select name="score_max_min_" id="score_max_min_" class="form-control" 
                                onChange="changeTest_(this)">
                                  <option value="">กรุณาเลือก</option>
                                                                      <option value=""  ></option>
                                                                </select>-->
               				  <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                              <div style="display:none">
                               	<input type="text" name="evalSubjectID[]" id="evalSubjectID[]" value="11" />
                              </div>
                           </td>
                           <td> (1 - 5) </td>
						    <td align="center">50</td>
							<td>&nbsp;</td>
                      </tr>

                                                     <div class="list_count">
                                <span id="list_count_4"></span>
                              </div>
                              <span id="list_count_test_4"></span>
                              
           			   
                        <div class="score_max" style="display:none;">
                        	<span id="get_score_max">4</span>
                            
                        </div>
                        
                                              
                      <tr>
                        <td colspan="3" align="right">
                        	<!--<div class="ge_score">
                            	รวมคะแนนประเมิน = <span id="put_sum_score"></span> คะแนน
                        	</div>-->
                         </td>
                      </tr>
                    </table></td>
                  </tr>
                   
                  <tr>
                    <td align="right">
                    	<div style="display:none">
                            <input type="text" name="eval" id="eval" value="1,2559" />
                            <input type="text" name="staffID" id="staffID" value="189" />
                            <input type="text" name="get_evalFormID" id="get_evalFormID" value="3" /> 
                            <input type="text" name="var_agencies" id="var_agencies" value="82" />
                       	</div>
                    	<button type="submit" class="btn btn-success">ขั้นตอนถัดไป &gt;&gt;</button>
                      	&nbsp;
                      	<button type="button" class="btn btn-warning">ยกเลิก</button>
                     	<!--<input name="eval" id="eval" type="hidden" value="" /> -->
                    </td>
                  </tr>
                </table>
              </form>
            </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 

 	
  
   var score_max     = $('.score_max').find('#get_score_max').text();	
   //alert(score_max);
	function test(sum_now){
		var form = document.getElementById("myForm"),
      	  inputs = form.getElementsByTagName("input"),
		  arr = [];
			test  = 0;  
		  for(var i=0, len=inputs.length; i<len; i++){
			if(inputs[i].type === "hidden"){
			  arr.push(inputs[i].value);
			  xx =  inputs[i].value;
			  test += $('#'+xx).val();
				//alert(test);
			}
		  }
		  
		  console.log(arr);
		

		 //alert(sum_now);
		 //var test = new Array(sum_now)
	  var put_sum_score 		= $('.ge_score').find('#put_sum_score').text();

 
		
	}

  function test_test(inputName,inputValue){
      //$.ajax({
        
      $.ajax({
            url:"http://dpo-ehr.smmms.com/rate/sum",
            type:'POST',
            data:{
                  GetinputName :inputName,
                  GetinputValue :inputValue,
                  eval:''
            },
            error:function(data){

            },
            success:function(data){
                console.log(data); 
            }
      });


  }
 
 
</script>

<style>
		input[type=number] {
		  float: left;
		  width: 70px;
		  height: 35px;
		  padding: 0;
		  font-size: 1.2em;
		  text-transform: uppercase;
		  text-align: center;
		  color: #93504C;
		  border: 2px #93504C solid;
		  background: none;
		  outline: none;
		  pointer-events: none;
		}
		
		span.spinner {
		  position: absolute;
		  height: 40px;
		  user-select: none;
		  -ms-user-select: none;
		  -moz-user-select: none;
		  -webkit-user-select: none;
		  -webkit-touch-callout: none;
		}
		
		span.spinner > .sub,
		span.spinner > .add {
		  float: left;
		  display: block;
		  width: 35px;
		  height: 35px;
		  text-align: center;
		  font-family: Lato;
		  font-weight: 700;
		  font-size: 1.2em;
		  line-height: 33px;
		  color: #93504C;
		  border: 2px #93504C solid;
		  border-right: 0;
		  border-radius: 2px 0 0 2px;
		  cursor: pointer;
		  transition: 0.1s linear;
		  -o-transition: 0.1s linear;
		  -ms-transition: 0.1s linear;
		  -moz-transition: 0.1s linear;
		  -webkit-transition: 0.1s linear;
		}
		
		span.spinner > .add {
		  top: 0;
		  border: 2px #93504C solid;
		  border-left: 0;
		  border-radius: 0 2px 2px 0;
		}
		
		span.spinner > .sub:hover,
		span.spinner > .add:hover {
		  background: #93504C;
		  color: #25323B;
		}
		 input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
		 -webkit-appearance: none;
		}

</style>

<script>
(function($) {
		$.fn.spinner = function() {
		this.each(function() {
		var el = $(this);
		
		// add elements
		el.wrap('<span class="spinner"></span>');     
		el.before('<span class="sub">-</span>');
		el.after('<span class="add">+</span>');
		
		// substract
		el.parent().on('click', '.sub', function () {
		if (el.val() > parseInt(el.attr('min')))
		el.val( function(i, oldval) { return --oldval; });
		});
		
		// increment
		el.parent().on('click', '.add', function () {
		if (el.val() < parseInt(el.attr('max')))
		el.val( function(i, oldval) { return ++oldval; });
		});
			});
		};
		})(jQuery);
		
	 $('input[type=number]').spinner();
</script>
               