<?php

class Rate_model extends MY_Model {
      
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	} 
 
	private function findUpperOrg($orgID) {
		$sql = "SELECT MAX(upperOrgID) as upperOrgID FROM tbl_org_chart
 				WHERE orgID=$orgID 
				AND assignID=2";	
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$result = $query->result();
			return $result[0]->upperOrgID; 
		} else {
			return 0;
		}			
	}

 	function get_dropdown_competency($orgID,$competencyType){
		$sql = "SELECT * FROM tbl_eval_subject_competency 
 				WHERE ecType=$competencyType
				 	AND ecOrgID=$orgID  
				ORDER BY ecText";

		$query = $this->db->query($sql);
 		$dropdowns	= $query->result();
		
		$dropdownlist['']	= "กรุณาเลือก";
		if ($query->num_rows() > 0){
			foreach($dropdowns as $dropdown){
				$dropdownlist[$dropdown->ecID]	= $dropdown->ecText;
			}	
		} 
		else {
			$upperOrgID = $this->findUpperOrg($orgID);
			if($upperOrgID != 0) {
				$dropdownlist = $this->get_dropdown_competency($upperOrgID,$competencyType);
			}
		}
		
		$finaldropdown	= $dropdownlist;
		return $finaldropdown;
	} 

 	function get_dropdown_kpi($orgID){
		/*
		$sql = "SELECT * FROM tbl_eval_subject_kpi 
 				WHERE eKpiOrgID=$orgID 
				ORDER BY eKpiText";	
		*/
		//Temporary
		$sql = "SELECT * FROM tbl_eval_subject_kpi 
 				WHERE eKpiOrgID=0 
				ORDER BY eKpiText";	
		$query = $this->db->query($sql);
 		$dropdowns	= $query->result();
		
		$dropdownlist['']	= "กรุณาเลือก";
		if ($query->num_rows() > 0){
			foreach($dropdowns as $dropdown){
				$dropdownlist[$dropdown->eKpiID]	= $dropdown->eKpiText;
			}	
		} 
		else {
			$upperOrgID = $this->findUpperOrg($orgID);
			if($upperOrgID != 0) {
				$dropdownlist = $this->get_dropdown_kpi($upperOrgID);
			}
		}
		
		$finaldropdown	= $dropdownlist;
		return $finaldropdown;
	} 

	function get_dropdown_all($id , $name ,$table){
		 
		$this->db->select(' * ');
		$this->db->order_by($name, "asc");
		$this->db->from($table);
		 
		$query 		= $this->db->get();
		$dropdowns	= $query->result();
		
		if ($query->num_rows() > 0){
			foreach($dropdowns as $dropdown){
				//echo $fruit = array_shift($dropdown);
				$dropdownlist['']	= "กรุณาระบุด้วย";
				$dropdownlist[$dropdown->$id.','.$dropdown->$name]	= $dropdown->$name;
			}	
		}else{
				$dropdownlist['']	= " กรุณาระบุด้วย";
		} 
		
		$finaldropdown	= $dropdownlist;
		return $finaldropdown;
	} 
	
	function get_dropdown_all_new($id , $name ,$table,$var_eval_date){
		//$eval_get			=	explode(",", $var_eval_date);
		//$array = array('evalYear' => $eval_get[0], 'evalRound' => $eval_get[1]);
		$this->db->select(' * ');
		$this->db->order_by($name, "asc");
		$this->db->from($table);
		//$this->db->where($array);	  
		$query 		= $this->db->get();
		$dropdowns	= $query->result();
		
		if ($query->num_rows() > 0){
			foreach($dropdowns as $dropdown){
				//echo $fruit = array_shift($dropdown);
				$dropdownlist['']	= "กรุณาระบุด้วย";
				$dropdownlist[$dropdown->$id.','.$dropdown->$name]	= $dropdown->$name;
			}	
		}else{
				$dropdownlist['']	= " กรุณาระบุด้วย";
		} 
		
		$finaldropdown	= $dropdownlist;
		return $finaldropdown;
	} 
	
	function get_dropdown_org($id , $name ,$table,$where){
		   		 
		/*$this->db->select(' * ');
		$this->db->order_by($name, "asc");
		$this->db->from($table);
		$this->db->where('assignID', $where);
		$this->db->like('orgName', 'ฝ่าย', 'after');	  
		$query 		= $this->db->get();*/
		$query = $this->db->query("
									SELECT * FROM ".$table."  as t1 
 									WHERE t1.assignID = ".$where." 
									  and t1.upperOrgID = 1 
									-- and ( t1.orgName LIKE '%ฝ่าย%'  or t1.orgName LIKE '%สำนักงาน%' ) 
									ORDER BY ".$name." ASC
							    ");
 		$dropdowns	= $query->result();
		
		if ($query->num_rows() > 0){
			foreach($dropdowns as $dropdown){
				//echo $fruit = array_shift($dropdown);
				$dropdownlist['']	= "ทั้งหมด";
				$dropdownlist[$dropdown->$id]	= $dropdown->$name;
			}	
		}else{
				$dropdownlist['']	= " ทั้งหมด";
		} 
		
		$finaldropdown	= $dropdownlist;
		return $finaldropdown;
	} 
	
	function get_dropdown_org_option($id , $name ,$table,$where){
 
		$query = $this->db->query("
									SELECT * FROM ".$table."  as t1 
 									WHERE t1.assignID = ".$where." 
									 and t1.upperOrgID = 1 
									--  and ( t1.orgName LIKE '%ฝ่าย%'  or t1.orgName LIKE '%สำนักงาน%' ) 
									ORDER BY ".$name." ASC
							    ");
 		$dropdowns	= $query->result();
		
		 
		return $dropdowns;
	} 
		
 	function get_dropdown_all1($id , $name ,$table ){
		//$this->db->order_by($id, "asc");
		$sql		= $this->db->get($table);
		$dropdowns	= $sql->result();
		
		if ($sql->num_rows() > 0){
			foreach($dropdowns as $dropdown){
				$dropdownlist['']	= "เลือกปีงบประมาณ";
				$dropdownlist[$dropdown->$id]	= $dropdown->$name;
			}
		}else{
				$dropdownlist['']	= "เลือกปีงบประมาณ";
		} 
		
		$finaldropdown	= $dropdownlist;
		return $finaldropdown;
	}   
	
	function rate_emp($OrgID,$date){ 
	  
	 ## where in ##
		#$names = array('Frank', 'Todd', 'James');
		#$this->db->where_in('username', $names);
		#// Produces: WHERE username IN ('Frank', 'Todd', 'James')

        $this->db->select('t1.staffID ,t2.ID  , t2.staffPreName ,`t2`.`staffFName`, `t2`.`staffLName`, `t2`.`staffImage` ,t3.positionName');
        $this->db->from('tbl_staff_work as t1');
       // $this->db->where('t1.workStartDate <=' ,$date);
	   if($OrgID !=''){
        	$this->db->where('t1.OrgID in ('.$OrgID.')' );
	   }else{
		    $this->db->where('t1.OrgID in (00)' );
		 }
		//$this->db->where_in('t1.OrgIDs', $OrgID);
        //$this->db->where('t1.isHeader' ,true);
        //$this->db->where('t2.staffID !=','');
		//$this->db->join('tbl_staff as t2 ' , 't2.ID = t1.staffID','inner');
        $this->db->join('tbl_staff as t2 ' , 't1.staffID = t2.ID ','inner');
		$this->db->join('tbl_position as t3' ,'t1.positionID = t3.positionID' ,'inner');
		
        $query 		= $this->db->get();
 		$data 		= $query->result(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
	}
	
	function rate_eval_doc($evalRound,$evalType){  
        $eval_get			=	explode(",", $evalRound);
        $this->db->select('*');
        $this->db->from('`tbl_eval_date` as t1');
        $this->db->where('t1.evalRound' ,$eval_get[0]);
		$this->db->where('t1.evalYear' ,$eval_get[1]);
		$this->db->where('t2.evalType' ,$evalType);
        $this->db->join('tbl_eval_form as t2 ' , 't1.evalRound = t2.evalRound and t1.evalYear = t2.evalYear','inner');
        $query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		//echo $this->db->last_query();
 		return ($query->num_rows() > 0) ? $data : NULL;
            
    }
	
	function rate_eval_dates($eval_date){  
        $eval_get	=	explode(",", $eval_date);
        $this->db->select('*');
        $this->db->from('`tbl_eval_date` as t1');
        $this->db->where('t1.evalRound' ,$eval_get[0]);
		$this->db->where('t1.evalYear' ,$eval_get[1]); 
        $query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
            
    }
	
	function rate_emp_doc($staffID){  
        $querystr = "SELECT * FROM tbl_staff WHERE staffID='".$staffID."'";
        /*	
        $this->db->select('t2.staffID, t2.ID, t2.orgID   , t2.staffPreName ,`t2`.`staffFName`, `t2`.`staffLName`, `t2`.`staffImage` ,t3.positionName');
        $this->db->from('tbl_staff_work as t1');
        $this->db->where('t2.ID' ,$staffID);
        $this->db->join('tbl_staff as t2 ' , 't2.ID = t1.staffID','inner');
        $this->db->join('tbl_position as t3' ,'t3.positionID = t1.positionID' ,'left');
        */
        //SELECT `t2`.`staffID`, `t2`.`ID`, `t2`.`orgID`, `t2`.`staffPreName`, `t2`.`staffFName`, `t2`.`staffLName`, `t2`.`staffImage`, `t3`.`positionName` FROM (`tbl_staff_work` as t1) INNER JOIN `tbl_staff` as t2 ON `t2`.`ID` = `t1`.`staffID` LEFT JOIN `tbl_position` as t3 ON `t3`.`positionID` = `t1`.`positionID` WHERE `t2`.`ID` = '10343'
        //$query 		= $this->db->get();
        $query      = $this->db->query($querystr);
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
            
    }
	
	function get_eval_group_subject($evalFormID,$evalType){
		
		$this->db->select('*');
        $this->db->from('`tbl_eval_group_subject` as t1 ');
        $this->db->join('tbl_eval_form as t2' , 't1.evalFormID = t2.evalFormID','inner');
		//$this->db->join('tbl_eval_subject as t3' , 't1.evalGroupSubjectID = t3.evalGroupSubjectID','inner');
		$this->db->where('t2.evalFormID' ,$evalFormID);
		$this->db->where('t2.evalType' ,$evalType);
        $query 		= $this->db->get();
 		$data 		= $query->result(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
 		
	}
	
	function get_eval_subject($evalGroupSubjectID){
		
		$this->db->select('*');
        $this->db->from('`tbl_eval_subject` as t1');
 		$this->db->where('t1.evalGroupSubjectID' ,$evalGroupSubjectID);
        $query 		= $this->db->get();
 		$data 		= $query->result(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
 	
	}
	
	function get_sum_eval_subject($evalGroupSubjectID){
		
		$this->db->select_sum('t1.maxScore');
        $this->db->from('`tbl_eval_subject` as t1');
 		$this->db->where('t1.evalGroupSubjectID' ,$evalGroupSubjectID);
        $query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
 	
	}
	
	function delete_eval($empID, $evalFormID){
		$this->db->where('empID', $empID);
		$this->db->where('evalFormID', $evalFormID);
  		$this->db->delete('tbl_eval');		
 	}

	function delete_eval_value($empID, $evalFormID, $evalSubjectID){
		$this->db->where('empID', $empID);
		$this->db->where('evalFormID', $evalFormID);
		$this->db->where('evalSubjectID', $evalSubjectID);
  		$this->db->delete('tbl_eval');		
 	}

	function insert_eval($data){
		$this->delete_eval_value($data['empID'],$data['evalFormID'],$data['evalSubjectID']);
		$this->db->insert('tbl_eval', $data);
 	}

	function delete_summary_eval($evalYear, $evalRound, $evalStep, $staffID, $rankID){
		$this->db->where('evalYear', $evalYear);
		$this->db->where('evalRound', $evalRound);
		$this->db->where('evalStep', $evalStep);
		$this->db->where('staffID', $staffID);
		$this->db->where('rankID', $rankID);
  		$this->db->delete('tbl_eval_summary');		
 	}

	function insert_summary_eval($data){
		$this->delete_summary_eval($data['evalYear'],$data['evalRound'],$data['evalStep'],$data['staffID'],$data['rankID']);
		$this->db->insert('tbl_eval_summary', $data);
 	}

    function update_eval($empID,$evalFormID,$evalSubjectID,$data){
        //$this->db->insert("tbl_eval", $data);
		$sql	=	$this->db->where('empID', $empID);
		$sql	=	$this->db->where('evalFormID', $evalFormID);
		$sql	=	$this->db->where('evalSubjectID', $evalSubjectID);
		$sql	=	$this->db->update('tbl_eval', $data); 	
		if($this->db->affected_rows() > 0) {
			return 'data succesfully inserted';
		} else {
			return 'data failed inserted';
		}
    }
	
	function insert_eval_note($data){
  		$this->db->insert("tbl_eval_notes", $data);
		if($this->db->affected_rows() > 0){
			return 'succesfully';
		}else{
			return 'failed';
		}
	}	
	
	
	 function update_eval_note($empID,$evalFormID,$data){
        //$this->db->insert("tbl_eval", $data);
		$sql	=	$this->db->where('empID', $empID);
		$sql	=	$this->db->where('evalFormID', $evalFormID);
 		$sql	=	$this->db->update('tbl_eval_notes', $data); 	
		if($this->db->affected_rows() > 0){
				  return 'data succesfully inserted';
		}else{
				 return 'data failed inserted';
		}
    }
	
	
	function tbl_eval_form_level($maxScore){
		/*$this->db->select('*');
        $this->db->from('tbl_eval_form_level as t1');
 		$this->db->where('t1.maxScore >=' ,$maxScore);
  		$this->db->limit(1);
        $query 		= $this->db->get();
 		$data 		= $query->row_array(); 
  		return ($query->num_rows() > 0) ? $data : NULL;*/
		
		$query = $this->db->query('SELECT  *
									FROM tbl_eval_form_level as t1
									Where '. $maxScore.' BETWEEN minScore and maxScore
									 '
							);
        return $query->row_array(); 
		
		
	}

    function get_eval($empID,$evalFormID){
        $this->db->select('*');
        $this->db->from('tbl_eval as t1');
        $this->db->where('t1.empID ' ,$empID);
        $this->db->where('t1.evalFormID ' ,$evalFormID); 
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
        
    }
	
	function get_eval_count_people($empID,$evalFormID,$evalType){
        $this->db->select('count(*) as count_proples');
        $this->db->from('tbl_eval as t1');
        $this->db->where('t1.empID ' ,$empID);
        $this->db->where('t1.evalFormID ' ,$evalFormID); 
		$this->db->where('t1.evalType ' ,$evalType);
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
    }
	
	function get_eval_staff($empID,$evalFormID){
        $this->db->select('SUM(evalScore) as sumScore');
        $this->db->from('tbl_eval as t1');
        $this->db->where('t1.empID ' ,$empID);
        $this->db->where('t1.evalFormID ' ,$evalFormID); 
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($data['sumScore'] === null) ? -1 : $data['sumScore']; 
    }

	function get_eval_count_subject_group_subject($evalFormID) {		
		$this->db->select('  count(*) as subject_group_subject ');
        $this->db->from('`tbl_eval_group_subject` as t1 ');
        $this->db->where('t1.evalFormID' ,$evalFormID);
		$this->db->join('tbl_eval_subject as t2' ,'t1.evalGroupSubjectID = t2.evalGroupSubjectID' ,'inner'); 
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;        
    }
	
	function get_eval_edit($empID,$evalFormID,$evalSubjectID="") {
        $this->db->select('*');
        $this->db->from('tbl_eval as t1');
        $this->db->where('t1.empID' ,$empID);
        $this->db->where('t1.evalFormID ' ,$evalFormID); 
		if($evalSubjectID != "") {
			$this->db->where('t1.evalSubjectID ' ,$evalSubjectID); 
		}
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
    }

	function get_eval_data($empID,$evalFormID,$evalSubjectID="") {
        $this->db->select('*');
        $this->db->from('tbl_eval as t1');
        $this->db->where('t1.empID' ,$empID);
        $this->db->where('t1.evalFormID ' ,$evalFormID); 
		if($evalSubjectID != "") {
			$this->db->where('t1.evalSubjectID ' ,$evalSubjectID); 
		}
		$this->db->order_by("t1.evalVariable","desc");
        $query      = $this->db->get();
        $data       = $query->result(); 
        return ($query->num_rows() > 0) ? $data : NULL;
    }
	
	function get_competency_data($empID,$evalFormID,$evalType="") {
        $this->db->select('*');
        $this->db->from('tbl_eval as t1');
        $this->db->where('t1.empID' ,$empID);
        $this->db->where('t1.evalFormID ' ,$evalFormID); 
		if($evalType != "") {
			$this->db->where('t1.evalType ' ,$evalType); 
		}
		$this->db->order_by("t1.evalVariable","desc");
        $query      = $this->db->get();
        $data       = $query->result(); 
        return ($query->num_rows() > 0) ? $data : NULL;
    }
	

	function get_eval_notes($empID,$evalFormID,$evalType){
        $this->db->select('*');
        $this->db->from('tbl_eval_notes as t1');
        $this->db->where('t1.empID ' ,$empID); 
		$this->db->where('t1.evalFormID ' ,$evalFormID); 
		$this->db->where('t1.evalType ' ,$evalType); 
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
        
    }
	
	
	/*
		$this->db->select('word')->from('words')->where('wordID', 3);
		$sub = $this->subquery->start_subquery('select');
		$sub->select('number')->from('numbers')->where('numberID', 2);
		$this->subquery->end_subquery('number');
	*/
	
	function get_evalScore1(){
		$this->db->select('t1.empID,t2.staffPreName ,t2.staffFName , t2.staffLName ,t1.evalFormID ,sum(t1.evalScore) as s_evalScore , COUNT(t1.empID) as c_empID');
        $this->db->from('`tbl_eval` as t1 ');
        // $this->db->where('t1.empID ' ,$empID);
	    $this->db->group_by('empID'); 
		$this->db->join('tbl_staff as t2' ,'t1.empID = t2.ID' ,'left'); 
        $query      = $this->db->get();
        $data       = $query->result(); 
        return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	function get_evalScore($org_array,$assingID){
		
		/*$query = $this->db->query('SELECT `t1`.`empID` , t3.staffPreName , t3.staffFName , t3.staffLName ,
									`t1`.`evalFormID`, sum(t1.evalScore) as s_evalScore, COUNT(t1.empID) as c_empID ,
									(
										SELECT levelText FROM tbl_eval_form_level WHERE evalFormID = t1.evalFormID and sum(t1.evalScore) BETWEEN minScore and maxScore
									) as grade
									FROM (`tbl_eval` as t1) 
									LEFT JOIN tbl_staff as t3 ON `t1`.`empID`= t3.ID
									GROUP BY t1.empID ORDER BY empID ASC'
							);
		$query = $this->db->query('SELECT `t1`.`empID`   ,t4.orgID, t3.staffPreName , t3.staffFName , t3.staffLName , `t1`.`evalFormID`, sum(t1.evalScore) as s_evalScore,
									 COUNT(t1.empID) as c_empID ,
									 ( SELECT levelText FROM tbl_eval_form_level 
																			WHERE evalFormID = t1.evalFormID and sum(t1.evalScore) BETWEEN minScore and maxScore 
										) as grade 
									FROM (`tbl_eval` as t1) 
									LEFT  JOIN tbl_staff as t3 ON `t1`.`empID`= t3.ID  
									LEFT JOIN tbl_staff_work t4 ON t3.ID = t4.staffID
									 
									GROUP BY t1.empID '
							);
		 $query = $this->db->query('SELECT `t1`.`empID` ,t4.orgID , t3.staffPreName , t3.staffFName , t3.staffLName  ,`t1`.`evalFormID`, sum(t1.evalScore) as s_evalScore,
									 COUNT(t1.empID) as c_empID ,
									 ( 		SELECT levelText FROM tbl_eval_form_level 
										   	WHERE evalFormID = t1.evalFormID and sum(t1.evalScore) BETWEEN minScore and maxScore 
									  ) as grade 
									FROM (`tbl_eval` as t1) 
									INNER JOIN tbl_staff_work as t4 ON t1.empID = t4.staffID
									INNER JOIN tbl_staff as t3 ON t4.staffID = t3.ID
									-- LEFT  JOIN tbl_staff as t3 ON `t1`.`empID`= t3.ID  
									-- LEFT JOIN tbl_staff_work t4 ON t3.ID = t4.staffID
									WHERE t4.orgID in ('.$org_array.')
									GROUP BY t1.empID '
							);	 */				
		if($org_array !=''){
			$query = $this->db->query(' 
										SELECT `t1`.`empID` ,t4.orgID, t3.staffPreName , t3.staffFName , t3.staffLName , `t1`.`evalFormID`, 
										sum(t1.evalScore) as s_evalScore, 
										COUNT(t1.empID) as c_empID , 
										( SELECT levelText FROM tbl_eval_form_level WHERE evalFormID = t1.evalFormID and sum(t1.evalScore) BETWEEN minScore and maxScore  limit 1) as grade 
										FROM (`tbl_eval` as t1) 
										LEFT JOIN tbl_staff as t3 ON `t1`.`empID`= t3.ID 
										
										LEFT JOIN tbl_staff_work t4 ON t3.ID = t4.staffID 
										LEFT JOIN tbl_org_chart t5 ON  t4.orgID = t5.orgID
										
										WHERE t5.`assignID` =  '.$assingID.'  AND  t5.orgID  in  ('.$org_array.') 
										GROUP BY t1.empID
											
				
									');		
									
		}else{
			$query = $this->db->query(' 
										SELECT `t1`.`empID` ,t4.orgID, t3.staffPreName , t3.staffFName , t3.staffLName , `t1`.`evalFormID`, 
										sum(t1.evalScore) as s_evalScore, 
										COUNT(t1.empID) as c_empID , 
										( SELECT levelText FROM tbl_eval_form_level WHERE evalFormID = t1.evalFormID and sum(t1.evalScore) BETWEEN minScore and maxScore ) as grade 
										FROM (`tbl_eval` as t1) 
										LEFT JOIN tbl_staff as t3 ON `t1`.`empID`= t3.ID 
										
										LEFT JOIN tbl_staff_work t4 ON t3.ID = t4.staffID 
										LEFT JOIN tbl_org_chart t5 ON  t4.orgID = t5.orgID
										
										WHERE t5.`assignID` =  '.$assingID.'  AND  t5.orgID  in  (00) 
										GROUP BY t1.empID
											
				
									');				
		}
        return $query->result(); 
		 
	}
	
	/*function get_count_evalPerson($empArray){
		$this->db->select(' COUNT(DISTINCT empID) as person ');
        $this->db->from('`tbl_eval` as t1 ');
  		//$this->db->join('tbl_staff as t2' ,'t1.empID = t2.ID' ,'left'); 
		$this->db->where('t1.empID',$empArray);
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
		
	}*/
	
	function get_count_evalPerson($orgID){
		$this->db->select(' count(DISTINCT t1.empID) as person');
        $this->db->from('`tbl_eval` as t1');
  		$this->db->join('tbl_staff_work as t2' ,'t1.empID = t2.staffID' ,'inner'); 
		$this->db->join('tbl_staff as t3' ,'t2.staffID = t3.ID' ,'inner');
		$this->db->where('t2.orgID',$orgID);
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	/*function get_count_evalScore($evalFormID){
		$this->db->select(' SUM(evalScore) as score ');
        $this->db->from('`tbl_eval` as t1 ');
  		//$this->db->join('tbl_staff as t2' ,'t1.empID = t2.ID' ,'left'); 
		//$this->db->where('evalFormID', $evalFormID);
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
		
	}*/
	
	function get_count_evalScore($orgID){
		$this->db->select('  SUM(evalScore) as score');
        $this->db->from('`tbl_eval` as t1');
  		$this->db->join('tbl_staff_work as t2' ,'t1.empID = t2.staffID' ,'inner'); 
		$this->db->join('tbl_staff as t3' ,'t2.staffID = t3.ID' ,'inner');
		$this->db->where('t2.orgID',$orgID);
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	// ## ORG 
	
	function get_org_upper($upperOrgID, $assignID){
		$this->db->select('group_concat(orgID)');
        $this->db->from('tbl_org_chart ');
  		//$this->db->join('tbl_staff as t2' ,'t1.empID = t2.ID' ,'left'); 
		$this->db->where('upperOrgID', $upperOrgID);
		$this->db->where('assignID', $assignID);
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	function structural_work($upperOrgID,$status,$assignID){
		  
		
		if($status == 'department'){   // แผนก
					$query = $this->db->query('
										 SELECT * FROM tbl_org_chart as t1  WHERE (t1.orgID = "'.$upperOrgID.'" and t1.assignID = "'.$assignID.'" )    '
	      							);
		}elseif($status == 'pile'){ // กอง
					$query = $this->db->query('
										 SELECT * FROM tbl_org_chart as t1  WHERE (t1.orgID = "'.$upperOrgID.'" and t1.assignID = "'.$assignID.'" )    '
	      							);
		}elseif($status == 'party'){ // ฝ่าย
				  $query = $this->db->query('
										 SELECT * FROM tbl_org_chart as t1  WHERE (t1.orgID = "'.$upperOrgID.'" and t1.assignID = "'.$assignID.'" )    '
	      							);
		}
		
 		$data       = $query->row_array(); 
  		
		return ($query->num_rows() > 0) ? $data : NULL; 
		
		
	}
	
	function structural_staff($staffID){
	
		$query = $this->db->query('
										SELECT t1.staffID,t1.orgID FROM `tbl_staff_work` as t1 WHERE t1.staffID = '.$staffID.' '
	      							);
		$data       = $query->row_array(); 
  		
		return ($query->num_rows() > 0) ? $data : NULL; 
	
	}


	function getEvalForm($evalYear='', $evalRound='', $evalType){
		$sql = "select evalFormID,evalRound,evalYear from tbl_eval_form where 1 ";
		$sql .= $evalYear==null? '' : " and evalYear = '$evalYear' " ;
		$sql .= $evalRound==null? '' : "and evalRound = '$evalRound' ";
		$sql .= " and evalType='$evalType' order by evalYear desc,evalRound DESC limit 1";
		return $this->db->query($sql)->row();
	}
		
	 
} 