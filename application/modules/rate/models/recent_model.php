<?php

class Recent_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	} 
 
 
   function recent_get_dropdown_all($id , $name ,$table){
		   		 
				$this->db->select($name);
				//$this->db->order_by($name, "asc");
				$this->db->from($table);
				$this->db->limit(1); 
				$this->db->order_by($name,'desc');		  
				$query 		= $this->db->get();
				$dropdowns	= $query->result();
  				
				if ($query->num_rows() > 0){
					foreach($dropdowns as $dropdown){
						//echo $fruit = array_shift($dropdown);
						$dropdownlist['']	= "กรุณาระบุด้วย"; 
						$dropdownlist[$dropdown->$name]	= $dropdown->$name  ;
 					}	
				}else{
 						$dropdownlist['']	= " กรุณาระบุด้วย";
 				} 
 				
				$finaldropdown	= $dropdownlist;
				return $finaldropdown;
	} 
	
	 function recent_get_dropdown_roundRound($where,$evalRound){
 		 
		 if($evalRound !=''){
			 $rows = $this->db
           				->select('*')
            			->where('t1.evalYear', $where)
						//->where('t1.evalRound', $evalRound)
           				->order_by('evalRound','asc')
            			->get('tbl_eval_date as t1')
            			->result();
						
		}else{
			 $rows = $this->db
           				->select('*')
            			->where('t1.evalYear', $where)
		
           				 ->order_by('evalRound','asc')
            			->get('tbl_eval_date as t1')
            			->result();
		}
        $tree = '';
         
        if (count($rows) > 0) {
            $i = 1;
			$tree  .=  "<select name=\"evalRound\" id=\"evalRound\" class=\"form-control\" >";
            $tree  .=  "<option value=\"\" >กรุณาระบุรอบด้วย</option>";
            foreach ($rows as $key  =>$row) { 
				 if($evalRound == $row->evalRound){ 
                        $selected =  "selected='selected'" ;
                    }else{
                        $selected = ""; 
                    }
                $tree  .=  "<option value='".$row->evalRound."' ".$selected." >";    
                $tree  .=  $row->evalRound;
                $tree  .=  "</option>";
            }
			 $tree  .=  "</select>";
        }
        return $tree;
	} 
	
	function recent_get_dropdown_org($id , $name ,$table,$where){
		   		 
				$this->db->select(' * ');
				$this->db->order_by($name, "asc");
				$this->db->from($table);
				$this->db->where('assignID', $where);
				$this->db->where('upperOrgID', 1);
				//$this->db->like('orgName', 'ฝ่าย', 'after');	  
				$query 		= $this->db->get();
				$dropdowns	= $query->result();
  				
				if ($query->num_rows() > 0){
					foreach($dropdowns as $dropdown){
						//echo $fruit = array_shift($dropdown);
						$dropdownlist['']	= "กรุณาระบุด้วย";
						$dropdownlist[$dropdown->$id]	= $dropdown->$name;
 					}	
				}else{
 						$dropdownlist['']	= " กรุณาระบุด้วย";
 				} 
 				
				$finaldropdown	= $dropdownlist;
				return $finaldropdown;
				
	} 
	
	function org_faction($assignID){
 	
			/*$this->db->select(' t1.orgID,t1.orgName,t1.assignID,t2.evalRound,t2.evalYear,t2.completeDate  ');
 			$this->db->from('tbl_org_chart as t1');
			$this->db->where('t1.assignID', $assignID);
			$this->db->like('t1.orgName', 'ฝ่าย', 'after');	 
			$this->db->join('tbl_eval_submit as t2' ,'t1.orgID = t2.orgID' ,'left');  
			$this->db->order_by('t2.completeDate','desc');
			$query 	= $this->db->get();
			$data 	= $query->result();
			return ($query->num_rows() > 0) ? $data : NULL;*/
			$query = $this->db->query("
											SELECT t1.orgID,t1.orgName,t1.assignID,t2.evalRound,t2.evalYear,t2.completeDate FROM tbl_org_chart  as t1 
											LEFT JOIN tbl_eval_submit as t2 ON t2.orgID = t1.orgID 
											-- WHERE t1.assignID = ".$assignID."  and ( t1.orgName LIKE '%ฝ่าย%'  or t1.orgName LIKE '%สำนักงาน%' ) 
											WHERE t1.assignID = ".$assignID." and t1.upperOrgID = 1 
											ORDER BY t2.completeDate desc, t1.orgName ASC
									 ");
        	$data	= $query->result(); 
  			return ($query->num_rows() > 0) ? $data : NULL;
		
	}
		
 	function insert_submit($data){
		
		$evalYear		= $data['evalYear'];
		$evalRound		= $data['evalRound'];
		$orgID			= $data['orgID'];
 		 
		$this->db->select('*');
		$this->db->from('tbl_eval_submit as t1');
		$this->db->where('t1.evalYear' ,$evalYear); 
		$this->db->where('t1.evalRound' ,$evalRound); 
		$this->db->where('t1.orgID' ,$orgID); 
  		$query 		= $this->db->get();
 		 
		if($query->num_rows() == 0){
				$sql	=	$this->db->insert("tbl_eval_submit",$data);
				//exit();	 
				if($this->db->affected_rows() > 0){
						 return 'succesfully';
				}else{
						 return 'failed';
				}
		}else{
				return 'failed';
		}
  	}
	
	
	function update_submit($evalYear,$evalRound,$data){ 
  			
			//$this->db->where('evalYear', $evalYear);
			//$this->db->where('evalRound', $evalRound);
			//$this->db->update('tbl_eval_submit',$data); 	
			 
			//if($this->db->affected_rows() > 0){
					// return 'succesfully';
			//}else{
					 //return 'failed';
			//}
  	}
	
	function recent_submit($evalYear,$evalRound,$orgID){
 	
			$this->db->select(' * ');
 			$this->db->from('tbl_eval_submit as t1');
			$this->db->where('t1.evalYear' ,$evalYear); 
			$this->db->where('t1.evalRound' ,$evalRound); 
			$this->db->where('t1.orgID' ,$orgID);   
			$query 	= $this->db->get();
			$data 	= $query->row_array();
			return ($query->num_rows() > 0) ? true : false;
		
	}
	 
	 function get_recent_submit($evalYear,$evalRound,$orgID){
 	
			$this->db->select(' * ');
 			$this->db->from('tbl_eval_submit as t1');
			$this->db->where('t1.evalYear' ,$evalYear); 
			$this->db->where('t1.evalRound' ,$evalRound); 
			$this->db->where('t1.orgID' ,$orgID);   
			$query 	= $this->db->get();
			$data 	= $query->row_array();
			return ($query->num_rows() > 0) ? $data : NULL;
		
	}
} 