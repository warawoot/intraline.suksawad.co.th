
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	
	<h5 style="text-align:center;">
		<br>
			รายงานจำนวนพนักงานที่ออกราชการในปีงบประมาณ <?php echo date("Y")+543;?>
		
	</h5>
	<br><br>

	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th>ฝ่าย / สำนัก</th>
			<th>เกษียณปกติ</th>
			<th>เกษียณก่อนกำหนด</th>
			<th>ลาออก</th>
			<th>เลิกจ้าง</th>
			<th>ตาย</th>
			<th>รวม</th>
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 
					$i++; 
					$countexp = 0;
					$countexpb = 0;
					$countresign = 0;
					$countlayoff = 0;
					$countdeath = 0;
					
						$ro = $this->staff_model->getStatusByOrg($row->orgID);
						if(!empty($ro)){

							$dn = date('N', strtotime((date("Y")-1).'-10-01'));

							foreach($ro as $rowo){

								if($dn == 6){
									//Saturday
									if($rowo->workStartDate == (date("Y")-1).'-10-03')
										continue;
									
								}else if($dn == 7){
									//Sunday
									if($rowo->workStartDate == (date("Y")-1).'-10-02')
										continue;
										
								}else if($rowo->workStartDate == (date("Y")-1).'-10-01')
									continue;

								if($rowo->workStatusID == $this->config->item('work_status_retired')){
									$expDate = getExpiredDateByWork($rowo->staffBirthday,true);
									$list = explode("-",$expDate);
									if($list[0] == date("Y"))
										$countexp++;
									else
										$countexpb++;
									
								}else if($rowo->workStatusID == $this->config->item('work_status_resign'))
									$countresign++;
								else if($rowo->workStatusID == $this->config->item('work_status_layoff'))
									$countlayoff++;
								else if($rowo->workStatusID == $this->config->item('work_status_death'))
									$countdeath++;
								
							}
						}
					?>
					<tr>
						<td style="text-align:left;"><?php echo $row->orgName; ?></td>
						<td><?php echo $countexp; ?></td>
						<td><?php echo $countexpb; ?></td>
						<td><?php echo $countresign; ?></td>
						<td><?php echo $countlayoff; ?></td>
						<td><?php echo $countdeath; ?></td>
						<td><?php echo ($countexp+$countexpb+$countresign+$countlayoff+$countdeath); ?></td>
					
						
					</tr>
				
				
		<?php }}

		if($i == 0)
			echo '<tr><td colspan="7">ไม่พบข้อมูล</td></tr>';
		 ?>
		
	</table>
	</div>


	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportstatstatus/print_pdf');
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportstatstatus/print_excel');
        }

        
</script>
