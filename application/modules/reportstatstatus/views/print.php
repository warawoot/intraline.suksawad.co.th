
<h4 style="text-align:center;">
		<br>
			รายงานจำนวนพนักงานที่ออกราชการในปีงบประมาณ <?php echo date("Y")+543;?>
		
	</h4>
	<br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th>ฝ่าย / สำนัก</th>
			<th>เกษียณปกติ</th>
			<th>เกษียณก่อนกำหนด</th>
			<th>ลาออก</th>
			<th>เลิกจ้าง</th>
			<th>ตาย</th>
			<th>รวม</th>
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 
					$i++; 
					$countexp = 0;
					$countexpb = 0;
					$countresign = 0;
					$countlayoff = 0;
					$countdeath = 0;
					
						$ro = $this->staff_model->getStatusByOrg($row->orgID);
						if(!empty($ro)){

							$dn = date('N', strtotime((date("Y")-1).'-10-01'));

							foreach($ro as $rowo){

								if($dn == 6){
									//Saturday
									if($rowo->workStartDate == (date("Y")-1).'-10-03')
										continue;
									
								}else if($dn == 7){
									//Sunday
									if($rowo->workStartDate == (date("Y")-1).'-10-02')
										continue;
										
								}else if($rowo->workStartDate == (date("Y")-1).'-10-01')
									continue;

								if($rowo->workStatusID == $this->config->item('work_status_retired')){
									$expDate = getExpiredDateByWork($rowo->staffBirthday,true);
									$list = explode("-",$expDate);
									if($list[0] == date("Y"))
										$countexp++;
									else
										$countexpb++;
									
								}else if($rowo->workStatusID == $this->config->item('work_status_resign'))
									$countresign++;
								else if($rowo->workStatusID == $this->config->item('work_status_layoff'))
									$countlayoff++;
								else if($rowo->workStatusID == $this->config->item('work_status_death'))
									$countdeath++;
								
							}
						}
					?>
					<tr>
						<td ><?php echo $row->orgName; ?></td>
						<td style="text-align:center;"><?php echo $countexp; ?></td>
						<td style="text-align:center;"><?php echo $countexpb; ?></td>
						<td style="text-align:center;"><?php echo $countresign; ?></td>
						<td style="text-align:center;"><?php echo $countlayoff; ?></td>
						<td style="text-align:center;"><?php echo $countdeath; ?></td>
						<td style="text-align:center;"><?php echo ($countexp+$countexpb+$countresign+$countlayoff+$countdeath); ?></td>
					
						
					</tr>
				
				
		<?php }}

		if($i == 0)
			echo '<tr><td colspan="7">ไม่พบข้อมูล</td></tr>';
		 ?>
		
	</table>
	</div>
