<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="adiscover">
        <meta name="keywords" content="HTML,CSS,XML,JavaScript">
        <meta name="author" content="Warawoot Petchphoo">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="libraries/instashow/elfsight-instagram-feed.js"></script>
        <style>
            body {
                margin : 0px;
            }
        </style>
    </head>
    <body>
        <div data-is
            data-is-api="libraries/instashow/api/index.php"
            data-is-source="<?=$feed?>"
            data-is-post-template="classic"
            data-is-width="auto"
            data-is-layout="grid"
            data-is-columns="5"
            data-is-rows="3"
            data-is-gutter="20"
            data-is-lang="en"
            >
        </div>
    </body>
</html>