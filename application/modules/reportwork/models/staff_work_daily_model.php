<?php
class Staff_work_daily_model extends Default_Model{

  function __construct() {    	
      parent::__construct();
      $this->_table = 'tbl_staff_work_daily';
      $this->_pk = 'dailyID';
      $this->_timestamps = false;
  }

}            
