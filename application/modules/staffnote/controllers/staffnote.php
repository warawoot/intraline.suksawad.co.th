<?php

class Staffnote extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		Xcrud_config::$editor_url = base_url().'editors/ckeditor/ckeditor.js';
		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_note')->where('staffID =', $id);
		
		
		//// List /////
		$col_name = array(
			'noteName' => 'ชื่อเรื่อง',
			'noteDesc' => 'รายละเอียด'

		);

		$xcrud->columns('noteName,noteDesc');
		$xcrud->label($col_name);
		// End List//

		//// Form //////

		$xcrud->pass_var(array('staffID'=>$id));
		$xcrud->fields('noteName,noteDesc');

		$data['html'] = $xcrud->render();
		

		$data['id'] = $id;
		$data['title'] = "Note";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

}
/* End of file staffnote.php */
/* Location: ./application/module/staffnote/staffnote.php */