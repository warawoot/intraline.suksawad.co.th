<?php

class Staffmistake extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_mistake')->where('staffID =', $id);
		$xcrud->relation('mistakeTypeID','tbl_mistake_type','mistakeID','mistakeName');
		
		//// List /////
		$col_name = array(
			'mistakeNo' => 'เลขที่คำสั่ง',
			'mistakeName' => 'เรื่อง',
			'mistakeDate' => 'วันที่มีผล',
			'mistakeTypeID' => 'ประเภท',
			'mistakePrice' => 'หักเงินเดือนร้อยละ (โทษตัดเงินเดือน)',
			'mistakeMonth' => 'จำนวนเดือน',
			'mistakeDetail' => 'รายละเอียด',
			'mistakeFile' => 'เอกสารประกอบ',
			'mistakeStatus' => ''

		);

		$xcrud->columns('mistakeNo,mistakeDate,mistakeTypeID');
		$xcrud->label($col_name);
		//$xcrud->column_pattern('mistakeStartDate',' {value} {mistakeEndDate}');
		//$xcrud->column_callback('mistakeStartDate','show_mistake_date');
		$xcrud->column_callback('mistakeDate','toBDDate');
		//$xcrud->unset_numbers()->unset_pagination()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud->pass_var(array('staffID'=>$id));
		$xcrud->fields('mistakeNo,mistakeName,mistakeDetail,mistakeDate,mistakeTypeID,mistakePrice,mistakeMonth,mistakeFile,mistakeStatus');
		$xcrud->change_type('mistakeFile', 'file', '', array('not_rename'=>true));
		$xcrud->change_type('mistakeStatus','radio','',array('1'=>'พ้นจากการเป็นผู้ค้ำประกัน','2'=>'พ้นจากการเป็นผู้ถูกค้ำประกัน'));
		//$xcrud->change_type('mistakeType','select','',array('1'=>'ไล่ออก','2'=>'เลิกจ้าง','3'=>'ติดเงินเดือน','4'=>'ภาคทัณฑ์'));
		$xcrud->validation_required('mistakeNo,mistakeDate,mistakeTypeID');

		$data['html'] = $xcrud->render();
		

		$data['id'] = $id;
		$data['title'] = "ความผิด / โทษ";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

}
/* End of file staffmistake.php */
/* Location: ./application/module/staffmistake/staffmistake.php */