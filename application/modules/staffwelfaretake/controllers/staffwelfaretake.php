<?php

class Staffwelfaretake extends MY_Controller {
   
    function __construct() {
        parent::__construct();

        $this->setModel('staff_welfare_take_model');

        $this->load->model('staff_model');
        $this->load->model('welfare_take_child_model');
		$this->load->model('welfare_take_edu_model');
		$this->load->model('welfare_take_milk_model');
		$this->load->model('welfare_take_hospital_model');
		$this->load->model('welfare_take_pfund_model');
		$this->load->model('welfare_take_image_model');
    }
 

    public function index() {
         
        $this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();

        $xcrud->table('tbl_staff');
        $xcrud->relation('positionID','tbl_position','positionID','positionName');

        $col_name = array(
            'staffID' => 'รหัสพนักงาน',  
            'staffFName' => 'ชื่อ',
            'rankID' => 'ระดับ',
            'positionID' => 'ตำแหน่ง',
            'orgID' => 'ฝ่าย / สำนักงาน'
           

        );
        $xcrud->columns('staffID,staffFName,rankID,positionID,orgID');
        $xcrud->label($col_name);
        $xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
        $xcrud->column_callback('rankID','calRankWork');
        $xcrud->column_callback('positionID','calPositionWork');
        $xcrud->column_callback('orgID','calOrgIDWork');

        $xcrud->column_width('staffID','5%');

        $xcrud->unset_add()->unset_view()->unset_edit()->unset_remove();
        $xcrud->button(site_url().'staffwelfaretake/welfare/?token={ID}','สิทธิ์สวัสดิการ','fa fa-plus-square','btn-success');

        $data['html'] = $xcrud->render();
        $data['title'] = "เบิกสวัสดิการ";
        $this->template->load('template/admin2', 'main',$data);
    }

    public function welfare(){

        $data['staffID'] = $this->input->get('token',true);

        $this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();
        $xcrud->table('tbl_staff_welfare_take')->where('staffID =',$data['staffID']);
        $xcrud->relation('welTypeID','tbl_welfare_type','welTypeID','welTypeName');
        $col_name = array(
            'welTypeID' => 'ประเภทสวัสดิการ',
            'takeDate'  => 'วันที่เบิก',
            'takePersonType'   => 'ผู้ได้รับสิทธิ์',
            'personID'  => 'รูปแบบ'
        );

        $xcrud->columns('welTypeID,personID,takeDate,takePersonType');
        $xcrud->label($col_name);
        $xcrud->column_callback('takeDate','toBDDate');
        $xcrud->column_callback('personID','show_staff_welfix_take');
        $xcrud->column_callback('takePersonType','show_person_type');

        $xcrud->unset_add()->unset_view()->unset_edit()->unset_remove();

        $xcrud->button(site_url().'staffwelfaretake/view/?token={takeID}&staff='.$data['staffID'],'ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
        $xcrud->button(site_url().'staffwelfaretake/edit/?token={takeID}&staff='.$data['staffID'],'แก้ไข','glyphicon glyphicon-edit','btn-warning');
        $xcrud->button('javascript:deleteTake({takeID});','ลบ','glyphicon glyphicon-remove','btn-danger');


        $data['html'] = $xcrud->render();
        $data['staffName'] = getStaffName($data['staffID']);
        $data['title'] = "เบิกสวัสดิการ";
        $this->template->load("template/admin2",'main', $data);
    }

    public function add(){

        $data['staffID'] = $this->input->get('token',true);
        $data['title'] = "เบิกสวัสดิการ";
        $this->template->load("template/admin2",'form', $data);
    }

    public function submit(){

        if(!empty($_POST['staffID'])){

        	$takeID                =   mysql_real_escape_string(trim($this->input->post('takeID',true)));
            $staffID               =   mysql_real_escape_string(trim($this->input->post('staffID',true)));
        	$takeDate              =   mysql_real_escape_string(trim($this->input->post('takeDate',true)));
        	$takePersonType        =   mysql_real_escape_string(trim($this->input->post('takePersonType',true)));
        	$personID              =   (isset($_POST['personID'])) ? mysql_real_escape_string(trim($this->input->post('personID',true))) : $staffID; 
            $welfareType           =   mysql_real_escape_string(trim($this->input->post('welfareType',true)));
   			$welTypeIDO			   =   mysql_real_escape_string(trim($this->input->post('welTypeIDO',true)));
            ////////start transaction///////////
            $this->db->trans_begin();


            $path = './assets/welfare/';

            


            $arr = array(
                'welTypeID'         => $welfareType,
                'takeDate'          => toCEDate($takeDate),
                'takePersonType'   => $takePersonType,
                'staffID'           => $staffID,
                'personID'          => $personID
            );  

            $lasted_id = $this->save($arr,$takeID);



            if(!empty($_FILES['takefile'])){
							
				$numFile = count($_FILES['takefile']['name']);		
				// Upload Photo 
				  for($i=0; $i< $numFile; $i++){
					 if($_FILES['takefile']['name'][$i] != ''){
					 
					 	$upname=($_FILES["takefile"]["name"][$i]);
						$ext = substr($upname, strrpos($upname, '.') + 1);
							

						
						$name = md5(round(microtime(true) * 1000));
						
						
						$file_path = $path . $name.'.'.$ext;
						if(move_uploaded_file($_FILES['takefile']['tmp_name'][$i], $file_path)) {
							
							//image_thumb('assets/welfare/'.$name.'.'.$ext);
							
							$tid = (!empty($takeID)) ? $takeID : $lasted_id;
							$arr_image = array(
								'takeID'			=>  $tid,
								'takeFile'			=> $name.'.'.$ext	
							); 
							$this->welfare_take_image_model->save($arr_image);	
						}
					
						
					  }
				}
			}



            if((!empty($takeID)) && (!empty($welTypeIDO)) && ($welTypeIDO != $welfareType)){

            	// delete old welfare//

            	switch($welTypeIDO){
            		// ค่าช่วยเหลือบุตร //
					case  '1' : 

						$this->welfare_take_child_model->delete_by(array('takeID'=>$takeID));
						break;
					// ค่าการศึกษาบุตร //
					case  '2' : 
						$this->welfare_take_edu_model->delete_by(array('takeID'=>$takeID));
						break;
					// ค่านมสวัสดิการ //
					case  '3' : 
						$this->welfare_take_milk_model->delete_by(array('takeID'=>$takeID));
						break;
					// ค่ารักษาพยาบาล //
					case  '4' : 
						$this->welfare_take_hospital_model->delete_by(array('takeID'=>$takeID));
						break;
					// กองทุนสำรองเลี้ยงชีพ //
					case  '6' : 
						$this->welfare_take_pfund_model->delete_by(array('takeID'=>$takeID));
						break;
            	}

            }

            switch($welfareType){
				// ค่าช่วยเหลือบุตร //
				case  '1' : 
					$takeChildID = mysql_real_escape_string(trim($this->input->post('takeChildID',true)));
					$welfarePrice = 	mysql_real_escape_string(trim($this->input->post('welfarePrice',true)));

					if(empty($takeID)){
						$arr_child = array( 'takeID' 		=> $lasted_id,
										'welfarePrice' 	=> $welfarePrice);
					}else{
						if(!empty($takeChildID)){
							$arr_child = array( 'welfarePrice' 	=> $welfarePrice);
						}else{
							$arr_child = array( 'takeID' 		=> $takeID,
										'welfarePrice' 	=> $welfarePrice);
						}
					}
					
					$this->welfare_take_child_model->save($arr_child,$takeChildID);
					
					break;
				// ค่าการศึกษาบุตร //
				case  '2' : 
					$takeEduID = mysql_real_escape_string(trim($this->input->post('takeEduID',true)));
					$priceStatus = mysql_real_escape_string(trim($this->input->post('priceStatus',true)));
					$welfarePrice = mysql_real_escape_string(trim($this->input->post('welfarePrice',true)));
					if(empty($takeID)){
						$arr_edu = array( 'takeID' 		=> $lasted_id,
										'priceStatus'		=> $priceStatus,
										'welfarePrice'	=> $welfarePrice);
						$this->welfare_take_edu_model->save($arr_edu);
					}else{
						if($welTypeIDO != $welfareType){
							$arr_edu = array( 'takeID' 		=> $takeID,
										'priceStatus'		=> $priceStatus,
										'welfarePrice'	=> $welfarePrice);

							$this->welfare_take_edu_model->save($arr_edu);
						}
						
					}
					/*
					$eduTypeID = 	mysql_real_escape_string(trim($this->input->post('eduTypeID',true)));
					$welfareaccept = 	mysql_real_escape_string(trim($this->input->post('welfareaccept',true)));
					$eduID = 	mysql_real_escape_string(trim($this->input->post('eduID',true)));
					$eduFactID = 	mysql_real_escape_string(trim($this->input->post('eduFactID',true)));
					$welfarePrice = 	mysql_real_escape_string(trim($this->input->post('welfarePrice',true)));
					if(empty($takeID)){
						$arr_edu = array( 'takeID' 		=> $lasted_id,
										'eduTypeID'		=> $eduTypeID,
										'eduHaveFund'	=> $welfareaccept,
										'eduID'			=> $eduID,
										'eduFactID'		=> $eduFactID,
										'welfarePrice' 	=> $welfarePrice);
						$this->welfare_take_edu_model->save($arr_edu);
					}else{
						if($welTypeIDO != $welfareType){
							$arr_edu = array( 'takeID' 		=> $takeID,
										'eduTypeID'		=> $eduTypeID,
										'eduHaveFund'	=> $welfareaccept,
										'eduID'			=> $eduID,
										'eduFactID'		=> $eduFactID,
										'welfarePrice' 	=> $welfarePrice);

							$this->welfare_take_edu_model->save($arr_edu);
						}
						
					}*/

					
					/*if(!empty($takeEduID)){
						$arr_edu = array( 'eduTypeID'		=> $eduTypeID,
										'eduHaveFund'	=> $welfareaccept,
										'eduID'			=> $eduID,
										'eduFactID'		=> $eduFactID,
										'welfarePrice' 	=> $welfarePrice);
					}else{
						$arr_edu = array( 'takeID' 		=> $lasted_id,
										'eduTypeID'		=> $eduTypeID,
										'eduHaveFund'	=> $welfareaccept,
										'eduID'			=> $eduID,
										'eduFactID'		=> $eduFactID,
										'welfarePrice' 	=> $welfarePrice);
					}*/
					
					
					break;
				// ค่านมสวัสดิการ //
				case  '3' : 
					$takeMilkID = mysql_real_escape_string(trim($this->input->post('takeMilkID',true)));
					$startAge = 	mysql_real_escape_string(trim($this->input->post('startAge',true)));
					$endAge = 	mysql_real_escape_string(trim($this->input->post('endAge',true)));
					$milkNum = 	mysql_real_escape_string(trim($this->input->post('milkNum',true)));
					/*if(!empty($takeMilkID)){
						$arr_milk = array('startAge'		=> $startAge,
										'endAge'		=> $endAge,
										'milkNum'		=> $milkNum);
					}else{
						$arr_milk = array( 'takeID' 		=> $lasted_id,
										'startAge'		=> $startAge,
										'endAge'		=> $endAge,
										'milkNum'		=> $milkNum);
					}
					*/

					if(empty($takeID)){
						$arr_milk = array( 'takeID' 		=> $lasted_id,
										'startAge'		=> $startAge,
										'endAge'		=> $endAge,
										'milkNum'		=> $milkNum);
						$this->welfare_take_milk_model->save($arr_milk);
					}else{
						if($welTypeIDO != $welfareType){
							$arr_milk = array( 'takeID' 		=> $takeID,
											'startAge'		=> $startAge,
											'endAge'		=> $endAge,
											'milkNum'		=> $milkNum);
							$this->welfare_take_milk_model->save($arr_milk);
						}

					}
					
					
					break;
				// ค่ารักษาพยาบาล //
				case  '4' :
					$takeHosID = mysql_real_escape_string(trim($this->input->post('takeHosID',true)));
					$hosID 		= 	mysql_real_escape_string(trim($this->input->post('hosID',true)));
					$diseaseName 		= 	mysql_real_escape_string(trim($this->input->post('diseaseName',true)));
					$patTypeID 		= 	mysql_real_escape_string(trim($this->input->post('patTypeID',true)));
					$takeStartDate 			= 	mysql_real_escape_string(trim($this->input->post('takeStartDate',true)));
					$takeEndDate 	= 	mysql_real_escape_string(trim($this->input->post('takeEndDate',true)));
					$rightHosID = $this->input->post('rightHosID');
					$welfarePricePay = $this->input->post('welfarePricePay');
					$welfarePriceRequest = $this->input->post('welfarePriceRequest');
					/*if(!empty($takeHosID)){
						$arr_hos = array( 'hosTypeID'		=> $hosTypeID,
										'patTypeID'		=> $patTypeID,
										'feeID'			=> $feeID,
										'welfarePrice'	=> $welfarePrice);
					}else{
						$arr_hos = array( 	'takeID' 		=> $lasted_id,
										'hosTypeID'		=> $hosTypeID,
										'patTypeID'		=> $patTypeID,
										'feeID'			=> $feeID,
										'welfarePrice'	=> $welfarePrice);
					}*/
					$arr_price = array();
					for($i=0; $i<count($rightHosID); $i++){
						
							$arr_price[] = array('id'=>$rightHosID[$i],'pay'=>$welfarePricePay[$i],'request'=>$welfarePriceRequest[$i]);
							
							
					}

					if(empty($takeID)){
						$arr_hos = array( 	'takeID' 		=> $lasted_id,
										'hosID'		=> $hosID,
										'diseaseName'		=> $diseaseName,
										'patTypeID'		=> $patTypeID,
										'takeStartDate'			=> toCEDate($takeStartDate),
										'takeEndDate'	=> toCEDate($takeEndDate),
										'takePrice'	=> json_encode($arr_price));
						$this->welfare_take_hospital_model->save($arr_hos);
					}else{
						if($welTypeIDO != $welfareType){
							$arr_hos = array( 	'takeID' 		=> $takeID,
										'hosID'		=> $hosID,
										'diseaseName'		=> $diseaseName,
										'patTypeID'		=> $patTypeID,
										'takeStartDate'			=> toCEDate($takeStartDate),
										'takeEndDate'	=> toCEDate($takeEndDate),
										'takePrice'	=> json_encode($arr_price));
							$this->welfare_take_hospital_model->save($arr_hos);
						}
						
					}
					
					
					
					break;
				// กองทุนสำรองเลี้ยงชีพ //
				case  '6' :
					$takepFundID = mysql_real_escape_string(trim($this->input->post('takepFundID',true)));
					$fundType 		= 	mysql_real_escape_string(trim($this->input->post('fundType',true)));
					$fundRateC 		= 	mysql_real_escape_string(trim($this->input->post('fundRateC',true)));
					$fundRateG 		= 	mysql_real_escape_string(trim($this->input->post('fundRateG',true)));
					$fundStartYear = 	mysql_real_escape_string(trim($this->input->post('fundStartYear',true)));
					$fundEndYear = 	mysql_real_escape_string(trim($this->input->post('fundEndYear',true)));
					
					$fundRate = ($fundType == "C") ? $fundRateC : $fundRateG;
					/*if(!empty($takepFundID)){
						$arr_pfund = array( 
										'fundType'		=> $fundType,
										'fundRate'		=> $fundRate,
										'fundStartYear'	=> $fundStartYear,
										'fundEndYear'	=> $fundEndYear);
					}else{
						$arr_pfund = array( 'takeID' 		=> $lasted_id,
										'fundType'		=> $fundType,
										'fundRate'		=> $fundRate,
										'fundStartYear'	=> $fundStartYear,
										'fundEndYear'	=> $fundEndYear);
					}*/

					if(empty($takeID)){
						$arr_pfund = array( 'takeID' 		=> $lasted_id,
										'fundType'		=> $fundType,
										'fundRate'		=> $fundRate,
										'fundStartYear'	=> $fundStartYear,
										'fundEndYear'	=> $fundEndYear);
						$this->welfare_take_pfund_model->save($arr_pfund,$takepFundID);
						
					}else{
						if($welTypeIDO != $welfareType){
							$arr_pfund = array( 'takeID' 		=> $takeID,
										'fundType'		=> $fundType,
										'fundRate'		=> $fundRate,
										'fundStartYear'	=> $fundStartYear,
										'fundEndYear'	=> $fundEndYear);
							$this->welfare_take_pfund_model->save($arr_pfund,$takepFundID);
						}
						
					}
					
					break;
			}

			if ($this->db->trans_status() == FALSE){
				$this->db->trans_rollback();  
				redirect('staffwelfaretake/welfare/?token='.$staffID,'refresh');
			}else{
				$this->db->trans_commit();
				redirect('staffwelfaretake/welfare/?token='.$staffID,'refresh');
			} 
            //if($this->save($arr,$rightID))
            //    redirect('staffwelfaretake/welfare/?token='.$staffID,'refresh');

   
        }
    }

    public function view(){
        if(!empty($_GET['token'])){

            $takeID = mysql_real_escape_string(trim($this->input->get('token',true)));
            $data['staffID'] = mysql_real_escape_string(trim($this->input->get('staff',true)));
            $data['r'] = $this->staff_welfare_take_model->get_by(array('takeID'=>$takeID),true);
            
            $data['rp'] =  $this->welfare_take_image_model->get_by(array('takeID'=>$takeID));

            $data['title'] = "เบิกสวัสดิการ";
            
            $data['takeID'] = $takeID;
            $this->template->load("template/admin2",'form', $data);
        }   
    }

    public function edit(){
        if(!empty($_GET['token'])){

            $takeID = mysql_real_escape_string(trim($this->input->get('token',true)));
            $data['staffID'] = mysql_real_escape_string(trim($this->input->get('staff',true)));
            $data['r'] = $this->staff_welfare_take_model->get_by(array('takeID'=>$takeID),true);
            
            $data['rp'] =  $this->welfare_take_image_model->get_by(array('takeID'=>$takeID));

            $data['title'] = "เบิกสวัสดิการ";
            
            $data['takeID'] = $takeID;
            $this->template->load("template/admin2",'form', $data);
        }   
    }
     

     function deleteFile(){
            if(!empty($_POST['fileID'])){
                
               $takeID = htmlspecialchars($this->input->post('takeID',true));
               $fileID = htmlspecialchars($this->input->post('fileID',true));
                
                $r = $this->welfare_take_image_model->get_by(array('takeID'=>$takeID,'takeFile'=>$fileID),true);
				
                $this->welfare_take_image_model->delete_by(array('takeID'=>$takeID,'takeFile'=>$fileID));
                
				if(!empty($r)){

					//$list = explode('.',$r->takeFile);
					@unlink('./assets/welfare/'.$r->takeFile);
					//@unlink('./assets/welfare/'.$list[0].'_thumb.'.$list[1]);
					 echo json_encode(array('flag'=>TRUE));
                    
                }else 
                   echo json_encode(array('flag'=>FALSE));  
                
            }else
                echo json_encode(array('flag'=>FALSE)); 
            
        }

        function listFile(){
        	if(!empty($_POST['takeID'])){
        		$takeID = htmlspecialchars($this->input->post('takeID',true));
        		$rp =  $this->welfare_take_image_model->get_by(array('takeID'=>$takeID));
        		$string = "";
        		if(!empty($rp)){
                    foreach($rp as $rowp){ 
                        $string .= '<div class="form-group">';
                        $string .= '<label class="control-label col-sm-3">ชื่อไฟล์:</label>';
                        $string .='<div class="col-sm-9">'.$rowp->takeFile;
                        $string .='<img src="'.base_url().'assets/images/delete_file.png"  name="del_file" id="del_file" onclick="javascript:deleteFile('.$takeID.',\''.$row->takeFile.'\')"> ';                                           
                        $string .='</div>';
                     	$string .=' </div>';
                    }
               }

               echo $string;

        	}
        }

        function deleteTake(){
        	if(!empty($_POST['takeID'])){
        		$takeID = htmlspecialchars($this->input->post('takeID',true));
        		$r = $this->staff_welfare_take_model->get_by(array('takeID'=>$takeID),true);
        		$welTypeID = (!empty($r)) ? $r->welTypeID : "";

        		////////start transaction///////////
            	$this->db->trans_begin();

        		 switch($welTypeID){
					// ค่าช่วยเหลือบุตร //
					case  '1' : $this->welfare_take_child_model->delete_by(array('takeID'=>$takeID)); break;
					// ค่าการศึกษาบุตร //			
					case  '2' : $this->welfare_take_edu_model->delete_by(array('takeID'=>$takeID)); break;
					// ค่านมสวัสดิการ //
					case  '3' : $this->welfare_take_milk_model->delete_by(array('takeID'=>$takeID)); break;
					// ค่ารักษาพยาบาล //
					case  '4' : $this->welfare_take_hospital_model->delete_by(array('takeID'=>$takeID)); break;
					// กองทุนสำรองเลี้ยงชีพ //
					case  '6' : $this->welfare_take_pfund_model->delete_by(array('takeID'=>$takeID)); break;

				}

				$ri = $this->welfare_take_image_model->get_by(array('takeID'=>$takeID));
				if(!empty($ri)){
					foreach($ri as $rowi){
						@unlink('./assets/welfare/'.$rowi->takeFile);
					}
				}

				$this->welfare_take_image_model->delete_by(array('takeID'=>$takeID));
				$this->staff_welfare_take_model->delete($takeID);



				if ($this->db->trans_status() == FALSE){
					$this->db->trans_rollback();  
					echo json_encode(array('flag'=>false));
				}else{
					$this->db->trans_commit();
					echo json_encode(array('flag'=>true));
				} 

        	}else
        		echo json_encode(array('flag'=>false));
        	
        }

   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>