<style>
    .error_lbl{
        border-color:red !important;
    }
        .ui-dialog-titlebar-close{
            display:none
        }
        .header-product{
            background-color: pink;
            font-weight: bold;
        }
        .header-product th{
            text-align: center;
            vertical-align: top;
        }
        .btn-none{
            display:none;
        }
</style>

<div class="col-md-12" ng-controller="welfaretakeCtrl" data-ng-init="init()">

     <div class="col-separator box col-separator-first col-unscrollable">
        <div class="col-table">
                   <div class="row">
                      <div class="col-sm-12">
                          <section class="panel">
                              <header class="panel-heading">
                                 <h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?></h3>

                              </header>
                              <div class="panel-body">
                              <form class="form-horizontal " id="welfaretakeForm" name="welfaretakeForm" method="post" action="<?php echo site_url(); ?>staffwelfaretake/submit" enctype="multipart/form-data">
                            
                                <input type="hidden" name="takeID" id="takeID" value="<?php echo !empty($takeID) ? $takeID : ""; ?>">
                                <input type="hidden" name="staffID" id="staffID" value="<?php echo !empty($staffID) ? $staffID : ""; ?>">
                                <input type="hidden" name="welTypeIDO" value="<?php echo !empty($r) ? $r->welTypeID : ""; ?>">
                                <input type="hidden" name="mode" id="mode" value="<?php echo $this->uri->segment('2'); ?>">
                                <div class="xcrud">
                                  <div class="xcrud-container">
                                      <div class="xcrud-view">
                                        <div class="form-horizontal">
                                          
                                           <div class="form-group">
                                            <label class="control-label col-sm-3">วันที่เบิก</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="takeDate" value="<?php echo !empty($r) ? toBEDate($r->takeDate) : ""; ?>" data-type="text" data-required="1" class="xcrud-input xcrud-datepicker form-control" data-type="date">
                                                <?php }else{
                                                echo toBEDateThai($r->takeDate);
                                              } ?>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">ชื่อผู้เบิก</label>
                                              <div class="col-sm-9">
                                              {{staffName}}
                                              </div>
                                          </div>
                                           <div class="form-group">
                                            <label class="control-label col-sm-3">ประเภทพนักงาน</label>
                                              <div class="col-sm-9">
                                              {{staffType}}
                                              </div>
                                          </div>
                                           <div class="form-group">
                                            <label class="control-label col-sm-3">ตำแหน่ง</label>
                                              <div class="col-sm-9">
                                              {{staffPosition}}
                                              </div>
                                          </div>
                                            <div class="form-group">
                                            <label class="control-label col-sm-3">เงินเดือน</label>
                                              <div class="col-sm-9">
                                              {{staffSalary}}
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-sm-3">เบิกให้</label>
                                              <div class="col-sm-9">
                                                 <?php if($this->uri->segment('2') != 'view'){ 
                                                        //echo getDropdown(array('1'=>'ตนเอง','2'=>'บิดา-มารดา','3'=>'คู่สมรส','4'=>'บุตร'),'rightPersonType',$r->rightPersonType,'class="form-control"');
                                                  ?>
                                                      <select 
                                                        ng-init="ddlperson = {id: ddl.person}"
                                                        ng-model="ddlperson"
                                                        ng-change="ddl.person = ddlperson.id; change_person() "
                                                        ng-options="value.name for value in persons track by value.id"
                                                        class="form-control"
                                                        name="takePersonType" id="takePersonType">
                                                        
                                                      </select>
                                                  <?php
                                                   }else{
                                                      
                                                        switch($r->takePersonType){
                                                          case '1' : echo 'ตนเอง'.'<br>'; break;
                                                          case '2' : echo 'บิดา-มารดา'.'<br>'; break;
                                                          case '3' : echo 'คู่สมรส'.'<br>'; break;
                                                          case '4' : echo 'บุตร'.'<br>'; break;
                                                        }
                                                      
                                                  } ?>                                              
                                            </div>
                                          </div>
                                          
                                          <div class="form-group">
                                              <label class="control-label col-sm-3">ชื่อ - สกุล</label>
                                              <div class="col-sm-9">
                                                 <div id="divperson"></div>                                             
                                            </div>
                                          </div>

          
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">ประเภทสวัสดิการ</label>
                                            <div class="col-sm-9">
                                               <?php //echo contractType_ddl("contractType",'','class="form-control" ng-change="change()"');?>  
                                               <?php if($this->uri->segment('2') != 'view'){ ?>                                           
                                                  <select 
                                                    ng-init="ddlwelfare = {id: ddl.type}"
                                                    ng-model="ddlwelfare"
                                                    ng-change="ddl.type = ddlwelfare.welTypeID; change() "
                                                    ng-options="value.welTypeName for value in myOptions track by value.welTypeID"
                                                    class="form-control"
                                                    name="welfareType" id="welfareType">
                                                    
                                                  </select>
                                                 <?php }else{
                                                  echo getWelfareTypeName($r->welTypeID);
                                                } ?>  

                                                  
                                            </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-sm-3">รูปแบบ</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              {{welfarefix}}
                                              <?php }else{
                                                    echo getFixName($r->welTypeID);
                                              } ?>                                               
                                            </div>
                                          </div>
                                         
                                          
                                         <div id="formDetail"></div>
                                           
                                          <?php if($this->uri->segment('2') != 'view'){ ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">เอกสารแนบ</label>
                                            <div class="col-sm-9">
                                               <input type="file" id="takefile" name="takefile[]" multiple="multiple"/>                                             
                                          </div>
                                          
                                        </div>
                                        <?php } ?>
                                          <div id="listfile">
                                        <?php 
                                           if(!empty($rp)){
                                            foreach($rp as $rowp){ ?>
                                                <div class="form-group">
                                                  <label class="control-label col-sm-3">ชื่อไฟล์:</label>
                                                  <div class="col-sm-9">
                                                    <?php if($this->uri->segment('2') != 'view'){ ?>
                                                    <?php echo $rowp->takeFile; ?><img src='<?php echo base_url(); ?>assets/images/delete_file.png'  name='del_file' id='del_file' onclick='javascript:deleteFile("<?php echo $takeID; ?>","<?php echo $rowp->takeFile; ?>")'>                                            
                                                    <?php }else{ echo $rowp->takeFile; }?>

                                                </div>
                                              </div>

                                          <?php  }
                                           }
                                           
                                          ?>
                                          </div>

                                      </div>


                                  </div>
                                  <div class="xcrud-top-actions btn-group">
                                      <?php if($this->uri->segment('2') != 'view'){ ?>
                                      <a class="btn btn-primary"  href="javascript:submitForm();">บันทึกและย้อนกลับ</a>
                                      <?php } ?>
                                      <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>staffwelfaretake/welfare/?token=<?php echo !empty($staffID) ? $staffID : ""; ?>'">ย้อนกลับ</a>
                                  </div>
                                  </form>
                                  
                              </div>
                          </section>
                      </div>
                  </div>
            
             
                
        </div>  
      </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css">
<script src="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url(); ?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script>
<script>
  $(function(){
      $('.xcrud-datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            changeMonth: true,
            changeYear: true
          });

      

  })

  function submitForm(){
    $('#welfaretakeForm').submit();
  }
  
</script>
<script>
        var myApp = angular.module("baseApp",[]);


        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
             
          }
        }]);

        myApp.controller('welfaretakeCtrl', ['$scope', '$location','$http', function($scope, $location,$http) {

         $scope.persons = [
            {id:'1', name:'ตนเอง'},
            {id:'2', name:'บิดา-มารดา'},
            {id:'3', name:'คู่สมรส'},
            {id:'4', name:'บุตร'}
          ];

          $scope.init = function () {
              
              if("<?php echo $this->uri->segment('2') ?>" == "view"){

                  $http.get("<?php echo site_url()?>welfaretype/getFormTake", { params: { "id": '<?php echo (!empty($r)) ? $r->welTypeID : ""?>', "welfare_take" : '<?php echo (!empty($takeID)) ? $takeID : ""?>',"mode" : "view","personType":'<?php echo (!empty($r)) ? $r->takePersonType : ""?>',"personID":'<?php echo (!empty($r)) ? $r->personID : ""?>' } }).success(function (data, status, header, config) {
                        $('#formDetail').html(data);
                       
                    }).error(function (data, status, headers, config) {
                        
                    });

                  $http.get("<?php echo site_url()?>staffperson/getStaffPerson", { params: { "id": '<?php echo (!empty($r)) ? $r->takePersonType : ""?>',"personID": '<?php echo (!empty($r)) ? $r->personID : ""?>' ,"staffID":$('#staffID').val(),"mode":"view"} }).success(function (data, status, header, config) {
                      $('#divperson').html(data);
                     
                  }).error(function (data, status, headers, config) {
                      
                  });
                
              }else{

                  $http.get("<?php echo site_url()?>welfaretype/getJsonData").success(function (data, status, header, config) {
                    $scope.myOptions = data;
                    $scope.ddlwelfare.welTypeID = '<?php echo (!empty($r)) ? $r->welTypeID : ""?>';
                    $scope.ddlperson.id = '<?php echo (!empty($r)) ? $r->takePersonType : ""?>';

                    if($scope.ddlwelfare.welTypeID != ""){
                       $http.get("<?php echo site_url()?>welfaretype/getWelfareFix", { params: { "id": $scope.ddlwelfare.welTypeID } }).success(function (data, status, header, config) {
                          $scope.welfarefix = data;
                         
                      }).error(function (data, status, headers, config) {
                          
                      });


                       $http.get("<?php echo site_url()?>welfaretype/getFormTake", { params: { "id": $scope.ddlwelfare.welTypeID, "welfare_take" : '<?php echo (!empty($takeID)) ? $takeID : ""?>',"mode" : "edit" ,"personType":'<?php echo (!empty($r)) ? $r->takePersonType : ""?>',"personID":'<?php echo (!empty($r)) ? $r->personID : ""?>'} }).success(function (data, status, header, config) {
                          $('#formDetail').html(data);
                           $('.xcrud-datepicker').datepicker({
                            dateFormat: 'dd/mm/yy',
                            firstDay: 1,
                            changeMonth: true,
                            changeYear: true
                          });

                          
                         
                      }).error(function (data, status, headers, config) {
                          
                      });

                      if($scope.ddlperson.id != ""){
                          $http.get("<?php echo site_url()?>staffperson/getStaffPerson", { params: { "id": $scope.ddlperson.id ,"staffID":$('#staffID').val(),"personID":'<?php echo (!empty($r)) ? $r->personID : ""?>'} }).success(function (data, status, header, config) {
                            $('#divperson').html(data);
                           
                         }).error(function (data, status, headers, config) {
                            
                         });
                      }


                      
                    }

                    

                    

                   
                }).error(function (data, status, headers, config) {
                    
                });

              }


             $.ajax({
                url: "<?php echo site_url()?>staffwork/getStaffDetail",
                type:"GET",
                data : { id:$('#staffID').val() },
                async:false,
                dataType:"json",
                
                success:function(data){
                  
                  $scope.staffName = data.staffName;
                  $scope.staffType = data.staffType;
                  $scope.staffPosition = data.staffPosition;
                  $scope.staffSalary = data.staffSalary;
                  
                }
              });

          };

          
           
          $scope.change = function(){
              /*var fruitName = $.grep($scope.Fruits, function (fruit) {
                      return fruit.Id == fruitId;
                  })[0].Name;*/

              $http.get("<?php echo site_url()?>welfaretype/getWelfareFix", { params: { "id": $scope.ddlwelfare.welTypeID } }).success(function (data, status, header, config) {
                  $scope.welfarefix = data;
                 
              }).error(function (data, status, headers, config) {
                  
              });

              $http.get("<?php echo site_url()?>welfaretype/getFormTake", { params: { "id": $scope.ddlwelfare.welTypeID, "welfare_take" :'<?php echo (!empty($takeID)) ? $takeID : ""?>',"mode" : $('#mode').val(),"personType":$('#takePersonType').val(),"personID":$('#personID').val()} }).success(function (data, status, header, config) {
                  $('#formDetail').html(data);
                  $('.xcrud-datepicker').datepicker({
                            dateFormat: 'dd/mm/yy',
                            firstDay: 1,
                            changeMonth: true,
                            changeYear: true
                          });
                   

              }).error(function (data, status, headers, config) {
                  
              });

              

          }


          $scope.change_person = function(){
             
              $http.get("<?php echo site_url()?>staffperson/getStaffPerson", { params: { "id": $scope.ddlperson.id ,"staffID":$('#staffID').val()} }).success(function (data, status, header, config) {
                  $('#divperson').html(data);
                 
              }).error(function (data, status, headers, config) {
                  
              });

              

          }


        }]);

    function deleteFile(takeID,fileID){
    
        $.ajax({
            url: "<?php echo site_url(); ?>staffwelfaretake/deleteFile",
            type:"POST",
            data:{takeID:takeID,
                  fileID:fileID
            },
            async:false,
            dataType:"json",
            success:function(data){
                if(data.flag===true){
                   
                    loadFileData();
          
                }
            }
       });
    }

    function loadFileData()
    {   
        try{
            var responseData = $.ajax({    
                url: "<?php echo site_url(); ?>staffwelfaretake/listFile",
                data : {takeID:$('#takeID').val()},
                type : 'POST',
                async:false
            }).responseText ;
             
            $('#listfile').html(responseData);
        }catch(e){}     

    } 

</script>
