<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php echo $staffName; ?></small></h3>
                <?php if(!isset($_GET['back'])){ ?>
                 <a href="<?php echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php echo $this->config->item('txt_back')?></a>
                 <?php } ?>
                 <br><br>
            </header>

            <div class="panel-body">

              <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body profile-information">
                      <form class="form-horizontal " id="staffprintForm" name="staffprintForm" target="_blank" method="post" action="<?php echo site_url(); ?>staffprint/print_pdf" >
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                       <div class="col-md-5">
                           <div class="profile-desk">
                               <div class="form-horizontal">
                                <input type="checkbox" name="chk_info" id="chk_info" checked> ข้อมูลส่วนตัว<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="chk_name" id="chk_name" checked> ชื่อ - สกุล<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_bday" id="chk_bday" checked> วันเกิด<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_cname" id="chk_cname"> ประวัติการเปลี่ยนชื่อ - สกุล<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_address" id="chk_address"> ที่อยู่ปัจจุบัน<br/>

                                 <input type="checkbox" name="chk_work" id="chk_work" checked> ประวัติการทำงาน<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_position" id="chk_position" checked> ตำแหน่ง<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_eday" id="chk_eday" checked> วันที่เกษียณอายุ<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_status" id="chk_status" checked> สถานภาพการทำงาน<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_dwork" id="chk_dwork" checked> รายละเอียดการทำงาน<br/>
                                
                                <input type="checkbox" name="chk_edu" id="chk_edu" checked> การศึกษา<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_edu1" id="chk_edu1" checked> แรกเข้า<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_edu2" name="chk_edu2" checked> จบจริง<br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;  <input type="checkbox" name="chk_edu3" name="chk_edu3"> ปัจจุบัน<br/>
                                
                                 <input type="checkbox" name="chk_insignia" id="chk_insignia" checked> เครื่องราช<br/>
                                 <input type="checkbox" name="chk_mistake" id="chk_mistake"> ประวัติความผิด / โทษ<br/>

                               </div>
                           </div>
                       </div>
                        <div class="col-md-5">
                           <div class="profile-desk">
                               <div class="form-horizontal">

                                 <input type="checkbox" name="chk_seminar" id="chk_seminar"> อบรม / สัมมนา<br/>
                                 <input type="checkbox" name="chk_feat" id="chk_feat"> ความดีความชอบ<br/>
                                 <input type="checkbox" name="chk_contract" id="chk_contract"> สัญญาจ้าง<br/> 
                                 <input type="checkbox" name="chk_person" id="chk_person"> คู่สมรส / บุตร / บิดามารดา<br/>
                                  <input type="checkbox" name="chk_contact" id="chk_contact"> ผู้ติดต่อฉุกเฉิน<br/>
                                 <input type="checkbox" name="chk_note" id="chk_note"> Note<br/>
                               </div>
                           </div>
                       </div>
                    </div>
                </section>
            </div>
                <div id="staffInfo">
                    <h5 style="text-align:center;">ทะเบียนข้อมูลพนักงานระดับ <?php echo getRankByWork($id); ?> </h5>
                    <div class="xcrud-list-container">
                      <table class="xcrud-list table table-striped table-hover table-bordered">
                      
                        <tr class="xcrud-th">
                          <thead>
                          <th width="25%" class="staffinfo">ชื่อ - นามสกุล<br>
                              ตำแหน่ง ......<br>
                              สังกัด แผนก / สำนักงาน / ฝ่าย
                          </th>
                          <th width="15%" class="staffinsignia">ปีที่ได้รับเครื่องราช ฯ และ<br>ชนิดของเครื่องราช</th>
                          <th width="15%" class="staffedu">คุณวุฒิ<br>แรกเข้า / จบจริง <br> สถาบันการศึกษา</th>
                          <th width="35%" class="staffwork">ประวัติการทำงาน</th>
                          <th width="10%" class="staffnote" style="display:none">หมายเหตุ</th>
                          </thead>
                        </tr>
                        
                        <?php
                        if($rs->num_rows() > 0){ ?>
                          <tr>
                            <td class="staffinfo">
                              <!-- staff -->
                              <table>
                                <tr id="staffname"><td><?php echo $rs->row()->staffPreName.' '.$rs->row()->staffFName.' '.$rs->row()->staffLName;?></td></tr>
                                <tr id="staffposition"><td>ตำแหน่ง <?php echo getPositionByWork($id);?></td></tr>
                                <tr><td>สังกัด   
                                <?php $org2 = getOrg2ByWork($id);
                                $org1 = getOrg1ByWork($id);
                                $org2 = (!empty($org2)) ? ' / '.$org2 : "";
                                $org1 = (!empty($org1)) ? ' / '.$org1 : "";
                                echo getOrgByWork($id).$org2.$org1; ?></td></tr>
                                <tr><td> <img alt="" src="<?php echo site_url();?>uploads/<?php echo ($rs->num_rows() > 0) ? $rs->row()->staffImage : ""; ?>"></td></tr>
                                <tr id="staffcname" style="display:none">
                                  <td>
                                     <table>
                                    <?php if($rcn->num_rows() > 0){ 
                                            foreach($rcn->result() as $rowcn){
                                      ?>
                                      <tr><td><?php echo toFullDate($rowcn->dateChange,'th'); ?></td><td><?php echo $rowcn->staffPreName.' '.$rowcn->staffFName.' '.$rowcn->staffLName;?></td></tr>
                                    <?php }} ?>
                                    
                                  </table>
                                  </td>
                                </tr>
                                <tr id="staffaddress" style="display:none"><td><?php echo $rs->row()->staffAddressNow; ?></td></tr>
                              </table>

                            </td>
                            <td class="staffinsignia">
                              <!-- insignia -->
                              
                               <table>
                                <?php if($ri->num_rows() > 0){ 
                                        foreach($ri->result() as $rowi){
                                  ?>
                                  <tr><td>พ.ศ. <?php echo ($rowi->insigniaYear+543).' '.$rowi->insigniaName.' ('.$rowi->insigniaAlias.')'; ?> </td></tr>
                                <?php }} ?>
                                
                              </table>
                              
                            </td>
                            <td class="staffedu">
                              <!-- education -->
                               <table class="staffedu1">
                                <?php if($re->num_rows() > 0){ 
                                  ?>
                                  <tr><td><?php echo $re->row()->eduDegreeName?></td></tr>
                                  <tr><td><?php echo $re->row()->eduDepartmentName?></td></tr>
                                  <tr><td><?php echo $re->row()->eduPlaceName?></td></tr>
                                <?php } ?>
                                <tr><td></td></tr>
                                <tr><td></td></tr>
                                </table>
                                <table class="staffedu2">
                                <?php if($re2->num_rows() > 0){ 
                                  ?>
                                  <tr><td><?php echo $re2->row()->eduDegreeName?></td></tr>
                                  <tr><td><?php echo $re2->row()->eduDepartmentName?></td></tr>
                                  <tr><td><?php echo $re2->row()->eduPlaceName?></td></tr>
                                <?php } ?>
                                <tr><td></td></tr>
                                <tr><td></td></tr>
                              </table>
                              <table class="staffedu3" style="display:none">
                                <?php if($re3->num_rows() > 0){ 
                                  ?>
                                  <tr><td><?php echo $re3->row()->eduDegreeName?></td></tr>
                                  <tr><td><?php echo $re3->row()->eduDepartmentName?></td></tr>
                                  <tr><td><?php echo $re3->row()->eduPlaceName?></td></tr>
                                <?php } ?>
                              </table>

                            </td>
                            <td class="staffwork">
                              <!-- work -->
                               <table>
                                  <tr><td id="staffbday">เกิดวันที่ <?php echo toFullDate($rs->row()->staffBirthday,'th'); ?></td><td id="staffeday">วันเกษียณอายุ <?php echo getExpiredDateByWork($rs->row()->staffBirthday); ?></td></tr>
                                   <tr id="staffstatus"><td>สถานภาพการทำงาน</td><td><?php echo getWorkStatusByWork($id); ?></td></tr>
                                   <?php if($rw->num_rows() > 0){ 
                                        $j=0;
                                        foreach($rw->result() as $roww){
                                         
                                          $date = (($j+1) == $rw->num_rows()) ? toFullDate($roww->workStartDate,'th').' - ปัจจุบัน' : toFullDate($roww->workStartDate,'th');
                                  ?>
                                  <tr class="staffdwork"><td><?php echo $date; ?></td><td><?php echo $roww->workDetail; ?></td></tr>

                                <?php $j++; }} ?>
                               </table>
                            </td>
                            <td class="staffnote" style="display:none">
                              <!-- note -->
                              <table>
                                   <?php if($rn->num_rows() > 0){ 
                                        $j=0;
                                        foreach($rn->result() as $rown){ ?>
                                         <tr><td><?php echo $rown->noteDesc; ?></td></tr>
                                     <?php   }
                                    }?>
                                </table>
                            </td>
                          </tr>
                          
                            
                        <?php } ?>
          
                      </table>

                       

                    </div>
                </div>

                <div id="staffmistake" style="display:none">
                   <h5 style="text-align:center;">ประวัติความผิด / โทษ</h5>
                   <?php echo $htmlmistake; ?>
                </div>

                <div id="staffseminar" style="display:none">
                   <h5 style="text-align:center;">อบรม / สัมมนา</h5>
                   <?php echo $htmlseminar; ?>
                </div>

                <div id="stafffeat" style="display:none">
                   <h5 style="text-align:center;">ความดีความชอบ</h5>
                   <?php echo $htmlfeat; ?>
                </div>

                <div id="staffcontract" style="display:none">
                   <h5 style="text-align:center;">สัญญาจ้าง</h5>
                   <?php echo $htmlcontract; ?>
                </div>

                <div id="staffperson" style="display:none">
                   <h5 style="text-align:center;">คู่สมรส / บุตร / บิดา-มารดา</h5>
                   <?php echo $htmlperson; ?>
                </div>

                <div id="staffcontact" style="display:none">
                   <h5 style="text-align:center;">ผู้ติดต่อฉุกเฉิน</h5>
                   <?php echo $htmlcontact; ?>
                </div>

                <div class="btn-group col-sm-2 col-sm-offset-5">                         
                    <a class="btn btn-primary"  href="#" onclick="submitForm()">พิมพ์</a>
                 </div>
                 </form>
            </div>
        </section>
    </div>
</div>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

         $('#chk_info').click(function() {
            $(".staffinfo").toggle(this.checked);
        });

         $('#chk_name').click(function() {
            $("#staffname").toggle(this.checked);
        });

         $('#chk_address').click(function() {
            $("#staffaddress").toggle(this.checked);
        });

         $('#chk_cname').click(function() {
            $("#staffcname").toggle(this.checked);
        });

         $('#chk_bday').click(function() {
            $("#staffbday").toggle(this.checked);
        });

         $('#chk_eday').click(function() {
            $("#staffeday").toggle(this.checked);
        });

         $('#chk_position').click(function() {
            $("#staffposition").toggle(this.checked);
        });

          $('#chk_status').click(function() {
            $("#staffstatus").toggle(this.checked);
        });

         $('#chk_dwork').click(function() {
            $(".staffdwork").toggle(this.checked);
        });

        $('#chk_insignia').click(function() {
            $(".staffinsignia").toggle(this.checked);
        });

        $('#chk_edu').click(function() {
            $(".staffedu").toggle(this.checked);
        });

         $('#chk_edu1').click(function() {
            $(".staffedu1").toggle(this.checked);
        });

         $('#chk_edu2').click(function() {
            $(".staffedu2").toggle(this.checked);
        });

         $('#chk_edu3').click(function() {
            $(".staffedu3").toggle(this.checked);
        });

        $('#chk_work').click(function() {
            $(".staffwork").toggle(this.checked);
        });

        $('#chk_mistake').click(function() {
            $("#staffmistake").toggle(this.checked);
        });
       
        $('#chk_seminar').click(function() {
            $("#staffseminar").toggle(this.checked);
        });

        $('#chk_feat').click(function() {
            $("#stafffeat").toggle(this.checked);
        });

        $('#chk_contract').click(function() {
            $("#staffcontract").toggle(this.checked);
        });

        $('#chk_person').click(function() {
            $("#staffperson").toggle(this.checked);
        });

         $('#chk_contact').click(function() {
            $("#staffcontact").toggle(this.checked);
        });

         $('#chk_note').click(function() {
            $(".staffnote").toggle(this.checked);
        });

        function submitForm(){
          $('#staffprintForm').submit();
        }
</script>