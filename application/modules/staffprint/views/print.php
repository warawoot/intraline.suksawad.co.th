<div style="text-align:right;"><?php echo toFullDate(date("Y-m-d")).' '.date("H:i:s"); ?></div>
<div id="staffInfo">
    <div style="text-align:center;"><img style="width: 105px; height: 105px;" src="/uploads/images/dpo-logo%281%29.jpeg" alt="http://dpo-ehr.smmms.com/uploads/images/logodpo.png"></div>

    <h4 style="text-align:center;">ทะเบียนข้อมูลพนักงานระดับ <?php echo getRankByWork($id); ?> </h4>
    <div class="xcrud-list-container">
      <table style="border: 1px solid #ddd;">
      
        <tr class="xcrud-th">
          <thead>
          <?php if($chk_info == "1"){?>
          <th width="25%" class="staffinfo" style="background: #efefef;">ชื่อ - นามสกุล<br>
              ตำแหน่ง ......<br>
              สังกัด แผนก / สำนักงาน / ฝ่าย
          </th>
          <?php } ?>
           <?php if($chk_insignia == "1"){?>
          <th width="15%" class="staffinsignia" style="background: #efefef;">ปีที่ได้รับเครื่องราช ฯ และ<br>ชนิดของเครื่องราช</th>
           <?php } ?>
          <?php if($chk_edu == "1"){?>
          <th width="15%" class="staffedu" style="background: #efefef;">คุณวุฒิ<br>แรกเข้า / จบจริง <br> สถาบันการศึกษา</th>
          <?php } ?>
          <?php if($chk_work == "1"){?>
          <th width="35%" class="staffwork" style="background: #efefef;">ประวัติการทำงาน</th>
          <?php } ?>
          <?php if($chk_note == "1"){?>
          <th width="10%" class="staffnote" style="background: #efefef;">หมายเหตุ</th>
          <?php } ?>
          </thead>
        </tr>
        
        <?php
        if($rs->num_rows() > 0){ ?>
          <tr>
             <?php if($chk_info == "1"){?>
            <td class="staffinfo">
              <!-- staff -->
              <table>
                <?php if($chk_name == "1"){?>
                <tr id="staffname"><td><?php echo $rs->row()->staffPreName.' '.$rs->row()->staffFName.' '.$rs->row()->staffLName;?></td></tr>
                 <?php } ?>
                <?php if($chk_position == "1"){?>
                <tr id="staffposition"><td>ตำแหน่ง <?php echo getPositionByWork($id);?></td></tr>
                <?php } ?>
                <tr><td>สังกัด   
                <?php $org2 = getOrg2ByWork($id);
                $org1 = getOrg1ByWork($id);
                $org2 = (!empty($org2)) ? ' / '.$org2 : "";
                $org1 = (!empty($org1)) ? ' / '.$org1 : "";
                echo getOrgByWork($id).$org2.$org1; ?></td></tr>
                <tr><td> <img alt="" src="<?php echo site_url();?>uploads/<?php echo ($rs->num_rows() > 0) ? $rs->row()->staffImage : ""; ?>"></td></tr>
                 <?php if($chk_cname == "1"){?>
                <tr id="staffcname">
                  <td>
                     <table>
                    <?php if($rcn->num_rows() > 0){ 
                            foreach($rcn->result() as $rowcn){
                      ?>
                      <tr><td><?php echo toFullDate($rowcn->dateChange,'th'); ?></td><td><?php echo $rowcn->staffPreName.' '.$rowcn->staffFName.' '.$rowcn->staffLName;?></td></tr>
                    <?php }} ?>
                    
                  </table>
                  </td>
                </tr>
                <?php } ?>
                <?php if($chk_address == "1"){?>
                <tr id="staffaddress"><td><?php echo $rs->row()->staffAddressNow; ?></td></tr>
                 <?php } ?>
              </table>

            </td>
            <?php } ?>
            <?php if($chk_insignia == "1"){?>
            <td class="staffinsignia">
              <!-- insignia -->
              
               <table>
                <?php if($ri->num_rows() > 0){ 
                        foreach($ri->result() as $rowi){
                  ?>
                  <tr><td>พ.ศ. <?php echo ($rowi->insigniaYear+543).' '.$rowi->insigniaName.' ('.$rowi->insigniaAlias.')'; ?> </td></tr>
                <?php }} ?>
                
              </table>
              
            </td>
            <?php } ?>
            <?php if($chk_edu == "1"){?>
            <td class="staffedu">
              <!-- education -->
              <?php if($chk_edu1 == "1"){ ?>
               <table class="staffedu1">
                <?php if($re->num_rows() > 0){  ?>
                  ?>
                  <tr><td><?php echo $re->row()->eduDegreeName?></td></tr>
                  <tr><td><?php echo $re->row()->eduDepartmentName?></td></tr>
                  <tr><td><?php echo $re->row()->eduPlaceName?></td></tr>
                <?php } ?>
                <!--<tr><td></td></tr>
                <tr><td></td></tr>-->
                </table>
                 <?php } ?>
                 <?php if($chk_edu2 == "1"){?>
                <table class="staffedu2">
                <?php if($re2->num_rows() > 0){ 
                  ?>
                  <tr><td><?php echo $re2->row()->eduDegreeName?></td></tr>
                  <tr><td><?php echo $re2->row()->eduDepartmentName?></td></tr>
                  <tr><td><?php echo $re2->row()->eduPlaceName?></td></tr>
                <?php } ?>
                <!--<tr><td></td></tr>
                <tr><td></td></tr>-->
              </table>
               <?php } ?>
               <?php if($chk_edu3 == "1"){?>
              <table class="staffedu3">
                <?php if($re3->num_rows() > 0){ 
                  ?>
                  <tr><td><?php echo $re3->row()->eduDegreeName?></td></tr>
                  <tr><td><?php echo $re3->row()->eduDepartmentName?></td></tr>
                  <tr><td><?php echo $re3->row()->eduPlaceName?></td></tr>
                <?php } ?>
              </table>
             <?php } ?>
            </td>
            <?php } ?>
            <?php if($chk_work == "1"){?>
            <td class="staffwork">
              <!-- work -->
               <table>

                  <tr><?php if($chk_bday == "1"){?><td id="staffbday">เกิดวันที่ <?php echo toFullDate($rs->row()->staffBirthday,'th'); ?></td> <?php } ?><?php if($chk_eday == "1"){?><td id="staffeday">วันเกษียณอายุ <?php echo getExpiredDateByWork($rs->row()->staffBirthday); ?></td><?php } ?></tr>
                   <?php if($chk_status == "1"){?>
                   <tr id="staffstatus"><td>สถานภาพการทำงาน</td><td><?php echo getWorkStatusByWork($id); ?></td></tr>
                    <?php } ?>
                   <?php if($rw->num_rows() > 0){ 
                        $j=0;
                        foreach($rw->result() as $roww){
                         
                          $date = (($j+1) == $rw->num_rows()) ? toFullDate($roww->workStartDate,'th').' - ปัจจุบัน' : toFullDate($roww->workStartDate,'th');
                  ?>
                   <?php if($chk_dwork == "1"){?>
                  <tr class="staffdwork"><td><?php echo $date; ?></td><td><?php echo $roww->workDetail; ?></td></tr>
                  <?php } ?>

                <?php $j++; }} ?>
               </table>
            </td>
            <?php } ?>
             <?php if($chk_note == "1"){?>
            <td class="staffnote">
              <!-- note -->
              <table>
                   <?php if($rn->num_rows() > 0){ 
                        $j=0;
                        foreach($rn->result() as $rown){ ?>
                         <tr><td><?php echo $rown->noteDesc; ?></td></tr>
                     <?php   }
                    }?>
                </table>
            </td>
            <?php } ?>
          </tr>
          
            
        <?php } ?>

      </table>

       

    </div>
</div>
 <?php if($chk_mistake == "1"){?>
<div id="staffmistake" >
   <h4 style="text-align:center;">ประวัติความผิด / โทษ</h4>
   <?php echo $htmlmistake; ?>
</div>
<?php } ?>
<?php if($chk_seminar == "1"){?>
<div id="staffseminar">
   <h4 style="text-align:center;">อบรม / สัมมนา</h4>
   <?php echo $htmlseminar; ?>
</div>
<?php } ?>
<?php if($chk_feat == "1"){?>
<div id="stafffeat">
   <h4 style="text-align:center;">ความดีความชอบ</h4>
   <?php echo $htmlfeat; ?>
</div>
<?php } ?>
<?php if($chk_contract == "1"){?>
<div id="staffcontract">
   <h4 style="text-align:center;">สัญญาจ้าง</h4>
   <?php echo $htmlcontract; ?>
</div>
<?php } ?>
<?php if($chk_person == "1"){?>
<div id="staffperson">
   <h4 style="text-align:center;">คู่สมรส / บุตร / บิดา-มารดา</h4>
   <?php echo $htmlperson; ?>
</div>
<?php } ?>
<?php if($chk_contact == "1"){?>
<div id="staffcontact">
   <h4 style="text-align:center;">ผู้ติดต่อฉุกเฉิน</h4>
   <?php echo $htmlcontact; ?>
</div>
<?php } ?>
