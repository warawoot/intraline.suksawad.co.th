<?php

class Staffprint extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		$this->setModel("staff_work_model");

		$this->load->model('orgchart_assigndate_model');
		
	}
	
	public function index(){

		Xcrud_config::$editor_url = base_url().'editors/ckeditor/ckeditor.js';


		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		$data['rs'] = $this->db->from('tbl_staff s')
							//->join('tbl_work_status w','s.workStatusID = w.workStatusID','left')
							->where('ID',$id)
							->get();			
		
		$data['ri'] = $this->db->from('tbl_staff_insignia s')
								->join('tbl_insignia i','s.insigniaType = i.insigniaID')
								->where('s.staffID',$id)
								->order_by('s.insigniaYear','ASC')
								->get();
								
		$data['re'] = $this->db->from('tbl_staff_graduation g')
								->join('tbl_edu_degree d','g.graduationDegree = d.eduDegreeID')
								->join('tbl_edu_department dp','g.graduationDepartment = dp.eduDepartmentID')
								->join('tbl_edu_place p','g.graduationUniversity = p.eduPlaceID')
								->where(array('staffID'=>$id,'graduationType'=>'1'))
								->get();
								
		$data['re2'] = $this->db->from('tbl_staff_graduation g')
								->join('tbl_edu_degree d','g.graduationDegree = d.eduDegreeID')
								->join('tbl_edu_department dp','g.graduationDepartment = dp.eduDepartmentID')
								->join('tbl_edu_place p','g.graduationUniversity = p.eduPlaceID')
								->where(array('staffID'=>$id,'graduationType'=>'2'))
								->get();

		$data['re3'] = $this->db->from('tbl_staff_graduation g')
								->join('tbl_edu_degree d','g.graduationDegree = d.eduDegreeID')
								->join('tbl_edu_department dp','g.graduationDepartment = dp.eduDepartmentID')
								->join('tbl_edu_place p','g.graduationUniversity = p.eduPlaceID')
								->where(array('staffID'=>$id,'graduationType'=>'3'))
								->get();

		$data['rw'] = $this->db->from('tbl_staff_work w')
								->join('tbl_position p','w.positionID = p.positionID')
								->where('w.staffID',$id)
								->order_by('w.workStartDate','ASC')
								->get();

		$data['rn'] = $this->db->from('tbl_staff_note')
								->where('staffID',$id)
								->get();

		$data['rcn'] = $this->db->from('tbl_staff_name')
								->where('staffID',$id)
								->get();

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		$xcrud->table('tbl_staff_mistake')->where('staffID =', $id);
		//// List /////
		$col_name = array(
			'mistakeNo' => 'เลขที่คำสั่ง',
			'mistakeDate' => 'วันที่มีผล',
			'mistakeDetail' => 'รายละเอียด'
		);
		$xcrud->columns('mistakeNo,mistakeDate,mistakeDetail');
		$xcrud->label($col_name);
		$xcrud->column_callback('mistakeDate','toBDDate');
		$xcrud->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlmistake'] = $xcrud->render();

		$xcrud2 = xcrud_get_instance();
		$xcrud2->table('tbl_staff_seminar')->where('staffID =',$id);
		$xcrud2->relation('seminarID','tbl_seminar','seminarID','seminarTitle');
		$col_name = array(
			'seminarID' =>'หัวข้ออบรม/สัมมนา',
			'staffID' => 'วันที่จัดอบรม',
			'isLecturer' => 'ผู้เข้าร่วม / วิทยากร'
		);
		$xcrud2->columns('staffID,seminarID,isLecturer');
		$xcrud2->label($col_name);
		$xcrud2->column_callback('staffID','show_seminar_date');
		$xcrud2->change_type('isLecturer','select','',array('1'=>'ผู้เข้าร่วม / วิทยากร','0'=>''));
		$xcrud2->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlseminar'] = $xcrud2->render();

		$xcrud3 = xcrud_get_instance();
		$xcrud3->table('tbl_project_member')->where('person_id =', $id);
		$xcrud3->join('project_id','tbl_project','id');
		$xcrud3->join('position_id','tbl_project_position','id');
		
		//// List /////
		$col_name = array(
			'tbl_project.startDate' => 'วันที่',
			'tbl_project.name' => ' หัวข้อ / โครงการ',
			'tbl_project_position.name' => 'ตำแหน่ง'

		);

		$xcrud3->columns('tbl_project.startDate,tbl_project.name,tbl_project_position.name');
		$xcrud3->label($col_name);
		$xcrud3->column_pattern('tbl_project.startDate',' {value} - {tbl_project.endDate}');
		$xcrud3->column_callback('tbl_project.startDate','show_feat_date');
		$xcrud3->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlfeat'] = $xcrud3->render();

		$xcrud4 = xcrud_get_instance();
		$xcrud4->table('tbl_staff_contract')->where('staffID',$id);
		$xcrud4->relation('contractType','tbl_contract_type','contracttypeID','contracttypeType');
		$xcrud4->columns('contractNumber,contractType,contractField');
		$xcrud4->column_callback('contractType','calAgeContract');
		$xcrud4->column_callback('contractField','calTimeContract');
		$col_name = array(
			'contractNumber' => 'เลขที่',
			//'contractDate' => 'วันที่ทำสัญญา',
			'contractType' => 'อายุสัญญา',
			'contractField' => 'ระยะเวลาของสัญญา',
			'contractID'	=> 'อายุสัญญา'
		);		
		$xcrud4->label($col_name);
		$xcrud4->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlcontract'] = $xcrud4->render();

		$xcrud5 = xcrud_get_instance();
		$xcrud5->table('tbl_staff_person')->where('staffID',$id);
		$xcrud5->columns('personType,personFName,personStatus,personWork');
		$xcrud5->column_pattern('personFName','{value} {personLName}');
		$xcrud5->change_type('personType','select','',array('3'=>'บิดา','4'=>'มารดา','1'=>'คู่สมรส','2'=>'บุตร'));
		$xcrud5->change_type('personStatus','radio','',array('1'=>'สมรส','0'=>'หย่า','2'=>'เสียชีวิต','3'=>'มีชีวิตอยู่','4'=>'ยกเลิกการใช้สิทธิ์สวัสดิการ'));
		$col_name = array(
			'personFName' => 'ชื่อ - สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personType' => 'ความสัมพันธ์'
		);		
		$xcrud5->label($col_name);
		$xcrud5->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlperson'] = $xcrud5->render();

		$xcrud6 = xcrud_get_instance();
		$xcrud6->table('tbl_staff_contact')->where('staffID',$id);
		$xcrud6->columns('contactRelation,contactFName,contactWork,contactTel');
		$xcrud6->column_pattern('contactFName','{value} {contactLName}');
		$col_name = array(
			'contactFName' => 'ชื่อ - สกุล',
			'contactRelation' =>'ความสัมพันธ์',
			'contactTel'=>'เบอร์โทรศัพท์',
			'contactWork'=>'สถานที่ทำงาน'
		);		
		$xcrud6->label($col_name);
		$xcrud6->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlcontact'] = $xcrud6->render();

		$data['title'] = "พิมพ์ประวัติ";
		$data['staffName'] = getStaffName($id);
		$data['id'] = $id;
        $this->template->load("template/admin",'main', $data);

	}

	public function print_pdf(){
		// get id staff //
		if(!empty($_POST['id'])){


		$id = mysql_real_escape_string($this->input->post('id'));

		$data['chk_info'] = (!empty($_POST['chk_info'])) ? "1" : "";
		$data['chk_work'] = (!empty($_POST['chk_work'])) ? "1" : "";
		$data['chk_edu'] = (!empty($_POST['chk_edu'])) ? "1" : "";
		$data['chk_insignia'] = (!empty($_POST['chk_insignia'])) ? "1" : "";
		$data['chk_mistake'] = (!empty($_POST['chk_mistake'])) ? "1" : "";
		$data['chk_seminar'] = (!empty($_POST['chk_seminar'])) ? "1" : "";
		$data['chk_feat'] = (!empty($_POST['chk_feat'])) ? "1" : "";
		$data['chk_contract'] = (!empty($_POST['chk_contract'])) ? "1" : "";
		$data['chk_person'] = (!empty($_POST['chk_person'])) ? "1" : "";
		$data['chk_contact'] = (!empty($_POST['chk_contact'])) ? "1" : "";
		$data['chk_note'] = (!empty($_POST['chk_note'])) ? "1" : "";
		$data['chk_name'] = (!empty($_POST['chk_name'])) ? "1" : "";
		$data['chk_bday'] = (!empty($_POST['chk_bday'])) ? "1" : "";
		$data['chk_cname'] = (!empty($_POST['chk_cname'])) ? "1" : "";
		$data['chk_address'] = (!empty($_POST['chk_address'])) ? "1" : "";
		$data['chk_position'] = (!empty($_POST['chk_position'])) ? "1" : "";
		$data['chk_eday'] = (!empty($_POST['chk_eday'])) ? "1" : "";
		$data['chk_status'] = (!empty($_POST['chk_status'])) ? "1" : "";
		$data['chk_dwork'] = (!empty($_POST['chk_dwork'])) ? "1" : "";
		$data['chk_edu1'] = (!empty($_POST['chk_edu1'])) ? "1" : "";
		$data['chk_edu2'] = (!empty($_POST['chk_edu2'])) ? "1" : "";
		$data['chk_edu3'] = (!empty($_POST['chk_edu3'])) ? "1" : "";
		

		$data['rs'] = $this->db->from('tbl_staff s')
							//->join('tbl_work_status w','s.workStatusID = w.workStatusID','left')
							->where('ID',$id)
							->get();			
		
		$data['ri'] = $this->db->from('tbl_staff_insignia s')
								->join('tbl_insignia i','s.insigniaType = i.insigniaID')
								->where('s.staffID',$id)
								->order_by('s.insigniaYear','ASC')
								->get();

		$data['re'] = $this->db->from('tbl_staff_graduation g')
								->join('tbl_edu_degree d','g.graduationDegree = d.eduDegreeID')
								->join('tbl_edu_department dp','g.graduationDepartment = dp.eduDepartmentID')
								->join('tbl_edu_place p','g.graduationUniversity = p.eduPlaceID')
								->where(array('staffID'=>$id,'graduationType'=>'1'))
								->get();

		$data['re2'] = $this->db->from('tbl_staff_graduation g')
								->join('tbl_edu_degree d','g.graduationDegree = d.eduDegreeID')
								->join('tbl_edu_department dp','g.graduationDepartment = dp.eduDepartmentID')
								->join('tbl_edu_place p','g.graduationUniversity = p.eduPlaceID')
								->where(array('staffID'=>$id,'graduationType'=>'2'))
								->get();

		$data['re3'] = $this->db->from('tbl_staff_graduation g')
								->join('tbl_edu_degree d','g.graduationDegree = d.eduDegreeID')
								->join('tbl_edu_department dp','g.graduationDepartment = dp.eduDepartmentID')
								->join('tbl_edu_place p','g.graduationUniversity = p.eduPlaceID')
								->where(array('staffID'=>$id,'graduationType'=>'3'))
								->get();

		$data['rw'] = $this->db->from('tbl_staff_work w')
								->join('tbl_position p','w.positionID = p.positionID')
								->where('w.staffID',$id)
								->order_by('w.workStartDate','ASC')
								->get();

		$data['rn'] = $this->db->from('tbl_staff_note')
								->where('staffID',$id)
								->get();

		$data['rcn'] = $this->db->from('tbl_staff_name')
								->where('staffID',$id)
								->get();

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		$xcrud->table('tbl_staff_mistake')->where('staffID =', $id);
		//// List /////
		$col_name = array(
			'mistakeNo' => 'เลขที่คำสั่ง',
			'mistakeDate' => 'วันที่มีผล',
			'mistakeDetail' => 'รายละเอียด'
		);
		$xcrud->columns('mistakeNo,mistakeDate,mistakeDetail');
		$xcrud->label($col_name);
		$xcrud->column_callback('mistakeDate','toBDDate');
		$xcrud->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlmistake'] = $xcrud->render();

		$xcrud2 = xcrud_get_instance();
		$xcrud2->table('tbl_staff_seminar')->where('staffID =',$id);
		$xcrud2->relation('seminarID','tbl_seminar','seminarID','seminarTitle');
		$col_name = array(
			'seminarID' =>'หัวข้ออบรม/สัมมนา',
			'staffID' => 'วันที่จัดอบรม',
			'isLecturer' => 'ผู้เข้าร่วม / วิทยากร'
		);
		$xcrud2->columns('staffID,seminarID,isLecturer');
		$xcrud2->label($col_name);
		$xcrud2->column_callback('staffID','show_seminar_date');
		$xcrud2->change_type('isLecturer','select','',array('1'=>'ผู้เข้าร่วม / วิทยากร','0'=>''));
		$xcrud2->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlseminar'] = $xcrud2->render();
		
		$xcrud3 = xcrud_get_instance();
		$xcrud3->table('tbl_project_member')->where('person_id =', $id);
		$xcrud3->join('project_id','tbl_project','id');
		$xcrud3->join('position_id','tbl_project_position','id');
		
		//// List /////
		$col_name = array(
			'tbl_project.startDate' => 'วันที่',
			'tbl_project.name' => ' หัวข้อ / โครงการ',
			'tbl_project_position.name' => 'ตำแหน่ง'

		);

		$xcrud3->columns('tbl_project.startDate,tbl_project.name,tbl_project_position.name');
		$xcrud3->label($col_name);
		$xcrud3->column_pattern('tbl_project.startDate',' {value} - {tbl_project.endDate}');
		$xcrud3->column_callback('tbl_project.startDate','show_feat_date');
		$xcrud3->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlfeat'] = $xcrud3->render();

		$xcrud4 = xcrud_get_instance();
		$xcrud4->table('tbl_staff_contract')->where('staffID',$id);
		$xcrud4->relation('contractType','tbl_contract_type','contracttypeID','contracttypeType');
		$xcrud4->columns('contractNumber,contractType,contractField');
		$xcrud4->column_callback('contractType','calAgeContract');
		$xcrud4->column_callback('contractField','calTimeContract');
		$col_name = array(
			'contractNumber' => 'เลขที่',
			//'contractDate' => 'วันที่ทำสัญญา',
			'contractType' => 'อายุสัญญา',
			'contractField' => 'ระยะเวลาของสัญญา',
			'contractID'	=> 'อายุสัญญา'
		);		
		$xcrud4->label($col_name);
		$xcrud4->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlcontract'] = $xcrud4->render();

		$xcrud5 = xcrud_get_instance();
		$xcrud5->table('tbl_staff_person')->where('staffID',$id);
		$xcrud5->columns('personType,personFName,personStatus,personWork');
		$xcrud5->column_pattern('personFName','{value} {personLName}');
		$xcrud5->change_type('personType','select','',array('3'=>'บิดา','4'=>'มารดา','1'=>'คู่สมรส','2'=>'บุตร'));
		$xcrud5->change_type('personStatus','radio','',array('1'=>'สมรส','0'=>'หย่า','2'=>'เสียชีวิต','3'=>'มีชีวิตอยู่','4'=>'ยกเลิกการใช้สิทธิ์สวัสดิการ'));
		$col_name = array(
			'personFName' => 'ชื่อ - สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personType' => 'ความสัมพันธ์'
		);		
		$xcrud5->label($col_name);
		$xcrud5->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlperson'] = $xcrud5->render();

		$xcrud6 = xcrud_get_instance();
		$xcrud6->table('tbl_staff_contact')->where('staffID',$id);
		$xcrud6->columns('contactRelation,contactFName,contactWork,contactTel');
		$xcrud6->column_pattern('contactFName','{value} {contactLName}');
		$col_name = array(
			'contactFName' => 'ชื่อ - สกุล',
			'contactRelation' =>'ความสัมพันธ์',
			'contactTel'=>'เบอร์โทรศัพท์',
			'contactWork'=>'สถานที่ทำงาน'
		);		
		$xcrud6->label($col_name);
		$xcrud6->unset_add()->unset_edit()->unset_view()->unset_remove()->unset_search()->unset_numbers()->unset_pagination()->unset_limitlist();
		$data['htmlcontact'] = $xcrud6->render();
		$data['id'] = $id;
		
		$data_r['html'] = $this->load->view('print',$data,true);
		//echo $data_r['html']; exit;
		$this->load->view('print_pdf',$data_r);

		}else
			show_404();
	}

	
}
/* End of file staffprint.php */
/* Location: ./application/module/staffprint/staffprint.php */