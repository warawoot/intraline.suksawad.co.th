<?php 
/* 
1. Line 31 -> 2559 was hardcoded but replaced
$year : is made from the URL
*/ 
 $year = $var_evalYear;
?>

<style type="text/css">
	.form-control {
 	  color: #343232;
	}
 
	.bs-example:after {
			position: absolute;
			top: 15px;
			left: 15px;
			font-size: 12px;
			font-weight: 700;
			color: #959595;
			text-transform: uppercase;
			letter-spacing: 1px;
			content: " ";
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <?php /* 2559 should not be hardcoded */ ?>
                <h3>ข้อพิจารณาเลื่อนขั้นปี <?php echo $year?></h3>
            </header>
            <div class="form-group">
                
             </div>
             
             <form class="form-inline">
               <label class="control-label col-sm-3">กรอบวงเงินที่ได้รับอนุมัติ</label>
                <div class="col-sm-9">
                    <input type="text" style="width:150px" class="form-control" value="7.5">
                    <button type="button" class="btn btn-success btn-sm">Update</button>
                </div>
            </form>
             <br />

			<div class="panel-body">
                	<form action="<?php echo site_url();?>/salarys/" method="post" enctype="multipart/form-data">
                            <div class="form-group">
								  <button type="button" class="btn btn-success" onclick="window.open('<?php echo site_url();?>assets/import/EvalUp-Export.xls')">Export to Excel</button>
								  <button type="button" class="btn btn-info" onclick="window.open('<?php echo site_url();?>assets/import/EvalUpERP-Export.xls')">Export to ERP</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div> 
 
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
          <div class="panel-body">

          	<table class="xcrud-list table table-striped table-hover table-bordered">
				<tr class="xcrud-th">
					<th style="width:150px">ฝ่าย/สำนักงาน</th>
					<td align="left"><?php echo $sort_org_chart['orgName'];?></td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">จำนวนคน</th>
					<td align="left"><div id="number_people"></div></td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">ผู้มีสิทธิเลื่อนขั้น</th>
					<td align="left">&nbsp;</td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px" >กรอบวงเงินร้อยละ 7.5</th>
					<td align="left" id="page_max">&nbsp;</td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">วงเงินที่คำนวณได้</th>
					<td align="left" id="page_total" style="color:red">&nbsp;</td>
				</tr>
			   </table>
          </div>

          <div class="panel-body">  
           
       		<!--<table class="xcrud-list table table-striped table-hover table-bordered">
				<tr class="xcrud-th">
					<th style="width:150px">จำนวนคน</th>
					<td align="left"><div id="number_people"></div> </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">คะแนนเฉลี่ย (xi) </th>
					<td align="left"><div id="average"></div> </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">(&Sigma;(xi-x)<sup>2</sup>) / N </th>
					<td align="left">
                    	<div class="xcrud-overlay-sigma" >
                            	<span id="squareAll">&nbsp;</span>
                        </div>
                    </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">SD </th>
					<td align="left"> 
                    	<div class="xcrud-overlay-sd">
                            	<span id="sd">&nbsp;</span>
                        </div>
                    </td>
				</tr>
			 </table>-->
           
          </div>
          
          
          
            <br /> 
            <div class="panel-body">  

          <form action="<?php echo base_url(); ?>salarys/changeSalaries" method="post">
          <input type="hidden" name="current_url" value="<?php echo $this->uri->uri_string().'?'.$_SERVER['QUERY_STRING'];?>" />
          <input type="hidden" name="current_year" value="<?php echo $year; ?>" />
                  <table class="xcrud-list table table-striped table-hover table-bordered">
                        <thead>
                            <tr class="xcrud-th">
                                <th colspan="2" class="xcrud-num"> รหัสพนักงาน</th>
                                <th class="xcrud-column xcrud-action">ชื่อ-นามสกุล</th>
                                <th class="xcrud-column xcrud-action">ระดับ</th>
                                <th class="xcrud-column xcrud-action">คะแนน</th>
                                <th class="xcrud-column xcrud-action">เกรด</th>
                                <th class="xcrud-column xcrud-action">xi-x</th>
                                <th class="xcrud-column xcrud-action">(xi-x)2</th>
                                <th class="xcrud-column xcrud-action">T-Score</th>
                                <th class="xcrud-column xcrud-action">เงินเดือน</th>
                                <th class="xcrud-column xcrud-action">ขั้นที่</th>
                                <th class="xcrud-column xcrud-action">ขั้นที่ได้รับ</th>
                                <th class="xcrud-column xcrud-action">เงินเดือน</th>
                                <th class="xcrud-column xcrud-action">ขั้นที่</th>
                            </tr>    
                        </thead>
                        <tbody>
               			    <?php 
								 $num = 1 ;
								 $eval_get	= $this->rate_tsocre->rate_emp_party($var_agencies,$assingID);
                 $sd1 = 0;
                 $sd2 = 0; 
                 $sd3 = 0; 
                 $sd4 = 0; 
								 if(count($eval_get)>0){
									foreach ($eval_get as $key => $item) {
										 # code...
										//$eval	= $this->rate_model->get_eval($item->person_id,$round);
										 										
										if($item->person_id != ''){
											 $eval		= $this->rate_tsocre->tScoreSumScore($item->person_id);
											 $Sumscore	= ($eval['SumEvalScore'] !=''?$eval['SumEvalScore']:'-');
											 $var_sum1	= $eval['SumEvalScore'];
											 $xideletex1	= $Sumscore - $average; 
											 if($xideletex1<0){
													$bg_minus	=	"#EC0E0E";	 
											 }else{
													$bg_minus	=	'';	 
											 }
											 	 
                              ?> 
                         	<tr class="xcrud-row xcrud-row-0" id="rowID_<?php echo $item->person_id;?>" style="color:<?php echo $bg_minus;?>"> 
                            <td colspan="2" class="xcrud-current xcrud-num"> 
                            <input name="employee_id[]" id="employee_id[]" type="hidden" value="<?php echo $item->person_id;?>" />                                            <?php echo $item->person_id;?></td>
                            <td>&nbsp;<?php echo $item->person;?>&nbsp;<?php echo $item->personLName;?></td>
                            <td class="current_rank"><?php echo $item->rank;?></td>
                            <td>
                                <?php 
                                      echo $Sumscore;
                                      if($Sumscore != '-'){
                                        $eval_grade			= $this->rate_tsocre->tScoreSumGrade($eval['SumEvalScore'],$eval['evalFormID']);
                                        $eval_grade_text	= $eval_grade['levelText'];
                                      }else{
                                        $eval_grade_text = '-';
                                      }
                                 ?>
                            </td>
                            <td>
                                <?php
                                    echo  $eval_grade_text;
                                ?>
                            </td>
                            <td>
												<?php 
												 	if($Sumscore != '-'){
 														//echo $xideletex1 ;
														echo number_format($xideletex1, 4, '.', '');
													}else{
													   echo '-' ;
													}
												?>
                                            </td>
                                            <td>
                                            	<?php
													if($Sumscore != '-'){												
														$square1	=	pow($xideletex1,2);
														//echo $square1;
														echo number_format($square1, 4, '.', '');

                            $sd1	    += $square1;
													}else{
														echo '-';
														$sd1	    += 0;
													}
												?>
                                            </td>
                                            <td>
                                            	<?php
													if($Sumscore != '-'){	
                                                		echo number_format((($xideletex1/$sd*10)+50), 4, '.', ''); 
													}else{
														echo '-';	
													}
												?>
                                            </td>
                             <?php /* Salary && Step *** 
                            Remark:
                            So by directly calling your model from the view layer, you circumvent the application logic contained in your controllers, which is messy (and unexpected for any other developer having to work with your project).
                            The proper MVC way: Fetch your model in the controller action that outputs your view and have the controller pass the model on to your view.
                            
                            Will not change the current pattern now()
                             */

                            $salaries  = $this->salarys_model->get_2_last_salaries_employee($item->person_id,$year); 
                            $countAvailableSteps = 0;
                            if(count($salaries)>0 && array_key_exists("positionID", $salaries[0]) && array_key_exists("rankID", $salaries[0])){
                              $positionID = $salaries[0]["positionID"];
                              $rankID = $salaries[0]["rankID"];
                              $countAvailableSteps  = $this->salarys_model->count_steps_position($positionID,$rankID,$year);  
                            }

                             ?>                          
                              <td class="current_salary"><?php echo (count($salaries)>0 && array_key_exists("salary", $salaries[0])) ? $salaries[0]["salary"] + 0 : "&nbsp;"; ?></td>
                              <td class="current_position"><?php $positionID1 = (count($salaries)>0 && array_key_exists("positionID", $salaries[0])) ? $salaries[0]["positionID"] + 0 : "&nbsp;"; 
                                  echo $positionID1; 
                              ?></td>

                              <td>
                              <?php $positionID2 = (count($salaries)>1 && array_key_exists("positionID", $salaries[1])) ? $salaries[1]["positionID"] + 0 : "&nbsp;"; 
                                    $stepPos = -1;
                                                         $stepPos = (!empty($positionID1) && $positionID1!="&nbsp;" && !empty($positionID2) && $positionID2!="&nbsp;" )? ($positionID2-$positionID1)+0: -1;

                              ?>
                              	<select style="width:100%" class="addStep" name="emp_<?php echo $item->person_id;?>">
                                      <option <?php if($stepPos<0) echo "selected"; ?> disabled="disabled"></option> 
                                      <option value="0" <?php if($stepPos==0) echo "selected"; ?> >0</option>
                                      <?php if($countAvailableSteps > 0){ ?><option value="0.5" <?php if($stepPos==0.5) echo "selected"; ?> >0.5</option><?php } ?>
                                      <?php if($countAvailableSteps > 1){ ?><option value="1" <?php if($stepPos==1) echo "selected"; ?> >1</option><?php } ?>
                                      <?php if($countAvailableSteps > 2){ ?><option value="1.5" <?php if($stepPos==1.5) echo "selected"; ?> >1.5</option><?php } ?>
                                      <?php if($countAvailableSteps > 3){ ?><option value="2" <?php if($stepPos==2) echo "selected"; ?> >2</option><?php } ?>
                                  </select>
                              </td>
                              <td class="cell_new_salary"><?php echo (count($salaries)>1 && array_key_exists("salary", $salaries[1])) ? $salaries[1]["salary"] + 0 : "&nbsp;"; ?></td>
                              <td class="cell_new_position"><?php echo $positionID2 ?></td>
                          </tr> 
                                 <?php 
                                           $eval_get_tsocre	= $this->rate_tsocre->rate_emp_division($item->orgID,$assingID); 
                                           foreach ($eval_get_tsocre as $key => $item2) {
                                                if($item2->group_id != ''){
												   $eval	= $this->rate_tsocre->tScoreSumScore($item2->group_id);
                                                   $Sumscore	= ($eval['SumEvalScore'] !=''?$eval['SumEvalScore']:'-');
												   if(empty($var_sum2))$var_sum2=0;
                           $var_sum2		+= $eval['SumEvalScore'];
												   $xideletex2	=	 $Sumscore - $average; 
												 	if($xideletex2<0){
														$bg_minus	=	"#EC0E0E";	 
												 	}else{
														$bg_minus	=	'';	 
												 	}
                                 ?> 
                                                <tr class="xcrud-row xcrud-row-0"  id="rowID_<?php echo $item2->group_id;?>"  style="color:<?php echo $bg_minus;?>"> 
                                                    <td colspan="2" class="xcrud-current xcrud-num"> 
                                                    <input name="employee_id[]" id="employee_id[]" type="hidden" value="<?php echo $item2->group_id;?>" />                                                    <?php echo $item2->group_id;?></td>
                                                    <td>&nbsp;<?php echo $item2->groups;?>&nbsp;<?php echo $item2->groupsLName;?></td>
                                                    <td class="current_rank"><?php echo $item2->rank;?></td>
                                                    <td>
                                                        <?php 
                                                               echo $Sumscore;
                                                              if($Sumscore != '-'){
                                                                $eval_grade	= $this->rate_tsocre->tScoreSumGrade($eval['SumEvalScore'],$eval['evalFormID']);
                                                                $eval_grade_text	=	$eval_grade['levelText'];
                                                              }else{
                                                                $eval_grade_text = '-';
                                                              }
                                                         ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                                echo $eval_grade_text;
                                                         ?>
                                                    </td>
                                                    <td><?php 
															 if($Sumscore != '-'){
																//$xideletex2	=	 $Sumscore - $average; 
																//echo $xideletex2;
																echo number_format($xideletex2, 4, '.', '');
															 }else{
															   	echo "-";	 
															 }
														?> 
                                                	</td>
                                                    <td>
                                                    	<?php
															if($Sumscore != '-'){
																$square2	=	pow($xideletex2,2);
																//echo $square2;
																echo number_format($square2, 4, '.', '');
																$sd2	    += $square2;
															}else{
																echo '-';
																$sd2	    +=0;
															}
														?>
                                                    </td>
                                                    <td>
                                                    	<?php
															if($Sumscore != '-'){
																echo number_format((($xideletex2/$sd*10)+50), 4, '.', ''); 
															}else{
																echo "-";	
															}
														?>
                              </td>
                             <?php /* Salary && Step  ***/
                           
                            $salaries  = $this->salarys_model->get_2_last_salaries_employee($item2->group_id,$year); 
                            $countAvailableSteps = 0;
                            if(count($salaries)>0 && array_key_exists("positionID", $salaries[0]) && array_key_exists("rankID", $salaries[0])){
                              $positionID = $salaries[0]["positionID"];
                              $rankID = $salaries[0]["rankID"];
                              $countAvailableSteps  = $this->salarys_model->count_steps_position($positionID,$rankID,$year);  
                            }

                             ?>                          
                               <td class="current_salary"><?php echo (count($salaries)>0 && array_key_exists("salary", $salaries[0])) ? $salaries[0]["salary"] + 0 : "&nbsp;"; ?></td>
                              <td class="current_position"><?php $positionID1 = (count($salaries)>0 && array_key_exists("positionID", $salaries[0])) ? $salaries[0]["positionID"] + 0 : "&nbsp;"; 
                                  echo $positionID1; 
                                ?></td>

                                <td>
                                <?php $positionID2 = (count($salaries)>1 && array_key_exists("positionID", $salaries[1])) ? $salaries[1]["positionID"] + 0 : "&nbsp;"; 
                                      $stepPos = -1;
                                      $stepPos = (!empty($positionID1) && $positionID1!="&nbsp;" && !empty($positionID2) && $positionID2!="&nbsp;" )? ($positionID2-$positionID1)+0: -1;

                                ?>
                                  <select style="width:100%" class="addStep" name="emp_<?php echo $item2->group_id;?>">
                                        <option <?php if($stepPos<0) echo "selected"; ?> disabled="disabled"></option> 
                                        <option value="0" <?php if($stepPos==0) echo "selected"; ?> >0</option>
                                        <?php if($countAvailableSteps > 0){ ?><option value="0.5" <?php if($stepPos==0.5) echo "selected"; ?> >0.5</option><?php } ?>
                                        <?php if($countAvailableSteps > 1){ ?><option value="1" <?php if($stepPos==1) echo "selected"; ?> >1</option><?php } ?>
                                        <?php if($countAvailableSteps > 2){ ?><option value="1.5" <?php if($stepPos==1.5) echo "selected"; ?> >1.5</option><?php } ?>
                                        <?php if($countAvailableSteps > 3){ ?><option value="2" <?php if($stepPos==2) echo "selected"; ?> >2</option><?php } ?>
                                    </select>
                                </td>
                                <td class="cell_new_salary"><?php echo (count($salaries)>1 && array_key_exists("salary", $salaries[1])) ? $salaries[1]["salary"] + 0 : "&nbsp;"; ?></td>
                                <td class="cell_new_position"><?php echo $positionID2 ?></td>
                            </tr> 
                          <?php }else{?>
                          <!--<tr class="xcrud-row xcrud-row-0">  
                              <td colspan="8" class="xcrud-current xcrud-num"> ไม่พบข้อมูล หัวหน้ากอง</td>
                          </tr>-->
                          <?php
                                }
                          ?>
                           <?php 
                                  $eval_get_tsocress	= $this->rate_tsocre->rate_emp_department($item2->orgID,$assingID); 
                                   foreach ($eval_get_tsocress as $key => $item4) {
                            ?> 
                               <?php 
                                      $eval_get_tsocress44	= $this->rate_tsocre->rate_emp_department_person($item4->orgID,$assingID); 
                                       foreach ($eval_get_tsocress44 as $key => $item5) {
                                           if($item5->department_id !=''){
																	
																	 $eval		= $this->rate_tsocre->tScoreSumScore($item5->department_id);
                                                                     $Sumscore	= ($eval['SumEvalScore'] !=''?$eval['SumEvalScore']:'-');
																	 if(empty($var_sum3))$var_sum3=0;
                                   $var_sum3	+= $eval['SumEvalScore'];
																	 
																	 $xideletex3	= $Sumscore - $average; 
																	 if($xideletex3<0){
																			$bg_minus	=	"#EC0E0E";	 
																	 }else{
																			$bg_minus	=	'';	 
																	 }
                                                      ?>
                                                                    <tr class="xcrud-row xcrud-row-0"  id="rowID_<?php echo $item5->department_id;?>"  style="color:<?php echo $bg_minus;?>"> 
                                                                        <td colspan="2" class="xcrud-current xcrud-num"> 
                                                                         <input name="employee_id[]" id="employee_id[]" type="hidden" value="<?php echo $item5->department_id;?>" />                                                                         <?php echo $item5->department_id;?></td>
                                                                        <td>&nbsp;<?php echo $item5->department;?>&nbsp;<?php echo $item5->departmentLName;?></td>
                                                                        <td class="current_rank"><?php echo $item5->rank; ?></td>
                                                                        <td>  
                                                                            <?php                                                                                      
                                                                                      echo $Sumscore;
                                                                                      if($Sumscore != '-'){
                                                                                        $eval_grade	= $this->rate_tsocre->tScoreSumGrade($eval['SumEvalScore'],$eval['evalFormID']);
                                                                                        $eval_grade_text	=	$eval_grade['levelText'];
                                                                                      }else{
                                                                                        $eval_grade_text = '-';
                                                                                      }
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                    echo $eval_grade_text;
                                                                             ?>
                                                                        </td>
                                                                        <td>
																			<?php 
																				 if($Sumscore != '-'){
																					//$xideletex3	= $Sumscore - $average; 
																					//echo $xideletex3;
																					echo number_format($xideletex3, 4, '.', '');
																				 }else{
																					echo "-"; 
																				  }
																			?>
                                                                        </td>
                                                                        <td>
                                                                        	<?php
																				 if($Sumscore != '-'){
																					$square3	=	pow($xideletex3,2);
																					//echo $square3;
																					echo number_format($square3, 4, '.', '');
																					$sd3	    += $square3;
																				 }else{
																					echo '-';
																					$sd3	    += 0;
																				 }
																			?>
                                                                        </td>
                                                                        <td>
                                                                        	<?php
																				 if($Sumscore != '-'){
																					echo number_format((($xideletex3/$sd*10)+50), 4, '.', ''); 
																				 }else{
																					echo '-'; 
																				  }
																			?>
                                                                        </td>

                             <?php /* Salary && Step ***/                            

                            $salaries  = $this->salarys_model->get_2_last_salaries_employee($item5->department_id,$year); 
                            $countAvailableSteps = 0;
                            if(count($salaries)>0 && array_key_exists("positionID", $salaries[0]) && array_key_exists("rankID", $salaries[0])){
                              $positionID = $salaries[0]["positionID"];
                              $rankID = $salaries[0]["rankID"];
                              $countAvailableSteps  = $this->salarys_model->count_steps_position($positionID,$rankID,$nextYear);  
                            }

                             ?>                          
                               <td class="current_salary"><?php echo (count($salaries)>0 && array_key_exists("salary", $salaries[0])) ? $salaries[0]["salary"] + 0 : "&nbsp;"; ?></td>
                              <td class="current_position"><?php $positionID1 = (count($salaries)>0 && array_key_exists("positionID", $salaries[0])) ? $salaries[0]["positionID"] + 0 : "&nbsp;"; 
                                  echo $positionID1; 
                                ?></td>

                                <td>
                                <?php $positionID2 = (count($salaries)>1 && array_key_exists("positionID", $salaries[1])) ? $salaries[1]["positionID"] + 0 : "&nbsp;"; 
                                      $stepPos = -1;
                                                           $stepPos = (!empty($positionID1) && $positionID1!="&nbsp;" && !empty($positionID2) && $positionID2!="&nbsp;" )? ($positionID2-$positionID1)+0: -1;

                                ?>
                                  <select style="width:100%" class="addStep" name="emp_<?php echo $item5->department_id;?>">
                                        <option <?php if($stepPos<0) echo "selected"; ?> disabled="disabled"></option> 
                                        <option value="0" <?php if($stepPos==0) echo "selected"; ?> >0</option>
                                        <?php if($countAvailableSteps > 0){ ?><option value="0.5" <?php if($stepPos==0.5) echo "selected"; ?> >0.5</option><?php } ?>
                                        <?php if($countAvailableSteps > 1){ ?><option value="1" <?php if($stepPos==1) echo "selected"; ?> >1</option><?php } ?>
                                        <?php if($countAvailableSteps > 2){ ?><option value="1.5" <?php if($stepPos==1.5) echo "selected"; ?> >1.5</option><?php } ?>
                                        <?php if($countAvailableSteps > 3){ ?><option value="2" <?php if($stepPos==2) echo "selected"; ?> >2</option><?php } ?>
                                    </select>
                                </td>
                                <td class="cell_new_salary"><?php echo (count($salaries)>1 && array_key_exists("salary", $salaries[1])) ? $salaries[1]["salary"] + 0 : "&nbsp;"; ?></td>
                                <td class="cell_new_position"><?php echo $positionID2 ?></td>
                                        </tr> 
                                                         
                                                                 <?php }else{?>
                                                                    <!--<tr class="xcrud-row xcrud-row-0">  
                                                                        <td colspan="8" class="xcrud-current xcrud-num"> ไม่พบข้อมูล หัวหน้าแผนก</td>
                                                                    </tr>-->
                                                                 <?php }?>     
                                                          <?php }?>       
                                                          <?php 
                                                            $eval_get_tsocress5	= $this->rate_tsocre->rate_employee_org($item5->orgID,$assingID); 
                                                            foreach ($eval_get_tsocress5 as $key => $item6) {
                                                          ?> 
                                                         <?php 
                                                                $eval_get_tsocress55	= $this->rate_tsocre->rate_employee_person($item6->orgID,$assingID); 
                                                                if(count($eval_get_tsocress55)>0){
                                                                    foreach ($eval_get_tsocress55 as $key => $item7) {
                                                                         if($item7->employee_id !=''){
																			$eval	= $this->rate_tsocre->tScoreSumScore($item7->employee_id);
                                                                            $Sumscore	= ($eval['SumEvalScore'] !=''?$eval['SumEvalScore']:'-');
 																			
                                      if(empty($var_sum4))$var_sum4=0;
                                      $var_sum4	+= $eval['SumEvalScore'];
																			
																			$xideletex4	= $Sumscore - $average; 
																			if($xideletex4<0){
																					$bg_minus	=	"#EC0E0E";	 
																			 }else{
																					$bg_minus	=	'';	 
																			 }
																			 if($item7->employee_id != $item5->department_id){
																			
                                                         ?>
                                                                          <tr class="xcrud-row xcrud-row-0"  id="rowID_<?php echo $item7->employee_id;?>"  style="color:<?php echo $bg_minus;?>"> 
                                                                                <td colspan="2" class="xcrud-current xcrud-num"> 
                                                                                    <input name="employee_id[]" id="employee_id[]" type="hidden" value="<?php echo $item7->employee_id;?>" />		
                                                                                    <?php  echo $item7->employee_id; ?>
                                                                                 </td>
                                                                                <td>&nbsp;
																				<?php //echo $item7->staffPreName; ?>
																				<?php echo $item7->employee;?> <?php echo $item7->employeeLName;?>&nbsp; 
                                                                                </td>
                                                                                <td class="current_rank"><?php echo $item7->rank;?></td>
                                                                                <td>
                                                                                    <?php 
                                                                                               echo $Sumscore;
                                                                                              if($Sumscore != '-'){
                                                                                                $eval_grade	= $this->rate_tsocre->tScoreSumGrade($eval['SumEvalScore'],$eval['evalFormID']);
                                                                                                $eval_grade_text	=	$eval_grade['levelText'];
                                                                                              }else{
                                                                                                $eval_grade_text = '-';
                                                                                              }
                                                                                    ?>
                                                                                </td> 
                                                                                <td>
                                                                                    <?php
                                                                                            echo $eval_grade_text;
                                                                                     ?>
                                                                                </td>
                                                                                <td>
																					<?php 
																						if($Sumscore != '-'){
																							//$xideletex4	= $Sumscore - $average; 
																							//echo $xideletex4;
																							echo number_format($xideletex4, 4, '.', '');
																						}else{
																							echo "-";	
																						}
																					?>
                                                                                </td>
                                                                                <td>
                                                                                	<?php
																						if($Sumscore != '-'){
																							$square4	=	pow($xideletex4,2);
																							//echo $square4;
																							echo number_format($square4, 4, '.', '');
																							$sd4	    += $square4;	
																						}else{
																							echo '-';
																							$sd4	    += 0;
																						}
																					?>
                                                                                </td>
                                                                                <td>
                                                                                	<?php
																						if($Sumscore != '-'){
																							echo number_format((($xideletex4/$sd*10)+50), 4, '.', ''); 
																						}else{
																							echo "-";	
																						}
																					?>
                                                                                </td>
                             <?php /* Salary && Step ***/
                              //echo "<script>console.log(".$item7->employee_id.");</script>";                            

                            $salaries  = $this->salarys_model->get_2_last_salaries_employee($item7->employee_id,$year); 
                            $countAvailableSteps = 0;
                            if(count($salaries)>0 && array_key_exists("positionID", $salaries[0]) && array_key_exists("rankID", $salaries[0])){
                              $positionID = $salaries[0]["positionID"];
                              $rankID = $salaries[0]["rankID"];
                              $countAvailableSteps  = $this->salarys_model->count_steps_position($positionID,$rankID,$nextYear);  
                            }

                             ?>                          
                                <td class="current_salary"><?php echo (count($salaries)>0 && array_key_exists("salary", $salaries[0])) ? $salaries[0]["salary"] + 0 : "&nbsp;"; ?></td>
                              <td class="current_position"><?php $positionID1 = (count($salaries)>0 && array_key_exists("positionID", $salaries[0])) ? $salaries[0]["positionID"] + 0 : "&nbsp;"; 
                                  echo $positionID1; 
                                ?></td>

                                <td>
                                <?php $positionID2 = (count($salaries)>1 && array_key_exists("positionID", $salaries[1])) ? $salaries[1]["positionID"] + 0 : "&nbsp;"; 
                                      $stepPos = -1;
                                                           $stepPos = (!empty($positionID1) && $positionID1!="&nbsp;" && !empty($positionID2) && $positionID2!="&nbsp;" )? ($positionID2-$positionID1)+0: -1;

                                ?>
                                  <select style="width:100%" class="addStep" name="emp_<?php echo $item7->employee_id;?>">
                                        <option <?php if($stepPos<0) echo "selected"; ?> disabled="disabled"></option> 
                                        <option value="0" <?php if($stepPos==0) echo "selected"; ?> >0</option>
                                        <?php if($countAvailableSteps > 0){ ?><option value="0.5" <?php if($stepPos==0.5) echo "selected"; ?> >0.5</option><?php } ?>
                                        <?php if($countAvailableSteps > 1){ ?><option value="1" <?php if($stepPos==1) echo "selected"; ?> >1</option><?php } ?>
                                        <?php if($countAvailableSteps > 2){ ?><option value="1.5" <?php if($stepPos==1.5) echo "selected"; ?> >1.5</option><?php } ?>
                                        <?php if($countAvailableSteps > 3){ ?><option value="2" <?php if($stepPos==2) echo "selected"; ?> >2</option><?php } ?>
                                    </select>
                                </td>
                                <td class="cell_new_salary"><?php echo (count($salaries)>1 && array_key_exists("salary", $salaries[1])) ? $salaries[1]["salary"] + 0 : "&nbsp;"; ?></td>
                                <td class="cell_new_position"><?php echo $positionID2 ?></td>
                            </tr> 
                                                                    <?php }
																	}else{?>
                                                                        <!--<tr class="xcrud-row xcrud-row-0">  
                                                                            <td colspan="8" class="xcrud-current xcrud-num"> ไม่พบข้อมูล พนักงานในแผนก</td>
                                                                        </tr>-->
                                                                    <?php }?>
                                                                 <?php }?>
                                                       <?php }else{
                                                    ?>  
                                                    <?php   
                                                        }
                                                    ?>
                                                    <?php } ?></tr>
                                                   <?php } ?>
                                             <?php } ?>
                                        <?php
                                          }else{?>
                                            <!--<tr class="xcrud-row xcrud-row-0">  
                                                <td colspan="8" class="xcrud-current xcrud-num"> ไม่พบข้อมูล หัวหน้าฝ่าย</td>
                                            </tr>-->
                                     <?php
                                        }
                                    }
                                 }else{
                                ?>	   
                             <tr class="xcrud-row xcrud-row-0"> 
                                <td colspan="14" class="xcrud-current xcrud-num">ไม่พบข้อมูล ฝ่าย</td>
                             </tr>
                        <?php } 
						
								$var_sums_score	= $var_sum1+$var_sum2+$var_sum3+$var_sum4 ;
								 
								if( $var_sums_score == ''){
									 $var_sums_score = 0; 
									 $squareAll		= 0;
								}else{
									 $var_sums_score	=  $var_sums_score;
									 $squareAll			= $sd1+$sd2+$sd3+$sd4;
 								}
						?>          
                       </tbody>
                     <tfoot>
                   </tfoot>
                </table> 
          <input type="submit" class="btn btn-success pull-right" value="save" />
          </form>
       	    </div> 
        </section>
   </div>
<div>

<script type="application/javascript">

/*  
	var form 	= document.getElementById("myForm"),
		inputs  = form.getElementsByTagName("input"),
		arr 	= [];
		  
	  for(var i=0, len=inputs.length; i<len; i++){
		if(inputs[i].type === "hidden"){
		  arr.push(inputs[i].value);
		}
	  }
  
    var n		=	arr.length;
	 
 	if(n !=''){
		   
		  var score		= <?php echo $var_sums_score;?> 
		  var average	= score / n ;	
		 
		  $('#number_people').html(n);  
		  $('#average').html(average.toFixed(4)); 
 		  
		  var square	= <?php echo $squareAll ;?> / n;
		  var sds		= Math.sqrt(square).toFixed(6);
		  
		  $('#squareAll').html(square.toFixed(4)); 
		  $('#sd').html(sds);  
		  //console.log(arr.length);
 		  
		  var XcrudAverage = "<?php echo $average;?>";
		  var aaaa = 1;
		  if (XcrudAverage == '') {
				 //alert(square );http://localhost/dpo_ehr/salarys/dept/82
				location.href = '<?php echo base_url() ;?>salarys/dept?n='+n+'&sd='+sds+'&average='+average.toFixed(4)+'&evalYear=<?php echo $var_evalYear; ?>&agencies=<?php echo $var_agencies;?>&evalRound=<?php echo $var_eval_date; ?>';
				
		  }	else{
			    
			 if(sds != "<?php echo $sd;?>"){ 	 
				location.href = '<?php echo base_url() ;?>salarys/dept?n='+n+'&sd='+sds+'&average='+average+'&evalYear=<?php echo $var_evalYear; ?>&agencies=<?php echo $var_agencies;?>&evalRound=<?php echo $var_eval_date; ?>';
			 }
		  }
		   
	 }
*/
   //JAVASCRIPT ADDED

  //INIT
  $("#page_max").html("28080");


jQuery.fn.extend({
  totalChange:function(){
    var total_salary = 0;
    $(".cell_new_salary").each(function(){
      total_salary += !isNaN(parseFloat($( this ).html()))?parseFloat($( this ).html()):0;
    });
    $("#page_total").html(total_salary);

    var page_max = parseFloat($("#page_max").html());
    if(total_salary > page_max){
      $("#page_total").css({ 'color': 'red' });
    }else{
      $("#page_total").css({ 'color': '#000' });
    }
  }
});
  $(this).totalChange();
  //EVENTS    
  $( "#page_total" ).change(function() {
    $(this).totalChange();
  });   
  //ACTIONS
  $.ajaxSetup({ cache: false });
  $(".addStep").change(function() {
    var parentRow = $(this).closest('tr');
    var rank = parseInt(parentRow.find('.current_rank').html());
    var position = parseFloat(parentRow.find('.current_position').html());
    var year = <?php echo $year; ?>;
    var step = parseFloat($(this).val());
    position += step;
    if(rank && position && year){
      $.getJSON( "<?php echo base_url(); ?>salarys/salaryEstimation", {rank:rank, position:position, year: year, ajax:'true'}, function( data ) {
        if(!data){
          alert("could not find the salary!");
        }
        if(data){
          if(data.salary){
              parentRow.find('.cell_new_salary').html(parseFloat(data.salary) + 0);
              $(this).totalChange();
          }
          if(data.positionID)parentRow.find('.cell_new_position').html(parseFloat(data.positionID) + 0);
                    
        }
      });
    } 
  });

 
</script>

 
 
 