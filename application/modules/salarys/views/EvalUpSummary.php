<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3> จัดการเลื่อนขั้นเงินเดือน </h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url(); ?>salarys" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">เลือกปีงบประมาณ</label>
                                <div class="col-sm-9">
                                    <?php 
                                        echo form_dropdown('eval_date_year',$eval_date_year,$year,'class="form-control" onchange="changeRate(this);"  ');
                                    ?>
                                </div>
                             </div>
                           <div class="form-group">
                              <label class="control-label col-sm-3">เลือกรอบ</label>
                              	<div class="col-sm-9">
                                <?php if($round ==''){?>
                                	<span id="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE">
<select name="evalRound" id="evalRound" class="form-control">
                                            <option value="" selected="selected">กรุณาระบุรอบด้วย</option>
                                         </select>
                                    </span>
                                    <?php
									}else{
                                    	//echo $round;
									?>
										<select name="evalRound" id="evalRound" class="form-control">
                                        	<option value="">กรุณาระบุรอบด้วย</option>
                                          <option value="<?php echo $round;?>" selected="selected"><?php echo $round;?></option>
                                       </select>
									<?php
									}
									?>
                             </div>
                      </div>
							<div class="form-group">
                              <label class="control-label col-sm-3">กรอบวงเงิน ร้อยละ </label>
                              	<div class="col-sm-9">
									<input id="percent" name="percent" type="text" style="width:200px" class="form-control" value="7.5">
                                </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">ค้นหา</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div> 
<br /><br />
<table class="xcrud-list table table-striped table-hover table-bordered">
    <thead>
       	 <tr class="xcrud-th">
                <th class="xcrud-num" rowspan="2">#</th>
                <th class="xcrud-column xcrud-action" rowspan="2" data-order="asc" data-orderby="tbl_eval_date.evalYear">
                ฝ่าย/สำนักงาน </th>
                <th colspan="2">พนักงาน/ลูกจ้าง</th>
                <th colspan="2">พนักงานจ้าง</th>
                <th colspan="2">รวม</th>
                <th colspan="2">ผู้ไม่มีสิทธิ์เลื่อนขั้น</th>
                <th colspan="2">ผู้มีสิทธิ์เลื่อนขั้น</th>
                <th rowspan="2">กรอบวงเงิน<br/>ร้อยละ 7.5 (บาท)</th>
                <th rowspan="2">ระดับ 8 ขึ้นไป<br/>อัตราเงินเดือน (บาท)</th> 
            </tr>		
            <tr class="xcrud-th">
                <th>(คน)</th>
                <th>(บาท)</th>
                <th>(คน)</th>
                <th>(บาท)</th>
                <th>(คน)</th>
                <th>(บาท)</th>
                <th>(คน)</th>
                <th>(บาท)</th>
                <th>(คน)</th>
                <th>(บาท)</th>
            </tr>		
        </thead>
        <tbody>
        <!--<tr class="xcrud-row xcrud-row-0" align="right"> 
            <td class="xcrud-current xcrud-num" align="left">test</td>
            <td align="left">&nbsp;<a href="<?php // echo site_url()?>salarys/dept">test</a></td>
            <td>27</td>
            <td>735,090</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>27</td>
            <td>735,090</td>
            <td>1</td>
            <td>16,830</td>
            <td>26</td>
            <td>718,260</td>
            <td>53,869.50</td>
            <td>73,370.00</td>
         </tr>-->
        <?php
        	 if(count($org_faction)>0){
            	foreach ($org_faction as $key => $item) {
		?>
           	<tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left"><?php echo ++$key;?></td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept/?agencies=<?php echo $item->orgID;?>&percent=<?php echo $percent;?>&evalYear=<?php echo $year;?>&evalRound=<?php echo $round;?>"><?php echo  $item->orgName; ?></a></td>
                <td>
                <?php
                	 	
						$sort_org	= $this->salarys_model->sort_org($item->orgID,$item->assignID);
 						$count_org = count(explode(",", $sort_org['orgID']));
					    $staffTypeID		=	"1,4,5 ";
						$sort_org	= $this->salarys_model->sort_orgID($item->orgID,$sort_org['orgID'],$count_org,$item->assignID);
 						$sort_staff	= $this->salarys_model->sort_staff( $sort_org['group_org'],$staffTypeID);
						$Employee_staff		=	$sort_staff['count_id'];
						echo $Employee_staff;	
						 
 				?>
                </td>
                <td>&nbsp;</td>
                <td>
                 <?php
				 	$staffTypeID		=	"6";
 					$sort_org	= $this->salarys_model->sort_org($item->orgID,$item->assignID); 
					$count_org = count(explode(",", $sort_org['orgID'])); 
					$sort_org	= $this->salarys_model->sort_orgID($item->orgID,$sort_org['orgID'],$count_org,$item->assignID); 
					$sort_staff			= $this->salarys_model->sort_staff( $sort_org['group_org'],$staffTypeID);
					$employees_hiring	=	$sort_staff['count_id']; 
					echo $employees_hiring;
  				?>
                </td>
                <td>&nbsp;</td>
                <td>
                <?php
                		echo $Employee_staff + $employees_hiring;
				?>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
           
       <?php }
            }else{
       ?> <tr class="xcrud-row xcrud-row-0" align="center">
           	  <td colspan="14" align="left" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
       	  </tr>
         <?php } ?>      
        </tbody>
    <tfoot>
   </tfoot>
</table>
  
<script type="text/javascript"> 

	function changeRate(obj){
    	//alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/recentdll') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   var obj = jQuery.parseJSON(response); 
				   //alert(response);
				   //console.log(obj); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(obj);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
				    location.replace('<?php echo base_url().'rate/recent' ?>');
				}
		});	
  	}
  
</script>				
        