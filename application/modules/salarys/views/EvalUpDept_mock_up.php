 <style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อพิจารณาเลื่อนขั้นปี 2559</h3>
            </header>
            <div class="form-group">
                
             </div>
             
             <form class="form-inline">
               <label class="control-label col-sm-3">กรอบวงเงินที่ได้รับอนุมัติ</label>
                <div class="col-sm-9">
                    <input type="text" style="width:150px" class="form-control" value="7.5">
                    <button type="button" class="btn btn-success btn-sm">Update</button>
                </div>
            </form>
             <br />

			<div class="panel-body">
                	<form action="http://dpo-ehr.smmms.com/rate/tscore" method="post" enctype="multipart/form-data">
                            <div class="form-group">
								  <button type="button" class="btn btn-success" onclick="window.open('<?php echo site_url();?>assets/import/EvalUp-Export.xls')">Export to Excel</button>
								  <button type="button" class="btn btn-info" onclick="window.open('<?php echo site_url();?>assets/import/EvalUpERP-Export.xls')">Export to ERP</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
			   ﻿<table class="xcrud-list table table-striped table-hover table-bordered">
				<tr class="xcrud-th">
					<th style="width:150px">ฝ่าย/สำนักงาน</th>
					<td align="left">ฝ่ายการตลาดและการขาย</td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">จำนวนคน</th>
					<td align="left">47</td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">ผู้มีสิทธิเลื่อนขั้น</th>
					<td align="left">47</td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">กรอบวงเงินร้อยละ 7.5</th>
					<td align="left">98,703.75</td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">วงเงินที่คำนวณได้</th>
					<td align="left" style="color:red">100,203.75</td>
				</tr>
			   </table>
	    	<br/><br/>
            <div class="panel-body">
                <link href="http://dpo-ehr.smmms.com/xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="http://dpo-ehr.smmms.com/xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                <link href="http://dpo-ehr.smmms.com/xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                <link href="http://dpo-ehr.smmms.com/xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                        <div class="xcrud">
                            <div class="xcrud-container">
                                <div class="xcrud-ajax">
                                    
                                    <div class="xcrud-list-container">
                                    
                                     ﻿<table class="xcrud-list table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr class="xcrud-th">
                                                <th class="xcrud-num">#</th>
                                                <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_eval_date.evalYear">รหัสพนักงาน </th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.evalRound" class="xcrud-column xcrud-action">ชื่อ-นามสกุล</th>
                                                <th align="center">ระดับ </th>
                                                <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_eval_date.endDate"> T-Score</th>
                                                <th align="center">เงินเดือน </th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ขั้นที่</th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ขั้นที่ได้รับ</th>
                                                <th align="center">เงินเดือน </th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ขั้นที่</th>
                                        </thead>
                                        <tbody>
												<tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">1</td>
                                                    <td>145</td>
                                                    <td>นางสุนันท์ ถาวรวงษ์</td>
                                                    <td align="right">7</td>
                                                    <td align="right">59.68</td>
                                                    <td align="right">57,830</td>
                                                    <td align="right">32</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected>1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">57,830</td>
                                                    <td align="right">32</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-1"> 
                                                    <td class="xcrud-current xcrud-num">2</td>
                                                    <td>144</td>
                                                    <td>นางสาวภูมิทิพย์ กูบโคกกรวด</td>
                                                    <td align="right">7</td>
                                                    <td align="right">53.70</td>
                                                    <td align="right">48,570</td>
                                                    <td align="right">29</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">48,570</td>
                                                    <td align="right">29</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-0" style="color:red"> 
                                                    <td class="xcrud-current xcrud-num">3</td>
                                                    <td>161</td>
                                                    <td>นางสาวณัฐยา เหล่าลดา</td>
                                                    <td align="right">6</td>
                                                    <td align="right">48.91</td>
                                                    <td align="right">57,830</td>
                                                    <td align="right">32</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">57,830</td>
                                                    <td align="right">32</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-1" style="color:red"> 
                                                    <td class="xcrud-current xcrud-num">4</td>
                                                    <td>28</td>
                                                    <td>นางสาวณัฐณิชา พุทธาราม</td>
                                                    <td align="right">4</td>
                                                    <td align="right">39.34</td>
                                                    <td align="right">15,890</td>
                                                    <td align="right">11</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">15,890</td>
                                                    <td align="right">11</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-0" style="color:red"> 
                                                    <td class="xcrud-current xcrud-num">5</td>
                                                    <td>168</td>
                                                    <td>นางกาญจนา ริมประโคน</td>
                                                    <td align="right">4</td>
                                                    <td align="right">45.32</td>
                                                    <td align="right">16,350</td>
                                                    <td align="right">11.5</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">16,350</td>
                                                    <td align="right">11.5</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-1"> 
                                                    <td class="xcrud-current xcrud-num">6</td>
                                                    <td>184</td>
                                                    <td>นางสิริรัตน์ พินิตตานนท์</td>
                                                    <td align="right">3</td>
                                                    <td align="right">53.10</td>
                                                    <td align="right">23,290</td>
                                                    <td align="right">17.5</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">23,290</td>
                                                    <td align="right">17.5</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">7</td>
                                                    <td>175</td>
                                                    <td>นางขนิษฐา ไวทยาคม</td>
                                                    <td align="right">3</td>
                                                    <td align="right">58.48</td>
                                                    <td align="right">16,830</td>
                                                    <td align="right">12</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">16,830</td>
                                                    <td align="right">12</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-1" style="color:red"> 
                                                    <td class="xcrud-current xcrud-num">8</td>
                                                    <td>1130</td>
                                                    <td>นายสมชาติ อ้อนอุบล</td>
                                                    <td align="right">3</td>
                                                    <td align="right">30.97</td>
                                                    <td align="right">15,000</td>
                                                    <td align="right">10</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">15,000</td>
                                                    <td align="right">10</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">9</td>
                                                    <td>131</td>
                                                    <td>นางวิไลรัตน์ ตันวิเชียร</td>
                                                    <td align="right">7</td>
                                                    <td align="right">64.47</td>
                                                    <td align="right">54,480</td>
                                                    <td align="right">31</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">54,480</td>
                                                    <td align="right">31</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-1" style="color:red"> 
                                                    <td class="xcrud-current xcrud-num">10</td>
                                                    <td>40</td>
                                                    <td>นางนพวรรณ หอมชื่นชม</td>
                                                    <td align="right">4</td>
                                                    <td align="right">38.14</td>
                                                    <td align="right">29,920</td>
                                                    <td align="right">21.5</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">29,920</td>
                                                    <td align="right">21.5</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">11</td>
                                                    <td>195</td>
                                                    <td>นางวิญญรักษ์ ภักดี</td>
                                                    <td align="right">5</td>
                                                    <td align="right">57.89</td>
                                                    <td align="right">35,150</td>
                                                    <td align="right">24</td>
                                                    <td align="center">
														<select>
															<option></option>
															<option>0.5</option>
															<option selected="selected">1</option>
															<option>1.5</option>
														</select>
													</td>
                                                    <td align="right">35,150</td>
                                                    <td align="right">24</td>
                                                 </tr>
                                           </tbody>
                                        <tfoot>
                                       </tfoot>
                                    </table>
									<br/><br/>
									<strong>หมายเหตุ</strong>
							                       ﻿<table class="xcrud-list table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr class="xcrud-th">
                                                <th class="xcrud-num">#</th>
                                                <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_eval_date.evalYear">รหัสพนักงาน </th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.evalRound" class="xcrud-column xcrud-action">ชื่อ-นามสกุล</th>
                                                <th align="center">หมายเหตุ</th>
                                        </thead>
                                        <tbody>
												<tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">1</td>
                                                    <td>145</td>
                                                    <td>นางสุนันท์ ถาวรวงษ์</td>
                                                    <td>ย้ายมาจากสำนักงาน อสค.ภาคกลาง</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-1"> 
                                                    <td class="xcrud-current xcrud-num">2</td>
                                                    <td>144</td>
                                                    <td>นางสาวภูมิทิพย์ กูบโคกกรวด</td>
                                                    <td>ย้ายไปฝ่ายอำนวยการ</td>
                                                 </tr>
												<tr class="xcrud-row xcrud-row-1"> 
                                                    <td class="xcrud-current xcrud-num">2</td>
                                                    <td>195</td>
                                                    <td>นางวิญญรักษ์ ภักดี</td>
                                                    <td>เกษียณอายุในปีงบประมาณ 2558</td>
                                                 </tr>
                                  </div>
                                </div>
                            <div class="xcrud-overlay" style="display: none;">
                            	<span id="sum_squ">
									512</span>
                            </div>
                         </div>
       				</div>
              </div>
         </section>
    </div>
</div>
 	