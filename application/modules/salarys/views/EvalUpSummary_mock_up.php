<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผลล่าสุด </h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url(); ?>salarys" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">เลือกปีงบประมาณ</label>
                                <div class="col-sm-9">
                                    <select name="eval_date_year" class="form-control" onchange="changeRate(this);"  >
<option value="">กรุณาระบุด้วย</option>
<option value="2559" selected="selected">2559</option>
</select>                                </div>
                             </div>
                              
                             <div class="form-group">
                              <label class="control-label col-sm-3">กรอบวงเงิน ร้อยละ </label>
                              	<div class="col-sm-9">
									<input type="text" style="width:200px" class="form-control" value="7.5">
                                </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">ค้นหา</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div> 
<br /><br />
<table class="xcrud-list table table-striped table-hover table-bordered">
    <thead>
        <tr class="xcrud-th">
            <th class="xcrud-num" rowspan="2">#</th>
            <th class="xcrud-column xcrud-action" rowspan="2" data-order="asc" data-orderby="tbl_eval_date.evalYear">
			ฝ่าย/สำนักงาน </th>
            <th colspan="2">พนักงาน/ลูกจ้าง</th>
            <th colspan="2">พนักงานจ้าง</th>
            <th colspan="2">รวม</th>
            <th colspan="2">ผู้ไม่มีสิทธิ์<br/>เลื่อนขั้น</th>
            <th colspan="2">ผู้มีสิทธิ์เลื่อนขั้น</th>
            <th rowspan="2">กรอบวงเงิน<br/>ร้อยละ 7.5 (บาท)</th>
            <th rowspan="2">ระดับ 8 ขึ้นไป<br/>อัตราเงินเดือน (บาท)</th> 
		</tr>		
        <tr class="xcrud-th">
            <th>(คน)</th>
            <th>(บาท)</th>
            <th>(คน)</th>
            <th>(บาท)</th>
            <th>(คน)</th>
            <th>(บาท)</th>
            <th>(คน)</th>
            <th>(บาท)</th>
            <th>(คน)</th>
            <th>(บาท)</th>
		</tr>		
    </thead>
    <tbody>
                    <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">1</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายนโยบายและแผนงาน</a></td>
                <td>27</td>
                <td>735,090</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>27</td>
                <td>735,090</td>
                <td>1</td>
                <td>16,830</td>
                <td>26</td>
                <td>718,260</td>
                <td>53,869.50</td>
                <td>73,370.00</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">2</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายทรัพยากรบุคคล</a>   </td>
                <td>35</td>
                <td>1,023,520</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>35</td>
                <td>1,023,520</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>35</td>
                <td>1,023,520</td>
                <td>76,764.00</td>
                <td>0.00</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">3</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายบัญชีและการเงิน</a>   </td>
                <td>36</td>
                <td>1,344,520</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>36</td>
                <td>1,344,520</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>36</td>
                <td>1,344,520</td>
                <td>100,839.00</td>
                <td>155,730.00</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">4</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายพัสดุและบริการ</a>   </td>
                <td>60</td>
                <td>1,747,800</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>60</td>
                <td>1,747,800</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>60</td>
                <td>1,747,800</td>
                <td>131,085.00</td>
                <td>0.00</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">5</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายการตลาดและการขาย</a>   </td>
                <td>47</td>
                <td>1,316,050</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>47</td>
                <td>1,316,050</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>47</td>
                <td>1,316,050</td>
                <td>98,703.75</td>
                <td>59,510</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">6</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายวิจัยและพัฒนาการเลี้ยงโคนม</a>   </td>
                <td>71</td>
                <td>2,077,900</td>
                <td>13</td>
                <td>142,050</td>
                <td>84</td>
                <td>2,219,950</td>
                <td>1</td>
                <td>15,000</td>
                <td>83</td>
                <td>2,204,950</td>
                <td>165,371.25</td>
                <td>48,570</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">7</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายส่งเสริมการเลี้ยงโคนม</a>   </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">8</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายท่องเที่ยวเชิงเกษตร</a>   </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">9</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายตรวจสอบและประเมินระบบงาน</a>   </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
                     <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">10</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">ฝ่ายอำนวยการ   </a></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
             <tr class="xcrud-row xcrud-row-1" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">11</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">สำนักงาน อสค. ภาคกลาง</a></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
             <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">12</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">สำนักงาน อสค. ภาคเหนือตอนบน</a></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
             <tr class="xcrud-row xcrud-row-1" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">12</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">สำนักงาน อสค. ภาคเหนือตอนล่าง</a></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
             <tr class="xcrud-row xcrud-row-0" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">14</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">สำนักงาน อสค. ภาคตะวันออกเฉียงเหรือ</a></td>
                <td>&nbsp;</td>

                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
             <tr class="xcrud-row xcrud-row-1" align="right"> 
                <td class="xcrud-current xcrud-num" align="left">15</td>
                <td align="left">&nbsp;<a href="<?php echo site_url()?>salarys/dept">สำนักงาน อสค. ภาคใต้</a></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
                   
       </tbody>
    <tfoot>
   </tfoot>
</table>
  
<script type="text/javascript"> 

	function changeRate(obj){
    	//alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "http://dpo-ehr.smmms.com/rate/recentdll",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   var obj = jQuery.parseJSON(response); 
				   //alert(response);
				   //console.log(obj); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(obj);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
					location.replace('http://dpo-ehr.smmms.com/rate/recent');
				}
		});	
  	}
  
</script>
                         
                </section>
        </section>
        <!--main content end-->

    </section>	
