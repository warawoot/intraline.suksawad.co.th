<?php

class Salarys extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
		$this->load->model('rate/rate_model');
		$this->load->model('rate/rate_tsocre');
		$this->load->model('rate/recent_model');
     $this->load->model('salarys_model');
		$this->load->model('rate/recent_model');
 		$this->load->model('sequence/sequence_model');
		$this->load->model('organization/organization_model');
 		$this->assingID	=	$this->sequence_model->get_last_assignID();
  		
     }
 

    public function index() {
		
		$percent			= $this->input->get_post('percent');
		$year				= $this->input->get_post('eval_date_year');
		$evalRound			= $this->input->get_post('evalRound');
		
 		$data['eval_date_year']	= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown 
        $data['data']			= ''; 
		
		$data['percent']	= $percent; 
		$data['year']		= $year; 
		$data['round']		= $evalRound; 
		
		$data['org_faction']	= $this->recent_model->org_faction($this->assingID);#Dropdown
		$data['staff']			= $this->salarys_model->staff('','');
		
		//echo "<pre>";
		//print_r($data['staff']);
         $this->template->load('template/admin', 'EvalUpSummary',$data);
		
    }
    
	
	public function dept() {
  		 
        $eval				= $this->input->get_post('eval'); 
		$staffID			= $this->input->get_post('staffID'); 
		$var_agencies		= $this->input->get_post('var_agencies'); 
	 	 
		$n					= $this->input->get_post('n'); 
		$sd					= $this->input->get_post('sd'); 
   		$evalYear			= $this->input->get_post('evalYear'); 
		$agencies			= $this->input->get_post('agencies'); 
  	  	$evalRound			= $this->input->get_post('evalRound');
		$average			= $this->input->get_post('average');
 		$squareAll			= $this->input->get_post('squareAll'); 
		
		if($evalYear == ''){
			redirect(site_url().'salarys', 'refresh');
	  		exit();
		}
		
		if($evalRound	==	''){
			redirect(site_url().'salarys', 'refresh');
	  		exit();
		}
		
   		if($evalYear != '' and $evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assingID);#Dropdown
			//$data['roundRound']		= $this->recent_model->recent_get_dropdown_roundRound($evalYear,$evalRound);#Dropdown
 		}else{
			//$data['roundRound']		= '';	
			$data['org_faction']	= '';
 		}
 		
		
		
		$data['sort_org_chart']			= $this->salarys_model->sort_org_chart($agencies,$this->assingID);
		## mode
		$data['eval_date_year']			= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown เลือกปีงบประมาณ 
   	$data['dropdown_org_chart']  	= $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assingID);#Dropdown
 		$data['get_evalScore'] 			= $this->rate_model->get_evalScore($agencies,$this->assingID); 
 		$data['get_count_evalPerson']	= $this->rate_model->get_count_evalPerson($agencies); 
		//echo "<pre>";
		//print_r($data['get_count_evalPerson']); 
 		 
		$data['get_count_evalScore']	= $this->rate_model->get_count_evalScore($agencies); 
 		
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
        $data['data']		=	'';
		$data['var_eval_date']	=	$evalRound; //evalRound
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:($var_agencies!=''?$var_agencies:0)); //agencies
		$data['assingID']		=	$this->assingID; //assingID 
 		$data['average']		=	$average;
		$data['sd']				=	$sd	;
		$data['squareAll']		=	$squareAll;
		$data['n']				=	$n;
		
        $this->template->load('template/admin', 'EvalUpDept',$data);
		
    } 
	
  public function salaryEstimation() {

    if(!empty($_GET['rank']) && !empty($_GET['position']) && !empty($_GET['year'])){ 

      $rank     = $this->input->get_post('rank'); 
      $position      = $this->input->get_post('position');
      $year      = $this->input->get_post('year');
      $ajax      = $this->input->get_post('ajax'); 

      $data  = $this->salarys_model->salaryEstimation($position,$rank,$year); 
      if($ajax === "true"){
          echo json_encode($data);     
      }else{
        //any view?
      } 
    }
  }	 
   
  public function changeSalaries(){
    //var_dump($this->input->post());
    $employeeArr = array();
    foreach ($this->input->post() as $key => $value) {
        if (strpos($key, 'emp_') === 0) {
            $employeeArr[$key] = $value;
        }
    }
    $current_year = $this->input->get_post('current_year');
   $this->salarys_model->changeSalaries($employeeArr,$current_year);
    

    $current_url = $this->input->get_post('current_url');
    if(!empty($current_url)){
      redirect($current_url, 'refresh'); 
    }else{
      redirect('/', 'refresh'); 
    }



  }

}
/* End of file  .php */
/* Location: ./application/module/  */
?>