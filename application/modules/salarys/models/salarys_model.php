<?php

class Salarys_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	}
    
	
	function staff($OrgID,$assignID){ 
 			
         $query = $this->db->query('
		 			SELECT  count(t1.ID) as c_id FROM `tbl_staff` as t1  
					INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID 
					INNER JOIN tbl_staff_type as t3 ON t2.staffTypeID = t3.typeID
					WHERE t2.orgID in(82,158,159,160,188,189,201,202,203,208,209,210)    '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
		
		// --   , t1.staffPreName , t1.staffFName ,t1.staffLName ,t1.rankID , t2.staffTypeID , t3.typeName 
	}   
	
	
	function sort_org($orgID,$assignID){
		
		$query = $this->db->query('
										SELECT GROUP_CONCAT(t1.orgID) as orgID
										FROM `tbl_org_chart` as t1 --  12 รายการ ทั้ง ฝ่าย กอง แผนก
										WHERE ( t1.orgID = '.$orgID.' and t1.assignID = '.$assignID.' ) or ( t1.upperOrgID = '.$orgID.' and t1.assignID = '.$assignID.' ) 
										-- list orgID : ฝ่าย กอง แผนก
												-- or ( t1.upperOrgID = 158 and t1.assignID = 6 ) --   แผนกของ กองการตลาด
												-- or ( t1.upperOrgID = 159 and t1.assignID = 6 ) --   แผนกของ กองการขาย
												-- or ( t1.upperOrgID = 208 and t1.assignID = 6 ) --   แผนกของ กองเลขานุคณะกรรมการโคนมและผลิตภัณฑ์นม
   								 '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
 		
	}
 	
	function sort_org_to_org($orgID,$assignID){
		
		$query = $this->db->query('
										SELECT t1.orgID , t1.orgName  
										FROM `tbl_org_chart` as t1 
										WHERE ( t1.orgID = '.$orgID.' and t1.assignID = '.$assignID.' ) or ( t1.upperOrgID = '.$orgID.' and t1.assignID = '.$assignID.' ) 
 												  or ( t1.upperOrgID = '.$orgID.' and t1.assignID = '.$assignID.' )  
												  or ( t1.upperOrgID = '.$orgID.' and t1.assignID = '.$assignID.' )  
												  or ( t1.upperOrgID = '.$orgID.' and t1.assignID = '.$assignID.' )  
   								 '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
 		
	}
	
	function sort_staff($orgID,$staffTypeID){
		
		$query = $this->db->query('
										SELECT  COUNT( DISTINCT(t2.orgID) ) as count_id FROM `tbl_staff` as t1  
										INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID
										INNER JOIN tbl_staff_type as t3 ON t2.staffTypeID = t3.typeID
 										WHERE t2.orgID in('.$orgID.') and t2.staffTypeID in('.$staffTypeID.')
    								 '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
 		
	}
	
	function sort_orgID($orgID,$org,$count_org,$assignID){
		$org		= $org;
		$org1		= explode(",", $org) ;
		$count		= $count_org-1;
 		$xx 		= " SELECT GROUP_CONCAT(t1.orgID) as group_org   FROM `tbl_org_chart` as t1 
						WHERE ( t1.orgID = $orgID and t1.assignID = 6 ) or ( t1.upperOrgID = $orgID and t1.assignID = $assignID) ";
						for( $i = 1; $i<=$count;$i++){
		$xx 		.= "
							or ( t1.upperOrgID = $org1[$i] and t1.assignID = $assignID ) 
					  ";
						}
 		 							 
		//return $xx;								 
		$query = $this->db->query($xx);
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
 		
	}
	
 
	function sort_org_chart($orgID,$assignID){
		
		$query = $this->db->query('
						 	SELECT * FROM `tbl_org_chart` as t1 WHERE t1.orgID = '.$orgID.' and  t1.assignID = '.$assignID.'
    				  '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
 		
	}


  function get_2_last_salaries_employee($employeeID, $year){
      
      if(!empty($employeeID) && is_numeric($employeeID) && !empty($year) && is_numeric($year)){
        $query = $this->db->query("
          SELECT * from (SELECT a.positionID, b.rankID, b.salary, b.budgetYear, a.budgetYearSalary FROM `tbl_staff_salary` a 
                join `tbl_salary_rate` b on a.rankID = b.rankID and a.positionID = b.positionID and a.budgetYear = b.budgetYear
                WHERE a.staffID = $employeeID and a.budgetYearSalary < $year order by a.budgetYearSalary desc limit 1 ) as a
          UNION SELECT a.positionID, b.rankID, b.salary, b.budgetYear, a.budgetYearSalary FROM `tbl_staff_salary` a 
                join `tbl_salary_rate` b on a.rankID = b.rankID and a.positionID = b.positionID and a.budgetYear = b.budgetYear 
                WHERE a.staffID = $employeeID and a.budgetYearSalary = $year;");
        $data       = $query->result_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
      }else{
        return NULL;
      } 
  }

  function count_steps_position($positionID,$rankID,$year){
      if(!empty($positionID) && is_numeric($positionID) && !empty($rankID) && is_numeric($rankID) && !empty($year) && is_numeric($year)){
        $query = $this->db->query("SELECT count(salaryRateID) count FROM `tbl_salary_rate` WHERE positionID > $positionID and rankID = $rankID and budgetYear = $year;");
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data['count'] : NULL; 
      }else{
        return NULL;
      }
  }

  function salaryEstimation($positionID,$rankID,$year){
      if(!empty($positionID) && is_numeric($positionID) && !empty($rankID) && is_numeric($rankID) && !empty($year) && is_numeric($year)){
        $query = $this->db->query("SELECT * FROM `tbl_salary_rate` WHERE `rankID` = $rankID and `positionID` = $positionID and `budgetYear` = $year limit 1");
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
      }else{
        return NULL;
      }
  } 

  function changeSalaries($employeeArr,$year){
    if(!empty($employeeArr) && count($employeeArr)>0 && !empty($year) && is_numeric($year)){

      $queryBudgetYearSalary = $this->db->query("
        SELECT `budgetYear` FROM `tbl_salary_rate` where budgetYear <= $year order by budgetYear desc limit 1
      ");
      $rowBudgetYearSalary = $queryBudgetYearSalary->row_array(); 
      $budgetYear = ($queryBudgetYearSalary->num_rows() > 0) ? $rowBudgetYearSalary['budgetYear'] : NULL; 

      if(!empty($budgetYear)){
        foreach ($employeeArr as $employeeID => $employeeStep){

          $employeeID = str_replace("emp_","",$employeeID);
          if(!empty($employeeID) && is_numeric($employeeID) && isset($employeeStep) && is_numeric($employeeStep)){
            //1.get last rank/position before date
            $queryLookup = $this->db->query("
              SELECT a.positionID, a.rankID, a.budgetYear, a.budgetYearSalary FROM `tbl_staff_salary` a
                    WHERE a.staffID = $employeeID and a.budgetYearSalary < $year order by a.budgetYearSalary desc limit 1
              ");
            $lookup = $queryLookup->row_array();
            if($queryLookup->num_rows() > 0){
              //Change Position
              $newPosition = $lookup['positionID']+$employeeStep;
              $rankID = $lookup['rankID'];
              //Check if new position exists
              $queryNewPosExist = $this->db->query("
              SELECT salaryRateID FROM `tbl_salary_rate`
                    WHERE positionID = $newPosition and rankID = $rankID and budgetYear = $budgetYear limit 1
              "); 

              $NewPosExist = $queryNewPosExist->result_array(); 
              if($queryNewPosExist->num_rows() > 0){

                  $queryStaffSalary = $this->db->query("SELECT staffSalaryID FROM `tbl_staff_salary` WHERE staffID = $employeeID and budgetYearSalary = $year;");
                  $countStaffSalary = $queryStaffSalary->row_array(); 
                  
                  if($queryStaffSalary->num_rows() > 0){
                    //Update - Exists already
                    $data = array(
                       'budgetYear' => $budgetYear,
                       'positionID' => $newPosition
                    );
                    $this->db->where('staffID', $employeeID);
                    $this->db->where('budgetYearSalary', $year);

                    $this->db->update('tbl_staff_salary', $data); 
                    //return $this->db->last_query();
                  //insert
                  }else{
                    $data = array(
                       'staffID' => $employeeID,
                       'rankID' => $rankID,
                       'positionID' => $newPosition,
                       'budgetYear' => $budgetYear,
                       'budgetYearSalary' => $year
                    );
                    $this->db->insert('tbl_staff_salary', $data);                
                  

                  }

               
              }

            }
          }
        }
      }
    }
  }

   
} 

	/*		Test 82 : ฝ่ายการตลาดและการขาย 
			
			SELECT t1.orgID , t1.orgName FROM `tbl_org_chart` as t1 -- 12 รายการ ทั้ง ฝ่าย กอง แผนก
			WHERE (t1.orgID = 82 and t1.assignID = 6) or ( t1.upperOrgID = 82 and t1.assignID = 6 ) -- list orgID : ฝ่าย กอง แผนก
 			or ( t1.upperOrgID = 158 and t1.assignID = 6 ) --  แผนกของ กองการตลาด
  			or ( t1.upperOrgID = 159 and t1.assignID = 6 ) --  แผนกของ กองการขาย
  			or ( t1.upperOrgID = 208 and t1.assignID = 6 )  --  แผนกของ กองเลขานุคณะกรรมการโคนมและผลิตภัณฑ์นม
 
	
	*/
	
	/*
			SELECT  t1.ID  , t1.staffPreName , t1.staffFName ,t1.staffLName ,t1.rankID , t2.staffTypeID , t3.typeName FROM `tbl_staff` as t1 --  
			INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID
			INNER JOIN tbl_staff_type as t3 ON t2.staffTypeID = t3.typeID
			WHERE t2.orgID in(82,158,159,160,188,189,201,202,203,208,209,210)  
	*/