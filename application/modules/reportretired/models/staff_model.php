<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

        public function getByAgeExpried(){
        	$sql = "SELECT s.*
        			FROM tbl_staff s";
        	$r = $this->db->query($sql);
        	return $r->result();
        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/reg/models/staff_model.php */