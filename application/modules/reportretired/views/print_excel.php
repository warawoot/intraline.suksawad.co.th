<?php

$year = (!empty($year)) ? $year : (date("Y")+543);

header('Content-type: application/excel');
$filename = 'รายชื่อพนักงานที่เกษียณอายุในสิ้นปีงบประมาณ '.$year.'.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        <style>
			table,th,td{
				border: 1px solid black;
				border-collapse:collapse;
				text-align:center;
			}


		</style>
    </head>
	<body>

	
		<h5 style="text-align:center;"></h5>
		<h5 style="text-align:center;">รายชื่อพนักงานที่เกษียณอายุในสิ้นปีงบประมาณ <?php echo $year; ?></h5>

		<br><br>

		<p style="font-size:11px;">ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></p>
		
		<div class="xcrud-list-container">
		<table class="xcrud-list table table-striped table-hover table-bordered" style="font-size:11px;">
		
			<tr >
			<th rowspan="2">ลำดับ</th>
			<th rowspan="2">ชื่อ - นามสกุล</th>
			<th rowspan="2">ตำแหน่ง/ระดับ</th>
			<th rowspan="2">ระดับ</th>
			<th rowspan="2">แผนก</th>
			<th rowspan="2">กอง</th>
			<th rowspan="2">ฝ่าย</th>
			<th rowspan="2">เลขบัตรประชาชน</th>
			<th rowspan="2">วัน/เดื อน/ปีเกิด</th>
			<th rowspan="2">วัน/เดือน/ปี ใน อ.ส.ค.</th>
			<th rowspan="2">เงินเดือน</th>
			<th colspan="3">อายุการทำงาน</th>
			
		</tr>
		
		
		<tr ><!-- td 14 ตัว-->
			
			<td>อายุการทำงาน</td>
			<td>เงิน 6 เท่า</td>
			<td>เงิน 10 เท่า</td>
		</tr>
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 

				$expiredDate = getExpiredDateByWork($row->staffBirthday,true);
				$list = explode("-",$expiredDate);
				if($list[0] == $year-543){
					$i++; ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td style="text-align:left;"><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
						<td style="text-align:left;"><?php echo getPositionByWork($row->ID); ?></td>
						<td><?php echo getRankByWork($row->ID);?></td>
						<td style="text-align:left;"><?php echo getOrgByWork($row->ID);?></td>
						<td style="text-align:left;"><?php echo getOrg2ByWork($row->ID);?></td>
						<td style="text-align:left;"><?php echo getOrg1ByWork($row->ID); ?></td>
						<td><?php echo $row->staffIDCard; ?></td>
						<td><?php echo toFullDate($row->staffBirthday,'th'); ?></td>
						<td><?php $datework = getWorkDateByWork($row->ID); echo ($datework != '-') ? toFullDate($datework,'th') : $datework; ?></td>
						<td></td>
						<td><?php echo getWorkAgeByWork($row->ID); ?></td>
						<td></td>
						<td></td>
					</tr>
				<?php } ?>
				
		<?php }}

		if($i == 0)
			echo '<tr><td colspan="14">ไม่พบข้อมูล</td></tr>';
		 ?>
			
		</table>
		</div>

		<br>
		<span style="font-size:11px;">หมายเหตุ  &nbsp;&nbsp;&nbsp;เงิน 8 เท่า สำหรับพนักงานที่ทำงานวนครบติดต่อกัน 5 ปีขึ้นไป<br>
		<p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;เงิน 10 เท่า สำหรับพนักงานที่ทำงานครบติดต่อกัน 15 ปีขึ้นไป</p>
		</span>
	</body>
</html>
