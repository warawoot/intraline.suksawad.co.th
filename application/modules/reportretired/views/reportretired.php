
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	<?php $year = (!empty($year)) ? $year : (date("Y")+543); ?>
	<h5 style="text-align:center;">
		<br>
			รายชื่อพนักงานที่เกษียณอายุในสิ้นปีงบประมาณ <?php echo $year; ?>
		
	</h5><br>
	ค้นหา ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=(date("Y")+60);  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;
	<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>
	<br><br>

	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="2">ลำดับ</th>
			<th rowspan="2">ชื่อ - นามสกุล</th>
			<th rowspan="2">ตำแหน่ง/ระดับ</th>
			<th rowspan="2">ระดับ</th>
			<th rowspan="2">แผนก</th>
			<th rowspan="2">กอง</th>
			<th rowspan="2">ฝ่าย</th>
			<th rowspan="2">เลขบัตรประชาชน</th>
			<th rowspan="2">วัน/เดื อน/ปีเกิด</th>
			<th rowspan="2">วัน/เดือน/ปี ใน อ.ส.ค.</th>
			<th rowspan="2">เงินเดือน</th>
			<th colspan="3">อายุการทำงาน</th>
			
		</tr>
		
		
		<tr ><!-- td 14 ตัว-->
			
			<td>อายุการทำงาน</td>
			<td>เงิน 6 เท่า</td>
			<td>เงิน 10 เท่า</td>
		</tr>
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 

				$expiredDate = getExpiredDateByWork($row->staffBirthday,true);
				$list = explode("-",$expiredDate);
				if($list[0] == $year-543){
					$i++; ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td style="text-align:left;"><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
						<td style="text-align:left;"><?php echo getPositionByWork($row->ID); ?></td>
						<td><?php echo getRankByWork($row->ID);?></td>
						<td style="text-align:left;"><?php echo getOrgByWork($row->ID);?></td>
						<td style="text-align:left;"><?php echo getOrg2ByWork($row->ID);?></td>
						<td style="text-align:left;"><?php echo getOrg1ByWork($row->ID); ?></td>
						<td><?php echo $row->staffIDCard; ?></td>
						<td><?php echo toFullDate($row->staffBirthday,'th'); ?></td>
						<td><?php $datework = getWorkDateByWork($row->ID); echo ($datework != '-') ? toFullDate($datework,'th') : $datework; ?></td>
						<td></td>
						<td><?php echo getWorkAgeByWork($row->ID); ?></td>
						<td></td>
						<td></td>
					</tr>
				<?php } ?>
				
		<?php }}

		if($i == 0)
			echo '<tr><td colspan="14">ไม่พบข้อมูล</td></tr>';
		 ?>
		
	</table>
	</div>


	<span>หมายเหตุ  &nbsp;&nbsp;&nbsp;เงิน 8 เท่า สำหรับพนักงานที่ทำงานวนครบติดต่อกัน 5 ปีขึ้นไป<br>
	<p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;เงิน 10 เท่า สำหรับพนักงานที่ทำงานครบติดต่อกัน 15 ปีขึ้นไป</p>
	</span>

	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        /*$('#year').change(function(){
        	window.location='<?php echo site_url();?>reportretired?year='+$('#year :selected').val();
        })*/
		function search(){
			window.location='<?php echo site_url();?>reportretired?year='+$('#year :selected').val();
		}

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportretired/print_pdf?year='+$('#year :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportretired/print_excel?year='+$('#year :selected').val());
        }
</script>
