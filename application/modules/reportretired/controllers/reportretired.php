<?php

class Reportretired extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		

		$data['year'] = $this->input->get('year');
		
		$data['r'] = $this->staff_model->getByAgeExpried();
		
		$data['title'] = "รายงานเกษีณอายุแต่ละปี";

        $this->template->load("template/admin",'reportretired', $data);

	}

	public function print_pdf(){

		$data['year'] = $this->input->get('year');

		$data['r'] = $this->staff_model->getByAgeExpried();

		$data_r['html'] = $this->load->view('print',$data,true);
		
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['year'] = $this->input->get('year');

		$data['r'] = $this->staff_model->getByAgeExpried();
		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportretired.php */
/* Location: ./application/module/reportretired/reportretired.php */