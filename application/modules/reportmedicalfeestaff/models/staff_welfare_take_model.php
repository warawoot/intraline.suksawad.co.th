<?php
class Staff_welfare_take_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff_welfare_take';
            $this->_pk = 'takeID';
        }

        public function getAllOrg(){

        	$assignID = getAssignLasted();

        	$sql = "SELECT * FROM tbl_org_chart 
					where (upperOrgID = '0' OR upperOrgID = '1')
					AND assignID = $assignID";
				
			$r = $this->db->query($sql);
			return ($r->num_rows() > 0) ? $r : NULL;

        }

        public function getAll($org){
        	if(!empty($org)){
        		$sql = "SELECT  h.*
                        FROM tbl_staff_work c 
                        JOIN tbl_staff s ON c.staffID = s.ID
                        JOIN tbl_staff_welfare_take t on s.ID = t.StaffID
                        JOIN tbl_welfare_type wt on t.welTypeID = wt.welTypeID
        				JOIN tbl_welfare_fixed f on wt.welFixID = f.welFixID
        				JOIN tbl_welfare_take_hospital h on t.takeID = h.takeID
                        WHERE c.workStartDate = (SELECT max(workStartDate) from tbl_staff_work WHERE staffID=c.staffID) AND c.orgID = $org AND f.welFixID = '1'";
				$r = $this->db->query($sql);
				return ($r->num_rows() > 0) ? $r : NULL;
        	} 
        }

        public function getDetail($org){
        	if(!empty($org)){
        		$sql = "SELECT  *
                        FROM tbl_staff_work c 
                        JOIN tbl_staff s ON c.staffID = s.ID
                        JOIN tbl_staff_welfare_take t on s.ID = t.StaffID
                        JOIN tbl_welfare_type wt on t.welTypeID = wt.welTypeID
        				JOIN tbl_welfare_fixed f on wt.welFixID = f.welFixID
        				JOIN tbl_welfare_take_hospital h on t.takeID = h.takeID
        				JOIN tbl_patient_type pt on h.patTypeID = pt.patTypeID
        				LEFT JOIN tbl_staff_person p on t.personID = p.personID
                        WHERE c.workStartDate = (SELECT max(workStartDate) from tbl_staff_work WHERE staffID=c.staffID) AND c.orgID = $org AND f.welFixID = '1'";
				$r = $this->db->query($sql);
				return ($r->num_rows() > 0) ? $r : NULL;
        	} 

        }

       
        /*public function getAll(){
        	$sql = "SELECT *
        			FROM tbl_staff_welfare_take t
        			JOIN tbl_staff s ON t.StaffID = s.ID
        			JOIN tbl_welfare_type wt on t.welTypeID = wt.welTypeID
        			JOIN tbl_welfare_fixed f on wt.welFixID = f.welFixID
        			JOIN tbl_welfare_take_hospital h on t.takeID = h.takeID
        			JOIN tbl_patient_type pt on h.patTypeID = pt.patTypeID
        			LEFT JOIN tbl_staff_person p on t.personID = p.personID

        			where f.welFixID = '1'";
        	 $r = $this->db->query($sql);
	         return ($r->num_rows() > 0) ? $r : NULL;
        }*/
        /*public function getAll($day,$month,$year){
	         if(!empty($day) || !empty($month) || !empty($year)){

	            $year -= 543;
	        
	             $sql = "SELECT t.takeDate,s.staffID,s.staffPreName,s.staffFName,s.staffLName,h.takePrice
	                    FROM tbl_staff_welfare_take t
	                    JOIN tbl_staff s on t.staffID = s.ID
	                    JOIN tbl_welfare_take_hospital h on t.takeID = h.takeID
	                    WHERE t.welTypeID = '4' AND ";
	            if(!empty($day)){
	                $sql.=" DAY(takeDate) = '$day' AND ";
	            }
	            if(!empty($month)){
	                 $sql.=" MONTH(takeDate) = '$month' AND ";
	              
	            }
	            if(!empty($year)){
	                $sql.=" YEAR(takeDate) = '$year' AND ";
	            }
	            
	            $sql = substr($sql,0,-4);

	            $r = $this->db->query($sql);
	            return ($r->num_rows() > 0) ? $r : NULL;
	        }
	        
	    }*/
        

	
}
/* End of file staff_welfare_take_model.php */
/* Location: ./application/module/staffwelfaretake/models/staff_welfare_take_model.php */