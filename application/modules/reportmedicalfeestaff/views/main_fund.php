<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <!--<h3><?php //echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php //echo $staffName; ?></small></h3>

                 <a href="<?php //echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php //echo $this->config->item('txt_back')?></a>
                 -->
            </header>

            <div class="panel-body">

              <div class="col-md-12">
                <section class="panel">

                    <div class="panel-body profile-information">
                       <header class="panel-heading">
                         <h3>กองทุน</h3>

                      </header>
                      <div class="form-group">
                          <label class="control-label col-sm-12"> <h4>ข้อมูลพนักงาน</h4></label>
                          
                        </div>
                       <div class="col-md-3">
                           <div class="profile-pic text-center">
                               <img alt="" src="<?php echo site_url();?>uploads/<?php echo (!empty($rs)) ? $rs->staffImage : ""; ?>">
                           </div>
                       </div>
                       <div class="col-md-4">
                           <div class="profile-desk">
                               <div class="form-horizontal">

                                <?php 
                                    if(!empty($rs)){
                                      $staffID = $rs->staffID;
                                      $staffName = $rs->staffFName.' '.$rs->staffLName;
                                      $expDate = getExpiredDateByWork($rs->staffBirthday);
                                      $expContract = getExpiredDateByContract($rs->ID);
                                      $status = getWorkStatusByWork($rs->ID);
                                      $age = calAge($rs->staffBirthday);
                                      $age = !empty($age) ? $age.' ปี' : $age;
                                    }else{
                                      $staffID = "";
                                      $staffName = "";
                                      $expDate = "";
                                      $expContract = "";
                                      $status = "";
                                      $age = "";
                                    }
                                  ?>
                                  <div class="form-group"><label class="control-label col-sm-6">รหัสพนักงาน</label><div class="col-sm-6"><?php echo $staffID; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">ชื่อ - สกุล</label><div class="col-sm-6"><?php echo $staffName; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">วันที่เกษียณ ฯ</label><div class="col-sm-6"><?php echo $expDate; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">วันที่ครบกำหนดสัญญาจ้าง</label><div class="col-sm-6"><?php echo $expContract; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">สถานภาพการทำงาน</label><div class="col-sm-6"><?php echo $status; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">อายุพนักงาน</label><div class="col-sm-6"><?php echo $age; ?></div></div>
                                  
                                  
                               </div>
                           </div>
                       </div>
                        <div class="col-md-4">
                           <div class="profile-desk">
                               <div class="form-horizontal">

                                <?php 
                                    if(!empty($rs)){
                                      $positonName = getPositionByWork($rs->ID);
                                      $rankName = getRankByWork($rs->ID);
                                      $orgID1 = getOrg1ByWork($rs->ID);
                                      $orgID2 = getOrg2ByWork($rs->ID);
                                      $orgID = getOrgByWork($rs->ID);
                                      $age =  getWorkAgeByWork($rs->ID);
                                    }else{
                                      $positonName = "";
                                      $rankName = "";
                                      $orgID1 = "";
                                      $orgID2 = "";
                                      $orgID = "";
                                      $age = "";
                                    }
                                  ?>
                                  <div class="form-group"><label class="control-label col-sm-6">ตำแหน่ง</label><div class="col-sm-6"><?php echo $positonName; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">ระดับ</label><div class="col-sm-6"><?php echo $rankName; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">ฝ่าย / สำนักงาน</label><div class="col-sm-6"><?php echo $orgID1; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">กอง / กลุ่มงาน</label><div class="col-sm-6"><?php echo $orgID2; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">แผนก</label><div class="col-sm-6"><?php echo $orgID; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">อายุงานทั้งหมด</label><div class="col-sm-6"><?php echo $age; ?></div></div>
                                  
                                  
                               </div>
                           </div>
                       </div>
                       
                        <div class="form-group">
                          <label class="control-label col-md-12"> <h4>ข้อมูลกองทุน</h4></label>
                          
                        </div>
                       <?php if(!empty($rp)){ ?>
                        <div class="control-label col-md-2">อายุงานทั้งหมด : </div>
                          <div class="col-md-10">
                             <?php echo $age; ?>
                          </div>
                          <div class="control-label col-md-2">เงินเดือน : </div>
                          <div class="col-md-10">
                             <?php echo (!empty($rp)) ? "15,000" : ""; ?>
                          </div>
                          <div class="control-label col-md-2">จำนวนเงิน : </div>
                          <div class="col-md-10">
                             <?php echo (!empty($rp)) ? ($age*15000) : ""; ?>
                          </div>
                       <?php }else{ ?>
                          <div class="control-label col-md-2">วันที่สมัครกองทุน : </div>
                          <div class="col-md-10">
                             <?php echo (!empty($rf)) ? toBEDateThai($rf->applyDate) : ""; ?>
                          </div>
                        
                       
                          <div class="control-label col-md-2">อัตราสะสม : </div>
                          <div class="col-md-10">
                             <?php echo (!empty($rf)) ? $rf->collectRate." %" : ""; ?>
                          </div>
                        
                         
                          <div class="control-label col-md-2">อัตราสมทบ : </div>
                          <div class="col-md-10">
                             <?php echo (!empty($rf)) ? $rf->grantRate." %" : ""; ?>
                          </div>
                        
                         
                          <div class="control-label col-md-2">เงินเดือน : </div>
                          <div class="col-md-10">
                             <?php echo (!empty($rf)) ? "15,000" : ""; ?>
                          </div>

                          <div class="control-label col-md-2">จำนวนเงิน : </div>
                          <div class="col-md-10">
                             <?php echo (!empty($rf)) ? (($rf->collectRate/100)*15000) + (($rf->grantRate/100)*15000) : ""; ?>
                          </div>
                        <?php } ?>
                        
                  </div>
                </section>
            </div>
                 <!--<a class="btn btn-success" href="javascript:window.location='<?php //echo site_url();?>staffwork/add/?token=<?php //echo $id;?>'"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</a>
                -->
                <?php //echo $html; ?>
               <div class="xcrud-top-actions btn-group">
                   
                    <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>staffwelfarecheck'">ย้อนกลับ</a>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);


</script>
<style>
  .xcrud-search-toggle{
    position:absolute;
    top:-35px;
    left:70px;
  }
</style>