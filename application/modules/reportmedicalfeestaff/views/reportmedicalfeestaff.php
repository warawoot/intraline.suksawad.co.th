
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h5 style="text-align:center;">
		<br>
			รายงานประวัติการเบิกค่ารักษาพยาบาล <?php echo $th_month[$month].' ปี '.$year; ?>
		
	</h5>
	<br>
	ค้นหา 
	วัน
	<select style="width:80px" id="day">
		<option value="" <?php echo ($day == "") ? "selected" : ""; ?>>-- กรุณาเลือก --</option>
		<?php $count = $day_arr[intval($month)];

		for($i=1; $i<=$count; $i++){ ?>
			<option value="<?php echo $i; ?>" <?php echo ($i == $day) ? "selected" : ""; ?>><?php echo $i; ?></option>
		<?php } ?>
		
	</select>


	&nbsp;&nbsp;&nbsp;เดือน
	
	<select style="width:80px" id="month">
		<?php foreach($th_month as $key=>$val){ ?>
		<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;
	<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>

	
	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ฝ่าย</th>
			<th>กอง</th>
			<th >แผนก</th>
			<th >ยอดการเบิก</th>
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){
				$list = explode("/",$row);

				$rp = $this->staff_welfare_take_model->getAll($list[0]);
				$price = 0;
				if($rp != NULL){
					foreach($rp->result() as $rowp){
						$arrp = json_decode($rowp->takePrice);
						foreach($arrp as $p){
							$price+=$p->request;
						}
						
					}
				}



				echo '<tr>';
				if($list[2] == 0 || $list[2] == 1){
					echo '<td colspan="3">'.$list[1].'</td>';
				}else if($list[2] == 2){
					echo '<td></td>';
					echo '<td colspan="2">'.$list[1].'</td>';
				}else if($list[2] == 3){
					echo '<td colspan="2"></td>';
					echo '<td>'.$list[1].'</td>';
				}

				echo '<td><a href="'.site_url().'reportmedicalfeestaff/detail?id='.$list[0].'">'.number_format($price).'</a></td>';
				//echo '<td colspan="4">'.$list[1].' '.$list[0].' '.$list[2].'</td>';
				echo '</tr>';
			}
		}
		/*if($r != NULL){
			foreach($r->result() as  $row){ 
					$i++;

					$rp = $this->staff_welfare_take_model->getAll($row->orgID);
					$price = 0;
					if($rp != NULL){
						foreach($rp->result() as $rowp){
							$arrp = json_decode($rowp->takePrice);
							foreach($arrp as $p){
								$price+=$p->request;
							}
							
						}
					}
					
				?>
				 <tr >
					<td><?php echo $row->orgName.' '.$row->orgID; ?></td>
					<td>
						<?php $rp = $this->staff_welfare_take_model->getOrg1($row->orgID); ?>

					</td>
					<td></td>
					<td><?php echo number_format($price); ?></td>
				</tr>
		<?php  } }else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}*/?>
		
		
	</table>
	</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function search(){
        	window.location='<?php echo site_url();?>reportmedicalfeestaff?day='+$('#day :selected').val()+'&month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
        }


        $('#month').change(function(){
        	day_arr = ["","31","29","31","30","31","30","31","31","30","31","30","31"];  
   			day = $('#day :selected').val();
        	month = $('#month :selected').val();
        	count = day_arr[parseInt(month)];
        	sel = (day == "") ? 'selected' : '';
        	html = "<option value='' "+sel+">-- กรุณาเลือก --</option>";

			for(i=1; i<=count; i++){ 

				sel = (day == i) ? 'selected' : '';
				html+='<option value="'+i+'" '+sel+'>'+i+'</option>';
			 } 
			 $("#day").html(html);
        })
        

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportmedicalfeestaff/print_pdf?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&day='+$('#day :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportmedicalfeestaff/print_excel?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&day='+$('#day :selected').val());
        }

</script>