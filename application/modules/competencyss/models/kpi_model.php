<?php

class Kpi_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	}
        
   function rate_eval_doc($evalRound,$evalType){  
        $eval_get			=	explode(",", $evalRound);
        $this->db->select('*');
        $this->db->from('`tbl_eval_competency_date` as t1');
        $this->db->where('t1.evalRound' ,$eval_get[0]);
		$this->db->where('t1.evalYear' ,$eval_get[1]);
		$this->db->where('t2.evalType' ,$evalType);
        $this->db->join('tbl_eval_form as t2 ' , 't1.evalRound = t2.evalRound','inner');
        $query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
            
    }
	
	
	function rate_emp_doc($staffID){  
        
        $this->db->select('t2.staffID, t2.ID, t2.orgID   , t2.staffPreName ,`t2`.`staffFName`, `t2`.`staffLName`, `t2`.`staffImage` ,t3.positionName');
        $this->db->from('tbl_staff_work as t1');
        $this->db->where('t2.ID' ,$staffID);
        $this->db->join('tbl_staff as t2 ' , 't2.ID = t1.staffID','inner');
        $this->db->join('tbl_position as t3' ,'t3.positionID = t1.positionID' ,'left');
        $query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
            
    }
	
	function get_eval_group_subject($evalFormID,$evalType){
		
		$this->db->select('*');
        $this->db->from('`tbl_ec_group_subject` as t1 ');
        $this->db->join('tbl_eval_competency_form as t2' , 't1.evalFormID = t2.evalFormID','inner');
		//$this->db->join('tbl_eval_subject as t3' , 't1.evalGroupSubjectID = t3.evalGroupSubjectID','inner');
		$this->db->where('t2.evalFormID' ,$evalFormID);
		$this->db->where('t2.evalType' ,$evalType);
        $query 		= $this->db->get();
 		$data 		= $query->result(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
 		
	}
	
	function get_eval_notes($empID,$evalFormID,$evalType){
        $this->db->select('*');
        $this->db->from('tbl_eval_notes_kpi as t1');
        $this->db->where('t1.empID ' ,$empID); 
		$this->db->where('t1.evalFormID ' ,$evalFormID); 
		$this->db->where('t1.evalType ' ,$evalType); 
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
        
    }
	
	function structural_work($upperOrgID,$status,$assignID){
		
		//echo $status;
		
		if($status == 'department'){   // แผนก
					$query = $this->db->query('
										 SELECT * FROM tbl_org_chart as t1  WHERE (t1.orgID = "'.$upperOrgID.'" and t1.assignID = "'.$assignID.'" )    '
	      							);
		}elseif($status == 'pile'){ // กอง
					$query = $this->db->query('
										 SELECT * FROM tbl_org_chart as t1  WHERE (t1.orgID = "'.$upperOrgID.'" and t1.assignID = "'.$assignID.'" )    '
	      							);
		}elseif($status == 'party'){ // ฝ่าย
				  $query = $this->db->query('
										 SELECT * FROM tbl_org_chart as t1  WHERE (t1.orgID = "'.$upperOrgID.'" and t1.assignID = "'.$assignID.'" )    '
	      							);
		}
		
 		$data       = $query->row_array(); 
  		 
		return ($query->num_rows() > 0) ? $data : NULL; 
		
		
	}
	
	function structural_staff($staffID){
	
		$query = $this->db->query('
										SELECT t1.staffID,t1.orgID FROM `tbl_staff_work` as t1 WHERE t1.staffID = '.$staffID.' '
	      							);
		$data       = $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL; 
	
	}
	
	
	function get_sum_eval_subject($evalGroupSubjectID){
		
		$this->db->select_sum('t1.maxScore');
        $this->db->from('`tbl_ec_subject` as t1');
 		$this->db->where('t1.evalGroupSubjectID' ,$evalGroupSubjectID);
        $query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
 	
	}
	
	
	function get_eval_subject($evalGroupSubjectID){
		
		$this->db->select('*');
        $this->db->from('`tbl_ec_subject` as t1');
 		$this->db->where('t1.evalGroupSubjectID' ,$evalGroupSubjectID);
        $query 		= $this->db->get();
 		$data 		= $query->result(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
 	
	}
	
	function get_eval_edit($empID,$evalFormID,$evalSubjectID){
        $this->db->select('*');
        $this->db->from('tbl_eval_competency as t1');
        $this->db->where('t1.empID' ,$empID);
        //$this->db->where('t1.evalFormID ' ,$evalFormID); 
		//$this->db->where('t1.evalSubjectID ' ,$evalSubjectID); 
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
        
    }
	
	
		
	function get_dropdown_all($id , $name ,$table, $where){
 				$this->db->select(' * ');
				$this->db->order_by($id, "asc");
				$this->db->from($table);
				$this->db->where($where);
 				$query 		= $this->db->get();
				$dropdowns	= $query->result();
  				
				if ($query->num_rows() > 0){
					foreach($dropdowns as $dropdown){
						//echo $fruit = array_shift($dropdown);
						$dropdownlist['']	= "กรุณาระบุด้วย";
						$dropdownlist[$dropdown->$id]	= $dropdown->$name;
 					}	
				}else{
 						$dropdownlist['']	= " กรุณาระบุด้วย";
 				} 
 				
				$finaldropdown	= $dropdownlist;
				return $finaldropdown;
		}
	
		
	
}
/* End of file hospital_model.php */
/* Location: ./application/module/hospital/hospital_model.php */