 <?php

class Competencyss extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('kpi_model');
    }
 

    public function index() {
         
		 $eval				= $this->input->get_post('eval'); // evalRound
		 $staffID			= $this->input->get_post('staffID'); 
		 $var_agencies	    = $this->input->get_post('var_agencies'); 
 		 
		 if($staffID  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 if($eval  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 $evalFormID		=	'';
		 $evalSubjectID		=	'';
		 $eval_doc			= $this->kpi_model->rate_eval_doc($eval,1); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		 $eval_emp	= $this->kpi_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		 ## แสดงส่วนของประเมิน กลุ่ม ##
		 
		 $data['get_eval_group_subject'] = $this->kpi_model->get_eval_group_subject($data['get_evalFormID'],1);  
		 
		 ## Note
		 //$data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID);
		 $data['get_eval_notes']	= $this->kpi_model->get_eval_notes($staffID,$eval_doc['evalFormID'],1);
		 
		 $data['CoreCompetency']		= $this->kpi_model->get_dropdown_all('ecID' , 'ecText' ,'tbl_eval_subject_competency','ecType = 1'); 
		 $data['FunctionalCompetency']	= $this->kpi_model->get_dropdown_all('ecID' , 'ecText' ,'tbl_eval_subject_competency','ecType = 2'); 
		 $data['get_eval_com']			= $this->kpi_model->get_eval_edit($staffID,$evalFormID,$evalSubjectID);
		 
		 
		 
 		 $data['eval']				= $eval; 
		 $data['staffID']			= $staffID; 
		 $data['var_agencies']		= $var_agencies; 
		 
         $this->template->load('template/admin', 'rate_doc_test',$data);
    }
     
   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>