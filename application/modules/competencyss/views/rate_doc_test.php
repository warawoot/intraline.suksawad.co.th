<style type="text/css">
	.form-control {
 	  color: #343232;
	}
  
  .table-evaluation tr td{
    padding:7px !important;
    vertical-align: middle !important;
  }
  .table-evaluation thead th{
  border-top: none; 
  border-bottom: none;
  border: 0 !important; 
  }  
    input[type=number] {
      float: left;
      width: 70px;
      height: 35px;
      padding: 0;
      font-size: 1.2em;
      text-transform: uppercase;
      text-align: center;
      color: #93504C;
      border: 2px #93504C solid;
      background: none;
      outline: none;
      pointer-events: none;
    }
    
    span.spinner {
      position: absolute;
      height: 40px;
      margin-top: -9px;
      user-select: none;
      -ms-user-select: none;
      -moz-user-select: none;
      -webkit-user-select: none;
      -webkit-touch-callout: none;
    }
    
    span.spinner > .sub,
    span.spinner > .add {
      float: left;
      display: block;
      width: 35px;
      height: 35px;
      text-align: center;
      font-family: Lato;
      font-weight: 700;
      font-size: 1.2em;
      line-height: 33px;
      color: #93504C;
      border: 2px #93504C solid;
      border-right: 0;
      border-radius: 2px 0 0 2px;
      cursor: pointer;
      transition: 0.1s linear;
      -o-transition: 0.1s linear;
      -ms-transition: 0.1s linear;
      -moz-transition: 0.1s linear;
      -webkit-transition: 0.1s linear;
    }
    
    span.spinner > .add {
      top: 0;
      border: 2px #93504C solid;
      border-left: 0;
      border-radius: 0 2px 2px 0;
    }
    
    span.spinner > .sub:hover,
    span.spinner > .add:hover {
      background: #93504C;
      color: #25323B;
    }
     input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
     -webkit-appearance: none;
    }

</style>
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading"> 
          <h3>ข้อมูลการประเมินผล competencys</h3>
      </header>
      <div class="panel-body">
        <form  id="myForm" method="post" action="<?php echo base_url() ?>ratecompetencys/docs" enctype="multipart/form-data">
          <table class="table table-striped" style="background-color: #fff; font-size:14px;">
            <tr>
              <td>
                <table width="100%" border="0">
                  <tr>
                    <td colspan="7" align="center" style=" font-size:16px;">
                      <strong> แบบประเมินผลการปฏิบัติงานประจำปี </strong> 
						          <?php echo $get_evalYear;?> <strong> ครั้งที่ </strong>
						          <?php echo $get_evalRound;?> ( <strong>วันที่</strong> <?php echo $get_startDate;?> - <?php echo $get_endDate;?>) 
                      <input name="evalType" id="evalType" type="hidden" value="1" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7" align="center"><strong> [ </strong><?php echo $get_evalNameText;?><strong> ] </strong></td>
                  </tr>
                  <tr>
                    <td><strong>ชื่อ-นามสกุล</strong> <?php  echo $get_staffPreName . $get_staffFName .'&nbsp;&nbsp;'. $get_staffLName;?></td>
                    <td></td>
                    <td>&nbsp;</td>
                    <td><strong>ตำแหน่ง<?php echo $get_positionName;?></strong></td>
                    <td></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php
 				  
                  		 $party			= $this->kpi_model->structural_work($var_agencies,'party',6);
  						 $staffIDz		= $this->kpi_model->structural_staff($staffID);
						// echo $staffIDz['orgID'];
  						 $department	= $this->kpi_model->structural_work($staffIDz['orgID'],'department',6);
						// echo 'department : '.$department['upperOrgID'];
  						 $pile			= $this->kpi_model->structural_work($department['upperOrgID'],'pile',6);
						 
				  ?>
                  <tr>
                    <td><strong><!--แผนก-->
                    <?php  echo $department['orgName']; ?></strong></td>
                    <td>&nbsp; </td>
                    <td>&nbsp;</td>
                    <td><strong><!--กอง-->
                    <?php  echo $pile['orgName']; ?></strong></td>
                    <td>&nbsp;</td>
                    <td><strong><!--ฝ่าย--></strong></td>
                    <td>&nbsp;<strong><?php   echo $party['orgName']; ?></strong></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" >
                  <tbody>
                      <thead>
                       <tr>
                          <td><strong>Core Competency</strong></td> 
                          <td><strong>ระดับที่คาดหวัง</strong></td>
                          <td><strong>คะแนนที่ได้</strong></td>
                          <td align="center"><strong>Competency Gap</strong></td>
                       </tr>
                      </thead>
                     <?php
                     	for($i = 1; $i<=5;$i++){
					 ?> 
                      <tr>
                        <td>
                        <?php
                        		echo form_dropdown('CoreCompetency[]',$CoreCompetency,($get_eval_com['ecID']!='')?$get_eval_com['ecID']:'','class="form-control"');
						?> 
                        </td>
                        <td><input type="text" class="form-control" id="c_expectedlevel[]" name="c_expectedlevel[]" placeholder="ระดับที่คาดหวัง" value="<?php echo ($get_eval_com['ecExpectScore']!='')?$get_eval_com['ecExpectScore']:'' ;?>"></td>
                        <td><input type="text" class="form-control" id="c_score[]" name="c_score[]" placeholder="คะแนนที่ได้" value="<?php echo ($get_eval_com['ecScore'] !='')?$get_eval_com['ecScore']:'' ;?>"></td>
                        <td align="center">&nbsp; </td>
                      </tr>
                     <?php } ?>
                      <tr>
                        <td><strong>Function Competency</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                     <?php
                     	for($i = 1; $i<=5;$i++){
					 ?> 
                      <tr>
                        <td> 
						<?php
                        		echo form_dropdown('FunctionalCompetency[]',$FunctionalCompetency,($get_eval_com['ecID']!='')?$get_eval_com['ecID']:'','class="form-control"');
						?>
                         </td>
                        <td><input type="text" class="form-control" id="f_expectedlevel[]" name="f_expectedlevel[]" placeholder="ระดับที่คาดหวัง" value="<?php echo ($get_eval_com['ecExpectScore'] !='')?$get_eval_com['ecExpectScore']:'';?>"></td>
                        <td><input type="text" class="form-control" id="f_score[]" name="f_score[]" placeholder="คะแนนที่ได้" value="<?php echo ($get_eval_com['ecScore'] !='')?$get_eval_com['ecScore']:''; ?>"></td>
                        <td align="center">&nbsp; </td>
                      </tr>
                     <?php
						}
					 ?>
           	    </table>
              </td>
          </tr> 
         </table>
       <div style="display:none">
            <input type="text" name="eval" id="eval" value="<?php echo $eval;?>" />
            <input type="text" name="staffID" id="staffID" value="<?php echo $staffID;?>" />
            <input type="text" name="get_evalFormID" id="get_evalFormID" value="<?php echo $get_evalFormID;?>" /> 
            <input type="text" name="var_agencies" id="var_agencies" value="<?php echo $var_agencies;?>" />
        </div>

        <button type="submit" class="btn btn-success">บันทึก</button>
        &nbsp;
        <button type="button" class="btn btn-warning">ยกเลิก</button>


      </form>
    </div>
     
  </section>
  </div>
</div>
<script type="text/javascript"> 
  

  function calculateTotalScore(){
      var totalScore = 0; 
       $(".spinner").find('input[type=number]').map(function(){
          totalScore += parseInt(this.value);
      }).get();
      $('#totalScore').html(totalScore);
      updateLevel(totalScore);
  }

  var nextLevelScore = -1;
  var prevLevelScore = 0;
  var lookingForLevel = false;
  function updateLevel(totalScore){
    if((totalScore > nextLevelScore || totalScore < prevLevelScore) && !lookingForLevel){
      $("#evalResults").html('loading ...');
      lookingForLevel = true;
      $.getJSON( "<?php echo base_url(); ?>ratecompetencys/getScoreAjax", {total:totalScore}, function( data ) {
        prevLevelScore = parseInt(data.minScore);
        nextLevelScore = parseInt(data.maxScore);
        $("#evalResults").html('<strong>ระดับ ' + data.levelText + ' &nbsp;&nbsp;&nbsp;&nbsp;( </strong>' + data.minScore + ' - ' + data.maxScore + ' <strong>)</strong>');
      })
      .fail(function(jqXHR, textStatus, errorThrown) { $("#evalResults").html("Error getting results"); })
      .always(function() { lookingForLevel = false; });
    }

  }



(function($) {
    $.fn.spinner = function() {
		this.each(function() {
		var el = $(this);
		
		// add elements
		el.wrap('<span class="spinner"></span>');     
		el.before('<span class="sub">-</span>');
		el.after('<span class="add">+</span>');
		
		// substract
		el.parent().on('click', '.sub', function () {
		if (el.val() > parseInt(el.attr('min')))
		  el.val( function(i, oldval) { return --oldval; });
      	  calculateTotalScore();
		});
		
		// increment
		el.parent().on('click', '.add', function () {
		if (el.val() < parseInt(el.attr('max')))
		  el.val( function(i, oldval) { return ++oldval; });
      calculateTotalScore();
		});
			});
		};
		})(jQuery);
		
	 $('input[type=number]').spinner();
   calculateTotalScore();

  //The only script I found to scroll with offset on html5 error
  var delay = 0;
  var offset = 80;

  document.addEventListener('invalid', function(e){
     $(e.target).addClass("invalid");
     $('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset }, delay);
  }, true);
  document.addEventListener('change', function(e){
     $(e.target).removeClass("invalid")
  }, true);


</script>
 