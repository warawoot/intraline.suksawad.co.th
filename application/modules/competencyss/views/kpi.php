<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล</h3>
            </header>
            <div class="panel-body">
              <form  id="myForm" method="post" action="<?php echo base_url(); ?>rate/demo" enctype="multipart/form-data">
                <table class="table" style="background-color: #fff; font-size:14px;">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td colspan="7" align="center" style=" font-size:16px;"><strong>แบบประเมินสมรรถนะ  (Competency Assessment ) สำหรับระดับปฏิบัติการ
</strong>
<br/>
<br/>
ประจำปีงบประมาณ 2558 <strong> ครั้งที่ </strong>1 ( <strong>วันที่</strong> 1 สิงหาคม 2558 - 31 สิงหาคม 2558) </td>
                      </tr>
                      <tr>
                        <td><strong>ชื่อ-นามสกุล</strong></td>
                        <td>นางโกวิทย์&nbsp;&nbsp;นิธิชัย</td>
                        <td>&nbsp;</td>
                        <td><strong>ตำแหน่ง</strong></td>
                        <td>นักวิชาการ</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>แผนก</strong></td>
                        <td>&nbsp; </td>
                        <td>&nbsp;</td>
                        <td><strong>กอง</strong></td>
                        <td>&nbsp; </td>
                        <td><strong>ฝ่าย</strong></td>
                        <td>&nbsp; </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td><strong>สมรรถนะประจำตำแหน่ง (Job Competency)</strong></td>
                        <td><strong>คะแนนที่คาดหวัง</strong></td>
                        <td><strong>ผลการประเมิน</strong>&nbsp;</td>
                        <td><strong>Competency Gap</strong>&nbsp;</td>
                      </tr>
                                            <tr>
                        <td><strong>1. Core Competency</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_10" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;1.1 ปริมาณงานที่ทำได้ตามที่ได้รับมอบหมาย</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_10" id="score_max_min_10" class="form-control" 
                        onChange="changeTest_10(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                  </select>
							  <script>
                                 function changeTest_10(obj){
                                       var inputName = 'score_max_min_10';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_10	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_10); 
										//$('#list_score_1_10').html(score_10); 
										//test(10);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_1_10"></span>
                              </div>
                          </td>
                        <td align="right">5</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_11" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;1.2 คุณภาพของผลงาน (ความถูกต้อง/ครบถ้วน/ความประณีต)</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_11" id="score_max_min_11" class="form-control" 
                        onChange="changeTest_11(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                  </select>
							  <script>
                                 function changeTest_11(obj){
                                       var inputName = 'score_max_min_11';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_11	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_11); 
										//$('#list_score_1_11').html(score_11); 
										//test(11);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_12" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;1.3  ระยะเวลาที่ใช้ (ความรวดเร็ว/ทันเวลา)</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_12" id="score_max_min_12" class="form-control" 
                        onChange="changeTest_12(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                  </select>
							  <script>
                                 function changeTest_12(obj){
                                       var inputName = 'score_max_min_12';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_12	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_12); 
										//$('#list_score_1_12').html(score_12); 
										//test(12);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_1_12"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_13" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;1.4  ทรัพยากรที่ใช้ (การประหยัด/ความคุ้มค่าของการใช้ทรัพยากร)</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_13" id="score_max_min_13" class="form-control" 
                        onChange="changeTest_13(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                  </select>
							  <script>
                                 function changeTest_13(obj){
                                       var inputName = 'score_max_min_13';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_13	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_13); 
										//$('#list_score_1_13').html(score_13); 
										//test(13);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_1_13"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>

                                                      <div class="list_count">
                                <span id="list_count_1"></span>
                              </div>
                              <span id="list_count_test_1"></span>
           					                         <tr>
                        <td><strong>2. Functional Competency</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_15" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2.1 ความรู้ความสามารถทักษะในงาน (ความรอบรู้ในงาน</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_15" id="score_max_min_15" class="form-control" 
                        onChange="changeTest_15(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                      <option value="24">24</option>
                                                      <option value="25">25</option>
                                                      <option value="26">26</option>
                                                      <option value="27">27</option>
                                                      <option value="28">28</option>
                                                      <option value="29">29</option>
                                                      <option value="30">30</option>
                                                      <option value="31">31</option>
                                                      <option value="32">32</option>
                                                      <option value="33">33</option>
                                                      <option value="34">34</option>
                                                      <option value="35">35</option>
                                                      <option value="36">36</option>
                                                      <option value="37">37</option>
                                                      <option value="38">38</option>
                                                      <option value="39">39</option>
                                                      <option value="40">40</option>
                                                  </select>
							  <script>
                                 function changeTest_15(obj){
                                       var inputName = 'score_max_min_15';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_15	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_15); 
										//$('#list_score_2_15').html(score_15); 
										//test(15);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_2_15"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_16" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2.2 การตัดสินใจ การแก้ปัญหา</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_16" id="score_max_min_16" class="form-control" 
                        onChange="changeTest_16(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                      <option value="24">24</option>
                                                  </select>
							  <script>
                                 function changeTest_16(obj){
                                       var inputName = 'score_max_min_16';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_16	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_16); 
										//$('#list_score_2_16').html(score_16); 
										//test(16);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_2_16"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_17" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2.3 การทำงานเป็นทีม</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_17" id="score_max_min_17" class="form-control" 
                        onChange="changeTest_17(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                      <option value="24">24</option>
                                                      <option value="25">25</option>
                                                      <option value="26">26</option>
                                                  </select>
							  <script>
                                 function changeTest_17(obj){
                                       var inputName = 'score_max_min_17';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_17	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_17); 
										//$('#list_score_2_17').html(score_17); 
										//test(17);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_2_17"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_18" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2.4 การประสานงานกับหน่วยงานภายในและภายนอกองค์กร</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_18" id="score_max_min_18" class="form-control" 
                        onChange="changeTest_18(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                  </select>
							  <script>
                                 function changeTest_18(obj){
                                       var inputName = 'score_max_min_18';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_18	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_18); 
										//$('#list_score_2_18').html(score_18); 
										//test(18);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_2_18"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                                                      <div class="list_count">
                                <span id="list_count_2"></span>
                              </div>
                              <span id="list_count_test_2"></span>
                                                      <div class="list_count">
                                <span id="list_count_4"></span>
                              </div>
                              <span id="list_count_test_4"></span>
           					     
                        <div class="score_max" style="display:none;">
                        	<span id="get_score_max">4</span>
                            
                        </div>
                        
                                              
                      <tr>
                        <td colspan="3" align="right">
                        	<!--<div class="ge_score">
                            	รวมคะแนนประเมิน = <span id="put_sum_score"></span> คะแนน
                        	</div>-->
                         </td>
                      </tr>
                    </table></td>
                  </tr>
                   
                  <tr>
                    <td align="right"><div style="display:none">
                    	<input type="text" name="eval" id="eval" value="1" />
                        <input type="text" name="staffID" id="staffID" value="23015" /></div>
                    	<button type="submit" class="btn btn-success">ขั้นตอนถัดไป &gt;&gt;</button>
                      	&nbsp;
                      	<button type="button" class="btn btn-warning">ยกเลิก</button>
                     	<!--<input name="eval" id="eval" type="hidden" value="" /> -->
                    </td>
                  </tr>
                </table>
              </form>
            </div>
             
        </section>
    </div>
</div>