<?php

class Staffgraduation extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_graduation')->where('staffID =',$id);
		
		$xcrud->relation('graduationName','tbl_education','eduID','eduName');
		$xcrud->relation('graduationDegree','tbl_edu_degree','eduDegreeID','eduDegreeName');
		$xcrud->relation('graduationFaculty','tbl_edu_faculty','eduFactID','eduFactName');
		$xcrud->relation('graduationDepartment','tbl_edu_department','eduDepartmentID','eduDepartmentName');
		$xcrud->relation('graduationUniversity','tbl_edu_place','eduPlaceID','eduPlaceName');
		
		//// List /////
		$col_name = array(
			'graduationName' => 'ระดับการศึกษา',
			'graduationDegree' => 'ปริญญา / วุฒิการศึกษา',
			'graduationDepartment' => 'สาขา',
			'graduationFaculty' => 'คณะ / สายการเรียน',
			'graduationUniversity' => ' สถานศึกษา',
			'graduationSYear' => 'ปีที่เริ่มการศึกษา',
			'graduationEYear' => 'ปีที่สำเร็จการศึกษา',
			'graduationGPA' => 'เกรดเฉลี่ย',
			'graduationType' => ''

		);

		$xcrud->columns('graduationSYear,graduationEYear,graduationName,graduationFaculty,graduationDepartment');
		$xcrud->label($col_name);
		$xcrud->column_width('graduationSYear','15%')->column_width('graduationEYear','15%')->column_width('graduationName','20%')->column_width('graduationFaculty','20%');
		//$xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
		
		// End List//

		//// Form //////
		$xcrud->pass_var('staffID',$id);
		$xcrud->fields('graduationName,graduationDegree,graduationUniversity,graduationFaculty,graduationDepartment,graduationSYear,graduationEYear,graduationGPA,graduationType');
		$xcrud->change_type('graduationType','radio','1',array('1'=>'วุฒิการศึกษาแรกบรรจุ','2'=>'วุฒิการศึกษาจบจริง','3'=>'วุฒิการศึกษาปัจจุบัน'));
		for($i=date("Y"); $i>=(date("Y")-50); $i--){
			$year[$i] = $i+543;
		}
		
		$xcrud->change_type('graduationSYear','select','',$year);
		$xcrud->change_type('graduationEYear','select','',$year);
		//$xcrud->validation_pattern('graduationSYear,graduationEYear','numeric');
		
		$xcrud->validation_required('graduationName,graduationDegree,graduationUniversity');

		// End Form//
		
		$data['html'] = $xcrud->render();
		
		
		$data['id'] = $id;
		$data['title'] = "การศึกษา";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

}
/* End of file staffgraduation.php */
/* Location: ./application/module/staffgraduation/staffgraduation.php */