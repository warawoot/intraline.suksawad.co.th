<?php

class Dashboard extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('dashboard_model');		
		$this->load->model('absence/absence_model');		
		$this->load->model('reportdaily/reportdaily_model');		
		$this->load->model('organization/organization_model');		
	}
	
	public function index(){
		if($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==4)
		{
			$data['rBirth'] = $this->dashboard_model->getByBirthday();
			$data['rLeave'] = $this->dashboard_model->getLeave(date("Y-m-d"),date("Y-m-d"));
			$agencies = $this->input->get_post('agencies');
			$datestr = $this->input->get_post('datestr');

	        if($datestr == "") {
	            $datestr = date("d/m/").(date("Y")+543);
	        } 

	        $data['datestr'] = $datestr;
			$date_start 		= explode('/',$datestr); //สร้างตัวแปรมาเก็บวันที่
			$strYear_s 			= $date_start[2]-543;
			$strMonth_s			= $date_start[1];
			$strDay_s			= $date_start[0];
	        $strDate_s 		    = $strYear_s."-".$strMonth_s."-".$strDay_s;  

	        $orgIDManage = $this->session->userdata('orgIDManage');
	        if(empty($this->input->get('orgID'))){
	        	$selected= $orgIDManage;
	        	$dataOrg = $this->dashboard_model->getOrgDashborad($orgIDManage);
	        }else{
	        	$selected= $this->input->get('orgID');
	        	$dataOrg = $this->dashboard_model->getOrgDashborad($this->input->get('orgID'));
	        }
				
			// คนที่ลาภายในวันนี้ แยกตามแผนก
			if(!empty($dataOrg)) {
			$tableReportLervers = "<table class='table table-striped'>
									<tr>
										<th class='thlight'>รายชื่อพนักงาน (แยกตามแผนก)</th>
									</tr>";
			foreach ($dataOrg as $row) {
				$dataLeaves = "";
				$tableLeavebr = "<tr>
									<td><b>".$row->orgName."</b></td> 
								  </tr>";
	            $ag = $row->orgID.$this->organization_model->getSubOrg($row->orgID);
	            $tableLeavebr .= "<tr><td>";

				$dataLeaves = $this->dashboard_model->getLeaveDb($strDate_s,$ag); 
				
	            if(!empty($dataLeaves)) {
					$tableLeavebr .=  "<table class='table table-striped'>
											<tr class='lighttr'>
												<th width='30%'>ชื่อ</th>
												<th width='25%'>หน่วยงาน</th>
												<th width='25%'>ประเภทการลา</th>
												<th width='20%'>วันที่ลา</th>
											</tr>
										";
					foreach ($dataLeaves as $rowLeavebr) {
						 if ($rowLeavebr->staffNickName != "") {
						 	$Nickle = "(".$rowLeavebr->staffNickName.")" ; 
						 }else{
							$Nickle	= '';
						 } 

						 if ($rowLeavebr->name != NULL ) {
							 $Names = $rowLeavebr->name ;
						 }else{
						 	 echo '<tr><td colspan="5" style="text-align:center">ไม่พบข้อมูล</td></tr>';
						 }

						 $filename = site_url().'assets/staff/'.$rowLeavebr->staffID.'.jpg';
                            if (false!==file($filename)) {
                                $Proto = '<img src="'.$filename.' " class="portrait" style="width:50px">';
                            } else {
                                $Proto = '<img src="'.base_url().'./assets/staff/default-user.png" class="portrait" style="width:50px">';   
                            }

						$tableLeavebr .=  "<tr>
												<td>".$Proto." ".$Names." ".$Nickle." </td>
												<td>".$rowLeavebr->orgName."</td>
												<td>".$rowLeavebr->absName."</td>
												<td>".toFullDate($rowLeavebr->absStartDate,'th','full')." ถึง <br> ".toFullDate($rowLeavebr->absEndDate,'th','full')."</td>
											  </tr>";
					}
					$tableLeavebr .=  "</table>";
					$tableLeavebr .= "</td></tr>";
					$tableReportLervers .= $tableLeavebr;
				}

			}
			$tableReportLervers .= "</table>";
			$data['tableReportLervers'] = $tableReportLervers;
		}      



	        // คนที่เกิดภายใน 5 วันนี้ แยกเป็นแผนก
			if(!empty($dataOrg)) {
			$tableReportBir = "<table class='table table-striped'>
									<tr>
										<th class='thlight'>รายชื่อพนักงาน (แยกตามแผนก)</th>
									</tr>";
			foreach ($dataOrg as $row) {
				$dataBir = "";
				$tableLeaveOrg = "<tr>
									<td><b>".$row->orgName."</b></td> 
								  </tr>";
	            $ag = $row->orgID.$this->organization_model->getSubOrg($row->orgID);
	            $tableLeaveOrg .= "<tr><td>";

				$dataBir = $this->dashboard_model->getbir($strDate_s,$ag); 
				
	            if(!empty($dataBir)) {
					$tableLeaveOrg .=  "<table class='table table-striped'>
											<tr class='lighttr'>
												<th width='37%'>ชื่อ</th>
												<th width='38%'>หน่วยงาน</th>
												<th width='25%'>วันเกิด</th>
											</tr>
										";
					foreach ($dataBir as $rowLeave) {
						 if ($rowLeave->staffNickName != "") {
						 	$Nick = "(".$rowLeave->staffNickName.")" ; 
						 }else{
							$Nick	= '';
						 } 

						 if ($rowLeave->name != NULL ) {
							 $Namebir = $rowLeave->name ;
						 }else{
						 	 echo '<tr><td colspan="5" style="text-align:center">ไม่พบข้อมูล</td></tr>';
						 }

						 $filename = site_url().'assets/staff/'.$rowLeave->staffID.'.jpg';
                            if (false!==file($filename)) {
                                $Proto = '<img src="'.$filename.' " class="portrait" style="width:50px">';
                            } else {
                                $Proto = '<img src="'.base_url().'./assets/staff/default-user.png" class="portrait" style="width:50px">';   
                            }

						$tableLeaveOrg .=  "<tr>
												<td>".$Proto." ".$Namebir." ".$Nick." </td>
												<td>".$rowLeave->orgName."</td>
												<td>".toFullDate($rowLeave->staffBirthday,'th','full')."</td>
											  </tr>";
					}
					$tableLeaveOrg .=  "</table>";
					$tableLeaveOrg .= "</td></tr>";
					$tableReportBir .= $tableLeaveOrg;
				}

			}
			$tableReportBir .= "</table>";
			$data['tableReportBir'] = $tableReportBir;
		}      

			
 			$this->template->load('template/admin','adminview',$data);			
		}
		else
		{	
			$gs1 = $this->absence_model->getTotalLeave(date('Y'),$this->session->userdata('ID'),'1,8')->TotalLeave;
			$gs2 = $this->absence_model->getTotalLeave(date('Y'),$this->session->userdata('ID'),2)->TotalLeave; 
			$gs7 = $this->absence_model->getTotalLeave(date('Y'),$this->session->userdata('ID'),7)->TotalLeave; 
			$absTotal = $this->absence_model->getTotalLeave(date('Y'),$this->session->userdata('ID'))->TotalLeave; 
			$absOther = $absTotal - ($gs1 + $gs2 + $gs7);


			$data = array(
				'gs1' => $gs1,
				'gs2' => $gs2,
				'gs7' => $gs7,
				'absOther' => $absOther
			);
			/*
			echo "<pre>";
			print_r($data);
			echo "</pre>";
	*/

			$data['ac1'] = $this->absence_model->getAbsenceQuota(1);
			$data['ac2'] = $this->absence_model->getAbsenceQuota(2);
			$data['ac7'] = $this->absence_model->getAbsenceQuota(7);
			$data['sa1'] = $this->absence_model->getStaffAbsence(date('Y'),$this->session->userdata('ID'),1);
			$data['sa2'] = $this->absence_model->getStaffAbsence(date('Y'),$this->session->userdata('ID'),2);
			$data['sa7'] = $this->absence_model->getStaffAbsence(date('Y'),$this->session->userdata('ID'),7);
			$data['sao'] = $this->absence_model->getStaffAbsence(date('Y'),$this->session->userdata('ID'));

			$data['abs'] = $this->absence_model->getStaffAbsenceData(date('Y'),'',$this->session->userdata('ID'));
 			$this->template->load('template/admin','userview',$data);		
		}
	}
}
/* End of file dashboard.php */
/* Location: ./application/module/dashboard/dashboard.php */