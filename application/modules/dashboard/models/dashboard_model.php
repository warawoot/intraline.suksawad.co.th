<?php
class Dashboard_model extends MY_Model{

    function __construct() {    	
        parent::__construct();
        $this->_table = 'tbl_user';
        $this->_pk = 'userID';
    }

    public function authPassword($username,$password){
        $r = $this->db->get_where($this->_table,array('userName'=>$username,'userCredential'=>md5($password)));
        return ($r->num_rows() > 0) ? $r : NULL;
    }

    function getUserData($userID){
        $query = $this->db->query("
            SELECT 
                CONCAT(t1.staffPreName ,t1.staffFName ,'  ',t1.staffLName) as name  , 
                t1.staffImage as image                
            FROM `tbl_staff` t1 
            WHERE t1.staffID  = $userID 
            LIMIT 1 
        ");
        return $query->row_array();  
    } 

    function getLeave($startDate, $endDate){
        $sql = "SELECT 
                    s.staffID,  
                    s.orgID,
                    CONCAT(s.staffPreName ,s.staffFName ,'  ',s.staffLName) as name  , 
                    s.staffNickName,
                    sa.absStartDate,
                    sa.absEndDate,
                    at.absName 
                FROM `tbl_staff` s  
                INNER JOIN `tbl_staff_absence` sa 
                    ON sa.staffID = s.ID 
                INNER JOIN `tbl_absence_type` at
                    ON sa.absTypeID = at.absTypeID 
                WHERE  sa.statusApprove != 3 AND DATE(sa.absStartDate) <= '".$startDate."' AND DATE(sa.absEndDate) >= '".$endDate."'  
                 ";
        echo "<!--".$sql."-->"; 
        $r = $this->db->query($sql);
        return ($r->num_rows() > 0) ? $r : NULL;
        //return NULL;
    } 


     public function getLeaveDb($datestr, $ag){
            if(!empty($datestr)){            
                 $sql = "SELECT 
                        s.staffID,
                        CONCAT(s.staffPreName ,s.staffFName ,'  ',s.staffLName) as name  , 
                        s.staffNickName,
                        sa.absStartDate,
                        sa.absEndDate,
                        sa.status_absence,
                        an.absName,
                        os.orgName
                    FROM `tbl_staff` s  
                    INNER JOIN `tbl_org_chart` os ON s.orgID = os.orgID
                    INNER JOIN `tbl_staff_absence` sa  ON sa.staffID = s.ID 
                    INNER JOIN `tbl_absence_type` an ON sa.absTypeID = an.absTypeID 
                    WHERE DATE(sa.absStartDate) <= '$datestr' AND DATE(sa.absEndDate) >= '$datestr' AND sa.statusApprove != 3";
                    if(!empty($ag)){
                       $sql .= " AND s.orgID IN ($ag)"; 
                        }
                       $sql .= "ORDER BY s.orgID,an.absName,sa.status_absence ASC"; //,s.staffID ASC 
                    $r = $this->db->query($sql);
                    return ($r->num_rows() > 0) ? $r->result() : NULL;
            }
            
        }
		
	function getByBirthday(){
        // $year -= 543;
        $day = date("d");
        $preday = $day + 5; 
        //pre birthday 5 day
        $month = date("m");
            $sql = "SELECT
                    s.staffID,  
                    s.orgID,
                    CONCAT(s.staffPreName ,s.staffFName ,'  ',s.staffLName) as name  , 
                    s.staffNickName ,
                    s.staffBirthday                              
                FROM tbl_staff s 
                WHERE s.status <> 2 ";
        if(!empty($day)){
            $sql.="AND DAY(staffBirthday) <= '$preday' AND DAY(staffBirthday) >= '$day'";
        }
        if(!empty($month)){
                $sql.="AND MONTH(staffBirthday) = '$month' ";
        }
        /*
        if(!empty($year)){
            $sql.=" YEAR(staffBirthday) = '$year' AND ";
        }
        */
        $sql.= " ORDER BY DAY(staffBirthday),YEAR(staffBirthday)  ASC";
        $sql = substr($sql,0,-4);
        //echo "<pre>";
        //print_r($sql);
        $r = $this->db->query($sql);
        //echo $sql;
        return ($r->num_rows() > 0) ? $r : NULL;
    }


    public function getbir($datestr, $ag){
            if(!empty($datestr)){ 
            // $year -= 543;
            $day = date("d");
            $preday = $day + 5; 
            //pre birthday 5 day
            $month = date("m");           
                 $sql = "SELECT 
                        s.staffID,
                        CONCAT(s.staffPreName ,s.staffFName ,'  ',s.staffLName) as name  ,
                        s.staffNickName ,
                        s.staffBirthday,
                        os.orgName
                    FROM `tbl_staff` s  
                    INNER JOIN `tbl_org_chart` os ON s.orgID = os.orgID
                    where status <> 2 ";
                    
                    if(!empty($ag)){
                       $sql .= " AND s.orgID IN ($ag)"; 
                        }
                     if(!empty($day)){
                        $sql.="AND DAY(staffBirthday) <= '$preday' AND DAY(staffBirthday) >= '$day'";
                        }
                     if(!empty($month)){
                        $sql.="AND MONTH(staffBirthday) = '$month' ";
                        }
                       $sql .= " ORDER BY DAY(staffBirthday),YEAR(staffBirthday),os.orgName ASC"; //,s.staffID ASC 
                    $sql = substr($sql,0,-4);
                    $r = $this->db->query($sql);
                    return ($r->num_rows() > 0) ? $r->result() : NULL;
            }
            
        }


         public function getOrgDashborad($orgID=''){            
                $sql = "SELECT oc.orgID, oc.orgName
                         FROM tbl_org_chart oc";
                $sql.= " WHERE upperOrgID in (2,3) ";

                       
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r->result() : NULL;     
        }
            
}
/* End of file user_model.php */
/* Location: ./application/module/user/models/user_model.php */