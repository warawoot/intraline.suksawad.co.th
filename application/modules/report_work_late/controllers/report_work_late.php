<?php

class Report_work_late extends MY_Controller {
	
     function __construct() {
    	parent::__construct();
     
  		//$this->load->model('course_no_model');
  
        $this->load->model('staff_model');
  		  $this->load->model('orgchart_assigndate_model');

         $this->load->model('staff_work_late_model');

        $this->load->module('pagination_module');

		 
    }
  	 
    public function index() {
		
      $page =  (!empty($_POST['page'])) ? $this->input->post('page') : 1;
      $pagesize = 25;
      $start = ($page-1)*$pagesize;

      $startDate  =  (!empty($_POST['start_date'])) ? toCEDate($this->input->get_post('start_date')) : date("Y-m-d");
      $endDate  = (!empty($_POST['end_date'])) ? toCEDate($this->input->get_post('end_date')) : date("Y-m-d");
		  $month	 = (!empty($_POST['month'])) ? $this->input->post('month') : date('m');
 		  $year       = (!empty($_POST['year'])) ? $this->input->post('year')-543 : date('year')+543;
      $data['assignID'] = $this->orgchart_assigndate_model->getLastedDate();
      $empID         = (!empty($_POST['empID'])) ? $this->input->post('empID') : "";
      $orgID = (!empty($_POST['orgID'])) ? $this->input->post('orgID'):"";
      //$data['assignID'] = $this->orgchart_assigndate_model->getLastedDate();

      $data['r'] = $this->staff_work_late_model->getLateData($startDate,$endDate,$year,$empID,$orgID);
      $tmp = 1;
      /*if(!empty($r)){
        foreach($r as $row){
          if($tmp > $start && $tmp <= ($pagesize+$start))
            $data['r'][] = $row;
          
          $tmp++;
        }
      }
      */
        // loop ผลรวม
     /* $numTimStart=0;
      $numTimeEnd = 0;
       foreach($r as $re){
          $numTimStart = $numTimStart+$re->TimeStart;
          $numTimeEnd = $numTimeEnd+$re->TimeEnd;
        }
      $data['numTimStart'] = $numTimStart;
      $data['numTimeEnd'] = $numTimeEnd;
    */
      $data['startDate'] = $startDate;
      $data['endDate'] = $endDate;
      $data['year'] = $year;
      $data['month'] = $month;
      $data['empID'] = $empID;
      $data['orgID'] = $orgID;
      $data['mode'] = "view_late";
      /* $count = count($r);
      if($count > 0)
          $data['viewpage'] = $this->pagination_module->index($pagesize,$page,$count);
      else
      */
      $data['viewpage'] = '';
      $data['start'] = $start;
		  $data['title'] = "รายงานสรุปจำนวนนาทีที่สาย"; 
      $this->template->load('template/admin', 'main',$data);
 		 
     }


	 
}

?>