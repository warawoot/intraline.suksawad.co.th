<style type="text/css">
	.form-control {
 	  color: #343232;
	}

</style>
<style>
  #add_trainee{
    position:absolute;
    top:140px;
    left:80px;
  }
</style>
<style>
.red1
{
  background: #F5A9A9;
}

.red2
{
  background: #F78181;
}
.yellow{
  background:#FFFF00;
}
.yellow1
{
  background:#F3E2A9;
}

.yellow2
{
  background:#F5DA81;
}

.green1
{
  background:#BCF5A9;
}

.green2
{
  background:#81F781;
}
select{
  margin-bottom:20px;
}
</style>
<?php
$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
          ?>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
<link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3><?php echo !empty($title) ? $title : "ข้อมูลการศึกษา"; ?></h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('report_work_late') ?>" method="post" id="searchForm" enctype="multipart/form-data">
                      <input type="hidden" name="page" id="page">
                       <div class="form-group">
                          <label class="control-label col-sm-3">วันที่</label>
                          <div class="col-sm-9">
                            <input class="form-control" type="text" data-type="date"  name="start_date" id="start_date" value="<?php echo toBEDate($startDate); ?>">
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="control-label col-sm-3">วันที่สิ้นสุด</label>
                          <div class="col-sm-9">
                              <input class="form-control" type="text" data-type="date"  name="end_date" id="end_date" value="<?php echo toBEDate($endDate); ?>">
                          </div>
                       </div>
                    	<div class="form-group">
                            <label class="control-label col-sm-3">แผนก</label>
                            <div class="col-sm-9">
                                <select class="xcrud-input form-control" data-type="select" name="orgID" id="orgID"> 
                                  <option value="">--กรุณาเลือก--</option>
                                <?php 
                                    echo getDropdownTree('0','',$assignID,$orgID);
                                ?>
                                </select>
                            </div>
                      </div>

                         <div class="form-group">
                            <label class="control-label col-sm-3">พนักงาน</label>
                            <div class="col-sm-9">
                               
                                <?php 
                                    echo getDropdown(listData('tbl_staff','staffIDCardOff','staffFName,staffLName'),'empID', $empID, "class='xcrud-input  form-control'");
                                ?>
                               
                            </div>
                         </div>
                            
                            <div class="form-group">
                                  <button type="submit" id="search" class="btn btn-warning">ค้นหา</button>

                            </div>
                    </form>
                    
            </div>
         </section>
    </div>
</div> 
<!--
    <a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
    <a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>
   --> 
<table class="xcrud-list table table-hover table-bordered">
    <thead>
        <tr class="xcrud-th">
            <th class="xcrud-num" width="1%">#</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> รหัสพนักงาน </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"> ชื่อ-นามสกุล </th>
            <!--th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID">ฝ่าย </th-->
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID">แผนก </th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID">มาสาย</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID">ออกก่อน</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID">หักลบ</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID">คงเหลือ</th>
            <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff.orgID"></th>
            
         </tr>   
    </thead>
    <tbody>
        <?php 
          $num = $start+1 ;
          $tmp_id = '';
          $sumMinuteLate1 = 0;
          $sumMinuteLate3 = 0;
          $sumDeductTime  = 0;
          $totalMinuteLate= 0;
          if(!empty($r)){
            foreach ($r as $row) {
            ?>
          
                <tr class="xcrud-row xcrud-row-0 <?php //echo $class; ?>" > 
                    <td class="xcrud-current xcrud-num"><?php echo $num;?></td>
                    <td><?php echo $row->staffID;?></td>
                    <td style="white-space: nowrap;"><?php echo $row->staffName;?></td>
                    <!--td style="white-space: nowrap;"><?php echo getOrg1ByWork($row->ID);?></td-->
                    <td style="white-space: nowrap;"><?php echo getOrgByWork($row->ID); ?></td>
                    <td class="text-right"><?php echo (($row->sumMinuteLate1 != 0)? number_format($row->sumMinuteLate1) : '-');?></td>
                    <td class="text-right"><?php echo (($row->sumMinuteLate3 != 0)? number_format($row->sumMinuteLate3) : '-');?></td>
                    <td class="text-right"><?php echo (($row->sumDeductTime != 0)?  number_format($row->sumDeductTime)  : '-');?></td>
                    <td class="text-right"><?php echo (($row->totalMinuteLate != 0)?number_format($row->totalMinuteLate): '-');?></td>
                    <td style="white-space: nowrap;"><a href="report_work/?mode=<?php echo $mode;?>&empID=<?php echo $row->staffIDCardOff;?>&start_date=<?=toBEDate($startDate);?>&end_date=<?=toBEDate($endDate);?>&orgID=<?$orgID;?>&problem=1">รายละเอียด</a></td>
                </tr>
                   
                <?php 
                    $sumMinuteLate1  += $row->sumMinuteLate1;
                    $sumMinuteLate3  += $row->sumMinuteLate3;
                    $sumDeductTime   += $row->sumDeductTime;
                    $totalMinuteLate += $row->totalMinuteLate;
                    $num++; 
              }
              ?>
              <tr class="xcrud-row xcrud-row-0"> 
                   
                    <td colspan="4"><strong>จำนวน </strong><?=$num-1;?> คน</td>
                    <td class="text-right"><?php echo number_format($sumMinuteLate1);?></td>
                    <td class="text-right"><?php echo number_format($sumMinuteLate3);?></td>
                    <td class="text-right"><?php echo number_format($sumDeductTime);?></td>
                    <td class="text-right"><?php echo number_format($totalMinuteLate);?></td>
                    <td></td>
                </tr>
              <?php
            }else{
         ?>	   

            <tr class="xcrud-row xcrud-row-0 green1"> 
                
                  <td colspan="9" style="text-align:center;">ไม่พบข้อมูล</td>
                  
            </tr>
           
        <?php } ?>          
       </tbody>
    <tfoot>
   </tfoot>
</table>
  
  <?php echo $viewpage; ?>


<script type="text/javascript"> 

    

    function openModal(staff_id,staff_name,staff_lastname,date_in,date_in_db,time_in,time_out){
      //var staffID = staff_id.toString();
      var staffID =staff_id.toString();
      var staffName =staff_name;
      var date= date_in.toString() ;
      var time_out_chek;
      if(time_out===''){
        time_out_chek = ""; 
      }else{
        time_out_chek = "-"+time_out.toString();
      }
      var time = time_in+time_out_chek;
      var staffLastname = staff_lastname;
      var name = staffID+"-".concat(staffName)+"   "+staffLastname;
      $('#staff').val(name);
      $('#date').val(date);
      $('#time').val(time);
      //hidden
      $('#staff_id').val(staff_id);
       $('#daily_date').val(date_in_db);
       $('#time_in').val(time_in);
      //$('#daily_date').val(daily_date);
      //alert(staff_id);
      $('#myModal').modal('show');
    }

    /*function submitForm(){
      $('#staffForm').submit();
    }*/
    
	

    $(document).ready(function () {
     var dateBefore=null;  
      $("#start_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      });
      
      $("#end_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      }); 

     
   });

   function submitForm(){
    $('#approveForm').submit();
   }

   function getPaging(pageID){
      $('#page').val(pageID);
      $('#searchForm').submit();
   }
    function print_pdf(){
          window.open('<?php echo site_url();?>report_work/print_pdf?start_date='+$('#start_date1').val()+'&end_date='+$('#end_date1').val()+'&orgID='+$('#orgID').val()+'&empID='+$('#empID').val()+'&problem='+$('#problem:checked').val());
        }

        function print_excel(){
          window.open('<?php echo site_url();?>report_work/print_excel?start_date='+$('#start_date1').val()+'&end_date='+$('#end_date1').val()+'&orgID='+$('#orgID').val()+'&empID='+$('#empID').val()+'&problem='+$('#problem:checked').val());
        }
  
</script>
 