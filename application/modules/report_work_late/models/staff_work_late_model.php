<?php
class Staff_work_late_model extends Default_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff_work_late';
            $this->_pk = 'amtID';
            $this->_timestamps = false;
        }

    public function getLateData($startDate='',$endDate='',$year='',$staffID='',$orgID=''){
          $whereDate = "";
          $whereMonth = "";
          $whereYear = "";
          $whereOrg = "";
          $whereStaffID = "";
          if(!empty($startDate) && !empty($endDate)){
            $whereDate = "  AND (DATE(dateDaily) BETWEEN '$startDate' AND '$endDate') "; 
          }
          if(!empty($orgID)){
              $whereOrg = " AND c.orgID IN ($orgID".$this->getLowerOrg($orgID).") ";
            } 
          if(!empty($staffID)){
              $whereStaffID = " AND staffIDCardOff = '$staffID' ";
          } 

          $sql = "SELECT
                      s.ID
                      ,s.staffID
                      ,s.staffIDCardOff
                      ,CONCAT(s.staffFName,' ',s.staffLName) AS staffName 
                      ,IFNULL(late1.sumMinuteLate1,0) AS sumMinuteLate1
                      ,IFNULL(late3.sumMinuteLate3,0) AS sumMinuteLate3
                      ,(
                        IFNULL(approve1.deductTime,0) + 
                        IFNULL(approve3.deductTime,0)
                       ) as sumDeductTime
                      ,(
                        IFNULL(late1.sumMinuteLate1,0) + 
                        IFNULL(late3.sumMinuteLate3,0) - 
                        IFNULL(approve1.deductTime,0) - 
                        IFNULL(approve3.deductTime,0)
                       ) as totalMinuteLate 
                  FROM
                    tbl_staff s 
                  LEFT JOIN tbl_staff_work c  
                    ON c.staffID = s.ID  
                  LEFT JOIN 
                  /*****************************************************************************
                  จำนวนนาทีที่มาสาย 
                  ******************************************************************************/
                  (SELECT
                    T1.staffID
                    ,SUM(T1.minuteLate1) as sumMinuteLate1 
                  FROM
                  ( 
                    SELECT 
                      staffID
                      ,(TIME_TO_SEC(SUBTIME(TIME(MIN(dateDaily)),'08:00:00')) / 60) AS minuteLate1
                    FROM  `tbl_staff_work_daily` 
                    WHERE 1 
                      AND TIME(dateDaily) < '10:00:00'
                      AND TIME(dateDaily) > '08:00:00'
                      $whereDate
                      /*AND staffID='10002'*/
                    GROUP BY staffID, DATE(dateDaily)
                  ) AS T1
                  GROUP BY T1.staffID 
                  ) AS late1
                  ON s.staffIDCardOff = late1.staffID

                  LEFT JOIN
                  /*****************************************************************************
                  หาผลรวมวลาที่ Approve ช่วงเช้า
                  ******************************************************************************/
                  (
                  SELECT
                    w.staffID
                    ,SUM(TIME_TO_SEC(SUBTIME(TIME(w.dateTimeLate), wa.approvedTime))/60) AS deductTime 
                  FROM
                  /*เวันเวลาที่มาสาย*/
                    (
                    SELECT 
                      staffID
                      ,MIN(dateDaily) AS dateTimeLate 
                    FROM  `tbl_staff_work_daily` w 
                    WHERE 1 
                      AND TIME(dateDaily) < '10:00:00'
                      AND TIME(dateDaily) > '08:00:00'
                      $whereDate
                      /*AND staffID='10002'*/
                    GROUP BY staffID, DATE(dateDaily)
                    ) AS w 

                  INNER JOIN
                  /*วันเวลาที่ Approve */
                    (
                    SELECT
                      staffID
                      ,dateDaily 
                      ,approvedTime
                    FROM  `tbl_staff_work_daily_approve`
                    WHERE 1 
                      AND periodType = 1 
                      $whereDate
                    ) AS wa
                  ON w.staffID = wa.staffID
                  AND DATE(w.dateTimeLate) = DATE(wa.dateDaily)
                  GROUP BY w.staffID
                  ) AS approve1
                  ON s.staffIDCardOff = approve1.staffID

                  /*****************************************************************************
                  จำนวนนาทีที่กลับก่อน
                  ******************************************************************************/

                  LEFT JOIN
                  (
                  SELECT
                    T3.staffID
                    ,SUM(T3.minuteLate3) as sumMinuteLate3 
                  FROM
                  ( 
                    SELECT 
                      staffID
                      ,(TIME_TO_SEC(SUBTIME('17:00:00',TIME(MIN(dateDaily)))) / 60) AS minuteLate3
                    FROM  `tbl_staff_work_daily` 
                    WHERE 1 
                      AND TIME(dateDaily) > '16:00:00'
                      AND TIME(dateDaily) < '17:00:00'
                      $whereDate
                      /*AND staffID='10002'*/
                    GROUP BY staffID, DATE(dateDaily)
                  ) AS T3
                  GROUP BY T3.staffID 
                  ) AS late3
                  ON s.staffIDCardOff = late3.staffID

                  LEFT JOIN
                  /*****************************************************************************
                  หาผลรวมวลาที่ Approve ช่วงเย็น
                  ******************************************************************************/
                  (
                  SELECT
                    w.staffID
                    ,SUM(TIME_TO_SEC(SUBTIME(wa.approvedTime, TIME(w.dateTimeLate)))/60) AS deductTime 
                  FROM
                  /*เวันเวลาที่มาสาย*/
                    (
                    SELECT 
                      staffID
                      ,MIN(dateDaily) AS dateTimeLate 
                    FROM  `tbl_staff_work_daily` w 
                    WHERE 1 
                      AND TIME(dateDaily) > '16:00:00'
                      AND TIME(dateDaily) < '17:00:00'
                      $whereDate
                      /*AND staffID='10002'*/
                    GROUP BY staffID, DATE(dateDaily)
                    ) AS w 

                  INNER JOIN
                  /*วันเวลาที่ Approve */
                    (
                    SELECT
                      staffID
                      ,dateDaily 
                      ,approvedTime
                    FROM  `tbl_staff_work_daily_approve`
                    WHERE 1 
                      AND periodType = 3 
                      $whereDate
                    ) AS wa
                  ON w.staffID = wa.staffID
                  AND DATE(w.dateTimeLate) = DATE(wa.dateDaily)
                  GROUP BY w.staffID
                  ) AS approve3
                  ON s.staffIDCardOff = approve3.staffID

                  WHERE 
                    (late1.sumMinuteLate1 IS NOT NULL OR late3.sumMinuteLate3 IS NOT NULL)
                    $whereOrg
                    $whereStaffID
                  ORDER BY totalMinuteLate DESC
                  ";
            $r = $this->db->query($sql);
            return  $r->result(); 

    }
    private function getLowerOrg($orgID){
            $strOrg = "";
            $sql = "SELECT orgID FROM tbl_org_chart WHERE upperOrgID=$orgID";
            $r = $this->db->query($sql);
            $result = $r->result();
            foreach ($result as $key => $row) {
                $strOrg .= ",".$row->orgID;
                $this->getLowerOrg($row->orgID);
            }
            return $strOrg;  
        }
}            
