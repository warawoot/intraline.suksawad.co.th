<?php

class Position_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="tbl_position";
        $this->pk = "positionID";
	}
    
	function max_id(){
	    $this->db->select_max('positionID');
		$this->db->from('tbl_position');
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->positionID; 
 		}else{
			$data 		=  '1'; //  hot เท่ากับ login แต่ status ไม่เท่า 2
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
   function max_data($positionID){
	    $this->db->select('*');
		$this->db->from('`tbl_position`');
		$this->db->where('`positionID`' ,$positionID);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        //print_r($data);
		return ($query->num_rows() > 0) ? $data : NULL;
    }       
   
} 