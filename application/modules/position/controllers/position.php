<?php

class Position extends MY_Controller {
    
  
    function __construct() {
    parent::__construct();
     	$this->load->model('position_model');
    }
 
    public function index() {
		
		$max 	= $this->position_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
		}else{
 			$max_id  =  $max+1;
 		}
		
        $xcrud		=	xcrud_get_instance();
        
		$xcrud->table('tbl_position'); 
        $arr		=	array( "positionName"=>"ชื่อตำแหน่งงาน" );
		$xcrud->pass_var('positionID',$max_id,'create'); 
		$xcrud->validation_required('positionName') ;
		
 		$xcrud->label($arr);
        $xcrud->columns($arr); 
        $xcrud->fields($arr); 
          
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
		 
        $this->template->load('template/admin', 'position',$data);
		
    }
     
   
} 
?>