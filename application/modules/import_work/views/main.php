<style type="text/css">
  .form-control {
    color: #343232;
  }

</style>
<style>
  #add_trainee{
    position:absolute;
    top:140px;
    left:80px;
  }
  #loading_div{
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    z-index:200;
}

.default {
  background: #292929;
  border: 1px solid #111; 
  border-radius: 5px; 
  overflow: hidden;
  box-shadow: 0 0 5px #333;       
}
.default div {
  background-color: #1a82f7;
  background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#0099FF), to(#1a82f7)); 
  background: -webkit-linear-gradient(top, #0099FF, #1a82f7); 
  background: -moz-linear-gradient(top, #0099FF, #1a82f7); 
  background: -ms-linear-gradient(top, #0099FF, #1a82f7); 
  background: -o-linear-gradient(top, #0099FF, #1a82f7);
}
#progressBar div {
    color: #fff;
    font-size: 12px;
    height: 100%;
    line-height: 22px;
    text-align: right;
    width: 0;
}
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
<link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>นำเข้าข้อมูลบันทึกเวลาเข้างาน</h3>
            </header>
            <div class="panel-body">
                  <form action="<?php echo base_url('import_work/save') ?>" method="post" enctype="multipart/form-data" id="formImport">
                        <div class="form-group">
                                <label class="control-label col-sm-3">File</label>
                                <div class="col-sm-9">
                                    <input type='file' name="file_import" id="import_file" class="form-control">
                                </div>
                                <p>หมายเหตุ: ไม่ควรนำเข้าข้อมูลเกิน 2000 แถวต่อครั้ง และแถวสุดท้ายต้องตัดวันที่และรหัสพนักงานให้อยู่ในกลุ่มเดียวกัน</p>
                             </div>
                            
                            <div class="form-group">
                                  <button type="submit" class="btn btn-warning">ตกลง</button>
                            </div>
                    </form>

                    <div id="loading_div"><img src='<?php echo base_url();?>assets/images/loading.gif' alt='Loading...' /></div>

                    
            </div>
         </section>
    </div>
</div> 
<br /><br />
<div class="default" id="progressBar"><div style="width: 850px;"></div></div>
  

 

<script type="text/javascript"> 

    

    var cur_p =0;
    var flg_in = false;
    $(document).ready(function(){
        $('#loading_div').hide();
        $('#progressBar').hide();
        progressBar(0, $('#progressBar'));
      })

    $(document).ready(function () {
     var dateBefore=null;  
      $("#start_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      });
      
      $("#end_date").datepicker({  
        dateFormat: 'dd/mm/yy',  
        /*showOn: 'button',  */
        buttonImageOnly: false,  
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
        //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        changeMonth: true,  
        changeYear: true 
      }); 

     
   });

   /*
    $('#formImport').submit(function(e){
        importfile();
        e.preventDefault();    
    });
    */

    function importfile(){
      var form = $('#formImport') [0];
      $.ajax({
            url: '<?php echo site_url(); ?>import_work/save',
            type: 'POST',
            data: new FormData( form ),
            processData: false,
            contentType: false,
            success: function (data) {
                alert("นำเข้าข้อมูลสำเร็จ " + data + " รายการ");
            }
        });
        //getProgress();
    }

    function getProgress(){
        //console.log("progress called");
        $('#formImport').hide();
        //$('#loading_div').show();
        $('#progressBar').show();
        $.get( "<?php echo site_url(); ?>import_work/getProcess", function( data ) {
            console.log(data);
            var p = data.progressExl;
            var r = data.row_saved;
            var all = data.row_all;

            if(cur_p == p && cur_p != 0 && !Boolean(flg_in)){
                flg_in = true;
                alert('Import ข้อมูลเรียบร้อยแล้ว');
            location.reload();
            }else{
                flg_in = false;
                cur_p = p;
                progressBar(p, $('#progressBar'));
                if (r < all){
                    setTimeout(function () {
                        getProgress();
                    }, 5000);
                }else{
                    alert('Import ข้อมูลเรียบร้อยแล้ว');
                   location.reload();
                }
            }
        }, "json");
    }

    function progressBar(percent, $element) {
        var progressBarWidth = percent * $element.width() / 100;
        $element.find('div').animate({ width: progressBarWidth }, 500).html(percent + "%&nbsp;");
    }
  
</script>
 