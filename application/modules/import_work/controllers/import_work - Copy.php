<?php

class Import_work extends MY_Controller {
	
     function __construct() {
    	parent::__construct();
     
		//$this->load->model('course_no_model');
		$this->load->model('staff_work_daily_model');
		$this->load->model('orgchart_assigndate_model');
		 
    }
  	 
    public function index() {
		
		  session_start();
      $_SESSION['progressExl'] = 0;
      $_SESSION['row_saved'] = 0;
      $_SESSION['row_all'] = 1;
      session_write_close();

		  $data['title'] = "Import"; 
      $this->template->load('template/admin', 'main',$data);
 		 
     }

     function getProcess(){
      session_start();
      session_write_close();
      $output = array();
      $output['progressExl'] = $_SESSION['progressExl'];
      $output['row_saved'] = $_SESSION['row_saved'];
      $output['row_all'] = $_SESSION['row_all'];
      echo json_encode($output);  
    }
	 
   function save(){

      //ob_start();
      ini_set('memory_limit', '100M');
      ini_set('max_execution_time', '3600');
      ini_set('max_input_time', '3600');


      session_start();
      $_SESSION['progressExl'] = 0;
      $_SESSION['row_saved'] = 0;
      $_SESSION['row_all'] = 1;
      session_write_close();
      if(is_uploaded_file($_FILES['file_import']['tmp_name'])){

        $path = './assets/import/';
        $filename = $_FILES["file_import"]["name"];

        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        //error_reporting(E_ALL);


        move_uploaded_file($_FILES['file_import']['tmp_name'], $path.$_FILES['file_import']['name']);
        
        //$this->db->trans_begin();
        $fp = file($path.$filename, FILE_SKIP_EMPTY_LINES);
        $num_row = count($fp)-1;
        //echo $num_row; exit;
        
        session_start();
        $progress = $_SESSION['progressExl'];
        $row_save = $_SESSION['row_saved'];
        $row = 1;
        
        //Here is what we need, write into a session var
        /*session_start();
        $_SESSION['progressExl'] = $progress;
        $_SESSION['row_saved'] = 0;
        $_SESSION['row_all'] = $num_row;
        session_write_close();*/
        $_SESSION['row_all'] = $num_row;
        session_write_close();
        //--- End of what is needed
        sleep(2);

        
        $seq = "";
        $row = 1;
        $i = 1;
        $staffID = "00";
        $b=1;
        $date = strtotime('1990-01-01');
        if (($handle = fopen($path.$filename, "r")) !== FALSE) {
           while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            
              $str = "";
              if($row > $row_save){
                    //echo "<pre>";
                    //print_r($data);
                    //echo "</pre>";
                    $num = count($data);
                    
                    //$row++;
                  
                    if(is_numeric($data[2]) && !empty($data[2]) && !empty($data[3])){
                      $list = explode(' ',$data[3]);
                      /*
                       echo "<pre>";
                    print_r($data);
                    echo "</pre>";
                    */
                      //$listd = explode('/',$list[0]);
                      $arr = array(
                            'staffID' => $data[2],
                            'dateDaily'   => toCEDate($list[0]).' '.$list[1],
                            'create_date' => date('Y-m-d H:i:s')
                          );
                      $date_daily = toCEDate($list[0]);
                      
                      // if has data delete old data
                       $count_all = $this->db->where(array('staffID' =>$data[2],'DATE(dateDaily)'=>$date_daily))->count_all_results('tbl_staff_work_daily');
                        // echo $this->db->last_query();
                         if($count_all >= 1){
                              if($staffID == $data[2] || $b==1){ 
                                  if($date != strtotime($date_daily)){
                                        $this->staff_work_daily_model->delete_by(array('staffID' =>$data[2],'DATE(dateDaily)'=>$date_daily));
                                    
                                    //echo $this->db->last_query();
                                        $b==2;
                                        $staffID = $data[2];
                                        $date = strtotime($date_daily);
                                  }
                              }else{
                                  $staffID = $data[2];
                                  $date = strtotime($date_daily);
                              }
                             
                         }else{
                              $staffID = $data[2];
                              $date = strtotime($date_daily);
                         }
                          
                         $this->staff_work_daily_model->save($arr);
                         //exit; 
                        
                        
                         
                        $progress =  (100/$num_row)*$i;
                        $i++;

                        //Here is what we need, write into a session var
                        session_start();
                        $_SESSION['progressExl'] = number_format($progress,2);
                        $_SESSION['row_saved'] = $count_all;
                        $_SESSION['row_all'] = $num_row;
                        session_write_close();
                   }
                   
                
                  //--- End of what is needed
                  //sleep(2);

                }
                $row++;
                
            }
            fclose($handle);
           
        }
        @unlink($path.$filename);
      }
    }
    
     public function import(){
        if(is_uploaded_file($_FILES['file_import']['tmp_name'])){


          $path = './assets/import/';
          $filename = $_FILES["file_import"]["name"];
          move_uploaded_file($_FILES['file_import']['tmp_name'], $path.$_FILES['file_import']['name']);
          
          $row = 1;
          if (($handle = fopen($path.$filename, "r")) !== FALSE) {
              while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $str = "";
                if($row > 1){
                   $num = count($data);
                    
                    $row++;
                  
                    //echo $data[2].' '.$data[3].'<br>';
                    //print_r($data);

                    if(is_numeric($data[2]) && !empty($data[2]) && !empty($data[3])){
                      $list = explode(' ',$data[3]);

                      //$listd = explode('/',$list[0]);
                      $arr = array(
                            'staffID' => $data[2],
                            'dateDaily'   => toCEDate($list[0]).' '.$list[1],
                            'create_date' => date('Y-m-d H:i:s')
                          );
                        $this->staff_work_daily_model->save($arr);
                        
                      }
                    

                    /*$ra= $this->student_activity_online_model->checkDupReserv($eventID,$data[0]);
                    if($ra == NULL){
                      $seq = $this->student_activity_online_model->getLastedReservSeq($eventID);
                              $seq = ($seq != NULL) ? $seq+1 : 1;
                              $userID = $this->student_activity_online_model->getUserID($data[0]);
                      $arr = array(
                          'IdEvent' => $eventID,
                          'seq'   => $seq,
                          'UserID'  => $userID,
                          'status'  => '3',
                          'create_date' => date('Y-m-d H:i:s'),
                          'join_date'   => date('Y-m-d H:i:s')
                        );
                      $this->student_activity_online_model->save($arr);
                   }*/
                   
                }
                
                  $row++;

              }
              fclose($handle);
          }
            @unlink($path.$filename);
            
            header('Content-Type: text/html; charset=utf-8');
            echo"<script language=\"JavaScript\">";
            echo"alert('Import ข้อมูลเรียบร้อยแล้ว');";
            echo"window.location='".site_url()."import_work'";
            echo"</script>";

          }else{
            header('Content-Type: text/html; charset=utf-8');
            echo"<script language=\"JavaScript\">";
            echo"alert('กรุณาเลือกไฟล์');";
            echo"window.location='".site_url()."import_work'";
            echo"</script>";
        }
     }

   
     public function do_after_import(){
        $this->load->library('import_daily');
        $this->import_daily->do_after_import();
     }
	 

}

?>