<?php
class Orgchart_assigndate_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_orgchart_assigndate';
            $this->_pk = 'assignID';
        }

        public function getLastedDate($date=''){
        	$sql = "select assignID from tbl_orgchart_assigndate ";
            if(!empty($date)){
                $sql.=" where assignDate <= '$date'";
            }
            $sql.=" order by assignDate DESC LIMIT 1";
        	$r = $this->db->query($sql);
        	return ($r->num_rows() > 0) ? $r->row()->assignID : "1";
        }
        

	
}
/* End of file Orgchart_assigndate_model.php */
/* Location: ./application/module/staffwork/models/Orgchart_assigndate_model.php */