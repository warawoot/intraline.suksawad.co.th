<?php

class Staffwelfare extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_medical')->where('staffID =', $id);
		
		
		//// List /////
		$col_name = array(
			'medicalDate' => 'วันที่เบิก',
			'medicalPerson' => 'ผ้เข้ารับการรักษา',
			'medicalStartDate' => 'วันที่เริ่มรักษา',
			'medicalEndDate' => 'วันที่สิ้นสุดการรักษา',
			'medicalDetail' => 'รายการ',
			'medicalPrice' => 'จำนวนเงิน'

		);

		$xcrud->columns('medicalDate,medicalPerson,medicalDetail,medicalPrice');
		$xcrud->label($col_name);
		$xcrud->column_callback('medicalDate','toBDDate');
		$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		$xcrud->column_width("medicalDate","20%")->column_width("medicalDetail","40%");
		// End List//

		//// Form //////

		$xcrud->pass_var(array('staffID'=>$id));
		$xcrud->fields('medicalDate,medicalPerson,medicalStartDate,medicalEndDate,medicalDetail,medicalPrice');
		
		$xcrud->unset_add()->unset_edit()->unset_remove();

		$data['html1'] = $xcrud->render();
		//$xcrud->hide_button("return");

		//$xcrud->validation_required('personFName,personLName');
		// End Form//
		
		
		// select table//
		$xcrud2 = xcrud_get_instance();
		$xcrud2->table('tbl_staff_edu_welfare')
				//->join('staffID','tbl_staff_person','staffID` AND `tbl_staff_edu_welfare`.`eduChild` = `tbl_staff_person`.`personChild')
				//->where('staffID =', $id)->where('tbl_staff_person.personType =','2');
				->where('staffID =', $id);
				//->join('eduChild','tblstaffperson','personChild');
		//// List /////
		$col_name = array(
			'eduChild' => 'บุตรคนที่',
			'eduName' => 'ระดับการศึกษา',
			'eduYear' => 'ปีการศึกษา',
			'eduPrice' => 'จำนวนเงิน'

		);

		$xcrud2->columns('eduChild,eduName,eduYear,eduPrice');
		$xcrud2->column_callback('eduYear','toBDYear');
		$xcrud2->column_callback('eduPrice','showNumberFormat');

		$xcrud2->label($col_name);
		
		$xcrud2->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		$xcrud2->column_width("eduChild","15%");
		// End List//

		//// Form //////

		$xcrud2->pass_var(array('staffID'=>$id));
		$xcrud2->fields('eduChild,eduName,eduYear,eduPrice');
		// End Form//
		
		$xcrud2->unset_add()->unset_edit()->unset_remove();

		$data['html2'] = $xcrud2->render();



		// select table//
		$xcrud3 = xcrud_get_instance();
		$xcrud3->table('tbl_staff_milk_welfare')->where('staffID =', $id);
				//->join('staffID','tbl_staff_person','staffID` AND `tbl_staff_milk_welfare`.`milkChildNo` = `tbl_staff_person`.`personChild')
				//->where('staffID =', $id)->where('tbl_staff_person.personType =','2');
				
		/*$xcrud2->query('SELECT milkChildNo,MONTH(m.milkDate) as month,YEAR(m.milkDate) as year,sum(m.milkNum) as nummilk,personFName,personLName,personBirthday FROM tblstaffmilkwelfare m
						join tblstaffperson p on m.staffID = p.staffID and m.milkChildNo = p.personChild 
						WHERE m.staffID = '.$id.'
						GROUP BY MONTH(m.milkDate),YEAR(m.milkDate)');
		*/
		
		//// List /////
		$col_name = array(
			'milkChildNo' => 'บุตรคนที่',
			'tbl_staff_person.personFName' => 'ชื่อ - สกุล',
			'milkDate' => 'วันที่เบิกจ่าย',
		//	'tblstaffperson.personBirthday' => 'อายุ',
			'milkNum' => 'จำนวนถุง'

		);

		//$xcrud3->columns('milkChildNo,milkDate,tbl_staff_person.personBirthday,milkNum');
		$xcrud3->columns('milkChildNo,milkDate,milkNum');
		$xcrud3->column_callback('milkDate','toBDDate');

		$xcrud3->label($col_name);
		//$xcrud2->column_pattern('month','{value} {year}');
		//$xcrud3->column_pattern('tbl_staff_person.personFName','{value} {tbl_staff_person.personLName}');
		//$xcrud3->column_callback('tbl_staff_person.personBirthday','getAgeChild');
		$xcrud3->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		$xcrud3->column_width("milkChildNo","15%");
		// End List//

		//// Form //////

		$xcrud3->pass_var(array('staffID'=>$id));
		//$xcrud3->fields('milkChildNo,tbl_staff_person.personFName,tbl_staff_person.personBirthday,milkDate');
		$xcrud3->fields('milkChildNo,tbl_staff_person.personFName,milkDate');
		// End Form//
		
		$xcrud3->unset_add()->unset_edit()->unset_remove();

		$data['html3'] = $xcrud3->render();


		// select table//
		$xcrud4 = xcrud_get_instance();
		$xcrud4->table('tbl_staff_child_welfare')
				//->join('staffID','tbl_staff_person','staffID` AND `tbl_staff_child_welfare`.`childNo` = `tbl_staff_person`.`personChild')
				//->where('staffID =', $id)->where('tbl_staff_person.personType =','2');
				->where('staffID =', $id);
		
		//// List /////
		$col_name = array(
			'childNo' => 'บุตรคนที่',
			'childDate' => 'วันที่ยื่นคำร้อง',
			'childMonth' => 'เดือนที่เบิกจ่าย',
			'childPrice' => 'จำนวนเงิน',
			'childStatus' => 'สถานะเบิกจ่าย'

		);

		$xcrud4->columns('childNo,childDate,childMonth,childPrice,childStatus');
		$xcrud4->column_callback('childDate','toBDDate');

		$xcrud4->label($col_name);
		
		$xcrud4->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud4->pass_var(array('staffID'=>$id));
		$xcrud4->fields('childNo,childDate,childMonth,childPrice,childStatus');
		$xcrud4->change_type('childStatus','select','',array('N'=>'ปกติ','E'=>'ครบกำหนดอายุ'));
		$month = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","พฤศจิกายน","ธันวาคม");

		$xcrud4->change_type('childMonth','select','',$month);

		// End Form//
		
		$xcrud4->unset_add()->unset_edit()->unset_remove();

		$data['html4'] = $xcrud4->render();

	
		$data['title5'] = "";
		$arrWel = array("","กองทุนเลี้ยงชีพ","เงินปูนบำเน็จ");



		$r = $this->db->get_where('tbl_staff',array('staffID'=>$id));
		if($r->num_rows() > 0){
			$welfaretype = $r->row()->staffWelfareType;
			$data['title5'] = $arrWel[$welfaretype];


			$xcrud5 = xcrud_get_instance();
			

			if($welfaretype == '2'){

				$rp = $this->db->get_where('tbl_staff_pension',array('staffID'=>$id));
				$idp = ($rp->num_rows() > 0) ? $rp->row()->pensionID : '';

				if(!empty($idp)){

					$xcrud5->table('tbl_staff_pension')->where('staffID =', $id)
						->join('staffID','tbl_staff','staffID');

					$xcrud5->relation('tbl_staff.positionID','tblposition','positionID','positionName');
					$xcrud5->relation('tbl_staff.orgID1','tblorgchart','orgID','orgName','tbl_org_chart.upperOrgID is null');

					$col_name = array(
						'tbl_staff.staffID' => 'รหัสพนักงาน',
						'tbl_staff.staffFName' => 'ชื่อ - สกุล',
						'tbl_staff.staffIDCard' => 'เลขที่ประจำตัวประชาชน',
						'tbl_staff.positionID' => 'ตำแหน่ง',
						'tbl_staff.orgID1' => 'หน่วยงาน',
						'tbl_staff.staffDateWork' => 'วันที่บรรจุ',
						//'tbl_staff.staffBirthday' => 'อายุงาน (ปี)',
						'pensionSalary' => 'เงินเดือนเดือนสุดท้าย',
						'tbl_staff.orgID' => 'เงินปูนบำเน็จ'
					);
					$xcrud5->label($col_name);
					

					//$xcrud5->fields('tbl_staff.staffID,tbl_staff.staffFName,tbl_staff.staffIDCard,tbl_staff.positionID,tbl_staff.orgID1,tbl_staff.staffDateWork,tbl_staff.staffBirthday,pensionSalary,tbl_staff.orgID');
					$xcrud5->fields('tbl_staff.staffID,tbl_staff.staffFName,tbl_staff.staffIDCard,tbl_staff.positionID,tbl_staff.orgID1,tbl_staff.staffDateWork,pensionSalary,tbl_staff.orgID');
					
					$xcrud5->column_pattern('tbl_staff.staffFName','{tbl_staff.staffPreName}   {value}  {tbl_staff.staffLName}');
					//$xcrud5->column_callback('tbl_staff.staffBirthday','calAgeWork');
					$xcrud5->column_callback('tbl_staff.orgID','calPension');
					$xcrud5->column_callback('pensionSalary','showNumberFormat');
					$xcrud5->column_callback('tbl_staff.staffDateWork','toBDDate');
					
					$xcrud5->unset_list();
					$data['html5'] = $xcrud5->render('view',$idp);
				}
				


			}else if($welfaretype == '1'){
				$rp = $this->db->get_where('tbl_staff_provident',array('staffID'=>$id));
				$idp = ($rp->num_rows() > 0) ? $rp->row()->providentID : '';

				if(!empty($idp)){

					$xcrud5->table('tbl_staff_provident')->where('staffID =', $id)
						->join('staffID','tbl_staff','staffID');

					$col_name = array(
						'tbl_staff.staffID' => 'รหัสพนักงาน',
						'tbl_staff.staffFName' => 'ชื่อ - สกุล',
						'providentDate'	=> 'วันที่เป็นสมาชิก',
						'tbl_staff.staffDateWork' => 'วันที่บรรจุเข้าทำงาน',
						//'tbl_staff.staffBirthday' => 'อายุงาน (ปี)',
						'providentStatus' => 'สถานะสมาชิก'
					);
					$xcrud5->label($col_name);

					//$xcrud5->fields('tbl_staff.staffID,tbl_staff.staffFName,providentDate,tbl_staff.staffDateWork,tbl_staff.staffBirthday,providentStatus');
					$xcrud5->fields('tbl_staff.staffID,tbl_staff.staffFName,providentDate,tbl_staff.staffDateWork,providentStatus');
					$xcrud5->column_pattern('tbl_staff.staffFName','{tbl_staff.staffPreName}   {value}  {tbl_staff.staffLName}');
					//$xcrud5->column_callback('tbl_staff.staffBirthday','calAgeWork');
					$xcrud5->column_callback('tbl_staff.staffDateWork','toBDDate');
					$xcrud5->column_callback('providentDate','toBDDate');
					$xcrud5->change_type('providentStatus','radio','',array('N'=>'ปกติ','E'=>'สิ้นสุด'));

					$xcrud5->unset_list();
					$data['html5'] = $xcrud5->render('view',$idp);

					$xcrud6 = xcrud_get_instance();
					$xcrud6->table('tbl_staff_pay_salary')->where('staffID =', $id);
					$xcrud6->columns('payDate,percentCollect,payCollect,percentGrant,payGrant');
					$col_name = array(
						'payDate' => 'วัน / เดือน / ปี ที่จ่ายเงินเดือน',
						'percentCollect' => '%',
						'payCollect' => 'เงินสะสม',
						'percentGrant' => '%',
						'payGrant' => 'เงินสมทบ'
					);
					$xcrud6->label($col_name);
    				$xcrud6->unset_add()->unset_search()->unset_view()->unset_edit()->unset_remove();
    				$xcrud6->column_callback("payDate","toBDDate");
    				//$xcrud6->column_pattern('payCollect','{percentCollect}%     {value}');
    				//$xcrud6->column_pattern('payGrant','{percentGrant}%     {value}');
    				//$xcrud6->column_callback("payPrice","calPriceCollect");
    				//$xcrud6->column_callback("staffID","calPriceGrant");
    				$xcrud6->column_width("payDate","25%");
    				$xcrud6->change_type('amount', 'price', '5', array('prefix'=>'$'));
    				$xcrud6->sum("payCollect,payGrant");
    				
					$data['html6'] = $xcrud6->render();


				}
			}

		}

		$data['id'] = $id;
		$data['title'] = "สวัสดิการ";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

}
/* End of file staffwelfare.php */
/* Location: ./application/module/staffwelfare/staffwelfare.php */