<style>
.tab-pane,.panel-heading{
    margin-top:30px;
}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <!--<h3><?php //echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php //echo $staffName; ?></small></h3>

                <a href="<?php //echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php //echo $this->config->item('txt_back')?></a>
                 -->
            </header>
            <div class="panel-body">
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs nav-justified ">
                            <li class="active">
                                <a href="#first" data-toggle="tab"> ค่ารักษาพยาบาล </a>
                            </li>
                             <li>
                                <a href="#second" data-toggle="tab"> ค่าช่วยเหลือการศึกษาบุตร </a>
                            </li>
                             <li>
                                <a href="#third" data-toggle="tab"> ค่าใช้จ่ายนมสวัสดิการ </a>
                            </li>
                            <li>
                                <a href="#four" data-toggle="tab"> ค่าช่วยเหลือบุตร</a>
                            </li>
                            <?php if(!empty($title5)){ ?>
                            <li>
                                <a href="#five" data-toggle="tab"> <?php echo $title5; ?></a>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content tasi-tab">

                            <div id="first" class="tab-pane active">

                                <?php echo $html1; ?>
                            </div>

                            <div id="second" class="tab-pane">
                                <?php echo $html2; ?>
                            </div>

                             <div id="third" class="tab-pane">
                                <?php echo $html3; ?>
                            </div>

                             <div id="four" class="tab-pane">
                                <?php echo $html4; ?>
                            </div>
                            <?php if(!empty($title5)){ ?>
                             <div id="five" class="tab-pane">
                                <?php echo !empty($html5) ? $html5 : ""; ?>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
$(function(){

    
    //$('.btn-primary').attr("id","btn_save");
    
    $('.btn-primary').removeAttr("data-after");
    $('.btn-primary').attr("data-after","edit");


})

jQuery(document).on("xcrudafterrequest",function(event,container){

    if(Xcrud.current_task == 'save')
    {
        //Xcrud.show_message(container,'บันทึกเรียบร้อยแล้ว','success');

        /*$('.btn-warning').attr("id","btn_back");
        $('.btn-warning').attr("onclick","window.location='../reg'");
        $('.btn-warning').removeAttr("class");
        $('#btn_back').attr("class","btn btn-warning");*/

        //$('.btn-primary').attr("id","btn_save");
        //$('.btn-primary').removeAttr("data-after");
        //$('.btn-primary').attr("data-after","edit");
        //window.location="<?php echo site_url()?>staffperson/?token=<?php echo $id?>";
    }
});

</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);


</script>