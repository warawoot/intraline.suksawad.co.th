<style type="text/css">
	.form-control {
 	  color: #343232;
	}
    #ui-datepicker-div{  
		font-size: 0.9em;
 	}  
 	.ui-datepicker-trigger{
	  opacity: .5;
	}
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <!-- jquery -->
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
-->  <!-- jquery for DEV
  <script src="javascripts/jquery.min.js"></script>-->
  <script src="<?php echo base_url()?>assets/orgChart/jquery.smmms-orgChart-ver7.js"></script>
  <script src="<?php echo base_url()?>assets/orgChart/jquery-print.js"></script>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/orgChart/smmms-orgChart.css">

 <div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลรายงาน / ผังโครงสร้างองค์กร</h3>
            </header>
            <div class="panel-body">
                <link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                     <div class="xcrud">
                        <div class="xcrud-container">
                            <div class="xcrud-ajax">
                             
                                <div class="xcrud-view">
                                    <div class="form-horizontal">
                                          
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">แสดงผังโครงสร้างองค์กร ณ วันที่ *</label>
                                                <div class="col-sm-9">
                                                    <input  data-required="1" type="text" data-type="date" value="" name="dp1433735797502" id="dp1433735797502" onchange="myFunction()">
                                                </div>
                                                <div id="demo"></div>
                                       </div>
                                       <!--div class="form-group">
                                            <label class="control-label col-sm-3">หน่วยงาน</label>
                                                <div class="col-sm-9">
                                                  <select class="xcrud-input form-control form-control" data-type="select" name="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE" id="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE" maxlength="11" onkeypress="myFunction()"> 
														 
                                                     </select>   
                                                </div>
                                       </div--> 
                                    </div>
                                 </div>
                                <div class="xcrud-top-actions btn-group">
                                    <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action" onclick="modal_ck()" style="width:100px;" >แสดง</a><!--<a href="#" data-task="list" class="btn btn-warning xcrud-action" onclick="modal_back()">ย้อนกลับ</a>-->
                                </div>
                            </div>
                     </div>
                </div> 
             </div>
             
        </section>
    </div>
</div>

<!--<div id="report-organization">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                 <header class="panel-heading"> 
                    <h4><?php //echo $data1; ?></h4>
                </header>
                 <div class="panel-body">
                     <div id ="test-chart"></div>
                 </div>
             </section>
        </div>
    </div>
</div>-->


<script type="text/javascript"> 

 $(document).ready(function () {
	 var dateBefore=null;  
	  $("#dp1433735797502").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  
			buttonImageOnly: false, */ 
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true ,
			showOn: "button",
			buttonImage: "<?php echo base_url()?>assets/images/common_calendar_month_-20.png",
			buttonImageOnly: true
	  });
 }); 
 
 function modal_back(){
	location.replace('<?php echo base_url().'report/organization'; ?>');
 }
 
 
 function myFunction() {
    var x = document.getElementById("dp1433735797502").value;
   // var dp1433735797502 = $("#dp1433735797502").val('');
	$.ajax({
			url: "<?php echo base_url('report/get_ddl_organization') ?>",
			type: 'POST',
			data: {
					assignDate: x 
			},
			success: function(response) {
				//Do Something 
               // var obj = jQuery.parseJSON(response); 
 				$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(response);
                //alert(obj.assignID);
				//var dp1433735797502 = $("#dp1433735797502").val('');
			},
			error: function(xhr) {
				//Do Something to handle error
				//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
				location.replace('<?php echo base_url().'report/organization' ?>');
			}
	});	
 }
 
 function modal_ck(){
	 
 	var startDate	= $("#dp1433735797502").val();
 	var ddl_organization	= $("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").val();
	if(startDate == ''){
		  alert('กรุณาระบุ เลขที่คำสั่ง ด้วยค่ะ');
		  location.href = '<?php echo base_url().'report/organization' ?>';
 	}else{
		  console.log("<?php echo base_url('report/check_add?action='.$actions) ?>&assignDate=" + startDate );
		  $('#test-chart').smmmsOrgChart({
								data: "<?php echo base_url('report/check_add?action='.$actions) ?>&assignDate=" + startDate,
								backgroundColor:"#0fb4e7",
								textColor:"#FFF",
								borderColor:"#000" 
		  });
 		  //$('#myModal').modal('show');		
		  		
		var convert_date	=	startDate; // 01/06/2015
		var res = convert_date.split("/");
		var res_d	=	res[0];
		var res_m	=	res[1];
		var res_y	=	res[2];
		
 		var new_startDate	=	res_y+res_m+res_d;
		
 		//location.replace("<?php echo base_url().'report/tree'; ?>?assignDate=" + new_startDate);
		//location.href = '<?php echo base_url(); ?>report/tree/?assignDate='+new_startDate;  
		//window.location.href = '<?php echo base_url(); ?>report/tree/?assignDate='+new_startDate;
		
		
   /*var options = {
        height: "100%", // sets the height in pixels of the window.
        width: "100%", // sets the width in pixels of the window.
        toolbar: 0, // determines whether a toolbar (includes the forward and back buttons) is displayed {1 (YES) or 0 (NO)}.
        scrollbars: 0, // determines whether scrollbars appear on the window {1 (YES) or 0 (NO)}.
        status: 0, // whether a status line appears at the bottom of the window {1 (YES) or 0 (NO)}.
        resizable: 1, // whether the window can be resized {1 (YES) or 0 (NO)}. Can also be overloaded using resizable.
        left: 0, // left position when the window appears.
        top: 0, // top position when the window appears.
        center: 0, // should we center the window? {1 (YES) or 0 (NO)}. overrides top and left
        createnew: 0, // should we create a new window for each occurance {1 (YES) or 0 (NO)}.
        location: 0, // determines whether the address bar is displayed {1 (YES) or 0 (NO)}.
        menubar: 0 // determines whether the menu bar is displayed {1 (YES) or 0 (NO)}.
    };

    var parameters = "location=" + options.location +
                     ",menubar=" + options.menubar +
                     ",height=" + options.height +
                     ",width=" + options.width +
                     ",toolbar=" + options.toolbar +
                     ",scrollbars=" + options.scrollbars +
                     ",status=" + options.status +
                     ",resizable=" + options.resizable +
                     ",left=" + options.left +
                     ",screenX=" + options.left +
                     ",top=" + options.top +
                     ",screenY=" + options.top;

     target url
    var target = "<?php echo base_url(); ?>report/tree/?assignDate="+new_startDate ;

    popup = window.open(target, 'popup', parameters);*/
		
	//mod by bomb
	window.open("<?php echo base_url(); ?>report/tree/?assignDate="+new_startDate+"&ddl=1","_blank", "toolbar=no, scrollbars=no, resizable=yes ,location=no ,menubar=no,fullscreen =yes");
	
	/*
	window.open("<?php echo base_url(); ?>report/tree/?assignDate="+new_startDate+"&ddl="+ddl_organization ,"_blank", "toolbar=no, scrollbars=no, resizable=yes ,location=no ,menubar=no,fullscreen =yes");
	*/
		
	} //end if	
 }
</script>
 