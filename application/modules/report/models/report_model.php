<?php

class Report_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	}
    
	function  check_orgchart_assigndate($assignDate){
		//SELECT * FROM `tbl_orgchart_assigndate` WHERE assignDate <= '2015-06-17'  ORDER BY assignDate desc limit 1
		$this->db->select('*');
		$this->db->from('`tbl_orgchart_assigndate`');
		$this->db->where('`assignDate` <=' ,$assignDate);
		$this->db->order_by('assignDate' , 'desc');
		$this->db->limit(1);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
		
		/*if($query->num_rows() > 0){
			 //echo $data;
			 //foreach($data as $key => $item):
 				//echo $data['assignID'];
				//echo $data['assignDate'];
				return   $this->check_orgchart_tree($data['assignID'],$data['assignDate']); 
    	 	// endforeach;
		}else{
 			   return NULL;	
 		}*/
		
	}
	
	function  check_orgchart_tree($assignID,$assignDate){
		//SELECT * FROM `tbl_org_chart` as t1 
		//INNER JOIN tbl_orgchart_assigndate as t2 ON t1.assignID = t2.assignID
		//Where t1.assignID = 1 and t2.assignDate <= '2015-06-01'
 		$this->db->select('*');
		$this->db->join('tbl_orgchart_assigndate as t2', 't1.assignID = t2.assignID','inner');
		
		$this->db->join('tbl_staff as t3', 't1.orgID = t3.orgID1','inner');
		
		$this->db->from('`tbl_org_chart` as t1');
		$this->db->where('t1.assignID' ,$assignID);
		$this->db->where('t2.assignDate <=' ,$assignDate); 
   		$query 		= $this->db->get();
 		$data 		= $query->result(); 
		 
 		//return ($query->num_rows() > 0) ? $data : NULL;
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	/*function getCategoryTree($level = 0 , $prefix = '' , $assignID=0 ,$assignDate) {
		
		$token	= $this->input->get('token');
		$rows = $this->db
			//->select('t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName, t2.staffFName as staffFName1 , t2.staffLName as staffLName11
//, t3.staffFName as staffFName2 , t3.staffLName as staffLName22')
			->select('t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t3.assignDate')
			//->where('t1.upperOrgID_modify', $level)
			->where('t1.upperOrgID', $level)
			->where('t1.assignID', $assignID)
			->where('t3.assignDate', $assignDate)
			//->join('tbl_staff as t2' , 't1.managerID = t2.ID','left')
			//->join('tbl_staff as t3' , 't1.assistantID = t3.ID','left')
			->join('tbl_orgchart_assigndate as  t3' , 't1.assignID =  t3.assignID','left')
			->order_by('orgID','asc')
			->get('tbl_org_chart as t1')
 			->result();
		 ;
		$tree = '';
		 
		if (count($rows) > 0) {
 			$i = 1;
  			foreach ($rows as $key  =>$row) {//<i class="fa fa-caret-right"></i>
				 
 				$button_view	= base_url().'organization/view?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID;
				$button_edit	= base_url().'organization/edit?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID.'&upperOrgID='.$row->upperOrgID;
				$button_delete	= base_url().'organization/delete?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID;
 					//$tree .= $prefix  . $row->orgShortName   . "\n";
					// Append subcategories
   					$tree .= '<tr >';
  						$tree .= '<td>';
						if($row->upperOrgID ==0){
							$tree .=   '<i class="fa fa-caret-down"></i> '  . $row->orgShortName ;
						}else{
							$tree .=   $prefix.'&nbsp;'. $row->orgShortName  ;
						}	
 						$tree .= '</td>';
						$tree .= '<td>';
							$tree .= $row->orgName   ;
						$tree .= '</td>';
 						$tree .= '<td>';
 						$tree .= '</td>'; 
 						$tree .= '</tr>';
 					$tree .= $this->getCategoryTree($row->orgID, $prefix . '--',$row->assignID ,$assignDate);
  			}
 		}
		return $tree;
		
	}*/
	
   
} 