<?php

class Report extends MY_Controller {
   
    function __construct() {
    parent::__construct();
     	$this->load->model('report_model');
		$this->load->model('organization/organization_model');
    }
 
    public function index() {
		
		/*$max 	= $this->position_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
		}else{
 			$max_id  =  $max+1;
 		}
		
        $xcrud		=	xcrud_get_instance();
        
		$xcrud->table('tbl_position'); 
        $arr		=	array( "positionName"=>"ชื่อตำแหน่งงาน" );
		$xcrud->pass_var('positionID',$max_id,'create'); 
		$xcrud->validation_required('positionName') ;
		
 		$xcrud->label($arr);
        $xcrud->columns($arr); 
        $xcrud->fields($arr); 
          
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
		 
        $this->template->load('template/admin', 'position',$data);*/
		
		$data['data']	=	'รายงาน / ผังโครงสร้างองค์กร';
		 
        $this->template->load('template/admin', 'report',$data);
		
    }
	
	public function organization() {
 	
 		$data['actions']	= 'add';
		
		$data['data']		= 'รายงาน / ผังโครงสร้างองค์กร';
		$data['data1']		= 'ค้นหารายงาน / ผังโครงสร้างองค์กร'; 
		$data['data2']		= '';
		$data['assignID']  =  '' ;
		
		// format date : 2015-05-01
		$assignDate			= $this->input->get_post("assignDate"); 
		
		if($assignDate !=''){
			
			$date_start 		= explode('/',$assignDate);  
			  
			$strYear_s 			= $date_start[2];
			$strMonth_s			= $date_start[1];
			$strDay_s			= $date_start[0];
			 
			
			$date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s; 
			
			$get_data			= $this->report_model->check_orgchart_assigndate($date_start); 
			 
			echo $assignID		=	$get_data['assignID'];
			echo $assignDate		=	$get_data['assignDate'];
			
			
		}else{
			$assignID		=	'';
			$assignDate		=	'';
		}
		$data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , $assignID ,$upperOrgID='' );#Dropdown  $token
		
		
		
        $this->template->load('template/admin', 'report',$data);
		
	}
	
	public function check_add(){
	    
		$data['date_start'] = $this->input->get_post("assignDate"); 
		$data['data1']		= 'แสดงรายงาน / ผังโครงสร้างองค์กร';
		
		
		$action 		= $this->input->get_post('action');
		
		// format date : 2015-05-01
		$date_start 		= explode('/',$data['date_start']);  
 		  
		$strYear_s 			= $date_start[2];
		$strMonth_s			= $date_start[1];
		$strDay_s			= $date_start[0];
		 
		
		$date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s; 
		
		$get_data			= $this->report_model->check_orgchart_assigndate($date_start); 
		 
		$assignID		=	$get_data['assignID'];
		$assignDate		=	$get_data['assignDate'];
		$ddl_organization	= '';
		//$get_orgchart_tree	= $this->report_model->check_orgchart_tree($assignID,$assignDate);  
		//$data['print_data_tree'] = $this->report_model->getCategoryTree($level = 0, $prefix = '' , $assignID ,$assignDate ); 
		
		
		$data_tree  = 	$this->organization_model->categories($assignID,$ddl_organization); 
		//$data['token']	=	$token;
 		 
		$new = array();
		foreach ($data_tree as $a){
			 $new[$a['upperOrgID']][] = $a;
			 /*
			 echo "<pre>";
			 print_r($a);
			 */
		}
		$data['tree'] = $this->organization_model->createTree($new, $new[0],0); // changed createTree
		
		//echo "<pre>";
		//print_r($data['tree']);
		//echo "<br/>";
		
 		if($data['date_start'] !=''){
			//echo "succesfully";
			$data['data2']  = $data['date_start'];
			$data['data3']  = 'succesfully';
			//$data['data']   = $print_data_tree;
			//$data['data']   = json_encode($data['tree']);
			echo json_encode($data['tree']);
			
			$data['assignID']  = $assignID  ;
		}else{
			//echo 'failed';
			$data['data2']  = '';
			$data['data3']  = 'failed';
			$data['assignID']  = ''  ;
		}
		
		//$this->load->view ( 'report_from',$data); 
       // $this->template->load('template/admin', 'report',$data);
		
 	}
	
	
	public function tree() {
 	
 		$data['actions']				= 'add';
		$data['date_start'] 			= (string)$this->input->get_post("assignDate"); 
		$data['ddl_organization'] 		= $this->input->get("ddl");
		$data['data']		= 'รายงาน / ผังโครงสร้างองค์กร';
		$data['data1']		= 'ค้นหารายงาน / ผังโครงสร้างองค์กร'; 
		$data['data2']		= '';
 		
		$data['date_start']		= $data['date_start'];
        $this->load->view('report_tree',$data);
		
	}

	public function get_tree_new() {
		$date_start 		= $this->input->get("assignDate"); 
		$ddl_organization 	= $this->input->get("ddl"); 
 		$get_data			= $this->report_model->check_orgchart_assigndate($date_start);
		$assignID			= $get_data['assignID'];
		$assignDate			= $get_data['assignDate'];

		//Level 0 The TOP:
		$root  = 	$this->organization_model->getTree_new($assignID,$ddl_organization); //เพิ่มเติมส่วนการแสดงตามสายงาน 		
	}

	/*  get_tree old suksawad

	public function get_tree() {  		
		$date_start 			= $this->input->get("assignDate"); 
		$ddl_organization 		= $this->input->get("ddl"); 
		$data['data1']		= 'แสดงรายงาน / ผังโครงสร้างองค์กร';
 		//echo $date_start ;
		$action 		= $this->input->get_post('action');
 
 		$get_data			= $this->report_model->check_orgchart_assigndate($date_start); 
 		 
		$assignID		=	$get_data['assignID'];
		$assignDate		=	$get_data['assignDate'];
 		
		//$data_tree  = 	$this->organization_model->categories($assignID,$ddl_organization); //เพิ่มเติมส่วนการแสดงตามสายงาน 
		$data_tree  = 	$this->organization_model->getTree_new($assignID,$ddl_organization); //เพิ่มเติมส่วนการแสดงตามสายงาน
		//print_r($data_tree); 
		
		//$data['token']	=	$token;
		
		//echo "<pre>";
		//print_r($data_tree ) ;

		$new = array();
		$upperOrgID = array();
		$i = 0;
		foreach ($data_tree as $a){
			 $new[$a['upperOrgID']][] = $a; 
 			 $upperOrgID[$i] = $a['upperOrgID']; // for get first upperOrgID of ddl
 			 $i = $i+1;
			 //echo "<pre>";
			 //print_r($a);
		}
		// echo "<pre>";
		// print_r($new[3]);
		// echo "All <br>";
		// print_r($new);


		$data['tree'] = $this->organization_model->createTree($new, $new[$upperOrgID[0]],0,$date_start); //$assignDate changed createTree
 		//echo "<pre>";
	    //print_r($data['tree']);
		
		
 		if($date_start !=''){
			//echo "succesfully";
			$data['data2']  = $date_start ;
			$data['data3']  = 'succesfully';
			//echo json_encode($data['tree']);
			echo json_encode($data['tree']);
		}else{
			//echo 'failed';
			$data['data2']  = '';
			$data['data3']  = 'failed';
		}
		
		//$this->template->load('template/admin', 'report_tree',$data);
		
 	}*/

 	public function get_tree(){ //get_tree ehr

		$date_start 			= $this->input->get("assignDate"); 
		$ddl_organization 		= $this->input->get("ddl"); 
		$data['data1']		= 'แสดงรายงาน / ผังโครงสร้างองค์กร';
		$action 		= $this->input->get_post('action'); 
 		$get_data		= $this->report_model->check_orgchart_assigndate($date_start); 
		$assignID		=	$get_data['assignID'];
		$assignDate		=	$get_data['assignDate'];
		$data_tree  = 	$this->organization_model->getTree_new($assignID,$ddl_organization); //เพิ่มเติมส่วนการแสดงตามสายงาน 
		$new = array();
		$upperOrgID = array();
		$i = 0;
		foreach ($data_tree as $a){
			 $new[$a['upperOrgID']][] = $a; 
 			 $upperOrgID[$i] = $a['upperOrgID']; // for get first upperOrgID of ddl
 			 $i = $i+1;
		}

		$data['tree'] = $this->organization_model->createTree($new, $new[$upperOrgID[0]],0,$date_start); //$assignDate changed createTree
		
 		if($date_start !=''){
			//echo "succesfully";
			$data['data2']  = $date_start ;
			$data['data3']  = 'succesfully';
			echo  json_encode($data['tree']);
			/*
			echo "<pre>";
			print_r($data['tree']);
			echo "</pre>";
			*/
		}else{
			//echo 'failed';
			$data['data2']  = '';
			$data['data3']  = 'failed';
		}
		
		//$this->template->load('template/admin', 'report_tree',$data);
		
 	}


	
	public function get_ddl_organization(){
		
		$date_start 		= $this->input->post("assignDate"); 
  		 
 		
		if($date_start !=''){
			
			$date_start 		= explode('/',$date_start);  
			  
			$strYear_s 			= $date_start[2];
			$strMonth_s			= $date_start[1];
			$strDay_s			= $date_start[0];
			 
			
			$date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s; 
			
			$get_data			= $this->report_model->check_orgchart_assigndate($date_start); 
			 
			$assignID		=	$get_data['assignID'];
			$assignDate		=	$get_data['assignDate'];
			//$orgID		=	$get_data['orgID'];
			$data['assignID']=$get_data['assignDate'];
			
		}else{
			$assignID		=	'';
			$assignDate		=	'';
		}
		 
		 $data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , $assignID ,$upperOrgID='' );#Dropdown 
		 
		 echo  $data['dropdown_org_chart'];
		 //echo  json_encode($data['assignID']);
		
	}
     
	public function get_ddl_organization_new(){
		
		$date_start 		= $this->input->post("assignDate"); 
  		 
 		
		if($date_start !=''){
			
			$date_start 		= explode('-',$date_start);  
			  
			$strYear_s 			= $date_start[2];
			$strMonth_s			= $date_start[1];
			$strDay_s			= $date_start[0];
			 
			
			$date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s; 
			
			$get_data			= $this->report_model->check_orgchart_assigndate($date_start); 
			 
			$assignID		=	$get_data['assignID'];
			$assignDate		=	$get_data['assignDate'];
			//$orgID		=	$get_data['orgID'];
			$data['assignID']=$get_data['assignDate'];
			
		}else{
			$assignID		=	'';
			$assignDate		=	'';
		}
		 
		 $data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , $assignID ,$upperOrgID='' );#Dropdown 
		 
		 echo  $data['dropdown_org_chart'];
		 //echo  json_encode($data['assignID']);
		
	}
     
   
} 
?>