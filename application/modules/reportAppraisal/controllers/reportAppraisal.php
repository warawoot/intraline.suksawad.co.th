<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReportAppraisal extends MX_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model('reportAppraisalmodel');   
        $this->load->model('rate/rate_model');      
        $this->load->model('reg/staff_model');      
     	$this->load->model('organization/organization_model');   
    }

    function index() {        
		$agencies = $this->input->get_post('agencies');
        $data['dropdown_org_chart'] = ""; 
        if($this->session->userdata('roleID') == 3) {
		    $data['dropdown_org_chart'] = $this->organization_model->getDropdownTree(0 , $prefix = '' , $type='sub' ,$agencies);#Dropdown                  
            $this->template->load('template/admin-fluid', 'reportAppraisalFull',$data);
        } else {
            echo "<div align=center>Cannot Access This Report</div>";
        }
    }   

    function reportM() {               
		$agencies = $this->input->get_post('agencies');
        if($agencies != "")
        {
            $ag = $agencies.$this->organization_model->getSubOrg($agencies);
            $data['reportM'] = $this->getReportM($ag);
        }

        $this->template->load('template/blank-fluid', 'reportAppraisalM',$data);
    }   
  
    function reportS() {               
		$agencies = $this->input->get_post('agencies');
        if($agencies != "")
        {
            $ag = $agencies.$this->organization_model->getSubOrg($agencies);
            $data['reportS'] = $this->getReportS($ag);
        }

        $this->template->load('template/blank-fluid', 'reportAppraisalS',$data);
    }
    
    function reportO() {               
		$agencies = $this->input->get_post('agencies');
        if($agencies != "")
        {
            $ag = $agencies.$this->organization_model->getSubOrg($agencies);
            $data['reportO'] = $this->getReportO($ag);
        }

        $this->template->load('template/blank-fluid', 'reportAppraisalO',$data);
    }
    
    function excelM(){
        $agencies = $this->input->get_post('agencies');
        if($agencies != "")
        {
            $ag = $agencies.$this->organization_model->getSubOrg($agencies);
            $data['reportM'] = $this->getReportM($ag);
        }
 
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=M.xls");  //File name extension was wrong
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        
        $this->load->view('excelAppraisalM',$data);
    }

    function excelS(){
        $agencies = $this->input->get_post('agencies');
        if($agencies != "")
        {
            $ag = $agencies.$this->organization_model->getSubOrg($agencies);
            $data['reportS'] = $this->getReportS($ag);
        }
 
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=S.xls");  //File name extension was wrong
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        
        $this->load->view('excelAppraisalS',$data);
    }

    function excelO(){
        $agencies = $this->input->get_post('agencies');
        if($agencies != "")
        {
            $ag = $agencies.$this->organization_model->getSubOrg($agencies);
            $data['reportO'] = $this->getReportO($ag);
        }
 
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=O.xls");  //File name extension was wrong
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        
        $this->load->view('excelAppraisalO',$data);
    }

    private function getReportM($ag)
    {   
        $reportM = $this->reportAppraisalmodel->get_reportAppraisal($ag,'3',2);     
        $resultArray = array(); //$reportM;
        foreach($reportM as $row){			
            //LOVE
            if($row->A_Love != '0' && $row->A_Love != null) {
                $love = $row->A_Love;
                $love_modified = ($row->A_Love != $row->Love) ? "modified" : "";
            } else {
                $love = $row->Love;
                $love_modified = "";
            }
            $A11 = round($love,2);
            ($A11 == null) ? $A1 = "-" : $A1 = number_format($A11,2);
            
            $B11 = $A1 * 0.05;
            ($B11 == null) ? $B1 = "-" : $B1 = number_format($B11,2);

            //KPI
            $KPI = ($row->A_KPI != '0' && $row->A_KPI != null) ? $row->A_KPI : $row->KPI;
            $A22 = round($KPI,2);
            ($A22 == null) ? $A2 = "-" : $A2 = number_format($A22,2);
            
            $B22 = round($A2 * 0.3,1);
            ($B22 == null) ? $B2 = "-" : $B2 = number_format($B22,2);

            //FC
            $Competency = ($row->A_Competency != '0' && $row->A_Competency != null) ? $row->A_Competency : $row->Competency;		
            $A33 =  round($Competency,2);
            ($A33 == null) ? $A3 = "-" : $A3 = number_format($A33,2);

            $B33 = $A3 * 0.3;
            ($B33 == null) ? $B3 = "-" : $B3 = number_format($B33,2);

            //MC
            $CompetencyM = ($row->A_CompetencyM != '0' && $row->A_CompetencyM != null) ? $row->A_CompetencyM : $row->CompetencyM;			
            $A44 =  round($CompetencyM,2);
            ($A44 == null) ? $A4 = "-" : $A4 = number_format($A44,2);

            $B44 = $A4 * 0.3;
            ($B44 == null) ? $B4 = "-" : $B4 = number_format($B44,2);

            //$C1 = ($row->A_C1 != '0') ? $row->A_C1 : $row->C1;						
            $C1 = round($row->C1,2);
            $sum = round($B2 + $B3 + $B4 + $C1,2); // ตัด $B1 + 

            $score = number_format($sum,2);
            $calPromoteM = $this->calPromoteM($sum,$row->salary);

            $rowM = array(   
                'staffID' => $row->staffID,
                'staffName' => ($row->staffFName." ".$row->staffLName),
                'orgName' => $row->orgName,
                'A1' => $A1,
                'B1' => $B1,
                'A2' => $A2,
                'B2' => $B2,
                'A3' => $A3,
                'B3' => $B3,
                'A4' => $A4,
                'B4' => $B4,
                'C1' => $C1,
                'love_modified'=> $love_modified,
                'sum' => $sum,
                'salary' => $row->salary,
                'score' => $score,
                'grade' => $calPromoteM["grade"],
                'newSalary' => number_format($calPromoteM["newSalary"],0),
                'bonus' => number_format($calPromoteM["bonus"],0),
                'salaryAdj' => $row->salaryAdj,
                'salaryFinal' => number_format($calPromoteM["newSalary"] + $row->salaryAdj),
                'bonusAdj' => $row->bonusAdj,                                
                'bonusFinal' => number_format($calPromoteM["bonus"] + $row->bonusAdj)
            ); 
            array_push($resultArray,$rowM);
        }     
        return $resultArray;  
    }

    public function calPromoteM($score, $salary)
    {
        if($score >= 90 ){
            $grade = 'A+';
            $newSalary = $salary * 1.06;
            $bonus = $salary * 1.2 * 1.0;
        }else if($score >= 80){
            $grade = 'A';
            $newSalary = $salary * 1.05;
            $bonus = $salary * 1.1 * 1.0;
        }else if($score >= 75){
            $grade = 'B+';
            $newSalary = $salary * 1.04;
            $bonus = $salary * 1.0 * 1.0;
        }else if($score >= 70){
            $grade = 'B';
            $newSalary = $salary * 1.03;
            $bonus = $salary * 0.9 * 1.0;
        }else if($score >= 65){
            $grade = 'C+';
            $newSalary = $salary * 1.02;
            $bonus = $salary * 0.8 * 1.0;
        }else if($score >= 60){
            $grade = 'C';
            $newSalary = $salary * 1.01;
            $bonus = $salary * 0.7 * 1.0;
        }else if($score >= 50){
            $grade = 'D';
            $newSalary = $salary * 1;
            $bonus = $salary * 0;
        }else{
            $grade = 'F';
            $newSalary = $salary * 1;
            $bonus = $salary * 0;
        }
        $promoteData = array(   
            'grade' => $grade,
            'newSalary' => $newSalary,
            'bonus' => $bonus
        ); 
        return $promoteData;
    }

    private function getReportS($ag){
        
        $reportS = $this->reportAppraisalmodel->get_reportAppraisal($ag,'2',2); 
        $resultArray = array();
        foreach($reportS as $row){				
            
            //LOVE
            if($row->A_Love != '0' && $row->A_Love != null) {
                $love = $row->A_Love;
                $love_modified = ($row->A_Love != $row->Love) ? "modified" : "";
            } else {
                $love = $row->Love;
                $love_modified = "";
            }
            $A11 = round($love,2);
            ($A11 == null) ? $A1 = "-" : $A1 = number_format($A11,2);
            
            $B11 = round($A1,2);
            ($B11 == null) ? $B1 = "-" : $B1 = number_format($B11,2);

            //KPI
            if($row->A_KPI != '0' && $row->A_KPI != null) {
                $KPI = $row->A_KPI;
                $KPI_modified = ($row->A_KPI != $row->KPI) ? "modified" : "";
            } else {
                $KPI = $row->KPI;
                $KPI_modified = "";
            }
            $A22 = round($KPI,2);
            ($A22 == null) ? $A2 = "-" : $A2 = number_format($A22,2);
            
            $B22 = round($A2 * 0.3,2);
            ($B22 == null) ? $B2 = "-" : $B2 = number_format($B22,2);

            //FC
            if($row->A_Competency != '0' && $row->A_Competency != null) {
                $Competency = $row->A_Competency;
                $Competency_modified = ($row->A_Competency != $row->Competency) ? "modified" : "";
            } else {
                $Competency = $row->Competency;
                $Competency_modified = "";
            }
            $A33 =  round($Competency,2);
            ($A33 == null) ? $A3 = "-" : $A3 = number_format($A33,2);

            $B33 = round($A3 * 0.25,2);
            ($B33 == null) ? $B3 = "-" : $B3 = number_format($B33,2);

            //MC
            if($row->A_CompetencyM != '0' && $row->A_CompetencyM != null) {
                $CompetencyM = $row->A_CompetencyM;
                $CompetencyM_modified = ($row->A_CompetencyM != $row->CompetencyM) ? "modified" : "";
            } else {
                $CompetencyM = $row->CompetencyM;
                $CompetencyM_modified = "";
            }
            $A44 =  round($CompetencyM,2);
            ($A44 == null) ? $A4 = "-" : $A4 = number_format($A44,2);

            $B44 = round($A4 * 0.2,2);
            ($B44 == null) ? $B4 = "-" : $B4 = number_format($B44,2);

            $C1 = round($row->C1,2);
            $C2 = round($row->C2,2);

            $sum = round($B1 + $B2 + $B3 + $B4 + $C1 + $C2, 2);

            $score = number_format($sum,2);

            $calPromoteS = $this->calPromoteS($sum,$row->salary);

            $rowS = array(   
                'staffID' => $row->staffID,
                'staffName' => ($row->staffFName." ".$row->staffLName),
                'orgName' => $row->orgName,
                'A1' => $A1,
                'B1' => $B1,
                'A2' => $A2,
                'B2' => $B2,
                'A3' => $A3,
                'B3' => $B3,
                'A4' => $A4,
                'B4' => $B4,
                'C1' => $C1,
                'C2' => $C2,    
                'sum' => $sum,
                'love_modified'=> $love_modified,
                'KPI_modified' => $KPI_modified,
                'Competency_modified' => $Competency_modified,
                'CompetencyM_modified' => $CompetencyM_modified,
                'salary' => $row->salary,
                'score' => $score,
                'grade' => $calPromoteS["grade"],
                'newSalary' => number_format($calPromoteS["newSalary"],0),
                'bonus' => number_format($calPromoteS["bonus"],0),
                'salaryAdj' => $row->salaryAdj,
                'salaryFinal' => number_format($calPromoteS["newSalary"] + $row->salaryAdj),
                'bonusAdj' => $row->bonusAdj,                                
                'bonusFinal' => number_format($calPromoteS["bonus"] + $row->bonusAdj)
            ); 
            array_push($resultArray,$rowS);
        }
        return $resultArray;
    }

    public function calPromoteS($score, $salary)
    {
        if($score >= 90 ){
            $grade = 'A+';
            $newSalary = $salary * 1.06;
            $bonus = $salary * 1.1 * 1.0;
        }else if($score >= 80){
            $grade = 'A';
            $newSalary = $salary * 1.05;
            $bonus = $salary * 1.0 * 1.0;
        }else if($score >= 75){
            $grade = 'B+';
            $newSalary = $salary * 1.04;
            $bonus = $salary * 0.9 * 1.0;
        }else if($score >= 70){
            $grade = 'B';
            $newSalary = $salary * 1.03;
            $bonus = $salary * 0.8 * 1.0;
        }else if($score >= 65){
            $grade = 'C+';
            $newSalary = $salary * 1.02;
            $bonus = $salary * 0.7 * 1.0;
        }else if($score >= 60){
            $grade = 'C';
            $newSalary = $salary * 1.01;
            $bonus = $salary * 0.6 * 1.0;
        }else if($score >= 50){
            $grade = 'D';
            $newSalary = $salary * 1;
            $bonus = $salary * 0;
        }else{
            $grade = 'F';
            $newSalary = $salary * 1;
            $bonus = $salary * 0;
        }
        $promoteData = array(   
            'grade' => $grade,
            'newSalary' => $newSalary,
            'bonus' => $bonus
        ); 
        return $promoteData;
    }

    private function getReportO($ag){
        
        $reportO = $this->reportAppraisalmodel->get_reportAppraisal($ag,'1',1);         
        $resultArray = array();

        foreach($reportO as $row){				
            
            //LOVE
            if($row->A_Love != '0' && $row->A_Love != null) {
                $love = $row->A_Love;
                $love_modified = ($row->A_Love != $row->Love) ? "modified" : "";
            } else {
                $love = $row->Love;
                $love_modified = "";
            }
            $A11 = round($love,2);
            ($A11 == null) ? $A1 = "-" : $A1 = number_format($A11,2);
            
            $B11 = round($A1,2);
            ($B11 == null) ? $B1 = "-" : $B1 = number_format($B11,2);

            //KPI
            if($row->A_KPI != '0' && $row->A_KPI != null) {
                $KPI = $row->A_KPI;
                $KPI_modified = ($row->A_KPI != $row->KPI) ? "modified" : "";
            } else {
                $KPI = $row->KPI;
                $KPI_modified = "";
            }
            $A22 = round($KPI,2);
            ($A22 == null) ? $A2 = "-" : $A2 = number_format($A22,2);
            
            $B22 = round($A2 * 0.4,2);
            ($B22 == null) ? $B2 = "-" : $B2 = number_format($B22,2);

            //FC
            if($row->A_Competency != '0' && $row->A_Competency != null) {
                $Competency = $row->A_Competency;
                $Competency_modified = ($row->A_Competency != $row->Competency) ? "modified" : "";
            } else {
                $Competency = $row->Competency;
                $Competency_modified = "";
            }
            //$Competency = ($row->A_Competency != '0' && $row->A_Competency != null) ? $row->A_Competency : $row->Competency;		
            $A33 =  round($Competency,2);
            ($A33 == null) ? $A3 = "-" : $A3 = number_format($A33,2);

            $B33 = round($A3 * 0.3,2);
            ($B33 == null) ? $B3 = "-" : $B3 = number_format($B33,2);

            $C1 = round($row->C1,2);
            $C2 = round($row->C2,2);
            $sum = round($B1 + $B2 + $B3 + $C1 + $C2,2);
            $score = number_format($sum,2);

            $calPromoteO = $this->calPromoteO($sum,$row->salary);

            $rowS = array(   
                'staffID' => $row->staffID,
                'staffName' => ($row->staffFName." ".$row->staffLName),
                'orgName' => $row->orgName,
                'A1' => $A1,
                'B1' => $B1,
                'A2' => $A2,
                'B2' => $B2,
                'A3' => $A3,
                'B3' => $B3,
                'A4' => 0,
                'B4' => 0,
                'C1' => $C1,
                'C2' => $C2,    
                'sum' => $sum,
                'love_modified'=> $love_modified,
                'KPI_modified' => $KPI_modified,
                'Competency_modified' => $Competency_modified,
                'salary' => $row->salary,
                'score' => $score,
                'grade' => $calPromoteO["grade"],
                'newSalary' => number_format($calPromoteO["newSalary"],0),
                'bonus' => number_format($calPromoteO["bonus"],0),
                'salaryAdj' => $row->salaryAdj,
                'salaryFinal' => number_format($calPromoteO["newSalary"] + $row->salaryAdj),
                'bonusAdj' => $row->bonusAdj,                                
                'bonusFinal' => number_format($calPromoteO["bonus"] + $row->bonusAdj)
            ); 
            array_push($resultArray,$rowS);
        }
        return $resultArray;
    }

    public function calPromoteO($score, $salary)
    {
        if($score >= 90 ){
            $grade = 'A+';
            $newSalary = $salary * 1.06;
            $bonus = $salary * 1.1 * 1.0;
        }else if($score >= 80){
            $grade = 'A';
            $newSalary = $salary * 1.05;
            $bonus = $salary * 1.0 * 1.0;
        }else if($score >= 75){
            $grade = 'B+';
            $newSalary = $salary * 1.04;
            $bonus = $salary * 0.9 * 1.0;
        }else if($score >= 70){
            $grade = 'B';
            $newSalary = $salary * 1.03;
            $bonus = $salary * 0.8 * 1.0;
        }else if($score >= 65){
            $grade = 'C+';
            $newSalary = $salary * 1.02;
            $bonus = $salary * 0.7 * 1.0;
        }else if($score >= 60){
            $grade = 'C';
            $newSalary = $salary * 1.01;
            $bonus = $salary * 0.6 * 1.0;
        }else if($score >= 50){
            $grade = 'D';
            $newSalary = $salary * 1;
            $bonus = $salary * 0;
        }else{
            $grade = 'F';
            $newSalary = $salary * 1;
            $bonus = $salary * 0;
        }
        $promoteData = array(   
            'grade' => $grade,
            'newSalary' => $newSalary,
            'bonus' => $bonus
        ); 
        return $promoteData;
    }
    
    public function updateData() {
        $A1 = 0;
        $A2 = 0;
        $A3 = 0;
        $C1 = 0;
        $C2 = 0;
        $salary = 0;
        $salaryAdj = 0;
        $bonusAdj = 0;

        $staffID = $this->input->get_post('staffID');
        $A1 = $this->input->get_post('A1');
        $A2 = $this->input->get_post('A2');
        $A3 = $this->input->get_post('A3');
        $A4 = $this->input->get_post('A4');
        $C1 = $this->input->get_post('C1');
        $C2 = $this->input->get_post('C2');
        $salary = $this->input->get_post('salary');
        $salaryAdj = $this->input->get_post('salaryAdj');
        $bonusAdj = $this->input->get_post('bonusAdj');

        $inputArray  = 	array(
                        'evalYear'  => 2561,
                        'evalRound' => 2,
                        'evalStep'  => 1,
                        'rankID'    => 2,
                        'staffID'   => $staffID,
                        'approverID'=> $this->session->userdata('userID'),
                        'A1'        => $A1== null ? 0 : $A1,
                        'A2'        => $A2== null ? 0 : $A2,
                        'A3'        => $A3== null ? 0 : $A3,
                        'A4'        => $A4== null ? 0 : $A4,
                        'C1'        => $C1== null ? 0 : $C1,
                        'C2'        => $C2== null ? 0 : $C2,
                        'salaryAdj' => $salaryAdj == null ? 0 : $salaryAdj,
                        'bonusAdj'  => $bonusAdj == null ? 0 : $bonusAdj                       
                );

        $this->rate_model->insert_summary_eval($inputArray);
        $this->staff_model->updateSalary($staffID, $salary);
        echo json_encode("บันทึกข้อมูลเรียบร้อยแล้ว");
    }

    function updateScore() {
        return false;
        /*
        $A1 = 0;
        $A2 = 0;
        $A3 = 0;
        $C1 = 0;
        $C2 = 0;
        foreach($this->input->get_post('M_staffID') as  $key => $value) {
            $A1 = $this->input->get_post('M_A1_'.$value);
            $A2 = $this->input->get_post('M_A2_'.$value);
            $A3 = $this->input->get_post('M_A3_'.$value);
            $A4 = $this->input->get_post('M_A4_'.$value);
            $C1 = $this->input->get_post('M_C1_'.$value);
            $C2 = 0;

            $inputArray  = 	array(
                            'evalYear'  => 2560,
                            'evalRound' => 2,
                            'evalStep'  => 1,
                            'rankID'    => 2,
                            'staffID'   => $value,
                            'approverID'=> $this->session->userdata('userID'),
                            'A1'        => $A1,
                            'A2'        => $A2,
                            'A3'        => $A3,
                            'A4'        => $A4,
                            'C1'        => $C1,
                            'C2'        => $C2
                    );
            $this->rate_model->insert_summary_eval($inputArray);

        }

        foreach($this->input->get_post('S_staffID') as  $key => $value) {
            $A1 = $this->input->get_post('S_A1_'.$value);
            $A2 = $this->input->get_post('S_A2_'.$value);
            $A3 = $this->input->get_post('S_A3_'.$value);
            $A4 = $this->input->get_post('S_A4_'.$value);
            $C1 = $this->input->get_post('S_C1_'.$value);
            $C2 = $this->input->get_post('S_C2_'.$value);

            $inputArray  = 	array(
                            'evalYear'  => 2560,
                            'evalRound' => 2,
                            'evalStep'  => 1,
                            'rankID'    => 1,
                            'staffID'   => $value,
                            'approverID'=> $this->session->userdata('userID'),
                            'A1'        => $A1,
                            'A2'        => $A2,
                            'A3'        => $A3,
                            'A4'        => $A4,
                            'C1'        => $C1,
                            'C2'        => $C2
                    );
            $this->rate_model->insert_summary_eval($inputArray);

        }

        foreach($this->input->get_post('O_staffID') as  $key => $value) {
            $A1 = $this->input->get_post('O_A1_'.$value);
            $A2 = $this->input->get_post('O_A2_'.$value);
            $A3 = $this->input->get_post('O_A3_'.$value);
            $A4 = 0;
            $C1 = $this->input->get_post('O_C1_'.$value);
            $C2 = $this->input->get_post('O_C2_'.$value);

            $inputArray  = 	array(
                            'evalYear'  => 2560,
                            'evalRound' => 2,
                            'evalStep'  => 1,
                            'rankID'    => 0,
                            'staffID'   => $value,
                            'approverID'=> $this->session->userdata('userID'),
                            'A1'        => $A1,
                            'A2'        => $A2,
                            'A3'        => $A3,
                            'A4'        => $A4,
                            'C1'        => $C1,
                            'C2'        => $C2
                    );
            $this->rate_model->insert_summary_eval($inputArray);
        }

        echo "<script>alert('Complete');document.location='".base_url()."reportAppraisal?agencies=".$this->input->get_post('agencies')."';</script>";
        */
    }

}