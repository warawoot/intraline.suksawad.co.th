<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReportAppraisalModel extends MY_Model {

    function __construct() {    	
        parent::__construct();     
        $this->table="";
        $this->pk = "";      
    }        

    function hello() {
        return "Hi how are you?";
    }

    function get_reportAppraisal($agencies,$rankID,$evalType) {        
        $sql = "SELECT 
                s.staffID,
                s.staffFName,
                s.staffLName,
                s.orgID, 
                s.salary,
                o.orgName, 
                Love.sum as Love,                   
                Competency.sum as Competency,          
                CompetencyM.sum as CompetencyM,          
                KPI.sum as KPI,
                e.A1 as A_Love,                   
                e.A2 as A_KPI,                  
                e.A3 as A_Competency,          
                e.A4 as A_CompetencyM,
                e.C1 as C1,
                e.C2 as C2,
                e.salaryAdj,
                e.bonusAdj                            
            FROM 
                tbl_staff s 
            LEFT JOIN 
                (SELECT * FROM tbl_eval_summary WHERE evalYear=2561 AND evalRound=1 AND rankID=2) e ON s.staffID = e.staffID 
            INNER JOIN 
                tbl_org_chart o ON s.orgID = o.orgID 
                LEFT JOIN 
                (
                SELECT 
                    staffID,    
                    SUM(evalScore) / 2 AS sum,
                    evalFormID 
                FROM tbl_staff 

                LEFT JOIN tbl_eval ON tbl_staff.staffID = tbl_eval.empID  
                WHERE evalFormID=8 
                GROUP BY empID
                ) AS Love ON s.staffID = Love.staffID                 
                LEFT JOIN
                (
                SELECT 
                    staffID,   
                    (SUM(evalScore) / SUM(evalVariable)) * 100 AS sum,
                    evalFormID 
                FROM tbl_staff 
                LEFT JOIN tbl_eval ON tbl_staff.staffID = tbl_eval.empID  
                WHERE evalFormID=9 AND evalType=1
                GROUP BY empID
                ) AS Competency ON s.staffID = Competency.staffID 

                LEFT JOIN
                (
                SELECT 
                    staffID,   
                    (SUM(evalScore) / SUM(evalVariable)) * 100 AS sum,
                    evalFormID 
                FROM tbl_staff 
                LEFT JOIN tbl_eval ON tbl_staff.staffID = tbl_eval.empID  
                WHERE evalFormID=9 AND evalType=2
                GROUP BY empID
                ) AS CompetencyM ON s.staffID = CompetencyM.staffID  

                LEFT JOIN
                (
                SELECT 
                    staffID,   
                    (SUM(evalScore * evalVariable) / (SUM(evalVariable)*5)) * 100 AS sum,
                    evalFormID 
                FROM tbl_staff 
                LEFT JOIN tbl_eval ON tbl_staff.staffID = tbl_eval.empID
                    WHERE evalFormID=10
                GROUP BY empID
                ) AS KPI ON s.staffID = KPI.staffID                   
            WHERE  
                s.rankID IN (".$rankID.") 
                AND s.status <> 2";

        if($agencies != null)
        {
            $sql .= " AND s.OrgID IN (".$agencies.")";
        }

        $sql .= " ORDER BY s.orgID ASC, s.staffID ASC";
        $query = $this->db->query($sql);
        if($query->num_rows())
        {
            //echo $sql;
            return $query->result();            
        }
        else {
            return null;
        }
    }

    function getOrgName() {
        $sqlOrg = "SELECT orgName,OrgID FROM tbl_org_chart";
        $orgName = $this->db->query($sqlOrg);
        return $orgName->result();  
    }   

}