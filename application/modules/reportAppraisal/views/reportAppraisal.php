<?php
	if($this->session->userdata('roleID')==3) {
		$readonly = "";
		$salarydisplay = "block";
	} else {
		$readonly = "readonly";		
		$salarydisplay = "none";
	} 
?>
<style type="text/css">
td {
	padding : 5px !important;
}

.form-control {
	color: #343232;	
}

input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
	-webkit-appearance: none;
}

.table-results tr td{
	padding:5px !important;
	vertical-align: middle !important;
}
.table-results thead th{
	border-top: none; 
	border-bottom: none;
	border: 0 !important; 
}

th {
	color:#FFF !important;
	padding : 5px !important;
	text-align:center;
}

.excel-style {
	padding : 0px !important;
}

.excel-form {
	width : 100% !important;
	line-height : 20px !important;
	height : 20px !important;
	border : none !important; 
	background-color: transparent !important; 
}

select.excel-form {
	width : 700px !important;
	line-height : 40px !important;
	height : 40px !important;
	border : none !important; 
	background-color: transparent !important; 
}

input[type=number].excel-form {
	margin-top : 10px !important;
	width : 80px !important;
	text-align:right;
	font-size:14px;
	font-weight:bold;
	display:table-cell;
}

.summary {
	padding-right : 10px !important;
	text-align:right;
	font-size:14px;
	font-weight:bold;
}

.table th {
	text-align: center;				
}

.tdC{
	text-align:center;
}

.tdR{
	text-align:right;			
}

#th{
	padding-bottom: 25px;
}

#colorG1{
	background:#f2f2f2;
}

#colorG2{
	background:#e6e6e6;
}

.textTD{
	font-weight:bold;
}

.form-control {
	color: #343232;
}

.orgName {
	font-size:8pt;
	color:#999;
}

.salary {
	display:<?=$salarydisplay?>;
	width:100px;
}
</style>
<section class="panel">      
	<div class="panel-body">
		<form action="<?=base_url('reportAppraisal') ?>" method="get" enctype="multipart/form-data">        
			<div class="form-group" >
			<div class="row" style="padding:10px">
				<div class="col-md-3"><b>หน่วยงาน</b></div>
				<div class="col-md-9">
					<select class="form-control form-control" data-type="select" name="agencies" onchange="this.form.submit();"> 
						<option value=''>กรุณาเลือก</option>
						<?=$dropdown_org_chart?>
					</select>
				</div>
			</div>            
			</div> 
		</form>
	</div>
</section>

<form action="<?=base_url('reportAppraisal') ?>/updateScore" method="post" enctype="multipart/form-data">    
<input id="agencies" name="agencies" type="hidden" value="<?=$_GET['agencies']?>"/>   
<b>รายงานคะแนนระดับ M</b><br><br> 
<table class="table table-striped">
<?php		
	if(!empty($reportM))
	{
?>
	<thead>
	<tr>
		<th rowspan="3" id="th">พนักงาน</th>
		<th colspan="2">KPIs (30%)</th>	
		<th colspan="2">FC (30%)</th>	
		<th colspan="2">MC (30%)</th>	
		<th colspan="2">LOVE (5%)</h>
		<th colspan="2" id="th">แบบสอบถาม<br>LOVE (5%)</th>		
		<th rowspan="3" id="th">ผลรวม</th>
		<th rowspan="3" id="th">เกรด</th>
		<th rowspan="3" class="salary">เงินเดือน</th>
		<th rowspan="3" class="salary">ปรับ</th>
		<th rowspan="3" class="salary">เงินเดือนใหม่</th>
	</tr>
	<tr>
		<th>รอบที่ 1</th>
		<th>รอบที่ 2</th>

		<th>รอบที่ 1</th>
		<th>รอบที่ 2</th>

		<th>รอบที่ 1</th>
		<th>รอบที่ 2</th>

		<th>รอบที่ 1</th>
		<th>รอบที่ 2</th>

		<th>รอบที่ 1</th>
		<th>รอบที่ 2</th>

	</tr>		
	<tr>
		<th>ร้อยละ</th>
		<th>ร้อยละ</th>

		<th>ร้อยละ</th>
		<th>ร้อยละ</th>

		<th>ร้อยละ</th>
		<th>ร้อยละ</th>

		<th>ร้อยละ</th>
		<th>ร้อยละ</th>

		<th>คะแนน</th>
		<th>คะแนน</th>
	</tr>		
	</thead>
	<tbody>
	<?php		
		foreach($reportM as $row){				

			//LOVE
			$love = ($row->A_Love != '0') ? $row->A_Love : $row->Love;
			$A11 = round($love,2);
			($A11 == null) ? $A1 = "-" : $A1 = $A11;
			
			$B11 = $A1 * 0.05;
			($B11 == null) ? $B1 = "-" : $B1 = $B11;

			//KPI
			$KPI = ($row->A_KPI != '0') ? $row->A_KPI : $row->KPI;
			$A22 = round($KPI,2);
			($A22 == null) ? $A2 = "-" : $A2 = $A22;
			
			$B22 = round($A2 * 0.3,1);
			($B22 == null) ? $B2 = "-" : $B2 = $B22;

			//FC
			$Competency = ($row->A_Competency != '0') ? $row->A_Competency : $row->Competency;		
			$A33 =  round($Competency,2);
			($A33 == null) ? $A3 = "-" : $A3 = $A33;

			$B33 = $A3 * 0.3;
			($B33 == null) ? $B3 = "-" : $B3 = $B33;

			//MC
			$CompetencyM = ($row->A_CompetencyM != '0') ? $row->A_CompetencyM : $row->CompetencyM;			
			$A44 =  round($CompetencyM,2);
			($A44 == null) ? $A4 = "-" : $A4 = $A44;

			$B44 = $A4 * 0.3;
			($B44 == null) ? $B4 = "-" : $B4 = $B44;

			$C1 = ($row->A_C1 != '') ? $row->A_C1 : $row->C1;						
			$C1 = round($C1,2);

			$sum = round($B1 + $B2 + $B3 + $B4 + $C1,2);

			$score = $sum;

			($row->salary == null) ? $salary = "-" : $salary = number_format($row->salary,0);

			if($score >= 90 ){
				$grade = 'A+'; // 6.0%
				($row->salary == null) ? $newsalary = "-" : $newsalary = $row->salary * 1.06;
			}else if($score >= 80){
				$grade = 'A';  // 5.0%
				($row->salary == null) ? $newsalary = "-" : $newsalary = $row->salary * 1.05;
			}else if($score >= 75){
				$grade = 'B+'; // 4.0%
				($row->salary == null) ? $newsalary = "-" : $newsalary = $row->salary * 1.04;
			}else if($score >= 70){
				$grade = 'B';  // 3.0%
				($row->salary == null) ? $newsalary = "-" : $newsalary = $row->salary * 1.03;
			}else if($score >= 65){
				$grade = 'C+'; // 2.0%
				($row->salary == null) ? $newsalary = "-" : $newsalary = $row->salary * 1.02;
			}else if($score >= 60){
				$grade = 'C';  // 1.0%
				($row->salary == null) ? $newsalary = "-" : $newsalary = $row->salary * 1.01;
			}else if($score >= 50){
				$grade = 'D';  // 0.0%
				($row->salary == null) ? $newsalary = "-" : $newsalary = $row->salary * 1.00;
			}else{
				$grade = 'F';  // 0.0%
				($row->salary == null) ? $newsalary = "-" : $newsalary = $row->salary * 1.00;
			}

		?>
				<tr>					
                    <td id="tdC">
					<input type="hidden" name="M_staffID[]" id="M_staffID[]" value="<?=$row->staffID?>"> 
					<?php echo $row->staffFName.' '.$row->staffLName ?><br>
					<span class='orgName'><?= $row->orgName?></span>
					</td>

					<td id="colorG1" class="tdR"><span id="M_B2_<?=$row->staffID?>"><?=$B2?></span></td>	
					<td class="tdR">-</td>

					<td id="colorG1" class="tdR"><span id="M_B3_<?=$row->staffID?>"><?=$B3?></span></td>	
					<td class="tdR">-</td>

					<td id="colorG1" class="tdR"><span id="M_B4_<?=$row->staffID?>"><?=$B4?></span></td>	
					<td class="tdR">-</td>

					<td id="colorG1" class="tdR"><span id="M_B1_<?=$row->staffID?>"><?=$B1?></span></td>	
					<td class="tdR">-</td>

					<td style="text-align:center">
						<input type="number" 
							min="0" 
							max="5" 
							step="0.01" 
							value="<?=$C1?>"  
							name="M_C1_<?=$row->staffID?>" 
							id="M_C1_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,5);calculateScoreM(<?=$row->staffID?>);" 
							<?=$readonly?>
							/>					
					</td>	
					<td style="text-align:center">
						<input type="number" 
							min="0" 
							max="5" 
							step="0.01" 
							value="0"  
							name="M_C2_<?=$row->staffID?>" 
							id="M_C2_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,5);calculateScoreM(<?=$row->staffID?>);" 
							<?=$readonly?>
							/>					
					</td>	

					<td class="tdR"><span id="M_totalPercent_<?=$row->staffID?>"><?=$sum?></span></td>	
					<td class="tdC textTD" id="colorG2"><span id="M_grade_<?=$row->staffID?>"><?=$grade?></span></td>
					<td class="salary tdR"><?=$salary?></td>
					<td style="text-align:center">
						<input type="number" 
							step="1" 
							value="0"  
							name="SalaryAdjust_<?=$row->staffID?>" 
							id="SalaryAdjust_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="calculateSalary(<?=$row->staffID?>);" 
							<?=$readonly?>
							/>					
					</td>	
					<td class="salary tdR">
						<span id="NewSalary_<?=$row->staffID?>"><?=$newsalary?></span>
						<input type="hidden" id="NewSalaryOriginal_<?=$row->staffID?>" value="<?=$newsalary?>">
					</td>
				</tr>
		<?php  } ?>
	</tbody>
<?php } else { ?>
	<tr><td style="text-align:center"><h4>ไม่พบข้อมูล</h4></td></tr>
<?php } ?>
</table>

<br><b>รายงานคะแนนระดับ S</b><br><br> 
<table class="table table-striped">
<?php		
	if(!empty($reportS))
	{
?>
	<thead>
	<tr>
		<th rowspan="2" id="th">พนักงาน</th>
		<th colspan="2">KPIs (30%)</th>	
		<th colspan="2">FC (30%)</th>	
		<th colspan="2">MC (20%)</th>	
		<th colspan="2">LOVE (5%)</h>
		<th rowspan="2" id="th">แบบสอบถาม<br>LOVE (5%)</th>		
		<th rowspan="2" id="th">อื่นๆ (10%)</th>		
		<th rowspan="2" id="th">ผลรวม</th>
		<th rowspan="2" id="th">เกรด</th>
	</tr>
	<tr>
		<th>คะแนน</th>
		<th>ร้อยละ</th>
		<th>คะแนน</th>
		<th>ร้อยละ</th>
		<th>คะแนน</th>
		<th>ร้อยละ</th>				
		<th>คะแนน</th>
		<th>ร้อยละ</th>
	</tr>		
	</thead>
	<tbody>
	<?php		
		foreach($reportS as $row){				

			//LOVE
			$love = ($row->A_Love != '0') ? $row->A_Love : $row->Love;
			$A11 = round($love,2);
			($A11 == null) ? $A1 = "-" : $A1 = $A11;
			
			$B11 = round($A1 * 0.05,2);
			($B11 == null) ? $B1 = "-" : $B1 = $B11;

			//KPI
			$KPI = ($row->A_KPI != '0') ? $row->A_KPI : $row->KPI;
			$A22 = round($KPI,2);
			($A22 == null) ? $A2 = "-" : $A2 = $A22;
			
			$B22 = round($A2 * 0.3,2);
			($B22 == null) ? $B2 = "-" : $B2 = $B22;

			//FC
			$Competency = ($row->A_Competency != '0') ? $row->A_Competency : $row->Competency;		
			$A33 =  round($Competency,2);
			($A33 == null) ? $A3 = "-" : $A3 = $A33;

			$B33 = round($A3 * 0.3,2);
			($B33 == null) ? $B3 = "-" : $B3 = $B33;

			//MC
			$CompetencyM = ($row->A_CompetencyM != '0') ? $row->A_CompetencyM : $row->CompetencyM;			
			$A44 =  round($CompetencyM,2);
			($A44 == null) ? $A4 = "-" : $A4 = $A44;

			$B44 = round($A4 * 0.2,2);
			($B44 == null) ? $B4 = "-" : $B4 = $B44;

			$C1 = ($row->A_C1 != '0') ? $row->A_C1 : $row->C1;						
			$C1 = round($C1,2);

			$C2 = ($row->A_C2 != '0') ? $row->A_C2 : $row->C2;						
			$C2 = round($C2,2);

			$sum = round($B1 + $B2 + $B3 + $B4 + $C1 + $C2, 2);

			$score = $sum;

			if($score >= 90 ){
				$grade = 'A+';
			}else if($score >= 80){
				$grade = 'A';
			}else if($score >= 75){
				$grade = 'B+';
			}else if($score >= 70){
				$grade = 'B';
			}else if($score >= 65){
				$grade = 'C+';
			}else if($score >= 60){
				$grade = 'C';
			}else if($score >= 50){
				$grade = 'D';
			}else{
				$grade = 'F';
			}
		?>
				<tr>					
                    <td id="tdC">
					<input type="hidden" name="S_staffID[]" id="S_staffID[]" value="<?=$row->staffID?>">
					<?php echo $row->staffFName.' '.$row->staffLName ?>
					<br><span class='orgName'><?= $row->orgName?></span>
					</td>

					<td class="tdC">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$A2?>"  
							name="S_A2_<?=$row->staffID?>" 
							id="S_A2_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreS(<?=$row->staffID?>);"
							/>					
					</td>
					<td id="colorG1" class="tdR"><span id="S_B2_<?=$row->staffID?>"><?=$B2?></span></td>	

					<td class="tdC">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$A3?>"  
							name="S_A3_<?=$row->staffID?>" 
							id="S_A3_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreS(<?=$row->staffID?>);"
							/>										
					</td>					
					<td id="colorG1" class="tdR"><span id="S_B3_<?=$row->staffID?>"><?=$B3?></span></td>	

					<td class="tdC">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$A4?>"  
							name="S_A4_<?=$row->staffID?>" 
							id="S_A4_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreS(<?=$row->staffID?>);"
							/>										
					</td>					
					<td id="colorG1" class="tdR"><span id="S_B4_<?=$row->staffID?>"><?=$B4?></span></td>	

					<td class="tdC">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$A1?>"  
							name="S_A1_<?=$row->staffID?>" 
							id="S_A1_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreS(<?=$row->staffID?>);" 
							/>					
					</td>															
					<td id="colorG1" class="tdR"><span id="S_B1_<?=$row->staffID?>"><?=$B1?></span></td>	

					</td>	

					<td style="text-align:center">
						<input type="number" 
							min="0" 
							max="5" 
							step="0.01" 
							value="<?=$C1?>"  
							name="S_C1_<?=$row->staffID?>" 
							id="S_C1_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,5);calculateScoreS(<?=$row->staffID?>);" 
							<?=$readonly?>
							/>					
					</td>	
					<td style="text-align:center">
						<input type="number" 
							min="0" 
							max="10" 
							step="0.01" 
							value="<?=$C2?>"  
							name="S_C2_<?=$row->staffID?>" 
							id="S_C2_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,10);calculateScoreS(<?=$row->staffID?>);" 
							<?=$readonly?>
							/>					
					</td>	

					<td class="tdR"><span id="S_totalPercent_<?=$row->staffID?>"><?=$sum?></span></td>	
					<td class="tdC textTD" id="colorG2"><span id="S_grade_<?=$row->staffID?>"><?=$grade?></span></td>
				</tr>
		<?php  } ?>
	</tbody>
<?php } else { ?>
	<tr><td style="text-align:center"><h4>ไม่พบข้อมูล</h4></td></tr>
<?php } ?>
</table>

<br><b>รายงานคะแนนระดับ O</b><br>
<table class="table table-striped">
<?php		
	if(!empty($reportO))
	{
?>
	<thead>
	<tr>
		<th rowspan="2" id="th">พนักงาน</th>
		<th colspan="2">KPIs (40%)</th>	
		<th colspan="2">FC (30%)</th>	
		<th colspan="2">LOVE (10%)</h>
		<th rowspan="2" id="th">แบบสอบถาม<br>LOVE (10%)</th>		
		<th rowspan="2" id="th">อื่นๆ (10%)</th>		
		<th rowspan="2" id="th">ผลรวม</th>
		<th rowspan="2" id="th">เกรด</th>
	</tr>
	<tr>
		<th>คะแนน</th>
		<th>ร้อยละ</th>
		<th>คะแนน</th>
		<th>ร้อยละ</th>
		<th>คะแนน</th>
		<th>ร้อยละ</th>				
	</tr>		
	</thead>
	<tbody>
	<?php		
		foreach($reportO as $row){				

			//LOVE
			//$love = ($row->A_Love != '0' || $row->A_Love != null) ? $row->A_Love : $row->Love;
			$love = $row->Love;
			//$A11 = round($love,2);
			//($A11 == null) ? $A1 = "-" : $A1 = $A11;
			$A1 = $love;
			
			$B11 = round($A1 * 0.1,2);
			($B11 == null) ? $B1 = "-" : $B1 = $B11;

			//KPI
			$KPI = ($row->A_KPI != '0' || $row->A_KPI !== null) ? $row->A_KPI : $row->KPI;
			$A22 = round($KPI,2);
			($A22 == null) ? $A2 = "-" : $A2 = $A22;
			
			$B22 = round($A2 * 0.4,2);
			($B22 == null) ? $B2 = "-" : $B2 = $B22;

			//FC
			$Competency = ($row->A_Competency != '0' || $row->A_Competency != null) ? $row->A_Competency : $row->Competency;		
			$A33 =  round($Competency,2);
			($A33 == null) ? $A3 = "-" : $A3 = $A33;

			$B33 = round($A3 * 0.3,2);
			($B33 == null) ? $B3 = "-" : $B3 = $B33;

			$C1 = ($row->A_C1 != '0') ? $row->A_C1 : $row->C1;						
			$C1 = round($C1,2);

			$C2 = ($row->A_C2 != '0') ? $row->A_C2 : $row->C2;						
			$C2 = round($C2,2);

			$sum = round($B1 + $B2 + $B3 + $C1 + $C2,2);

			$score = $sum;

			if($score >= 90 ){
				$grade = 'A+';
			}else if($score >= 80){
				$grade = 'A';
			}else if($score >= 75){
				$grade = 'B+';
			}else if($score >= 70){
				$grade = 'B';
			}else if($score >= 65){
				$grade = 'C+';
			}else if($score >= 60){
				$grade = 'C';
			}else if($score >= 50){
				$grade = 'D';
			}else{
				$grade = 'F';
			}

		?>
				<tr>					
                    <td id="tdC"><input type="hidden" name="O_staffID[]" id="O_staffID[]" value="<?=$row->staffID?>">  
					<?php echo $row->staffFName.' '.$row->staffLName ?><br>
					<span class='orgName'><?= $row->orgName?></span></td>

					<td class="tdC">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$A2?>"  
							name="O_A2_<?=$row->staffID?>" 
							id="O_A2_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreO(<?=$row->staffID?>);"
							/>					
					</td>
					<td id="colorG1" class="tdR"><span id="O_B2_<?=$row->staffID?>"><?=$B2?></span></td>	

					<td class="tdC">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$A3?>"  
							name="O_A3_<?=$row->staffID?>" 
							id="O_A3_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreO(<?=$row->staffID?>);"
							/>										
					</td>					
					<td id="colorG1" class="tdR"><span id="O_B3_<?=$row->staffID?>"><?=$B3?></span></td>	

					<td class="tdC">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$A1?>"  
							name="O_A1_<?=$row->staffID?>" 
							id="O_A1_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreO(<?=$row->staffID?>);" 
							/>					
					</td>															
					<td id="colorG1" class="tdR"><span id="O_B1_<?=$row->staffID?>"><?=$B1?></span></td>	

					<td style="text-align:center">
						<input type="number" 
							min="0" 
							max="10" 
							step="0.01" 
							value="<?=$C1?>"  
							name="O_C1_<?=$row->staffID?>" 
							id="O_C1_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,10);calculateScoreO(<?=$row->staffID?>);" 
							<?=$readonly?>
							/>					
					</td>	
					<td style="text-align:center">
						<input type="number" 
							min="0" 
							max="10" 
							step="0.01" 
							value="<?=$C2?>"  
							name="O_C2_<?=$row->staffID?>" 
							id="O_C2_<?=$row->staffID?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,10);calculateScoreO(<?=$row->staffID?>);" 
							<?=$readonly?>
							/>					
					</td>	

					<td class="tdR"><span id="O_totalPercent_<?=$row->staffID?>"><?=$sum?></span></td>	
					<td class="tdC textTD" id="colorG2"><span id="O_grade_<?=$row->staffID?>"><?=$grade?></span></td>
				</tr>
		<?php  } ?>
	</tbody>	
<br>
<?php	
	}else{ ?>
	<tr><td style="text-align:center"><h4>ไม่พบข้อมูล</h4></td></tr>
<?php } ?>
</table>
<input type="submit" value="Update คะแนน"></input>
</form>

<script type="text/javascript"> 

function calGrade(calScore)
{
	if(calScore >= 90 ){
		grade = 'A+';
	}else if(calScore >= 80){
		grade = 'A';
	}else if(calScore >= 75){
		grade = 'B+';
	}else if(calScore >= 70){
		grade = 'B';
	}else if(calScore >= 65){
		grade = 'C+';
	}else if(calScore >= 60){
		grade = 'C';
	}else if(calScore >= 50){
		grade = 'D';
	}else{
		grade = 'F';
	}
	return grade;
}

function setvallength(obj,minval,maxval) 
{
  if(obj.value>maxval){obj.value=maxval;} else if(obj.value<minval){obj.value=minval;}
  if(obj.value == ''){obj.value='0';}
}

function calculateSalary(staffID)
{
	var A  = parseFloat($('#SalaryAdjust_'+staffID).val()); //LOVE แบบสอบถาม
	var B  = parseFloat($('#NewSalaryOriginal_'+staffID).val()); //LOVE แบบสอบถาม
	$('#NewSalary_'+staffID).html((A + B).toFixed(2));
}

function calculateScoreM(staffID)
{
	var A1 = 0;
	var A2 = 0;
	var A3 = 0;
	var A4 = 0;
	var C1 = 0;
	var calScore = 0;
	var grade = "";

	A1  = parseFloat($('#M_A1_'+staffID).val()) * 0.05; //LOVE
	$('#M_B1_'+staffID).html(A1.toFixed(2));

	A2  = parseFloat($('#M_A2_'+staffID).val()) * 0.3; //KPI
	$('#M_B2_'+staffID).html(A2.toFixed(2));

	A3  = parseFloat($('#M_A3_'+staffID).val()) * 0.3; //FC
	$('#M_B3_'+staffID).html(A3.toFixed(2));

	A4  = parseFloat($('#M_A4_'+staffID).val()) * 0.3; //MC
	$('#M_B4_'+staffID).html(A4.toFixed(2));

	C1  = parseFloat($('#M_C1_'+staffID).val()); //LOVE แบบสอบถาม
	
	calScore = A1 + A2 + A3 + A4 + C1;
	$('#M_totalPercent_'+staffID).html(calScore.toFixed(2));
	$('#M_grade_'+staffID).html(calGrade(calScore));	
}

function calculateScoreS(staffID)
{
	var A1 = 0;
	var A2 = 0;
	var A3 = 0;
	var A4 = 0;
	var C1 = 0;
	var C2 = 0;
	var calScore = 0;
	var grade = "";

	A1  = parseFloat($('#S_A1_'+staffID).val()) * 0.05; //LOVE
	$('#S_B1_'+staffID).html(A1.toFixed(2));

	A2  = parseFloat($('#S_A2_'+staffID).val()) * 0.3; //KPI
	$('#S_B2_'+staffID).html(A2.toFixed(2));

	A3  = parseFloat($('#S_A3_'+staffID).val()) * 0.3; //FC
	$('#S_B3_'+staffID).html(A3.toFixed(2));

	A4  = parseFloat($('#S_A4_'+staffID).val()) * 0.2; //MC
	$('#S_B4_'+staffID).html(A4.toFixed(2));

	C1  = parseFloat($('#S_C1_'+staffID).val()); //LOVE แบบสอบถาม
	C2  = parseFloat($('#S_C2_'+staffID).val()); //คะแนนอื่นๆ
	
	calScore = A1 + A2 + A3 + A4 + C1 + C2;
	$('#S_totalPercent_'+staffID).html(calScore.toFixed(2));
	$('#S_grade_'+staffID).html(calGrade(calScore));	
}

function calculateScoreO(staffID)
{
	var A1 = 0;
	var A2 = 0;
	var A3 = 0;
	var C1 = 0;
	var C2 = 0;
	var calScore = 0;
	var grade = "";

	A1  = parseFloat($('#O_A1_'+staffID).val()) * 0.1; //LOVE
	$('#O_B1_'+staffID).html(A1.toFixed(2));

	A2  = parseFloat($('#O_A2_'+staffID).val()) * 0.4; //KPI
	$('#O_B2_'+staffID).html(A2.toFixed(2));

	A3  = parseFloat($('#O_A3_'+staffID).val()) * 0.3; //FC
	$('#O_B3_'+staffID).html(A3.toFixed(2));

	C1  = parseFloat($('#O_C1_'+staffID).val()); //LOVE แบบสอบถาม
	C2  = parseFloat($('#O_C2_'+staffID).val()); //คะแนนอื่นๆ
	
	calScore = A1 + A2 + A3 + C1 + C2;
	$('#O_totalPercent_'+staffID).html(calScore.toFixed(2));
	$('#O_grade_'+staffID).html(calGrade(calScore));	
}

</script>