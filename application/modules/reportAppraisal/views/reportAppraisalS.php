<script src="<?=base_url()?>assets/js/tablescroll/table-scroll.min.js"></script>
<?php
	if($this->session->userdata('roleID')==3) {
		$readonly = "";
		$salarydisplay = "block";
	} else {
		$readonly = "readonly";		
		$salarydisplay = "none";
	} 
?>
<form action="<?=base_url('reportAppraisal') ?>/updateScore" method="post" enctype="multipart/form-data">    
<input id="agencies" name="agencies" type="hidden" value="<?=$_GET['agencies']?>"/> 
	<b>รายงานคะแนนระดับ S</b>
	<div class="pull-right">
		<a href="<?=site_url('reportAppraisal/excelS?agencies='.$_GET['agencies']);?>" class="btn btn-success btn-save">
			<i class="fa fa-file-excel-o btn-save"></i> Excel
		</a>
		<br><br>
	</div>
	<br><br> 
	<table class="table-results table table-striped" id="tableS">
	<?php		
		if(!empty($reportS))
		{
	?>
		<thead>
		<tr>
			<th rowspan="2" id="th">พนักงาน</th>
			<th colspan="2">KPIs (30%)</th>	
			<th colspan="2">FC (25%)</th>	
			<th colspan="2">MC (20%)</th>	
			<th id='th'>LOVE (10%)</h>
			<th id="th">คะแนนทดสอบ (10%)</th>		
			<th id="th">คะแนนสถิติ (5%)</th>		
			<th rowspan="2" id="th">ผลรวม</th>
			<th rowspan="2" id="th">เกรด</th>
			<?php
				if($this->session->userdata('roleID')==3) {
			?>
			<th rowspan="2" class="salary">เงินเดือน</th>
			<th colspan="3">เงินเดือนใหม่</th>	
			<th colspan="3">Bonus</th>	
			<th rowspan="2">บันทึก</th>
			<?php
				}
			?>
		</tr>
		<tr>
			<th>คะแนน</th>
			<th>ร้อยละ</th>
			<th>คะแนน</th>
			<th>ร้อยละ</th>
			<th>คะแนน</th>
			<th>ร้อยละ</th>				
			<th>คะแนน</th>
			<th>คะแนน</th>
			
			<th>คะแนน</th>

			<?php if($this->session->userdata('roleID')==3) {?>
			<th class="salary">ก่อนปรับ</th>
			<th class="salary">ปรับ +/-</th>
			<th class="salary">หลังปรับ</th>

			<th class="salary">ก่อนปรับ</th>
			<th class="salary">ปรับ +/-</th>
			<th class="salary">หลังปรับ</th>
			<?php } ?>
		</tr>		
		</thead>
		<tbody>
		<?php foreach($reportS as $row){?>
				<tr>					
					<td id="tdC">
					<input type="hidden" name="S_staffID[]" id="S_staffID[]" value="<?=$row["staffID"]?>">
					<?php echo $row["staffName"] ?>
					<br><span class='orgName'><?= $row["orgName"]?></span>
					</td>

					<td class="tdC <?=$row["KPI_modified"]?>">
						<input type="number" 
							min="0" 
							max="135" 
							step="0.01" 
							value="<?=$row["A2"]?>"  
							name="S_A2_<?=$row["staffID"]?>" 
							id="S_A2_<?=$row["staffID"]?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,135);calculateScoreS(<?=$row["staffID"]?>);"
							/>					
					</td>
					<td id="colorG1" class="tdR"><span id="S_B2_<?=$row["staffID"]?>"><?=$row["B2"]?></span></td>	

					<td class="tdC <?=$row["Competency_modified"]?>">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$row["A3"]?>"  
							name="S_A3_<?=$row["staffID"]?>" 
							id="S_A3_<?=$row["staffID"]?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreS(<?=$row["staffID"]?>);"
							/>										
					</td>					
					<td id="colorG1" class="tdR"><span id="S_B3_<?=$row["staffID"]?>"><?=$row["B3"]?></span></td>	

					<td class="tdC <?=$row["love_modified"]?>">
						<input type="number" 
							min="0" 
							max="100" 
							step="0.01" 
							value="<?=$row["A4"]?>"  
							name="S_A4_<?=$row["staffID"]?>" 
							id="S_A4_<?=$row["staffID"]?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,100);calculateScoreS(<?=$row["staffID"]?>);"
							/>										
					</td>					
					<td id="colorG1" class="tdR"><span id="S_B4_<?=$row["staffID"]?>"><?=$row["B4"]?></span></td>	

					<td class="tdC <?=$row["CompetencyM_modified"]?>">
						<input type="number" 
							min="0" 
							max="10" 
							step="0.01" 
							value="<?=$row["A1"]?>"  
							name="S_A1_<?=$row["staffID"]?>" 
							id="S_A1_<?=$row["staffID"]?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,10);calculateScoreS(<?=$row["staffID"]?>);" 
							/>
					</td>	

					<td style="text-align:center">
						<input type="number" 
							min="0" 
							max="10" 
							step="0.01" 
							value="<?=$row["C1"]?>"  
							name="S_C1_<?=$row["staffID"]?>" 
							id="S_C1_<?=$row["staffID"]?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,10);calculateScoreS(<?=$row["staffID"]?>);" 
							/>					
					</td>	
					<td style="text-align:center">
						<input type="number" 
							min="0" 
							max="5" 
							step="0.01" 
							value="<?=$row["C2"]?>"  
							name="S_C2_<?=$row["staffID"]?>" 
							id="S_C2_<?=$row["staffID"]?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,5);calculateScoreS(<?=$row["staffID"]?>);" 
							/>					
					</td>	

					<?php
						if($this->session->userdata('roleID')==3) {
					?>
					<td class="tdR"><span id="S_totalPercent_<?=$row["staffID"]?>"><?=$row["sum"]?></span></td>	
					<td class="tdC textTD" id="colorG2"><span id="S_grade_<?=$row["staffID"]?>"><?=$row["grade"]?></span></td>
					<td class="salary tdR">
						<input type="number" 
							min="0" 
							max="999999" 
							step="0.01" 
							value="<?=$row['salary']?>"  
							name="S_Salary_<?=$row['staffID']?>" 
							id="S_Salary_<?=$row['staffID']?>" 
							class="excel-form"  
							onKeyUp="setvallength(this,0,999999);calculateScoreS(<?=$row['staffID']?>);" 
						/>					
					</td>

					<td class="salary tdR"><span id="S_New_Salary_<?=$row['staffID']?>"><?=$row['newSalary']?></span></td>
					<td class="salary tdR">
						<input type="number" 
							min="-999999" 
							max="999999" 
							step="1" 
							value="<?=$row["salaryAdj"]?>"  
							name="S_Salary_Adj_<?=$row['staffID']?>" 
							id="S_Salary_Adj_<?=$row['staffID']?>" 
							class="excel-form"  
							onKeyUp="calculateScoreS(<?=$row['staffID']?>);" 
							/>					
					</td>
					<td class="salary tdR"><span id="S_Salary_Final_<?=$row["staffID"]?>"><?=$row["salaryFinal"]?></span></td>

					<td class="salary tdR"><span id="S_Bonus_<?=$row["staffID"]?>"><?=$row["bonus"]?></span></td>
					<td class="salary tdR">
						<input type="number" 
							min="-999999" 
							max="999999" 
							step="1" 
							value="<?=$row["bonusAdj"]?>"  
							name="S_Bonus_Adj_<?=$row['staffID']?>" 
							id="S_Bonus_Adj_<?=$row['staffID']?>" 
							class="excel-form"  
							onKeyUp="calculateScoreS(<?=$row['staffID']?>);" 
							/>					
					</td>
					<td class="salary tdR"><span id="S_Bonus_Final_<?=$row["staffID"]?>"><?=$row["bonusFinal"]?></span></td>

					<td class="tdC"><a href="javascript:saveDataS('<?=$row["staffID"]?>')" class="btn-sm btn-success"><i class="btn-save fa fa-floppy-o"></i></a></td>
					<?php
						}
					?>
				</tr>
			<?php  } ?>
		</tbody>
	<?php } else { ?>
		<tr><td style="text-align:center"><h4>ไม่พบข้อมูล</h4></td></tr>
	<?php } ?>
	</table>
	<script type="text/javascript">
	$('#tableS').table_scroll({
		rowsInScrollableArea: 10
	});
	</script>
<!--input type="submit" value="Update คะแนน" class="salary"></input-->
</form>