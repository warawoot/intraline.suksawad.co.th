<table border="1">
<?php		
	if(!empty($reportS))
	{
?>
<thead>
	<tr>
		<th colspan="16"><center>รายงานคะแนนระดับ S</center></th>
	</tr>
	<tr>
		<th rowspan="2">พนักงาน</th>
		<th colspan="2">KPIs (30%)</th>	
		<th colspan="2">FC (25%)</th>	
		<th colspan="2">MC (20%)</th>	
		<th colspan="">LOVE (10%)</h>
		<th rowspan="">คะแนนทดสอบ (10%)</th>		
		<th rowspan="">คะแนนสถิติ (5%)</th>		
		<th rowspan="2">ผลรวม</th>
		<th rowspan="2">เกรด</th>
        <th rowspan="2">เงินเดือน</th>
        <th colspan="3">เงินเดือนใหม่</th>	
        <th colspan="3">Bonus</th>	
	</tr>
	<tr>
			<th>คะแนน</th>
			<th>ร้อยละ</th>
			<th>คะแนน</th>
			<th>ร้อยละ</th>
			<th>คะแนน</th>
			<th>ร้อยละ</th>				
			<th>คะแนน</th>
			<th>คะแนน</th>
			
			<th>คะแนน</th>

        <th>ก่อนปรับ</th>
        <th>ปรับ +/-</th>
        <th>หลังปรับ</th>

        <th>ก่อนปรับ</th>
        <th>ปรับ +/-</th>
        <th>หลังปรับ</th>
		
	</tr>		
</thead>
<tbody>
	<?php foreach($reportS as $row){?>
			<tr>					
				<td>
					<?php echo $row["staffName"] ?><br>
					<?=$row["orgName"]?>
				</td>

				<td><?=$row["A2"]?></td>
				<td><?=$row["B2"]?></td>	

				<td><?=$row["A3"]?></td>					
				<td><?=$row["B3"]?></td>	

				<td><?=$row["A4"]?></td>					
				<td><?=$row["B4"]?></td>	

				<td><?=$row["A1"]?></td>															
				
				<td><?=$row["C1"]?></td>	
				<td><?=$row["C2"]?></td>	

				<td><?=$row["sum"]?></td>	
				<td><?=$row["grade"]?></td>
				<td><?=$row["salary"]?></td>

				<td><?=$row["newSalary"]?></td>
				<td><?=$row["salaryAdj"]?></td>
				<td><?=$row["salaryFinal"]?></td>

				<td><?=$row["bonus"]?></td>
				<td><?=$row["bonusAdj"]?></td>
				<td><?=$row["bonusFinal"]?></td>
			</tr>
		<?php  } ?>
	</tbody>
	<?php } else { ?>
		<tr><td style="text-align:center"><h4>ไม่พบข้อมูล</h4></td></tr>
	<?php } ?>
</table>