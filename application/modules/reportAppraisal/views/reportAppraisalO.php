<script src="<?=base_url()?>assets/js/tablescroll/table-scroll.min.js"></script>
<?php
	if($this->session->userdata('roleID')==3) {
		$readonly = "";
		$salarydisplay = "block";
	} else {
		$readonly = "readonly";		
		$salarydisplay = "none";
	} 
?>
<form action="<?=base_url('reportAppraisal') ?>/updateScore" method="post" enctype="multipart/form-data">    
<input id="agencies" name="agencies" type="hidden" value="<?=$_GET['agencies']?>"/>   
	<b>รายงานคะแนนระดับ O</b>
		<?php
			if($this->session->userdata('roleID')==3) {
		?>
		<div class="pull-right">
			<a href="<?=site_url('reportAppraisal/excelO?agencies='.$_GET['agencies']);?>" class="btn btn-success btn-save">
				<i class="fa fa-file-excel-o btn-save"></i> Excel
			</a>
			<br><br>
		</div>
		<?php
			}
		?>
	<br><br>
	<table class="table-results table table-striped" id="tableO">
	<?php		
		if(!empty($reportO))
		{
	?>
		<thead>
		<tr>
			<th rowspan="2" id="th">พนักงาน</th>
			<th colspan="2">KPIs (40%)</th>	
			<th colspan="2">FC (30%)</th>	
			<th rowspan="2">LOVE (10%)</h>
			<th rowspan="2" id="th">คะแนนทดสอบ (10%)</th>		
			<th rowspan="2" id="th">คะแนนสถิติ (10%)</th>		
			<th rowspan="2" id="th">ผลรวม</th>
			<th rowspan="2" id="th">เกรด</th>
			<?php
				if($this->session->userdata('roleID')==3) {
			?>
			<th rowspan="2" class="salary">เงินเดือน</th>
			<th colspan="3">เงินเดือนใหม่</th>	
			<th colspan="3">Bonus</th>	
			<th rowspan="2">บันทึก</th>
			<?php
				}
			?>	
		</tr>
		<tr>
			<th>คะแนน</th>
			<th>ร้อยละ</th>
			<th>คะแนน</th>
			<th>ร้อยละ</th>
						

			<?php if($this->session->userdata('roleID')==3) {?>
			<th class="salary">ก่อนปรับ</th>
			<th class="salary">ปรับ +/-</th>
			<th class="salary">หลังปรับ</th>

			<th class="salary">ก่อนปรับ</th>
			<th class="salary">ปรับ +/-</th>
			<th class="salary">หลังปรับ</th>
			<?php } ?>
		</tr>		
		</thead>
		<tbody>
		<?php foreach($reportO as $row){?>
			<tr>					
				<td id="tdC"><input type="hidden" name="O_staffID[]" id="O_staffID[]" value="<?=$row['staffID']?>">  
				<?=$row['staffName'] ?><br>
				<span class='orgName'><?=$row['orgName']?></span></td>

				<td class="tdC <?=$row['KPI_modified']?>">
					<input type="number" 
						min="0" 
						max="135" 
						step="0.01" 
						value="<?=$row['A2']?>"  
						name="O_A2_<?=$row['staffID']?>" 
						id="O_A2_<?=$row['staffID']?>" 
						class="excel-form"  
						onKeyUp="setvallength(this,0,135);calculateScoreO(<?=$row['staffID']?>);"
						/>					
				</td>
				<td id="colorG1" class="tdR"><span id="O_B2_<?=$row['staffID']?>"><?=$row['B2']?></span></td>	

				<td class="tdC <?=$row['Competency_modified']?>">
					<input type="number" 
						min="0" 
						max="100" 
						step="0.01" 
						value="<?=$row['A3']?>"  
						name="O_A3_<?=$row['staffID']?>" 
						id="O_A3_<?=$row['staffID']?>" 
						class="excel-form"  
						onKeyUp="setvallength(this,0,100);calculateScoreO(<?=$row['staffID']?>);"
						/>										
				</td>					
				<td id="colorG1" class="tdR"><span id="O_B3_<?=$row['staffID']?>"><?=$row['B3']?></span></td>	

				<td class="tdC <?=$row['love_modified']?>">
					<input type="number" 
						min="0" 
						max="10" 
						step="0.01" 
						value="<?=$row['A1']?>"  
						name="O_A1_<?=$row['staffID']?>" 
						id="O_A1_<?=$row['staffID']?>" 
						class="excel-form"  
						onKeyUp="setvallength(this,0,10);calculateScoreO(<?=$row['staffID']?>);" 
						/>					
				</td>															
				
				<td style="text-align:center">
					<input type="number" 
						min="0" 
						max="10" 
						step="0.01" 
						value="<?=$row['C1']?>"  
						name="O_C1_<?=$row['staffID']?>" 
						id="O_C1_<?=$row['staffID']?>" 
						class="excel-form"  
						onKeyUp="setvallength(this,0,10);calculateScoreO(<?=$row['staffID']?>);" 
						/>					
				</td>	
				<td style="text-align:center">
					<input type="number" 
						min="0" 
						max="10" 
						step="0.01" 
						value="<?=$row['C2']?>"  
						name="O_C2_<?=$row['staffID']?>" 
						id="O_C2_<?=$row['staffID']?>" 
						class="excel-form"  
						onKeyUp="setvallength(this,0,10);calculateScoreO(<?=$row['staffID']?>);" 
						/>					
				</td>	

				<td class="tdR"><span id="O_totalPercent_<?=$row['staffID']?>"><?=$row['sum']?></span></td>	
				<td class="tdC textTD" id="colorG2"><span id="O_grade_<?=$row['staffID']?>"><?=$row['grade']?></span></td>

				<?php
					if($this->session->userdata('roleID')==3) {
				?>
				<td class="salary tdR">
					<input type="number" 
						min="0" 
						max="999999" 
						step="0.01" 
						value="<?=$row['salary']?>"  
						name="O_Salary_<?=$row['staffID']?>" 
						id="O_Salary_<?=$row['staffID']?>" 
						class="excel-form"  
						onKeyUp="setvallength(this,0,999999);calculateScoreO(<?=$row['staffID']?>);" 
						/>					
				</td>

				<td class="salary tdR"><span id="O_New_Salary_<?=$row['staffID']?>"><?=$row['newSalary']?></span></td>
				<td class="salary tdR">
					<input type="number" 
						min="-999999" 
						max="999999" 
						step="1" 
						value="<?=$row["salaryAdj"]?>"  
						name="O_Salary_Adj_<?=$row['staffID']?>" 
						id="O_Salary_Adj_<?=$row['staffID']?>" 
						class="excel-form"  
						onKeyUp="calculateScoreO(<?=$row['staffID']?>);" 
						/>					
				</td>
				<td class="salary tdR"><span id="O_Salary_Final_<?=$row["staffID"]?>"><?=$row["salaryFinal"]?></span></td>

				<td class="salary tdR"><span id="O_Bonus_<?=$row['staffID']?>"><?=$row['bonus']?></span></td>
				<td class="salary tdR">
					<input type="number" 
						min="-999999" 
						max="999999" 
						step="1" 
						value="<?=$row["bonusAdj"]?>"  
						name="O_Bonus_Adj_<?=$row['staffID']?>" 
						id="O_Bonus_Adj_<?=$row['staffID']?>" 
						class="excel-form"  
						onKeyUp="calculateScoreO(<?=$row['staffID']?>);" 
						/>					
				</td>
				<td class="salary tdR"><span id="O_Bonus_Final_<?=$row["staffID"]?>"><?=$row["bonusFinal"]?></span></td>

				<td class="tdC"><a href="javascript:saveDataO('<?=$row["staffID"]?>')" class="btn-sm btn-success"><i class="btn-save fa fa-floppy-o"></i></a></td>
				<?php
					}
				?>	
			</tr>
			<?php  } ?>
		</tbody>	
	<br>
	<?php	
		}else{ ?>
		<tr><td style="text-align:center"><h4>ไม่พบข้อมูล</h4></td></tr>
	<?php } ?>
	</table>
	<script type="text/javascript">
	$('#tableO').table_scroll({
		rowsInScrollableArea: 10
	});
	</script>
<!--input type="submit" value="Update คะแนน" class="salary"></input-->
</form>
