<script src="<?=base_url()?>assets/js/tablescroll/table-scroll.min.js"></script>
<?php
	if($this->session->userdata('roleID')==3) {
		$readonly = "";
		$salarydisplay = "block";
	} else {
		$readonly = "readonly";		
		$salarydisplay = "none";
	} 
?>
<style type="text/css">
thead td {
	background-color : #1379C7 !important;
}
td {
	padding : 5px !important;
}

.form-control {
	color: #343232;	
}

input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
	-webkit-appearance: none;
}

.table-results tr td{
	padding:5px !important;
	vertical-align: middle !important;
}
.table-results thead th{
	font-size:12px !important;
}

th {
	color:#FFF !important;
	padding : 5px !important;
	text-align:center;
}

.excel-style {
	padding : 0px !important;
}

.excel-form {
	width : 100px !important;
	line-height : 20px !important;
	height : 20px !important;
	border : none !important; 
	background-color: transparent !important; 
}

select.excel-form {
	width : 700px !important;
	line-height : 40px !important;
	height : 40px !important;
	border : none !important; 
	background-color: transparent !important; 
}

input[type=number].excel-form {
	margin-top : 20px !important;
	width : 60px !important;
	text-align:right;
	font-size:12px;
	font-weight:bold;
	display:table-cell;
}

.summary {
	padding-right : 10px !important;
	text-align:right;
	font-size:12px;
	font-weight:bold;
}

.table th {
	text-align: center;				
}

.tdC{
	text-align:center;
}

.tdR{
	text-align:right;			
}

.tdR span {
	font-size:12px !important;
}

#th{
	padding-bottom: 25px;
}

#colorG1{
	background:#f2f2f2;
}

#colorG2{
	background:#e6e6e6;
}

.textTD{
	font-weight:bold;
}

.form-control {
	color: #343232;
}

.orgName {
	font-size:8pt;
	color:#999;
}

.salary {
	display:<?=$salarydisplay?>;
}

.modified {
	background-color : #cceeff;
}

.btn-save {
	color:#FFF !important;
}
</style>
<script>
$( function() {
	$( "#tabs" ).tabs({
      beforeLoad: function( event, ui ) {
        ui.jqXHR.fail(function() {
          ui.panel.html(
            "มีปัญหาในการ Load ข้อมูล" );
        });
      }
    });
});
</script>
<section class="panel">      
	<div class="panel-body">
		<form action="<?=base_url('reportAppraisal') ?>" method="get" enctype="multipart/form-data">        
			<div class="form-group" >
			<div class="row" style="padding:10px">
				<div class="col-md-3 text-right"><b>หน่วยงาน</b></div>
				<div class="col-md-9">
					<select class="form-control form-control" style="width:350px" data-type="select" name="agencies" onchange="this.form.submit();"> 
						<option value=''>กรุณาเลือก</option>
						<?=$dropdown_org_chart?>
					</select>
				</div>
			</div>            
			</div> 
		</form>
	</div>
</section>

<form action="<?=base_url('reportAppraisal') ?>/updateScore" method="post" enctype="multipart/form-data"> 
<?php if(isset($_GET['agencies']) && $_GET['agencies'] != "") { ?>   
<input id="agencies" name="agencies" type="hidden" value="<?=$_GET['agencies']?>"/>   
<div id="tabs">
	<ul>
		<li><a href="<?=base_url()?>reportAppraisal/reportIM?agencies='<?=$_GET['agencies']?>'">รายงานคะแนนระดับ M</a></li>
		<li><a href="<?=base_url()?>reportAppraisal/reportIS?agencies='<?=$_GET['agencies']?>'">รายงานคะแนนระดับ S</a></li>
		<li><a href="<?=base_url()?>reportAppraisal/reportIO?agencies='<?=$_GET['agencies']?>'">รายงานคะแนนระดับ O</a></li>
	</ul>
</div>
<?php }?>
<!--input type="submit" value="Update คะแนน" class="salary"></input-->
</form>

<script type="text/javascript">

function saveDataM(staffID)
{
	var dataPost = {
		staffID : staffID,
		A1: $('#M_A1_'+staffID).val(),
		A2: $('#M_A2_'+staffID).val(),
		A3: $('#M_A3_'+staffID).val(),
		A4: $('#M_A4_'+staffID).val(),
		C1: $('#M_C1_'+staffID).val(),
		C2: 0,
		salary: $('#M_Salary_'+staffID).val(),
		salaryAdj: $('#M_Salary_Adj_'+staffID).val(),
		bonusAdj: $('#M_Bonus_Adj_'+staffID).val()
	};

	$.ajax({
		url: "<?=base_url()?>reportAppraisal/updateData",
		method: 'post',
		data: dataPost,
		dataType: 'json',
		success: function(data){
			alert(data);
		}
	});
}

function saveDataS(staffID)
{
	var dataPost = {
		staffID : staffID,
		A1: $('#S_A1_'+staffID).val(),
		A2: $('#S_A2_'+staffID).val(),
		A3: $('#S_A3_'+staffID).val(),
		A4: $('#S_A4_'+staffID).val(),
		C1: $('#S_C1_'+staffID).val(),
		C2: $('#S_C2_'+staffID).val(),
		salary: $('#S_Salary_'+staffID).val(),
		salaryAdj: $('#S_Salary_Adj_'+staffID).val(),
		bonusAdj: $('#S_Bonus_Adj_'+staffID).val()
	};

	$.ajax({
		url: "<?=base_url()?>reportAppraisal/updateData",
		method: 'post',
		data: dataPost,
		dataType: 'json',
		success: function(data){
			alert(data);
		}
	});
}

function saveDataO(staffID)
{
	var dataPost = {
		staffID : staffID,
		A1: $('#O_A1_'+staffID).val(),
		A2: $('#O_A2_'+staffID).val(),
		A3: $('#O_A3_'+staffID).val(),
		A4: 0,
		C1: $('#O_C1_'+staffID).val(),
		C2: $('#O_C2_'+staffID).val(),
		salary: $('#O_Salary_'+staffID).val(),
		salaryAdj: $('#O_Salary_Adj_'+staffID).val(),
		bonusAdj: $('#O_Bonus_Adj_'+staffID).val()
	};

	$.ajax({
		url: "<?=base_url()?>reportAppraisal/updateData",
		method: 'post',
		data: dataPost,
		dataType: 'json',
		success: function(data){
			alert(data);
		}
	});
}

function calGrade(calScore)
{
	if(calScore >= 90 ){
		grade = 'A+';
	}else if(calScore >= 80){
		grade = 'A';
	}else if(calScore >= 75){
		grade = 'B+';
	}else if(calScore >= 70){
		grade = 'B';
	}else if(calScore >= 65){
		grade = 'C+';
	}else if(calScore >= 60){
		grade = 'C';
	}else if(calScore >= 50){
		grade = 'D';
	}else{
		grade = 'F';
	}
	return grade;
}

function calSalary(calScore,salary)
{
	var nf = Intl.NumberFormat();
	if(calScore >= 90 ){
		newSalary = salary * 1.06;
	}else if(calScore >= 80){
		newSalary = salary * 1.05;
	}else if(calScore >= 75){
		newSalary = salary * 1.04;
	}else if(calScore >= 70){
		newSalary = salary * 1.03;
	}else if(calScore >= 65){
		newSalary = salary * 1.02;
	}else if(calScore >= 60){
		newSalary = salary * 1.01;
	}else if(calScore >= 50){
		newSalary = salary * 1.00;
	}else{
		newSalary = salary;
	}
	
	return newSalary.toFixed(0);
}

function calBonusM(calScore,salary)
{
	var nf = Intl.NumberFormat();
	if(calScore >= 90 ){
		bonus = salary * 1.2 * 1.0;
	}else if(calScore >= 80){
		bonus = salary * 1.1 * 1.0;
	}else if(calScore >= 75){
		bonus = salary * 1.0 * 1.0;
	}else if(calScore >= 70){
		bonus = salary * 0.9 * 1.0;
	}else if(calScore >= 65){
		bonus = salary * 0.8 * 1.0;
	}else if(calScore >= 60){
		bonus = salary * 0.7 * 1.0;
	}else if(calScore >= 50){
		bonus = salary * 0;
	}else{
		bonus = salary * 0;
	}
	return bonus.toFixed(0);
}

function calBonusS(calScore,salary)
{
	var nf = Intl.NumberFormat();
	if(calScore >= 90 ){
		bonus = salary * 1.1 * 1.0;
	}else if(calScore >= 80){
		bonus = salary * 1.0 * 1.0;
	}else if(calScore >= 75){
		bonus = salary * 0.9 * 1.0;
	}else if(calScore >= 70){
		bonus = salary * 0.8 * 1.0;
	}else if(calScore >= 65){
		bonus = salary * 0.7 * 1.0;
	}else if(calScore >= 60){
		bonus = salary * 0.6 * 1.0;
	}else if(calScore >= 50){
		bonus = salary * 0;
	}else{
		bonus = salary * 0;
	}
	return bonus.toFixed(0);
}

function calBonusO(calScore,salary)
{
	var nf = Intl.NumberFormat();
	if(calScore >= 90 ){
		bonus = salary * 1.0 * 1.0;
	}else if(calScore >= 80){
		bonus = salary * 0.9 * 1.0;
	}else if(calScore >= 75){
		bonus = salary * 0.8 * 1.0;
	}else if(calScore >= 70){
		bonus = salary * 0.7 * 1.0;
	}else if(calScore >= 65){
		bonus = salary * 0.6 * 1.0;
	}else if(calScore >= 60){
		bonus = salary * 0.5 * 1.0;
	}else if(calScore >= 50){
		bonus = salary * 0;
	}else{
		bonus = salary * 0;
	}
	return bonus.toFixed(0);
}

function setvallength(obj,minval,maxval) 
{
  if(obj.value>maxval){obj.value=maxval;} else if(obj.value<minval){obj.value=minval;}
  if(obj.value == ''){obj.value='0';}
}

function calculateScoreM(staffID)
{
	var A1 = 0;
	var A2 = 0;
	var A3 = 0;
	var A4 = 0;
	var C1 = 0;
	var calScore = 0;
	var grade = "";

	A1  = parseFloat($('#M_A1_'+staffID).val()) * 0.05; //LOVE
	$('#M_B1_'+staffID).html(A1.toFixed(2));

	A2  = parseFloat($('#M_A2_'+staffID).val()) * 0.3; //KPI
	$('#M_B2_'+staffID).html(A2.toFixed(2));

	A3  = parseFloat($('#M_A3_'+staffID).val()) * 0.3; //FC
	$('#M_B3_'+staffID).html(A3.toFixed(2));

	A4  = parseFloat($('#M_A4_'+staffID).val()) * 0.3; //MC
	$('#M_B4_'+staffID).html(A4.toFixed(2));

	C1  = parseFloat($('#M_C1_'+staffID).val()); //LOVE แบบสอบถาม

	calScore =  A2 + A3 + A4 + C1; // A ตัดออก

	salary  		= parseFloat($('#M_Salary_'+staffID).val());
	salary_New		= calSalary(calScore,salary.toFixed(0));
	salary_Adj  	= parseFloat($('#M_Salary_Adj_'+staffID).val());
	salary_Final	= parseFloat(salary_New) + parseFloat(salary_Adj);
	bonus_Adj  		= parseFloat($('#M_Bonus_Adj_'+staffID).val());
	bonus_New		= calBonusM(calScore,salary.toFixed(0));
	bonus_Final 	= parseFloat(bonus_New) + parseFloat(bonus_Adj);

	$('#M_totalPercent_'+staffID).html(calScore.toFixed(2));
	$('#M_grade_'+staffID).html(calGrade(calScore));	
	$('#M_New_Salary_'+staffID).html(salary_New);	
	$('#M_Salary_Final_'+staffID).html(salary_Final);	
	$('#M_Bonus_'+staffID).html(bonus_New);	
	$('#M_Bonus_Final_'+staffID).html(bonus_Final);	
}

function calculateScoreS(staffID)
{
	var A1 = 0;
	var A2 = 0;
	var A3 = 0;
	var A4 = 0;
	var C1 = 0;
	var C2 = 0;
	var calScore = 0;
	var grade = "";

	A1  = parseFloat($('#S_A1_'+staffID).val()); //LOVE * 0.05
	//$('#S_B1_'+staffID).html(A1.toFixed(2));

	A2  = parseFloat($('#S_A2_'+staffID).val()) * 0.3; //KPI
	$('#S_B2_'+staffID).html(A2.toFixed(2));

	A3  = parseFloat($('#S_A3_'+staffID).val()) * 0.25; //FC
	$('#S_B3_'+staffID).html(A3.toFixed(2));

	A4  = parseFloat($('#S_A4_'+staffID).val()) * 0.2; //MC
	$('#S_B4_'+staffID).html(A4.toFixed(2));

	C1  = parseFloat($('#S_C1_'+staffID).val()); //LOVE แบบสอบถาม
	C2  = parseFloat($('#S_C2_'+staffID).val()); //คะแนนอื่นๆ
	
	calScore = A1 + A2 + A3 + A4 + C1 + C2;

	salary  		= parseFloat($('#S_Salary_'+staffID).val());
	salary_New		= calSalary(calScore,salary.toFixed(0));
	salary_Adj  	= parseFloat($('#S_Salary_Adj_'+staffID).val());
	salary_Final	= parseFloat(salary_New) + parseFloat(salary_Adj);
	bonus_Adj  		= parseFloat($('#S_Bonus_Adj_'+staffID).val());
	bonus_New		= calBonusS(calScore,salary.toFixed(0));
	bonus_Final 	= parseFloat(bonus_New) + parseFloat(bonus_Adj);

	$('#S_totalPercent_'+staffID).html(calScore.toFixed(2));
	$('#S_grade_'+staffID).html(calGrade(calScore));	
	$('#S_New_Salary_'+staffID).html(salary_New);	
	$('#S_Salary_Final_'+staffID).html(salary_Final);	
	$('#S_Bonus_'+staffID).html(bonus_New);	
	$('#S_Bonus_Final_'+staffID).html(bonus_Final);	
}

function calculateScoreO(staffID)
{
	var A1 = 0;
	var A2 = 0;
	var A3 = 0;
	var C1 = 0;
	var C2 = 0;
	var calScore = 0;
	var grade = "";

	A1  = parseFloat($('#O_A1_'+staffID).val()); //LOVE  * 0.1
	$('#O_B1_'+staffID).html(A1.toFixed(2));

	A2  = parseFloat($('#O_A2_'+staffID).val()) * 0.4; //KPI
	$('#O_B2_'+staffID).html(A2.toFixed(2));

	A3  = parseFloat($('#O_A3_'+staffID).val()) * 0.3; //FC
	$('#O_B3_'+staffID).html(A3.toFixed(2));

	C1  = parseFloat($('#O_C1_'+staffID).val()); //LOVE แบบสอบถาม
	C2  = parseFloat($('#O_C2_'+staffID).val()); //คะแนนอื่นๆ

	calScore = A1 + A2 + A3 + C1 + C2;

	salary  		= parseFloat($('#O_Salary_'+staffID).val());
	salary_New		= calSalary(calScore,salary.toFixed(0));
	salary_Adj  	= parseFloat($('#O_Salary_Adj_'+staffID).val());
	salary_Final	= parseFloat(salary_New) + parseFloat(salary_Adj);
	bonus_Adj  		= parseFloat($('#O_Bonus_Adj_'+staffID).val());
	bonus_New		= calBonusO(calScore,salary.toFixed(0));
	bonus_Final 	= parseFloat(bonus_New) + parseFloat(bonus_Adj);

	$('#O_totalPercent_'+staffID).html(calScore.toFixed(2));
	$('#O_grade_'+staffID).html(calGrade(calScore));	
	$('#O_New_Salary_'+staffID).html(salary_New);	
	$('#O_Salary_Final_'+staffID).html(salary_Final);	
	$('#O_Bonus_'+staffID).html(bonus_New);	
	$('#O_Bonus_Final_'+staffID).html(bonus_Final);	
}

</script>
