<?php

class Structure_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="tbl_position_line";
        $this->pk = "id";
	}
    
	function max_id(){
	    $this->db->select_max('id');
		$this->db->from('tbl_position_line');
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->id; 
 		}else{
			$data 		=  '1'; //  hot เท่ากับ login แต่ status ไม่เท่า 2
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
   function max_data($id){
	    $this->db->select('*');
		$this->db->from('`tbl_position_line`');
		$this->db->where('`id`' ,$id);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        //print_r($data);
		return ($query->num_rows() > 0) ? $data : NULL;
    }   
	
	function get_data($id){
	    $this->db->select('*');
		$this->db->from('`tbl_position_line`');
		$this->db->where('`id`' ,$id);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        //print_r($data);
		return ($query->num_rows() > 0) ? $data : NULL;
    }       
	
	function distinct($id){
		$this->db->select('*');
		$this->db->from('`tbl_position_line`');
		$this->db->where('`id`' ,$id);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
       
		return ($query->num_rows() > 0) ? $data : NULL;
    }
	
	
	function insert_data($data){
		
		$id			= $data['id'];
		$name		= $data['name'];
		 
		$this->db->select('id , name');
		$this->db->from('tbl_position_line as t1');
		$this->db->where('t1.id' ,$id);   
  		$query 		= $this->db->get();
 		 
		if($query->num_rows() == 0){
				$sql	=	$this->db->insert("tbl_position_line",$data);
				  
				if($this->db->affected_rows() > 0){
						 return 'succesfully';
						 echo '1';
				}else{
						 return 'failed';
						  echo '2';
				}
		}else{
				return 'failed';
				echo '3';
		}
 			
 	}
   
} 