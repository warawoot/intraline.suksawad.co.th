<style type="text/css">
	.form-control {
 	  color: #343232;
	}
    #ui-datepicker-div{  
		font-size: 0.9em;
 	}  
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
 <div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลโครงสร้างตำแหน่งงาน</h3>
            </header>
            <div class="panel-body">
                <link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                     <div class="xcrud">
                        <div class="xcrud-container">
                            <div class="xcrud-ajax">
                             
                                <div class="xcrud-view">
                                    <div class="form-horizontal">
                                         
                                        <div class="form-group"><label class="control-label col-sm-3">ข้อมูลโครงสร้างตำแหน่งงาน*</label>
                                            <div class="col-sm-9"><input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="" name="dGJsX3Byb2plY3QubmFtZQ--" id="dGJsX3Byb2plY3QubmFtZQ--" maxlength="255"></div>
                                        </div>
                                         
                                    </div>
                                 </div>
                                <div class="xcrud-top-actions btn-group">
                                    <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action" onclick="modal_ck()">บันทึกและย้อนกลับ</a><a href="#" data-task="list" class="btn btn-warning xcrud-action" onclick="modal_back()">ย้อนกลับ</a>
                                </div>
                            </div>
                     </div>
                </div> 
             </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 
   
 
 function modal_back(){
	location.replace('<?php echo base_url().'structure'; ?>');
 }
 
 function modal_ck(){
 	 
	var name	= $("#dGJsX3Byb2plY3QubmFtZQ--").val();
 	
	 if(name == ''){
		  alert('กรุณาระบุ ชื่อโครงสร้างตำแหน่งงาน ด้วยค่ะ');
		  location.href = '<?php echo base_url().'structure/add' ?>';
	 
	 }else{
		 
		  $.ajax({
				url: "<?php echo base_url('structure/check_add?action='.$actions) ?>",
				type: 'POST',
				data: {
						id: <?php echo $id;?>,
						name: name 
				}, 
				success: function(response) {
					//Do Something 
						if(response == 'succesfully'){
 							location.replace('<?php echo base_url().'structure'?>');
						}else{
 							//var obj = jQuery.parseJSON(response); 
							alert('กรุณาลองใหม่อีกครั้ง');
							location.replace('<?php echo base_url().'structure/add' ?>');
						}
				},
				error: function(xhr) {
					//Do Something to handle error
					alert('กรุณาลองใหม่อีกครั้ง !!');
					location.replace('<?php echo base_url().'structure' ?>');
				}
				
		 }); //end $.ajax
		  
	} //end if	
 }
</script>