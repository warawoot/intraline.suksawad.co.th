<?php

class Structure extends MY_Controller {
    
  
    function __construct() {
    parent::__construct();
     	$this->load->model('structure_model');
    }
 
    public function index() {
 
		 
        $xcrud		=	xcrud_get_instance();
        
		$xcrud->table('tbl_position_line'); 
        $arr		=	array( "name"=>"ชื่อโครงสร้างตำแหน่งงาน" );
		 
		//$xcrud->pass_var('id',$max_id,'create');  
		$xcrud->validation_required('name') ;
		
		$xcrud->button(site_url().'jobd/?token={id}','ตำแแหน่ง','fa fa-folder-open');  
		
		$xcrud->before_remove('delete_position_line_data'); 
				
 		$xcrud->label($arr);
        $xcrud->columns($arr); 
        $xcrud->fields($arr); 
          
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
		 
        $this->template->load('template/admin', 'structure',$data);
		
    }
     
   	
	public function add(){
		
		$max 	= $this->structure_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
		}else{
 			$max_id  =  $max+1;
 		}
 		
		//////////////////////// Step 2 Check Max Id distinct///////////////////////	 
		
		$distinct	= $this->structure_model->distinct($max_id); 
		
		if(is_array($distinct)){
			redirect(site_url().'structure', 'refresh');
			exit();
		}
 
 		$data['actions']	= 'add';
		$data['id']	= $max_id;
		 
  		//$data['get_data'] = $this->project_model->get_date_edit($evalYear,$evalRound); 
        $this->template->load('template/admin', 'structure_form',$data);
		 
	}
	
	public function check_add() {
		
		$id				= $this->input->get_post("id");
		$name 			= $this->input->get_post("name");
 		$action 		= $this->input->get_post("action");
 		
  		$data	=	array( 
  				"id"=>$id,
				"name"=>$name   
		
		);
		
		$data2	=	array( 
		
  				"name"=>$name  
		
		);
   
		if($action == 'edit'){
			//$insert_data = $this->evaluation_model->update_data($evalYear,$evalRound,$data2); 
 		}elseif($action == 'add'){
			$insert_data = $this->structure_model->insert_data($data); 	
 		}
  		 
 		if($insert_data == 'succesfully'){
				echo  'succesfully';
		}else{
				echo 'failed';
		}									
         
	}  
	
} 
?>