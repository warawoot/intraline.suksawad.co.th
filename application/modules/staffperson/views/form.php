<style>
    .error_lbl{
        border-color:red !important;
    }
        .ui-dialog-titlebar-close{
            display:none
        }
        .header-product{
            background-color: pink;
            font-weight: bold;
        }
        .header-product th{
            text-align: center;
            vertical-align: top;
        }
        .btn-none{
            display:none;
        }
</style>

<div class="col-md-12">

     <div class="col-separator box col-separator-first col-unscrollable">
        <div class="col-table">
                   <div class="row">
                      <div class="col-sm-12">
                          <section class="panel">
                             
                              <div class="panel-body">
                              <form class="form-horizontal " id="staffpersonForm" name="staffpersonForm" method="post" action="<?php echo site_url(); ?>staffperson/submit" >
                                <input type="hidden" name="staffID" value="<?php echo $staffid; ?>">
                                <input type="hidden" name="id" value="<?php echo !empty($id) ? $id : ""; ?>">
                                <div class="xcrud">
                                  <div class="xcrud-container">
                                      <div class="xcrud-view">
                                        <div class="form-horizontal">
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">ชื่อ*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="personFName" value="<?php echo !empty($r) ? $r->personFName : ""; ?>" data-type="text" data-required="1" class="xcrud-input form-control">
                                                <?php }else{
                                                echo $r->personFName;
                                              } ?>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                            <label class="control-label col-sm-3">นามสกุล*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="personLName" value="<?php echo !empty($r) ? $r->personLName : ""; ?>" data-type="text" data-required="1" class="xcrud-input form-control">
                                                <?php }else{
                                                echo $r->personLName;
                                              } ?>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-sm-3">เลขประชาชน*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="personIDCard" id="personIDCard" value="<?php echo !empty($r) ? $r->personIDCard : ""; ?>" data-type="text" data-required="1" class="xcrud-input form-control">
                                                <?php }else{
                                                echo $r->personIDCard;
                                              } ?>
                                              </div>
                                          </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-3">วันเกิด*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="personBirthday" value="<?php echo !empty($r) ? toBEDate($r->personBirthday) : ""; ?>" data-type="text" data-required="1" class="xcrud-input xcrud-datepicker form-control" data-type="date">
                                                <?php }else{
                                                echo toBEDateThai($r->personBirthday);
                                              } ?>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                            <label class="control-label col-sm-3">สถานภาพ</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                  $chk1 = "checked";
                                                  $chk2 = "";
                                                  $chk3 = "";
                                                  $chk1 = ($r->personStatus == "3") ? "checked" : $chk1;
                                                  $chk2 = ($r->personStatus == "2") ? "checked" : $chk2;
                                                  $chk3 = ($r->personStatus == "4") ? "checked" : $chk3;
                                                ?>
                                                <input type="radio" name="personStatus" value="3" <?php echo $chk1; ?>> ยังมีชีวิตอยู่<br>
                                                <input type="radio" name="personStatus" value="2" <?php echo $chk2; ?>> เสียชีวิต<br>
                                                <input type="radio" name="personStatus" value="4" <?php echo $chk3; ?>> ยกเลิกการใช้สิทธิ์สวัสดิการ

                                              <?php }else{ 
                                                  $chk1 = ($r->personStatus == "3") ? "checked" : "";
                                                  $chk2 = ($r->personStatus == "2") ? "checked" : "";
                                                  $chk3 = ($r->personStatus == "4") ? "checked" : "";
                                                ?>
                                                <input type="radio" name="personStatus" value="3" <?php echo $chk1; ?> disabled> ยังมีชีวิตอยู่<br>
                                                <input type="radio" name="personStatus" value="2" <?php echo $chk2; ?> disabled> เสียชีวิต<br>
                                                <input type="radio" name="personStatus" value="4" <?php echo $chk3; ?> disabled> ยกเลิกการใช้สิทธิ์สวัสดิการ
                                              <?php } ?>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                            <label class="control-label col-sm-3">มีบุตรกับคู่สมรส</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                 $personMother = (!empty($r)) ? $r->personMother : "";

                                                echo getDropdown(listDataSql("SELECT * FROM tbl_staff_person WHERE personType = '1' AND staffID = '$staffid'",'personID','personFName,personLName','ASC'),'personMother',$personMother,'class="form-control"');
                                               }else{
                                                echo getPersonName($r->personMother);
                                              } ?>
                                              </div>
                                          </div>

                                          <div class="xcrud-top-actions btn-group">
                                            <?php if($this->uri->segment('2') != 'view'){ ?>
                                            <a class="btn btn-primary"  href="javascript:submitForm();">บันทึกและย้อนกลับ</a>
                                            <?php } ?>
                                            <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>staffperson/back/?token=<?php echo $staffid; ?>'">ย้อนกลับ</a>
                                        </div>
                                        </form>
                              </div>
                          </section>
                      </div>
                  </div>
            
             
                
        </div>  
      </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css">
<script src="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url(); ?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script>
<script>
$(function(){
      $('.xcrud-datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            changeMonth: true,
            changeYear: true
          });

      

  })
  function submitForm(){

    $.get("<?php echo site_url()?>staffperson/validateIDCard", 
        {  "id": '<?php echo $staffid;?>',"idcard":document.getElementById("personIDCard").value,"status":document.querySelector('input[name="personStatus"]:checked').value,"person":'<?php echo !empty($id) ? $id : ""; ?>'}
      ).success(function (data, status, header, config) {
            if(data == '1'){
                
                alert("เลขประชาชนถูกใช้แล้ว");
            }else{
                $('#staffpersonForm').submit();
            }
            
      }).error(function (data, status, headers, config) {
          
      });
  }
  
</script>
