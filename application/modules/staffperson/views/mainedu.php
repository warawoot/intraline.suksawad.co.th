<style>
.tab-pane,.panel-heading{
    margin-top:30px;
}
</style>
<div class="row">
    <div class="col-sm-12" ng-controller="personCtrl">
        <section class="panel">
            <header class="panel-heading">
                <!--<h3><?php //echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php //echo $staffName; ?></small></h3>
                 <a href="<?php //echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php //echo $this->config->item('txt_back')?></a>-->
            </header>
            <div class="panel-body">
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs nav-justified ">
                            <li>
                                <a href="#first"  data-toggle="tab"> บิดา </a>
                            </li>
                             <li>
                                <a href="#second" data-toggle="tab"> มารดา </a>
                            </li>
                             <li>
                                <a href="#third" data-toggle="tab"> คู่สมรส </a>
                            </li>
                            <li class="active">
                                <a href="#four" id="clickfirst" data-toggle="tab"> บุตร </a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content tasi-tab">

                            <div id="first" class="tab-pane ">

                                <?php echo $html1; ?>
                            </div>

                            <div id="second" class="tab-pane">
                                <?php echo $html2; ?>
                            </div>

                             <div id="third" class="tab-pane">
                                <?php echo $html3; ?>
                            </div>
                            <div id="four" class="tab-pane active">
                                <?php if($this->uri->segment('2') == 'graduation'){ ?>
                                <a href="<?php echo site_url(); ?>staffperson/back/?token=<?php echo $id;?>" id="btn_back"  class="btn btn-default">ย้อนกลับหน้าบุตร</a>
                                <?php }else if($this->uri->segment('2') == 'back'){  ?>

                                     <a class="btn btn-success"  href="#" ng-click="addPerson()"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</a>
                                   
                                <?php } ?>
                                <?php echo $html4; ?>
                            </div>
                        </div>
                    </div>
                
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
$(function(){

    

    $('#first .btn-warning').attr("id","btn_back");
    $('#first .btn-warning').attr("onclick","window.location='../reg'");
    $('#first .btn-warning').removeClass("xcrud-action");

    $('#second .btn-warning').attr("id","btn_back");
    $('#second .btn-warning').attr("onclick","window.location='../reg'");
    $('#second .btn-warning').removeClass("xcrud-action");

    //$('.btn-primary').attr("id","btn_save");
    
    $('.btn-primary').removeAttr("data-after");
    $('.btn-primary').attr("data-after","edit");



})

jQuery(document).on("xcrudafterrequest",function(event,container){

    if(Xcrud.current_task == 'save')
    {
        $('#first .btn-warning').attr("id","btn_back");
        $('#first .btn-warning').attr("onclick","window.location='../reg'");
        $('#first .btn-warning').removeClass("xcrud-action");

        $('#second .btn-warning').attr("id","btn_back");
        $('#second .btn-warning').attr("onclick","window.location='../reg'");
        $('#second .btn-warning').removeClass("xcrud-action");

        $('.btn-primary').removeAttr("data-after");
        $('.btn-primary').attr("data-after","edit");
        //window.location="<?php echo site_url()?>staffperson/?token=<?php echo $id?>";
    }
});

</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        myApp.controller('personCtrl', ['$scope', '$location','$http', function($scope, $location,$http) {

            $scope.addPerson = function(){
               $http.get("<?php echo site_url()?>staffperson/getCountPerson", { params: { "id": '<?php echo $id;?>' } }).success(function (data, status, header, config) {
                    if(data == 3){
                        alert("มีข้อมูลบุตรครบ 3 คนแล้ว ไม่สามารถเพิ่มข้อมูลได้");
                    }else{
                        window.location='<?php echo site_url();?>staffperson/add/?token=<?php echo $id;?>';
                    }
                 
              }).error(function (data, status, headers, config) {
                  
              });
               
          }

        }]); 


</script>
<style>
  #third .xcrud-search-toggle{
    position:absolute;
      left:75px;
  }
   #four .xcrud-search-toggle{
    position:absolute;
     top:-35px;
    left:130px;
  }
  
</style>