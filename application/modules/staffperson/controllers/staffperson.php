<?php

class Staffperson extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		$this->setModel('staff_person_model');
		$this->load->model('staff_model');
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '3');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา'

		);

		$xcrud->columns('personFName,personStatus,personWork');
		$xcrud->label($col_name);
		$xcrud->column_pattern('personFName','{value} {personLName}');
		$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud->pass_var(array('staffID'=>$id,'personType'=>'3'));
		$xcrud->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
		$xcrud->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
		$xcrud->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
		$xcrud->set_lang("save_return","บันทึก");
		$xcrud->hide_button("return");

		$xcrud->validation_required('personFName,personLName');
		// End Form//
		
		$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'3'));

		if($r->num_rows() > 0) 
			$data['html1'] = $xcrud->render('edit',$r->row()->personID);
		else
			$data['html1'] = $xcrud->render('create');
		

		// select table//
		$xcrud2 = xcrud_get_instance();
		$xcrud2->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '4');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา'

		);

		$xcrud2->columns('personFName,personStatus,personWork');
		$xcrud2->label($col_name);
		$xcrud2->column_pattern('personFName','{value} {personLName}');
		$xcrud2->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud2->pass_var(array('staffID'=>$id,'personType'=>'4'));
		$xcrud2->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
		$xcrud2->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
		$xcrud2->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
		$xcrud2->set_lang("save_return","บันทึก");
		$xcrud2->hide_button("return");
		$xcrud2->validation_required('personFName,personLName');
		// End Form//
		
		$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'4'));

		if($r->num_rows() > 0) 
			$data['html2'] = $xcrud2->render('edit',$r->row()->personID);
		else
			$data['html2'] = $xcrud2->render('create');


		// select table//
		$xcrud3 = xcrud_get_instance();
		$xcrud3->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '1');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา',
			'personIDCard' => 'เลขบัตรประชาชน',
			'personFileHousehold' => 'ทะเบียนบ้าน',
			'personFileMarriage' => 'ทะเบียนสมรส'

		);

		$xcrud3->columns('marryDate,personFName,personLName,personWork');
		$xcrud3->label($col_name);
		//$xcrud3->column_pattern('personFName','{value} {personLName}');
		$xcrud3->unset_numbers()->unset_pagination()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud3->pass_var(array('staffID'=>$id,'personType'=>'1'));
		$xcrud3->fields('marryDate,personFName,personLName,personIDCard,personWork,personTel,personStatus,personFileHousehold,personFileMarriage');
		$xcrud3->change_type('personStatus','radio','',array('1'=>'สมรส','0'=>'หย่า','2'=>'เสียชีวิต'));
		$xcrud3->column_callback('marryDate','toBDDate');
		$xcrud3->column_width('personFName','25%')->column_width('personLName','25%');
		$xcrud3->change_type('personFileHousehold', 'file', '', array('not_rename'=>true));
 		$xcrud3->change_type('personFileMarriage', 'file', '', array('not_rename'=>true));
		//$xcrud3->set_lang("save_return","บันทึก");
		//$xcrud3->hide_button("return");

		$xcrud3->validation_required('marryDate,personFName,personLName,personIDCard');
		$xcrud3->validation_pattern('personTel,personIDCard','numeric')->validation_required('personIDCard',13);


		// End Form//

		
		
		$data['html3'] = $xcrud3->render();


		// select table//
		$xcrud4 = xcrud_get_instance();
		$xcrud4->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '2');
		
		$xcrud4->relation('motherID','tbl_staff_person','personID','personFName','staffID = '.$id.' and personType = \'1\'');
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personBirthday' => 'วันเกิด',
			'personIDCard' => 'เลขบัตรประชาชน',
			'personStatus' => 'สถานภาพ'

		);

		$xcrud4->button(site_url().'staffperson/graduation/?token='.$id.'&person={personID}','การศึกษา','fa fa-book','btn-danger'); 
		$xcrud4->button(site_url().'staffperson/view/?token='.$id.'&person={personID}','ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
		$xcrud4->button(site_url().'staffperson/edit/?token='.$id.'&person={personID}','แก้ไข','glyphicon glyphicon-edit','btn-warning'); 

		$xcrud4->columns('personFName,personLName,personLName,personBirthday');
		$xcrud4->label($col_name);
		//$xcrud4->column_pattern('personFName','{value} {personLName}');
		$xcrud4->column_callback('personBirthday','toBDDate');
		$xcrud4->unset_numbers()->unset_pagination()->unset_limitlist();
		$xcrud4->column_width('personChild','20%');
		// End List//

		//// Form //////

		$xcrud4->pass_var(array('staffID'=>$id,'personType'=>'2'));
		$xcrud4->fields('personFName,personLName,personIDCard,personBirthday,personStatus');
		$xcrud4->change_type('personStatus','radio','1',array('3'=>'ยังมีชีวิตอยู่','2'=>'เสียชีวิต','4'=>'ยกเลิกการใช้สิทธิ์สวัสดิการ'));
		//$xcrud4->set_lang("save_return","บันทึก");

		//$xcrud3->hide_button("return");

		$xcrud4->validation_required('personFName,personLName,personIDCard,personBirthday');
		$xcrud4->validation_pattern('personIDCard','numeric')->validation_required('personIDCard',13);

		// End Form//ß
		
		$xcrud4->unset_add();
		$xcrud4->unset_view();
		$xcrud4->unset_edit();

		$data['html4'] = $xcrud4->render();

		$data['id'] = $id;
		$data['title'] = "คู่สมรส / บุตร / บิดา-มารดา";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

	public function graduation(){
		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		$personid = mysql_real_escape_string($this->input->get('person'));

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		// select table//
		$xcrud->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '3');
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา'

		);

		$xcrud->columns('personFName,personStatus,personWork');
		$xcrud->label($col_name);
		$xcrud->column_pattern('personFName','{value} {personLName}');
		$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud->pass_var(array('staffID'=>$id,'personType'=>'3'));
		$xcrud->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
		$xcrud->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
		$xcrud->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
		$xcrud->set_lang("save_return","บันทึก");
		$xcrud->hide_button("return");

		$xcrud->validation_required('personFName,personLName');
		// End Form//
		
		$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'3'));

		if($r->num_rows() > 0) 
			$data['html1'] = $xcrud->render('edit',$r->row()->personID);
		else
			$data['html1'] = $xcrud->render('create');
		

		// select table//
		$xcrud2 = xcrud_get_instance();
		$xcrud2->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '4');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา'

		);

		$xcrud2->columns('personFName,personStatus,personWork');
		$xcrud2->label($col_name);
		$xcrud2->column_pattern('personFName','{value} {personLName}');
		$xcrud2->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud2->pass_var(array('staffID'=>$id,'personType'=>'4'));
		$xcrud2->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
		$xcrud2->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
		$xcrud2->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
		$xcrud2->set_lang("save_return","บันทึก");
		$xcrud2->hide_button("return");
		$xcrud2->validation_required('personFName,personLName');
		// End Form//
		
		$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'4'));

		if($r->num_rows() > 0) 
			$data['html2'] = $xcrud2->render('edit',$r->row()->personID);
		else
			$data['html2'] = $xcrud2->render('create');


		// select table//
		$xcrud3 = xcrud_get_instance();
		$xcrud3->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '1');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา',
			'personIDCard' => 'เลขบัตรประชาชน',
			'personFileHousehold' => 'ทะเบียนบ้าน',
			'personFileMarriage' => 'ทะเบียนสมรส'

		);

		$xcrud3->columns('marryDate,personFName,personLName,personWork');
		$xcrud3->label($col_name);
		//$xcrud3->column_pattern('personFName','{value} {personLName}');
		$xcrud3->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud3->pass_var(array('staffID'=>$id,'personType'=>'1'));
		$xcrud3->fields('marryDate,personFName,personLName,personIDCard,personWork,personTel,personStatus,personFileHousehold,personFileMarriage');
		$xcrud3->change_type('personStatus','radio','',array('1'=>'สมรส','0'=>'หย่า','2'=>'เสียชีวิต'));
		$xcrud3->column_callback('marryDate','toBDDate');
		$xcrud3->column_width('personFName','25%')->column_width('personLName','25%');
		$xcrud3->change_type('personFileHousehold', 'file', '', array('not_rename'=>true));
 		$xcrud3->change_type('personFileMarriage', 'file', '', array('not_rename'=>true));
		//$xcrud3->set_lang("save_return","บันทึก");
		//$xcrud3->hide_button("return");

		$xcrud3->validation_required('marryDate,personFName,personLName,personIDCard');
		$xcrud3->validation_pattern('personTel,personIDCard','numeric')->validation_required('personIDCard',13);


		// End Form//
		$data['html3'] = $xcrud3->render();


		// select table//
		$xcrud4 = xcrud_get_instance();
		$xcrud4->table('tbl_staff_edu_child')->where('personID =', $personid);
		$xcrud4->relation('eduRankID','tbl_education','eduID','eduName');
		$xcrud4->relation('eduPlaceID','tbl_edu_place','eduPlaceID','eduPlaceName');
		$xcrud4->relation('eduFactID','tbl_edu_faculty','eduFactID','eduFactName');
		$xcrud4->relation('eduDepartmentID','tbl_edu_department','eduDepartmentID','eduDepartmentName');
		$xcrud4->column_callback('eduStartDate','toBDDate');
		$xcrud4->column_callback('eduEndDate','toBDDate');

		//// List /////
		$col_name = array(
			'eduRankID' => 'ระดับการศึกษา',
			'eduPlaceID' => 'สถานศึกษา',
			'eduStartDate' => 'วันที่เริ่ม',
			'eduEndDate' => 'วันที่สำเร็จ',
			'eduFactID'	=> 'คณะ / สายการเรียน',
			'eduDepartmentID' => 'สาขา'

		);
		$xcrud4->pass_var('personID',$personid);
		$xcrud4->columns('eduRankID,eduPlaceID,eduStartDate,eduEndDate');
		$xcrud4->label($col_name);


		$xcrud4->fields('eduRankID,eduPlaceID,eduFactID,eduDepartmentID,eduStartDate,eduEndDate');

		$data['html4'] = $xcrud4->render();
		
		$data['id'] = $id;
		$data['title'] = "คู่สมรส / บุตร / บิดา-มารดา";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'mainedu', $data);

	}

	public function back(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '3');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา'

		);

		$xcrud->columns('personFName,personStatus,personWork');
		$xcrud->label($col_name);
		$xcrud->column_pattern('personFName','{value} {personLName}');
		$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud->pass_var(array('staffID'=>$id,'personType'=>'3'));
		$xcrud->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
		$xcrud->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
		$xcrud->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
		$xcrud->set_lang("save_return","บันทึก");
		$xcrud->hide_button("return");

		$xcrud->validation_required('personFName,personLName');
		// End Form//
		
		$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'3'));

		if($r->num_rows() > 0) 
			$data['html1'] = $xcrud->render('edit',$r->row()->personID);
		else
			$data['html1'] = $xcrud->render('create');
		

		// select table//
		$xcrud2 = xcrud_get_instance();
		$xcrud2->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '4');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา'

		);

		$xcrud2->columns('personFName,personStatus,personWork');
		$xcrud2->label($col_name);
		$xcrud2->column_pattern('personFName','{value} {personLName}');
		$xcrud2->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud2->pass_var(array('staffID'=>$id,'personType'=>'4'));
		$xcrud2->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
		$xcrud2->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
		$xcrud2->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
		$xcrud2->set_lang("save_return","บันทึก");
		$xcrud2->hide_button("return");
		$xcrud2->validation_required('personFName,personLName');
		// End Form//
		
		$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'4'));

		if($r->num_rows() > 0) 
			$data['html2'] = $xcrud2->render('edit',$r->row()->personID);
		else
			$data['html2'] = $xcrud2->render('create');


		// select table//
		$xcrud3 = xcrud_get_instance();
		$xcrud3->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '1');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา',
			'personIDCard' => 'เลขบัตรประชาชน',
			'personFileHousehold' => 'ทะเบียนบ้าน',
			'personFileMarriage' => 'ทะเบียนสมรส'

		);

		$xcrud3->columns('marryDate,personFName,personLName,personWork');
		$xcrud3->label($col_name);
		//$xcrud3->column_pattern('personFName','{value} {personLName}');
		$xcrud3->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud3->pass_var(array('staffID'=>$id,'personType'=>'1'));
		$xcrud3->fields('marryDate,personFName,personLName,personIDCard,personWork,personTel,personStatus,personFileHousehold,personFileMarriage');
		$xcrud3->change_type('personStatus','radio','',array('1'=>'สมรส','0'=>'หย่า','2'=>'เสียชีวิต'));
		$xcrud3->column_callback('marryDate','toBDDate');
		$xcrud3->column_width('personFName','25%')->column_width('personLName','25%');
		$xcrud3->change_type('personFileHousehold', 'file', '', array('not_rename'=>true));
 		$xcrud3->change_type('personFileMarriage', 'file', '', array('not_rename'=>true));
		//$xcrud3->set_lang("save_return","บันทึก");
		//$xcrud3->hide_button("return");

		$xcrud3->validation_required('marryDate,personFName,personLName,personIDCard');
		$xcrud3->validation_pattern('personTel,personIDCard','numeric')->validation_required('personIDCard',13);


		// End Form//

		
		
		$data['html3'] = $xcrud3->render();


		// select table//
		$xcrud4 = xcrud_get_instance();
		$xcrud4->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '2');
		
		$xcrud4->relation('motherID','tbl_staff_person','personID','personFName','staffID = '.$id.' and personType = \'1\'');
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personBirthday' => 'วันเกิด',
			'personIDCard' => 'เลขบัตรประชาชน',
			'personStatus' => 'สถานภาพ'

		);

		$xcrud4->button(site_url().'staffperson/graduation/?token='.$id.'&person={personID}','การศึกษา','fa fa-book','btn-danger'); 
		$xcrud4->button(site_url().'staffperson/view/?token='.$id.'&person={personID}','ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
		$xcrud4->button(site_url().'staffperson/edit/?token='.$id.'&person={personID}','แก้ไข','glyphicon glyphicon-edit','btn-warning'); 

		$xcrud4->columns('personFName,personLName,personLName,personBirthday');
		$xcrud4->label($col_name);
		//$xcrud4->column_pattern('personFName','{value} {personLName}');
		$xcrud4->column_callback('personBirthday','toBDDate');
		$xcrud4->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		$xcrud4->column_width('personChild','20%');
		// End List//

		//// Form //////

		$xcrud4->pass_var(array('staffID'=>$id,'personType'=>'2'));
		$xcrud4->fields('personFName,personLName,personIDCard,personBirthday,personStatus');
		$xcrud4->change_type('personStatus','radio','1',array('3'=>'ยังมีชีวิตอยู่','2'=>'เสียชีวิต','4'=>'ยกเลิกการใช้สิทธิ์สวัสดิการ'));
		//$xcrud4->set_lang("save_return","บันทึก");

		//$xcrud3->hide_button("return");

		$xcrud4->validation_required('personFName,personLName,personIDCard,personBirthday');
		$xcrud4->validation_pattern('personIDCard','numeric')->validation_required('personIDCard',13);

		// End Form//ß
		
		$xcrud4->unset_add();
		$xcrud4->unset_view();
		$xcrud4->unset_edit();

		$data['html4'] = $xcrud4->render();

		$data['id'] = $id;
		$data['title'] = "คู่สมรส / บุตร / บิดา-มารดา";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'mainedu', $data);

	}

	public function add(){
		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));
		

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		// select table//
		$xcrud->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '3');
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา'

		);

		$xcrud->columns('personFName,personStatus,personWork');
		$xcrud->label($col_name);
		$xcrud->column_pattern('personFName','{value} {personLName}');
		$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud->pass_var(array('staffID'=>$id,'personType'=>'3'));
		$xcrud->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
		$xcrud->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
		$xcrud->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
		$xcrud->set_lang("save_return","บันทึก");
		$xcrud->hide_button("return");

		$xcrud->validation_required('personFName,personLName');
		// End Form//
		
		$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'3'));

		if($r->num_rows() > 0) 
			$data['html1'] = $xcrud->render('edit',$r->row()->personID);
		else
			$data['html1'] = $xcrud->render('create');
		

		// select table//
		$xcrud2 = xcrud_get_instance();
		$xcrud2->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '4');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา'

		);

		$xcrud2->columns('personFName,personStatus,personWork');
		$xcrud2->label($col_name);
		$xcrud2->column_pattern('personFName','{value} {personLName}');
		$xcrud2->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud2->pass_var(array('staffID'=>$id,'personType'=>'4'));
		$xcrud2->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
		$xcrud2->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
		$xcrud2->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
		$xcrud2->set_lang("save_return","บันทึก");
		$xcrud2->hide_button("return");
		$xcrud2->validation_required('personFName,personLName');
		// End Form//
		
		$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'4'));

		if($r->num_rows() > 0) 
			$data['html2'] = $xcrud2->render('edit',$r->row()->personID);
		else
			$data['html2'] = $xcrud2->render('create');


		// select table//
		$xcrud3 = xcrud_get_instance();
		$xcrud3->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '1');
		
		
		//// List /////
		$col_name = array(
			'personFName' => 'ชื่อ',
			'personLName' => 'สกุล',
			'personStatus' => 'สถานภาพ',
			'personWork' => 'สถานที่ทำงาน',
			'personWorkEtc' => 'อื่นๆ',
			'marryDate' => 'วันที่สมรส',
			'personTel' => 'เบอร์โทรศัพท์',
			'personChile' => 'ลำดับที่ (บุตร)',
			'personBirthday' => 'วันเกิด',
			'mothdayID' =>'มารดา',
			'personIDCard' => 'เลขบัตรประชาชน',
			'personFileHousehold' => 'ทะเบียนบ้าน',
			'personFileMarriage' => 'ทะเบียนสมรส'

		);

		$xcrud3->columns('marryDate,personFName,personLName,personWork');
		$xcrud3->label($col_name);
		//$xcrud3->column_pattern('personFName','{value} {personLName}');
		$xcrud3->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
		
		// End List//

		//// Form //////

		$xcrud3->pass_var(array('staffID'=>$id,'personType'=>'1'));
		$xcrud3->fields('marryDate,personFName,personLName,personIDCard,personWork,personTel,personStatus,personFileHousehold,personFileMarriage');
		$xcrud3->change_type('personStatus','radio','',array('1'=>'สมรส','0'=>'หย่า','2'=>'เสียชีวิต'));
		$xcrud3->column_callback('marryDate','toBDDate');
		$xcrud3->column_width('personFName','25%')->column_width('personLName','25%');
		$xcrud3->change_type('personFileHousehold', 'file', '', array('not_rename'=>true));
 		$xcrud3->change_type('personFileMarriage', 'file', '', array('not_rename'=>true));
		//$xcrud3->set_lang("save_return","บันทึก");
		//$xcrud3->hide_button("return");

		$xcrud3->validation_required('marryDate,personFName,personLName,personIDCard');
		$xcrud3->validation_pattern('personTel,personIDCard','numeric')->validation_required('personIDCard',13);


		// End Form//
		$data['html3'] = $xcrud3->render();


		$data_f['staffid'] = $id;
        $data['html4'] = $this->load->view('form', $data_f,true);


        $data['id'] = $id;
		$data['title'] = "คู่สมรส / บุตร / บิดา-มารดา";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'mainedu', $data);

	}

	public function submit(){

		if(!empty($_POST['staffID'])){

			$staffID				= 	mysql_real_escape_string(trim($this->input->post('staffID',true)));
			$id						= 	mysql_real_escape_string(trim($this->input->post('id',true)));
			$personFName			= 	mysql_real_escape_string(trim($this->input->post('personFName',true)));
			$personLName			= 	mysql_real_escape_string(trim($this->input->post('personLName',true)));
			$personIDCard			= 	mysql_real_escape_string(trim($this->input->post('personIDCard',true)));
			$personBirthday			= 	toCEDate(mysql_real_escape_string(trim($this->input->post('personBirthday',true))));
			$personStatus			= 	mysql_real_escape_string(trim($this->input->post('personStatus',true)));
			$personMother			= 	mysql_real_escape_string(trim($this->input->post('personMother',true)));
			
			$arr = array(
				'staffID'			=> $staffID,
				'personIDCard'		=> $personIDCard,
				'personFName'		=> $personFName,
				'personLName'		=> $personLName,
				'personStatus'		=> $personStatus,
				'personBirthday'	=> $personBirthday,
				'personType'		=> '2',
				'personMother'		=> $personMother
			);	

			if($this->save($arr,$id)){
				redirect('staffperson/back/?token='.$staffID);
			}
			
		}
	}

	public function view(){
		if(!empty($_GET['token'])){

			$id = mysql_real_escape_string(trim($this->input->get('token',true)));

			$personid = mysql_real_escape_string(trim($this->input->get('person',true)));

			
			$this->load->helper('xcrud_helper');
			$xcrud = xcrud_get_instance();
			// select table//
			$xcrud->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '3');
			//// List /////
			$col_name = array(
				'personFName' => 'ชื่อ',
				'personLName' => 'สกุล',
				'personStatus' => 'สถานภาพ',
				'personWork' => 'สถานที่ทำงาน',
				'personWorkEtc' => 'อื่นๆ',
				'marryDate' => 'วันที่สมรส',
				'personTel' => 'เบอร์โทรศัพท์',
				'personChile' => 'ลำดับที่ (บุตร)',
				'personBirthday' => 'วันเกิด',
				'mothdayID' =>'มารดา'

			);

			$xcrud->columns('personFName,personStatus,personWork');
			$xcrud->label($col_name);
			$xcrud->column_pattern('personFName','{value} {personLName}');
			$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
			
			// End List//

			//// Form //////

			$xcrud->pass_var(array('staffID'=>$id,'personType'=>'3'));
			$xcrud->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
			$xcrud->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
			$xcrud->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
			$xcrud->set_lang("save_return","บันทึก");
			$xcrud->hide_button("return");

			$xcrud->validation_required('personFName,personLName');
			// End Form//
			
			$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'3'));

			if($r->num_rows() > 0) 
				$data['html1'] = $xcrud->render('edit',$r->row()->personID);
			else
				$data['html1'] = $xcrud->render('create');
			

			// select table//
			$xcrud2 = xcrud_get_instance();
			$xcrud2->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '4');
			
			
			//// List /////
			$col_name = array(
				'personFName' => 'ชื่อ',
				'personLName' => 'สกุล',
				'personStatus' => 'สถานภาพ',
				'personWork' => 'สถานที่ทำงาน',
				'personWorkEtc' => 'อื่นๆ',
				'marryDate' => 'วันที่สมรส',
				'personTel' => 'เบอร์โทรศัพท์',
				'personChile' => 'ลำดับที่ (บุตร)',
				'personBirthday' => 'วันเกิด',
				'mothdayID' =>'มารดา'

			);

			$xcrud2->columns('personFName,personStatus,personWork');
			$xcrud2->label($col_name);
			$xcrud2->column_pattern('personFName','{value} {personLName}');
			$xcrud2->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
			
			// End List//

			//// Form //////

			$xcrud2->pass_var(array('staffID'=>$id,'personType'=>'4'));
			$xcrud2->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
			$xcrud2->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
			$xcrud2->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
			$xcrud2->set_lang("save_return","บันทึก");
			$xcrud2->hide_button("return");
			$xcrud2->validation_required('personFName,personLName');
			// End Form//
			
			$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'4'));

			if($r->num_rows() > 0) 
				$data['html2'] = $xcrud2->render('edit',$r->row()->personID);
			else
				$data['html2'] = $xcrud2->render('create');


			// select table//
			$xcrud3 = xcrud_get_instance();
			$xcrud3->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '1');
			
			
			//// List /////
			$col_name = array(
				'personFName' => 'ชื่อ',
				'personLName' => 'สกุล',
				'personStatus' => 'สถานภาพ',
				'personWork' => 'สถานที่ทำงาน',
				'personWorkEtc' => 'อื่นๆ',
				'marryDate' => 'วันที่สมรส',
				'personTel' => 'เบอร์โทรศัพท์',
				'personChile' => 'ลำดับที่ (บุตร)',
				'personBirthday' => 'วันเกิด',
				'mothdayID' =>'มารดา',
				'personIDCard' => 'เลขบัตรประชาชน',
				'personFileHousehold' => 'ทะเบียนบ้าน',
				'personFileMarriage' => 'ทะเบียนสมรส'

			);

			$xcrud3->columns('marryDate,personFName,personLName,personWork');
			$xcrud3->label($col_name);
			//$xcrud3->column_pattern('personFName','{value} {personLName}');
			$xcrud3->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
			
			// End List//

			//// Form //////

			$xcrud3->pass_var(array('staffID'=>$id,'personType'=>'1'));
			$xcrud3->fields('marryDate,personFName,personLName,personIDCard,personWork,personTel,personStatus,personFileHousehold,personFileMarriage');
			$xcrud3->change_type('personStatus','radio','',array('1'=>'สมรส','0'=>'หย่า','2'=>'เสียชีวิต'));
			$xcrud3->column_callback('marryDate','toBDDate');
			$xcrud3->column_width('personFName','25%')->column_width('personLName','25%');
			$xcrud3->change_type('personFileHousehold', 'file', '', array('not_rename'=>true));
	 		$xcrud3->change_type('personFileMarriage', 'file', '', array('not_rename'=>true));
			//$xcrud3->set_lang("save_return","บันทึก");
			//$xcrud3->hide_button("return");

			$xcrud3->validation_required('marryDate,personFName,personLName,personIDCard');
			$xcrud3->validation_pattern('personTel,personIDCard','numeric')->validation_required('personIDCard',13);


			// End Form//
			$data['html3'] = $xcrud3->render();


			$data_f['r'] = $this->staff_person_model->get_by(array('personID'=>$personid),true);
			
			$data_f['staffid'] = $id;
			$data_f['id'] = $personid;
	        $data['html4'] = $this->load->view('form', $data_f,true);


	        $data['id'] = $id;
			$data['title'] = "คู่สมรส / บุตร / บิดา-มารดา";
			$data['staffName'] = getStaffName($id);
	        $this->template->load("template/tab",'mainedu', $data);

			
		}	
	}

	public function edit(){
		if(!empty($_GET['token'])){

			$id = mysql_real_escape_string(trim($this->input->get('token',true)));

			$personid = mysql_real_escape_string(trim($this->input->get('person',true)));

			
			$this->load->helper('xcrud_helper');
			$xcrud = xcrud_get_instance();
			// select table//
			$xcrud->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '3');
			//// List /////
			$col_name = array(
				'personFName' => 'ชื่อ',
				'personLName' => 'สกุล',
				'personStatus' => 'สถานภาพ',
				'personWork' => 'สถานที่ทำงาน',
				'personWorkEtc' => 'อื่นๆ',
				'marryDate' => 'วันที่สมรส',
				'personTel' => 'เบอร์โทรศัพท์',
				'personChile' => 'ลำดับที่ (บุตร)',
				'personBirthday' => 'วันเกิด',
				'mothdayID' =>'มารดา'

			);

			$xcrud->columns('personFName,personStatus,personWork');
			$xcrud->label($col_name);
			$xcrud->column_pattern('personFName','{value} {personLName}');
			$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
			
			// End List//

			//// Form //////

			$xcrud->pass_var(array('staffID'=>$id,'personType'=>'3'));
			$xcrud->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
			$xcrud->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
			$xcrud->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
			$xcrud->set_lang("save_return","บันทึก");
			$xcrud->hide_button("return");

			$xcrud->validation_required('personFName,personLName');
			// End Form//
			
			$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'3'));

			if($r->num_rows() > 0) 
				$data['html1'] = $xcrud->render('edit',$r->row()->personID);
			else
				$data['html1'] = $xcrud->render('create');
			

			// select table//
			$xcrud2 = xcrud_get_instance();
			$xcrud2->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '4');
			
			
			//// List /////
			$col_name = array(
				'personFName' => 'ชื่อ',
				'personLName' => 'สกุล',
				'personStatus' => 'สถานภาพ',
				'personWork' => 'สถานที่ทำงาน',
				'personWorkEtc' => 'อื่นๆ',
				'marryDate' => 'วันที่สมรส',
				'personTel' => 'เบอร์โทรศัพท์',
				'personChile' => 'ลำดับที่ (บุตร)',
				'personBirthday' => 'วันเกิด',
				'mothdayID' =>'มารดา'

			);

			$xcrud2->columns('personFName,personStatus,personWork');
			$xcrud2->label($col_name);
			$xcrud2->column_pattern('personFName','{value} {personLName}');
			$xcrud2->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
			
			// End List//

			//// Form //////

			$xcrud2->pass_var(array('staffID'=>$id,'personType'=>'4'));
			$xcrud2->fields('personFName,personLName,personStatus,personWork,personWorkEtc');
			$xcrud2->change_type('personStatus','select','',array('3'=>'มีชีวิตอยู่','2'=>'ถึงแก่กรรม'));
			$xcrud2->change_type('personWork','select','',array('อสค'=>'อสค','เคยทำ อสค'=>'เคยทำ อสค','อื่นๆ'=>'อื่นๆ'));
			$xcrud2->set_lang("save_return","บันทึก");
			$xcrud2->hide_button("return");
			$xcrud2->validation_required('personFName,personLName');
			// End Form//
			
			$r = $this->db->get_where('tbl_staff_person',array('staffID'=>$id,'personType'=>'4'));

			if($r->num_rows() > 0) 
				$data['html2'] = $xcrud2->render('edit',$r->row()->personID);
			else
				$data['html2'] = $xcrud2->render('create');


			// select table//
			$xcrud3 = xcrud_get_instance();
			$xcrud3->table('tbl_staff_person')->where('staffID =', $id)->where('personType =', '1');
			
			
			//// List /////
			$col_name = array(
				'personFName' => 'ชื่อ',
				'personLName' => 'สกุล',
				'personStatus' => 'สถานภาพ',
				'personWork' => 'สถานที่ทำงาน',
				'personWorkEtc' => 'อื่นๆ',
				'marryDate' => 'วันที่สมรส',
				'personTel' => 'เบอร์โทรศัพท์',
				'personChile' => 'ลำดับที่ (บุตร)',
				'personBirthday' => 'วันเกิด',
				'mothdayID' =>'มารดา',
				'personIDCard' => 'เลขบัตรประชาชน',
				'personFileHousehold' => 'ทะเบียนบ้าน',
				'personFileMarriage' => 'ทะเบียนสมรส'

			);

			$xcrud3->columns('marryDate,personFName,personLName,personWork');
			$xcrud3->label($col_name);
			//$xcrud3->column_pattern('personFName','{value} {personLName}');
			$xcrud3->unset_numbers()->unset_pagination()->unset_search()->unset_limitlist();
			
			// End List//

			//// Form //////

			$xcrud3->pass_var(array('staffID'=>$id,'personType'=>'1'));
			$xcrud3->fields('marryDate,personFName,personLName,personIDCard,personWork,personTel,personStatus,personFileHousehold,personFileMarriage');
			$xcrud3->change_type('personStatus','radio','',array('1'=>'สมรส','0'=>'หย่า','2'=>'เสียชีวิต'));
			$xcrud3->column_callback('marryDate','toBDDate');
			$xcrud3->column_width('personFName','25%')->column_width('personLName','25%');
			$xcrud3->change_type('personFileHousehold', 'file', '', array('not_rename'=>true));
	 		$xcrud3->change_type('personFileMarriage', 'file', '', array('not_rename'=>true));
			//$xcrud3->set_lang("save_return","บันทึก");
			//$xcrud3->hide_button("return");

			$xcrud3->validation_required('marryDate,personFName,personLName,personIDCard');
			$xcrud3->validation_pattern('personTel,personIDCard','numeric')->validation_required('personIDCard',13);


			// End Form//
			$data['html3'] = $xcrud3->render();


			$data_f['r'] = $this->staff_person_model->get_by(array('personID'=>$personid),true);
			
			$data_f['staffid'] = $id;
			$data_f['id'] = $personid;
	        $data['html4'] = $this->load->view('form', $data_f,true);


	        $data['id'] = $id;
			$data['title'] = "คู่สมรส / บุตร / บิดา-มารดา";
			$data['staffName'] = getStaffName($id);
	        $this->template->load("template/tab",'mainedu', $data);

			
		}	
	}

	public function getCountPerson(){
		if(!empty($_GET['id'])){
			$id = mysql_real_escape_string(trim($this->input->get('id',true)));

			$r = $this->staff_person_model->get_by(array('staffID'=>$id,'personType'=>'2'));

			echo count($r);

		}
	}

	public function validateIDCard(){
		if(!empty($_GET['id'])){
			$id = mysql_real_escape_string(trim($this->input->get('id',true)));
			$idcard = mysql_real_escape_string(trim($this->input->get('idcard',true)));
			$status = mysql_real_escape_string(trim($this->input->get('status',true)));
			$person = (!empty($_GET['person'])) ? mysql_real_escape_string(trim($this->input->get('person',true))) : "";
			$r = $this->staff_person_model->get_by(array('staffID'=>$id,'personType'=>'2','personIDCard'=>$idcard),true);
		
			if(!empty($r)){
				if(!empty($person)){
					if(($r->personStatus != '2') && ($r->personID != $person)){
						echo '1';
						exit;
					}
				}else{
					if($r->personStatus != '2'){
						echo '1';
						exit;
					}
				}
				
			}
			
			echo '0';

		}
	}

	public function getStaffPerson(){
		if(!empty($_GET['id']) && !empty($_GET['staffID'])){
			//person type//
			$id = mysql_real_escape_string(trim($this->input->get('id',true)));
			// staff ID//
			$staffID = mysql_real_escape_string(trim($this->input->get('staffID',true)));
			$mode = mysql_real_escape_string(trim($this->input->get('mode',true)));
			$personID = (isset($_GET['personID'])) ? mysql_real_escape_string(trim($this->input->get('personID',true))): "";
			if($mode != "view"){
				switch($id){
					//ตนเอง//
					case '1' : $r = $this->staff_model->get_by(array('ID'=>$staffID),true);
								echo (!empty($r)) ? $r->staffFName.' '.$r->staffLName : "";
								break;
					//บิดา-มารดา//
					case '2' : echo getDropdown(listDataSql("SELECT * FROM tbl_staff_person WHERE (personType = 3 || personType = 4) AND staffID = '$staffID'",'personID','personFName,personLName'),'personID',$personID,'class="form-control"'); break;
					//คู่สมรส//
					case '3' : echo getDropdown(listDataSql("SELECT * FROM tbl_staff_person WHERE personType = 1 AND staffID = '$staffID'",'personID','personFName,personLName'),'personID',$personID,'class="form-control"'); break;
					//บุตร//
					case '4' : echo getDropdown(listDataSql("SELECT * FROM tbl_staff_person WHERE personType = 2 AND staffID = '$staffID'",'personID','personFName,personLName'),'personID',$personID,'class="form-control"'); break;
				}
			}else{
				if($id == '1'){
					$r = $this->staff_model->get_by(array('ID'=>$staffID),true);
					echo (!empty($r)) ? $r->staffFName.' '.$r->staffLName : "";
				}else{
					$r = $this->staff_person_model->get_by(array('personID'=>$personID),true);
					echo (!empty($r)) ? $r->personFName.' '.$r->personLName : "";
				}
				
			}
			
		}
	}

}
/* End of file staffmove.php */
/* Location: ./application/module/staffmove/staffmove.php */