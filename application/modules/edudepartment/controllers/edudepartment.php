<?php

class Edudepartment extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		
		// select table//
		$xcrud->table('tbl_edu_department');
		
		
		//// List /////
		$col_name = array(
			'eduDepartmentName' => 'สาขา'

		);

		$xcrud->columns('eduDepartmentName');
		$xcrud->label($col_name);
		// End List//

		//// Form //////

		$xcrud->fields('eduDepartmentName');

		$data['html'] = $xcrud->render();
		

		$data['title'] = "สาขาวิชา";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file edudepartment.php */
/* Location: ./application/module/edudepartment/edudepartment.php */