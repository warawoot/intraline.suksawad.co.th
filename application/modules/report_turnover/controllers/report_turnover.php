<?php

class Report_turnover extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('organization_model');
		
	}
	
	public function index(){


		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);


 		$assignID = getAssignLasted();

        $ida = array();
        $this->getCategoryTree(0, $assignID,$ida); 

        $data['r'] = $ida;
       
		$this->template->load('template/admin', 'reportturnover',$data);


	}

	function getCategoryTree($upper = 0 , $assignID=0,&$ida) {

        $rows = $this->organization_model->getTree($upper,$assignID);
        $tree = '';


         
        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {

                    
                    
                     if(count($ida) > 0){
                        if($row->upperOrgID == 1) {
                            $level = 1;
                        } else{
                            for($i=0; $i<count($ida);$i++){
                                $list = explode(':',$ida[$i]);
                                if($list[0] == $row->upperOrgID){
                                    $level = $list[2]+1;
                                    break;
                                }              
                            }
                          
                        }
                    }else
                        $level = 0;

                   
                        array_push($ida,$row->orgID.':'.$row->orgName.':'.$level.':'.$row->upperOrgID);
                        //echo $row->orgID.' '.$row->orgName.' '.$level.' '.$row->upperOrgID.'<br>';
                        $this->getCategoryTree($row->orgID, $row->assignID,$ida);
            }
        }
        
    }

	public function print_pdf(){

		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);

		$assignID = getAssignLasted();
        $ida = array();
        $this->getCategoryTree(0, $assignID,$ida); 

        $data['r'] = $ida;
        
		$data_r['html'] = $this->load->view('print',$data,true);
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		
		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);

		$assignID = getAssignLasted();
        $ida = array();
        $this->getCategoryTree(0, $assignID,$ida); 

        $data['r'] = $ida;
		
		$this->load->view('print_excel',$data);

	}

}
/* End of file report_turnover.php */
/* Location: ./application/module/report_turnover/report_turnover.php */