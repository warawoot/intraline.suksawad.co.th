<?php

class Organization_model extends MY_Model {
    
  
    function __construct() {
        parent::__construct();
        $this->table="tbl_org_chart";
        $this->pk = "orgID";
    }
        
    public function getTree($upper,$assignID){

         $rows = $this->db->select('t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t1.upperOrgID')
                ->where('t1.upperOrgID', $upper)
                ->where('t1.assignID', $assignID)
                ->order_by('sequence','asc')
                ->get('tbl_org_chart as t1')
                ->result();
        return $rows;
       
    }
} 