<?php

$year = (!empty($year)) ? $year : (date("Y")+543);

header('Content-type: application/excel');
$filename = 'report_turnover.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        <style>
			table,th,td{
				border: 1px solid black;
				border-collapse:collapse;
				text-align:center;
			}


		</style>
    </head>
	<body>

	
		<h3 style="text-align:center;">
		<br>
			อัตราพนักงานเข้าใหม่ / พนักงานออก
		
		</h5>
		
		<br>
		
		
		<div class="xcrud-list-container">
		<table class="xcrud-list table table-striped table-hover table-bordered">
			<tr >
				<th >หน่วยงาน</th>
				<th>ฝ่าย</th>
				<th>แผนก</th>
				<th>เข้าใหม่</th>
				<th>ออก</th>
				<th>ผลต่าง</th>
			</tr>
			
			
			<?php 
			$i=0;
			if(!empty($r)){
				$sum_in = 0;
				$sum_out = 0;
				$sum_diff = 0;
				foreach($r as $row){
					
					$list = explode(":",$row);

					if($list[2] <= 3)
					echo '<tr>';

					if($list[2] == 0 || $list[2] == 1){
						echo '<td colspan="3" style="text-align:left;">'.$list[1].'</td>';
					}else if($list[2] == 2){
						echo '<td></td>';
						echo '<td colspan="2" style="text-align:left;">'.$list[1].'</td>';
					}else if($list[2] == 3){
						echo '<td colspan="2" ></td>';
						echo '<td style="text-align:left;">'.$list[1].'</td>';
					}
					if($list[2] <= 3){
			?>
						<td style="text-align:center;"><?php $in = getEmployeeIn($list[0],$month,$year); $sum_in+=$in; echo $in; ?></td>
						<td style="text-align:center;"><?php $out = getEmployeeOut($list[0],$month,$year); $sum_out+=$out; echo $out; ?></td>
						<td style="text-align:center;"><?php $diff = $in-$out; $sum_diff+=$diff; echo $diff;?></td>
						</tr>
			<?php	}
				} ?>
					<tr>
						<td colspan="3">รวมทั้งหมด</td>
						<td style="text-align:center;"><?php echo number_format($sum_in); ?></td>
						<td style="text-align:center;"><?php echo number_format($sum_out); ?></td>
						<td style="text-align:center;"><?php echo number_format($sum_diff); ?></td>
					</tr>
			<?php } ?>
		
			
		</table>
		</div>

	</body>
</html>
