
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
          ?>
	<h5 style="text-align:center;">
		<br>
			อัตราพนักงานเข้าใหม่ / พนักงานออก
		
	</h5>
	
	<br>
	
	<div class=" col-lg-5 col-md-5 col-sm-5 col-xs-12">
		<label for="">ค้นหา เดือน</label>

		<select  id="month" class="form-control">
			<?php foreach($th_month as $key=>$val){ ?>
			<option  value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
			<?php } ?>
		</select>
	</div>
	<div class=" col-lg-5 col-md-5 col-sm-5 col-xs-12">
		
		<!--ส่งค่าไปยัง function search -->
		<label for="">ปี พ.ศ.</label>
		<select  class="form-control" id="year">
			<?php for($i=date("Y");  $i>=(date("Y")-60); $i--){ ?>
			<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
			<?php } ?>
			
		</select>
	</div>

	<!--ส่งค่าไปยัง function search -->
	
	<div class="col-lg-2 col-md-2 col-sm-5 col-xs-12" style="margin-top:20px;">
		<a class="btn btn-warning"  style="display:bottom;" href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>
	</div>

	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px;">	
		<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
		<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>
	</div>
	
	
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-hover table-bordered">
		<tr class="xcrud-th">
			<th >หน่วยงาน</th>
			<th>ฝ่าย</th>
			<th>แผนก</th>
			<th>เข้าใหม่</th>
			<th>ออก</th>
			<th>ผลต่าง</th>
		</tr>
		
		
		<?php 
		$i=0;
		if(!empty($r)){
			$sum_in = 0;
			$sum_out = 0;
			$sum_diff = 0;
			
			foreach($r as $row){
				
				$list = explode(":",$row);

				if($list[2] <= 3)
				echo '<tr>';

				if($list[2] == 0 || $list[2] == 1){
					echo '<td colspan="3" style="text-align:left;">'.$list[1].'</td>';
				}else if($list[2] == 2){
					echo '<td></td>';
					echo '<td colspan="2" style="text-align:left;">'.$list[1].'</td>';
				}else if($list[2] == 3){
					echo '<td colspan="2" ></td>';
					echo '<td style="text-align:left;">'.$list[1].'</td>';
				}

				if($list[2] <= 3){
		?>
		
									<!--ส่งค่าไปยังหน้า ganeral_helper-->
					<td><?php $in = getEmployeeIn($list[0],$month,$year); $sum_in+=$in; echo $in; ?></td>
					<td><?php $out = getEmployeeOut($list[0],$month,$year); $sum_out+=$out; echo $out; ?></td>
					<td><?php $diff = $in-$out; $sum_diff+=$diff; echo $diff;?></td>
					</tr>
		<?php	  }
				} ?>
				<tr>
					<td colspan="3">รวมทั้งหมด</td>
					<td><?php echo number_format($sum_in); ?></td>
					<td><?php echo number_format($sum_out); ?></td>
					<td><?php echo number_format($sum_diff); ?></td>
				</tr>
		<?php } ?>
	
		
	</table>
	</div>


	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function search(){
        															 // กรณีนี้ส่งค่าไปยัง index
			window.location='<?php echo site_url();?>report_turnover?month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
		}

        function print_pdf(){
        	window.open('<?php echo site_url();?>report_turnover/print_pdf?month='+$('#month :selected').val()+'&year='+$('#year :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>report_turnover/print_excel?month='+$('#month :selected').val()+'&year='+$('#year :selected').val());
        }

        
</script>
