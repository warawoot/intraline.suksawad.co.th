<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
          ?>
	<h3 style="text-align:center;">
		<br>
			อัตราพนักงานเข้าใหม่ / พนักงานออก
		
	</h5>
	
	<br>
	
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
		<tr >
			<th >หน่วยงาน</th>
			<th>ฝ่าย</th>
			<th>แผนก</th>
			<th>เข้าใหม่</th>
			<th>ออก</th>
			<th>ผลต่าง</th>
		</tr>
		
		
		<?php 
		$i=0;
		if(!empty($r)){
			$sum_in = 0;
			$sum_out = 0;
			$sum_diff = 0;
			foreach($r as $row){
				
				$list = explode(":",$row);

				if($list[2] <= 3)
				echo '<tr>';

				if($list[2] == 0 || $list[2] == 1){
					echo '<td colspan="3" style="text-align:left;">'.$list[1].'</td>';
				}else if($list[2] == 2){
					echo '<td></td>';
					echo '<td colspan="2" style="text-align:left;">'.$list[1].'</td>';
				}else if($list[2] == 3){
					echo '<td colspan="2" ></td>';
					echo '<td style="text-align:left;">'.$list[1].'</td>';
				}

				if($list[2] <= 3){
		?>
					<td style="text-align:center;"><?php $in = getEmployeeIn($list[0],$month,$year); $sum_in+=$in; echo $in; ?></td>
					<td style="text-align:center;"><?php $out = getEmployeeOut($list[0],$month,$year); $sum_out+=$out; echo $out; ?></td>
					<td style="text-align:center;"><?php $diff = $in-$out; $sum_diff+=$diff; echo $diff;?></td>
					</tr>
		<?php	}
			} ?>
				<tr>
					<td colspan="3">รวมทั้งหมด</td>
					<td style="text-align:center;"><?php echo number_format($sum_in); ?></td>
					<td style="text-align:center;"><?php echo number_format($sum_out); ?></td>
					<td style="text-align:center;"><?php echo number_format($sum_diff); ?></td>
				</tr>
		<?php } ?>
	
		
	</table>
	</div>