<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

       public function getAllOrg(){

            $assignID = getAssignLasted();

            $sql = "SELECT * FROM tbl_org_chart 
                    where (upperOrgID = '0' OR upperOrgID = '1')
                    AND assignID = $assignID";
            $r = $this->db->query($sql);
            return $r->result();

        }

        public function getRankByOrg($org,$rankID,$workID='',$staffID=''){

            if(!empty($org)){

                if(!empty($workID))
                    $subsql = " SELECT max(workStartDate) from tbl_staff_work WHERE staffID=c.staffID AND workID != $workID AND c.staffID = $staffID";
                else
                    $subsql = " SELECT max(workStartDate) from tbl_staff_work WHERE staffID=c.staffID";

                $sql = "SELECT  c.*
                        FROM tbl_staff_work c 
                        JOIN tbl_staff s ON c.staffID = s.ID
                        WHERE c.workStartDate = ($subsql) AND c.orgID = $org AND c.rankID = $rankID";

                

                $r = $this->db->query($sql);
                return $r->result();
            }   

        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/reg/models/staff_model.php */