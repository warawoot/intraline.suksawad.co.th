<?php

$year = (!empty($year)) ? $year : (date("Y")+543);

header('Content-type: application/excel');
$filename = 'โครงสร้างเลื่อนระดับพนักงาน .xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        <style>
			table,th,td{
				border: 1px solid black;
				border-collapse:collapse;
				text-align:center;
			}


		</style>
    </head>
	<body>

	
		<h5 style="text-align:center;"></h5>
		<h5 style="text-align:center;">โครงสร้างเลื่อนระดับพนักงาน ณ วันที่ <?php echo toFullDate(date("Y-m-d")); ?></h5>

		<br>

		<p style="font-size:11px;">ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></p>
		
		<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered" style="font-size:11px;">
	
		<tr >
			<th rowspan="2">ฝ่าย / สำนัก</th>
			<th colspan="11">ช่วงระดับ</th>
			<!--<th rowspan="2">ระดับเฉลีย</th>-->
			
			
		</tr>
		
		
		<tr >
			
			<td>1 -> 2</td>
			<td>2 -> 3</td>
			<td>3 -> 4</td>
			<td>4 -> 5</td>
			<td>5 -> 6</td>
			<td>6 -> 7</td>
			<td>7 -> 8</td>
			<td>8 -> 9</td>
			<td>9 -> 10</td>
			<td>ล.1 -> ล.2</td>
			<td>ล.2 -> ล.3</td>
		</tr>
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 

						$count1=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,2);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,1,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count1++;
								}	
							}	
						}

						$count2=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,3);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,2,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count2++;
								}	
							}	
						}

						$count3=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,4);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,3,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count3++;
								}	
							}	
						}
						
						$count4=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,5);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,4,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count4++;
								}	
							}	
						}

						$count5=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,6);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,5,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count5++;
								}	
							}	
						}

						$count6=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,7);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,6,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count6++;
								}	
							}	
						}

						$count7=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,8);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,7,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count7++;
								}	
							}	
						}

						$count8=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,9);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,8,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count8++;
								}	
							}	
						}

						$count9=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,10);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,9,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count9++;
								}	
							}	
						}

						$count10=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,11);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,10,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count10++;
								}	
							}	
						}

						$count11=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,12);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,11,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count11++;
								}	
							}	
						}

					 ?>
					<tr>
						
						<td><?php echo $row->orgName; ?></td>
						<td style="text-align:center;"><?php echo $count1; ?></td>
						<td style="text-align:center;"><?php echo $count2; ?></td>
						<td style="text-align:center;"><?php echo $count3; ?></td>
						<td style="text-align:center;"><?php echo $count4; ?></td>
						<td style="text-align:center;"><?php echo $count5; ?></td>
						<td style="text-align:center;"><?php echo $count6; ?></td>
						<td style="text-align:center;"><?php echo $count7; ?></td>
						<td style="text-align:center;"><?php echo $count8; ?></td>
						<td style="text-align:center;"><?php echo $count9; ?></td>
						<td style="text-align:center;"><?php echo $count10; ?></td>
						<td style="text-align:center;"><?php echo $count11; ?></td>
						<!--<td></td>-->
					</tr>
				
				
		<?php }}

		
		 ?>
		
	</table>
	</div>

	</body>
</html>
