
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	
	<h5 style="text-align:center;">
		<br>
			โครงสร้างเลื่อนระดับพนักงาน ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?>
		
	</h5>
	<br><br>

	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="2">ฝ่าย / สำนัก</th>
			<th colspan="11">ช่วงระดับ</th>
			<!--<th rowspan="2">ระดับเฉลีย</th>-->
			
			
		</tr>
		
		
		<tr >
			
			<td>1 -> 2</td>
			<td>2 -> 3</td>
			<td>3 -> 4</td>
			<td>4 -> 5</td>
			<td>5 -> 6</td>
			<td>6 -> 7</td>
			<td>7 -> 8</td>
			<td>8 -> 9</td>
			<td>9 -> 10</td>
			<td>ล.1 -> ล.2</td>
			<td>ล.2 -> ล.3</td>
		</tr>
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 

						$count1=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,2);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,1,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count1++;
								}	
							}	
						}

						$count2=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,3);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,2,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count2++;
								}	
							}	
						}

						$count3=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,4);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,3,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count3++;
								}	
							}	
						}
						
						$count4=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,5);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,4,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count4++;
								}	
							}	
						}

						$count5=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,6);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,5,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count5++;
								}	
							}	
						}

						$count6=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,7);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,6,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count6++;
								}	
							}	
						}

						$count7=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,8);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,7,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count7++;
								}	
							}	
						}

						$count8=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,9);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,8,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count8++;
								}	
							}	
						}

						$count9=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,10);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,9,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count9++;
								}	
							}	
						}

						$count10=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,11);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,10,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count10++;
								}	
							}	
						}

						$count11=0;
						$ro = $this->staff_model->getRankByOrg($row->orgID,12);
						if(!empty($ro)){

							foreach($ro as $rowo){
								$ro2 = $this->staff_model->getRankByOrg($row->orgID,11,$rowo->workID,$rowo->staffID);

								if(!empty($ro2)){
									$count11++;
								}	
							}	
						}

					 ?>
					<tr>
						
						<td style="text-align:left;"><?php echo $row->orgName; ?></td>
						<td><?php echo $count1; ?></td>
						<td><?php echo $count2; ?></td>
						<td><?php echo $count3; ?></td>
						<td><?php echo $count4; ?></td>
						<td><?php echo $count5; ?></td>
						<td><?php echo $count6; ?></td>
						<td><?php echo $count7; ?></td>
						<td><?php echo $count8; ?></td>
						<td><?php echo $count9; ?></td>
						<td><?php echo $count10; ?></td>
						<td><?php echo $count11; ?></td>
						<!--<td></td>-->
					</tr>
				
				
		<?php }}

		
		 ?>
		
	</table>
	</div>


	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportstatrank/print_pdf');
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportstatrank/print_excel');
        }
</script>
