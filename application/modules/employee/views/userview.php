<style>
  .ui-datepicker {
      width : 100%;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>ระบบใบลา</h3>
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span style="color:#A9A6A6;">ระบบบริการตัวเองของพนักงาน</span>
      </header>
    </section>
  </div>
</div>

<div class="row">
  <div class="col-md-3">
    <div class="pastel-pink" style="padding:20px">
      <h4>ลาป่วย : <?=$sa1["numDay"]?> วัน</h4>
      <h6>สามารถลาได้ <?=$ac1["numDay"]?> วัน ได้รับค่าจ้างปกติ</h6>
    </div>
    <div class="icon">
      <i class="ion ion-medkit"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-blue" style="padding:20px">
      <h4>ลากิจ : <?=$sa2["numDay"]?> วัน</h4>
      <h6>ไม่จ่ายค่าจ้างตามจำนวนวันลา</h6>
    </div>
    <div class="icon">
      <i class="ion ion-briefcase"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-green" style="padding:20px">
      <h4>ลาพักผ่อน : <?=$sa7["numDay"]?> วัน</h4>
      <h6>จากสิทธิ <?=$ac7["numDay"]?> วัน / การทำงาน 1 ปี</h6>
    </div>
    <div class="icon">
      <i class="ion ion-ios-partlysunny-outline"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-yellow" style="padding:20px">
      <h4>ลาอื่นๆ : <?=$sao["numDay"]?> วัน</h4>
      <h6>&nbsp;</h6>
    </div>
    <div class="icon">
      <i class="ion ion-email"></i>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-9">
  <br><br>
  <div class="col-md-3"><h4>ประวัติการลาปี </h4></div>
  <div class="col-md-6">
      <select name="filter_year" id="filter_year" class="form-control" style="width:100px; float:right">
        <?php
          for ($yi = date("Y")-5; $yi <= date("Y"); $yi++) {
            if($yi == $y){
              $selecty	=	"selected=\"selected\"";
            }else{
              $selecty	=	'';
            }
            echo '<option value="'.$yi.'"'.$selecty.' > '.($yi+543).' </option>';
          }
        ?>
      </select>  
  </div>
  <div class="col-md-3" style="padding-left:72px;padding-top:5px"><a class="btn btn-success" href="<?=base_url()?>absence/add"><i class="glyphicon glyphicon-plus-sign"></i> สร้างใบลา</a>  </div>
  <br><br><br><br>
  <table class="table">
      <thead>
        <tr>
          <th colspan="2" class="text-center">วันที่ลา</th>
          <th class="text-center">ประเภท</th>
          <th class="text-center">สถานะ</th>
          <th class="text-center">ไฟล์</th>
          <th class="text-center">หมายเหตุ</th>
        </tr>
      </thead>
      <tbody>
      <?php
    	if($abs != NULL){
          $rowclass = "";
      		foreach($abs->result() as  $row){

            switch ($row->absTypeID) {
              case 1:
              case 8:
                $rowclass = "danger";
                break;
              case 2:
                $rowclass = "info";
                break;
              case 7:
                $rowclass = "success";
                break;
              default:
                $rowclass = "warning";
              }

            switch ($row->statusApprove) {
              case 1:
                $statusApprove = "อนุมัติแล้ว";
                break;
              case 2:
                $statusApprove = "ไม่อนุมัติ";
                break;
              case 3:
                $statusApprove = "ยกเลิก";
                break;
              default:
                $statusApprove = "รออนุมัติ";
              }

              if($row->statusApprove == 3) {
                $rowclass .= " cancelled";
              }

        ?>
        <tr class="<?=$rowclass?>">
          <td style="width:15%;" class="text-center"><?=toFullDate($row->absStartDate,'th');?></td>
          <td style="width:15%;" class="text-center"><?=toFullDate($row->absEndDate,'th');?></td>
          <td style="width:30%;"><?=$row->absName?></td>
          <td style="width:20%;" class="text-center"><?=$statusApprove?></td>
          <td><a href="<?php echo site_url()."uploads/absenceFileMedical/".$row->absenceFile ?>" target="_blank" style="color:blue"><?php if($row->absenceFile != ''){echo '<i class="fa fa-medkit fa-2x" title="ไฟล์"></i>';}?></a></td>
          <td style="width:20%;" class="text-center">
            <?php if($row->statusApprove!=3) {?>
              <a class="btn btn-warning"
                href="<?=base_url()?>absence/edit?id=<?=$row->stfAbsID?>&amp;empID=<?=$row->staffID?>"
                title="แก้ไข"><i class="glyphicon glyphicon-edit"></i></a>
            <?php
            } else {
            ?>
            <a class="btn btn-default"
              href="javascript:alert('ใบลานี้ยกเลิกไปแล้ว ไม่สามารถแก้ไขได้');"
              title="แก้ไข"><i class="glyphicon glyphicon-edit"></i></a>
            <?php
            }
            ?>
          </td>
        </tr>
	    <?php
	    } //foreach
	    }else{?>
	    <tr><td colspan="4" style="text-align:center">ไม่พบข้อมูล</td></tr>
	    <?php
    	}?>
      </tbody>
    </table>
  </div>
  <div class="col-md-3">
    <br><br><br>
    <div id="calMonthPrev"></div><br>
    <div id="calMonthCurrent"></div><br>
    <div id="calMonthNext"></div><br>
  </div>
</div>
<script>

$(document).ready(function () {
    $("#calMonthCurrent").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        yearOffSet: 543,
        yearRange: '-20:+0',
        changeMonth: false,
        changeYear: false ,
        showOn: "both",
        buttonImage: "<?=base_url()?>assets/images/common_calendar_month_-20.png",
        buttonImageOnly: false,
        inline: true
    });

    $("#calMonthNext").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        yearOffSet: 543,
        yearRange: '-20:+0',
        changeMonth: false,
        changeYear: false ,
        showOn: "both",
        buttonImage: "<?=base_url()?>assets/images/common_calendar_month_-20.png",
        buttonImageOnly: false,
        inline: true,
        defaultDate: '+1m'
    });

    $("#calMonthPrev").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        yearOffSet: 543,
        yearRange: '-20:+0',
        changeMonth: false,
        changeYear: false ,
        showOn: "both",
        buttonImage: "<?=base_url()?>assets/images/common_calendar_month_-20.png",
        buttonImageOnly: false,
        inline: true,
        defaultDate: '-1m'
    });
});
</script>
