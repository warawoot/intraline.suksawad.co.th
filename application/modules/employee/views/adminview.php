<div class="row">
  <div class="col-md-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>Dashboard</h3>
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span style="color:#A9A6A6;">Dashboard</span>
      </header>
    </section>  
  </div>
</div>
<div class="row">
  <!--div class="col-md-2 col-md-offset-1"><a href="<?=base_url()?>appoint"><img src="<?=base_url()?>assets/images/icon/icon-orgchart.png" style="border:solid #CCC 1px" width="100%" /></a></div-->
  <div class="col-md-3"><a href="<?=base_url()?>reg"><img src="<?=base_url()?>assets/images/icon/icon-people.png" style="border:solid #CCC 1px" width="100%" /></a></div>
  <div class="col-md-3"><a href="<?=base_url()?>report_work"><img src="<?=base_url()?>assets/images/icon/icon-timeattendance.png" style="border:solid #CCC 1px" width="100%" /></a></div>
  <div class="col-md-3"><a href="<?=base_url()?>absence"><img src="<?=base_url()?>assets/images/icon/icon-leave.png" style="border:solid #CCC 1px" width="100%" /></a></div>
  <div class="col-md-3"><a href="<?=base_url()?>rate"><img src="<?=base_url()?>assets/images/icon/icon-performance.png" style="border:solid #CCC 1px" width="100%" /></a></div>
</div>

<!--div class="row" style="padding-top:30px">
  <div class="col-md-6">
    <img src="<?=base_url()?>assets/images/dashboard/Chart_TurnOver.jpg" width="100%" /><br/>
    <a class="btn btn-info" href="/report_turnover" style="width:100%"><i class="fa fa-refresh"></i> รายงาน Turn Over </a>
  </div>
  <div class="col-md-6">
    <img src="<?=base_url()?>assets/images/dashboard/Chart_TimeAttn.jpg" width="100%" /><br/>
    <a class="btn btn-info" href="/report_work" style="width:100%"><i class="fa fa-clock-o"></i> รายงาน Time Attendance </a>
  </div>
</div-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <div class="panel-body" style="text-align:center;"> 
                <div class="xcrud-list-container">
                <h3 style="float: left;">พนักงานที่ลาวันนี้</h3>
                <table class="xcrud-list table table-striped table-hover table-bordered">  
                  <tr>
                    <th>รหัส</th>
                    <th>ชื่อ-สกุล</th>
                  </tr>
                  <?php 
                  if($rLeave != NULL){
                    foreach($rLeave->result() as $row){ 
                      ?>
                      <tr> 
                        <td><?php echo $row->staffID;?></td>
                        <td>
                        <?php
                            $filename = site_url().'assets/staff/'.$row->staffID.'.jpg';
                            if (false!==file($filename)) {
                                echo '<img src="'.$filename.' " class="portrait" style="width:50px">';
                            } else {
                                echo '<img src="'.base_url().'./assets/staff/default-user.png" class="portrait" style="width:50px">';   
                            }
                        ?>                        
                        <?php echo $row->name; ?> 
                        <?php if ($row->staffNickName != "") echo "(".$row->staffNickName.")"; ?></td>
                      </tr>
                  <?php  } }else{
                    echo '<tr><td colspan="5" style="text-align:center">ไม่พบข้อมูล</td></tr>';
                  }?>
                </table>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <div class="panel-body" style="text-align:center;"> 
                <div class="xcrud-list-container">
                <h3 style="float: left;">พนักงานที่มีวันเกิดในช่วง 5 วันนี้</h3>
                <table class="xcrud-list table table-striped table-hover table-bordered">  
                  <tr>
                    <th>พนักงาน</th>
                    <th>แผนก</th>
                    <th>วันเกิด</th>
                  </tr>
                  <?php 
                  if($rBirth != NULL){
                    foreach($rBirth->result() as  $row){ 
                      ?>
                      <tr> 
                        <td>
                        <?php
                            $filename = site_url().'assets/staff/'.$row->staffID.'.jpg';
                            if (false!==file($filename)) {
                                echo '<img src="'.$filename.' " class="portrait" style="width:50px">';
                            } else {
                                echo '<img src="'.base_url().'./assets/staff/default-user.png" class="portrait" style="width:50px">';   
                            }
                        ?>                                                
                        <?php echo $row->name; ?>
                        <?php if ($row->staffNickName != "") echo "(".$row->staffNickName.")"; ?></td>                        
                        </td>
                        <td><?php echo getOrgByID($row->orgID); ?></td>
                        <td><?php echo toFullDate($row->staffBirthday,'th','full'); ?></td>
                      </tr>
                  <?php  } }else{
                    echo '<tr><td colspan="5" style="text-align:center">ไม่พบข้อมูล</td></tr>';
                  }?>
                </table>
                </div>
            </div>
        </section>
        CI_VERSION : <?php echo CI_VERSION;?>
    </div>
</div>