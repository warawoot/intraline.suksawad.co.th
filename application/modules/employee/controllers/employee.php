<?php

class Employee extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('Employee_model');		
		$this->load->model('absence/absence_model');		
	}
	
	public function index(){
		if($this->session->userdata('roleID')==3)
		{
			$data['rBirth'] = $this->Employee_model->getByBirthday();
			$data['rLeave'] = $this->Employee_model->getLeave(date("Y-m-d"),date("Y-m-d"));
 			$this->template->load('template/admin','adminview',$data);			
		}
		else
		{
			$data['ac1'] = $this->absence_model->getAbsenceQuota(1);
			$data['ac2'] = $this->absence_model->getAbsenceQuota(2);
			$data['ac7'] = $this->absence_model->getAbsenceQuota(7);
			$data['sa1'] = $this->absence_model->getStaffAbsence(date('Y'),$this->session->userdata('ID'),1);
			$data['sa2'] = $this->absence_model->getStaffAbsence(date('Y'),$this->session->userdata('ID'),2);
			$data['sa7'] = $this->absence_model->getStaffAbsence(date('Y'),$this->session->userdata('ID'),7);
			$data['sao'] = $this->absence_model->getStaffAbsence(date('Y'),$this->session->userdata('ID'));
			$data['abs'] = $this->absence_model->getStaffAbsenceData(date('Y'),'',$this->session->userdata('ID'));
 			$this->template->load('template/admin','userview',$data);						
		}
	}
}
/* End of file dashboard.php */
/* Location: ./application/module/dashboard/dashboard.php */