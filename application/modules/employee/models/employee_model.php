<?php
class Employee_model extends MY_Model{

    function __construct() {    	
        parent::__construct();
        $this->_table = 'tbl_user';
        $this->_pk = 'userID';
    }

    public function authPassword($username,$password){
        $r = $this->db->get_where($this->_table,array('userName'=>$username,'userCredential'=>md5($password)));
        return ($r->num_rows() > 0) ? $r : NULL;
    }

    function getUserData($userID){
        $query = $this->db->query("
            SELECT 
                CONCAT(t1.staffPreName ,t1.staffFName ,'  ',t1.staffLName) as name  , 
                t1.staffImage as image                
            FROM `tbl_staff` t1 
            WHERE t1.staffID  = $userID 
            LIMIT 1 
        ");
        return $query->row_array();  
    } 

    function getLeave($startDate, $endDate){
        $sql = "SELECT 
                  s.staffID,  
                  CONCAT( s.staffPreName,s.staffFName ,'  ',s.staffLName) as name ,
                  s.staffNickName
            FROM tbl_staff s  
            WHERE s.status != 2
            ORDER BY staffID ASC
                 ";
        echo "<!--".$sql."-->"; 
        $r = $this->db->query($sql);
        return ($r->num_rows() > 0) ? $r : NULL;
        //return NULL;
    } 
		
	function getByBirthday(){
        // $year -= 543;
        $day = date("d");
        $preday = $day + 5; 
        //pre birthday 5 day
        $month = date("m");
            $sql = "SELECT
                    s.staffID,  
                    s.orgID,
                    CONCAT(s.staffPreName ,s.staffFName ,'  ',s.staffLName) as name  , 
                    s.staffNickName ,
                    s.staffBirthday                              
                FROM tbl_staff s 
                WHERE s.status <> 2 ";
        if(!empty($day)){
            $sql.="AND DAY(staffBirthday) <= '$preday' AND DAY(staffBirthday) >= '$day'";
        }
        if(!empty($month)){
                $sql.="AND MONTH(staffBirthday) = '$month' ";
        }
        /*
        if(!empty($year)){
            $sql.=" YEAR(staffBirthday) = '$year' AND ";
        }
        */
        $sql.= " ORDER BY DAY(staffBirthday),YEAR(staffBirthday)  ASC";
        $sql = substr($sql,0,-4);
        //echo "<pre>";
        //print_r($sql);
        $r = $this->db->query($sql);
        //echo $sql;
        return ($r->num_rows() > 0) ? $r : NULL;
    }
            
}
/* End of file user_model.php */
/* Location: ./application/module/user/models/user_model.php */