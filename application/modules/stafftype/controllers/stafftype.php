<?php

class stafftype extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		
		// get id staff //
		


		$xcrud->table('tbl_staff_type');
		

		
		//// List /////
		$col_name = array(
			'typeName'=>'ประเภทพนักงาน'
		);

		
		
		
		$xcrud->columns('typeName');
		$xcrud->label($col_name);
		

		//// Form //////

		$xcrud->fields('typeName');


		$xcrud->validation_required('typeName');

		
		
		
		
		$data['html'] = $xcrud->render();
		$data['title'] = "ประเภทพนักงาน";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file staffseminar.php */
/* Location: ./application/module/stafftype/stafftype.php */