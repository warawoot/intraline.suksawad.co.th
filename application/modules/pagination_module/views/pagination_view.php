<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/pagination_module.css" type="text/css">
<style>
.img_prv_nxt{
  padding:5px;
}
</style>
<div class="clear"></div>
<div id="paging_button">
    <?php
         $pagenext  = 10;     //จำนวนหน้าที่จะแสดง
         $start = 1;
         if ($page<=1){
            $page=1;
        } 
         //กำหนดค่าในการแบ่งตัวเลขหน้า
        //$start = $page-$pagenext;    //กำหนดค่าในการแบ่งตัวเลขหน้า
        $start = ceil($page / 10)-1;
        if($start<1){
            $start=1;
        }else{
            $start = ($start * $pagenext) +1;
        }

        $end = $start+9;    //กำหนดค่าในการแบ่งตัวเลขหน้า
        if($end>=$numpage){
            $end=$numpage;
        } 

        if($start>$pagenext){    //ถ้าจำนวนหน้าเกินที่กำหนดใน $pagenext ให้โชว์ "..."
            if($start > 10)
                $back= (floor($start/10))*10;
            else
                $back = 1; 

            //if($mode == 'view'){ 
            //        echo '<div class="img_prv_nxt">'.image_asset('icon/paging_first.png').'</div><div class="img_prv_nxt">'.image_asset('icon/paging_prv.png').'</div>'; 
            //}else{
            ?>
            <div class="img_prv_nxt">
              <a href="javascript:getPaging(1)" ><i class="fa fa-backward"></i></a>
            </div>
            <div class="img_prv_nxt">
              <a href="javascript:getPaging(<?php echo $back; ?>)" ><i class="fa fa-chevron-left"></i></a>
            </div>
    <?php    //echo "<li>...</li>";
            //} 
          }

         for($i=$start;$i<=$end;$i++){    //ใช้ for loop เพื่อโชว์เลขหน้า
            if($i==$page){                
               echo "<div id='page_selected'>$i</div> ";
            }else{   //และเมื่อ $i ค่าไม่เท่ากับ $page ก็ให้โชว์ตัวเลข และสามารถกด link ได้  
                //if($mode == 'view'){
                //    echo '<div id="bgnexttwo">'.$i.'</div>';
                //}else{
                ?>                                    
                <div id='bgnexttwo'><a href="javascript:getPaging(<?php echo $i; ?>)" ><?php echo $i; ?></a></div>
     <?php  } //}
        } 

        if($end<$numpage){    //ถ้า $end มีค่าน้อยกว่า $numpage จะโชว์ "..." 
            //echo "<div>...</div> ";
         }

         if($page<$numpage){    //เช็คว่าถ้าอยู่หน้าที่น้อยกว่า 1 ให้แสดงเครื่องหมาย >> และ > เพื่อเลื่อนไปข้างหน้า
            $next=$end+1; 
            if($next > $numpage){
                $next = $numpage; 
            }else{
               // if($mode == 'view'){
               //     echo '<div>Next</div><div>Last</div>';
               // }else{
            ?>   
            <div class="img_prv_nxt">
              <a href="javascript:getPaging(<?php echo $next; ?>)" ><i class="fa fa-chevron-right"></i></a>
            </div> 
            <div class="img_prv_nxt">
              <a href="javascript:getPaging(<?php echo $numpage; ?>)" ><i class="fa fa-forward"></i></a>
            </div>
    <?php } } //}    ?>
    
</div>