<?php

class Pagination_module extends MX_Controller {
	
	function __construct() {
            parent::__construct();
		
	}
	
        public function index($pagesize = 12,$page = 1,$count = 1,$module='19'){
            
            $data['pagesize'] = $pagesize;
            $data['page'] = $page;
            $data['numpage'] = ceil($count/$pagesize); 
            $data['module'] = $module;
            $data['mode'] = 'view';
            return $this->load->view("pagination_module/pagination_view",$data,true);
        }
        
	

}
/* End of file pagination_module.php */
/* Location: ./application/module/pagination_module/pagination_module.php */