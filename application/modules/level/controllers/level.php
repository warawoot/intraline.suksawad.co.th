<?php

class Level extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('level_model');
    }
 

    public function index() {
 		 
        $max 	= $this->level_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
		}else{
 			$max_id  =  $max+1;
 		}
		
		$distinct_id	= $this->level_model->distinct_level($max_id); 
		
		if(is_array($distinct_id)){
			redirect(site_url().'level', 'refresh');
			exit();
		}
 	 
	 	Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/editors/ckeditor/ckeditor.js'; 
 		$xcrud	=	xcrud_get_instance(); 
        $xcrud->table('tbl_emp_level'); 
        $arr			=	array("emp_level_name"=>"ชื่อระดับ","emp_level_desc"=>"มาตราฐานหน้าที่","emp_level_knowledge"=>"มาตราฐานความรู้" ,"salary"=>"เงินเดือน" ,"workingLife"=>"อายุงาน" ,"levelPosition"=>"การเลื่อนตำแหน่ง" , );
 		
 		$xcrud->change_type('emp_level_desc','textarea' );
		$xcrud->change_type('emp_level_knowledge','textarea' );
		$xcrud->change_type('levelPosition','textarea' );
		// -- validation_required -- 
		$xcrud->validation_required('emp_level_name') ;
 		$xcrud->pass_var('emp_level_id',$max_id,'create');
 		 
        $xcrud->label($arr);
        $xcrud->columns($arr);//แสดงในตางราง $xcrud->columns('userName');  
   		 
		//$xcrud->before_remove('delete_user_data'); 
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
		$data['id']	=   '';
        $this->template->load('template/admin', 'level',$data);
    }
     
   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>