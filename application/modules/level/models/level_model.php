<?php

class Level_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="tbl_emp_level";
        $this->pk = "emp_level_id";
	}
        
    function max_id(){
	    $this->db->select_max('emp_level_id');
		$this->db->from('tbl_emp_level');
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->emp_level_id; 
 		}else{
			$data 		=  '1'; //  hot เท่ากับ login แต่ status ไม่เท่า 2
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
   function max_data($emp_level_id){
	    $this->db->select('*');
		$this->db->from('`tbl_emp_level`');
		$this->db->where('`emp_level_id`' ,$emp_level_id);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
	function distinct_level($emp_level_id){
		$this->db->select('*');
		$this->db->from('`tbl_emp_level`');
		$this->db->where('`emp_level_id`' ,$emp_level_id);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
 		return ($query->num_rows() > 0) ? $data : NULL;
    }
	
}
/* End of file level_model.php */
/* Location: ./application/module/hospital/level.php */