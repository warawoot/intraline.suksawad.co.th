<?php

class Workplace_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="tbl_work_place";
        $this->pk = "workPlaceID";
	}
        
    function max_id(){
	    $this->db->select_max('workPlaceID');
		$this->db->from('tbl_work_place');
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->workPlaceID; 
 		}else{
			$data 		=  '1'; //  hot เท่ากับ login แต่ status ไม่เท่า 2
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
   function max_data($workPlaceID){
	    $this->db->select('*');
		$this->db->from('`tbl_work_place`');
		$this->db->where('`workPlaceID`' ,$workPlaceID);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        //print_r($data);
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
} 