<?php

class Reportturnover extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('reportturnover_model');
		$this->load->model('organization/organization_model');
		
	}
	
	public function index(){

		//$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$month = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$year = $this->input->get('year')==null ? date("Y") : $this->input->get('year')-543 ;
		
		$i=0;
		foreach ($this->reportturnover_model->getOrg(2) as $key => $item) {
			$i++;
			
			/*
			$GenderM = $this->CountGender('m',$item->orgID);
			$GenderF = $this->CountGender('f',$item->orgID);
			$M = $this->CountRank('2',$item->orgID);
			$S = $this->CountRank('1',$item->orgID);
			$O = $this->CountRank('0',$item->orgID);
			$datastaff['GenderM'] = $GenderM[0]->gender;
			$datastaff['GenderF'] = $GenderF[0]->gender;
			$datastaff['M'] = $M[0]->Rank;
			$datastaff['S'] = $S[0]->Rank;
			$datastaff['O'] = $O[0]->Rank;
			*/
			$orgID = $item->orgID;
			$datastaff['orgID'] = $orgID;
			$datastaff['orgName'] = $item->orgName;
			$subOrg = $item->orgID.$this->organization_model->getSubOrg($item->orgID);
			$staff = $this->reportturnover_model->getStaff($subOrg);
			//echo $this->db->last_query().'<br>';
			$datastaff['staffCurrent'] = $staff->num_rows();
			$datastaff['M']=0;
			$datastaff['S']=0;
			$datastaff['O']=0;
			$datastaff['GenderM']=0;
			$datastaff['GenderF']=0;
			foreach ($staff->result() as $row) {
				$rankID = $row->rankID;
				if($rankID=='2'){
					$datastaff['M']++;
				}else if($rankID=='1'){
					$datastaff['S']++;
				}else if($rankID=='0'){
					$datastaff['O']++;
				}


				$staffGender = $row->staffGender;
				if($staffGender=='m'){
					$datastaff['GenderM']++;
				}else if($staffGender=='f'){
					$datastaff['GenderF']++;
				}
				
			}

			$turnIn = $this->reportturnover_model->turnIn($month,$year,$subOrg);
			$turnOut = $this->reportturnover_model->turnOut($month,$year,$subOrg);
			$datastaff['turnIn'] = $turnIn;
			$datastaff['turnOut'] = $turnOut;
			//$datastaff['turnOverRate'] = ($turnIn / $turnOut) * 100;
			$datastaff['turnOverRate'] = intval(($turnOut * 100) / $turnIn);

			$rs[] = $datastaff;
			
			foreach ($this->reportturnover_model->getOrg($orgID,$orgID) as $key => $item) {
				//echo  $orgID.'<br>';
				//echo $item->orgID.'<br>';
				$datastaff['orgName'] = $item->orgName;
				$datastaff['orgID'] = $item->orgID;
				$datastaff['upperOrgID'] = $item->upperOrgID;
				if($item->orgID==$orgID){
					$orgID = $orgID;
				}else{
					$orgID = $item->orgID.$this->organization_model->getSubOrg($item->orgID);
				}
	
				$staff = $this->reportturnover_model->getStaff($orgID);
				$datastaff['staffCurrent'] = $staff->num_rows();
				//echo $this->db->last_query().'<br>';
				$datastaff['M']=0;
				$datastaff['S']=0;
				$datastaff['O']=0;
				$datastaff['GenderM']=0;
				$datastaff['GenderF']=0;
				foreach ($staff->result() as $row) {
					$rankID = $row->rankID;
					if($rankID=='2'){
						$datastaff['M']++;
					}else if($rankID=='1'){
						$datastaff['S']++;
					}else if($rankID=='0'){
						$datastaff['O']++;
					}


					$staffGender = $row->staffGender;
					if($staffGender=='m'){
						$datastaff['GenderM']++;
					}else if($staffGender=='f'){
						$datastaff['GenderF']++;
					}
					
				}

				$turnIn = $this->reportturnover_model->turnIn($month,$year,$orgID);
				$turnOut = $this->reportturnover_model->turnOut($month,$year,$orgID);
				$datastaff['turnIn'] = $turnIn;
				$datastaff['turnOut'] = $turnOut;
				//$datastaff['turnOverRate'] = ($turnIn / $turnOut) * 100;
				$datastaff['turnOverRate'] = intval(($turnOut * 100) / $turnIn);


				$rs2[] = $datastaff;

			}


		}

		//echo "<pre>";
		//print_r($rs);
		/*$staffGender = array();
		foreach ($this->reportturnover_model->getOrg(2) as $key => $item) {
			$datastaff['orgName'] = $item->orgName;
			$datastaff['GenderM'] = $this->CountGender('m',$item->orgID);
			$datastaff['genderF'] = $this->CountGender('f',$item->orgID);
			$datas[] = $datastaff;
		}

		echo $this->CountGender('m','100');
		echo "<pre>";*/
		//print_r($datas);
		//$data['r'] =$r;
		//print_r($data['r']);

		$data['r'] = $rs;
		//$rs2 = array_push($rs2,$rsManager);
		//echo "<pre>";
		//print_r($rs2);
		//print_r($rsManager);
		$data['month'] = $month;
		$data['r2'] = $rs2;
		$data['r3'] = $rs3;
		$data['rManager'] = $rsManager;
		$data['title'] = "รายงานอัตรากำลัง เดือน ";
        $this->template->load("template/admin",'reportturnover',$data);

	}

/*
	public function CountGender($gender,$orgID){
		$sql = "select COUNT(staffGender ) as gender from tbl_staff where orgID in (select orgID from tbl_org_chart where upperOrgID='$orgID' or orgID='$orgID') and staffGender ='$gender' ";
		return $this->db->query($sql)->result();
	}

	public function CountRank($RankID,$orgID){
		$sql = "select COUNT(*) as Rank from tbl_staff where orgID in (select orgID from tbl_org_chart where upperOrgID='$orgID' or orgID='$orgID') and RankID ='$RankID' ";
		return $this->db->query($sql)->result();
	}
*/













}
/* End of file reportbirthday.php */
/* Location: ./application/module/reportbirthday/reportbirthday.php */