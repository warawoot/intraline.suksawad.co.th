<style type="text/css">
	.table th{
		color: #fff;
		text-align: center;
	}
</style>
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --',
		'01'=>'มกราคม',
		'02'=>'กุมภาพันธ์',
		'03'=>'มีนาคม',
		'04'=>'เมษายน',
		'05'=>'พฤษภาคม',
		'06'=>'มิถุนายน',
		'07'=>'กรกฎาคม',
		'08'=>'สิงหาคม',
		'09'=>'กันยายน',
		'10'=>'ตุลาคม',
		'11'=>'พฤศจิกายน',
		'12'=>'ธันวาคม');
   // $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  

	$current_year = date('Y')+543;
	$year_a = $current_year - 10;
	if(empty($this->input->get('year'))){
		$getyear = $current_year;
	}else{
		$getyear = $this->input->get('year');
	}

?>
	



	

	

	
	

<div class="row">
<div class="container">
	<div class="col-sm-12">
		<h5 class="text-center" ><?php echo $title.$th_month[$month].' '.$getyear;?></h5>
		<hr style="padding-bottom: 20px;">
	</div>
	<div class="col-sm-12"  style="padding: 0px;">
	<form id="formMain" action="<?php echo site_url();?>reportturnover" method="get"  enctype="multipart/form-data">
		<div class="col-sm-2" style="padding: 0px;">
		
		<select class="form-control"  id="month" name="month">
			<?php foreach($th_month as $key=>$val){ ?>
			<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
			<?php } ?>
			
		</select>
		</div>
		<div class="col-sm-2"  style="padding: 0px;">
		<div class="input-group">
			<select class="form-control"  id="year" name="year">
				<?php 
				
				for($year=$year_a;$year<=$current_year;$year++){ ?>
				<option value="<?php echo $year; ?>" <?php echo ($getyear == $year) ? "selected" : ""; ?>><?php echo $year; ?></option>
				<?php } ?>
				
			</select>
			<div class="input-group-btn">
				<button type="submit" style="line-height: 1.0;" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> ค้นหา</button>
			</div>
		</div>
		</div>
		<div class="col-sm-8"  style="padding: 0px;">
    	<!--<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>-->
    	</div>
		<!--<div class="col-sm-6"  style="padding: 0px;">
       
        		<button type="submit" class="btn btn-warning"><i class="fa fa-search"></i> ค้นหา</button>
        </div>-->
      
    </form>
    </div>
    
</div>
</div>

	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="2">ผังโครงสร้างการบริหาร</th>
			<th colspan="3" rowspan="2" >อัตรากำลัง</th>
			<th colspan="11">อัตรากำลังปัจจุบัน</th>	
		</tr>
			<th colspan="3">ระดับตำแหน่ง</th>
			<th colspan="2">ประเภทการจ้าง</th>
			<th colspan="2">เพศ</th>
			<th colspan="3">Turnover rate</th>
			<th>รอสรรหา</th>
		<tr>
			<th>update</th>
			<th>มาตรฐาน</th>
			<th>ปัจจุบัน</th>
			<th>+-</th>
			<th>M</th>
			<th>S</th>
			<th>O</th>
			<th>รายเดือน</th>
			<th>รายวัน</th>
			<th>ชาย</th>
			<th>หญิง</th>
			<th>เข้า</th>
			<th>ออก</th>
			<th>%</th>
			<th>ว่าง</th>
		</tr>

		
		<?php 
		$i=0;
		
		if($r != NULL){
			foreach($r as  $row){ 
					$i++;

					
				?>
				<tr>
			<td class="bg-success"><?=$row['orgName']?></td>
			<td class="bg-success"></td>
			<td class="bg-success"><?=$row['staffCurrent'];?></td>
			<td class="bg-success"></td>
			<td class="bg-success"><?=$row['M'];?></td>
			<td class="bg-success"><?=$row['S'];?></td>
			<td class="bg-success"><?=$row['O'];?></td>
			<td class="bg-success"></td>
			<td class="bg-success"></td>
			<td class="bg-success"><?=$row['GenderM'];?></td>
			<td class="bg-success"><?=$row['GenderF'];?></td>
			<td class="bg-success"><?=$row['turnIn'];?></td>
			<td class="bg-success"><?=$row['turnOut'];?></td>
			<td class="bg-success"><?=$row['turnOverRate'];?></td>
			<td class="bg-success"></td>
		</tr>
		<?php

			foreach($r2 as  $row2){ 
				//echo $row2['orgName'].'<br>';
				if($row['orgID']==$row2['upperOrgID'] or $row['orgID']==$row2['orgID']){
					$orgName = $row['orgID']==$row2['orgID'] ? 'ผู้จัดการ/ผู้ช่วยผู้จัดการ' : $row2['orgName'];

					
				?>
				<tr>
					<td class=""> -- <?=$orgName?></td>
					<td class=""></td>
					<td class=""><?=$row2['staffCurrent'];?></td>
					<td class=""></td>
					<td class=""><?=$row2['M'];?></td>
					<td class=""><?=$row2['S'];?></td>
					<td class=""><?=$row2['O'];?></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""><?=$row2['GenderM'];?></td>
					<td class=""><?=$row2['GenderF'];?></td>
					<td class=""><?=$row2['turnIn'];?></td>
					<td class=""><?=$row2['turnOut'];?></td>
					<td class=""><?=$row2['turnOverRate'];?></td>
					<td class=""></td>
				</tr>

				<?php

					/*foreach($r3 as  $row3){ 
					if($row2['orgID']==$row3['upperOrgID']){

						
					?>
					<tr>
						<td class=""> ---- <?=$row3['orgName']?></td>
						<td><?=$row3['orgID'];?></td>
						<td></td>
						<td></td>
						<td><?=$row3['M'];?></td>
						<td><?=$row3['S'];?></td>
						<td><?=$row3['O'];?></td>
						<td></td>
						<td></td>
						<td><?=$row3['GenderM'];?></td>
						<td><?=$row3['GenderF'];?></td>
						<td>เข้า</td>
						<td>ออก</td>
						<td>%</td>
						<td></td>
					</tr>



		<?php 
					}
					}*/
				}
			}
		} 
	}else{
		
			echo '<tr><td colspan="15" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>



<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function search(){
        	window.location='<?php echo site_url();?>reportturnover?&month='+$('#month :selected').val();
        }


        $('#month').change(function(){
        	day_arr = ["","31","29","31","30","31","30","31","31","30","31","30","31"];  
   			day = $('#day :selected').val();
        	month = $('#month :selected').val();
        	count = day_arr[parseInt(month)];
        	sel = (day == "") ? 'selected' : '';
        	html = "<option value='' "+sel+">-- กรุณาเลือก --</option>";

			for(i=1; i<=count; i++){ 

				sel = (day == i) ? 'selected' : '';
				html+='<option value="'+i+'" '+sel+'>'+i+'</option>';
			 } 
			 $("#day").html(html);
        })
        

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportturnover/print_pdf?month='+$('#month :selected').val()+'&orgID='+$('#orgID :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportturnover/print_excel?month='+$('#month :selected').val()+'&orgID='+$('#orgID :selected').val());
        }
   
</script>