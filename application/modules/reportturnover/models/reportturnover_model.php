<?php
class Reportturnover_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = '';
            $this->_pk = '';
        }

    



        public function getOrg($orgID='',$andID=''){            
                $sql = "SELECT orgID, orgName,upperOrgID
                         FROM tbl_org_chart";
                $sql.= " WHERE upperOrgID='$orgID'";
                if(!empty($andID)){
                    $sql .=" or orgID = '$andID'";
                }

                       
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r->result() : NULL;     
        }

        public function getStaff($orgID){
            //echo $sql = "select * from tbl_staff where orgID in ($orgID) or orgID in (select orgID from tbl_org_chart where upperOrgID='$orgID')";
            //$sql = "select * from tbl_staff as a left join tbl_org_chart as b on a.orgID=b.orgID where a.orgID in ($orgID)";
             $sql = "select * from tbl_staff where orgID in ($orgID) and status <> '2'";
            return $this->db->query($sql);
        }


        public function turnIn($month, $year, $orgID){
            /*$query =  $this->db->select('staffID')
                            ->from('tbl_staff')
                            ->where_in('orgID',$orgID)
                            ->where('MONTH(dateIn)',$month)
                            ->where('YEAR(dateIn)',$year)
                            ->get();*/
            $sql = "select staffID from tbl_staff where orgID in($orgID) and MONTH(dateIn)='$month' and YEAR(dateIn)='$year' and status <> '2'";
            $query = $this->db->query($sql);
            //echo $this->db->last_query().'<br>';
            return $query->num_rows;
            
        }


        public function turnOut($month, $year, $orgID){
            $sql = "select staffID from tbl_staff where orgID in($orgID) and MONTH(dateOut)='$month' and YEAR(dateOut)='$year' and status='2'";
            $query = $this->db->query($sql);
            return $query->num_rows;
        }





        

}

/* End of file staff_model.php */
/* Location: ./application/module/Reportstatusemployees/models/staff_model.php */
?>