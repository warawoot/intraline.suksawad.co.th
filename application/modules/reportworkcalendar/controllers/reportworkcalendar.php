<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReportWorkCalendar extends MX_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model('ReportWorkCalendarmodel');       
        $this->load->model('rate/rate_model');       
     	$this->load->model('organization/organization_model');        
    }

    function index() {               
		$agencies = $this->input->get_post('agencies');
        $data['dropdown_org_chart'] = ""; 

        if($this->session->userdata('roleID') == 3) {
		    $data['dropdown_org_chart'] = $this->organization_model->getDropdownTree(0 , $prefix = '' , $type='sub' ,$agencies);#Dropdown                  
        } else {
            $rows = $this->organization_model->getOrgByManager($this->session->userdata('userID'));

            foreach($rows as $row) {
		        $data['dropdown_org_chart'] .= $this->organization_model->getDropdownTree($row->orgID , $prefix = '' , $type='' ,$agencies);#Dropdown                                  
            }
        }

        if($agencies != "")
        {
            $ag = $agencies.$this->organization_model->getSubOrg($agencies);
            $data['reportO'] = $this->ReportAppraisalmodel->get_reportAppraisal($ag,'0',1); 
            $data['reportS'] = $this->ReportAppraisalmodel->get_reportAppraisal($ag,'1',2); 
            echo "<!--".$this->db->last_query()."-->";
            $data['reportM'] = $this->ReportAppraisalmodel->get_reportAppraisal($ag,'2',2); 
        }

        $this->template->load('template/admin', 'reportworkcalendar',$data);
    }   
  
    function updateScore() {
        $A1 = 0;
        $A2 = 0;
        $A3 = 0;
        $C1 = 0;
        $C2 = 0;
        foreach($this->input->get_post('M_staffID') as  $key => $value) {
            $A1 = $this->input->get_post('M_A1_'.$value);
            $A2 = $this->input->get_post('M_A2_'.$value);
            $A3 = $this->input->get_post('M_A3_'.$value);
            $A4 = $this->input->get_post('M_A4_'.$value);
            $C1 = $this->input->get_post('M_C1_'.$value);
            $C2 = 0;

            $inputArray  = 	array(
                            'evalYear'  => 2560,
                            'evalRound' => 1,
                            'evalStep'  => 1,
                            'rankID'    => 2,
                            'staffID'   => $value,
                            'approverID'=> $this->session->userdata('userID'),
                            'A1'        => $A1,
                            'A2'        => $A2,
                            'A3'        => $A3,
                            'A4'        => $A4,
                            'C1'        => $C1,
                            'C2'        => $C2
                    );
            $this->rate_model->insert_summary_eval($inputArray);

        }

        foreach($this->input->get_post('S_staffID') as  $key => $value) {
            $A1 = $this->input->get_post('S_A1_'.$value);
            $A2 = $this->input->get_post('S_A2_'.$value);
            $A3 = $this->input->get_post('S_A3_'.$value);
            $A4 = $this->input->get_post('S_A4_'.$value);
            $C1 = $this->input->get_post('S_C1_'.$value);
            $C2 = $this->input->get_post('S_C2_'.$value);

            $inputArray  = 	array(
                            'evalYear'  => 2560,
                            'evalRound' => 1,
                            'evalStep'  => 1,
                            'rankID'    => 1,
                            'staffID'   => $value,
                            'approverID'=> $this->session->userdata('userID'),
                            'A1'        => $A1,
                            'A2'        => $A2,
                            'A3'        => $A3,
                            'A4'        => $A4,
                            'C1'        => $C1,
                            'C2'        => $C2
                    );
            $this->rate_model->insert_summary_eval($inputArray);

        }

        foreach($this->input->get_post('O_staffID') as  $key => $value) {
            $A1 = $this->input->get_post('O_A1_'.$value);
            $A2 = $this->input->get_post('O_A2_'.$value);
            $A3 = $this->input->get_post('O_A3_'.$value);
            $A4 = 0;
            $C1 = $this->input->get_post('O_C1_'.$value);
            $C2 = $this->input->get_post('O_C2_'.$value);

            $inputArray  = 	array(
                            'evalYear'  => 2560,
                            'evalRound' => 1,
                            'evalStep'  => 1,
                            'rankID'    => 0,
                            'staffID'   => $value,
                            'approverID'=> $this->session->userdata('userID'),
                            'A1'        => $A1,
                            'A2'        => $A2,
                            'A3'        => $A3,
                            'A4'        => $A4,
                            'C1'        => $C1,
                            'C2'        => $C2
                    );
            $this->rate_model->insert_summary_eval($inputArray);

        }

        echo "<script>alert('Complete');document.location='".base_url()."reportworkcalendar?agencies=".$this->input->get_post('agencies')."';</script>";

    }

}