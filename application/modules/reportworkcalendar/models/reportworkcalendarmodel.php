<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReportWorkCalendarmodel extends MY_Model {

    function __construct() {    	
        parent::__construct();     
        $this->table="";
        $this->pk = "";      
    }        

    function get_reportAppraisal($agencies,$rankID,$evalType) {        
        $sql = "SELECT 
                S.staffID,
                S.staffFName,
                S.staffLName,
                S.orgID, 
                o.orgName, 
                Love.sum as Love,                   
                Competency.sum as Competency,          
                CompetencyM.sum as CompetencyM,          
                KPI.sum as KPI,
                e.A1 as A_Love,                   
                e.A2 as A_KPI,                  
                e.A3 as A_Competency,          
                e.A4 as A_CompetencyM,
                0 as C1,
                0 as C2,
                e.C1 as A_C1,
                e.C2 as A_C2                           
            FROM 
                tbl_staff S 
            LEFT JOIN 
                tbl_eval_summary e ON s.staffID = e.staffID 
            INNER JOIN 
                tbl_org_chart o ON s.orgID = o.orgID 
                LEFT JOIN 
                (
                SELECT 
                    staffID,    
                    SUM(evalScore) / 2 AS sum,
                    evalFormID 
                FROM tbl_staff 

                LEFT JOIN tbl_eval ON tbl_staff.staffID = tbl_eval.empID  
                WHERE evalFormID=1
                GROUP BY empID
                ) AS Love ON S.staffID = Love.staffID                 

                LEFT JOIN
                (
                SELECT 
                    staffID,   
                    (SUM(evalScore) / SUM(evalVariable)) * 100 AS sum,
                    evalFormID 
                FROM tbl_staff 
                LEFT JOIN tbl_eval ON tbl_staff.staffID = tbl_eval.empID  
                WHERE evalFormID=2 AND evalType=1
                GROUP BY empID
                ) AS Competency ON S.staffID = Competency.staffID 

                LEFT JOIN
                (
                SELECT 
                    staffID,   
                    (SUM(evalScore) / SUM(evalVariable)) * 100 AS sum,
                    evalFormID 
                FROM tbl_staff 
                LEFT JOIN tbl_eval ON tbl_staff.staffID = tbl_eval.empID  
                WHERE evalFormID=2 AND evalType=2
                GROUP BY empID
                ) AS CompetencyM ON S.staffID = CompetencyM.staffID  

                LEFT JOIN
                (
                SELECT 
                    staffID,   
                    (SUM(evalScore * evalVariable) / (SUM(evalVariable)*5)) * 100 AS sum,
                    evalFormID 
                FROM tbl_staff 
                LEFT JOIN tbl_eval ON tbl_staff.staffID = tbl_eval.empID
                    WHERE evalFormID=4
                GROUP BY empID
                ) AS KPI ON S.staffID = KPI.staffID                   
            WHERE  
                s.rankID IN (".$rankID.") ";

        if($agencies != null)
        {
            $sql .= " AND S.OrgID IN (".$agencies.")";
        }

        $sql .= " ORDER BY s.orgID ASC, s.staffID ASC";
        $query = $this->db->query($sql);
        if($query->num_rows())
        {
            return $query->result();            
        }
        else {
            return null;
        }
    }

    function getOrgName() {
        $sqlOrg = "SELECT orgName,OrgID FROM tbl_org_chart";
        $orgName = $this->db->query($sqlOrg);
        return $orgName->result();  
    }   

}