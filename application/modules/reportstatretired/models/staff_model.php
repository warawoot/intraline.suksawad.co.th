<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

        public function getAllOrg(){

            $assignID = getAssignLasted();

            $sql = "SELECT * FROM tbl_org_chart 
                    where (upperOrgID = '0' OR upperOrgID = '1')
                    AND assignID = $assignID";
            $r = $this->db->query($sql);
            return $r->result();

        }

        public function getStaffByOrg($org){

            if(!empty($org)){
                $sql = "SELECT  s.staffGender,s.staffBirthday
                        FROM tbl_staff_work c 
                        JOIN tbl_staff s ON c.staffID = s.ID
                        WHERE c.workID = (SELECT max(workID) from tbl_staff_work WHERE staffID=c.staffID) AND c.orgID = $org";
                $r = $this->db->query($sql);
                return $r->result();
            }   

        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/reg/models/staff_model.php */