<?php

class Reportstatretired extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		

		$data['year'] = !empty($_GET['year']) ?  $this->input->get('year') : (date("Y")+542);
		$data['year_end'] = !empty($_GET['year_end']) ?  $this->input->get('year_end') : (date("Y")+543);

		$data['r'] = $this->staff_model->getAllOrg();
		
		$data['title'] = "รายงานเกษีณอายุแต่ละปี";

        $this->template->load("template/admin",'reportstatretired', $data);

	}

	public function print_pdf(){

		$data['year'] = !empty($_GET['year']) ?  $this->input->get('year') : (date("Y")+542);
		$data['year_end'] = !empty($_GET['year_end']) ?  $this->input->get('year_end') : (date("Y")+543);

		$data['r'] = $this->staff_model->getAllOrg();
		$data_r['html'] = $this->load->view('print',$data,true);

		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['year'] = !empty($_GET['year']) ?  $this->input->get('year') : (date("Y")+542);
		$data['year_end'] = !empty($_GET['year_end']) ?  $this->input->get('year_end') : (date("Y")+543);

		$data['r'] = $this->staff_model->getAllOrg();
		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportstatretired.php */
/* Location: ./application/module/reportstatretired/reportstatretired.php */