
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	
	<h5 style="text-align:center;">
		<br>
			รายละเอียดแสดงจำนวนพนักงานและลูกจ้างประจำเกษียณอายุในปีพ.ศ.<?php echo $year; ?> - <?php echo $year_end; ?>
		
	</h5><br>
	ค้นหา ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;ถึง ปี พ.ศ.
	<select style="width:80px" id="year_end">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year_end) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;

	<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>
	<br><br>

	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="3">ที่</th>
			<th rowspan="3">ฝ่าย / สำนัก</th>
			<th colspan="<?php echo (($year_end-$year+1)*3)+4;?>">จำนวนพนักงานเกษียณอายุในปี พ.ศ.<?php echo $year; ?> - <?php echo $year_end; ?></th>

		</tr>
		
		
		<tr >
			
			<td>ปี พ.ศ.</td>
			<?php for($i=$year; $i<=$year_end; $i++){ ?>
			<td colspan="3"><?php echo $i; ?></td>
			<?php } ?>
			<td colspan="3">รวม</td>
			
		</tr>
		<tr >
			
			<td>แยกเพศ</td>
			<?php for($i=$year; $i<=$year_end; $i++){ ?>
				<td>ชาย</td>
				<td>หญิง</td>
				<td>รวม</td>
			<?php } ?>
			<td>ชาย</td>
			<td>หญิง</td>
			<td>รวม</td>
			
		</tr>
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 

				
					$i++; 
					$sum['m'] = 0;
					$sum['f'] = 0;
					for($j=$year; $j<=$year_end; $j++){ 
						$arr_count[$j]['m']=0;
						$arr_count[$j]['f']=0;
					}
					
					$ro = $this->staff_model->getStaffByOrg($row->orgID);
					if(!empty($ro)){
						foreach($ro as $rowo){
							$dateexp = getExpiredDateByWork($rowo->staffBirthday,true);
							$list = explode("-",$dateexp);
							
							for($j=$year; $j<=$year_end; $j++){ 
								if($list[0] == $j-543){
									$arr_count[$j][$rowo->staffGender]+=1;
									$sum[$rowo->staffGender]	+= 1;
								}
							}
						}
						
					}
					?>
					<tr>
						<td><?php echo $i; ?></td>
						<td colspan="2" style="text-align:left;"><?php echo $row->orgName; ?></td>
						
						<?php for($j=$year; $j<=$year_end; $j++){ ?>
							<td><?php echo $arr_count[$j]['m']; ?></td>
							<td><?php echo $arr_count[$j]['f']; ?></td>
							<td><?php echo $arr_count[$j]['m']+$arr_count[$j]['f']; ?></td>
						<?php } ?>
						<td><?php echo $sum['m']; ?></td>
						<td><?php echo $sum['f']; ?></td>
						<td><?php echo $sum['m']+$sum['f']; ?></td>
						
					</tr>
				<?php //} ?>
				
		<?php }}

		if($i == 0)
			echo '<tr><td colspan="14">ไม่พบข้อมูล</td></tr>';
		 ?>
		
	</table>
	</div>


	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        /*$('#year').change(function(){
        	window.location='<?php echo site_url();?>reportretired?year='+$('#year :selected').val();
        })*/
		function search(){
			window.location='<?php echo site_url();?>reportstatretired?year='+$('#year :selected').val()+'&year_end='+$('#year_end :selected').val();
		}

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportstatretired/print_pdf?year='+$('#year :selected').val()+'&year_end='+$('#year_end :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportstatretired/print_excel?year='+$('#year :selected').val()+'&year_end='+$('#year_end :selected').val());
        }
</script>
