<?php  ?><?php echo $year; ?> - <?php echo $year_end; ?>
		
	</h4><br>
	
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th rowspan="3">ที่</th>
			<th rowspan="3">ฝ่าย / สำนัก</th>
			<th colspan="<?php echo (($year_end-$year+1)*3)+4;?>">จำนวนพนักงานเกษียณอายุในปี พ.ศ.<?php echo $year; ?> - <?php echo $year_end; ?></th>

		</tr>
		
		
		<tr >
			
			<td>ปี พ.ศ.</td>
			<?php for($i=$year; $i<=$year_end; $i++){ ?>
			<td colspan="3" style="text-align:center;"><?php echo $i; ?></td>
			<?php } ?>
			<td colspan="3" style="text-align:center;">รวม</td>
			
		</tr>
		<tr >
			
			<td>แยกเพศ</td>
			<?php for($i=$year; $i<=$year_end; $i++){ ?>
				<td style="text-align:center;">ชาย</td>
				<td style="text-align:center;">หญิง</td>
				<td style="text-align:center;">รวม</td>
			<?php } ?>
			<td style="text-align:center;">ชาย</td>
			<td style="text-align:center;">หญิง</td>
			<td style="text-align:center;">รวม</td>
			
		</tr>
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 

				
					$i++; 
					$sum['m'] = 0;
					$sum['f'] = 0;
					for($j=$year; $j<=$year_end; $j++){ 
						$arr_count[$j]['m']=0;
						$arr_count[$j]['f']=0;
					}
					
					$ro = $this->staff_model->getStaffByOrg($row->orgID);
					if(!empty($ro)){
						foreach($ro as $rowo){
							$dateexp = getExpiredDateByWork($rowo->staffBirthday,true);
							$list = explode("-",$dateexp);
							
							for($j=$year; $j<=$year_end; $j++){ 
								if($list[0] == $j-543){
									$arr_count[$j][$rowo->staffGender]+=1;
									$sum[$rowo->staffGender]	+= 1;
								}
							}
						}
						
					}
					?>
					<tr>
						<td><?php echo $i; ?></td>
						<td colspan="2" style="text-align:left;"><?php echo $row->orgName; ?></td>
						
						<?php for($j=$year; $j<=$year_end; $j++){ ?>
							<td style="text-align:center;"><?php echo $arr_count[$j]['m']; ?></td>
							<td style="text-align:center;"><?php echo $arr_count[$j]['f']; ?></td>
							<td style="text-align:center;"><?php echo $arr_count[$j]['m']+$arr_count[$j]['f']; ?></td>
						<?php } ?>
						<td style="text-align:center;"><?php echo $sum['m']; ?></td>
						<td style="text-align:center;"><?php echo $sum['f']; ?></td>
						<td style="text-align:center;"><?php echo $sum['m']+$sum['f']; ?></td>
						
					</tr>
				<?php //} ?>
				
		<?php }}

		if($i == 0)
			echo '<tr><td colspan="14">ไม่พบข้อมูล</td></tr>';
		 ?>
		
	</table>
	</div>

