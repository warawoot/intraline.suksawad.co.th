<?php

class Edufaculty extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		
		// select table//
		$xcrud->table('tbl_edu_faculty');
		
		
		//// List /////
		$col_name = array(
			'eduFactName' => 'คณะ / สายการเรียน'

		);

		$xcrud->columns('eduFactName');
		$xcrud->label($col_name);
		// End List//

		//// Form //////

		$xcrud->fields('eduFactName');

		$data['html'] = $xcrud->render();
		

		$data['title'] = "คณะ / สายการเรียน";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file edufaculty.php */
/* Location: ./application/module/edufaculty/edufaculty.php */