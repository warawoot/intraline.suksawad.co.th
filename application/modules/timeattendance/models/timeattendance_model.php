<?php

class timeattendance_model extends MY_Model {
    
	function __construct() {
		parent::__construct();
	}
    
	public function checkin($input){
		$data = array(
			'empID' 		=> $input["empID"],
			'checkInType' 	=> $input["checkInType"],
			'checkInTime' 	=> $input["checkInTime"],
			'inRange'		=> $input["inRange"]
		);
		$this->db->insert('tbl_timeattendance', $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function saveLineID($staffID, $lineID){
		if($staffID && $lineID) {
			$matchStaff = $this->lineExist($lineID);
			if(!$this->staffExist($staffID)) {
				return 'staff_not_found';
			} elseif($matchStaff != null) {
				return 'line_exist';
			} else {
				$sql = "UPDATE tbl_staff SET lineID='$lineID' WHERE staffID='$staffID'";
				$this->db->query($sql);     
				return 'success';
			}
		} else {
			return 'input_val_error';
		}
	}	

	public function saveLineID_email($emailValue, $lineID){
		if($emailValue && $lineID) {
			$matchStaff = $this->lineExist($lineID);
			if(!$this->emailExist($emailValue)) {
				return 'email_not_found';
			} elseif($matchStaff != null) {
				return 'line_exist';
			} else {
				$sql = "UPDATE tbl_staff SET lineID='$lineID' WHERE staffEmail='$emailValue'";
				$this->db->query($sql);     
				return 'success';
			}
		} else {
			return 'input_email_error';
		}
	}	

	public function staffExist($staffID){
		$this->db->select('COUNT(*)');
		$this->db->where('staffID', $staffID);
		$query=$this->db->get('tbl_staff');
		$result = $query->row_array();
		$count = $result['COUNT(*)'];
		return $count;
	}

	public function emailExist($emailValue){
		$this->db->select('COUNT(*)');
		$this->db->where('staffEmail', $emailValue);
		$query=$this->db->get('tbl_staff');
		$result = $query->row_array();
		$count = $result['COUNT(*)'];
		return $count;
	}

	public function lineExist($lineID){
		$this->db->select('ID,staffID,staffFName,staffLName,staffNickName');
		$this->db->where('lineID', $lineID);
		$query=$this->db->get('tbl_staff');
		$result = $query->row_array();
		return $result;
	}
}
/* End of file level_model.php */
/* Location: ./application/module/hospital/level.php */