<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <div style="text-align:center"> 
                <h3><?=$message_head?></h3>
            </div>
            <div class="panel-body" style="text-align:center">
                <h5 style="width:100%;text-align:left"><?=$message?></h5>
            </div>
        </section>
    </div>
</div>