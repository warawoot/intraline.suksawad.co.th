<?php

class Rate_tscore_calculate extends MY_Model {
   
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	} 
	
	function calculate_emp($OrgID,$date){ 
 
         $this->db->select('*');
         $this->db->from('`tbl_org_chart` as t1 ');
  		 $this->db->where('(t1.orgID = 82 and t1.assignID = 6)' );
         $query 		= $this->db->get();
 		 $data 		= $query->result(); 
 		 return ($query->num_rows() > 0) ? $data : NULL;
	}
 
	
	function calculate_emp3($OrgID,$assignID){ // ชื่อพนักงาน ฝ่าย 
		 
		$query = $this->db->query('
		 			SELECT * ,
					(
						SELECT  t3.ID FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as person_id,
(
						SELECT  CONCAT(t3.staffPreName,t3.staffFName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.'   limit 1
					) as person,
(
						SELECT  CONCAT(t3.staffLName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.'   limit 1
					) as personLName
FROM `tbl_org_chart` as t1 INNER JOIN tbl_staff_work as t5 ON t1.orgID = t5.orgID
WHERE (t1.orgID = '.$OrgID.' and t1.assignID = '.$assignID.'  ) 
						         '
				 );
        return $query->result(); 
		
		
	}
	
	function calculate_emp22($OrgID,$assignID,$empID){ // ชื่อพนักงาน กอง 
 			
         $query = $this->db->query('
		 			SELECT * ,
					(
						SELECT  t3.ID FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as group_id,
(
						SELECT  CONCAT(t3.staffPreName,t3.staffFName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' and t2.staffID = '.$empID.' limit 1
					) as groups,
(
						SELECT  CONCAT(t3.staffLName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' and t2.staffID = '.$empID.' limit 1
					) as groupsLName
FROM `tbl_org_chart` as t1   INNER JOIN tbl_staff_work as t5 ON t1.orgID = t5.orgID
WHERE (t1.upperOrgID = '.$OrgID.' and t1.assignID = '.$assignID.' and t5.staffID = '.$empID.')
						         '
				 );
        return $query->result(); 
		
 	}
	
	
	function calculate_emp4($OrgID,$assignID,$empID){ 
 			
         $query = $this->db->query('
		 			SELECT * FROM (`tbl_org_chart` as t1) WHERE  t1.upperOrgID = '.$OrgID.' and t1.assignID = '.$assignID.'  
						         '
				 );
        return $query->result(); 
 		 
	}
	
	function calculate_emp44($OrgID,$assignID,$empID){  // ชื่อพนักงงานแผนก
 			
         $query = $this->db->query('
		 			SELECT * ,
					(
						SELECT  t3.ID FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as department_id,
(
						SELECT  CONCAT(t3.staffPreName,t3.staffFName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' and t2.staffID = '.$empID.' limit 1
					) as department,
(
						SELECT  CONCAT(t3.staffLName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' and t2.staffID = '.$empID.' limit 1
					) as departmentLName
FROM `tbl_org_chart` as t1  INNER JOIN tbl_staff_work as t5 ON t1.orgID = t5.orgID
WHERE (t1.orgID = '.$OrgID.' and t1.assignID = '.$assignID.'  and t5.staffID = '.$empID.') 
						         '
				 );
        return $query->result(); 
		
		 
	}
	
	function eval_submit($orgID,$evalYear,$evalRound){
		 $query = $this->db->query("SELECT * FROM tbl_eval_submit_kpi as t1 WHERE t1.orgID = '".$orgID."' and t1.evalYear = '".$evalYear."' and t1.evalRound = '".$evalRound."' " );
         $data 		= $query->result(); 
 		 return ($query->num_rows() > 0) ? $data : NULL;
 	}
	 
	function eval_org_chart($orgID,$assignID){
		 $query 	= $this->db->query('SELECT * FROM tbl_org_chart WHERE upperOrgID = '.$orgID.'  and assignID = '.$assignID.' ' );
         $data 		= $query->result(); 
 		 return ($query->num_rows() > 0) ? $data : NULL;
		//return 'test !!';
		  
 	}
	
	function eval_tbl_eval_form($evalYear,$evalRound){
		 $query 	= $this->db->query('SELECT * FROM `tbl_eval_competency_form` as t1  WHERE t1.evalYear = '.$evalYear.' AND t1.evalRound = '.$evalRound.'' );
         $data 		= $query->result(); 
 		 return ($query->num_rows() > 0) ? $data : NULL;
		//return 'test !!';
		  
 	}
	
}

?>