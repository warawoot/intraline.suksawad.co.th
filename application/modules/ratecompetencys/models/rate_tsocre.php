<?php

class Rate_tsocre extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	} 
	
	function rate_emp($OrgID,$date){ 
 
         $this->db->select('*');
         $this->db->from('`tbl_org_chart` as t1 ');
         // $this->db->where('t1.workStartDate <=' ,$date);  or (t1.upperOrgID = 82 and t1.assignID = 6)
 		 $this->db->where('(t1.orgID = 82 and t1.assignID = 6)' );
         $query 		= $this->db->get();
 		 $data 		= $query->result(); 
 		 return ($query->num_rows() > 0) ? $data : NULL;
	}
 
	
	function rate_emp_party($OrgID,$assignID){
		 
		$query = $this->db->query('
		 			SELECT * ,
					(
						SELECT  t3.ID FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as person_id,
(
						SELECT  CONCAT(t3.staffPreName,t3.staffFName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as person,
(
						SELECT  CONCAT(t3.staffLName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as personLName ,
					( SELECT CONCAT(t4.rankName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID INNER JOIN tbl_rank as t4 ON t3.rankID = t4.rankID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1 ) as rank  ,
					(
						SELECT  t3.staffImage FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as staffImage ,
 					( SELECT t3.positionName FROM tbl_staff_work  as t2 INNER JOIN tbl_position as t3 ON t2.positionID = t3.positionID WHERE t1.orgID = t2.orgID AND t1.assignID =  '.$assignID.' limit 1 ) as positionName
 
FROM `tbl_org_chart` as t1 
WHERE (t1.orgID = '.$OrgID.' and t1.assignID = '.$assignID.') 
						         '
				 );
        return $query->result(); 
		
		
	}
	
	function rate_emp_division($OrgID,$assignID){ 
 			
         $query = $this->db->query('
		 			SELECT * ,
					(
						SELECT  t3.ID FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as group_id,
(
						SELECT  CONCAT(t3.staffPreName,t3.staffFName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as groups,
(
						SELECT  CONCAT(t3.staffLName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as groupsLName,
					( SELECT CONCAT(t4.rankName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID INNER JOIN tbl_rank as t4 ON t3.rankID = t4.rankID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1 ) as rank  ,
					(
						SELECT  t3.staffImage FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as staffImage ,
 					( SELECT t3.positionName FROM tbl_staff_work as t2 INNER JOIN tbl_position as t3 ON t2.positionID = t3.positionID WHERE t1.orgID = t2.orgID AND t1.assignID =  '.$assignID.' limit 1  				  ) as positionName
					
					
FROM `tbl_org_chart` as t1 
WHERE (t1.upperOrgID = '.$OrgID.' and t1.assignID = '.$assignID.')
						         '
				 );
        return $query->result(); 
		
		 
	}
	
	
	function rate_emp_department($OrgID,$assignID){ 
 			
         $query = $this->db->query('
		 			SELECT * FROM (`tbl_org_chart` as t1) WHERE  t1.upperOrgID = '.$OrgID.' and t1.assignID = '.$assignID.'  
						         '
				 );
        return $query->result(); 
		
		 
	}
	
	function rate_emp_department_person($OrgID,$assignID){ 
 			
         $query = $this->db->query('
		 			SELECT * ,
					(
						SELECT  t3.ID FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as department_id,
(
						SELECT  CONCAT(t3.staffPreName,t3.staffFName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as department,
(
						SELECT  CONCAT(t3.staffLName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as departmentLName,
					( SELECT CONCAT(t4.rankName) FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID INNER JOIN tbl_rank as t4 ON t3.rankID = t4.rankID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1 ) as rank  ,
					(
						SELECT  t3.staffImage FROM tbl_staff_work as t2 INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID WHERE t1.orgID = t2.orgID AND t1.assignID = '.$assignID.' limit 1
					) as staffImage  ,
 					( SELECT t3.positionName FROM tbl_staff_work as t2 INNER JOIN tbl_position as t3 ON t2.positionID = t3.positionID WHERE t1.orgID = t2.orgID AND t1.assignID =  '.$assignID.' limit 1  				  ) as positionName
FROM `tbl_org_chart` as t1 
WHERE (t1.orgID = '.$OrgID.' and t1.assignID = '.$assignID.') 
						         '
				 );
        return $query->result(); 
 		 
	}
	 
	
	function rate_employee_org($OrgID,$assignID){ 
          $query = $this->db->query('
		 			SELECT * FROM (`tbl_org_chart` as t1) WHERE  t1.OrgID = '.$OrgID.' and t1.assignID = '.$assignID.'  
						         '
				 );
        return $query->result(); 
 		 
	}
	
	function rate_employee_person($OrgID,$assignID){ 
  
	    $query = $this->db->query('
		 			SELECT t1.orgID ,t3.ID as employee_id , CONCAT(t3.staffPreName,t3.staffFName)  as employee , CONCAT(t3.staffLName) as employeeLName , t4.positionName ,t5.rankName  as rank ,t3.staffImage  as staffImage FROM `tbl_staff_work` as t1
					INNER JOIN tbl_org_chart as t2 ON t1.orgID = t2.orgID
					INNER JOIN tbl_staff as t3 ON t1.staffID = t3.ID
					INNER JOIN tbl_position as t4 ON t1.positionID = t4.positionID
					INNER JOIN tbl_rank as t5 ON t1.rankID = t5.rankID
					WHERE t1.orgID =  '.$OrgID.'  and t2.assignID = '.$assignID.' 
				  '
		  );
        return $query->result(); 
		
		 
	}
	
	
	function rate_employee_person_new($OrgID,$assignID){ 
 			
         $query = $this->db->query('
		 			SELECT t1.orgID ,t3.ID as employee_id , CONCAT(t3.staffPreName,t3.staffFName)  as employee , CONCAT(t3.staffLName) as employeeLName , t4.positionName ,t5.rankName  as rank ,t3.staffImage  as staffImage FROM `tbl_staff_work` as t1
					INNER JOIN tbl_org_chart as t2 ON t1.orgID = t2.orgID
					INNER JOIN tbl_staff as t3 ON t1.staffID = t3.ID
					INNER JOIN tbl_position as t4 ON t1.positionID = t4.positionID
					INNER JOIN tbl_rank as t5 ON t1.rankID = t5.rankID
					WHERE t1.orgID =  '.$OrgID.'  and t2.assignID = '.$assignID.' 
				  '
		  );
        return $query->result(); 
		
		 
	}
	
	function rate_emp5($OrgID,$assignID){ 
 			
         $query = $this->db->query('
		 			SELECT * FROM (`tbl_org_chart` as t1) WHERE  t1.orgID= '.$OrgID.' and t1.assignID = '.$assignID.'    '
				 );
        return $query->result(); 
 		 
	}
	
	function rate_emp55($OrgID,$assignID){ 
 			
         $query = $this->db->query('
		 			 SELECT  t3.ID, t3.staffPreName, t3.staffID, t3.staffFName, t3.staffLName FROM tbl_staff_work as t2 
INNER JOIN tbl_staff as t3 ON t2.staffID = t3.ID 
WHERE (t2.orgID = '.$OrgID.')   '
				 );
        return $query->result(); 
		
		 
	}
	
	
	## คำนวณ T - Score ##
	
	function tScoreCountPerson(){
		
		$this->db->select(' count(DISTINCT t1.empID) as person');
        $this->db->from('`tbl_eval` as t1');
  		$this->db->join('tbl_staff_work as t2' ,'t1.empID = t2.staffID' ,'inner'); 
		$this->db->join('tbl_staff as t3' ,'t2.staffID = t3.ID' ,'inner');
		$this->db->where('t2.orgID',$orgID);
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	function tScoreSumScore($empID){
		
		$query = $this->db->query('
		 			SELECT SUM(evalScore) as SumEvalScore ,t1.evalFormID FROM `tbl_eval_competency` as t1 WHERE  t1.empID = '.$empID.' '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
		
	}
	
	function tScoreSumGrade($score,$evalFormID){
		
		$query = $this->db->query('
		 			SELECT levelText FROM tbl_eval_form_level WHERE evalFormID = '.$evalFormID.' and '.$score.' BETWEEN minScore and maxScore '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
		
	}
	
	function tScoreSumPersons($score,$evalFormID){
		
		$query = $this->db->query('
		 			SELECT levelText FROM tbl_eval_form_level WHERE evalFormID = '.$evalFormID.' and '.$score.' BETWEEN minScore and maxScore '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
		
	}
	
	function tScoreGetEmployee($Employee){
		
		$query = $this->db->query('
		 			SELECT count(DISTINCT t1.empID) as person FROM `tbl_eval_competency` as t1 WHERE t1.empID in ( '.$Employee.' ) '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
		
	}
	
	
	function tblEvalEmployee($Employee){
		
		$query = $this->db->query('
		 			SELECT DISTINCT t1.empID  FROM `tbl_eval_competency` as t1 WHERE t1.empID = '.$Employee.' '
				 );
        $data       = $query->row_array(); 
        return ($query->num_rows() > 0) ? $data : NULL; 
		
	}
 
	
}

?>