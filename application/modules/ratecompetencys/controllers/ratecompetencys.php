<?php

class Ratecompetencys extends MY_Controller {
	
     function __construct() {
    	parent::__construct();
     	$this->load->model('rate_model');
		$this->load->model('rate_tsocre');
		$this->load->model('recent_model');
		$this->load->model('rate_tscore_calculate');
		$this->load->model('report/report_model');
		$this->load->model('sequence/sequence_model');
		$this->load->model('organization/organization_model');
		$this->assingID	=	$this->sequence_model->get_last_assignID();
		 
    }
  	 
    public function index() {
		
		## Recent ##
		 
		$year				= $this->input->get_post('year'); 
		$round				= $this->input->get_post('round'); 
		$recentz			= $round.','.$year;
 		
		$eval_date			= $this->input->get_post('eval_date');//get evalRound
 		
		$evalYear			= $this->input->post('evalYear');//get evalYear
		$agencies			= $this->input->post('agencies'); 
		 
		// Redirect  : [ eval=1,2558&staffID=161&var_agencies=82 ]
		$eval				= $this->input->get('eval'); 
		$staffID			= $this->input->get('staffID'); 
		$var_agencies		= $this->input->get('var_agencies'); 
 
		
		// End Redirect
		$var_eval_date				 = ($eval_date !=''?$eval_date:($eval !=''?$eval:($recentz !=''?$recentz:'')));
		
		## mode
		
		$data['eval_date']	= $this->rate_model->get_dropdown_all_new('evalRound' , 'evalYear' ,'tbl_eval_competency_date',$var_eval_date);#Dropdown 
   	    $data['dropdown_org_chart']  = $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assingID);#Dropdown
 		$data['get_dropdown_org_option']  = $this->rate_model->get_dropdown_org_option('orgID' , 'orgName' ,'tbl_org_chart',$this->assingID);#Dropdown
		
		//echo "<pre>";
		//print_r($data['get_dropdown_org_option']);
		
		if($eval_date !=''){
			$rate_eval_dates	= $this->rate_model->rate_eval_dates($eval_date); 
			 $data['dateDue']	= DateThaiHelper($rate_eval_dates['dateDue']);
		 
		}elseif($recentz !=''){
			$rate_eval_dates	= $this->rate_model->rate_eval_dates($recentz); 
			 $data['dateDue']	= DateThaiHelper($rate_eval_dates['dateDue']);
		 
		}
		
        $data['data']		=	'';
		$data['var_eval_date']	=	$var_eval_date; //evalRound
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:($var_agencies!=''?$var_agencies:0)); //agencies
		$data['assingID']		=	$this->assingID; //assingID
		
		// recent_submit($evalYear,$evalRound,$orgID) 
  		
		## Recent ##
		$data['year']		=	$year;
		$data['round']		=	$round;
 		
        $this->template->load('template/admin', 'test',$data);	
  		 
     }
	 
	 public function competency(){
		 $data['title']		=	'ประเมิน Competency';
		 $data['data']		=	'';
		// $this->template->load('template/admin', 'rate_competency',$data);
		$this->load->view("eval-competency");
	 }
	 
	 public function kpi(){
  		 
		 $data['title']		=	'Job Profile';
		 $eval				= $this->input->get_post('eval'); // evalRound
		 $staffID			= $this->input->get_post('staffID'); 
		 $var_agencies	    = $this->input->get_post('var_agencies'); 
 		 
		 if($staffID  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 if($eval  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval,2); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		 ## แสดงส่วนของประเมิน กลุ่ม ##
		 
		 $data['get_eval_group_subject'] = $this->rate_model->get_eval_group_subject($data['get_evalFormID'],2);  
		 //echo "<pre>";
		 //print_r( $data['get_eval_group_subject']);
		## Sum 
		  
		// echo "<pre>";
		// print_r($data['get_eval_notes']);
		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID; 
		 $data['var_agencies']	=	$var_agencies; 
		 $data['data']		=	'';
		 $data['data']		=	'';
		## Note
		 $data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID,$eval_doc['evalFormID'],2);
 
		 $this->template->load('template/admin', 'EvalFormKPI',$data);
		 //$this->load->view("eval-kpi");
	 }
	 
	 public function assessment(){
		 
		  /* (
					 -- SELECT * FROM tbl_org_chart as t1 
			-- WHERE (t1.orgID = 30 and t1.assignID = 6 )  
			
			-- SELECT * FROM tbl_org_chart as t1 
			--  WHERE (t1.orgID = 190 and t1.assignID = 6)
			
			-- SELECT * FROM tbl_org_chart as t1 
			-- WHERE (  t1.upperOrgID = 190 and t1.assignID =6
		  )*/
		 
		  $eval				= $this->input->get_post('eval'); // evalRound
		  $staffID			= $this->input->get_post('staffID'); 
		  $var_agencies	    = $this->input->get_post('var_agencies'); 
 		 
		 if($staffID  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 if($eval  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval,1); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		 ## แสดงส่วนของประเมิน กลุ่ม ##
		 
		 $data['get_eval_group_subject'] = $this->rate_model->get_eval_group_subject($data['get_evalFormID'],1);  

		 ## Note
		 //$data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID);
		 $data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID,$eval_doc['evalFormID'],1);
		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID; 
		 $data['var_agencies']	=	$var_agencies; 
		 //$data['data']		=	'';
		 $this->template->load('template/admin', 'rate_doc_test',$data);
	 }

	 public function getScoreAjax(){
	 	 $total			=	$this->input->get_post('total');
	 	 if(empty($total))$total = 0; //if not defined
	 	 # score 
 		 $maxScore	= $this->rate_model->tbl_eval_form_level(intval($total));
		 echo json_encode($maxScore);
	 }
	 
	public function docs(){
		 
 		 $evalType				=	 1; //$this->input->get_post('evalType'); // evalRound
		 $eval					=	explode(",",$this->input->get_post('eval')) ; // evalRound
		 $staffID				=	$this->input->get_post('staffID');
		 //$eval_doc				=	$this->rate_model->rate_eval_doc($eval); 
		 $ddl_count		    	=	count($this->input->get_post('ddl')) -1;	
		 
		 $ddlCoreCompetency			=	$this->input->get_post('CoreCompetency')  ;	
		  
		 $ddlFunctionalCompetency	=	$this->input->get_post('FunctionalCompetency')  ;
		 $ddlc_expectedlevel		=	$this->input->get_post('c_expectedlevel')  ;
		 $ddlc_score				=	$this->input->get_post('c_score')  ;	
		 
		 $ddlf_expectedlevel	=	$this->input->get_post('f_expectedlevel')  ;	
		 $ddlf_score			=	$this->input->get_post('f_score')  ;	
		 
		 $evalSubjectID_count	=	count($this->input->get_post('evalSubjectID'))  ;	
		 $evalSubjectID			=	$this->input->get_post('evalSubjectID');
		 $evalFormID			=	$this->input->get_post('get_evalFormID');	
		 $var_agencies			=	$this->input->get_post('var_agencies');	
		 
		 $total = 0;
		 $subject	=	'';
		 
		if($this->input->get_post('eval') == NULL){
			
			
			 for($i=0;$i<=4;$i++){
	 			//$total		 	+= $this->input->get_post($ddl[$i]);
	 			//$ddl_value		=	$this->input->get_post($ddl[$i]); 
				
				$ddl_CoreCompetency	=  $ddlCoreCompetency[$i] ; 
				$ddl_FunctionalCompetency	=  $ddlFunctionalCompetency[$i] ; 
				$ddl_c_expectedlevel		=  $ddlc_expectedlevel[$i] ; 
				$ddl_c_score				=  $ddlc_score[$i] ; 
 				$ddl_f_expectedlevel		=  $ddlf_expectedlevel[$i] ; 
				$ddl_f_score				=  $ddlf_score[$i] ; 
	 		    
	 			//$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array(	'empID' => $staffID, 
 	 									'ecID'=>$ddl_CoreCompetency,
	 									'ecScore'=>$ddl_c_score,'ecYear'=>$eval[1],
										'evalRound'=>$eval[0],
										'ecExpectScore'=>$ddl_c_expectedlevel
	 								 );
						
 	 			$insert_eval	=	$this->rate_model->insert_eval($arrayName);
	 		 }
			 
			 for($i=0;$i<=4;$i++){
	 			//$total		 	+= $this->input->get_post($ddl[$i]);
	 			//$ddl_value		=	$this->input->get_post($ddl[$i]); 
				
 				$ddl_FunctionalCompetency	=  $ddlFunctionalCompetency[$i] ; 
  				$ddl_f_expectedlevel		=  $ddlf_expectedlevel[$i] ; 
				$ddl_f_score				=  $ddlf_score[$i] ; 
	 		    
	 			//$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array(	'empID' => $staffID, 
 	 									'ecID'=>$ddl_FunctionalCompetency,
	 									'ecScore'=>$ddl_f_score,'ecYear'=>$eval[1],
										'evalRound'=>$eval[0],
										'ecExpectScore'=>$ddl_f_expectedlevel
	 								 );
						
 	 			$insert_eval	=	$this->rate_model->insert_eval($arrayName);
	 		 }
			 
	 	}else{
	 		for($i=0;$i<=4;$i++){
	 			//$total		 	+= $this->input->get_post($ddl[$i]);
	 			//$ddl_value		=	$this->input->get_post($ddl[$i]); 
	 		  	
				$ddl_CoreCompetency			= $ddlCoreCompetency[$i] ; 
 				$ddl_c_expectedlevel		= $ddlc_expectedlevel[$i] ; 
				$ddl_c_score				= $ddlc_score[$i] ; 
  	 		    
	 			//$subject 	= $evalSubjectID[$i] ;
	 			$arrayName  = array(	 
 	 									 
	 									'ecScore'=>$ddl_c_score,'ecYear'=>$eval[1],
										'evalRound'=>$eval[0],
										'ecExpectScore'=>$ddl_c_expectedlevel
	 								 );
									 
	 			$update_eval = $this->rate_model->update_eval($staffID,$ddl_CoreCompetency,$subject,$arrayName);
	 		 }
			 
			 for($i=0;$i<=4;$i++){
	 			//$total		 	+= $this->input->get_post($ddl[$i]);
	 			//$ddl_value		=	$this->input->get_post($ddl[$i]); 
	 		  	
 				$ddl_FunctionalCompetency	=  $ddlFunctionalCompetency[$i] ; 
   				$ddl_f_expectedlevel		=  $ddlf_expectedlevel[$i] ; 
				$ddl_f_score				=  $ddlf_score[$i] ; 
	 		    
	 			//$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array(	 
 	 									 
	 									'ecScore'=>$ddl_f_score,'ecYear'=>$eval[1],
										'evalRound'=>$eval[0],
										'ecExpectScore'=>$ddl_f_expectedlevel
	 								 );
									 
	 			$update_eval	=	$this->rate_model->update_eval($staffID,$ddl_FunctionalCompetency,$subject,$arrayName);
	 		 }
  			 
	 	}
 		//$data['get_eval']		=  $get_eval;
		$data['eval']			= $this->input->get_post('eval'); 
		$data['staffID']		= $staffID;
		$data['var_agencies']	= $var_agencies;
		$this->template->load('template/admin', 'rate_doc_test_ok',$data);
	}
	
	public function kpis(){
 		 $evalType				=	$this->input->get_post('evalType'); // evalRound
		 $eval					=	$this->input->get_post('eval'); // evalRound
		 $staffID				=	$this->input->get_post('staffID');
		 //$eval_doc				=	$this->rate_model->rate_eval_doc($eval); 
		 $ddl_count		    	=	count($this->input->get_post('ddl')) -1;	
		 $ddl					=	$this->input->get_post('ddl')  ;	
		 $evalSubjectID_count	=	count($this->input->get_post('evalSubjectID'))  ;	
		 $evalSubjectID			=	$this->input->get_post('evalSubjectID');
		 $evalFormID			=	$this->input->get_post('get_evalFormID');	
		 $var_agencies			=	$this->input->get_post('var_agencies');	
		 
		 $total = 0;

		 $get_eval 		=  $this->rate_model->get_eval($staffID,$evalFormID);
		if($get_eval == NULL){
			 for($i=0;$i<=$ddl_count;$i++){
	 			$total		 	+= $this->input->get_post($ddl[$i]);
	 			$ddl_value		=	$this->input->get_post($ddl[$i]); 
	 		  
	 			$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array('empID' => $staffID, 
	 								'evalFormID'=>$evalFormID,
	 								'evalSubjectID'=>$subject,
	 								'evalScore'=>$ddl_value,
									'evalType'=>$evalType
	 						);
	 			$insert_eval	=	$this->rate_model->insert_eval($arrayName);
	 		 }
	 	}else{
	 		for($i=0;$i<=$ddl_count;$i++){
	 			$total		 	+= $this->input->get_post($ddl[$i]);
	 			$ddl_value		=	$this->input->get_post($ddl[$i]); 
	 		  
	 			$subject 	=  	$evalSubjectID[$i] ;
	 			$arrayName  = 	array( 
	 								 
	 								'evalScore'=>$ddl_value
	 						);
	 			$update_eval	=	$this->rate_model->update_eval($staffID,$evalFormID,$subject,$arrayName);
	 		 }
	 	}

/*
		 //echo "total = " . $total;
 		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		 
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval,2); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		  # score 
 		 $maxScore	= $this->rate_model->tbl_eval_form_level($total);
		 //echo "<pre>";
		 //print_r($maxScore);
		 $data['evalFormID']	= $maxScore['evalFormID'];
		 $data['levelID']		= $maxScore['levelID'];
		 $data['levelText']		= $maxScore['levelText'];
		 $data['minScore']		= $maxScore['minScore'];
		 $data['maxScore']		= $maxScore['maxScore']; 
		 
	 
		 $data['eval']			= $eval; 
		 $data['staffID']		= $staffID;
 		 $data['total']			= $total;
		 $data['data']			= '';
		 $data['evalFormID']	= $evalFormID; 
		 $data['var_agencies']	= $var_agencies; 
		 $data['evalType']		= $evalType; 
		 $data['data']			= '';
*/

  		 //$this->template->load('template/admin', 'rate_doc_kpi',$data);
		 //$this->template->load('template/admin', 'rate_doc_test_sum',$data);
	//}
		// docs and sum are joined	 
	//public function sum(){ 
		//$eval				= $this->input->get_post('eval'); // evalRound
		//$staffID			= $this->input->get_post('staffID');
		//$total				= $this->input->get_post('total');
		//$evalFormID			= $this->input->get_post('evalFormID');
		$evalNote1			= $this->input->get_post('evalNote1');
		$evalNote2			= $this->input->get_post('evalNote2');
		$data['data']		=	'';
		//$var_agencies			= $this->input->get_post('var_agencies');
		$evalType				=	$this->input->get_post('evalType'); // evalRound
		
		$arrayName = array( 'empID' => $staffID ,
							'evalFormID'=> $evalFormID,
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
					
		$arrayNameUp = array(  
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
				
		$get_eval_notes	= $this->rate_model->get_eval_notes($staffID,$evalFormID,$evalType);//get_eval_notes($staffID);			
		
		if($get_eval_notes == NULL){	
 			$data['insert_eval_note']	=	$this->rate_model->insert_eval_note($arrayName);
		}else{
			$data['insert_eval_note']	=	$this->rate_model->update_eval_note($staffID,$evalFormID,$arrayNameUp);
		}
		
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['var_agencies']	=$var_agencies;
		$this->template->load('template/admin', 'rate_doc_test_ok',$data);
	}
	public function additional(){
 		 
		 $eval				= $this->input->get_post('eval'); // evalRound
		 $staffID			= $this->input->get_post('staffID');
		 $total				= $this->input->get_post('total');
		 $evalFormID		= $this->input->get_post('evalFormID');
		 $evalNote1			= $this->input->get_post('evalNote1');
		 $evalNote2			= $this->input->get_post('evalNote2');
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		 # score 
 		 $maxScore	= $this->rate_model->tbl_eval_form_level($total);
		 $data['evalFormID']	=	$maxScore['evalFormID'];
		 $data['levelID']	=	$maxScore['levelID'];
		 $data['levelText']	=	$maxScore['levelText'];
		 $data['minScore']	=	$maxScore['minScore'];
		 $data['maxScore']	=	$maxScore['maxScore']; 
 		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['total']		=	$total;
		 
		 $data['data']		=	'';
		 $this->template->load('template/admin', 'rate_doc_test_level',$data);
		   
		 
	}
	

	public function sum(){ 
		$eval				= $this->input->get_post('eval'); // evalRound
		$staffID			= $this->input->get_post('staffID');
		$total				= $this->input->get_post('total');
		$evalFormID			= $this->input->get_post('evalFormID');
		$evalNote1			= $this->input->get_post('evalNote1');
		$evalNote2			= $this->input->get_post('evalNote2');
		$data['data']		=	'';
		$var_agencies			= $this->input->get_post('var_agencies');
		$evalType				=	$this->input->get_post('evalType'); // evalRound
		
		$arrayName = array( 'empID' => $staffID ,
							'evalFormID'=> $evalFormID,
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
					
		$arrayNameUp = array(  
							'evalNote1'=> $evalNote1,
							'evalNote2'=> $evalNote2,
							'evalDate'=> date('Y-m-d H:i:s') ,
							'evalType'=> $evalType
					);
				
		$get_eval_notes	= $this->rate_model->get_eval_notes($staffID,$evalFormID,$evalType);//get_eval_notes($staffID);			
		
		if($get_eval_notes == NULL){	
 			$data['insert_eval_note']	=	$this->rate_model->insert_eval_note($arrayName);
		}else{
			$data['insert_eval_note']	=	$this->rate_model->update_eval_note($staffID,$evalFormID,$arrayNameUp);
		}
		
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['var_agencies']	=$var_agencies;
		$this->template->load('template/admin', 'rate_doc_test_ok',$data);
	}

	
	 
	public function tscore1(){
		
  		$varsd				= $this->input->get_post('sd');
		//echo  'varsd : '.$varsd;
		/// 2 - 09 - 2558 ///
		
		$eval_date			= $this->input->get_post('eval_date');//get evalRound
 		$evalYear			= $this->input->post('evalYear');//get evalYear
		$agencies			= $this->input->get_post('agencies'); 
 		/*$org_upper			= $this->rate_model->get_org_upper($agencies,$this->assingID);
 		foreach ($org_upper as $key => $value) {
			# code...
 			$org_array			=  $value ;
 		}
		echo $org_array;*/
		// $org_array			= $org_upper;
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
		//$data['eval_get']	= $this->rate_model->rate_emp($org_array,$eval_date); 
 		/// 1 - 09 - 2558 ///
  		$data['eval_date']				= $this->rate_model->get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown
		//$data['dropdown_org_chart'] = $this->organization_model->getDropdownTreeTscore($level = 0, $prefix = '' , $this->assingID ,$upperOrgID='' );#Dropdown  
		
		$data['dropdown_org_chart']  	= $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assingID);#Dropdown
		$data['get_evalScore'] 			= $this->rate_model->get_evalScore($agencies,$this->assingID); 
		
		$data['get_count_evalPerson']	= $this->rate_model->get_count_evalPerson($agencies); 
		 
		$data['get_count_evalScore']	= $this->rate_model->get_count_evalScore($agencies); 
		
		$data['varsd']			= $varsd;
		$data['data']			= '';
		$data['var_eval_date']	= $eval_date; //evalRound
		$data['var_evalYear']	= $evalYear;
		$data['var_agencies']	= $agencies; //agencies
		$this->template->load('template/admin', 'rate_tscore_test',$data);
		
  	} 
	
	public function sd(){
		 $sd						= $this->input->get_post('sd1');
		  
		 $data['data']		=	$sd ;
		
		$this->template->load('template/admin', 'rate_doc_test_sum1',$data);
		 //echo $type;
	}
	
	public function evaluation(){
		
		$data['data']		=	'';
		$this->template->load('template/admin', 'rate_doc_test_bomb',$data);
		
	}
	
	public function test(){
		
		$eval_date			= $this->input->post('eval_date');//get evalRound
		 
		$evalYear			= $this->input->post('evalYear');//get evalYear
		$agencies			= $this->input->post('agencies'); 
		 
		$data['eval_date']	= $this->rate_model->get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown
		 
		//$data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , $this->assingID ,$upperOrgID='' );#Dropdown  
  		$data['dropdown_org_chart']  = $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assingID);#Dropdown
		
		 
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
        $data['data']		=	'';
		$data['var_eval_date']	=	$eval_date; //evalRound
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:0); //agencies
		$data['assingID']		=	$this->assingID; //assingID
        $this->template->load('template/admin', 'test',$data);	
		
	}
	
	public function recent(){
		
		
		$eval_date				= $this->input->post('eval_date');//get evalRound
 		$evalYear				= $this->input->post('evalYear');//get evalYear
		$agencies				= $this->input->post('agencies'); 
 		$eval					= $this->input->get('eval'); 
		$staffID				= $this->input->get('staffID'); 
		$var_agencies			= $this->input->get('var_agencies'); 
		
		//parameter : recent
 		$recent_year		= $this->input->get_post('eval_date_year');
		$recent_evalRound	= $this->input->get_post('evalRound');
		// End Redirect
 		$data['eval_date_year']			= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown
 		
		$data['eval_date_roundRound']	= $this->recent_model->recent_get_dropdown_roundRound('evalYear' , 'evalRound' ,'tbl_eval_date',$recent_year);#Dropdown
   		if($recent_year != '' and $recent_evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assingID);#Dropdown
		    $data['roundRound']	= $this->recent_model->recent_get_dropdown_roundRound($recent_year,$recent_evalRound);#Dropdown
		}else{
			$data['org_faction']	=	'';
			$data['roundRound']		=	'';
		}
        $data['data']		=	'';
		$data['var_eval_date']	=	($eval_date !=''?$eval_date:($eval !=''?$eval:''));  
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:($var_agencies!=''?$var_agencies:0));  
		$data['assingID']		=	$this->assingID;  
		
		$data['recent_year']		=	$recent_year;  
		$data['recent_evalRound']	=	$recent_evalRound;  
       
		$data['data']		=	'';  
        $this->template->load('template/admin', 'recent',$data);
		
   }
   
   public function recentdll(){
	   
	   $recent_year		= $this->input->get_post('assignDate');
	   $roundRound		= $this->recent_model->recent_get_dropdown_roundRound($recent_year,'');#Dropdown
   		
	    echo json_encode($roundRound);
   
   }
   
   public function submit(){
	   
	   $year			= $this->input->post('year');
	   $round			= $this->input->post('round');
	   $var_agencies	= $this->input->post('var_agencies');
	   
	   $data			= array(
	   							"evalYear"=>$year,
								"evalRound"=>$round ,
								"orgID"=>$var_agencies,
								"completeDate"=>date('Y-m-d H:i:s')
	   					  );
	   $roundRound		= $this->recent_model->insert_submit($data);#Dropdown
	   if( $roundRound== 'succesfully'){
	   		redirect(site_url().'rate/recent?eval_date_year='.$year.'&evalRound='.$round.'&var_agencies='.$var_agencies.'', 'refresh');
	  		exit();
	   }
  }
  
  public function demo(){
	    $eval				= $this->input->get_post('eval'); // evalRound
		$staffID			= $this->input->get_post('staffID');
		$total				= $this->input->get_post('total');
		$evalFormID			= $this->input->get_post('evalFormID');
		$evalNote1			= $this->input->get_post('evalNote1');
		$evalNote2			= $this->input->get_post('evalNote2');
		$var_agencies		= $this->input->get_post('var_agencies');
		$data['data']		=	'';
		$data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID;
		 $data['var_agencies']	=$var_agencies;
		$this->template->load('template/admin', 'rate_doc_test_ok',$data);  
  }
	
  public function xx1() {
		
		## Recent ##
   		$evalYear			= $this->input->post('evalYear'); 
		$agencies			= $this->input->post('agencies'); 
  	  	$evalRound			= $this->input->post('evalRound'); 
   		if($evalYear != '' and $evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assingID);#Dropdown
			$data['roundRound']		= $this->recent_model->recent_get_dropdown_roundRound($evalYear,$evalRound);#Dropdown
 		}else{
			$data['roundRound']		= '';	
 		}
 		
		## mode
		$data['eval_date_year']		= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown เลือกปีงบประมาณ
    	$data['dropdown_org_chart']	= $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assingID);#Dropdown หน่วยงาน
 		$data['eval_eval_submit']	= $this->rate_tscore_calculate->eval_submit($agencies,$evalYear,$evalRound);
 		$data['var_agencies']		= $agencies;
		$data['var_evalYear']		= $evalYear;
 		$data['var_assignID']		 = $this->assingID;
        $this->template->load('template/admin', 'rate_tscore_demo',$data);	 
 		 
     }   
	 
	public function xx2() {
		
		$year				= $this->input->get_post('year'); 
		$round				= $this->input->get_post('round'); 
		//$recentz			= $round.','.$year;
 		
		//$eval_date			= $this->input->post('eval_date');//get evalRound
 		
		
		//$evalYear			= $this->input->post('evalYear');//get evalYear
		//$agencies			= $this->input->post('agencies'); 
		 
		// Redirect  : [ eval=1,2558&staffID=161&var_agencies=82 ]
		$eval				= $this->input->get('eval'); 
		$staffID			= $this->input->get('staffID'); 
		$var_agencies		= $this->input->get('var_agencies'); 
	 	## Recent ##
   		$evalYear			= $this->input->post('evalYear'); 
		$agencies			= $this->input->post('agencies'); 
  	  	$evalRound			= $this->input->post('evalRound'); 
   		if($evalYear != '' and $evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assingID);#Dropdown
			$data['roundRound']		= $this->recent_model->recent_get_dropdown_roundRound($evalYear,$evalRound);#Dropdown
 		}else{
			$data['roundRound']		= '';	
			$data['org_faction']	= '';
 		}
 		
		## mode
		$data['eval_date_year']		= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown เลือกปีงบประมาณ 
   	    $data['dropdown_org_chart']  = $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assingID);#Dropdown
 		
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
        $data['data']		=	'';
		$data['var_eval_date']	=	$evalRound; //evalRound
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:($var_agencies!=''?$var_agencies:0)); //agencies
		$data['assingID']		=	$this->assingID; //assingID
		
		// recent_submit($evalYear,$evalRound,$orgID) 
		
		
		 
		
		## Recent ##
		$data['year']		=	$year;
		$data['round']		=	$round;
 		
        $this->template->load('template/admin', 'rate_tscore_demo',$data);	
		
 	}
	
 	public function tscore() {
	 
		$eval				= $this->input->get_post('eval'); 
		$staffID			= $this->input->get_post('staffID'); 
		$var_agencies		= $this->input->get_post('var_agencies'); 
	 	## Recent ##.
		$n					= $this->input->get_post('n'); 
		$sd					= $this->input->get_post('sd'); 
   		$evalYear			= $this->input->get_post('evalYear'); 
		$agencies			= $this->input->get_post('agencies'); 
  	  	$evalRound			= $this->input->get_post('evalRound');
		$average			= $this->input->get_post('average');
 		$squareAll			= $this->input->get_post('squareAll'); 
   		if($evalYear != '' and $evalRound !='' ){
			$data['org_faction']	= $this->recent_model->org_faction($this->assingID);#Dropdown
			$data['roundRound']		= $this->recent_model->recent_get_dropdown_roundRound($evalYear,$evalRound);#Dropdown
 		}else{
			$data['roundRound']		= '';	
			$data['org_faction']	= '';
 		}
 		
		## mode
		$data['eval_date_year']			= $this->recent_model->recent_get_dropdown_all('evalRound' , 'evalYear' ,'tbl_eval_date');#Dropdown เลือกปีงบประมาณ 
   	    $data['dropdown_org_chart']  	= $this->rate_model->get_dropdown_org('orgID' , 'orgName' ,'tbl_org_chart',$this->assingID);#Dropdown
 		
		$data['get_evalScore'] 			= $this->rate_model->get_evalScore($agencies,$this->assingID); 
		
		$data['get_count_evalPerson']	= $this->rate_model->get_count_evalPerson($agencies); 
		//echo "<pre>";
		//print_r($data['get_count_evalPerson']); 
		 
		 
		$data['get_count_evalScore']	= $this->rate_model->get_count_evalScore($agencies); 
		
		
		//$data['eval_get']	= $this->rate_model->rate_emp($agencies,$eval_date); 
        $data['data']		=	'';
		$data['var_eval_date']	=	$evalRound; //evalRound
		$data['var_evalYear']	=	$evalYear;
		$data['var_agencies']	=	($agencies !=''?$agencies:($var_agencies!=''?$var_agencies:0)); //agencies
		$data['assingID']		=	$this->assingID; //assingID 
 		$data['average']		=	$average;
		$data['sd']				=	$sd	;
		$data['squareAll']		=	$squareAll;
		$data['n']				=	$n;
		## Recent ##
		//$data['year']		=	$year;
		//$data['round']		=	$round;
 		
        $this->template->load('template/admin', 'rate_tscore_demo',$data);	
		
		 
	}
	
	public function  tscore_sum(){
		
		$employee_id_count		    	=	count($this->input->post('employee_id')) ;	
		$employee_id					=	$this->input->post('employee_id')  ; 	
		
		//for($i=0;$i<=$employee_id_count;$i++){
			//$xx	= array_push($employee_id[$i]); 
		//}
		//echo "<pre>";
		//print_r($xx);
		//$var							=	$employee_id[ ];
		//  $employee 		= $this->rate_tsocre->tScoreGetEmployee(array_push($employee_id)); 
		//$secret 		= $_POST['secret'];
		// $ar				= explode(',',$employee_id);
		
		//echo "<pre>";
		//print_r($ar);
	   // echo json_encode($employee_id_count);
	}
	
	public function ekpi(){
		$data['title']		=	'Job Profile';
		 $eval				= $this->input->get_post('eval'); // evalRound
		 $staffID			= $this->input->get_post('staffID'); 
		 $var_agencies	    = $this->input->get_post('var_agencies'); 
 		 
		 if($staffID  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 if($eval  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval,2); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		 ## แสดงส่วนของประเมิน กลุ่ม ##
		 
		 $data['get_eval_group_subject'] = $this->rate_model->get_eval_group_subject($data['get_evalFormID'],2);  
		 //echo "<pre>";
		 //print_r( $data['get_eval_group_subject']);
		## Sum 
		  
		// echo "<pre>";
		// print_r($data['get_eval_notes']);
		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID; 
		 $data['var_agencies']	=	$var_agencies; 
		 $data['data']		=	'';
		 $data['data']		=	'';
		## Note
		 $data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID,$eval_doc['evalFormID'],2);
 
		 $this->template->load('template/admin', 'EvalFormKPI-Edit',$data);
		 //$this->load->view("eval-kpi");
	
	}
	public function eassessment(){
		 
		  $eval				= $this->input->get_post('eval'); // evalRound
		  $staffID			= $this->input->get_post('staffID'); 
		  $var_agencies	    = $this->input->get_post('var_agencies'); 
 		 
		 if($staffID  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 if($eval  == ''){
			 redirect(site_url().'rate', 'refresh');
			 exit();
 		 }
		 
		 ## เช็คส่วนของการประเมินผลปฏิบัติงาน ##
		// $eval				= array($round,$year);
 		 
		 $eval_doc			= $this->rate_model->rate_eval_doc($eval,1); 
		 
		 $data['get_evalYear']	= $eval_doc['evalYear'];
		 $data['get_evalRound']	= $eval_doc['evalRound'];
		 $data['get_startDate']	= DateThaiHelper($eval_doc['startDate']);
		 $data['get_endDate']	= DateThaiHelper($eval_doc['endDate']);
		 $data['get_evalNameText']	= $eval_doc['evalNameText'];
		 $data['get_evalFormID']	= $eval_doc['evalFormID']; 
		 ## แสดงส่วนของรายชื่อพนักงงาน ## 
		  
		 $eval_emp	= $this->rate_model->rate_emp_doc($staffID); 
		 
		 $data['get_staffPreName']	= $eval_emp['staffPreName'];
		 $data['get_staffFName']	= $eval_emp['staffFName'];
		 $data['get_staffLName']	= $eval_emp['staffLName'];
		 $data['get_positionName']	= $eval_emp['positionName'];  
		 $data['orgID']				= $eval_emp['orgID'];
		 
		 ## แสดงส่วนของประเมิน กลุ่ม ##
		 
		 $data['get_eval_group_subject'] = $this->rate_model->get_eval_group_subject($data['get_evalFormID'],1);  

		 ## Note
		 //$data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID);
		 $data['get_eval_notes']	= $this->rate_model->get_eval_notes($staffID,$eval_doc['evalFormID'],1);
		 
		 $data['eval']		=	$eval; 
		 $data['staffID']	=	$staffID; 
		 $data['var_agencies']	=	$var_agencies; 
		 $data['data']		=	'';
		 $this->template->load('template/admin', 'rate_doc_test-edit',$data);
	 }

}

?>