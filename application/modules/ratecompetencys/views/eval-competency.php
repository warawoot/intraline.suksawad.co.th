<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="images/favicon.png">
	<title>DPO</title>
	
	<!-- ######### CSS STYLES ######### -->
	<!--Core CSS -->
 
    <link href="http://dpo-ehr.smmms.com/assets/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="http://dpo-ehr.smmms.com/assets/bootstrap/assets/css/docs.min.css" rel="stylesheet">
    
    <link href="http://dpo-ehr.smmms.com/assets/css/bootstrap-reset.css" rel="stylesheet">
    <link href="http://dpo-ehr.smmms.com/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="http://dpo-ehr.smmms.com/assets/css/style.css" rel="stylesheet">
    <link href="http://dpo-ehr.smmms.com/assets/css/style-responsive.css" rel="stylesheet" />
    
    <link href="http://dpo-ehr.smmms.com/assets/css/table-style.css" rel="stylesheet">
    
    <link href="http://dpo-ehr.smmms.com/assets/css/jquery.dataTables.css" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<link href="http://dpo-ehr.smmms.com/assets/bootstrap/dist/css/bootstrap-theme-modify.css" rel="stylesheet">
	<!-- ######### SCRIPT ######### -->
	<!--Core js-->
	<script src="http://dpo-ehr.smmms.com/assets/js/jquery-2.1.3.min.js"></script>
    
	<script src="http://dpo-ehr.smmms.com/assets/js/3.2.0.bootstrap.min.js"></script>
	<script src="http://dpo-ehr.smmms.com/assets/js/jquery.dcjqaccordion.2.7.js" class="include" type="text/javascript"></script>
	<script src="http://dpo-ehr.smmms.com/assets/js/jquery.scrollTo.min.js"></script>
	<script src="http://dpo-ehr.smmms.com/assets/js/jquery.nicescroll.js"></script>
    
	<script src="http://dpo-ehr.smmms.com/assets/js/jquery.validate.js"></script>
    <script src="http://dpo-ehr.smmms.com/assets/js/jquery.dataTables.min.js"></script>
	
	<!--common script init for all pages-->
	<script src="http://dpo-ehr.smmms.com/assets/js/scripts.js"></script>
  
    <script src="http://dpo-ehr.smmms.com/assets/js/angular.min.js"></script>
    <script src="http://dpo-ehr.smmms.com/assets/js/angular-resource.min.js"></script> 
    <script src="http://dpo-ehr.smmms.com/assets/js/angular-route.min.js"></script>
    <style type="text/css">
        .form-control {
          color: #343232;
        }
    </style>
</head>
<body>	
    <section id="container" ng-app="baseApp" >
        <!--header start-->
        <header class="header fixed-top clearfix">
        <!--logo start-->
        <div class="brand">
            <a href="http://dpo-ehr.smmms.com/" class="logo">
                    <img src="http://dpo-ehr.smmms.com/assets/images/logodpo.png" alt="dpo" width="80px" height="80px">
            </a>
            <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
            </div>
        </div>
        <!--logo end-->
        <!--top_menu--><!--top_menu end-->
        <div class="top-nav clearfix">
            <!--search & user info start-->
            <div class="col-sm-6 col-md-6 pull-left" style="margin-top:3px; margin-left:30px;">
                           <!-- <form id="attributeForm" class="form-horizontal" role="form" action="http://dpo-ehr.smmms.com/dashboard" method="post">
                                <div class="input-group">
                                    <input type="text" class="form-control"   name="search" id="search" placeholder="ค้นหา" style=" border-radius: 0px;">
                                    <div class="input-group-btn">
                                        <button type="submit" id="check_license" class="btn btn-default" style="border-left: 0; border-radius: 0px;"/*onclick="modal_research()"*/>ค้นหา</button>
                                    </div>
                                </div>
                            </form>-->
            </div>
            <ul class="nav pull-right top-menu">
                <!-- user login dropdown start-->
                <li> 
                        	<div style="padding:0px;">
                                 <div class="img-circle " style="zoom:1.5; margin-top:3px; opacity:0.8;  filter:alpha(opacity=40); ">
                                 <a href="http://dpo-ehr.smmms.com/dashboard" title="หน้าแรก" class="text-muted">
                                <i class="glyphicon glyphicon-home "></i></a> 
                                  </div>
                             </div>  
                        </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        
						
                        <span class="username">admin</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                        <!--<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>-->
                        <li><a href="http://dpo-ehr.smmms.com/logout"><i class="fa fa-key"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <!--search & user info end-->
        </div>
        </header>
        <!--header end-->


        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse" ng-controller="menuCtrl">
                    <!-- sidebar menu start-->            
                    <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <li>
    <a ng-class="{active:isActive('/dashboard')}" href="http://dpo-ehr.smmms.com/dashboard">
        <i class="fa fa-home"></i>
        <span>หน้าแรก</span>
    </a>
</li> 

<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>โครงสร้างองค์กร</span>
        </a>
        <ul class="sub">
              <li>
               	  <a href="http://dpo-ehr.smmms.com/appoint"><i class="fa fa-check-square fa-fw"></i>&nbsp; จัดโครงสร้างองค์กร</a> 
              </li>
              <!--<li>
                  <a href="http://dpo-ehr.smmms.com/report/organization"><i class="fa fa-file"></i>&nbsp; ผังโครงสร้างองค์กร</a> 
              </li> --> 
         </ul>
</li>

<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>ตำแหน่งงาน</span>
        </a>
        <ul class="sub">
               <li>
               	<a href="http://dpo-ehr.smmms.com/structure"><i class="fa fa-check-square fa-fw"></i>&nbsp; โครงสร้างตำแหน่งงาน</a>  
              </li>
              <li>
                    <a href="http://dpo-ehr.smmms.com/sequence"><i class="fa fa-check-square fa-fw"></i>&nbsp; อัตราพนักงาน</a> 
              </li>
              <li class="sub-menu">
              <a href="#"><i class="fa fa-caret-down"></i>&nbsp; ข้อมูลพื้นฐาน</a> 
                   <ul class="sub sub-lv2">
                      <li>
                            <a href="http://dpo-ehr.smmms.com/position"><i class="fa fa-file"></i>&nbsp; ชื่อตำแหน่ง</a> 
                      </li>
                      <li>
                            <a href="http://dpo-ehr.smmms.com/level"><i class="fa fa-file"></i>&nbsp; ชื่อระดับ</a> 
                      </li>
                      <li>
                        	<a href="http://dpo-ehr.smmms.com/workplace"><i class="fa fa-file"></i>&nbsp; สถานที่ทำงาน</a> 
                      </li>
                      <li>
                        	<a href="http://dpo-ehr.smmms.com/workstatus"><i class="fa fa-file"></i>&nbsp; สถานะพนักงาน</a> 
                      </li>
                      
                  </ul>
             </li>
              
         </ul>
</li>

<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>โครงการ</span>
        </a>
        <ul class="sub">
              <li>
                     <a href="http://dpo-ehr.smmms.com/employment"><i class="fa fa-check-square fa-fw"></i>&nbsp; ข้อมูลโครงการ </a> 
              </li>  
              <li>
                    <a href="http://dpo-ehr.smmms.com/project"><i class="fa fa-check-square fa-fw"></i>&nbsp; ตำแหน่งงานตามโครงการ</a> 
              </li>
         </ul>
</li>
 
<!--<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>โครงสร้างองค์กร</span>
        </a>
        <ul class="sub">
              <li>
               	<a href="http://dpo-ehr.smmms.com/appoint"><i class="fa fa-check-square fa-fw"></i>&nbsp; จัดโครงสร้างองค์กร</a> 
              </li>
              <li>
               	<a href="http://dpo-ehr.smmms.com/structure"><i class="fa fa-check-square fa-fw"></i>&nbsp; โครงสร้างตำแหน่งงาน</a>  
              </li>
              <li>
                     <a href="http://dpo-ehr.smmms.com/employment"><i class="fa fa-check-square fa-fw"></i>&nbsp; งานโครงการ</a> 
              </li>
              <li>
                    <a href="http://dpo-ehr.smmms.com/project"><i class="fa fa-check-square fa-fw"></i>&nbsp; ตำแหน่งงานตามโครงการ</a> 
              </li>
              <li>
                    <a href="http://dpo-ehr.smmms.com/sequence"><i class="fa fa-check-square fa-fw"></i>&nbsp; อัตราพนักงงาน</a> 
              </li>
              <li class="sub-menu">
              <a href="#"><i class="fa fa-caret-down"></i>&nbsp; ข้อมูลพื้นฐาน</a> 
                   <ul class="sub sub-lv2">
                      <li>
                            <a href="http://dpo-ehr.smmms.com/position"><i class="fa fa-file"></i>&nbsp; ชื่อตำแหน่ง</a> 
                      </li>
                      <li>
                            <a href="http://dpo-ehr.smmms.com/level"><i class="fa fa-file"></i>&nbsp; ชื่อระดับ</a> 
                      </li>
                      <li>
                        	<a href="http://dpo-ehr.smmms.com/workplace"><i class="fa fa-file"></i>&nbsp; สถานที่ทำงาน</a> 
                      </li>
                      <li>
                        	<a href="http://dpo-ehr.smmms.com/workstatus"><i class="fa fa-file"></i>&nbsp; สถานะพนักงาน</a> 
                      </li>
                      
                  </ul>
             </li>
             <li class="sub-menu">
              <a href="#"><i class="fa fa-caret-down"></i>&nbsp; รายงาน</a> 
                   <ul class="sub sub-lv2">
                      <li>
                            <a href="http://dpo-ehr.smmms.com/report/organization"><i class="fa fa-file"></i>&nbsp; ผังโครงสร้างองค์กร</a> 
                      </li> 
                  </ul>
             </li>
          </ul>
</li>-->


<li class="sub-menu">
        <a  href="javascript:;" id="defaults" ng-class="getClass('/reg')">
              <i class="fa fa-user"></i>
              <span>ทะเบียนประวัติ</span>
        </a>
        <ul class="sub">
              <li>
                <a href="http://dpo-ehr.smmms.com/reg"><i class="fa fa-check-square fa-fw"></i>&nbsp; พนักงาน</a> 
              </li>
              <li>
                <a href="http://dpo-ehr.smmms.com/stafftype"><i class="fa fa-check-square fa-fw"></i>&nbsp; ประเภทพนักงาน</a> 
              </li>
              <li>
                <a href="http://dpo-ehr.smmms.com/contracttype"><i class="fa fa-check-square fa-fw"></i>&nbsp; ประเภทสัญญาจ้าง</a> 
              </li>
              <li>
                <a href="http://dpo-ehr.smmms.com/mistaketype"><i class="fa fa-check-square fa-fw"></i>&nbsp; ประเภทการลงโทษ</a> 
              </li>
               <li>
                <a href="http://dpo-ehr.smmms.com/notification"><i class="fa fa-check-square fa-fw"></i>&nbsp; แจ้งเตือน</a> 
             </li>
             <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i>
                        <span>เครื่องราช</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="http://dpo-ehr.smmms.com/insigniatype"><i class="fa fa-file"></i>&nbsp; ประเภทเครื่องราช</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/insignia"><i class="fa fa-file"></i>&nbsp; เครื่องราช</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/insigniacondition"><i class="fa fa-file"></i>&nbsp; เงื่อนไข / หลักเกณฑ์</a> 
                        </li>
                        
                  </ul>
            </li>
              <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>รายงาน</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="http://dpo-ehr.smmms.com/reportcontract"><i class="fa fa-file"></i>&nbsp; พนักงานครบกำหนดสัญญาจ้าง</a> 
                         </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/reportbirthday"><i class="fa fa-file"></i>&nbsp; พนักงานที่เกิด</a> 
                        </li>
                        <li> 
                          <a href="http://dpo-ehr.smmms.com/reportretired"><i class="fa fa-file"></i>&nbsp; พนักงานที่เกษียณอายุ</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/reportstatusemployees"><i class="fa fa-file"></i>&nbsp; พนักงานที่พ้นสภาพ</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/reportstatage"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ อายุงาน</a> 
                        </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportstatrank"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ เลื่อนระดับ</a> 
                        </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportstatcontract"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ สัญญาจ้าง</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/reportstatretired"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ เกษียณอายุ</a> 
                        </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportstatstatus"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ พ้นสภาพ</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/cerificatework"><i class="fa fa-file"></i>&nbsp; ใบรับรองการทำงาน</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/reportposition"><i class="fa fa-file"></i>&nbsp; ตำแหน่งว่าง</a> 
                        </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportinsignia"><i class="fa fa-file"></i>&nbsp; ผู้มีสิทธิ์ได้รับเครื่องราช</a> 
                        </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportrank"><i class="fa fa-file"></i>&nbsp; ผู้มีสิทธิ์เลื่อนระดับขั้น</a> 
                        </li>
                  </ul>
            </li>
           
               <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>ข้อมูลการศึกษา</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="http://dpo-ehr.smmms.com/education"><i class="fa fa-file"></i>&nbsp; ระดับการศึกษา</a> 
                         </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/eduplace"><i class="fa fa-file"></i>&nbsp; สถานศึกษา</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/edutype"><i class="fa fa-file"></i>&nbsp; ประเภทสถานศึกษา</a> 
                        </li>
                        <li> 
                          <a href="http://dpo-ehr.smmms.com/edufaculty"><i class="fa fa-file"></i>&nbsp; คณะ / สายการเรียน</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/edudepartment"><i class="fa fa-file"></i>&nbsp; สาขา</a> 
                        </li>
                        <li>
                          <a href="http://dpo-ehr.smmms.com/edudegree"><i class="fa fa-file"></i>&nbsp; วุฒิการศึกษา</a> 
                        </li>
                         
                        <li>
                          <a href="http://dpo-ehr.smmms.com/country"><i class="fa fa-file"></i>&nbsp; ประเทศ</a> 
                        </li>
                       
                  </ul>
            </li>
           
        </ul>
</li>

<li class="sub-menu">
        <a  href="javascript:;" id="defaults" ng-class="getClass('/welfare')">
              <i class="fa fa-medkit"></i>
              <span>สวัสดิการ</span>
        </a>
        <ul class="sub">
               <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>ข้อมูลพื้นฐาน</span>
                  </a>
                  <ul class="sub sub-lv2">
                      <li>
                          <a href="http://dpo-ehr.smmms.com/hospitaltype"><i class="fa fa-file"></i>&nbsp; ประเภทสถานพยาบาล</a> 
                      </li>
                      <li>
                          <a href="http://dpo-ehr.smmms.com/hospital"><i class="fa fa-file"></i>&nbsp; สถานพยาบาล</a> 
                      </li> 
                      <li>
                            <a href="http://dpo-ehr.smmms.com/patienttype"><i class="fa fa-file"></i>&nbsp; ประเภทผู้ป่วย</a> 
                      </li>
                      <li>
                            <a href="http://dpo-ehr.smmms.com/welfaretype"><i class="fa fa-file"></i>&nbsp; ประเภทสวัสดิการ</a> 
                      </li> 
                     <li>
                            <a href="http://dpo-ehr.smmms.com/welfarelistfee"><i class="fa fa-file"></i>&nbsp; รายการเบิกค่ารักษาพยาบาล</a> 
                      </li>
                      <li>
                            <a href="http://dpo-ehr.smmms.com/welfareright"><i class="fa fa-file"></i>&nbsp; สิทธิ์สวัสดิการ</a> 
                      </li> 
                      <li>
                            <a href="http://dpo-ehr.smmms.com/welfarepay"><i class="fa fa-file"></i>&nbsp; ช่องทางการรับเงิน</a> 
                      </li>
                      <li>
                            <a href="http://dpo-ehr.smmms.com/welfarecalendar"><i class="fa fa-file"></i>&nbsp; ปฏิทินการจ่ายเงิน</a> 
                      </li>  
                  </ul>
            </li>
            <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>ข้อมูลสวัสดิการ</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="http://dpo-ehr.smmms.com/staffwelfareright"><i class="fa fa-file"></i>&nbsp; กำหนดสิทธิ์สวัสดิการ</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/#"><i class="fa fa-file"></i>&nbsp; เบิกสวัสดิการ</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/#"><i class="fa fa-file"></i>&nbsp; Imkport/Export</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/#"><i class="fa fa-file"></i>&nbsp; ตรวจสอบ/เรียกดูสิทธิ์สวัสดิการ</a> 
                         </li>
                  </ul>
            </li>
            <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>แจ้งเตือน</span>
                  </a>
            </li>
                          <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>รายงาน</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="http://dpo-ehr.smmms.com/cerificateform"><i class="fa fa-file"></i>&nbsp; แบบฟอร์ม / หนังสือรับรอง</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportmedicalfeehistory"><i class="fa fa-file"></i>&nbsp; ประวัติการเบิกค่ารักษาพยาบาล</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportsicknessrank"><i class="fa fa-file"></i>&nbsp; สถิติการเจ็บป่วยของพนักงาน</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportmedicalfeerank"><i class="fa fa-file"></i>&nbsp; สถิติการเบิกค่ารักษาพยาบาล</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reporthealthcheck"><i class="fa fa-file"></i>&nbsp; รายงานตรวจสุขภาพ</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportstaffnothealthchecklist"><i class="fa fa-file"></i>&nbsp; รายชื่อพนักงานที่ไม่ตรวจสุขภาพ</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportmunstaffrank"><i class="fa fa-file"></i>&nbsp; จำนวนพนักงานตามเชิงสถิติ</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportmedicalfeestaff"><i class="fa fa-file"></i>&nbsp; สรุปการเบิกค่ารักษาพยาบาล</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportmedicalfeeall"><i class="fa fa-file"></i>&nbsp; ยอดการเบิกค่ารักษาพยาบาล</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reporthealthcheckfeestatement"><i class="fa fa-file"></i>&nbsp; ยอดการเบิกค่าตรวจสุขภาพ</a> 
                         </li>
                         <li>
                          <a href="http://dpo-ehr.smmms.com/reportduemedicalfee"><i class="fa fa-file"></i>&nbsp; สรุปเงินค้างชำระค่ารักษาพยาบาล</a> 
                         </li>
                  </ul>
              </li>
        </ul>
</li>
<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>ประเมิน</span>
        </a>
        <ul class="sub">
              <li>
                     <a href="<?php echo site_url();?>evaluation"><i class="fa fa-check-square fa-fw"></i>&nbsp;จัดการข้อมูลการประเมิน </a> 
              </li>  
              <li>
                    <a href="<?php echo site_url();?>rate"><i class="fa fa-check-square fa-fw"></i>&nbsp; ข้อมูลการประเมิน</a> 
              </li>
              <li>
                    <a href="<?php echo site_url();?>rate/tscore"><i class="fa fa-check-square fa-fw"></i>&nbsp; รายงาน</a> 
              </li>
              <!--<li>
                    <a href="<?php echo site_url();?>rate/tscore"><i class="fa fa-check-square fa-fw"></i>&nbsp; ข้อมูลการประเมิน</a> 
              </li>-->
         </ul>
</li>
<li>
    <a href="#">
        <i class="fa fa-gears"></i>
        <span>พัฒนาบุคลากร</span>
    </a>
</li>
<li>
    <a href="#">
        <i class="fa fa-clipboard"></i>
        <span>ฝึกอบรม</span>
    </a>
</li>		
                        </ul>
                    </div>        
            <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->


        <!--main content start-->
        <section id="main-content" >
                <section class="wrapper">

                    
                    
                    <style type="text/css">
	.form-control {
 	  color: #343232;
	 
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล</h3>
            </header>
            <div class="panel-body">
              <form  id="myForm" method="post" action="<?php echo base_url(); ?>rate/demo" enctype="multipart/form-data">
                <table class="table" style="background-color: #fff; font-size:14px;">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td colspan="7" align="center" style=" font-size:16px;"><strong>แบบประเมินสมรรถนะ  (Competency Assessment ) สำหรับระดับปฏิบัติการ
</strong>
<br/>
<br/>
ประจำปีงบประมาณ 2558 <strong> ครั้งที่ </strong>1 ( <strong>วันที่</strong> 1 สิงหาคม 2558 - 31 สิงหาคม 2558) </td>
                      </tr>
                      <tr>
                        <td><strong>ชื่อ-นามสกุล</strong></td>
                        <td>นางโกวิทย์&nbsp;&nbsp;นิธิชัย</td>
                        <td>&nbsp;</td>
                        <td><strong>ตำแหน่ง</strong></td>
                        <td>นักวิชาการ</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>แผนก</strong></td>
                        <td>&nbsp; </td>
                        <td>&nbsp;</td>
                        <td><strong>กอง</strong></td>
                        <td>&nbsp; </td>
                        <td><strong>ฝ่าย</strong></td>
                        <td>&nbsp; </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td><strong>สมรรถนะประจำตำแหน่ง (Job Competency)</strong></td>
                        <td><strong>คะแนนที่คาดหวัง</strong></td>
                        <td><strong>ผลการประเมิน</strong>&nbsp;</td>
                        <td><strong>Competency Gap</strong>&nbsp;</td>
                      </tr>
                                            <tr>
                        <td><strong>1. Core Competency</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_10" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;1.1 ปริมาณงานที่ทำได้ตามที่ได้รับมอบหมาย</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_10" id="score_max_min_10" class="form-control" 
                        onChange="changeTest_10(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                  </select>
							  <script>
                                 function changeTest_10(obj){
                                       var inputName = 'score_max_min_10';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_10	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_10); 
										//$('#list_score_1_10').html(score_10); 
										//test(10);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_1_10"></span>
                              </div>
                          </td>
                        <td align="right">5</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_11" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;1.2 คุณภาพของผลงาน (ความถูกต้อง/ครบถ้วน/ความประณีต)</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_11" id="score_max_min_11" class="form-control" 
                        onChange="changeTest_11(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                  </select>
							  <script>
                                 function changeTest_11(obj){
                                       var inputName = 'score_max_min_11';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_11	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_11); 
										//$('#list_score_1_11').html(score_11); 
										//test(11);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_1_11"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_12" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;1.3  ระยะเวลาที่ใช้ (ความรวดเร็ว/ทันเวลา)</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_12" id="score_max_min_12" class="form-control" 
                        onChange="changeTest_12(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                  </select>
							  <script>
                                 function changeTest_12(obj){
                                       var inputName = 'score_max_min_12';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_12	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_12); 
										//$('#list_score_1_12').html(score_12); 
										//test(12);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_1_12"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_13" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;1.4  ทรัพยากรที่ใช้ (การประหยัด/ความคุ้มค่าของการใช้ทรัพยากร)</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_13" id="score_max_min_13" class="form-control" 
                        onChange="changeTest_13(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                  </select>
							  <script>
                                 function changeTest_13(obj){
                                       var inputName = 'score_max_min_13';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_13	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_13); 
										//$('#list_score_1_13').html(score_13); 
										//test(13);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_1_13"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>

                                                      <div class="list_count">
                                <span id="list_count_1"></span>
                              </div>
                              <span id="list_count_test_1"></span>
           					                         <tr>
                        <td><strong>2. Functional Competency</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_15" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2.1 ความรู้ความสามารถทักษะในงาน (ความรอบรู้ในงาน</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_15" id="score_max_min_15" class="form-control" 
                        onChange="changeTest_15(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                      <option value="24">24</option>
                                                      <option value="25">25</option>
                                                      <option value="26">26</option>
                                                      <option value="27">27</option>
                                                      <option value="28">28</option>
                                                      <option value="29">29</option>
                                                      <option value="30">30</option>
                                                      <option value="31">31</option>
                                                      <option value="32">32</option>
                                                      <option value="33">33</option>
                                                      <option value="34">34</option>
                                                      <option value="35">35</option>
                                                      <option value="36">36</option>
                                                      <option value="37">37</option>
                                                      <option value="38">38</option>
                                                      <option value="39">39</option>
                                                      <option value="40">40</option>
                                                  </select>
							  <script>
                                 function changeTest_15(obj){
                                       var inputName = 'score_max_min_15';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_15	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_15); 
										//$('#list_score_2_15').html(score_15); 
										//test(15);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_2_15"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_16" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2.2 การตัดสินใจ การแก้ปัญหา</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_16" id="score_max_min_16" class="form-control" 
                        onChange="changeTest_16(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                      <option value="24">24</option>
                                                  </select>
							  <script>
                                 function changeTest_16(obj){
                                       var inputName = 'score_max_min_16';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_16	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_16); 
										//$('#list_score_2_16').html(score_16); 
										//test(16);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_2_16"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_17" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2.3 การทำงานเป็นทีม</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_17" id="score_max_min_17" class="form-control" 
                        onChange="changeTest_17(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                      <option value="24">24</option>
                                                      <option value="25">25</option>
                                                      <option value="26">26</option>
                                                  </select>
							  <script>
                                 function changeTest_17(obj){
                                       var inputName = 'score_max_min_17';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_17	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_17); 
										//$('#list_score_2_17').html(score_17); 
										//test(17);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_2_17"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                       <!--<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: </p>
<p>Message:  </p>
<p>Filename: </p>
<p>Line Number: </p>

</div>-->                      <input name="ddl[]" id="ddl[]" type="hidden" value="score_max_min_18" />
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;2.4 การประสานงานกับหน่วยงานภายในและภายนอกองค์กร</td><!--onChange="changeTest_(this)"-->
                        <td align="right">10</td>
                        <td >
                        <select name="score_max_min_18" id="score_max_min_18" class="form-control" 
                        onChange="changeTest_18(this)">
                          <option value="">กรุณาเลือก</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                  </select>
							  <script>
                                 function changeTest_18(obj){
                                       var inputName = 'score_max_min_18';
 										//alert(obj.options[obj.selectedIndex].value);
										var score_18	=	obj.options[obj.selectedIndex].value;
										var inputValue = obj.options[obj.selectedIndex].value;
 										//$('#put_sum_score').html(score_18); 
										//$('#list_score_2_18').html(score_18); 
										//test(18);
										//test_test(inputName,inputValue);
                                }
                             </script>
                              <div class="list_score">
                                <span id="list_score_2_18"></span>
                              </div>
                          </td>
                        <td align="right">10</td>
                      </tr>
                                                      <div class="list_count">
                                <span id="list_count_2"></span>
                              </div>
                              <span id="list_count_test_2"></span>
                                                      <div class="list_count">
                                <span id="list_count_4"></span>
                              </div>
                              <span id="list_count_test_4"></span>
           					     
                        <div class="score_max" style="display:none;">
                        	<span id="get_score_max">4</span>
                            
                        </div>
                        
                                              
                      <tr>
                        <td colspan="3" align="right">
                        	<!--<div class="ge_score">
                            	รวมคะแนนประเมิน = <span id="put_sum_score"></span> คะแนน
                        	</div>-->
                         </td>
                      </tr>
                    </table></td>
                  </tr>
                   
                  <tr>
                    <td align="right"><div style="display:none">
                    	<input type="text" name="eval" id="eval" value="1" />
                        <input type="text" name="staffID" id="staffID" value="23015" /></div>
                    	<button type="submit" class="btn btn-success">ขั้นตอนถัดไป &gt;&gt;</button>
                      	&nbsp;
                      	<button type="button" class="btn btn-warning">ยกเลิก</button>
                     	<!--<input name="eval" id="eval" type="hidden" value="" /> -->
                    </td>
                  </tr>
                </table>
              </form>
            </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 

 	
  
   var score_max     = $('.score_max').find('#get_score_max').text();	
   //alert(score_max);
	function test(sum_now){
		var form = document.getElementById("myForm"),
      	  inputs = form.getElementsByTagName("input"),
		  arr = [];
			test  = 0;  
		  for(var i=0, len=inputs.length; i<len; i++){
			if(inputs[i].type === "hidden"){
			  arr.push(inputs[i].value);
			  xx =  inputs[i].value;
			  test += $('#'+xx).val();
				//alert(test);
			}
		  }
		  
		  console.log(arr);
		
		 //alert(sum_now);
		 //var test = new Array(sum_now)
	  var put_sum_score 		= $('.ge_score').find('#put_sum_score').text();

     // Max Gruop  //
		// var score_max     = $('.score_max').find('#get_score_max').text();
		 //alert(score_max);
		 //result =  (parseFloat(put_sum_score)+parseFloat(sum_now)); 
      /*for (var i = 1; i <= score_max; i++) {
		alert(i);
        var xx     = $('.list_score').find('#list_score_'+score_max+'_'+sum_now).text();//list_score_1_10
        var total     = $('.list_score').find('#list_score_'+score_max+'_'+sum_now).text();//list_score_1_10
		var list_count_test_4      = $('.list_count').find('#list_count_'+i).text();//list_count_1
        //alert(score_max);alert(sum_now);
		var somthing	 = list_count_test_4 ;	
		alert(xx); 
		$('#put_sum_score_test').html(list_score_test); 
        $('#list_count_test_4').html(somthing); 
     };*/
		
	}

  function test_test(inputName,inputValue){
      //$.ajax({
        
      $.ajax({
            url:"http://dpo-ehr.smmms.com/rate/sum",
            type:'POST',
            data:{
                  GetinputName :inputName,
                  GetinputValue :inputValue,
                  eval:1
            },
            error:function(data){

            },
            success:function(data){
                console.log(data); 
            }
      });


  }
 
 
</script>
                         
                </section>
        </section>
        <!--main content end-->

    </section>	

    

</body>
</html>
	