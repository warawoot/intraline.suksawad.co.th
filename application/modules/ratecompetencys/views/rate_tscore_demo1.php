<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล </h3>
            </header>
            <div class="panel-body">
                	 <form action="<?php echo base_url('rate/xx') ?>" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">เลือกปีงบประมาณ</label>
                                <div class="col-sm-9">
                                    <?php 
                                        echo form_dropdown('evalYear',$eval_date_year,$year,'class="form-control" onchange="changeRate(this);" id="evalYear"  ');
                                    ?>
                                </div>
                             </div>
                             <div class="form-group">
                              <label class="control-label col-sm-3">รอบที่</label>
                              	<div class="col-sm-9">
                                <?php if($roundRound ==''){?>
                                	<span id="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE">
                                         <select name="evalRound" id="evalRound" class="form-control">
                                            <option value="" selected="selected">กรุณาระบุรอบด้วย</option>
                                         </select>
                                    </span>
                                    <?php
									}else{
                                    	echo $roundRound;
									}
									?>
                                 </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">หน่วยงาน</label>
                                <div class="col-sm-9">
                                     <?php  
 											echo form_dropdown('agencies',$dropdown_org_chart,$agencies,'class="form-control"  id="agencies"');
                                      ?>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">ระดับเริ่มต้น</label>
                                <div class="col-sm-9">
                                     	<select name="eval_date" class="form-control" >
                                                <option value="">กรุณาระบุด้วย</option>
                                                <option value="1" selected="selected">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                        </select>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">ระดับสิ้นสุด</label>
                                <div class="col-sm-9">
                                      	<select name="eval_date" class="form-control" >
                                            <option value="">กรุณาระบุด้วย</option>
                                            <option value="1" selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>           
                                  </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">ค้นหา</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div> 
 
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3> </h3>
            </header>
            <div class="panel-body">  
			   	﻿<table class="xcrud-list table table-striped table-hover table-bordered">
				<tr class="xcrud-th">
					<th style="width:150px">จำนวนคน</th>
					<td align="left">
						<?php 
							$n		=	 $get_count_evalPerson['person'];
							echo $n	; 
					   ?>
                    </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">คะแนนเฉลี่ย (xi) </th>
					<td align="left">
						<?php 
							$score		= $get_count_evalScore['score']; 
							$xiAverage	= $score / $n; 
							echo number_format($xiAverage , 0, '.', '')  ;
							
						?>                          
                       </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">(&Sigma;(xi-x)<sup>2</sup>) / N </th>
					<td align="left">
                    	<div class="xcrud-overlay-sigma" >
                            	<span id="sum_squ_sigma">&nbsp;</span>
                        </div>
                    </td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">SD </th>
					<td align="left"> 
                    	<div class="xcrud-overlay-sd">
                            	<span id="sum_ad_x">&nbsp;</span>
                        </div>
                    </td>
				</tr>
			   </table>
            </div>
            <div class="panel-body">  
                <table class="xcrud-list table table-striped table-hover table-bordered">
                        <thead>
                            <tr class="xcrud-th">
                                <th class="xcrud-num"># รหัสพนักงาน</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.evalRound" class="xcrud-column xcrud-action">ฝ่าย / กอง / แผนก / ชื่อ-นามสกุล</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.startDate" class="xcrud-column xcrud-action">คะแนน</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">เกรด</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">xi-x</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">(xi-x)<sup>2</sup></th>
                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">T-Score</th>
                                
                        </thead>
                        <tbody>
                            <?php 
                             $num = 1 ;
                             $eval_get	= $this->rate_tscore_calculate->calculate_emp3($var_agencies,$assingID,144); 
                             if(count($eval_get)>0){
                                foreach ($eval_get as $key => $item) {
                             ?>
                                       <tr class="xcrud-row xcrud-row-0"> 
                                            <td class="xcrud-current xcrud-num"><span class="xcrud-num">#</span> <?php echo $item->person_id;?></td>
                                            <td><?php echo $item->person;?>&nbsp;<?php echo $item->personLName;?></td>
                                            <td><?php //echo $eval_rate_tscore_calculate->s_evalScore;?></td>
                                            <td>xx</td>
                                            <td>xx</td>
                                            <td>xx</td>
                                            <td>xx</td>
                                        </tr>
                                   <?php 
                                   $eval_get_tsocre	= $this->rate_tscore_calculate->calculate_emp22($item->orgID,$assingID,144); 
                                   foreach ($eval_get_tsocre as $key => $item2) {
                                   ?>
                                       <tr class="xcrud-row xcrud-row-0"> 
                                            <td class="xcrud-current xcrud-num"><span class="xcrud-num">#</span>
                                            <?php //echo ++$key;?> <?php echo $item2->group_id;?></td>
                                            <td><?php echo $item2->groups;?>&nbsp;<?php echo $item2->groupsLName;?></td>
                                            <td>xx </td>
                                            <td>xx</td>
                                            <td>xx</td>
                                            <td>xx</td>
                                            <td>xx</td>
                                        </tr>
                                        <?php 
                                        $eval_get_tsocress	= $this->rate_tscore_calculate->calculate_emp4($item2->orgID,$assingID,144); 
                                        foreach ($eval_get_tsocress as $key => $item4) {
                                      
                                            $eval_get_tsocress44	= $this->rate_tscore_calculate->calculate_emp44($item4->orgID,$assingID,144); 
                                             foreach ($eval_get_tsocress44 as $key => $item5) {
                                             ?>
                                             <tr class="xcrud-row xcrud-row-0"> 
                                                <td class="xcrud-current xcrud-num"><span class="xcrud-num">#</span>
                                                <?php //echo ++$key;?> <?php echo $item5->department_id;?></td>
                                                <td><?php echo $item5->department;?>&nbsp;<?php echo $item5->departmentLName;?></td>
                                                <td> xx </td>
                                                <td>xx</td>
                                                <td>xx</td>
                                                <td>xx</td>
                                                <td>xx</td>
                                             </tr>
                                            <?php } 
                                         } 
                                  	}  
                              	}
                             }else{
                            
                            ?>	   
                                <tr class="xcrud-row xcrud-row-0"> 
                                    <td colspan="7" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
                                 </tr>
                            <?php } ?>          
                           </tbody>
                        <tfoot>
                       </tfoot>
                    </table>
            </div> 
        </section>
    </div>
<div>

<script type="application/javascript">
	function changeRate(obj){
    	//alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/recentdll') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   var obj = jQuery.parseJSON(response); 
				   //alert(response);
				   //console.log(obj); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(obj);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
				    location.replace('<?php echo base_url().'rate/recent' ?>');
				}
		});	
  	}
	
	var squares       = $('.xcrud-overlay').find('#sum_squ').text();	
	var sq_sum_all	  = squares / <?php echo $n;?>;
	var squares_sum	  = $('#sum_squ_sigma').html(sq_sum_all.toFixed(2));	
	 
	var sd			  = <?php echo $sum_squ;?> / <?php echo $n;?>;
 	var sd_sum		  = $('#sum_ad_x').html(Math.sqrt(sd).toFixed(2));
    var sd_sum_x	  = Math.sqrt(sd).toFixed(2);
	// Get Parameter SD 
	var XcrudSD  = "<?php echo $varsd;?>";
    if (XcrudSD == '') {
		location.href = '<?php echo base_url() ;?>rate/tscore?sd='+sd_sum_x+'&eval_date=<?php echo $var_eval_date; ?>&agencies=<?php echo $var_agencies;?>';
    }
				 
</script>
 
 