<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล </h3>
            </header>
            <div class="panel-body">
                	 <form action="<?php echo base_url('rate/xx') ?>" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">เลือกปีงบประมาณ</label>
                                <div class="col-sm-9">
                                    <?php 
                                        echo form_dropdown('evalYear',$eval_date_year,$var_evalYear,'class="form-control" onchange="changeRate(this);" id="evalYear"  ');
                                    ?>
                                </div>
                             </div>
                             <div class="form-group">
                              <label class="control-label col-sm-3">รอบที่</label>
                              	<div class="col-sm-9">
                                <?php if($roundRound ==''){?>
                                	<span id="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE">
                                         <select name="evalRound" id="evalRound" class="form-control">
                                            <option value="" selected="selected">กรุณาระบุรอบด้วย</option>
                                         </select>
                                    </span>
                                    <?php
									}else{
                                    	echo $roundRound;
									}
									?>
                                 </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">หน่วยงาน</label>
                                <div class="col-sm-9">
                                     <?php  
 											echo form_dropdown('agencies',$dropdown_org_chart,$var_agencies,'class="form-control"  id="agencies"');
                                     ?>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">ระดับเริ่มต้น</label>
                                <div class="col-sm-9">
                                     	<select name="eval_date" class="form-control" >
                                                <option value="">กรุณาระบุด้วย</option>
                                                <option value="1" selected="selected">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                        </select>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">ระดับสิ้นสุด</label>
                                <div class="col-sm-9">
                                      	<select name="eval_date" class="form-control" >
                                            <option value="">กรุณาระบุด้วย</option>
                                            <option value="1" selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>           
                                  </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">ค้นหา</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div> 
 
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3> </h3>
            </header>
            <div class="panel-body">  
			  
            </div>
            <div class="panel-body">  
                  <table class="xcrud-list table table-striped table-hover table-bordered">
                        <thead>
                            <tr class="xcrud-th">
                                <th class="xcrud-num">#</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.evalYear" class="xcrud-column xcrud-action">รหัสพนักงาน</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.evalRound" class="xcrud-column xcrud-action">ฝ่าย / กอง / แผนก / ชื่อ-นามสกุล</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.startDate" class="xcrud-column xcrud-action">ประเมินผลปฏิบัติงาน</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ประเมิน KPI</th>
                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ประเมิน Competency </th>
                            </tr>    
                        </thead>
                        <tbody>
                            <?php 
                             $num = 1 ;
                             
                             $eval_get	= $this->rate_tsocre->rate_emp_party($var_agencies,$assingID); 
                             if(count($eval_get)>0){
                                foreach ($eval_get as $key => $item) {
                                    # code...
                                    //$eval	= $this->rate_model->get_eval($item->person_id,$round);
                                     
                            ?>
                                <tr class="xcrud-row xcrud-row-0" style="background-color:#999"> 
                                    <td class="xcrud-current xcrud-num"><i class="fa fa-users"></i> </td>
                                    <td><?php //echo $item->orgID; ?></td>
                                    <td><i class="fa fa-angle-double-right"></i>&nbsp;
                                    <span style="color:#00F"><strong>ฝ่าย</strong> [ <?php echo $item->orgShortName .  '&nbsp;'.$item->orgName; ?> ]</span>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                 </tr> 
                                 <tr class="xcrud-row xcrud-row-0"> 
                                    <td class="xcrud-current xcrud-num"><?php //echo ++$key;?></td>
                                    <td><?php echo $item->person_id;?></td>
                                    <td>&nbsp;&nbsp; <i class="fa fa-user"></i>&nbsp;<?php echo $item->person;?>&nbsp;<?php echo $item->personLName;?></td>
                                    <td> 
                                           <?php
                                            $get_eval_notes				 = $this->rate_model->get_eval_notes($item->person_id);
                                            $eval_person				 = $this->rate_model->get_eval_count_people($item->person_id,$get_eval_notes['evalFormID']);
                                            $count_proples_person		 = $eval_person['count_proples'];
                                            $eval_subject_group_subject  = $this->rate_model->get_eval_count_subject_group_subject($get_eval_notes['evalFormID']);
                                            $subject_group_subject 		= $eval_subject_group_subject['subject_group_subject'];
                                            if($get_eval_notes	== NULL){
                                            ?>
                                            <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->person_id ;?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                                            <?php }else{
                                                    $get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
                                                     
                                            ?>
                                                <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $get_eval_nedit['empID'] ?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
                                            <?php } ?>
                                             </td>
                                            <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/profile');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->person_id ?>" title="">ประเมิน</a></td>
                                            <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->person_id ?>" title="">ประเมิน</a></td>
                                        </tr>
                                 <tr > 
                                    <td colspan="6" class="">&nbsp;</td>
                                 </tr>
                        <?php 
                                   $eval_get_tsocre	= $this->rate_tsocre->rate_emp_division($item->orgID,$assingID); 
                                   foreach ($eval_get_tsocre as $key => $item2) {
                                   ?>
                                    <tr class="xcrud-row xcrud-row-0" style="background-color:#CCC;"> 
                                        <td class="xcrud-current xcrud-num">&nbsp;</td>
                                        <td><?php //echo $item2->orgID; ?></td>
                                        <td>&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i>&nbsp;<span style="color:#099"><strong><?php echo ++$key;?>. กอง</strong> [ <?php echo $item2->orgShortName .  '&nbsp;'.$item2->orgName; ?> ]</span></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                     </tr>
                                        <tr class="xcrud-row xcrud-row-0"> 
                                            <td class="xcrud-current xcrud-num"><?php //echo ++$key;?></td>
                                            <td><?php echo $item2->group_id;?></td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>&nbsp;<?php echo $item2->groups;?>&nbsp;<?php echo $item2->groupsLName;?></td>
                                            <td> 
                                            <!--<a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>" title="">ประเมิน</a>-->
                                           <?php
                                            $get_eval_notes	=	$this->rate_model->get_eval_notes($item2->group_id);
                                            $eval_group	= $this->rate_model->get_eval_count_people($item2->group_id,$get_eval_notes['evalFormID']);
                                            $count_proples_group	=   $eval_group['count_proples'];
                                            if($get_eval_notes	== NULL){
                                            ?>
                                            <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                                            <?php }else{
                                                    $get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
                                                     
                                            ?>
                                                <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $get_eval_nedit['empID'] ?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
                                            <?php } ?>
                                            </td>
                                            <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/profile');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>" title="">ประเมิน</a></td>
                                            <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>" title="">ประเมิน</a></td>
                                        </tr>
                                         <?php 
                                                        $eval_get_tsocress	= $this->rate_tsocre->rate_emp_department($item2->orgID,$assingID); 
                                                        foreach ($eval_get_tsocress as $key => $item4) {
                                                            
                                                            ?>
                                                            <tr class="xcrud-row xcrud-row-0" style="background-color:#C0C3C5;"> 
                                                                <td class="xcrud-current xcrud-num"><?php //echo ++$key;?></td>
                                                                <td><?php //echo $item4->orgID; ?></td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i>  <span style="color:#C30"><strong><?php  echo ++$key;?>.) แผนก</strong> [ <?php echo $item4->orgShortName .  '&nbsp;'.$item4->orgName; ?> ]</span></td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <?php 
                                                            $eval_get_tsocress44	= $this->rate_tsocre->rate_emp_department_person($item4->orgID,$assingID); 
                                                            foreach ($eval_get_tsocress44 as $key => $item5) {
                                                            ?>
                                                                    <tr class="xcrud-row xcrud-row-0"> 
                                                                        <td class="xcrud-current xcrud-num"><?php //echo ++$key;?></td>
                                                                        <td><?php echo $item5->department_id;?></td>
                                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>&nbsp;<?php echo $item5->department;?>&nbsp;<?php echo $item5->departmentLName;?></td>
                                                                        <td>  
                                                            <?php
                                                            $get_eval_notes	=	$this->rate_model->get_eval_notes($item5->department_id);
                                                            $eval_departmen	= $this->rate_model->get_eval_count_people($item5->department_id,$get_eval_notes['evalFormID']);
                                                            $count_proples_departmen	=   $eval_departmen['count_proples'];
                                                             
                                                            if($get_eval_notes	== NULL){
                                                            ?>
                                                            <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item5->department_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                                                            <?php }else{
                                                                    $get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
                                                                     
                                                            ?>
                                                                <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $get_eval_nedit['empID'] ?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
                                                            <?php } ?>
                                                                        </td>
                                                            <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/profile');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item5->department_id ?>" title="">ประเมิน</a></td>
                                                            <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item5->department_id ?>" title="">ประเมิน</a></td>
                                                                    <!--</tr>-->
                                                            <?php }?>
                                                             
                                                            <!--zz-->
                                                             <?php 
                                                        $eval_get_tsocress5	= $this->rate_tsocre->rate_employee_org($item5->orgID,$assingID); 
                                                         
                                                        foreach ($eval_get_tsocress5 as $key => $item6) {
                                                            
                                                            ?>
                                                            <tr class="xcrud-row xcrud-row-0" style="background-color:#E6E6E6;"> 
                                                                <td class="xcrud-current xcrud-num">&nbsp; </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i><span style="color:#090;"><strong> พนักงานในแผนก</strong></span>
                                                                </td>
                                                                <td>&nbsp; </td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <?php 
                                                                $eval_get_tsocress55	= $this->rate_tsocre->rate_employee_person($item6->orgID,$assingID); 
                                                                if(count($eval_get_tsocress55)>0){
                                                                    foreach ($eval_get_tsocress55 as $key => $item7) {
                                                            ?>
                                                                    <tr class="xcrud-row xcrud-row-0"> 
                                                                        <td class="xcrud-current xcrud-num"> </td>
                                                                        <td>&nbsp;</td>
                                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <i class="fa fa-user"></i>
                                                                        &nbsp;<?php echo $item7->staffPreName; ?><?php echo $item7->staffFName;?> <?php echo $item7->staffLName;?>&nbsp; 
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                $get_eval_notes	=	$this->rate_model->get_eval_notes($item7->employee_id);
                                                                                $eval_departmen	= $this->rate_model->get_eval_count_people($item5->department_id,$get_eval_notes['evalFormID']);
                                                                                $count_proples_departmen	=   $eval_departmen['count_proples'];
                                                                                if($get_eval_notes	== NULL){
                                                                             ?>
                                                                                <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item7->employee_id; ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                                                                                <?php }else{
                                                                                        $get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
                                                                                ?>
                                                                                    <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $get_eval_nedit['empID'] ?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
                                                                                <?php } ?>
                                                                        </td> 
                                                                        <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/profile');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item7->employee_id; ?>" title="">ประเมิน</a></td>
                                                                        <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item7->employee_id; ?>" title="">ประเมิน</a></td>
                                                                   </tr>
                                                                    
                                                              <?php }?>
                                                             
                                                           <?php }else{
                                                        ?>
                                                            <tr class="xcrud-row xcrud-row-0">
                                                                <td class="xcrud-current xcrud-num"></td>
                                                                <td>&nbsp;</td>
                                                                <td align="center">- ไม่พบพนักงานในแผนก -</td>
                                                                <td>                                                        
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        <?php   
                                                            }?>
                                                            
                                                        <?php } ?></tr>
                                                            <!--zz-->
                                                        <?php }?>
                                                        
                                                 <?php } ?>
                                                  
                                            <?php }
             
                                             }else{
                                            ?>	   
                                <tr class="xcrud-row xcrud-row-0"> 
                                    <td colspan="6" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
                                 </tr>
                            <?php } ?>          
                           </tbody>
                        <tfoot>
                       </tfoot>
                    </table>
            </div> 
        </section>
    </div>
<div>

<script type="application/javascript">
	function changeRate(obj){
    	//alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/recentdll') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   var obj = jQuery.parseJSON(response); 
				   //alert(response);
				   //console.log(obj); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(obj);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
				    location.replace('<?php echo base_url().'rate/recent' ?>');
				}
		});	
  	}
	
	 
</script>
 
 