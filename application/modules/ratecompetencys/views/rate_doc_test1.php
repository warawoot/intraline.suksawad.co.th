<style type="text/css">
	.form-control {
 	  color: #343232;
	 
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล</h3>
            </header>
            <div class="panel-body">
              <form id="form1" name="form1" method="post" action="<?php echo base_url() ?>rate/docs">
                <table class="table" style="background-color: #fff; font-size:14px;">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td colspan="7" align="center" style=" font-size:16px;"><strong> แบบประเมินผลการปฏิบัติงานประจำปี </strong> <?php echo $get_evalYear;?> <strong> ครั้งที่ </strong><?php echo $get_evalRound;?> ( <strong>วันที่</strong> <?php echo $get_startDate;?> - <?php echo $get_endDate;?>) </td>
                      </tr>
                      <tr>
                        <td colspan="7" align="center"><strong> [ </strong><?php echo $get_evalNameText;?><strong> ] </strong></td>
                      </tr>
                      <tr>
                        <td><strong>ชื่อ-นามสกุล</strong></td>
                        <td><?php  echo $get_staffPreName . $get_staffFName .'&nbsp;&nbsp;'. $get_staffLName;?></td>
                        <td>&nbsp;</td>
                        <td><strong>ตำแหน่ง</strong></td>
                        <td><?php echo $get_positionName;?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>แผนก</strong></td>
                        <td>xx</td>
                        <td>&nbsp;</td>
                        <td><strong>กอง</strong></td>
                        <td>xx</td>
                        <td><strong>ฝ่าย</strong></td>
                        <td>xxx</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td><strong>โปรดวงกลมล้อมรอบตัวเลขคะแนนที่ประเมินในแต่ละปัจจัย</strong></td>
                        <td>&nbsp;</td>
                        <td><strong>ระดับคะแนนที่ให้</strong></td>
                      </tr>
                      <tr>
                        <td><strong>หัวข้อปัจจัยที่ประเมิน</strong></td>
                        <td>&nbsp;</td>
                        <td><strong>คะแนน</strong>&nbsp;</td>
                      </tr>
                      <?php 
                      if(is_array($get_eval_group_subject)){ 
         			     $num = 1;
                         foreach ($get_eval_group_subject as $key => $value) {      # code... 
        					$nums	=	$num++;
        					$sum_eval_subject = $this->rate_model->get_sum_eval_subject($value->evalGroupSubjectID);
                      ?>
                      <tr>
                        <td><strong><?php echo $value->evalGroupSubjectText;?></strong></td>
                        <td>&nbsp;</td>
                        <td><strong>น้ำหนักของปัจจัยที่ <?php echo $nums;?> = [ </strong><?php echo $sum_eval_subject['maxScore']; ?><strong> ] คะแนน</strong>&nbsp;</td>
                      </tr>
                      <?php 
						  $get_apen_name = $this->rate_model->get_eval_subject($value->evalGroupSubjectID);
						  $num_score	=	1;
						   foreach ($get_apen_name as $key => $item) {  
                             # code...
                      ?>
                      <tr style="font-size:12px; text-align:left;">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $item->evalSubjectText; ?></td><!--onChange="changeTest_<?php //echo $item->evalSubjectID; ?>(this)"-->
                        <td>&nbsp;</td>
                        <td>
                        <select name="score_max_min_<?php echo $item->evalSubjectID; ?>" id="score-max-min-<?php echo $item->evalSubjectID; ?>" class="form-control" onChange="changeTest_<?php echo $item->evalSubjectID; ?>(this)">
                          <?php for ($i=$item->minScore; $i <= $item->maxScore; $i++) {   ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                          <?php  } ?>
                        </select>
							 <script>
                                function changeTest_<?php echo $item->evalSubjectID; ?>(obj){
									//var score_max_in_<?php echo $item->evalSubjectID; ?> 		= new Array(obj.options[obj.selectedIndex].value);
									//alert(score_max_in_<?php //echo $item->evalSubjectID; ?>);
									//alert(obj.options[obj.selectedIndex].value);
									var score_<?php echo $item->evalSubjectID; ?>	=	obj.options[obj.selectedIndex].value;
									//var sum_now		=	score_<?php //echo $item->evalSubjectID; ?>;
									//test(score_max_in_<?php //echo $item->evalSubjectID; ?> );
									//alert(cars);
									$('#put_sum_score').html(score_<?php echo $item->evalSubjectID; ?>); 
                                }
                             </script><span id="score_<?php echo $item->evalSubjectID; ?>"></span>
                          </td>
                      </tr>
                       <?php     }
                          }
						  ?>  
                      <?php }else{
                      ?>
                      <tr>
                        <td colspan="3" align="center">ไม่พบข้อมูล</td>
                      </tr>
                      <?php  }   ?>
                      <tr>
                        <td colspan="3" align="right">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="3" align="right">รวมคะแนนประเมิน = <span id="put_sum_score">[sum[score]]</span> คะแนน</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0">
                      <tr>
                        <td><strong>1.ระดับผลการประเมิน &nbsp;&nbsp;</strong></td>
                        <td><strong>ระดับ X &nbsp;&nbsp;&nbsp;&nbsp;( </strong><?php echo $item->minScore; ?> - <?php echo $item->maxScore; ?> <strong>)</strong></td>
                      </tr>
                      <tr>
                        <td><strong>2.ควรได้รับการพัฒนาเพิ่มเติมในเรื่อง  </strong></td>
                        <td>
                          <textarea name="evalNote1" id="evalNote1" cols="45" rows="5"></textarea></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>3. ข้อคิดเห็นเพิ่มเติม </strong></td>
                        <td>
                          <textarea name="evalNote2" id="evalNote2" cols="45" rows="5"></textarea></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td align="right">
                    	<button type="submit" class="btn btn-success">บันทึก</button>
                      	&nbsp;
                      	<button type="button" class="btn btn-warning">ยกเลิก</button>
                     	<input name="eval" id="eval" type="hidden" value="<?php echo $eval;?>" /> 
                     </td>
                  </tr>
                </table>
              </form>
            </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 
  	
	function test(sum_now){
		alert(Math.floor(sum_now));
	}
 
 
</script>
 