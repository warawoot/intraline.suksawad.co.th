<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>รายงานการประเมินผล Grade / T-Score</h3>
            </header>
            <div class="panel-body">
                	<form action="<?php echo base_url('rate/tscore') ?>" method="post" enctype="multipart/form-data">
                    		<div class="form-group">
                                <label class="control-label col-sm-3">เลือกปีงบประมาณ/รอบ</label>
                                <div class="col-sm-9">
                                    <?php 
                                        echo form_dropdown('eval_date',$eval_date,'','class="form-control" ');
                                    ?>
                                </div>
                             </div>
                             <div class="form-group">
                                <label class="control-label col-sm-3">หน่วยงานแม่</label>
                                <div class="col-sm-9">
                                    <select class="xcrud-input form-control form-control" data-required="1" data-type="select" name="agencies" id="agencies" maxlength="11" >
                                         <?php  
                                            echo $dropdown_org_chart;
                                         ?>
                                    </select>
                                     
                                </div>
                            </div>
                            <div class="form-group">
                                  <button type="submit" class="btn btn-success">ค้นหา</button>
                            </div>
                    </form>
            </div>
         </section>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3> </h3>
            </header>
			   ﻿<table class="xcrud-list table table-striped table-hover table-bordered">
				<tr class="xcrud-th">
					<th style="width:150px">จำนวนคน</th>
					<td align="left"><?php echo $get_count_evalScore;?></td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">คะแนนเฉลี่ย (xi)</th>
					<td align="left">67.50</td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">(&Sigma;(xi-x)<sup>2</sup>) / N</th>
					<td align="left">313.25</td>
				</tr>
				<tr class="xcrud-th">
					<th style="width:150px">SD</th>
					<td align="left">17.698870</td>
				</tr>
			</table>
			<br/><br/>
            <div class="panel-body">
                <link href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url(); ?>xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                        <div class="xcrud">
                            <div class="xcrud-container">
                                <div class="xcrud-ajax">
                                    
                                    <div class="xcrud-list-container">
                                    
                                     ﻿<table class="xcrud-list table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr class="xcrud-th">
                                                <th class="xcrud-num">#</th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.evalYear" class="xcrud-column xcrud-action">รหัสพนักงาน</th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.evalRound" class="xcrud-column xcrud-action">ชื่อ-นามสกุล</th>
                                                <th align="center">คะแนน</th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">เกรด</th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">xi-x</th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">(xi-x)<sup>2</sup></th>
                                                <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">T-Score</th>
                                                
                                        </thead>
                                        <tbody>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">1</td>
                                                    <td>24005</td>
                                                    <td>นายสุมิตร&nbsp;ลิกขะไชย</td>
                                                    <td align="right">90</td>
                                                    <td align="center">A</td>
                                                    <td align="right">22.5</td>
                                                    <td align="right">506.25</td>
                                                    <td align="right">62.712676</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">2</td>
                                                    <td>35016</td>
                                                    <td>นางสาววรรณา&nbsp;พึ่งเพียร</td>
                                                    <td align="right">80</td>
                                                    <td align="center">A</td>
                                                    <td align="right">12.5</td>
                                                    <td align="right">156.25</td>
                                                    <td align="right">57.062598</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">1</td>
                                                    <td>30015</td>
                                                    <td>นาง วริลยา โตชูวงศ์</td>
                                                    <td align="right">72</td>
                                                    <td align="center">B</td>
                                                    <td align="right">4.5</td>
                                                    <td align="right">20.25</td>
                                                    <td align="right">52.542535</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">2</td>
                                                    <td>29003</td>
                                                    <td>นาย โกเพชร ทองแสง</td>
                                                    <td align="right">56</td>
                                                    <td align="center">D</td>
                                                    <td align="right"><a style="color:red">-11.5</a></td>
                                                    <td align="right">132.25</td>
                                                    <td align="right">43.502410</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">1</td>
                                                    <td>37003</td>
                                                    <td>นาง อำนาจ มานา</td>
                                                    <td align="right">66</td>
                                                    <td align="center">C</td>
                                                    <td align="right"><a style="color:red">-1.5</a></td>
                                                    <td align="right">2.25</td>
                                                    <td align="right">49.152488</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">2</td>
                                                    <td>39026</td>
                                                    <td>นาง แน่งน้อย สุทธศรี</td>
                                                    <td align="right">79</td>
                                                    <td align="center">B</td>
                                                    <td align="right">11.5</td>
                                                    <td align="right">132.25</td>
                                                    <td align="right">56.497590</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">1</td>
                                                    <td>29022</td>
                                                    <td>นาย ไพทูลย์ โตอ่อน</td>
                                                    <td align="right">88</td>
                                                    <td align="center">A</td>
                                                    <td align="right">20.5</td>
                                                    <td align="right">420.25</td>
                                                    <td align="right">61.582660</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">2</td>
                                                    <td>22011</td>
                                                    <td>นาง นงเยาว์ แก่นจักร์</td>
                                                    <td align="right">98</td>
                                                    <td align="center">A</td>
                                                    <td align="right">30.5</td>
                                                    <td align="right">930.25</td>
                                                    <td align="right">67.232739</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">1</td>
                                                    <td>29001</td>
                                                    <td>นาย ล้วน ชัยสงคราม</td>
                                                    <td align="right">42</td>
                                                    <td align="center">F</td>
                                                    <td align="right"><a style="color:red">-25.5</a></td>
                                                    <td align="right">650.25</td>
                                                    <td align="right">35.592301</td>
                                                 </tr>
                                                <tr class="xcrud-row xcrud-row-0"> 
                                                    <td class="xcrud-current xcrud-num">2</td>
                                                    <td>29010</td>
                                                    <td>นาง ศรีนวน ศึกขุนทด</td>
                                                    <td align="right">54</td>
                                                    <td align="center">D</td>
                                                    <td align="right"><a style="color:red">-13.5</a></td>
                                                    <td align="right">182.25</td>
                                                    <td align="right">42.372394</td>
                                                 </tr>
                                                             
                                           </tbody>
                                        <tfoot>
                                       </tfoot>
                                    </table>
                                    </div>
                                    <!--<div class="xcrud-nav">
                                        <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio">
                                            <button type="button" class="btn btn-default xcrud-action" data-limit="10">10</button>
                                            <button type="button" class="btn btn-default active xcrud-action" data-limit="50">50</button>
                                            <button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button>
                                            <button type="button" class="btn btn-default xcrud-action" data-limit="all">ทั้งหมด</button>
                                        </div>                        
                                    </div>-->
                                </div>
                            <div class="xcrud-overlay" style="display: none;"></div>
                        </div>
       				</div>
					<script src="<?php echo base_url(); ?>xcrud/plugins/jquery.min.js"></script>
                    <script src="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
                    <script src="<?php echo base_url(); ?>xcrud/plugins/jcrop/jquery.Jcrop.min.js"></script>
                    <script src="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
                    <script src="<?php echo base_url(); ?>editors/tinymce/tinymce.min.js"></script>
                    <script src="//maps.google.com/maps/api/js?sensor=false&amp;language=th"></script>
                    <script src="http://maps.gstatic.com/maps-api-v3/api/js/21/9a/intl/th_ALL/main.js"></script>
                    <script src="<?php echo base_url(); ?>xcrud/plugins/xcrud.js"></script>
                    <script src="<?php echo base_url(); ?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script>
                    <script src="<?php echo base_url(); ?>xcrud/languages/timepicker/jquery-ui-timepicker-th.js"></script> 
             </div>
             
        </section>
    </div>
</div>
 
<script type="text/javascript"> 

	
 
 
</script>
 