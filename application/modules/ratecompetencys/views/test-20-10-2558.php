<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมินผล </h3>
            </header>
            <div class="panel-body">
           	  <form action="<?php echo base_url('rate') ?>" method="post" enctype="multipart/form-data">
                  		<div class="form-group">
                              <label class="control-label col-sm-3">เลือกปีงบประมาณ/รอบ</label>
                              <div class="col-sm-9">
                                  <?php 
                                      echo form_dropdown('eval_date',$eval_date,$var_eval_date,'class="form-control" ');
                                  ?>
                              </div>
               			 </div>
                           <div class="form-group">
                              <label class="control-label col-sm-3">หน่วยงาน</label>
                              <div class="col-sm-9">
                                  <!--<select class="xcrud-input form-control form-control" data-required="1" data-type="select" name="agencies" id="agencies" maxlength="11" >
                                       <?php  
                                          //echo $dropdown_org_chart;
                                       ?>
                                  </select>-->
                                  <?php  
 										echo form_dropdown('agencies',$dropdown_org_chart,$var_agencies,'class="form-control"  id="agencies"');
                                   ?>
                              </div>
                          </div>
                          <div class="form-group" ><label class="control-label col-sm-3">&nbsp;</label>
                                <div class="col-sm-9"> &nbsp </div>
                          </div>
       		    		  <div class="form-group" ><label class="control-label col-sm-3">&nbsp;</label>
                                <div class="col-sm-9"> <button type="submit" class="btn btn-success">ค้นหา</button></div>
                          </div>
                         <div class="form-group" ><label class="control-label col-sm-3">&nbsp;</label>
                                <div class="col-sm-9"> <br /> 
                                 <table border="0" class="table" style="border:0px;border-top: 0px solid #ddd;" width="100%">
                                     <tr style="border:0px;border-top: 0px solid #ddd;">
                                        <td width="33%" style="border:0px;border-top: 0px solid #ddd;"> วันที่ครบกำหนดการประเมิน </td>
                                        <td width="67%" style="border:0px;border-top: 0px solid #ddd;"> <?php echo $dateDue;?> </td>
                                      </tr>
                                      <tr style="border:0px;border-top: 0px solid #ddd;">
                                        <td style="border:0px;border-top: 0px solid #ddd;"> ผู้ประเมิน </td>
                                        <td style="border:0px;border-top: 0px solid #ddd;"> 
                                          <select class="form-control" data-type="select" name="dGJsX2V2YWxfZm9ybS5Bc3Nlc1Nvcg--2" maxlength="1">
                                            <option value="0" selected="selected">ไม่กำหนด</option>
                                            <option value="6">ผู้บังคับบัญชาระดับแผนก</option>
                                            <option value="7">ผู้บังคับบัญชาระดับกอง</option>
                                            <option value="8">ผู้บังคับบัญชาระดับฝ่าย</option>
                 							<?php
                                            		if(count($get_dropdown_org_option)>0){
														foreach ($get_dropdown_org_option as $key => $item) {
											?>
                                            				<option value="<?php echo $item->orgID;?>"><?php echo $item->orgName;?></option>
                                            <?php
														}
													}
											?>
                                            <option value="10">รองผู้อำนวยการ</option>
                                            <option value="11">ผู้อำนวยการ</option>
                                          </select> 

                                        </td>
                                      </tr>
                                      <tr style="border:0px;border-top: 0px solid #ddd;">
                                        <td style="border:0px;border-top: 0px solid #ddd;"> ผู้บังคับบัญชาเหนือขึ้นไป </td>
                                        <td style="border:0px;border-top: 0px solid #ddd;"> 
                                          <select class="form-control" data-type="select" name="dGJsX2V2YWxfZm9ybS5Db21tYW5kZXI-2" maxlength="1">
                                            <option value="0" selected="selected">ไม่กำหนด</option>
                                            <option value="6">ผู้บังคับบัญชาระดับแผนก</option>
                                            <option value="7">ผู้บังคับบัญชาระดับกอง</option>
                                            <option value="8">ผู้บังคับบัญชาระดับฝ่าย</option>
                 							<?php
                                            		if(count($get_dropdown_org_option)>0){
														foreach ($get_dropdown_org_option as $key => $item) {
											?>
                                            				<option value="<?php echo $item->orgID;?>"><?php echo $item->orgName;?></option>
                                            <?php
														}
													}
											?>
                                            <option value="10">รองผู้อำนวยการ</option>
                                            <option value="11">ผู้อำนวยการ</option>
                                          </select>
                                        </td>
                                      </tr>
                                      <tr style="border:0px;border-top: 0px solid #ddd;">
                                        <td style="border:0px;border-top: 0px solid #ddd;"> ผู้รับรอง </td>
                                        <td style="border:0px;border-top: 0px solid #ddd;"> 
                                          <select class="form-control" data-type="select" name="dGJsX2V2YWxfZm9ybS5FbmRvcnNlZQ--2" maxlength="1">
                                            <option value="0" selected="selected">ไม่กำหนด</option>
                                            <option value="6">ผู้บังคับบัญชาระดับแผนก</option>
                                            <option value="7">ผู้บังคับบัญชาระดับกอง</option>
                                            <option value="8">ผู้บังคับบัญชาระดับฝ่าย</option>
                 							<?php
                                            		if(count($get_dropdown_org_option)>0){
														foreach ($get_dropdown_org_option as $key => $item) {
											?>
                                            				<option value="<?php echo $item->orgID;?>"><?php echo $item->orgName;?></option>
                                            <?php
														}
													}
											?>
                                            <option value="10">รองผู้อำนวยการ</option>
                                            <option value="11">ผู้อำนวยการ</option>
                                          </select>
                                         </td>
                                      </tr>
                                </table>
                        		</div>
                          </div> 
                       
                            
              </form>
            </div>
         </section>
    </div>
</div> 
 
<form action="<?php echo base_url('rate/submit') ?>" method="post" enctype="multipart/form-data">
 
 	<table class="xcrud-list table table-striped table-hover table-bordered">
            <thead>
                <tr class="xcrud-th">
                	<th class="xcrud-num">#</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.evalYear" class="xcrud-column xcrud-action">รหัสพนักงาน</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.evalRound" class="xcrud-column xcrud-action">ฝ่าย / กอง / แผนก / ชื่อ-นามสกุล</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.startDate" class="xcrud-column xcrud-action">ประเมินพฤติกรรม</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ประเมิน KPI</th>
                    <th data-order="asc" data-orderby="tbl_eval_date.endDate" class="xcrud-column xcrud-action">ประเมิน Competency </th>
                    
            </thead>
            <tbody>
            	<?php 
				 $num = 1 ;
				 
				 $eval_get	= $this->rate_tsocre->rate_emp_party($var_agencies,$assingID); 
				 if(count($eval_get)>0){
                    foreach ($eval_get as $key => $item) {
                        # code...
						//$eval	= $this->rate_model->get_eval($item->person_id,$round);
                 ?>
                    <tr class="xcrud-row xcrud-row-0" style="background-color:#999"> 
                        <td class="xcrud-current xcrud-num"><i class="fa fa-users"></i> </td>
                        <td><?php //echo $item->orgID; ?></td>
                        <td>
                        	<i class="fa fa-angle-double-right"></i>
                        	&nbsp;<span style="color:#00F"><strong>ฝ่าย</strong> [ <?php echo $item->orgShortName .  '&nbsp;'.$item->orgName; ?> ]</span> 
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                     </tr> 
                     <tr class="xcrud-row xcrud-row-0"> 
                                <td class="xcrud-current xcrud-num"><?php //echo ++$key;?></td>
                                <td><?php echo $item->person_id;?></td>
                                <td>&nbsp;&nbsp; <i class="fa fa-user"></i>&nbsp;<?php echo $item->person;?>&nbsp;<?php echo $item->personLName;?></td>
                                <td> 
                      <?php
                        		$get_eval_notes				 = $this->rate_model->get_eval_notes($item->person_id,$evalFormID,$evalType);//get_eval_notes($item->person_id);
								$eval_person				 = $this->rate_model->get_eval_count_people($item->person_id,$get_eval_notes['evalFormID'],1);
								$count_proples_person		 = $eval_person['count_proples'];
								$eval_subject_group_subject  = $this->rate_model->get_eval_count_subject_group_subject($get_eval_notes['evalFormID']);
								$subject_group_subject 		 = $eval_subject_group_subject['subject_group_subject'];
								 
             					 if($eval_person['count_proples'] == 0){
					 ?>
								<a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->person_id ;?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
								<?php }elseif($eval_person > 0){ 
										$get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
 								?>
									<a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/eassessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->person_id?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
								<?php } ?>
                                 </td>
                                <td>
                                <?php
                                	$eval_person				 = $this->rate_model->get_eval_count_people($item->person_id,$get_eval_notes['evalFormID'],2);
								 
									if($eval_person['count_proples'] == 0){
								?>
                                <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/kpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->person_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                                 <?php }else{
 								?>
                                 <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/ekpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->person_id ?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i>ประเมินแล้ว</a>
                                <?php	 
								 }?>
                                </td>
                                <td>
                                <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item->person_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a></td>
                         	</tr>
                     <tr > 
                        <td colspan="6" class="">&nbsp;</td>
                     </tr>
            <?php 
					   $eval_get_tsocre	= $this->rate_tsocre->rate_emp_division($item->orgID,$assingID); 
					   foreach ($eval_get_tsocre as $key => $item2) {
					   ?>
                     	<tr class="xcrud-row xcrud-row-0" style="background-color:#CCC;"> 
                            <td class="xcrud-current xcrud-num">&nbsp;</td>
                            <td><?php //echo $item2->orgID; ?></td>
                            <td>&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i>&nbsp;<span style="color:#099"><strong><?php echo ++$key;?>. กอง</strong> [ <?php echo $item2->orgShortName .  '&nbsp;'.$item2->orgName; ?> ]</span></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                         </tr>
                         	<tr class="xcrud-row xcrud-row-0"> 
                                <td class="xcrud-current xcrud-num"><?php //echo ++$key;?></td>
                                <td><?php echo $item2->group_id;?></td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>&nbsp;<?php echo $item2->groups;?>&nbsp;<?php echo $item2->groupsLName;?></td>
                                <td> 
                                <!--<a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>" title="">ประเมิน</a>-->
                               <?php
                        		//$get_eval_notes	=	$this->rate_model->get_eval_notes($item2->group_id);
								$get_eval_notes	   = $this->rate_model->get_eval_notes($item2->group_id,$evalFormID,$evalType);
								$eval_group	= $this->rate_model->get_eval_count_people($item2->group_id,$get_eval_notes['evalFormID'],1);
								$count_proples_group	=   $eval_group['count_proples'];
 								if($eval_group['count_proples']	== 0){
								?>
								<a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
								<?php }else{
										$get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
										 
								?>
									<a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/eassessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
								<?php } ?>
                                </td>
                                <td>
                                <?php
                                	$eval_group 	= $this->rate_model->get_eval_count_people($item2->group_id,$get_eval_notes['evalFormID'],2);
									 
									if( $eval_group['count_proples']   ==0){
								?> 
                                <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/kpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                                <?php
									}else{
								?> 
                                 <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/ekpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมินแล้ว</a>
                                <?php
									}
								?>
                                </td>
                                <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item2->group_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a></td>
                         	</tr>
                       		 <?php 
                							$eval_get_tsocress	= $this->rate_tsocre->rate_emp_department($item2->orgID,$assingID); 
                							foreach ($eval_get_tsocress as $key => $item4) {
                								
                								?>
                                            	<tr class="xcrud-row xcrud-row-0" style="background-color:#C0C3C5;"> 
                                                    <td class="xcrud-current xcrud-num"><?php //echo ++$key;?></td>
                                                    <td><?php //echo $item4->orgID; ?></td>
                                            		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i>  <span style="color:#C30"><strong><?php  echo ++$key;?>.) แผนก</strong> [ <?php echo $item4->orgShortName .  '&nbsp;'.$item4->orgName; ?> ]</span></td>
                                                    <td>&nbsp; </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <?php 
                								$eval_get_tsocress44	= $this->rate_tsocre->rate_emp_department_person($item4->orgID,$assingID); 
                 								foreach ($eval_get_tsocress44 as $key => $item5) {
                 								?>
                                                		<tr class="xcrud-row xcrud-row-0"> 
                                                            <td class="xcrud-current xcrud-num"><?php //echo ++$key;?></td>
                                                            <td><?php echo $item5->department_id;?></td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>&nbsp;<?php echo $item5->department;?>&nbsp;<?php echo $item5->departmentLName;?></td>
                                                            <td>  
                                                <?php
                                        		$get_eval_notes	=	$this->rate_model->get_eval_notes($item5->department_id,$evalFormID,$evalType) ;
                								$eval_departmen	= $this->rate_model->get_eval_count_people($item5->department_id,$get_eval_notes['evalFormID'],1);
                								$count_proples_departmen	=   $eval_departmen['count_proples'];
                								 
                 								if($eval_departmen['count_proples']	== 0){
                								?>
                								<a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item5->department_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                								<?php }else{
                										$get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
                										 
                								?>
                									<a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/eassessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item5->department_id ?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
                								<?php } ?>
                                                </td>
                                                <td>
                                                 <?php
														$eval_departmen 	= $this->rate_model->get_eval_count_people($item5->department_id,$get_eval_notes['evalFormID'],2);
														 
														if( $eval_departmen['count_proples']   ==0){
												 ?> 
                                                <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/kpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item5->department_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                                                <?php }else{?>
                                                <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/ekpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item5->department_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมินแล้ว</a>
                                                <?php }?>
                                                </td>
                                                <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item5->department_id ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a></td>
                                                        <!--</tr>-->
                                                <?php }?>
                                                 
                                                <!--zz-->
                                                 <?php 
                							$eval_get_tsocress5	= $this->rate_tsocre->rate_employee_org($item5->orgID,$assingID); 
											 
                							foreach ($eval_get_tsocress5 as $key => $item6) {
                								
                								?>
												<tr class="xcrud-row xcrud-row-0" style="background-color:#E6E6E6;"> 
                                                    <td class="xcrud-current xcrud-num">&nbsp; </td>
                                                    <td>&nbsp; </td>
                                            		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i><span style="color:#090;"><strong> พนักงานในแผนก</strong></span>
                                                    </td>
                                                 	<td>&nbsp; </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
      											<?php 
 													$eval_get_tsocress55	= $this->rate_tsocre->rate_employee_person($item6->orgID,$assingID); 
 													if(count($eval_get_tsocress55)>0){
														foreach ($eval_get_tsocress55 as $key => $item7) {
                 								?>
                                                		<tr class="xcrud-row xcrud-row-0"> 
                                                            <td class="xcrud-current xcrud-num"> </td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <i class="fa fa-user"></i>
                                                            &nbsp;<?php echo $item7->staffPreName; ?><?php echo $item7->staffFName;?> <?php echo $item7->staffLName;?>&nbsp; 
                                                            </td>
                                                            <td>
                                                            	<?php
																	$get_eval_notes	=	$this->rate_model->get_eval_notes($item7->employee_id,$evalFormID,$evalType) ;
																	$eval_departmen_people	= $this->rate_model->get_eval_count_people($item5->department_id,$get_eval_notes['evalFormID'],1);
																	$count_proples_departmen	=   $eval_departmen_people['count_proples'];
 																	if($eval_departmen_people['count_proples']	== 0){
																 ?>
																	<a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/assessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item7->employee_id; ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
																	<?php }else{
																			$get_eval_nedit	=	$this->rate_model->get_eval($get_eval_notes['empID'],$get_eval_notes['evalFormID']);
 																	?>
																		<a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/eassessment');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item7->employee_id ?>&var_agencies=<?php echo $var_agencies;?>" title=""><i class="fa fa-pencil-square-o"></i> ประเมินแล้ว</a>
																	<?php } ?>
                                                            </td> 
                                                            <td>
                                                            <?php
                                                            $eval_departmen_people	= $this->rate_model->get_eval_count_people($item7->employee_id,$get_eval_notes['evalFormID'],2);
															if($eval_departmen_people['count_proples']	== 0){
															?>
                                                            <a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/kpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item7->employee_id; ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a>
                                                            <?php 
															}else{
															?>
                                                            <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url('rate/ekpi');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item7->employee_id; ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมินแล้ว</a>
                                                            <?php } ?>
                                                            </td>
                                                            <td><a class="btn btn-default btn-sm btn-info" href="<?php echo base_url('rate/competency');?>?eval=<?php echo $var_eval_date;?>&staffID=<?php echo $item7->employee_id; ?>&var_agencies=<?php echo $var_agencies;?>" title="">ประเมิน</a></td>
                                                       </tr>
                                                		
                                                  <?php }?>
                                                 
                                               <?php }else{
											?>
		 	  									<tr class="xcrud-row xcrud-row-0">
                                                  	<td class="xcrud-current xcrud-num"></td>
													<td>&nbsp;</td>
                                                  	<td align="center">- ไม่พบพนักงานในแผนก -</td>
                                                  	<td>                                                        
                                                  	<td>&nbsp;</td>
                                                  	<td>&nbsp;</td>
			  									</tr>
                                            <?php   
												}?>
                                                
                                            <?php } ?></tr>
                                                <!--zz-->
                                            <?php }?>
                                            
                                     <?php } ?>
                                      
                                <?php }
 
                				 }else{
                 				?>	   
                	<tr class="xcrud-row xcrud-row-0"> 
                        <td colspan="6" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
                     </tr>
                <?php } ?>          
               </tbody>
            <tfoot>
           </tfoot>
        </table>
 <div class="form-group" style="text-align: right;">
 <strong style="color:red">หากยืนยันการส่งข้อมูลแล้วจะไม่สามารถแก้ไขการประเมินได้</strong><br/>
 <?php 
 
		if($var_eval_date !=''){
				$var_eval_date	=	explode(',',$var_eval_date);	
				$get_eval_date		=	$var_eval_date[1];
				$get_eval_round		=	$var_eval_date[0];
		}
		$year 				= ($year !=''?$year:$get_eval_date);
		$round 				= ($round !=''?$round:$get_eval_round);
		$recent_submit		= $this->recent_model->recent_submit($year,$round,$var_agencies);	
		
		if($year !='' and $round !='' and $var_agencies !='') {
 			if($recent_submit == false){
			
 ?>
                  <input name="year" id="year" type="hidden" value="<?php echo  $year ;?>" />
                  <input name="round" id="round" type="hidden" value="<?php echo $round ;?>" />
                  <input name="var_agencies" id="var_agencies" type="hidden" value="<?php echo $var_agencies;?>" />
                  <button type="submit" class="btn btn-warning"  ><i class="fa fa-plus-circle"></i> ส่งข้อมูลประเมิน</button>
  <?php 	}//else{
	  
  		}
	    //}
	//}
  ?>
 </div>
</form>
 

<script type="text/javascript"> 
	function changeRate(obj){
    	alert(obj.options[obj.selectedIndex].value);
		var x =	obj.options[obj.selectedIndex].value;
		$.ajax({
				url: "<?php echo base_url('rate/xx') ?>",
				type: 'POST',
				data: {
						assignDate: x 
				},
				success: function(response) {
					//Do Something 
				   // var obj = jQuery.parseJSON(response); 
					$("#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE").html(response);
					//alert(obj.assignID);
					//var dp1433735797502 = $("#dp1433735797502").val('');
				},
				error: function(xhr) {
					//Do Something to handle error
					//alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
					location.replace('<?php echo base_url().'rate/' ?>');
				}
		});	
  	}
 
 
</script>
 