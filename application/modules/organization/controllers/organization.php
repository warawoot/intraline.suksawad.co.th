<?php

class Organization extends MY_Controller {
    //http://okvee.net/th/post/category-nested-tree-for-ci2
    function __construct() {
    	parent::__construct();
     	$this->load->model('organization_model');
		 
    }
 
	public function tree() {
		$token	= $this->input->get('token');
		//$data['dpo_list_content']  = 	$this->organization_model->dpo_list_content($token);  
		//$data['data_categories'] = 	$this->organization_model->categories($token);
		
 		$data_tree  = 	$this->organization_model->categories($token); 
		$data['token']	=	$token;
 		 
		$new = array();
		foreach ($data_tree as $a){
			 $new[$a['upperOrgID']][] = $a;
		}
		$data['tree'] = $this->organization_model->createTree($new, $new[0]); // changed createTree
		echo "<pre>";
		print_r($data['tree']);
		//$data['print_data_tree'] = $this->organization_model->getCategoryTree($level = 0, $prefix = '' , $token); // changed createTree
	 	//$data['print_data_tree']	= $this->organization_model->generateTree($data_tree); 
		 
		//$this->template->load('template/admin', 'organization_form',$data);
 		 
    }
	 
 	 
    public function index() {
  		
		$token	= $this->input->get('token');
		
		if($token == ''){
			redirect(site_url().'appoint', 'refresh');
			exit();
		}
		
 		$data['token']	=	$token;
		
 		$data['print_data_tree'] = $this->organization_model->getCategoryTree($level = 0, $prefix = '' , $token ); 
 		
		$this->template->load('template/admin', 'organization_form',$data);
		 
     }
	
	public function add() {
 		
		$token	= $this->input->get('token');
        $max 	= $this->organization_model->max_id(); 
		$orgID	= '';
		$key	= '';
		$upperOrgID	= '';
		
		if($token == ''){
			redirect(site_url().'appoint', 'refresh');
			exit();
		}
		 
		if($max  == NULL){
			$max_id  =  $max+1;
 		}else{
			$max_id  =  $max+1;
   		}
  		 
		$distinct_orgId				= $this->organization_model->distinct_orgchart($max_id,$token); 
		//$data['dropdown_org_chart'] = $this->organization_model->get_dropdown_all('orgID' , 'orgName' ,'tbl_org_chart' , $token);#Dropdown 
		$data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , 'sub', $token);#Dropdown 
		
		if(count($distinct_orgId)>0){
			redirect(site_url().'organization/index?token='.$token, 'refresh');
			exit();
		}
 		$get_content	= $this->organization_model->get_content($key); 
 		$data['get_content'] 	= $get_content ;
 		$data['token']	=	$token;
		$data['max_id']	=	$max_id;
		$data['data']	=	''; 
		$data['action']	=	'add'; 
        $this->template->load('template/admin', 'organization_add',$data);
    }
    
	public function edit() {
		
		$token	= $this->input->get('token'); // assignID
        $key	= $this->input->get('key') ;  // key 
		$orgID	= $this->input->get('orgID') ; // orgID
		$upperOrgID	= $this->input->get('upperOrgID') ; // orgID 
		
		$data['dropdown_org_chart'] = $this->organization_model->getDropdownTree(0, $prefix = '' , 'sub', $upperOrgID);#Dropdown 
		
		if($key == ''){
			redirect(site_url().'organization/index?token='.$token, 'refresh');
			exit();
		}
 		
		$get_content	= $this->organization_model->get_content($key); 
  		 
 		$data['token']	=	$token;
		$data['key']	=	$key;
		$data['orgID']	=	$orgID;
		$data['get_content']	=	$get_content; 
 		$data['data']	=	''; 
		$data['action']	=	'edit'; 
        $this->template->load('template/admin', 'organization_add',$data);
	}
	 
	public function edit1() {
  		
        $max 	= $this->organization_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
 		}else{
			$max_id  =  $max+1;
   		}
		//
		$token	= $this->input->get('token');
		$orgID	= $this->input->get('orgID');
		$key	= $this->input->get('key'); 
		//$count_level 	= $this->organization_model->count_level($token); 
		//echo "<pre>";
		//print_r($count_level);
		
		if($token == ''){
			redirect(site_url().'appoint', 'refresh');
			exit();
		}
		
 		$xcrud	=	xcrud_get_instance();
		
		$xcrud->show_primary_ai_column(true);
		
        $xcrud->table('tbl_org_chart'); 
 		$xcrud->where('assignID', $token);
 		$xcrud->where('orgID ', $orgID);
		$xcrud->where('id ',$key);
        $arr		=	array("orgID"=>"id","orgShortName"=>"ชื่อย่อ","orgName"=>"ชื่อหน่วยงาน","upperOrgID"=>"หน่วยงานแม่" );
		$arr_fields	=	array("orgShortName"=>"ชื่อย่อ","orgName"=>"ชื่อหน่วยงาน","upperOrgID"=>"หน่วยงานแม่" );
        
		// relation //
  		//$xcrud->relation('upperOrgID','tbl_org_chart','orgID','orgName','tbl_org_chart.assignID = '.$token); // หน่วยงานแม่
		$xcrud->relation ( 'upperOrgID','tbl_org_chart','orgID','orgName','tbl_org_chart.assignID = '.$token, '', '', '', array('left_key'=>'upperOrgID','level_key'=>'orgID'), '', '' ) ;
		// demo : relation ( field, target_table, target_id, target_name, where_array, main_table, multi, concat_separator, tree, depend_field, depend_on ) 
		//$xcrud->relation('managerID','tbl_staff','ID',array('staffFName','staffLName')); // ผู้บังคับบัญชา
		//$xcrud->relation('assistantID','tbl_staff','ID',array('staffFName','staffLName')); // ที่ปรึกษา
		
		 
 		// -- validation_required -- 
		$xcrud->validation_required('orgShortName')->validation_required('orgName') ;
		 
		//$xcrud->column_pattern('orgName',' {orgName}');
 		
 		//$xcrud->pass_var('orgID',$max_id,'create');
		//$xcrud->pass_var('level',$count_level,'create');
		//$xcrud->pass_var('assignID',$token);
		//$xcrud->pass_var('upperOrgID_modify', '{orgID}', 'edit');
		
		 
        $xcrud->label($arr);
        $xcrud->columns($arr); 
		 
        $xcrud->fields($arr_fields); 
 		
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
  		$xcrud->unset_add();  
		$xcrud->unset_view(); 
		$xcrud->unset_remove();
		$xcrud->unset_search(); 
  		$data['button_add']	=	'edit';
		$data['token']	=	$token;
        //$data['data']	=	$xcrud->render('edit',$orgID ,$token);
		$data['data']	=	$xcrud->render('edit',$key);
		//$data['data']	=	$xcrud->render();
		 
        $this->template->load('template/admin', 'organization',$data);
		
    } 
	 
	 public function view() {
          
		$token	= $this->input->get('token');
		$orgID	= $this->input->get('orgID');
 		$key	= $this->input->get('key');
		
		if($token == ''){
			redirect(site_url().'appoint', 'refresh');
			exit();
		}
		
		$this->load->helper('xcrud_helper'); 
 		$xcrud	=	xcrud_get_instance();

 		$xcrud->table('tbl_staff');
		$xcrud->order_by('staffID','asc');
		$xcrud->where('orgID', $orgID);
		//echo "<pre>";
		//print_r($xcrud);
		$xcrud->relation('positionID','tbl_position','positionID','positionName');
		$xcrud->relation('provinceID','tbl_province','provinceID','provinceName');
		$xcrud->relation('districtID','tbl_district','districtID','districtName','','','','','','provinceID','provinceID');
		$xcrud->relation('subdistrictID','tbl_sub_district','subdistrictID','subdistrictName','','','','','','districtID','districtID');
		$xcrud->relation('provinceIDNow','tbl_province','provinceID','provinceName');
		$xcrud->relation('districtIDNow','tbl_district','districtID','districtName','','','','','','provinceID','provinceIDNow');
		$xcrud->relation('subdistrictIDNow','tbl_sub_district','subdistrictID','subdistrictName','','','','','','districtID','districtIDNow');

		$col_name = array(
			'staffID' => 'รหัสพนักงาน',
			'staffPreName' => 'คำนำหน้าชื่อ',
			'staffFName' => 'ชื่อ',
			'staffLName' => 'นามสกุล',
			'staffIDCard' => 'เลขประจำตัวประชาชน/หนังสือเดินทาง',
			'staffPreNameEN' => 'คำนำหน้าชื่อ (EN)',
			'staffFNameEN' => 'ชื่อ (EN)',
			'staffLNameEN' => 'นามสกุล (EN)',
			//'staffOldPreName' => 'คำนำหน้าชื่อ (เดิม)',
			'staffOldPreName' => '',
			'staffOldFName' => 'ชื่อ (เดิม)',
			'staffOldLName' => 'นามสกุล (เดิม)',
			'staffBirthday' => 'วันเกิด',
			'staffGender' => 'เพศ',
			'staffBlood' => 'หมู่โลหิต',
			'staffImage' => 'รูปภาพ',
			'rankID' => 'ระดับ',
			'positionID' => 'ตำแหน่ง',
			'orgID'	=> 'สังกัด',
			/*'staffDateWork' => 'วันที่บรรจุ',
			'workStatusID' => 'ที่อยู่ตามทะเบียนบ้าน',
			'workPlaceID' => 'ที่อยู่ปัจจุบัน',
			'orgID1'	=> 'ฝ่าย / สำนักงาน',
			'orgID2'	=> 'กอง / กลุ่มงาน',*/
			'staffImage'	=> 'รูป',
			'staffBirthday'	=> 'วันเกิด',
			'staffBirthday'	=> 'วันเกิด',
			'staffHight'	=> 'ส่วนสูง',
			'staffWeight' => 'น้ำหนัก',
			'staffTel'	=> 'เบอร์โทรศัพท์',
			'staffEmail' => 'E-mail',
			'staffPreNameEtc' => 'อื่นๆ',
			'staffPreNameEnEtc' => 'Etc',
			'staffEthnicity' => 'เชื้อชาติ',
			'staffNation'	=> 'สัญชาติ',
			'staffReligion' => 'ศาสนา',
			'staffStatus'	=> 'สถานะภาพ',
			'staffMilitary' => 'สถานะทางการทหาร',
			'staffDriver'  => 'ใบอนุญาติขับขี่',
			'staffTalent'  => 'ความรู้ความสามารถ',
			'staffAddress'  => 'เลขที่/หมู่บ้าน/ซอย (ทะเบียนบ้าน)',
			'provinceID'  => 'จังหวัด (ทะเบียนบ้าน)',
			'districtID'  => 'อำเภอ/เขต (ทะเบียนบ้าน)',
			'subdistrictID'  => 'ตำบล/แขวง (ทะเบียนบ้าน)',
			'zipCode'  => 'รหัสไปรษณีย์ (ทะเบียนบ้าน)',
			'staffAddressNow'  => 'เลขที่/หมู่บ้าน/ซอย (ปัจจุบัน)',
			'provinceIDNow'  => 'จังหวัด (ปัจจุบัน)',
			'districtIDNow'  => 'อำเภอ/เขต (ปัจจุบัน)',
			'subdistrictIDNow'  => 'ตำบล/แขวง (ปัจจุบัน)',
			'zipCodeNow'  => 'รหัสไปรษณีย์ (ปัจจุบัน)',
			'staffIDCardType' => 'ประเภทบัตรประจำตัว',
			'staffDriverDate' => 'วันที่ใบอนุญาตหมดอายุ',
			'staffAddrType' => 'ประเภทที่อยู่',
			'staffIDCardOff' => 'รหัสพนักงาน'

		);
		$xcrud->columns('staffID,staffFName,rankID,positionID,orgID');
		$xcrud->label($col_name);
		$xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
		$xcrud->column_callback('rankID','calRankWork');
		$xcrud->column_callback('positionID','calPositionWork');
		$xcrud->column_callback('orgID','calOrgIDWork');
		

		$xcrud->column_width('staffID','5%');

		$xcrud->button(site_url().'staffprint/?token={ID}','พิมพ์ประวัติ','fa fa-print','btn-danger');

		$xcrud->fields('staffImage,staffID,staffIDCardType,staffIDCard,staffPreName,staffPreNameEtc,staffFName,staffLName,staffPreNameEN,staffPreNameEnEtc,staffFNameEN,staffLNameEN');
		$xcrud->fields('staffOldPreName',false,false,'edit');
		$xcrud->fields('staffGender,staffBirthday,staffHight,staffWeight,staffBlood,staffEthnicity,staffNation,staffReligion,staffStatus,staffMilitary,staffDriver,staffDriverDate');
		$xcrud->fields('staffTalent,staffTel,staffEmail,staffAddress,provinceID,districtID,subdistrictID,zipCode,staffAddressNow,provinceIDNow,districtIDNow,subdistrictIDNow,zipCodeNow,staffAddrType');
		//$xcrud->subselect('staffFName','{staffFName} {staffLName}');
		
		$xcrud->change_type('staffImage', 'image', '', array(
	    'width' => 200,
	    'height' => 200,
	    'manual_crop' => true));
	   // $xcrud->change_type('staffID','text');
		$xcrud->change_type('staffGender','radio','',array('m'=>'ชาย','f'=>'หญิง'));
		$xcrud->change_type('staffBlood','radio','',array('A'=>'A','B'=>'B','AB'=>'AB','O'=>'O'));
		$xcrud->change_type('staffPreName','select','',array('นาย'=>'นาย','นาง'=>'นาง','นางสาว'=>'นางสาว','อื่นๆ'=>'อื่นๆ'));
		$xcrud->change_type('staffPreNameEN','select','',array('Mr.'=>'Mr.','Mrs.'=>'Mrs.','Miss'=>'Miss','Etc'=>'Etc'));
		$xcrud->change_type('staffIDCardType','radio','',array('1'=>'คนไทย','2'=>'ต่างชาติ','3'=>'ประชาชนพื้นที่สูง(ชาวเขา)'));
		$xcrud->change_type('staffAddrType','radio','',array('1'=>'บ้านตนเอง','2'=>'บ้านญาติ','3'=>'บ้านเช่า/หอพัก'));
		

		//$xcrud->change_type('staffStatus','radio','',array('โสด'=>'ไม่มีข้อมูลคู่สมรส','สมรส'=>'มีข้อมูลคู่สมรสเป็น "สมรส"','หย่า'=>'มีข้อมูลคู่สมรสเป็น "หย่า"','หม้าย'=>'มีข้อมูลคู่สมรสเป็น "เสียชีวิต"'));
		$xcrud->column_pattern('staffStatus','{ID}');
		$xcrud->field_callback('staffStatus','getStaffStatus');
		$xcrud->change_type('staffMilitary','radio','',array('เคยรับราชการทหาร'=>'เคยรับราชการทหาร','ได้รับการยกเว้น'=>'ได้รับการยกเว้น'));
		$xcrud->change_type('staffDriver','multiselect','',array('รถยนต์'=>'รถยนต์','รถจักรยานยนต์'=>'รถจักรยานยนต์','รถ 6 ล้อ'=>'รถ 6 ล้อ'));
		$xcrud->validation_required('staffFName,staffLName')->validation_required('staffIDCard',13);
		$xcrud->validation_pattern('staffIDCard,staffHight,staffWeight,staffTel,zipCode,zipCodeNow','numeric')->validation_pattern('staffEmail','email');
		

		$xcrud->field_callback('staffOldPreName','getLinkChangeName');

		//$xcrud->field_callback('workStatusID','getNull');
		//$xcrud->field_callback('workPlaceID','getNull');

		//$xcrud->set_attr('workStatusID',array('class'=>'label'));
		//$xcrud->unset_list();
		//$xcrud->hide_button('return');
		
		//$contactInfo = $xcrud->nested_table('staff','orderNumber','orderdetails','orderNumber'); // 2nd level


		//$staffname = $xcrud->nested_table('ข้อมูลติดต่อ','staffID','tblstaffaddress','staffID'); // 2nd level 2
    	//$staffname->columns('addressNo');
    	//$staffname->default_tab('ข้อมูลติดต่อ');
    	//echo "<pre>";
		//var_dump($xcrud);
		// $data['html'] = $xcrud->render();

		// $data['title'] = "ทะเบียนประวัติ";
  //       $this->template->load("template/admin2",'organization', $data);












  //       $xcrud->table('tbl_org_chart'); 
 	// 	$xcrud->where('assignID', $token);
 	// 	$xcrud->where('orgID', $orgID);
  //       $arr		=	array("orgShortName"=>"ชื่อย่อ","orgName"=>"ชื่อหน่วยงาน","upperOrgID"=>"หน่วยงานแม่" );//,"managerID"=>"ผู้บังคับบัญชา","assistantID"=>"ที่ปรึกษา"
		// $arr_fields	=	array("orgShortName"=>"ชื่อย่อ","orgName"=>"ชื่อหน่วยงาน","upperOrgID"=>"หน่วยงานแม่");
        
		// // relation //
  // 		$xcrud->relation('upperOrgID','tbl_org_chart','orgID','orgName','tbl_org_chart.assignID = '.$token); // หน่วยงานแม่
		// //$xcrud->relation ( 'upperOrgID','tblorgchart','orgID','orgName','tblorgchart.assignID = '.$token, '', '', '', array('left_key'=>'upperOrgID','level_key'=>'orgID'), '', '' ) ;
		// // demo : relation ( field, target_table, target_id, target_name, where_array, main_table, multi, concat_separator, tree, depend_field, depend_on ) 
		// $xcrud->relation('managerID','tbl_staff','ID','staffFName'); // ผู้บังคับบัญชา
		// $xcrud->relation('assistantID','tbl_staff','ID','staffFName'); // ที่ปรึกษา
		
		// // subselect //
		// //$xcrud->subselect('level','SELECT upperOrgID FROM tblorgchart WHERE upperOrgID = {upperOrgID}');
 	// 	// -- validation_required -- 
		// $xcrud->validation_required('orgShortName')->validation_required('orgName')->validation_required('managerID')->validation_required('managerID')  ;
		 
		// $xcrud->column_pattern('orgName',' {orgName}');
 		 
  //       $xcrud->label($arr);
  //       $xcrud->columns($arr); 
		 
  //       $xcrud->fields($arr_fields); 
         
  //       $xcrud->unset_print();
  //       $xcrud->unset_title();
  //       $xcrud->unset_csv();
  // 		$xcrud->unset_add(); 
		 
		//$xcrud->unset_edit();
		//$xcrud->unset_remove();
		//$xcrud->unset_search();
  		$data['button_add']	=	'view';
		$data['token']	=	$token;
        $data['html'] = $xcrud->render();
 		// $data['data']	=	$xcrud->render();
        $this->template->load('template/admin2', 'main',$data);
    }
	
	 
	
	public function delete() {
  		
        $max 	= $this->organization_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
 		}else{
			$max_id  =  $max+1;
   		}
		
		
		$token	= $this->input->get('token');
		$orgID	= $this->input->get('orgID');
		
		if($token == ''){
			redirect(site_url().'appoint', 'refresh');
			exit();
		} 
		
  		$data['token']	= $token; 
		$data['button_add']	= 'delete'; 
 		$data['data'] = $this->organization_model->delete_orgchart($orgID,$token) ;
		 
        $this->template->load('template/admin', 'organization',$data);
    }
	
	
	public function check_add() {
		
		$orgID			= $this->input->get_post("orgID");
		$assignID 		= $this->input->get_post("assignID");
		$orgShortName	= $this->input->get_post("orgShortName");
		$orgName		= $this->input->get_post("orgName");
		$upperOrgID		= $this->input->get_post("upperOrgID"); 
 		$action			= $this->input->get_post("action"); 
		$id				= $this->input->get_post("id"); 

        $sequence       = $this->input->get_post("sequence"); 
        $position       = $this->input->get_post("position"); 
        $rows           = $this->input->get_post("rows"); 
        $managerID      = $this->input->get_post("managerID"); 
        $lineToken      = $this->input->get_post("lineToken"); 
		
		$data	=	array( 
 				"orgID"=>$orgID,
				"assignID"=>$assignID ,
 				"orgShortName"=>$orgShortName,
				"orgName"=>$orgName,
				"upperOrgID"=>$upperOrgID,
                "sequence"=>$sequence,
                "position"=>$position,
                "rows"=>$rows,
				"managerID"=>$managerID,
				"lineToken"=>$lineToken 
		 );
		 
		$data2	=	array( 
 				"orgShortName"=>$orgShortName,
				"orgName"=>$orgName,
				"upperOrgID"=>$upperOrgID,
                "sequence"=>$sequence,
                "position"=>$position,
                "rows"=>$rows,
				"managerID"=>$managerID,
				"lineToken"=>$lineToken 
		 ); 
  		// action=edit",
		if($action == 'edit'){
			$insert_org_chart = $this->organization_model->update_org_chart($id,$orgID,$assignID,$data2); 
			 
 		}else{
			$insert_org_chart = $this->organization_model->insert_org_chart($data); 	
			 
		} 
  		 
		if($insert_org_chart == 'succesfully'){
				echo  'succesfully';
		}else{
				echo 'failed';
		}									
         
	}
	
	function check_search(){
		
		$s_search	= $this->input->get_post('search_c');
		$orgID		= $this->input->get_post('orgID');
		$assignID	= $this->input->get_post('assignID');
		$fields		= $this->input->get_post('fields');
		//$search = $this->organization_model->search_all($search ,$orgID , $assignID);
		
		$token	= $this->input->get('token');
		 		
 		$data['print_data_tree'] = $this->organization_model->getCategorySearch($level = 0, $prefix = '' , $assignID , $s_search ,$fields);
		 
		echo $data['print_data_tree'];
			
	}
	
	function tree2015(){
		
		 $data['data'] = '';
		 $this->template->load('template/admin', 'treebindingtojson',$data);
		  
   }

// --- ORG MANAGE --- //

   public function manage() {
		
		$orgID	= $this->input->get('orgID') ;
		$assignID	= $_GET['assignID'];
		
		$data['orgID']	=	$orgID;

		$data['list'] = $this->organization_model->listManage($orgID,$assignID);
		// echo $this->db->last_query();
        $this->template->load('template/admin', 'organization_manage',$data);
	}

	public function manage_add() {
		
		$orgID	= $this->input->get('orgID') ;
		$data['orgID']	 	= $orgID;
		$data['mode'] = 'add';
        $this->template->load('template/admin', 'manage_form',$data);
	}

	public function manage_edit() {
		
		$staffID	= $this->input->get('token') ;
		$assignID	= $this->input->get('assignID') ;
		$orgID	= $this->input->get('orgID') ;

		$data['manage'] = $this->organization_model->editManage($staffID,$orgID,$assignID);
		
		$data['staffID']	= $staffID;
		$data['assignID']	= $assignID;
		$data['orgID']	 	= $orgID;
		$data['mode'] = 'edit';
        $this->template->load('template/admin', 'manage_form',$data);
	}

	public function manage_delete() {
		
		$staffID	= $this->input->get('token') ;
		$orgID	= $this->input->get('orgID') ;
		$assingID	= $this->input->get('assingID') ; 

		$tables = array('tbl_org_manager');
		$this->db->where('staffID', $staffID);
		$this->db->where('orgID', $orgID);
		$this->db->delete($tables);

		redirect('/organization/manage?orgID='.$orgID.'&assignID='.$assignID, 'location');
        // $this->template->load('template/admin', 'manage_form',$data);
	}

	public function manageSubmit(){
		$assignID = $this->input->post('assignID');
		$mode	= $this->input->post('mode') ;
		$orgID	= $this->input->get('orgID') ;
		$staffID	= $this->input->post('dropdown_staff') ;
		$ID	= $this->input->post('staffID') ;
		$startDate	= $this->input->post('dp1433735797502') ;
		$endDate	= $this->input->post('dp1433735797503') ;
		$type	= $this->input->post('manageType') ;

		if($startDate !=''){
			$date_start = explode('/',$startDate);
			$sY = $date_start[2]-543;
			$sM = $date_start[1];
			$sD = $date_start[0];
		}
			$sDate = $sY.'-'.$sM.'-'.$sD;

		if($endDate !=''){
			$end_start = explode('/',$endDate);
			$eY = $end_start[2]-543;
			$eM = $end_start[1];
			$eD = $end_start[0];

			$eDate = $eY.'-'.$eM.'-'.$eD;
		}

		$arr = array(
			'orgID' 			=>  $orgID,
			'assignID' 			=>  $assignID,
			'staffID' 			=>	$staffID,
			'managerStartDate' 	=>  $sDate,
			'managerEndDate' 	=>	$eDate,
			'managerType' 		=>	$type,
		);
		
		if($mode == 'edit'){
			$this->organization_model->updateManage($ID,$orgID,$arr);
		}else{
			$this->organization_model->insertManage($arr);
		}

		redirect('/organization/manage?orgID='.$orgID.'&assignID='.$assignID, 'location');

        // $this->template->load('template/admin', 'manage?orgID='.$orgID);
		
	}
	
   
}

?>