<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลจัดการผังองค์กร</h3>
            </header>
            <div class="panel-body">
             
                <div class="xcrud">
                    <div class="xcrud-container">
                    <div class="xcrud-ajax">
                         <div class="xcrud-view">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">ชื่อย่อ*</label>
                                    <div class="col-sm-9" ><input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="<?php echo $get_content['orgShortName'];?>" name="dGJsX29yZ19jaGFydC5vcmdTaG9ydE5hbWU-" id="dGJsX29yZ19jaGFydC5vcmdTaG9ydE5hbWU-" maxlength="50"  >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">ชื่อหน่วยงาน*</label>
                                    <div class="col-sm-9" ><input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="<?php echo $get_content['orgName'];?>" name="dGJsX29yZ19jaGFydC5vcmdOYW1l" id="dGJsX29yZ19jaGFydC5vcmdOYW1l" maxlength="255" ></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">หน่วยงานแม่</label>
                                    <div class="col-sm-9">
                                    <select class="xcrud-input form-control form-control" data-type="select" name="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE" id="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE" maxlength="11"> 
                                        <?php 
                                           // echo form_dropdown('dGJsX29yZ19jaGFydC51cHBlck9yZ0lE',$dropdown_org_chart ,$token ,' id="dGJsX29yZ19jaGFydC51cHBlck9yZ0lE" class="xcrud-input form-control form-control"  data-type="select" maxlength="11" ');
                                        echo $dropdown_org_chart;
										?>
                                     </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-3">ลำดับที่*</label>
                                    <div class="col-sm-9" >
                                    	<input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="<?php echo $get_content['sequence'];?>" name="sequence" id="sequence" maxlength="255"  onkeypress="return isNumberKey(event)"
>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-3">ตำแหน่ง*</label>
                                    <div class="col-sm-9" >
                                        <div class="radio col-sm-3">
                                            <label>
                                              <input type="radio" name="position" id="position" value="L" <?php if ($get_content['position'] == 'L'){ echo "checked=\"checked\"";}?>>
                                             ซ้าย
                                            </label>
                                         </div>
                                         <?php if($get_content['position'] == ''){ ?>
                                         <div class="radio col-sm-3">
                                            <label>
                                              <input type="radio" name="position" id="position" value="M" checked="checked">
                                            ปกติ
                                            </label>
                                         </div>
                                         <?php }else{ ?>
                                         <div class="radio col-sm-3">
                                            <label>
                                              <input type="radio" name="position" id="position" value="M" <?php if ($get_content['position'] == 'M'){ echo "checked=\"checked\"";}?>>
                                            ปกติ
                                            </label>
                                         </div>
                                         <?php }?>
                                         <div class="radio col-sm-3">
                                            <label>
                                              <input type="radio" name="position" id="position" value="R" <?php if ($get_content['position'] == 'R'){ echo "checked=\"checked\"";}?>>
                                             ขวา
                                            </label>
                                         </div>
                                         <span id="log"></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-3">แถวที่*</label>
                                    <div class="col-sm-9" >
                                    	<input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="<?php echo $get_content['rows'];?>" name="rows" id="rows" maxlength="255" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3">ผู้บังคับบัญชา*</label>
                                    <div class="col-sm-9" >
                                    	<input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="<?php echo $get_content['managerID'];?>" name="managerID" id="managerID" maxlength="255">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3">Line Token*</label>
                                    <div class="col-sm-9" >
                                    	<input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="<?php echo $get_content['lineToken'];?>" name="lineToken" id="lineToken" maxlength="255">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
            
                        <div class="xcrud-top-actions btn-group">
                            <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action" onclick="modal_ck()">บันทึกและย้อนกลับ</a><a href="javascript:;" data-task="list" class="btn btn-warning xcrud-action" onclick="modal_back()">ย้อนกลับ</a></div>
                        <div class="xcrud-nav"> </div>
                    </div>
                <div class="xcrud-overlay" style="display: none;"></div>
            </div>
            </div>
			
         
         </div>
             
        </section>
    </div>
</div>


<script type="text/javascript">
		//xcrud-error
		function isNumberKey(evt) {
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				// Added to allow decimal, period, or delete
				if (charCode == 110 || charCode == 190 ) 
					return true;
				
				if (charCode > 31 && (charCode < 48 || charCode > 57 )) 
					return false;
				
				return true;
		} // isNumberKey
		
		jQuery(".xcrud-ajax").find("xcrud-error").css({"width":"100%"}).removeClass("xcrud-error");
		$(".xcrud-ajax").find('.xcrud-error').html('Create System');
		function modal_back(){
			location.replace('<?php echo base_url().'organization/index?token='.$token; ?>');
		}
		
		
		$( "input" ).on( "click", function() {
		  $( "#log" ).html( $( "input:checked" ).val());
		});

function modal_ck(){
	 	var name		        = $('#dGJsX29yZ19jaGFydC5vcmdTaG9ydE5hbWU-').val();
		var name_full	        = $('#dGJsX29yZ19jaGFydC5vcmdOYW1l').val();
  		var Mother_agency		= $('#dGJsX29yZ19jaGFydC51cHBlck9yZ0lE').val();

        var sequence            = $('#sequence').val();
        var rows                = $('#rows').val();
        var managerID           = $('#managerID').val();
        var lineToken           = $('#lineToken').val();
		var position            = $(".form-group").find('#log').text();
	 
 		 if(name == '' ){
			 alert('กรุณาระบุ ชื่อย่อ ด้วยค่ะ');
			 location.href = '<?php echo base_url().'organization/add?token='.$token; ?>';
		 }else if(name_full == ''){
			 alert('กรุณาระบุ ชื่อหน่วยงาน ด้วยค่ะ');
			 location.href = '<?php echo base_url().'organization/add?token='.$token; ?>';
		 }else{
			 
			<?php if($action == 'edit'){  ?>
				$.ajax({
							url: "<?php echo base_url('organization/check_add') ?>?id=<?php echo $key;?>&action=<?php echo $action;?>",
							type: 'POST',
							data: {
 									orgID: <?php echo $orgID;?>,
									assignID: <?php echo $token;?>,
									orgShortName: name,
									orgName: name_full,
									upperOrgID: Mother_agency,
                                    sequence: sequence,
                                    position: position,
                                    rows: rows,
                                    managerID: managerID,
                                    lineToken: lineToken
							},
							success: function(response) {
								//Do Something 
								//alert(response);
								if(response == 'succesfully'){
									location.replace('<?php echo base_url().'organization/index?token='.$token; ?>');
								}else if(response == 'failed'){
									location.replace('<?php echo base_url().'organization/index?token='.$token; ?>');
								}else{
									//var obj = jQuery.parseJSON(response); 
									location.replace('<?php echo base_url().'organization/add?token='.$token; ?>');
								}
							},
							error: function(xhr) {
								//Do Something to handle error
								alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
								location.replace('<?php echo base_url().'organization/add?token='.$token; ?>');
							}
				});	 
				
			<?php }elseif($action == 'add'){?> 
			
			  $.ajax({
 					url: "<?php echo base_url('organization/check_add') ?>?action=<?php echo $action;?>",
					type: 'POST',
					data: {
						    orgID: <?php echo $max_id;?>,
							assignID: <?php echo $token;?>,
							orgShortName: name,
							orgName: name_full,
							upperOrgID: Mother_agency ,
                            sequence: sequence,
                            position: position,
                            rows: rows,
                            managerID: managerID,
                            lineToken: lineToken
					},
					success: function(response) {
						//Do Something 
						
 						if(response == 'succesfully'){
 							location.replace('<?php echo base_url().'organization/index?token='.$token; ?>');
						}else{
 							//var obj = jQuery.parseJSON(response); 
							location.replace('<?php echo base_url().'organization/add?token='.$token; ?>');
						}
 					},
					error: function(xhr) {
						//Do Something to handle error
 						alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
 						location.replace('<?php echo base_url().'organization/add?token='.$token; ?>');
					}
				});	 
				
			<?php }else{ ?>	
 			  			location.replace('<?php echo base_url().'organization/index?token='.$token; ?>');
			<?php } ?>  
  		}
}
  
</script>