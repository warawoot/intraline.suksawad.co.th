<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลจัดการผังองค์กร</h3>
            </header>
            <div class="panel-body">
                <?php echo $data; ?> 
             </div>
             
        </section>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">

            </header>
            <?php $assignID = $_GET['assignID']?>

            <div class="panel-body">

              <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body profile-information">
                 <a class="btn btn-success" href="javascript:window.location='<?php echo site_url();?>organization/manage_add/index?orgID=<?php echo $orgID.'&assignID='.$assignID;?>'"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</a>
                 <a href="<?php echo base_url('organization')?>"><input class="btn btn-warning xcrud-action" type="button" value="ย้อนกลับ"></a>
                <br><br>
                 <table class="xcrud-list table table-striped table-hover table-bordered">
                    <thead>
                    <tr class="xcrud-th">
                    <?php $orgID = $_GET['orgID']?>

                        <th class="xcrud-num">#</th>
                        <th class="xcrud-column">รหัสพนักงาน</th>
                        <th class="xcrud-column" >ชื่อ - นามสกุล</th>
                        <th class="xcrud-column" >วันที่เริ่ม</th>
                        <th class="xcrud-column" >วันที่สิ้นสุด</th>
                        <th class="xcrud-column" >ประเภท</th>
                        <th class="xcrud-column" ></th>
                    </tr>   
                </thead>
                <tbody>
                 <?php if($list != ''): $num = 1;?>
                    <?php foreach($list as $row):?>
                    
                    <?php if($row->managerType !=''):?>
                        <?php 
                            if($row->managerType == '1')  $type = 'หัวหน้างาน';
                            elseif($row->managerType == '2')  $type = 'รักษาการ';
                            elseif($row->managerType == '3')  $type ='ปฏิบัติงานแทน'
                        ?>
                    <?php endif;?>

                    <tr class="xcrud-row xcrud-row-0" >
                        <td><?=$num++ ?></td>
                        <td><?=$row->staffID?></td>
                        <td><?=$row->staffPreName.''.$row->staffFName.' '.$row->staffLName?></td>
                        <td><?=toFullDate($row->managerStartDate,'th',true)?></td>
                        <td><?=toFullDate($row->managerEndDate,'th',true)?></td>
                        <td><?=$type?></td>
                        <td class="xcrud-current xcrud-actions xcrud-fix" width="10%">
                            <span class="btn-group">
                                <a class="btn btn-default btn-sm btn-success" href="<?php echo base_url("organization/manage_edit?token=$row->id&orgID=$row->orgID&assignID=$assignID");?>" title="แก้ไข"><i class="fa fa-edit"></i></a>
                                <!-- <a class="xcrud-action btn btn-info btn-sm" title="ดูข้อมูล" href="<?php echo base_url("organization/manage_view?token=$row->id&orgID=$row->orgID&assignID=$assignID");?>" data-primary="765" data-task="view"><i class="glyphicon glyphicon-search"></i></a> -->
                                <a class="xcrud-action btn btn-danger btn-sm"  onclick="if(confirm('ยืนยันการลบ')) return true; else return false;" title="ลบ" href="<?php echo base_url("organization/manage_delete/index?token=$row->id&orgID=$row->orgID&assignID=$assignID") ?>" data-primary="1" data-task="remove" onclick="return conf()"><i class="glyphicon glyphicon-remove"></i></a>
                            </span>
                        </td>
                    </tr>
                    <?php endforeach;?>
                <?php else:?>
                    <tr class="xcrud-row xcrud-row-0"> 
                        <td colspan="9" class="xcrud-current xcrud-num">ไม่พบข้อมูล</td>
                    </tr>
                <?php endif;?>
            </div>
        </section>
    </div>
</div>
