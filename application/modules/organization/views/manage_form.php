<style type="text/css">
	.form-control {
 	  color: #343232;
	}
    #ui-datepicker-div{  
		font-size: 0.9em;
 	}  
 	.ui-datepicker-trigger{
	  opacity: .5;
	}
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
<!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
   
<style type="text/css">    
   
.ui-tabs{    
    font-family:tahoma;    
    font-size:11px;
}    
.ui-widget-header {
    color: #000;
    font-weight: bold;
}
 
.ui-datepicker{  
    width:220px;  
    font-family:tahoma;  
    font-size:11px;  
    text-align:center;  
}  
.pf-input{
  color:#000 !important;
}
</style>

<style type="text/css">
.form-control {
   color: #343232;
}
</style>
<div class="row">
<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading"> 
            <h3>ข้อมูลจัดการตำแหน่งหัวหน้า</h3>
        </header>
        <div class="panel-body">
            <?php echo $data; ?> 
         </div>
         
    </section>
</div>
</div>
<?php 
    if($manage != ''){
        foreach($manage as $row){
            $staffID     = $row->staffID;
            $managerID     = $row->managerID;
            $StartDate    = $row->managerStartDate;
            // $endDate      = $row->managerEndDate;
            $managerType  = $row->managerType;

            $start = explode("-",$StartDate);
            $sY = $start[0]+543;
            $sM = $start[1];
            $sD = $start[2];

            $counS = $sD.'/'.$sM.'/'.$sY;

            if($row->managerEndDate != '' || $row->managerEndDate != null){
                $endDate      = $row->managerEndDate;
                $end = explode("-",$endDate);
                $eY = $end[0]+543;
                $eM = $end[1];
                $eD = $end[2];
    
                $counE = $eD.'/'.$eM.'/'.$eY;
            }else{
                $counE = '';
            }
           
            
        }
    }else{
            $staffID      = '';
            $managerID    = '';
            $StartDate    = '';
            $endDate      = '';
            $managerType  = '';

            $counS  = '';
            $counE  = '';
    }

?>
    <div class="xcrud-container">
        <div class="xcrud-ajax">
            
            <div class="xcrud-view">
                <div class="form-horizontal">
                
             <?php $orgID=$_GET['orgID']?>
             <?php $assignID = $_GET['assignID']?>

             <form action="<?=base_url('organization/manageSubmit?orgID='.$orgID)?>" method="post"  enctype="multipart/form-data" >  
             <input  type="hidden" name="orgID" value="<?=$orgID?>">
             <input  type="hidden" name="mode" value="<?=$mode?>">
             <input  type="hidden" name="staffID" value="<?=$staffID?>">
             <input  type="hidden" name="assignID" value="<?=$assignID?>">
            
                    <div class="form-group">
                        <label class="control-label col-sm-3">ชื่อ - นามสกุล*</label>
                        <div class="col-sm-4">
                            <input  data-required="1" type="text" class="form-control" name="dropdown_staff" id="dropdown_staff" placeholder="ค้นหาชื่อ......">
                        </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-3">วันที่เริ่ม*</label>
                    <div class="col-sm-4">
                        <div class="input-group date date-picker1">
                            <input class="form-control"  data-required="1"  data-type="date"  autocomplete="off" name="dp1433735797502" value="<?php  echo ($counS !='')? $counS:''; ?>" type="text" id="dp1433735797502">
                            <span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-3">วันที่สิ้นสุด*</label>
                        <div class="col-sm-4">
                            <div class="input-group date date-picker1">
                                <input class="form-control"  data-required="1"  data-type="date"  autocomplete="off" name="dp1433735797503" value="<?php  echo ($counE !='')? $counE:''; ?>" type="text" id="dp1433735797503">
                                <span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                  </div>

                    <div class="form-group">
                    <label class="control-label col-sm-3" for=""></label>
                    <div class="col-sm-9">
                        <?php if($mode != 'view'):?> 
                            <input  name="manageType" id="manageType" <?php if($managerType == '1'){echo "checked";}else{echo '';}?> type="radio" value="1">  หัวหน้างาน <br>
                            <input  name="manageType" id="manageType" <?php if($managerType == '2'){echo "checked";}else{echo '';}?> type="radio" value="2">  รักษาการแทน <br>
                            <input  name="manageType" id="manageType" <?php if($managerType == '3'){echo "checked";}else{echo '';}?> type="radio" value="3">  ปฎิบัติการแทน <br>
                        <?php else: echo $managerType ?>
                        <?php endif; ?>
                    </div>
                    </div><br><br>
                    <label class="control-label col-sm-3" for=""></label>
                    <div class="col-sm-9">
                     <input class="btn btn-primary xcrud-action" id="btsave" name="btsave" type="submit" value="บันทึกและย้อนกลับ">
                     <a href="<?php echo base_url('organization/manage?orgID='.$orgID.'&assignID='.$assignID)?>"><input class="btn btn-warning xcrud-action" type="button" value="ย้อนกลับ"></a>
                    </div>
                    </div>
                </div>
                <div class="xcrud-nav">  </div>
             </form>
           </div>
         </div>
         <div class="xcrud-overlay" style="display: none;"></div>
    </div>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/peoplefinder/javascripts/smmms-peopleFinder/smmms-peopleFinder.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/peoplefinder/stylesheets/font-awesome-4.4.0/css/font-awesome.min.css">


<script src="<?php echo base_url(); ?>assets/peoplefinder/javascripts/smmms-peopleFinder/jquery.smmms-peopleFinder-Xcrud.js"></script>

<script type="application/javascript">
 

 $(document).ready(function (){


     $('#dropdown_staff').smmmsPeopleFinder({
          url: '<?php echo base_url(); ?>peoplefinder',
          <?php if($mode == 'edit'){?>
          preloadFromUrl: '<?php echo base_url(); ?>peoplefinder/editM?id=<?php echo $staffID.'&mid='.$managerID; ?>',
          <?php } ?>
          idColumn: 'uid',
          imageFolderUrl: '<?php echo base_url(); ?>assets/staff/',
          textColumn: 'name',
          displayColumns: [
            {'map': 'image', 'image': true},            
            {
              'map': 'name'
            },
            [
              {
                'map': 'department'
              },
              {
                'map': 'position'
              }
            ]
          ],
          rows: 20
        });  

 }); 
 
  $('#btsave').on('click', function () {

		if ($('#dropdown_staff').val() == '') {
			alert('กรุณากรอกข้อมูล');
			$('#dropdown_staff').focus();
			return false;
		}

		if ($('#start').val() == '') {
			alert('กรุณากรอกข้อมูล');
			$('#start').focus();
			return false;
		}

        if ($('#manageType').val() == '') {
			alert('กรุณากรอกข้อมูล');
			$('#manageType').focus();
			return false;
		}
		
		// if ($('#dGJsX3N0YWZmX2Fic2VuY2UuYWJzRGV0YWls').val() == '') {
		// 	alert('กรุณากรอกข้อมูล');
		// 	$('#dGJsX3N0YWZmX2Fic2VuY2UuYWJzRGV0YWls').focus();
		// 	return false;
		// }

	});

</script>

<!--- datapicker -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/simple-line-icons/simple-line-icons.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/datepicker/css/datepicker.css" />

<script src="<?php echo base_url() ?>assets/datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/datepicker/js/bootstrap-datepicker-thai.js"></script>
<script src="<?php echo base_url() ?>assets/datepicker/js/locales/bootstrap-datepicker.th.js"></script>

<script type="text/javascript">
   $(function() {
        
        $('.date-picker1').find("input:text:first").datepicker({
            language:'th-th',
            format:'dd/mm/yyyy',
            autoclose: true,
             //startDate: new Date,
              //endDate: '+2d'
        });
        $(".date-picker1 button").click(function() {
          $(this).closest(".date-picker1").find("input:text:first").datepicker("show");
        });
        


       $('#dp1433735797502').change(function(){
            //alert();  

           var data = $(this).val();
           var arr = data.split('/');
           var dateYear = arr[2]-543;
           var dateStart = dateYear+"-"+arr[1]+"-"+arr[0];  

             $('.date-picker').find("input:text:first").datepicker("remove");
             $('.date-picker').find("input:text:first").datepicker({
                  language:'th-th',
                  format:'dd/mm/yyyy',
                  autoclose: true,
                  startDate: new Date(dateStart),
                  //endDate: '+2d'
              });
            $(".date-picker button").click(function() {
              $(this).closest(".date-picker").find("input:text:first").datepicker("show");
            });

           
       });

         

    });
</script>