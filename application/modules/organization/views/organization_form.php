<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลจัดการผังองค์กร</h3>
            </header>
             
             <div class="panel-body">
                	<div class="xcrud-top-actions" style="margin-bottom:23px;">
                    	<div class="btn-group pull-right">  </div>
                        <a href="<?php echo base_url() ?>organization/add?token=<?php echo $token;?>" data-task="create" class="btn btn-success xcrud-action"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</a><a class="xcrud-search-toggle btn btn-warning">ค้นหา</a>
                        <span class="xcrud-search form-inline" style="display:none;">
                        	<input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" id="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" onKeyPress="enterpressalert(event, this)">
                            <select class="xcrud-data xcrud-columns-select input-small form-control" name="column" id="column">
                                <!--<option value="">[ค้นจากทุกฟิลด์]</option>-->
                                 <option value="orgName" data-type="text">ชื่อหน่วยงาน</option>
                                <option value="orgShortName" data-type="text">ชื่อย่อ</option>
                                 <!--<option value="upperOrgID" data-type="relation">หน่วยงานแม่</option> -->
                            </select>
                            <span class="btn-group">
                            	<a class="xcrud-action btn btn-primary" href="javascript:;" data-search="1" >ตกลง</a>
                                <a class="xcrud-action-1 btn btn-default" href="javascript:;" data-search="0" style="display:none;">รีเซ็ต</a>
                            </span></span><div class="clearfix"></div>
                    </div> 
                    <div id="put_data" style="display:none;"></div>
                    <div id="get_data" >
                        <table   border="0" width="100%" class="table table-bordered table-hover" style="margin-top:5px; font-weight:bold;">
                            <thead> 
                                <tr style="background-color:#efefef;">
                                   <!-- <th>ลำดับ</th>-->
                                   <!-- <th>org id</th>
                                    <th>upper org id</th>-->
                                    <th>ชื่อย่อ</th>
                                    <th>ชื่อหน่วยงาน</th>
                                     <!--<th>ผู้บังคับบัญชา</th>  
                                   <th>ที่ปรึกษา</th>-->
                                    <th>&nbsp;</th> 
                                </tr>
                             </thead>
                            <?php  //echo count($print_data_tree);
                                  if ( $print_data_tree !='' ): 
                                     echo $print_data_tree ;
                                  else: 
                                  ?>
                                 <tr>
                                    <td colspan="11">ไม่พบข้อมูล</td>
                                </tr>
                            <?php endif; ?>
                       </table>
                </div>
             </div>
             <div class="xcrud-top-actions btn-group">
                <a href="javascript:;" data-task="list" class="btn btn-warning xcrud-action" onclick="modal_back()">ย้อนกลับ</a>
             </div>
         </section>
    </div> 
</div>
<script>

		function modal_back(){
			location.replace('<?php echo base_url().'appoint'; ?>');
		}
	//function modal_search(){ 
		
		$(".xcrud-search-toggle").click(function(){
         	$(".xcrud-search").fadeIn('');
			$(".xcrud-search-toggle").fadeOut("slow");
		});
 		
		$(".xcrud-action").click(function(){
 			search_engine(); //call function
 		});
		
		function myFunction(id,assignID,orgID){
 			if (confirm("ต้องการลบใช่หรือไม่") == true) {
				location.replace('<?php echo base_url().'organization' ?>'+"/delete?key="+id+"&token="+assignID+"&orgID="+orgID);
			} else {
				location.replace('<?php echo base_url().'organization/index?token='.$token; ?>');
			}
 		}
 		
		// check enter //
		function enterpressalert(e, input){
			var phrase	=	$("#phrase").val();
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13) { //Enter keycode
				// alert('enter press');
				 search_engine();  //call function
			}
		} 
		
		function search_engine(){
			var phrase	=	$("#phrase").val();
 			var column	=	$("#column").val();
			
			if(phrase == ''){
 				$(".xcrud-search").fadeOut("slow");
				$(".xcrud-search-toggle").fadeIn();
 			}else{
				  $(".xcrud-action-1").fadeIn();
				  $.ajax({
						url: "<?php echo base_url('organization/check_search') ?>",
						type: 'POST',
						data: {
 								assignID: <?php echo $token;?>,
								search_c: phrase,
								fields: column
						},
						success: function(response) {
							//Do Something 
 								$('#put_data').html(response);
								$("#put_data").show('');
								$("#get_data").hide('');
 						},
						error: function(xhr) {
							//Do Something to handle error
							alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
							location.replace('<?php echo base_url().'organization/index?token='.$token; ?>');
						}
						
				 }); //end $.ajax
				 $(".xcrud-action-1").click(function(){
					  location.replace('<?php echo base_url().'organization/index?token='.$token; ?>');
				 });
			} //end if	
		}
		
	//}
</script>
 