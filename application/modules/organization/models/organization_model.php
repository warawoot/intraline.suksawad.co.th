<?php

class Organization_model extends MY_Model {
    
  
    function __construct() {
        parent::__construct();
        $this->table="tbl_org_chart";
        $this->pk = "orgID";
    }
        
    function max_id(){
        $this->db->select_max('orgID');
        $this->db->from(' tbl_org_chart ');
        $query      = $this->db->get();
        if($query->num_rows() > 0){
            $row  = $query->row(); 
            $data = $row->orgID; 
        }else{
            $data       =  '1'; 
        }
        return ($query->num_rows() > 0) ? $data : NULL;
    }  
    
    function getOrgName($orgID) {
        $this->db->select_max('orgName');
        $this->db->from(' tbl_org_chart ');
        $this->db->where('orgID = ',$orgID);
        $query    = $this->db->get();
        if($query->num_rows() > 0){
            $row  = $query->row(); 
            $data = $row->orgName; 
        }else{
            $data       =  '1'; 
        }
        return ($query->num_rows() > 0) ? $data : NULL;        
    }

    function dpo_list_content($assignID){
        $this->db->select('*');
        $this->db->from('`tbl_org_chart`');
        $this->db->where('`assignID`' ,$assignID);
        //$this->db->order_by('upperOrgID','asc');
        $this->db->where('`upperOrgID` !=' ,0);
        $this->db->order_by('sequence','asc');
        $query      = $this->db->get();
        $data       = $query->result_array(); 
        
        return ($query->num_rows() > 0) ? $data : NULL;
    } 
    
    function categoriesToTree(&$categories) {
     $map = array(
            0 => array('subcategories' => array())
        );
        foreach ($categories as &$category) {
            $category['subcategories'] = array();
            $map[$category['orgID']] = &$category;
        }
        foreach ($categories as &$category) {
            $map[$category['upperOrgID']]['subcategories'][] = &$category;
        }
        return $map[0]['subcategories'];
    
    }
    

    function getTree_new($assignID,$upperOrgID) {
        if(empty($upperOrgID)){
            $upperOrgID=0;
        }
        //ROOOT
        $this->db->select(' t1.orgID, 
                                `t1`.`assignID`, 
                                `t1`.`upperOrgID`, 
                                `t1`.`orgShortName`, 
                                `t1`.`orgName` name ');
        $this->db->from(' `tbl_org_chart` as t1 ');
        $this->db->where('`t1`.`assignID`' ,$assignID);
        $this->db->where('`t1`.`orgID`' , $upperOrgID); 
        $query  = $this->db->get();
        $data   = $query->result_array();
        
        //LEVEL1
        $this->db->select(' t1.orgID, 
                                `t1`.`assignID`, 
                                `t1`.`upperOrgID`, 
                                `t1`.`orgShortName`, 
                                `t1`.`orgName` name ');
        $this->db->from(' `tbl_org_chart` as t1 ');
        $this->db->where('`t1`.`assignID`' ,$assignID);         
        $this->db->where('`t1`.`upperOrgID`' ,$upperOrgID);        
        $this->db->order_by('sequence','asc');
        $query  = $this->db->get();
        $data2   = $query->result_array();

        $arr_el = array(); //[];
        foreach($data2 as $el){
            $arr_el[]=$el['orgID'];
        }

        $data = array_merge($data, $data2);
        
        //LEVEL2
        if(count($arr_el) > 0){
            $this->db->select(' t1.orgID, 
                                    `t1`.`assignID`, 
                                    `t1`.`upperOrgID`, 
                                    `t1`.`orgShortName`, 
                                    `t1`.`orgName` name ');
            $this->db->from(' `tbl_org_chart` as t1 ');
            $this->db->where('`t1`.`assignID`' ,$assignID);         
            $this->db->where_in('`t1`.`upperOrgID`' ,$arr_el);        
            $this->db->order_by('sequence','asc');
            $query  = $this->db->get();
            $data2   = $query->result_array();

            $arr_el = array(); //[];
            foreach($data2 as $el){
                $arr_el[]=$el['orgID'];
            }

            $data = array_merge($data, $data2);
        }

        //LEVEL3
        if(count($arr_el) > 0){
            $this->db->select(' t1.orgID, 
                                    `t1`.`assignID`, 
                                    `t1`.`upperOrgID`, 
                                    `t1`.`orgShortName`, 
                                    `t1`.`orgName` name ');
            $this->db->from(' `tbl_org_chart` as t1 ');
            $this->db->where('`t1`.`assignID`' ,$assignID);         
            $this->db->where_in('`t1`.`upperOrgID`' ,$arr_el);        
            $this->db->order_by('sequence','asc');
            $query  = $this->db->get();
            $data2   = $query->result_array();

            $arr_el = array(); //[];
            foreach($data2 as $el){
                $arr_el[]=$el['orgID'];
            }

            $data = array_merge($data, $data2);
        }
        //LEVEL4
        if(count($arr_el) > 0){
            $this->db->select(' t1.orgID, 
                                    `t1`.`assignID`, 
                                    `t1`.`upperOrgID`, 
                                    `t1`.`orgShortName`, 
                                    `t1`.`orgName` name ');
            $this->db->from(' `tbl_org_chart` as t1 ');
            $this->db->where('`t1`.`assignID`' ,$assignID);         
            $this->db->where_in('`t1`.`upperOrgID`' ,$arr_el);        
            $this->db->order_by('sequence','asc');
            $query  = $this->db->get();
            $data2   = $query->result_array();

            $arr_el = array(); //[];
            foreach($data2 as $el){
                $arr_el[]=$el['orgID'];
            }

            $data = array_merge($data, $data2);
        }
        //LEVEL5
        if(count($arr_el) > 0){
            $this->db->select(' t1.orgID, 
                                    `t1`.`assignID`, 
                                    `t1`.`upperOrgID`, 
                                    `t1`.`orgShortName`, 
                                    `t1`.`orgName` name ');
            $this->db->from(' `tbl_org_chart` as t1 ');
            $this->db->where('`t1`.`assignID`' ,$assignID);         
            $this->db->where_in('`t1`.`upperOrgID`' ,$arr_el);        
            $this->db->order_by('sequence','asc');
            $query  = $this->db->get();
            $data2   = $query->result_array();

            $arr_el = array(); //[];
            foreach($data2 as $el){
                $arr_el[]=$el['orgID'];
            }

            $data = array_merge($data, $data2);
        }
        return $data;
    }  

     // Builds category list
 
    //function categories($category=0,$level=0) {
    function categories($assignID,$upperOrgID) {     
       //$this->db->select('SDB.*'
                         //);



        //$this->db->from('tbl_org_chart as t1');
        //$this->db->where('t1.assignID' ,$assignID);
        if($upperOrgID !=''  ){
            /*$this->db->from(" 
                (SELECT 
                        t1.orgID, 
                        `t1`.`assignID`, 
                        `t1`.`upperOrgID`, 
                        `t1`.`orgShortName`, 
                        `t1`.`orgName` name, 
                        `t2`.`staffFName` as manager, 
                        `t3`.`staffFName` as assistants 
                    FROM `tbl_org_chart` as t1 
                    LEFT JOIN `tbl_staff` as t2 
                        ON `t1`.`managerID` = `t2`.`ID` 
                    LEFT JOIN `tbl_staff` as t3 
                        ON `t1`.`assistantID` = `t3`.`ID` 
                    WHERE `t1`.`assignID` = ".$assignID." AND (`t1`.`orgID` = ".$upperOrgID." or `t1`.`upperOrgID` = ".$upperOrgID.")) as SDB
                            "); */  

                if ($upperOrgID ==1) {
                    $this->db->select(' t1.orgID, 
                                            `t1`.`assignID`, 
                                            `t1`.`upperOrgID`, 
                                            `t1`.`orgShortName`, 
                                            `t1`.`orgName` name ');
                    $this->db->from(' `tbl_org_chart` as t1 ');
                    $this->db->where('`t1`.`assignID`' ,$assignID); 
                    
                   /* $this->db->from(" 
                                    (SELECT 
                                            t1.orgID, 
                                            `t1`.`assignID`, 
                                            `t1`.`upperOrgID`, 
                                            `t1`.`orgShortName`, 
                                            `t1`.`orgName` name 
                                        FROM `tbl_org_chart` as t1 
                                        WHERE   `t1`.`assignID` = ".$assignID."  ) as SDB
                                    "); */

                }elseif ($upperOrgID == 'null') {
                  /*  $this->db->from(" 
                                    (SELECT 
                                            t1.orgID, 
                                            `t1`.`assignID`, 
                                            `t1`.`upperOrgID`, 
                                            `t1`.`orgShortName`, 
                                            `t1`.`orgName` name 
                                        FROM `tbl_org_chart` as t1 
                                        WHERE   `t1`.`assignID` = ".$assignID."  ) as SDB
                                    "); */

                    $this->db->select(' t1.orgID, 
                                            `t1`.`assignID`, 
                                            `t1`.`upperOrgID`, 
                                            `t1`.`orgShortName`, 
                                            `t1`.`orgName` name ');
                    $this->db->from('`tbl_org_chart` as t1 ');
                    $this->db->where('`t1`.`assignID`' ,$assignID); 

                }else{

                    /*$this->db->from(" 
                                    (SELECT 
                                            t1.orgID, 
                                            `t1`.`assignID`, 
                                            `t1`.`upperOrgID`, 
                                            `t1`.`orgShortName`, 
                                            `t1`.`orgName` name   
                                        FROM `tbl_org_chart` as t1 
                                        WHERE `t1`.`assignID` = ".$assignID." 
                                        and
                                        ( 
                                          `t1`.`upperOrgID` = ".$upperOrgID." OR (t1.orgID = ".$upperOrgID." AND t1.assignID = ".$assignID.") 
                                            or (`t1`.`assignID` = ".$assignID." and `t1`.`upperOrgID` = 0)) 
                                        )

                                    as SDB ");  */

                    $this->db->select(' t1.orgID, 
                                            `t1`.`assignID`, 
                                            `t1`.`upperOrgID`, 
                                            `t1`.`orgShortName`, 
                                            `t1`.`orgName` name ');
                    $this->db->from('`tbl_org_chart` as t1 ');
                    $this->db->where('`t1`.`assignID`' ,$assignID); 
                    $this->db->where('`t1`.`upperOrgID`' ,$upperOrgID);

                    $this->db->or_where('`t1`.`orgID`' ,$upperOrgID); 
                    $this->db->where('`t1`.`assignID`' ,$assignID);

                     $this->db->or_where('`t1`.`assignID`' ,$assignID); 
                     $this->db->where('`t1`.`upperOrgID`' ,0);

                }#check $upperOrgID 
                
        }else{

            /*$this->db->from(" 
                (SELECT 
                        t1.orgID, 
                        `t1`.`assignID`, 
                        `t1`.`upperOrgID`, 
                        `t1`.`orgShortName`, 
                        `t1`.`orgName` name 
                    WHERE `t1`.`assignID` = ".$assignID.") as SDB
                            ");     */
            $this->db->select(' t1.orgID, 
                                            `t1`.`assignID`, 
                                            `t1`.`upperOrgID`, 
                                            `t1`.`orgShortName`, 
                                            `t1`.`orgName` name ');
            $this->db->from('`tbl_org_chart` as t1 ');
            $this->db->where('`t1`.`assignID`' ,$assignID); 

        }#$upperOrgID !=''

        //$this->db->order_by('orgID','asc');
        $this->db->order_by('sequence','asc');
        
        
        //$this->db->join('tbl_staff as t2' , 't1.managerID = t2.ID','left');
        //$this->db->join('tbl_staff as t3' , 't1.assistantID = t3.ID','left');
        $query  = $this->db->get();
        $data   = $query->result_array();
        //print_r($data);
        //echo $query->num_rows();
        return ($query->num_rows() > 0) ? $data : NULL;
    }
    
    /*  createTree suksawad
    function createTree(&$list, $parent, $i,$datas){
        $tree = array();
        //echo "<pre>Parent<br>";
        //print_r( $parent);
        //echo "<br>list<br>";
        //print_r($list);
        
        foreach ($parent as $k=>$l){
             
            if(isset($list[$l['orgID']])){
                $l['loop'] = $i;
                if($i == 0)
                {
                    $l['type'] = "expand";
                }
                elseif($i == 1)
                {
                    $l['type'] = "tree";                    
                }
                elseif($i > 1)
                {
                    $l['type'] = "bullet";                  
                }

                //$l['manager'] = array('name'=>'Test1','position'=>'Position1');
                //$l['manager'] = array('name'=>'นางสาวจอมพรรณ มารอด','position'=>'เจ้าพนักงานธุรการ');
                //$l['assistants'] = array(array('name'=>'Test1','position'=>'Position1'),array('name'=>'Test1','position'=>'Position1'));  
 
                //$l['assistants'] =  array(array($this->assistants($staffID ='',  $l['orgID']))  , array( $this->assistants($staffID ='',  $l['orgID']))); 
                //$l['assistants'] = array($this->assistants($staffID ='',  $l['orgID'] ,array('name'=>'Test1','position'=>'Position1')));
                
                $l['departments'] = $this->createTree($list, $list[$l['orgID']],$i+1,$datas);
                $l['manager'] = $this->manager($l['orgID'],$datas);
                $l['assistants'] = $this->assistants($status ='isConsult',  $l['orgID'],$datas);
        
            }else{
                $l['type'] = "bullet";    
                $l['loop'] = $i;
                
                //$l['manager'] = array('name'=>'นางสาวจอมพรรณ มารอด','position'=>'เจ้าพนักงานธุรการ');
                //$l['assistants'] = array(array('name'=>'Test1','position'=>'Position1'),array('name'=>'Test1','position'=>'Position1'));      
                //print_r( array($this->assistants($staffID ='',  $l['orgID'])) ); 
                
                $l['manager'] = $this->manager($l['orgID'],$datas);
                $l['assistants'] = $this->assistants($status ='isConsult',  $l['orgID'],$datas);

                if($i==0){
                    $employee = array();
                    
                    foreach ($this->employee($l['orgID'])->result() as $row) {
                        $name = $row->staffPreName.$row->staffFName.' '.$row->staffLName;
                        $employee[] = array('name'=>$name);
                    }

                    $l['departments'] = $employee;
                }

                
            }

            //------ chk_img -------//

            if(!empty($l['manager']['image'])){
                    $chk_img = 'assets/staff/'.$l['manager']['image'];
                    if(!file_exists($chk_img)){
                        $l['manager']['image']='manager-default.jpg';
                    }
            }


            $tree[] = $l;
        }
        //echo "<pre>";
        //echo $tree;
        return $tree;
    }*/


    function createTree(&$list, $parent, $i,$datas){ // createTree ehr
        $tree = array();
        
        foreach ($parent as $k=>$l){
             
            if(isset($list[$l['orgID']])){
                $l['loop'] = $i;
                if($i == 0)
                {
                    $l['type'] = "expand";
                }
                elseif($i == 1)
                {
                    $l['type'] = "tree";                    
                }
                elseif($i == 2)
                {
                    $l['type'] = "bullet";                  
                }

                //$l['manager'] = array('name'=>'Test1','position'=>'Position1');
                //$l['manager'] = array('name'=>'นางสาวจอมพรรณ มารอด','position'=>'เจ้าพนักงานธุรการ');
                //$l['assistants'] = array(array('name'=>'Test1','position'=>'Position1'),array('name'=>'Test1','position'=>'Position1'));  
 
                //$l['assistants'] =  array(array($this->assistants($staffID ='',  $l['orgID']))  , array( $this->assistants($staffID ='',  $l['orgID']))); 
                //$l['assistants'] = array($this->assistants($staffID ='',  $l['orgID'] ,array('name'=>'Test1','position'=>'Position1')));
                
                $l['departments'] = $this->createTree($list, $list[$l['orgID']],$i+1,$datas);
                $l['manager'] = $this->manager($l['orgID'],$datas);
                //$l['assistants'] = $this->assistants($status ='isConsult',  $l['orgID'],$datas);
        
            }else{
                $l['type'] = "bullet";
                $l['loop'] = $i;
                //$l['manager'] = array('name'=>'นางสาวจอมพรรณ มารอด','position'=>'เจ้าพนักงานธุรการ');
                //$l['assistants'] = array(array('name'=>'Test1','position'=>'Position1'),array('name'=>'Test1','position'=>'Position1'));      
                //print_r( array($this->assistants($staffID ='',  $l['orgID'])) ); 
                
                $l['manager'] = $this->manager($l['orgID'],$datas);
                //$l['assistants'] = $this->assistants($status ='isConsult',  $l['orgID'],$datas);
                if($i==0){
                    $employee = array();
                    
                    foreach ($this->employee($l['orgID'])->result() as $row) {
                        $name = $row->staffPreName.$row->staffFName.' '.$row->staffLName;
                        $employee[] = array('name'=>$name);
                    }

                    $l['departments'] = $employee;
                }

                
            }
            $tree[] = $l;
        }
        return $tree;
    }
    
    function assistants($status , $OrgID ,$date){ //เป็นที่ปรึกษา
        
        $this->db->select('t2.staffPreName ,`t2`.`staffFName`, `t2`.`staffLName`, `t2`.`staffImage` ,t3.positionName');
        $this->db->from('tbl_staff_work as t1');
        $this->db->where('t1.OrgID' ,$OrgID); 

        //if ($status == 'isConsult') {
            $this->db->where('t4.isConsult' ,true); // เป็นที่ปรึกษา // add new 26-11-2558 :  $this->db->where('t1.isConsult' ,true)
            $this->db->where('t1.workStartDate <=' ,$date);
			$this->db->where('t1.workStatusID !=' ,3);
			
        //} else if ($status =='isHeader') {
            //$this->db->where('t1.isHeader' ,true); // ผู้บังคับบัญชา 
            //$this->db->where('t1.workStartDate <=' ,$data);
        //}

        $this->db->join('tbl_staff as t2 ' , 't2.ID = t1.staffID','inner'); #ทะเบียนประวัติ
        $this->db->join('tbl_position as t3' ,'t3.positionID = t1.positionID' ,'inner');#ตำแหน่ง
		$this->db->join('tbl_seguence as t4' ,'t1.orgID = t4.ID' ,'inner'); // add new 26-11-2558
		
       //$this->db->limit(1);
        
        $query      = $this->db->get();
        
        //$data       =   array();
        //echo $query->num_rows();
        /*if ($query->num_rows() > 0) {
             $row  = $query->row(); 
             $data['name']      =  $row->staffPreName . $row->staffFName .' '. $row->staffLName;
             $data['position']  =  $row->positionName; 
             $data['image']     =  $row->staffImage;
        }else{
             $data['name']      = '  ';
             $data['position']  = '  ';
             $data['image']     = 'manager-default.jpg';
        }*/
       // echo $query->num_rows();
        $data   = array();
         foreach($query->result_array() as $key =>$row){
            $data[$key]['name']      =  $row['staffPreName'].$row['staffFName'] .' '.$row['staffLName'];
            $data[$key]['position']  =  $row['positionName'];//$row->staffLName;
            $data[$key]['image']     =  $row['staffImage'];
        }
        //echo "<pre>";
        //print_r($data);
        return $data;       
            
    }



    /* Manager Old suksawad
    function manager($OrgID,$date){ 
        
        $this->db->select('s.staffID, s.staffPreName ,s.staffFName, s.staffLName, s.staffImage');
        $this->db->from('tbl_staff as s');
        $this->db->where('oc.OrgID' ,$OrgID);
        $this->db->join('tbl_org_chart as oc ' , 's.staffID = oc.ManagerID','inner');
        
        $this->db->limit(1);
        $query      = $this->db->get();

        $data   = array();
        foreach($query->result_array() as $key =>$row){
            $data['name']      =  $row['staffPreName'].$row['staffFName'] .' '.$row['staffLName'];
            $data['position']  =  '';//$row['positionName'];//$row->staffLName;
            $data['image']     =  $row['staffID'].".jpg";
        }
        return $data; 
            
    }*/

    function manager($OrgID,$date){ //manager ehr
        
        $sql = "SELECT 
                    `t1`.`staffPreName`, 
                    `t1`.`staffFName`, 
                    `t1`.`staffLName`, 
                    `t1`.`staffImage`, 
                    `t1`.`staffID`, 
                    `t3`.`positionName`,
                    `t2`.`managerType` 
                FROM `tbl_staff` as t1  
                INNER JOIN `tbl_org_manager` as t2 
                    ON `t2`.`staffID` = `t1`.`ID` 
                LEFT JOIN `tbl_position` as t3 
                    ON `t3`.`positionID` = `t1`.`positionID` 
                WHERE 1 
                    AND `t2`.`managerStartDate` <= '$date' 
                    AND ((`t2`.`managerEndDate` >= '$date') OR (`t2`.`managerEndDate` IS NULL)) ";
        $sql .="    AND `t2`.`OrgID` = '$OrgID'  ";
        //$sql .= " AND `t1`.`OrgID` = '$OrgID' ";  //EHR where OrgID on tbl_staff
        $sql .= "   AND `t1`.`staffStatus` != 3 
                LIMIT 1";

        $query = $this->db->query($sql);

        $data   = array();
        if ($query->num_rows() > 0) {
             // url image : echo base_url()/uploads/$row->staffImage
             $row  = $query->row(); 
             $data['name']      =  $row->staffPreName.$row->staffFName .' '.$row->staffLName;
            
            if($row->managerType==1){
                $data['position']  =  $row->positionName;//$row->staffLName;
            }else{
                if($row->managerType == '2')  $type = 'รักษาการ';
                elseif($row->managerType == '3')  $type ='ปฏิบัติงานแทน';
                $data['position']  =  $row->positionName."\r\n (".$type.")";//$row->staffLName;
            }
             
             if($row->staffID != '') {
                 if(file_exists("assets/staff/".$row->staffID.'.jpg')) {
                     $data['image'] = $row->staffID.'.jpg';
                 } else {
                     $data['image'] = 'manager-default.jpg';
                 }
             }

        }else{
             
             $data['name']      = ' ';
             $data['position']  = ' ';
             $data['image']     = 'manager-default.jpg';

        }
        //echo " xxxx";
        //echo "<pre>";
        //print_r($data);
        return $data; 
            
    }



    function employee($OrgID){ //employee
        //select  from tbl_staff where `status` = '0' 

        $this->db->select('staffPreName,staffFName,staffLName');
        $this->db->from('tbl_staff');
        $this->db->where('status' ,0);
        $this->db->where('orgID' ,$OrgID);
        $query      = $this->db->get();
        return $query; 
            
    }

    function sub_categories($assignID) {     
       
        $this->db->select('t1.orgID , t1.assignID ,t1.upperOrgID , t1.orgShortName,t1.orgName,t2.staffFName as staffFName1 ,t3.staffFName as staffFName2');
        $this->db->from('tbl_org_chart as t1');
        $this->db->where('t1.assignID' ,$assignID);
        //$this->db->where('t1.upperOrgID' ,$orgID);
        //$this->db->where('t1.orgID' ,$orgID);
        $this->db->join('tbl_staff as t2' , 't1.managerID = t2.ID','inner');
        $this->db->join('tbl_staff as t3' , 't1.assistantID = t3.ID','inner');
        $this->db->order_by('sequence','asc');
		$query      = $this->db->get();
        $data       = $query->row_array();
        return ($query->num_rows() > 0) ? $data : NULL;
    }
    
    function count_level($assignID) {    
        $this->db->select('count(*)');
        $this->db->from('tbl_org_chart as t1');
        $this->db->where('t1.assignID' ,$assignID); 
        $query      = $this->db->get();
        $data       = $query->result();
        return ($query->num_rows() > 0) ? $data : NULL;
    }
    
    function has_orgID($rows,$id) {
      foreach ($rows as $row) {
        if ($row['upperOrgID'] == $id)
          return true;
      }
      return false;
    }
    
    function build_menu($rows,$parent=0){  
      $level    =   1;
      $num  =   1;
      $result = "<ul>";
      foreach ($rows as $row)
      { //str_repeat("-", $level).
        if ($row['upperOrgID'] == $parent){
          $result.= "<li>  $row[orgShortName]   ";
          if ($this->has_orgID($rows,$row['orgID']))
            $result.= $this->build_menu($rows,$row['orgID']);
          $result.= "</li>";
        } ;
         
      }
      $result.= "</ul>";
    
      return $result;
    }
    
    
     
    function generateTree($datas, $parent = 0, $limit=0 ){
        if($limit > 1000) return ''; // Make sure not to have an endless recursion
            $level = 0;
            $tree = '';
            $num    = 1;
         
            $token  = $this->input->get('token');
            //$tree = '<table border="0" width="100%" class="table table-bordered table-hover" style="margin-top:5px;">';
            $level1 = 0;
            for($i=0, $ni=count($datas); $i < $ni; $i++){
                 $level++;
                 $prefix=''; 
                 $button_view   = base_url().'organization/view?token='.$token.'&orgID='.$datas[$i]['orgID'];
                 $button_edit   = base_url().'organization/edit?token='.$token.'&orgID='.$datas[$i]['orgID'];
                 
                    if($datas[$i]['upperOrgID'] == $parent){
                        $tree .= '<tr>';
                         $c = 0;
                         $append = '';
                            while ($c <  $i) {
                                $append.="-";
                                $c++;
                            }
                        $tree .= '<td>';
                            $tree .=  $level;
                        $tree .= '</td>';
                        /*$tree .= '<td>';
                            $tree .= $datas[$i]['orgID'];
                        $tree .= '</td>';
                        $tree .= '<td>';
                            $tree .= $datas[$i]['upperOrgID'];
                        $tree .= '</td>';   */
                        $tree .= '<td>';    
                        if ($datas[$i]['upperOrgID'] == '0') {
                            $tree .=  $datas[$i]['orgShortName'] ;
                        }else{
                            $tree .=  $append . $datas[$i]['orgShortName'] ;
                        }
                        $tree .= '</td>';
                        $tree .= '<td>';
                            $tree .= $datas[$i]['orgName']   ;
                        $tree .= '</td>';
                        $tree .= '<td>';
                            $tree .= $datas[$i]['staffFName1'];
                        $tree .= '</td>';
                        $tree .= '<td>';
                            $tree .= $datas[$i]['staffFName2'];
                        $tree .= '</td>';
                        $tree .= '<td>';
                            $tree .= '
                            <span class="btn-group"><a class="xcrud-action btn btn-info btn-sm" title="ดูข้อมูล" href="'.$button_view.'" data-primary="4" data-task="view"><i class="glyphicon glyphicon-search"></i></a><a class="xcrud-action btn btn-warning btn-sm" title="แก้ไข" href="'.$button_edit.'" data-primary="4" data-task="edit"><i class="glyphicon glyphicon-edit "></i></a><a class="xcrud-action btn btn-danger btn-sm" title="ลบ" href="javascript:;" data-primary="4" data-task="remove" data-confirm="ต้องการลบใช่หรือไม่?"><i class="glyphicon glyphicon-remove"></i></a></span>
                            ';
                        $tree .= '</td>'; 
                        $tree .= $this->generateTree($datas, $datas[$i]['orgID'], $limit++,$token);
                        $tree .= '</tr>';
                        
                    }
            }
            //$tree .= '</table>';
        return $tree;
    }
    
    function getCategoryTree($level = 0 , $prefix = '' , $assignID=0 ) {
        
        $token  = $this->input->get('token');
        $rows = $this->db
            //->select('t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName, t2.staffFName as staffFName1 , t2.staffLName as staffLName11
//, t3.staffFName as staffFName2 , t3.staffLName as staffLName22')
            ->select('t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t1.upperOrgID')
            //->where('t1.upperOrgID_modify', $level)
            ->where('t1.upperOrgID', $level)
            ->where('t1.assignID', $assignID)
            //->join('tbl_staff as t2' , 't1.managerID = t2.ID','left')
            //->join('tbl_staff as t3' , 't1.assistantID = t3.ID','left')
            ->order_by('sequence','asc') //mod by bomb == ->order_by('orgID','asc')
            ->get('tbl_org_chart as t1')
            ->result();
         ;
		
        $tree = '';
        // echo $this->db->last_query(); exit;
        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {//<i class="fa fa-caret-right"></i>
                $button_view_structure    = base_url().'report/tree/index?assignDate='.date('Ymd').'&ddl='.$row->orgID; 
                $button_view    = base_url().'organization/view?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID;
                $button_edit    = base_url().'organization/edit?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID.'&upperOrgID='.$row->upperOrgID;
                $button_delete  = base_url().'organization/delete?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID;
                $button_manage  = base_url().'organization/manage?orgID='.$row->orgID.'&assignID='.$token;
                    //$tree .= $prefix  . $row->orgShortName   . "\n";
                    // Append subcategories
                    
                    if( $prefix == ""){
                            $bg  = " background-color: #00B0FF;  color: #000";
                    }elseif( $prefix == "--"){
                            $bg  = " background-color: #FFF59D;  color: #000";
                    }elseif( $prefix == "----"){
                            $bg  = " background-color: #FFCC80;  color: #000";
                    }elseif( $prefix == "------"){
                            $bg  = " background-color: #9fd457c7;  color: #000";
                    }elseif( $prefix == "--------"){
                            $bg  = " background-color: #d1efa7d9;  color: #000";
                    }else{
                            $bg  = " background-color: #EFEFEF;  color: #000";
                    }
                    
                    $tree .= '<tr style="  '.$bg.' ">';
                         
                        $tree .= '<td>';
                        if($row->upperOrgID ==0){
                            $tree .=   '<i class="fa fa-caret-down"></i> '  . $row->orgShortName ;
                        }else{
                            $tree .=   $prefix.'&nbsp;'. $row->orgShortName ;
                        }   
                        $tree .= '</td>';
                        $tree .= '<td>';
                            $tree .= $prefix.'&nbsp'.$row->orgName   ;
                        $tree .= '</td>';
                         
                        $tree .= '<td>';
                        
                            // $tree .= '
                            // <span class="btn-group"><a class="xcrud-action btn btn-info btn-sm" title="ดูข้อมูล" href="'.$button_view.'" data-primary="4" data-task="view"><i class="glyphicon glyphicon-search"></i></a><a class="xcrud-action btn btn-warning btn-sm" title="แก้ไข" href="'.$button_edit.'" data-primary="4" data-task="edit"><i class="glyphicon glyphicon-edit "></i></a><a class="xcrud-action btn btn-danger btn-sm" title="ลบ" href="'.$button_delete.'" data-primary="4" data-task="remove" data-confirm="ต้องการลบใช่หรือไม่?"><i class="glyphicon glyphicon-remove"></i></a></span>
                            // ';


                            /// ซ่อนปุ่ม edit delet
                        
                           $tree .= '
                            <span class="btn-group">
                            <a class="xcrud-action btn btn-success btn-sm" title="จัดการ" href="'.$button_manage.'" data-primary="4" data-task="edit"><i class="glyphicon glyphicon-user "></i></a>
                            <a class="xcrud-action btn btn-default btn-sm" title="แสดงโครงสร้าง" href="'.$button_view_structure.'" data-primary="4" data-task="view"><i class="fa fa-sitemap"></i></a>
                            <a class="xcrud-action btn btn-info btn-sm" title="ดูข้อมูล" href="'.$button_view.'" data-primary="4" data-task="view"><i class="glyphicon glyphicon-search"></i></a>
                            <a class="xcrud-action btn btn-warning btn-sm" title="แก้ไข" href="'.$button_edit.'" data-primary="4" data-task="edit"><i class="glyphicon glyphicon-edit "></i></a><a class="xcrud-action btn btn-danger btn-sm" title="ลบ" href="#" data-primary="4" data-task="remove" data-confirm="ต้องการลบใช่หรือไม่?" onclick="myFunction('.$row->id.','.$row->assignID.','.$row->orgID.')"><i class="glyphicon glyphicon-remove"></i></a></span>
                            ';
                            
                        $tree .= '</td>';
                        
                    
                        $tree .= '</tr>';
                    $tree .= $this->getCategoryTree($row->orgID, $prefix . '--',$row->assignID);
            }
        }
        return $tree;
    }
    
    function getCategoryTree_tree($level = 0 , $prefix = '' , $assignID=0 ) {
        
        $token  = $this->input->get('token');
        $rows = $this->db
            //->select('t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName, t2.staffFName as staffFName1 , t2.staffLName as staffLName11
//, t3.staffFName as staffFName2 , t3.staffLName as staffLName22')
            ->select('t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t1.upperOrgID')
            //->where('t1.upperOrgID_modify', $level)
            ->where('t1.upperOrgID', $level)
            ->where('t1.assignID', $assignID)
            //->join('tbl_staff as t2' , 't1.managerID = t2.ID','left')
            //->join('tbl_staff as t3' , 't1.assistantID = t3.ID','left')
            ->order_by('orgID','asc')
            ->get('tbl_org_chart as t1')
            ->result();
         ;
        $tree = '';
         
        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {//<i class="fa fa-caret-right"></i>
                 
                $button_view    = base_url().'organization/view?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID;
                $button_edit    = base_url().'organization/edit?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID.'&upperOrgID='.$row->upperOrgID;
                $button_delete  = base_url().'organization/delete?key='.$row->id.'&token='.$token.'&orgID='.$row->orgID;
                    //$tree .= $prefix  . $row->orgShortName   . "\n";
                    // Append subcategories
                    
                    if( $prefix == "--"){
                            $bg  = " background-color: #006666;  color: #ffffff";
                    }else{
                            $bg  = " color: #837C7C";
                    }
                    if($row->upperOrgID==0){
                        $tree_css = 'class="treegrid-1 treegrid-expanded"';
                    }else{
                        $tree_css = '';
                    }
                    
                    $tree .= '<tr style="  '.$bg.' " '.$tree_css.'>';
                         
                        $tree .= '<td>';
                        if($row->upperOrgID ==0){
                            $tree .=   '<i class="fa fa-caret-down"></i> <span class="treegrid-expander"></span>'  . $row->orgShortName ;
                        }else{
                            $tree .=   $prefix.'&nbsp;'. $row->orgShortName ;
                        }   
                        $tree .= '</td>';
                        $tree .= '<td>';
                            $tree .= $row->orgName   ;
                        $tree .= '</td>';
                         
                        $tree .= '<td>';
                            /*$tree .= '
                            <span class="btn-group"><a class="xcrud-action btn btn-info btn-sm" title="ดูข้อมูล" href="'.$button_view.'" data-primary="4" data-task="view"><i class="glyphicon glyphicon-search"></i></a><a class="xcrud-action btn btn-warning btn-sm" title="แก้ไข" href="'.$button_edit.'" data-primary="4" data-task="edit"><i class="glyphicon glyphicon-edit "></i></a><a class="xcrud-action btn btn-danger btn-sm" title="ลบ" href="'.$button_delete.'" data-primary="4" data-task="remove" data-confirm="ต้องการลบใช่หรือไม่?"><i class="glyphicon glyphicon-remove"></i></a></span>
                            ';*/
                            $tree .= '
                            <span class="btn-group"><a class="xcrud-action btn btn-info btn-sm" title="ดูข้อมูล" href="'.$button_view.'" data-primary="4" data-task="view"><i class="glyphicon glyphicon-search"></i></a><a class="xcrud-action btn btn-warning btn-sm" title="แก้ไข" href="'.$button_edit.'" data-primary="4" data-task="edit"><i class="glyphicon glyphicon-edit "></i></a><a class="xcrud-action btn btn-danger btn-sm" title="ลบ" href="#" data-primary="4" data-task="remove" data-confirm="ต้องการลบใช่หรือไม่?" onclick="myFunction('.$row->id.','.$row->assignID.','.$row->orgID.')"><i class="glyphicon glyphicon-remove"></i></a></span>
                            ';
                        $tree .= '</td>'; 
                        $tree .= '</tr>';
                    $tree .= $this->getCategoryTree_tree($row->orgID, $prefix . '--',$row->assignID);
            }
        }
        return $tree;
    }
    
    function getDropdownTree($rootOrg = 0 , $prefix = '' , $type='' ,$selectedOrg) {
        
        $this->db->select('t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t1.upperOrgID');            
        $this->db->order_by('upperOrgID','asc');
        $this->db->order_by('sequence','asc');
        
        if($type == 'sub') {
            $this->db->where('t1.upperOrgID', $rootOrg);
        } else {
            $this->db->where('t1.orgID', $rootOrg);
        }

        $sql = $this->db->get('tbl_org_chart as t1');
        $rows = $sql->result();
        echo "<!-- RootORG=" . $rootOrg .", SQL=" . $this->db->last_query()."<br>-->";
         
        $tree = '';
         
        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {//<i class="fa fa-caret-right"></i>
                    if($selectedOrg == $row->orgID){ 
                        $selected =  "selected='selected'" ;
                    }else{
                        $selected = ""; 
                    }
                    // Append subcategories
                    if($selectedOrg == ''){
                        $tree  .=  "<option value='".$row->orgID."' >";
                    }else{
                        $tree  .=  "<option value='".$row->orgID."' ".$selected.">";    
                    }
                    $tree  .= $prefix .$row->orgName;
                    $tree  .=  "</option>";
                         
                    $tree .= $this->getDropdownTree($row->orgID, $prefix . '--','sub' , $selectedOrg);
            }
        }
        return $tree;
    }

    function getSubOrg($orgID) {
        $agencies = "";
        $sql    = "SELECT orgID FROM tbl_org_chart WHERE upperOrgID=".$orgID;
        $query  = $this->db->query($sql);
        $rows   = $query->result();
        if($query->num_rows())
        {
            foreach($rows as $row) {
                $agencies .= ",".$row->orgID;
                $agencies .= $this->getSubOrg($row->orgID);
            }        
        }        
        return $agencies;    
    }

    function getOrgByManager($managerID) {
        $sql = "SELECT 
                    o.id, 
                    o.orgName, 
                    o.orgID, 
                    o.orgShortName  
                FROM tbl_org_chart o 
                INNER JOIN tbl_staff s
                    ON o.OrgID = s.manageOrgID 
                WHERE
                    s.staffID=".$managerID;
        echo "<!--".$sql."-->";
        $orgName = $this->db->query($sql);
        if($orgName->num_rows())
        {
            return $orgName->result();            
        }
        else {
            return null;
        }
        /*
        if($orgName->row_count() > 0) {
            return $orgName->result();  
        } else {
            return null;
        }
        */
        /*
        $rows = $this->db
            ->select('t1.id, t1.orgID, t1.orgShortName, t1.orgName ')
             
            ->where('t1.managerID', $managerID)
            ->order_by('upperOrgID','asc')
            ->order_by('sequence','asc')
            ->get('tbl_org_chart as t1')
            ->result();
        return $rows;
        */
        
    }

    function getDropdownManager($managerID = 0 ,$prefix = '' ,$assignID=0 ,$OrgID) {
        $rows = $this->db
            ->select('t1.id, t1.orgID, t1.orgShortName, t1.orgName ')
             
            ->where('t1.managerID', $managerID)
            ->order_by('upperOrgID','asc')
            ->order_by('sequence','asc')
            ->get('tbl_org_chart as t1')
            ->result();
         ;
        $tree = '';
         
        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {//<i class="fa fa-caret-right"></i>
                    if($OrgID == $row->orgID){ 
                        $selected =  "selected='selected'" ;
                    }else{
                        $selected = ""; 
                    }
                    if($OrgID == ''){
                        $tree  .=  "<option value='".$row->orgID."' >";
                    }else{
                        $tree  .=  "<option value='".$row->orgID."' ".$selected.">";    
                    }
                    $tree  .= $prefix .$row->orgName;
                    $tree  .=  "</option>";                         
            }
        }
        return $tree;
    }

    function delete_orgchart($orgID,$assignID){
        $this->db->where('orgID', $orgID);
        $this->db->where('assignID', $assignID);
        $this->db->delete('tbl_org_chart'); 
    } 
     
    
    function distinct_orgchart($orgID,$token){
        $this->db->select('orgID');
        $this->db->from('tbl_org_chart as t1');
        $this->db->where('t1.orgID' ,$orgID); 
        $this->db->where('t1.assignID' ,$token); 
        $query      = $this->db->get();
        $data       = $query->result();
         
        return ($query->num_rows() > 0) ? $data : NULL;
    }
    
    function insert_org_chart($data){
        
        $orgID      =   $data['orgID'];
        $assignID   =   $data['assignID'];
         
        $this->db->select('orgID');
        $this->db->from('tbl_org_chart as t1');
        $this->db->where('t1.orgID' ,$orgID); 
        $this->db->where('t1.assignID' ,$assignID); 
        $query      = $this->db->get();
        //$data         = $query->result();
        //echo $query->num_rows();
        if($query->num_rows() == 0){
                $sql    =   $this->db->insert("tbl_org_chart",$data);
                //exit();    
                if($this->db->affected_rows() > 0){
                         return 'succesfully';
                }else{
                         return 'failed';
                }
        }else{
                return 'failed';
        }
            
    }
    
    function update_org_chart($id,$orgID,$assignID,$data){
            
            $this->db->where('id', $id);
            $this->db->where('orgID', $orgID);
            $this->db->where('assignID', $assignID);
            $this->db->update('tbl_org_chart',$data);   
            // echo $this->db->affected_rows();
            if($this->db->affected_rows() > 0){
                return 'succesfully';
            }else{
                return 'failed';
            }
    }
    
    
    
    function get_dropdown_all($id , $name ,$table,$token){
            $this->db->where('assignID', $token);
            $this->db->order_by($name, "asc");
            $sql        = $this->db->get($table);
             
            $dropdowns  = $sql->result();
            
            if ($sql->num_rows() > 0){
                foreach($dropdowns as $dropdown){
                //echo $fruit = array_shift($dropdown);
                $dropdownlist['']   = "- none -";
                //if($dropdown->upperOrgID != 0){
                    //$dropdownlist[$dropdown->$id] = '--'.$dropdown->$name;
                //}else{
                    $dropdownlist[$dropdown->$id]   = $dropdown->$name;
                //}
            }
            }else{
                    $dropdownlist['']   = "- none -";
            } 
            
            $finaldropdown  = $dropdownlist;
            return $finaldropdown;
    }   
    
    function toDropdown($arr)
    {
        foreach ($arr as $row) {
            $cat[$row['id']] = $row['name'];
            if ($row['parent'] != 0) {
                $cat[$row['id']] = '--' . $row['name'];
            }
        }
        return $cat;
    }
    
    function search_all($search ,$orgID , $assignID){
            //$search   =   $this->input->post('search');
            //$array = array('orgShortName' => $search, 'orgName' => $search, 'sname_th' => $search, 'CONCAT( t1.a," ",t1.b )' => $search);
            
            $array = array('orgShortName' => $search, 'orgName' => $search );
            
            $this->db->select('t1.id , t1.orgID , t1.assignID , t1.orgShortName ,t1.orgName ,t1.upperOrgID ');
            $this->db->from('`tbl_org_chart` as t1');
            
            $this->db->or_like($array); 
             
            $this->db->order_by('t1.orgShortName');
            $this->db->where('t1.orgID',$orgID);
            $this->db->where('t1.assignID',$assignID);
            
            $query = $this->db->get();
            $data = $query->result();
             
            return ($query->num_rows() > 0) ? $data : NULL;
        
    }
    
    function getCategorySearch($level = 0 , $prefix = '' , $assignID  , $search , $fields) {
        //$array = array('orgShortName' => $search, 'orgName' => $search,  'CONCAT( t1.orgShortName," ",t1.orgName )' => $search );
        if($fields == ''){
            $array = array('orgShortName' => $search, 'orgName' => $search);
        }else{
            $array = array( $fields => $search );
        }
        $rows = $this->db
             
            ->select('  `t1`.`id`, `t1`.`orgID`, `t1`.`assignID`, `t1`.`upperOrgID`, `t1`.`orgShortName`, `t1`.`orgName`  ')
            //->where('t1.upperOrgID', $level)
            ->where('t1.assignID', $assignID) 
            ->or_like($array, 'after') 
            //->like($array)
            ->order_by('t1.orgID','asc')
            ->get('tbl_org_chart as t1')
            ->result();
         ;
         
        $tree = '<table border="0" width="100%" class="table table-bordered table-hover" style="margin-top:5px; font-weight:bold;">
                        <thead> 
                            <tr style="background-color:#efefef;">
                               <!-- <th>ลำดับ</th>-->
                               <!-- <th>org id</th>
                                <th>upper org id</th>-->
                                <th>ชื่อย่อ</th>
                                <th>ชื่อหน่วยงาน</th>
                                 <!--<th>ผู้บังคับบัญชา</th>  
                               <th>ที่ปรึกษา</th>-->
                               <th>&nbsp;</th>  
                            </tr>
                         </thead>';
         
        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {
                 
                $button_view    = base_url().'organization/view?key='.$row->id.'&token='.$assignID.'&orgID='.$row->orgID;
                $button_edit    = base_url().'organization/edit?key='.$row->id.'&token='.$assignID.'&orgID='.$row->orgID.'&upperOrgID='.$row->upperOrgID;
                $button_delete  = base_url().'organization/delete?key='.$row->id.'&token='.$assignID.'&orgID='.$row->orgID;
                    // Append subcategories
                    $tree .= '<tr style="color: #837C7C;">';
                         
                        $tree .= '<td>';
                        if($row->upperOrgID ==0){
                            $tree .=   '<i class="fa fa-caret-down"></i> '  . $row->orgShortName ;
                        }else{
                            $tree .=   $prefix.'&nbsp;'. $row->orgShortName ;
                        }   
                        $tree .= '</td>';
                        $tree .= '<td>';
                            $tree .= $row->orgName   ;
                        $tree .= '</td>';
                         
                        $tree .= '<td>';
                        $tree .= '
                            <span class="btn-group"><a class="xcrud-action btn btn-info btn-sm" title="ดูข้อมูล" href="'.$button_view.'" data-primary="4" data-task="view"><i class="glyphicon glyphicon-search"></i></a><a class="xcrud-action btn btn-warning btn-sm" title="แก้ไข" href="'.$button_edit.'" data-primary="4" data-task="edit"><i class="glyphicon glyphicon-edit"></i></a><a class="xcrud-action btn btn-danger btn-sm" title="ลบ" href="#" data-primary="4" data-task="remove" data-confirm="ต้องการลบใช่หรือไม่?" onclick="myFunction('.$row->id.','.$row->assignID.','.$row->orgID.')"><i class="glyphicon glyphicon-remove"></i></a></span>
                            ';
                        $tree .= '</td>'; 
                        $tree .= '</tr>';
                    //$tree .= $this->getCategoryTreeSearch($row->upperOrgID, $prefix . '--',$row->assignID, $search , $fields);
            }
        }else{
            $tree   .=" <tr>";
            $tree   .="     <td colspan=\"11\">ไม่พบข้อมูล</td>";
            $tree   .=" </tr>";
        }
        $tree   .="</table>";
        //echo $tree;
        return $tree;
    } 
    
    function get_content($id){
        $this->db->select('*');
        $this->db->from('`tbl_org_chart`');
        $this->db->where('`id`' ,$id);
        $query      = $this->db->get();
        $data       = $query->row_array(); 
        
        return ($query->num_rows() > 0) ? $data : NULL;
    } 
     
    
   /*protected function getCategoryTree($level = 0, $prefix = '') {
        $rows = $this->db
            ->select('id,parent_id,name')
            ->where('parent_id', $level)
            ->order_by('id','asc')
            ->get('categories')
            ->result();
    
        $category = '';
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $category .= $prefix . $row->name . "\n";
                // Append subcategories
                $category .= $this->getCategoryTree($row->id, $prefix . '-');
            }
        }
        return $category;
    }
    
    public function printCategoryTree() {
        echo $this->getCategoryTree();
    }*/
     
	function getDropdownTreeTscore($level = 0 , $prefix = '' , $assignID=0 ,$upperOrgID) {
        
        $token  = $this->input->get('token');
        $rows = $this->db
            ->select('t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t1.upperOrgID')
             
            ->where('t1.upperOrgID', $level)
            ->where('t1.assignID', $assignID)
            ->like('t1.orgName', 'ฝ่าย', 'after')
            
			->order_by('orgID','asc')
            ->get('tbl_org_chart as t1')
            ->result();
         ;
        $tree = '';
         
        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {//<i class="fa fa-caret-right"></i>
			
                    if($upperOrgID == $row->orgID){ 
                        $selected =  "selected='selected'" ;
                    }else{
                        $selected = ""; 
                    }
                    // Append subcategories
                    if($upperOrgID == ''){
                        $tree  .=  "<option value='".$row->orgID."' >";
                    }else{
                        $tree  .=  "<option value='".$row->orgID."' ".$selected.">";    
                    }
                    $tree  .= $prefix .$row->orgName;
                    $tree  .=  "</option>";
                         
                    $tree .= $this->getDropdownTreeTscore($row->orgID, $prefix . '--',$row->assignID , $upperOrgID);
            }
        }
        return $tree;
    } 
	 



    //  --- ORG MANAGE --- //
    function listManage($orgID,$assignID){

        $this->db->select('t1.managerID,t1.orgID,t2.staffID,t1.staffID as id,t2.staffPreName,t2.staffFName,t2.staffLName,t1.managerStartDate,t1.managerEndDate,t1.managerType');
        $this->db->from('tbl_org_manager as t1');
        $this->db->join('tbl_staff as t2', 't1.staffID = t2.ID');
        $this->db->where('t1.orgID',$orgID);
        $this->db->where('assignID',$assignID);
        $this->db->order_by('t1.managerStartDate');
        $this->db->group_by('t1.managerID');
        $query = $this->db->get();
        $data = $query->result();

        return ($query->num_rows() > 0) ? $data : NULL;
    }

    function insertManage($arr){
        
        $this->db->insert('tbl_org_manager', $arr); 

    }

    function updateManage($ID,$orgID,$arr){
        
        $this->db->where('staffID', $ID);
        $this->db->where('orgID', $orgID);
        $this->db->update('tbl_org_manager', $arr); 

    }

    function editManage($staffID,$orgID,$assignID){
        
        $this->db->select('*');
        $this->db->from('tbl_org_manager as t1');
        $this->db->where('t1.orgID',$orgID);
        $this->db->where('t1.staffID',$staffID);
        $this->db->where('t1.assignID',$assignID);
        
        $query = $this->db->get();
        $data = $query->result();
            
        return ($query->num_rows() > 0) ? $data : NULL;
    }


    function getOrganization($orgID=''){
        $this->db->select('orgName,orgID')->from('tbl_org_chart');
        $this->db->where('orgID',$orgID);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query : NULL;


    }
	 
} 