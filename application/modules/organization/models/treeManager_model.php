<?
 

class treeManager extends MY_Model {

  
  private static $instance;
  public static function get() {
    return (!treeManager::$instance instanceof self) ? treeManager::$instance = new self($config) : treeManager::$instance;
  }

 
  function getTree( $array, $id_key = 'id', $parent_id_key = 'parent_id', $weight_key = 'weight' ){
    _assert( is_array( $array ), "need array!" );
    // assign children to every element
    foreach( $array as $index => $element )
      $array[ $index ]['children'] = $this->getChildren( $element[ $id_key ], $parent_id_key, $id_key, $weight_key, $array );
    // now remove non-parent nodes from root
    foreach( $array as $index => $element )
      if( $element[ $parent_id_key ] > 0 )
        unset( $array[ $index] );
    return $array;
  }

 
  function getChildren( $parent_id_value, $parent_id_key, $id_key, $weight_key, $array ){
    if( !is_array( $array ) ) return false;
    $children = array();
    // check if children look for their parent
    foreach( $array as $key => $item ){
      _assert( isset( $item[ $parent_id_key ] ), "parent_id key '{$parent_id_key}' not set in each array element...cannot build tree");
      if( $item[ $parent_id_key ] == $parent_id_value ){
        $weight     = $item[ $weight_key ];
        while( isset( $children[ $weight ] ) )
          $weight++;
        $children[ $weight ] = $item;
      }
    }
    // check if children have children
    foreach( $children as $key => $child ){
      $children[ $key ]['children'] = $this->getChildren( $child[ $id_key ], $parent_id_key, $id_key, $weight_key, $array );
    }
    ksort( $children );
    return $children;
  }

  
  function slapTree( $array, $indentSize = 3, $indentKey = "title_menu", $glue = "/", $spacer = " ",$child_key = "children", $firsttime = true )
  {
    global $slappedArray;
    if( !is_array($slappedArray) || $firsttime ){
      $slappedArray = array();
      $array = $this->addIndents( $array, $child_key, $indentSize, $indentKey, $glue, $spacer );
    }
    if( !is_array( $array ) ) return false;
    if( is_array( $array ) ){
      foreach( $array as $key => $element ){
        $children       = $element[ $child_key ];
        unset( $element[ $child_key ] );
        $slappedArray[] = $element;
        if( is_array( $children ) )
          $this->slapTree( $children, $indentSize, $indentKey, $glue, $spacer, $child_key, false );
      }
    }
    return $slappedArray;
  }

  
  function addIndents( $array, $child_key, $indentSize, $indentKey, $glue,  $spacer, $_level = 1 ){
    global $path;
    if( !is_array( $array ) ) return false;
    foreach( $array as $key => $element ){
      if( isset( $element['indent'] ) ) return;
      if( $_level == 1 ){
        $array[ $key ]['indent'] = 0;
        $array[ $key ][ "{$indentKey}_indent" ]   = $glue . $element[ $indentKey ];
        $array[ $key ][ "{$indentKey}_path" ]     = $glue . $element[ $indentKey ];
      }
      if( is_array( $element[ $child_key ] ) ){
        $path_bak  = $path;
        $path     .= $glue . $element[ $indentKey ];
        $array[ $key ][ $child_key ] = $this->addIndents( $array[ $key ][$child_key ], $child_key, $indentSize, $indentKey, $glue, $spacer, $_level + 1 );
        foreach( $element[ $child_key ] as $k => $child ){
          $array[ $key ][ $child_key ][$k]["indent"]                = $indent = $_level * $indentSize;
          $array[ $key ][ $child_key ][$k]["{$indentKey}_path"]     = $path . $glue . $child[ $indentKey ];
          $array[ $key ][ $child_key ][$k]["{$indentKey}_indent"]   = "{$glue} {$child[ $indentKey ]}";
          for( $i = 0; $i < (int)$indent; $i++ )
            $array[ $key ][ $child_key ][$k]["{$indentKey}_indent"] = $spacer . $array[ $key ][ $child_key ][$k]["{$indentKey}_indent"];
        }
        $path = $path_bak;
      }
    }
    return $array;
  }

   
  function moveNode( $action, $id_key, $id_value, $array, $weight_key = "weight" ){
    $candidate    = false;
    $neighbour    = false;
    $brothers     = array();
    // search our beloved candidate
    foreach( $array as $key_candidate => $node )
      if( $node[ $id_key ] == $id_value )
        $candidate = $node;
    // search for his brothers
    foreach( $array as $key_candidate => $node )
      if( $candidate['parent_id'] == $node['parent_id'] )
        $brothers[] = $node;
    // if candidate found lets try to find nearest neighbour
    for( $i = 0; $i < count($brothers); $i++ ){
      if( $brothers[ $i ]['id'] == $candidate['id'] ){
        $i = ( $action == "up" ) ? $i - 1 : $i + 1;
        break;
      }
    }
    $neighbour = ( $i >= 0 || $i < count($brothers) ) && isset( $brothers[$i] ) ? $brothers[$i] : false;
    // lets swap weights
    if( $candidate && $neighbour ){
       $neighbour_weight          = $neighbour[ $weight_key ];
       $neighbour[ $weight_key ]  = $candidate[ $weight_key ];
       $candidate[ $weight_key ]  = $neighbour_weight;
    }
    return ( $neighbour &&  $candidate ) ? array( $candidate, $neighbour ) : false;
  }
}

?>
