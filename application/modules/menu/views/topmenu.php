<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="MegaNavbarID">
    <!-- regular link -->
<?php
if($this->session->userdata('roleID')==1) //พนักงานทั่วไป
{
?>
    <ul class="nav navbar-nav navbar-left">
        <li><a href="<?=site_url();?>dashboard"><i class="fa fa-home"></i> หน้าหลัก</a></li>
    </ul>
<?php
}
else if($this->session->userdata('roleID')==2) //M
{
?>
    <ul class="nav navbar-nav navbar-left">
        <li><a href="<?=site_url();?>dashboard"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li><a href="<?=site_url();?>absence"><i class="fa fa-home"></i> ใบลา</a></li>
        <li><a href="<?=site_url();?>rate"><i class="fa fa-home"></i> ประเมิน</a></li> 
    </ul>
<?php
} else { //HR Manager
?>
    <ul class="nav navbar-nav navbar-left">
        <li class="dropdown-grid">
            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-sitemap"></i>&nbsp;<span class="hidden-sm">องค์กร</span><span class="caret"></span></a>
            <div class="dropdown-grid-wrapper" role="menu">
                <ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <li>
                        <div id="carousel-eg">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
                                    <ol class="carousel-indicators navbar-carousel-indicators h-divided">
                                        <li data-target="#carousel-eg" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">โครงสร้างองค์กร<span class="hidden-xs desc">กำหนดโครงสร้าง และผังองค์กร</span></a></li>
                                        <li data-target="#carousel-eg" data-slide-to="1" class=""><a href="javascript:void(0);" class="">ตำแหน่งงาน<span class="hidden-xs desc">ตำแหน่งงาน อัตรากำลัง</span></a></li>
                                        <li data-target="#carousel-eg" data-slide-to="2" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐาน<span class="hidden-xs desc">ข้อมูลพื้นฐานที่เกี่ยวข้อง</span></a></li>
                                    </ol>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>appoint"><i class="fa fa-file"></i>&nbsp; จัดการโครงสร้างองค์กร</a></li>
                                                    <li><a href="<?=site_url();?>reportturnover"><i class="fa fa-file"></i>&nbsp; รายงานอัตรากำลัง </a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>structure"><i class="fa fa-file"></i>&nbsp; จัดการตำแหน่งงาน</a></li>
                                                    <li><a href="<?=site_url();?>sequence"><i class="fa fa-file"></i>&nbsp; อัตรากำลัง</a></li>
                                                    <li><a href="<?=site_url();?>reportposition"><i class="fa fa-file"></i>&nbsp; ตำแหน่งว่าง</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>position"><i class="fa fa-file"></i>&nbsp; ชื่อตำแหน่ง</a></li>
                                                    <!--<li><a href="<?=site_url();?>level"><i class="fa fa-file"></i>&nbsp; ชื่อระดับ</a></li>-->
                                                    <li><a href="<?=site_url();?>workplace"><i class="fa fa-file"></i>&nbsp; สถานที่ทำงาน</a></li>
                                                    <li><a href="<?=site_url();?>workstatus"><i class="fa fa-file"></i>&nbsp; สถานะพนักงาน</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>

        <li class="dropdown-grid">
            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-user"></i>&nbsp;<span class="hidden-sm">พนักงาน</span><span class="caret"></span></a>
            <div class="dropdown-grid-wrapper" role="menu">
                <ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <li>
                        <div id="carousel-eg2">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
                                    <ol class="carousel-indicators navbar-carousel-indicators h-divided">
                                        <li data-target="#carousel-eg2" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">ทะเบียนประวัติ<span class="hidden-xs desc">ข้อมูลทะเบียนประวัติของพนักงาน</span></a></li>
                                        <li data-target="#carousel-eg2" data-slide-to="1" class=""><a href="javascript:void(0);" class="">เอกสารสัญญา<span class="hidden-xs desc">สัญญาจ้าง ใบรับรอง</span></a></li>
                                        <li data-target="#carousel-eg2" data-slide-to="2" class=""><a href="javascript:void(0);" class="">รายงาน<span class="hidden-xs desc">รายงานที่เกี่ยวข้อง</span></a></li>
                                        <li data-target="#carousel-eg2" data-slide-to="3" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐาน<span class="hidden-xs desc">ข้อมูลพื้นฐานที่เกี่ยวข้อง</span></a></li>
                                    </ol>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>reg"><i class="fa fa-file"></i>&nbsp; จัดการทะเบียนประวัติ</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>contracttype"><i class="fa fa-file"></i>&nbsp; จัดการสัญญาจ้าง</a></li>
                                                    <li><a href="<?=site_url();?>cerificatework"><i class="fa fa-file"></i>&nbsp; ใบรับรองการทำงาน</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-4by3">
                                                <ul>
        <li><a href="<?=site_url();?>reportbirthday"><i class="fa fa-file"></i>&nbsp; วันเกิดพนักงาน</a></li>
        <li><a href="<?=site_url();?>reportcontract"><i class="fa fa-file"></i>&nbsp; พนักงานครบกำหนดสัญญาจ้าง</a></li>
        <li><a href="<?=site_url();?>reportretired"><i class="fa fa-file"></i>&nbsp; พนักงานที่เกษียณอายุ</a></li>
        <li><a href="<?=site_url();?>reportstatusemployees"><i class="fa fa-file"></i>&nbsp; พนักงานที่พ้นสภาพ</a></li>
        <li><a href="<?=site_url();?>reportrank"><i class="fa fa-file"></i>&nbsp; ผู้มีสิทธิ์เลื่อนระดับขั้น</a></li>
        <li><a href="<?=site_url();?>reportstatage"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ อายุงาน</a></li>
        <li><a href="<?=site_url();?>reportstatcontract"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ สัญญาจ้าง</a></li>
        <li><a href="<?=site_url();?>reportstatretired"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ เกษียณอายุ</a></li>
        <li><a href="<?=site_url();?>reportstatstatus"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ พ้นสภาพ</a></li>
        <li><a href="<?=site_url();?>reportstatrank"><i class="fa fa-file"></i>&nbsp; รายงานเชิงสถิติ เลื่อนระดับ</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>stafftype"><i class="fa fa-file"></i>&nbsp; ประเภทพนักงาน</a></li>
                                                    <li><a href="<?=site_url();?>mistaketype"><i class="fa fa-file"></i>&nbsp; ประเภทการลงโทษ</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>

            <li class="dropdown-grid">
            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-video-camera"></i>&nbsp;<span class="hidden-sm">Time Attendance</span><span class="caret"></span></a>
            <div class="dropdown-grid-wrapper" role="menu">
                <ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <li>
                        <div id="carousel-eg3">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
                                    <ol class="carousel-indicators navbar-carousel-indicators h-divided">
                                        <li data-target="#carousel-eg3" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">ใบลา<span class="hidden-xs desc">ข้อมูลการลาของพนักงาน</span></a></li>
                                        <li data-target="#carousel-eg3" data-slide-to="1" class=""><a href="javascript:void(0);" class="">ข้อมูลการทำงาน<span class="hidden-xs desc">ข้อมูลการทำงาน</span></a></li>
                                    </ol>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>absence"><i class="fa fa-file"></i>&nbsp; จัดการใบลา</a></li>
                                                    <li><a href="<?=site_url();?>absence_condition"><i class="fa fa-file"></i>&nbsp; กำหนดเงื่อนไขวันลา</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>import_work"><i class="fa fa-file"></i>&nbsp; Import</a></li>
                                                    <li><a href="<?=site_url();?>report_work"><i class="fa fa-file"></i>&nbsp; รายงานสรุปการทำงาน</a></li>
                                                    <li><a href="<?=site_url();?>reportdaily"><i class="fa fa-file"></i>&nbsp; รายงานการทำงานประจำวัน</a></li>
                                                    <li><a href="<?=site_url();?>reportleave"><i class="fa fa-file"></i>&nbsp; รายงานการลา</a></li>
                                                    <li><a href="<?=site_url();?>holiday"><i class="fa fa-file"></i>&nbsp; จัดการวันหยุด</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>

<?php 
if ($this->session->userdata('roleID')==3) {
 ?>
        <li class="dropdown-grid">
            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-check-square"></i>&nbsp;<span class="hidden-sm">ประเมิน/ขึ้นเงินเดือน</span><span class="caret"></span></a>
            <div class="dropdown-grid-wrapper" role="menu">
                <ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <li>
                        <div id="carousel-eg4">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
                                    <ol class="carousel-indicators navbar-carousel-indicators h-divided">
                                        <li data-target="#carousel-eg4" data-slide-to="0" class=""><a href="javascript:void(0);" class="">แบบประเมิน<span class="hidden-xs desc">แบบประเมินพฤติกรรม/KPI</span></a></li>
                                        <!--
                                        <li data-target="#carousel-eg4" data-slide-to="1" class=""><a href="javascript:void(0);" class="">ประเมิน Competency<span class="hidden-xs desc">แบบประเมิน Competency</span></a></li>
                                        <li data-target="#carousel-eg4" data-slide-to="2" class=""><a href="javascript:void(0);" class="">พิจารณาขึ้นเงินเดือน<span class="hidden-xs desc">ข้อมูลการพิจารณาขึ้นเงินเดือน</span></a></li>
                                        -->
                                    </ol>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
<li><a href="<?=site_url();?>evaluation"><i class="fa fa-file"></i>&nbsp;จัดการแบบประเมิน </a></li>
<li><a href="<?=site_url();?>rate"><i class="fa fa-file"></i>&nbsp; ทำการประเมิน</a></li>
<li><a href="<?=site_url();?>rate/tscore"><i class="fa fa-file"></i>&nbsp; คะแนนการประเมิน</a></li>
<li><a href="<?=site_url();?>competencys/subjectkpi"><i class="fa fa-file"></i>&nbsp; จัดการข้อมูล KPI</a></li>
<li><a href="<?=site_url();?>competencys/subject"><i class="fa fa-file"></i>&nbsp; จัดการข้อมูล Competency</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
<li><a href="<?=site_url();?>salarys"><i class="fa fa-file"></i>&nbsp; ภาพรวมการขึ้นเงินเดือน</a></li>
<li><a href="<?=site_url();?>salarys/dept"><i class="fa fa-file"></i>&nbsp; จัดการขึ้นเงินเดือน</a>
                                                </ul>
                                            </div>
                                        </div-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>
        <?php 
            }
        ?>
<!--
        <li class="dropdown-grid">
            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-users"></i>&nbsp;<span class="hidden-sm">ผู้ใช้งาน</span><span class="caret"></span></a>
            <div class="dropdown-grid-wrapper" role="menu">
                <ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <li>
                        <div id="carousel-eg4">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
                                    <ol class="carousel-indicators navbar-carousel-indicators h-divided">
                                        <li data-target="#carousel-eg4" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">สิทธิ์<span class="hidden-xs desc">ข้อมูลสิทธิ์การเข้าใช้งานระบบ</span></a></li>
                                        <li data-target="#carousel-eg4" data-slide-to="1" class=""><a href="javascript:void(0);" class="">ผู้ใช้งาน<span class="hidden-xs desc">ข้อมูลผู้ใช้งานระบบ</span></a></li>
                                    </ol>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>roles"><i class="fa fa-file"></i>&nbsp; จัดการสิทธิ์</a></li>

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>users"><i class="fa fa-file"></i>&nbsp; จัดการผู้ใช้งาน</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>
-->
        <!--<li class="dropdown-grid">
            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-heartbeat"></i>&nbsp;<span class="hidden-sm">สวัสดิการ</span><span class="caret"></span></a>
            <div class="dropdown-grid-wrapper" role="menu">
                <ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <li>
                        <div id="carousel-eg3">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
                                    <ol class="carousel-indicators navbar-carousel-indicators h-divided">
                                        <li data-target="#carousel-eg3" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">เบิกสวัสดิการ<span class="hidden-xs desc">เบิกจ่ายและตรวจสอบสิทธิ์สวัสดิการ</span></a></li>
                                        <li data-target="#carousel-eg3" data-slide-to="1" class=""><a href="javascript:void(0);" class="">การตรวจสุขภาพ<span class="hidden-xs desc">ข้อมูลการตรวจสุขภาพ</span></a></li>
                                        <li data-target="#carousel-eg3" data-slide-to="2" class=""><a href="javascript:void(0);" class="">รายงาน<span class="hidden-xs desc">รายงานที่เกี่ยวข้อง</span></a></li>
                                        <li data-target="#carousel-eg3" data-slide-to="3" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐาน<span class="hidden-xs desc">ข้อมูลพื้นฐานที่เกี่ยวข้อง</span></a></li>
                                    </ol>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>staffwelfareright"><i class="fa fa-file"></i>&nbsp; กำหนดสิทธิ์สวัสดิการ</a></li>
                                                    <li><a href="<?=site_url();?>staffwelfaretake"><i class="fa fa-file"></i>&nbsp; เบิกสวัสดิการ</a></li>
                                                    <li><a href="<?=site_url();?>staffwelfarecheck"><i class="fa fa-file"></i>&nbsp; ตรวจสอบ/เรียกดูสิทธิ์สวัสดิการ</a></li>
                                                    <li><a href="<?=site_url();?>cerificatewelfare"><i class="fa fa-file"></i>&nbsp; แบบฟอร์ม / หนังสือรับรอง</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>reporthealthcheck"><i class="fa fa-file"></i>&nbsp; รายงานตรวจสุขภาพ</a></li>
                                                    <li><a href="<?=site_url();?>reportstaffnothealthchecklist"><i class="fa fa-file"></i>&nbsp; รายชื่อพนักงานที่ไม่ตรวจสุขภาพ</a></li>
                                                    <li><a href="<?=site_url();?>import"><i class="fa fa-file"></i>&nbsp; Import รายงานตรวจสุขภาพ</a></li>
                                                    <li><a href="<?=site_url();?>export"><i class="fa fa-file"></i>&nbsp; Export รายงานตรวจสุขภาพ</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>reportmedicalfeehistory"><i class="fa fa-file"></i>&nbsp; ประวัติการเบิกค่ารักษาพยาบาล</a></li>
                                                    <li><a href="<?=site_url();?>reportsicknessrank"><i class="fa fa-file"></i>&nbsp; สถิติการเจ็บป่วยของพนักงาน</a></li>
                                                    <li><a href="<?=site_url();?>reportmedicalfeerank"><i class="fa fa-file"></i>&nbsp; สถิติการเบิกค่ารักษาพยาบาล</a></li>
                                                    <li><a href="<?=site_url();?>reportmunstaffrank"><i class="fa fa-file"></i>&nbsp; จำนวนพนักงานตามเชิงสถิติ</a></li>
                                                    <li><a href="<?=site_url();?>reportmedicalfeestaff"><i class="fa fa-file"></i>&nbsp; สรุปการเบิกค่ารักษาพยาบาล</a></li>
                                                    <li><a href="<?=site_url();?>reportmedicalfeeall"><i class="fa fa-file"></i>&nbsp; ยอดการเบิกค่ารักษาพยาบาล</a></li>
                                                    <li><a href="<?=site_url();?>reporthealthcheckfeestatement"><i class="fa fa-file"></i>&nbsp; ยอดการเบิกค่าตรวจสุขภาพ</a></li>
                                                    <li><a href="<?=site_url();?>reportduemedicalfee"><i class="fa fa-file"></i>&nbsp; สรุปเงินค้างชำระค่ารักษาพยาบาล</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>hospitaltype"><i class="fa fa-file"></i>&nbsp; ประเภทสถานพยาบาล</a></li>
                                                    <li><a href="<?=site_url();?>hospital"><i class="fa fa-file"></i>&nbsp; สถานพยาบาล</a></li>
                                                    <li><a href="<?=site_url();?>patienttype"><i class="fa fa-file"></i>&nbsp; ประเภทผู้ป่วย</a></li>
                                                    <li><a href="<?=site_url();?>welfaretype"><i class="fa fa-file"></i>&nbsp; ประเภทสวัสดิการ</a></li>
                                                    <li><a href="<?=site_url();?>welfarelistfee"><i class="fa fa-file"></i>&nbsp; รายการเบิกค่ารักษาพยาบาล</a></li>
                                                    <li><a href="<?=site_url();?>welfareright"><i class="fa fa-file"></i>&nbsp; สิทธิ์สวัสดิการ</a></li>
                                                    <li><a href="<?=site_url();?>welfarepay"><i class="fa fa-file"></i>&nbsp; ช่องทางการรับเงิน</a></li>
                                                    <li><a href="<?=site_url();?>welfarecalendar"><i class="fa fa-file"></i>&nbsp; ปฏิทินการจ่ายเงิน</a></li>
                                                    <li><a href="<?=site_url();?>welfarefundlife"><i class="fa fa-file"></i>&nbsp; ข้อมูลกองทุนสำรองเลี้ยงชีพ</a></li>
                                                    <li><a href="<?=site_url();?>welfarefundpension"><i class="fa fa-file"></i>&nbsp; ข้อมูลกองทุนบำเหน็จ</a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>

        <li class="dropdown-grid">
            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-graduation-cap"></i>&nbsp;<span class="hidden-sm">พัฒนาบุคลากร</span><span class="caret"></span></a>
            <div class="dropdown-grid-wrapper" role="menu">
                <ul class="dropdown-menu col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <li>
                        <div id="carousel-eg5">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 divided" >
                                    <ol class="carousel-indicators navbar-carousel-indicators h-divided">
                                        <li data-target="#carousel-eg5" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">ศึกษาต่อ<span class="hidden-xs desc">ข้อมูลการลาศึกษาต่อ</span></a></li>
                                        <li data-target="#carousel-eg5" data-slide-to="1" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐานการศึกษา<span class="hidden-xs desc">ข้อมูลพื้นฐานด้านการศึกษา</span></a></li>
                                        <li data-target="#carousel-eg5" data-slide-to="2" class=""><a href="javascript:void(0);" class="">อบรม<span class="hidden-xs desc">ข้อมูลการอบรม</span></a></li>
                                        <li data-target="#carousel-eg5" data-slide-to="3" class=""><a href="javascript:void(0);" class="">ข้อมูลพื้นฐานอบรม<span class="hidden-xs desc">ข้อมูลพื้นฐานด้านการอบรม</span></a></li>
                                    </ol>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
                                                    <li><a href="<?=site_url();?>conedu"><i class="fa fa-file"></i>&nbsp; ลาศึกษาต่อ</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
<li><a href="<?=site_url();?>education"><i class="fa fa-file"></i>&nbsp; ระดับการศึกษา</a></li>
<li><a href="<?=site_url();?>eduplace"><i class="fa fa-file"></i>&nbsp; สถานศึกษา</a></li>
<li><a href="<?=site_url();?>edutype"><i class="fa fa-file"></i>&nbsp; ประเภทสถานศึกษา</a></li>
<li><a href="<?=site_url();?>edufaculty"><i class="fa fa-file"></i>&nbsp; คณะ / สายการเรียน</a></li>
<li><a href="<?=site_url();?>edudepartment"><i class="fa fa-file"></i>&nbsp; สาขา</a></li>
<li><a href="<?=site_url();?>edudegree"><i class="fa fa-file"></i>&nbsp; วุฒิการศึกษา</a></li>
<li><a href="<?=site_url();?>country"><i class="fa fa-file"></i>&nbsp; ประเทศ</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
<li><a href="<?=site_url();?>traintype"><i class="fa fa-file"></i>&nbsp; ประเภทหลักสูตร</a></li>
<li><a href="<?=site_url();?>trainer"><i class="fa fa-file"></i>&nbsp; วิทยากร</a></li>
<li><a href="<?=site_url();?>trainyear"><i class="fa fa-file"></i>&nbsp; วางแผนงบประมาณ</a></li>
<li><a href="<?=site_url();?>traincourse"><i class="fa fa-file"></i>&nbsp; จัดอบรม</a></li>
<li><a href="<?=site_url();?>trainevaluate"><i class="fa fa-file"></i>&nbsp; ประเมินผลการอบรม</a></li>
<li><a href="<?=site_url();?>traineecourse"><i class="fa fa-file"></i>&nbsp; กำหนดผู้เข้าร่วมอบรม</a></li>
<li><a href="<?=site_url();?>trainprice"><i class="fa fa-file"></i>&nbsp; บันทึกค่าใช้จ่าย</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <ul>
<li><a href="<?=site_url();?>traintype"><i class="fa fa-file"></i>&nbsp; ประเภทหลักสูตร</a></li>
<li><a href="<?=site_url();?>trainer"><i class="fa fa-file"></i>&nbsp; วิทยากร</a></li>
<li><a href="<?=site_url();?>trainyear"><i class="fa fa-file"></i>&nbsp; วางแผนงบประมาณ</a></li>
<li><a href="<?=site_url();?>traincourse"><i class="fa fa-file"></i>&nbsp; จัดอบรม</a></li>
<li><a href="<?=site_url();?>trainevaluate"><i class="fa fa-file"></i>&nbsp; ประเมินผลการอบรม</a></li>
<li><a href="<?=site_url();?>traineecourse"><i class="fa fa-file"></i>&nbsp; กำหนดผู้เข้าร่วมอบรม</a></li>
<li><a href="<?=site_url();?>trainprice"><i class="fa fa-file"></i>&nbsp; บันทึกค่าใช้จ่าย</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>








            <!--<li>
            <a href="<?=site_url();?>report_work"><i class="fa fa-suitcase"></i> รายงานสรุปการทำงาน</a>
        </li>-->
    </ul>
<?php
}
?>
