<li>
  <a ng-class="{active:isActive('/dashboard')}" href="<?php echo site_url();?>dashboard"><i class="fa fa-home"></i>หน้าแรก</a>
</li> 
<?php
if($this->session->userdata('roleID')==1)
{
?>
<li class="sub-menu">
  <a  href="javascript:;"><i class="fa fa-sitemap"></i>โครงสร้างองค์กร</a>
  <ul class="sub">
    <li><a href="<?php echo site_url();?>appoint"><i class="fa fa-sitemap"></i> จัดโครงสร้างองค์กร</a></li>
    <li><a href="<?php echo site_url();?>structure"><i class="fa fa-user-circle"></i> ตำแหน่งงาน</a></li>
    <li><a href="<?php echo site_url();?>sequence"><i class="fa fa-users"></i> อัตราพนักงาน</a> </li>
  </ul>
</li>
<li><a href="<?=site_url();?>reg"><i class="fa fa-user"></i>ทะเบียนประวัติ</a></li>
<li><a href="<?=site_url();?>report_work"><i class="fa fa-clock-o"></i>เวลาทำงาน</a></li>
<li><a href="<?=site_url(); ?>absence"><i class="fa fa-file-text"></i>ใบลา</a></li> 
<li><a href="<?=site_url(); ?>users"><i class="fa fa-users"></i> ผู้ใช้งาน</a></li> 
<li><a href="<?=site_url(); ?>roles"><i class="fa fa-unlock-alt"></i> สิทธิ์ผู้ใช้งาน</a></li>
<?php
}
else
{
?>
<li>
  <a  href="<?=site_url();?>reg/editStaff?id=<?=$this->session->userdata('ID')?>"><i class="fa fa-user"></i>ทะเบียนประวัติ</a>
</li>
<li>
    <a href="<?=site_url(); ?>absence"><i class="fa fa-file-text"></i>ใบลา</a> 
</li> 
<?php
}
?>
<!--
<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>ตำแหน่งงาน</span>
        </a>
        <ul class="sub">
              <li class="sub-menu">
              <a href="#"><i class="fa fa-caret-down"></i> ข้อมูลพื้นฐาน</a> 
                   <ul class="sub sub-lv2">
                      <li>
                            <a href="<?php echo site_url();?>position"><i class="fa fa-file"></i> ชื่อตำแหน่ง</a> 
                      </li>
                      <li>
                            <a href="<?php echo site_url();?>level"><i class="fa fa-file"></i> ชื่อระดับ</a> 
                      </li>
                      <li>
                        	<a href="<?php echo site_url();?>workplace"><i class="fa fa-file"></i> สถานที่ทำงาน</a> 
                      </li>
                      <li>
                        	<a href="<?php echo site_url();?>workstatus"><i class="fa fa-file"></i> สถานะพนักงาน</a> 
                      </li>
                      
                  </ul>
             </li>
              
         </ul>
</li>
-->
<!--li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>โครงการ</span>
        </a>
        <ul class="sub">
              <li>
                     <a href="<?php echo site_url();?>employment"><i class="fa fa-check-square fa-fw"></i>&nbsp; ข้อมูลโครงการ </a> 
              </li>  
              <li>
                    <a href="<?php echo site_url();?>project"><i class="fa fa-check-square fa-fw"></i>&nbsp; ตำแหน่งงานตามโครงการ</a> 
              </li>
         </ul>
</li--> 
<!--
<li class="sub-menu">
        <a  href="javascript:;" id="defaults" ng-class="getClass('/reg')">
              <i class="fa fa-user"></i>
              <span>ทะเบียนประวัติ</span>
        </a>
        <ul class="sub">
              <li>
                <a href="<?php echo site_url();?>reg"><i class="fa fa-check-square fa-fw"></i> พนักงาน</a> 
              </li>
              <li>
                <a href="<?php echo site_url();?>stafftype"><i class="fa fa-check-square fa-fw"></i> ประเภทพนักงาน</a> 
              </li>
              <li>
                <a href="<?php echo site_url();?>contracttype"><i class="fa fa-check-square fa-fw"></i> ประเภทสัญญาจ้าง</a> 
              </li>
              <li>
                <a href="<?php echo site_url();?>mistaketype"><i class="fa fa-check-square fa-fw"></i> ประเภทการลงโทษ</a> 
              </li>
               <li>
                <a href="<?php echo site_url();?>notification"><i class="fa fa-check-square fa-fw"></i> แจ้งเตือน</a> 
             </li>
              <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>รายงาน</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="<?php echo site_url(); ?>reportcontract"><i class="fa fa-file"></i> พนักงานครบกำหนดสัญญาจ้าง</a> 
                         </li>
                        <li>
                          <a href="<?php echo site_url(); ?>reportbirthday"><i class="fa fa-file"></i> พนักงานที่เกิด</a> 
                        </li>
                        <li> 
                          <a href="<?php echo site_url(); ?>reportretired"><i class="fa fa-file"></i> พนักงานที่เกษียณอายุ</a> 
                        </li>
                        <li>
                          <a href="<?php echo site_url(); ?>reportstatusemployees"><i class="fa fa-file"></i> พนักงานที่พ้นสภาพ</a> 
                        </li>
                        <li>
                          <a href="<?php echo site_url(); ?>reportstatage"><i class="fa fa-file"></i> รายงานเชิงสถิติ อายุงาน</a> 
                        </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportstatrank"><i class="fa fa-file"></i> รายงานเชิงสถิติ เลื่อนระดับ</a> 
                        </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportstatcontract"><i class="fa fa-file"></i> รายงานเชิงสถิติ สัญญาจ้าง</a> 
                        </li>
                        <li>
                          <a href="<?php echo site_url(); ?>reportstatretired"><i class="fa fa-file"></i> รายงานเชิงสถิติ เกษียณอายุ</a> 
                        </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportstatstatus"><i class="fa fa-file"></i> รายงานเชิงสถิติ พ้นสภาพ</a> 
                        </li>
                        <li>
                          <a href="<?php echo site_url(); ?>cerificatework"><i class="fa fa-file"></i> ใบรับรองการทำงาน</a> 
                        </li>
                        <li>
                          <a href="<?php echo site_url(); ?>reportposition"><i class="fa fa-file"></i> ตำแหน่งว่าง</a> 
                        </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportinsignia"><i class="fa fa-file"></i> ผู้มีสิทธิ์ได้รับเครื่องราช</a> 
                        </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportrank"><i class="fa fa-file"></i> ผู้มีสิทธิ์เลื่อนระดับขั้น</a> 
                        </li>
                  </ul>
            </li>
           
               <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>ข้อมูลการศึกษา</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="<?php echo site_url();?>education"><i class="fa fa-file"></i> ระดับการศึกษา</a> 
                         </li>
                        <li>
                          <a href="<?php echo site_url();?>eduplace"><i class="fa fa-file"></i> สถานศึกษา</a> 
                        </li>
                        <li>
                          <a href="<?php echo site_url();?>edutype"><i class="fa fa-file"></i> ประเภทสถานศึกษา</a> 
                        </li>
                        <li> 
                          <a href="<?php echo site_url();?>edufaculty"><i class="fa fa-file"></i> คณะ / สายการเรียน</a> 
                        </li>
                        <li>
                          <a href="<?php echo site_url();?>edudepartment"><i class="fa fa-file"></i> สาขา</a> 
                        </li>
                        <li>
                          <a href="<?php echo site_url();?>edudegree"><i class="fa fa-file"></i> วุฒิการศึกษา</a> 
                        </li>
                         
                        <li>
                          <a href="<?php echo site_url();?>country"><i class="fa fa-file"></i> ประเทศ</a> 
                        </li>
                       
                  </ul>
            </li>
           
        </ul>
</li>
-->
<!--li class="sub-menu">
        <a  href="javascript:;" id="defaults" ng-class="getClass('/welfare')">
              <i class="fa fa-medkit"></i>
              <span>สวัสดิการ</span>
        </a>
        <ul class="sub">
               <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>ข้อมูลพื้นฐาน</span>
                  </a>
                  <ul class="sub sub-lv2">
                      <li>
                          <a href="<?php echo site_url();?>hospitaltype"><i class="fa fa-file"></i>&nbsp; ประเภทสถานพยาบาล</a> 
                      </li>
                      <li>
                          <a href="<?php echo site_url();?>hospital"><i class="fa fa-file"></i>&nbsp; สถานพยาบาล</a> 
                      </li> 
                      <li>
                            <a href="<?php echo site_url();?>patienttype"><i class="fa fa-file"></i>&nbsp; ประเภทผู้ป่วย</a> 
                      </li>
                      <li>
                            <a href="<?php echo site_url();?>welfaretype"><i class="fa fa-file"></i>&nbsp; ประเภทสวัสดิการ</a> 
                      </li> 
                     <li>
                            <a href="<?php echo site_url();?>welfarelistfee"><i class="fa fa-file"></i>&nbsp; รายการเบิกค่ารักษาพยาบาล</a> 
                      </li>
                      <li>
                            <a href="<?php echo site_url();?>welfareright"><i class="fa fa-file"></i>&nbsp; สิทธิ์สวัสดิการ</a> 
                      </li> 
                      <li>
                            <a href="<?php echo site_url();?>welfarepay"><i class="fa fa-file"></i>&nbsp; ช่องทางการรับเงิน</a> 
                      </li>
                      <li>
                            <a href="<?php echo site_url();?>welfarecalendar"><i class="fa fa-file"></i>&nbsp; ปฏิทินการจ่ายเงิน</a> 
                      </li> 
                      <li>
                            <a href="<?php echo site_url();?>welfarefundlife"><i class="fa fa-file"></i>&nbsp; ข้อมูลกองทุนสำรองเลี้ยงชีพ</a> 
                      </li> 
                       <li>
                            <a href="<?php echo site_url();?>welfarefundpension"><i class="fa fa-file"></i>&nbsp; ข้อมูลกองทุนบำเหน็จ</a> 
                      </li> 
                  </ul>
            </li>
            <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>ข้อมูลสวัสดิการ</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="<?php echo site_url(); ?>staffwelfareright"><i class="fa fa-file"></i>&nbsp; กำหนดสิทธิ์สวัสดิการ</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>staffwelfaretake"><i class="fa fa-file"></i>&nbsp; เบิกสวัสดิการ</a> 
                         </li>  
                         <li>
                          <a href="<?php echo site_url(); ?>staffwelfarecheck"><i class="fa fa-file"></i>&nbsp; ตรวจสอบ/เรียกดูสิทธิ์สวัสดิการ</a> 
                         </li>
                          <li>
                          <a href="<?php echo site_url(); ?>import"><i class="fa fa-file"></i>&nbsp; Import รายงานตรวจสุขภาพ</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>export"><i class="fa fa-file"></i>&nbsp; Export รายงานตรวจสุขภาพ</a> 
                         </li>
                  </ul>
            </li>
            <li>
                <a href="<?php echo site_url();?>welfarenotification"><i class="fa fa-check-square fa-fw"></i>&nbsp; แจ้งเตือน</a> 
             </li>
            
                          <li class="sub-menu">
                  <a  href="javascript:;" id="defaults">
                        <i class="fa fa-caret-down"></i> 
                        <span>รายงาน</span>
                  </a>
                  <ul class="sub sub-lv2">
                        <li>
                          <a href="<?php echo site_url(); ?>cerificatewelfare"><i class="fa fa-file"></i>&nbsp; แบบฟอร์ม / หนังสือรับรอง</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportmedicalfeehistory"><i class="fa fa-file"></i>&nbsp; ประวัติการเบิกค่ารักษาพยาบาล</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportsicknessrank"><i class="fa fa-file"></i>&nbsp; สถิติการเจ็บป่วยของพนักงาน</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportmedicalfeerank"><i class="fa fa-file"></i>&nbsp; สถิติการเบิกค่ารักษาพยาบาล</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reporthealthcheck"><i class="fa fa-file"></i>&nbsp; รายงานตรวจสุขภาพ</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportstaffnothealthchecklist"><i class="fa fa-file"></i>&nbsp; รายชื่อพนักงานที่ไม่ตรวจสุขภาพ</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportmunstaffrank"><i class="fa fa-file"></i>&nbsp; จำนวนพนักงานตามเชิงสถิติ</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportmedicalfeestaff"><i class="fa fa-file"></i>&nbsp; สรุปการเบิกค่ารักษาพยาบาล</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportmedicalfeeall"><i class="fa fa-file"></i>&nbsp; ยอดการเบิกค่ารักษาพยาบาล</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reporthealthcheckfeestatement"><i class="fa fa-file"></i>&nbsp; ยอดการเบิกค่าตรวจสุขภาพ</a> 
                         </li>
                         <li>
                          <a href="<?php echo site_url(); ?>reportduemedicalfee"><i class="fa fa-file"></i>&nbsp; สรุปเงินค้างชำระค่ารักษาพยาบาล</a> 
                         </li>
                  </ul>
              </li>
        </ul>
</li-->
<!--
<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>ประเมินพนักงาน</span>
        </a>
        <ul class="sub">
              <li>
                     <a href="<?php echo site_url();?>evaluation"><i class="fa fa-check-square fa-fw"></i> จัดการข้อมูลการประเมิน </a> 
              </li>  
              <li>
                    <a href="<?php echo site_url();?>rate?year=2560&round=1&var_agencies=2"><i class="fa fa-check-square fa-fw"></i> ข้อมูลการประเมินล่าสุด</a> 
              </li>
              <li>
                    <a href="<?php echo site_url();?>competencys/subjectkpi"><i class="fa fa-check-square fa-fw"></i> ข้อมูล KPI</a> 
              </li>
              <li>
                    <a href="<?php echo site_url();?>rate/tscore"><i class="fa fa-check-square fa-fw"></i> รายงาน</a> 
              </li>
              <li>
                  <a href="<?php echo site_url(); ?>cerificateeval"><i class="fa fa-check-square fa-fw"></i> หนังสือเวียน</a> 
               </li>
              <!--<li>
                    <a href="<?php echo site_url();?>rate/tscore"><i class="fa fa-check-square fa-fw"></i>&nbsp; ข้อมูลการประเมิน</a> 
              </li>-->
          <!--    
          </ul>
</li>

<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>ประเมิน Competency</span>
        </a>
        <ul class="sub">
              <li>
                     <a href="<?php echo site_url();?>competency"><i class="fa fa-check-square fa-fw"></i> จัดการข้อมูลการประเมิน </a> 
              </li> 
               <li>
                    <a href="<?php echo site_url();?>competencys/subject"><i class="fa fa-check-square fa-fw"></i> ข้อมูล Competency</a> 
              </li> 
              <li>
                    <a href="<?php echo site_url();?>competencys/recent"><i class="fa fa-check-square fa-fw"></i> ข้อมูลการประเมินล่าสุด</a> 
              </li> 
          </ul>
</li>

<li class="sub-menu">
        <a  href="javascript:;" id="defaults">
              <i class="fa fa-sitemap"></i>
              <span>เลื่อนขั้นเงินเดือน</span>
        </a>
        <ul class="sub">
               <li>
                  <a href="<?php echo site_url(); ?>salarys"><i class="fa fa-check-square fa-fw"></i> จัดการการเลื่อนขั้นเงินเดือน</a> 
               </li>
               <!-- <li>
                  <a href="<?php echo site_url(); ?>salarys/dept"><i class="fa fa-check-square fa-fw"></i>&nbsp; จัดการเลื่อนเงินเดือน</a> 
               </li>-->
               
         </ul>
</li>
<!--li class="sub-menu">
    <a href="javascript:;" id="defaults" ng-class="getClass('/develop')">
        <i class="fa fa-gears"></i>
        <span>พัฒนาบุคลากร</span>
    </a>

    <ul class="sub">
           <li>
          	  <a href="<?php echo site_url(); ?>conedu"><i class="fa fa-check-square fa-fw"></i>&nbsp; ข้อมูลผู้ลาศึกษาต่อ</a> 	
           </li>
           <li>
              <a href="<?php echo site_url(); ?>developtype"><i class="fa fa-check-square fa-fw"></i>&nbsp; ประเภทการพัฒนา</a> 
           </li>
         
     </ul>

</li>
<li class="sub-menu">
    <a href="javascript:;" id="defaults" ng-class="getClass('/train')">
        <i class="fa fa-clipboard"></i>
        <span>ฝึกอบรม</span>
    </a>
     <ul class="sub">
           <li>
              <a href="<?php echo site_url(); ?>traintype"><i class="fa fa-check-square fa-fw"></i>&nbsp; ประเภทหลักสูตร</a> 
           </li>
           <li>
              <a href="<?php echo site_url(); ?>trainer"><i class="fa fa-check-square fa-fw"></i>&nbsp; วิทยากร</a> 
           </li>
           <li>
              <a href="<?php echo site_url(); ?>trainyear"><i class="fa fa-check-square fa-fw"></i>&nbsp; วางแผนงบประมาณ</a> 
           </li>
            <li>
              <a href="<?php echo site_url(); ?>traincourse"><i class="fa fa-check-square fa-fw"></i>&nbsp; จัดอบรม</a> 
           </li>
           <li>
              <a href="<?php echo site_url(); ?>trainevaluate"><i class="fa fa-check-square fa-fw"></i>&nbsp; ประเมินผลการอบรม</a> 
           </li>
          <li>
              <a href="<?php echo site_url(); ?>traineecourse"><i class="fa fa-check-square fa-fw"></i>&nbsp; กำหนดผู้เข้าร่วมอบรม</a> 
           </li>
           <li>
              <a href="<?php echo site_url(); ?>trainprice"><i class="fa fa-check-square fa-fw"></i>&nbsp; บันทึกค่าใช้จ่าย</a> 
           </li>
     </ul>
</li-->
<!--
-->