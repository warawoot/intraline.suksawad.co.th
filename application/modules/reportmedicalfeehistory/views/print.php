
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h4 style="text-align:center;">
		<br>
			รายงานประวัติการเบิกค่ารักษาพยาบาล <?php echo (!empty($day)) ? 'วันที่ '.$day : ""; ?> <?php echo (!empty($month)) ? 'ประจำเดือน '.$th_month[$month] : ""; ?> <?php echo (!empty($year)) ? 'ปี '.$year : ""; ?>
		
	</h4>
	
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ลำดับ</th>
			<th>วันที่เบิก</th>
			<th >ผู้เบิก</th>
			<th >เบิกให้</th>
			<th >ความสัมพันธ์</th>
			<th >ประเภทสวัสดิการ</th>
			<th >โรค</th>
			<th >ผู้ป่วยใน / นอก</th>
			<th >วันที่รักษา</th>
			<th >จำนวนเงินที่เบิก</th>
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
			foreach($r->result() as  $row){ 
					$i++;


				?>
				 <tr >
					<td><?php echo $i; ?></td>
					<td><?php echo toFullDate($row->takeDate,'th','full');?></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo ($row->takePersonType == '1') ? $row->staffFName.' '.$row->staffLName : $row->personFName.' '.$row->personLName; ?></td>
					<td>
					<?php 
					switch($row->takePersonType){
						case '1' :  echo 'ตนเอง'; break;
						case '2' :  echo 'บิดา-มารดา'; break;
						case '3' :  echo 'คู่สมรส'; break;
						case '4' :  echo 'บุตร'; break;
					}
					?>
					</td>
					<td><?php echo $row->welTypeName; ?></td>
					<td><?php echo $row->diseaseName; ?></td>
					<td><?php echo $row->patTypeName; ?></td>
					<td><?php echo toFullDate($row->takeStartDate,'th','full').' - '.toFullDate($row->takeEndDate,'th','full'); ?></td>
					<?php
					$price=0;
					$arrp = json_decode($row->takePrice);
					foreach($arrp as $rowp){
						$price+=$rowp->request;
					}
					?>
					<td><?php echo number_format($price); ?></td>
				</tr>
		<?php  } }else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>