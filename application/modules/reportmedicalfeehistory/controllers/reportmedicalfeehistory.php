<?php

class reportmedicalfeehistory extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		$this->load->model('staff_welfare_take_model');
		$this->load->model('welfare_take_hospital_model');

		
	}
	
	public function index(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_welfare_take_model->getAll($data['day'],$data['month'],$data['year']);
		
        $this->template->load("template/admin",'reportmedicalfeehistory', $data);

	}

	public function print_pdf(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_welfare_take_model->getAll($data['day'],$data['month'],$data['year']);


		$data_r['html'] = $this->load->view('print',$data,true);
		
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_welfare_take_model->getAll($data['day'],$data['month'],$data['year']);

		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportmedicalfeehistory.php */
/* Location: ./application/module/reportmedicalfeehistory/reportmedicalfeehistory.php */