<?php

class Staffwelfareright extends MY_Controller {
   
    function __construct() {
        parent::__construct();

        $this->setModel('staff_welfare_right_model');

        $this->load->model('staff_model');

        $this->load->model('staff_welfare_fund_model');
        $this->load->model('staff_welfare_pension_model');
    }
 

    public function index() {
         
        $this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();

        $xcrud->table('tbl_staff');
        $xcrud->relation('positionID','tbl_position','positionID','positionName');

        $col_name = array(
            'staffID' => 'รหัสพนักงาน',  
            'staffFName' => 'ชื่อ',
            'rankID' => 'ระดับ',
            'positionID' => 'ตำแหน่ง',
            'orgID' => 'ฝ่าย / สำนักงาน'
           

        );
        $xcrud->columns('staffID,staffFName,rankID,positionID,orgID');
        $xcrud->label($col_name);
        $xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
        $xcrud->column_callback('rankID','calRankWork');
        $xcrud->column_callback('positionID','calPositionWork');
        $xcrud->column_callback('orgID','calOrgIDWork');

        $xcrud->column_width('staffID','5%');

        $xcrud->unset_add()->unset_view()->unset_edit()->unset_remove();
        $xcrud->button(site_url().'staffwelfareright/welfare/?token={ID}','สิทธิ์สวัสดิการ','fa fa-plus-square','btn-success');
        $xcrud->button(site_url().'staffwelfareright/fund/?token={ID}','กองทุน','fa fa-book','btn-warning');

        $data['html'] = $xcrud->render();
        $data['title'] = "กำหนดสิทธิ์สวัสดิการ";
        $this->template->load('template/admin', 'main',$data);
    }

    public function welfare(){

        $data['staffID'] = $this->input->get('token',true);

        $this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();
        $xcrud->table('tbl_staff_welfare_right')->where('staffID =',$data['staffID']);
        $xcrud->relation('welTypeID','tbl_welfare_type','welTypeID','welTypeName');
        $col_name = array(
            'welTypeID' => 'ประเภทสวัสดิการ',
            'rightDate'  => 'วันที่มีผล',
            'rightPersonType'   => 'ผู้ได้รับสิทธิ์',
            'personID'  => 'รูปแบบ'
        );

        $xcrud->columns('welTypeID,personID,rightDate,rightPersonType');
        $xcrud->label($col_name);
        $xcrud->column_callback('rightDate','toBDDate');
        $xcrud->column_callback('personID','show_staff_welfix');
        $xcrud->column_callback('rightPersonType','show_person_type');

        $xcrud->unset_add()->unset_view()->unset_edit();

        $xcrud->button(site_url().'staffwelfareright/view/?token={rightID}&staff='.$data['staffID'],'ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
        $xcrud->button(site_url().'staffwelfareright/edit/?token={rightID}&staff='.$data['staffID'],'แก้ไข','glyphicon glyphicon-edit','btn-warning');

        $data['html'] = $xcrud->render();
        $data['staffName'] = getStaffName($data['staffID']);
        $data['title'] = "กำหนดสิทธิ์สวัสดิการ";
        $this->template->load("template/admin",'main', $data);
    }

    public function add(){

        $data['staffID'] = $this->input->get('token',true);
        $data['title'] = "กำหนดสิทธิ์สวัสดิการ";
        $this->template->load("template/admin",'form', $data);
    }

    public function submit(){

        if(!empty($_POST['welfareType'])){

            $welfareType            =   mysql_real_escape_string(trim($this->input->post('welfareType',true)));
            $rightID                =   mysql_real_escape_string(trim($this->input->post('rightID',true)));
            $staffID                =   mysql_real_escape_string(trim($this->input->post('staffID',true)));
            $rightDate              =   mysql_real_escape_string(trim($this->input->post('rightDate',true)));    
            $rightPersonType        =   mysql_real_escape_string(trim($this->input->post('rightPersonType',true)));
            $personID                =   (isset($_POST['personID'])) ? mysql_real_escape_string(trim($this->input->post('personID',true))) : $staffID;
            
            ////////start transaction///////////
           // $this->db->trans_begin();

            $arr = array(
                'welTypeID'         => $welfareType,
                'rightDate'         => toCEDate($rightDate),
                'rightPersonType'   => $rightPersonType,
                'staffID'           => $staffID,
                'personID'          => $personID
            ); 
            if($this->save($arr,$rightID))
                redirect('staffwelfareright/welfare/?token='.$staffID,'refresh');

   
        }else{
            redirect('staffwelfareright','refresh');
        }
    }

    public function view(){
        if(!empty($_GET['token'])){

            $rightID = mysql_real_escape_string(trim($this->input->get('token',true)));
            $data['staffID'] = mysql_real_escape_string(trim($this->input->get('staff',true)));
            $data['r'] = $this->staff_welfare_right_model->get_by(array('rightID'=>$rightID),true);
            
            
            $data['title'] = "กำหนดสิทธิ์สวัสดิการ";
            
            $data['rightID'] = $rightID;
            $this->template->load("template/admin",'form', $data);
        }   
    }

    public function edit(){
        if(!empty($_GET['token'])){

            $rightID = mysql_real_escape_string(trim($this->input->get('token',true)));
            $data['staffID'] = mysql_real_escape_string(trim($this->input->get('staff',true)));
            $data['r'] = $this->staff_welfare_right_model->get_by(array('rightID'=>$rightID),true);
            
            
            $data['title'] = "กำหนดสิทธิ์สวัสดิการ";
            
            $data['rightID'] = $rightID;
            $this->template->load("template/admin",'form', $data);
        }   
    }
     
     public function fund(){

        $data['staffID'] = $this->input->get('token',true);

        $this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();
        $xcrud->table('tbl_staff_welfare_fund')->where('staffID =',$data['staffID']);
        //$xcrud->relation('welTypeID','tbl_welfare_type','welTypeID','welTypeName');
        $col_name = array(
            'applyDate' => 'วันที่เปลี่ยน',
            'collectRate'  => 'อัตราสะสม',
            'grantRate'   => 'อัตราสมทบ'
        );

        $xcrud->columns('applyDate,collectRate,grantRate');
        $xcrud->label($col_name);
        $xcrud->column_callback('applyDate','toBDDate');

        $xcrud->unset_add()->unset_view()->unset_edit();

        $xcrud->button(site_url().'staffwelfareright/viewfund/?token={welfareFID}&staff='.$data['staffID'],'ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
        $xcrud->button(site_url().'staffwelfareright/editfund/?token={welfareFID}&staff='.$data['staffID'],'แก้ไข','glyphicon glyphicon-edit','btn-warning');

        $data['html'] = $xcrud->render();
        $data['staffName'] = getStaffName($data['staffID']);
        $data['title'] = "กำหนดสิทธิ์สวัสดิการ";
        $this->template->load("template/admin",'main_fund', $data);
    }

    public function addfund(){

        $data['staffID'] = $this->input->get('token',true);

        $data['title'] = "กองทุน";
        $data['staffName'] = getStaffName($data['staffID']);
        $this->template->load("template/admin",'form_fund', $data);
    }

    public function submitFundCollect(){
        if(!empty($_POST['collectRate']) || !empty($_POST['grantRate'])){

            $welfareFID             =   mysql_real_escape_string(trim($this->input->post('welfareFID',true)));
            $isApply                =   mysql_real_escape_string(trim($this->input->post('isApply',true)));
            $staffID                =   mysql_real_escape_string(trim($this->input->post('staffID',true)));
            $collectRate            =   mysql_real_escape_string(trim($this->input->post('collectRate',true)));    
            $collectDate            =   mysql_real_escape_string(trim($this->input->post('collectDate',true)));
            $grantRate           =   mysql_real_escape_string(trim($this->input->post('grantRate',true)));    
            $grantDate            =   mysql_real_escape_string(trim($this->input->post('grantDate',true)));
            $applyDate            =   mysql_real_escape_string(trim($this->input->post('applyDate',true)));

            $arr = array(
                'isApply'         => $isApply,
                'applyDate'         => toCEDate($applyDate),
                'collectDate'         => toCEDate($collectDate),
                'grantDate'         => toCEDate($grantDate),
                'collectRate'   => $collectRate,
                'staffID'           => $staffID,
                'grantRate'          => $grantRate
            ); 
            if($this->staff_welfare_fund_model->save($arr,$welfareFID))
                redirect('staffwelfareright/fund/?token='.$staffID,'refresh');

   
        }else{
            redirect('staffwelfareright','refresh');
        }
    }

    public function viewfund(){
        if(!empty($_GET['token'])){

            $welfareFID = mysql_real_escape_string(trim($this->input->get('token',true)));
            $data['staffID'] = mysql_real_escape_string(trim($this->input->get('staff',true)));
            $data['r'] = $this->staff_welfare_fund_model->get_by(array('welfareFID'=>$welfareFID),true);
            
            $data['mode'] = "view";
            $data['title'] = "กองทุน";
            
            $data['welfareFID'] = $welfareFID;
            $this->template->load("template/admin",'form_fund', $data);
        }   
    }

    public function editfund(){
        if(!empty($_GET['token'])){

            $welfareFID = mysql_real_escape_string(trim($this->input->get('token',true)));
            $data['staffID'] = mysql_real_escape_string(trim($this->input->get('staff',true)));
            $data['r'] = $this->staff_welfare_fund_model->get_by(array('welfareFID'=>$welfareFID),true);
            
            $data['mode'] = "edit";
            $data['title'] = "กองทุน";
            
            $data['welfareFID'] = $welfareFID;
            $this->template->load("template/admin",'form_fund', $data);
        }   
    }


    public function pension(){

        $data['staffID'] = $this->input->get('token',true);

        $data['r'] = $this->staff_welfare_pension_model->get_by(array('staffID'=>$data['staffID']),true);

        $data['staffName'] = getStaffName($data['staffID']);
        $data['title'] = "กองทุน";
        $this->template->load("template/admin",'form_pension', $data);
    }

    public function submitPensionCollect(){

            $welfarePID             =   mysql_real_escape_string(trim($this->input->post('welfarePID',true)));
            $isApply                =   mysql_real_escape_string(trim($this->input->post('isApply',true)));
            $staffID                =   mysql_real_escape_string(trim($this->input->post('staffID',true)));

            $arr = array(
                'isApply'         => $isApply,
                'staffID'           => $staffID
            ); 
            if($this->staff_welfare_pension_model->save($arr,$welfarePID))
                redirect('staffwelfareright/pension/?token='.$staffID,'refresh');

   
    }

    


   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>