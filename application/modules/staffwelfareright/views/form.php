<style>
    .error_lbl{
        border-color:red !important;
    }
        .ui-dialog-titlebar-close{
            display:none
        }
        .header-product{
            background-color: pink;
            font-weight: bold;
        }
        .header-product th{
            text-align: center;
            vertical-align: top;
        }
        .btn-none{
            display:none;
        }
</style>

<div class="col-md-12" ng-controller="welfarerightCtrl" data-ng-init="init()">

     <div class="col-separator box col-separator-first col-unscrollable">
        <div class="col-table">
                   <div class="row">
                      <div class="col-sm-12">
                          <section class="panel">
                              <header class="panel-heading">
                                 <h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?></h3>

                              </header>
                              <div class="panel-body">
                              <form class="form-horizontal " id="welfarerightForm" name="welfarerightForm" method="post" action="<?php echo site_url(); ?>staffwelfareright/submit" >
                            
                                <input type="hidden" name="rightID" value="<?php echo !empty($rightID) ? $rightID : ""; ?>">
                                <input type="hidden" name="staffID" id="staffID" value="<?php echo !empty($staffID) ? $staffID : ""; ?>">
                                <div class="xcrud">
                                  <div class="xcrud-container">
                                      <div class="xcrud-view">
                                        <div class="form-horizontal">
                                          
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">ประเภทสวัสดิการ*</label>
                                            <div class="col-sm-9">
                                               <?php //echo contractType_ddl("contractType",'','class="form-control" ng-change="change()"');?>  
                                               <?php if($this->uri->segment('2') != 'view'){ ?>                                           
                                                  <select 
                                                    ng-init="ddlwelfare = {id: ddl.type}"
                                                    ng-model="ddlwelfare"
                                                    ng-change="ddl.type = ddlwelfare.welTypeID; change() "
                                                    ng-options="value.welTypeName for value in myOptions track by value.welTypeID"
                                                    class="form-control"
                                                    name="welfareType" id="welfareType">
                                                    
                                                  </select>
                                                 <?php }else{
                                                  echo getWelfareTypeName($r->welTypeID);
                                                } ?>  

                                                  
                                            </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-sm-3">รูปแบบ</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              {{welfarefix}}
                                              <?php }else{
                                                    echo getFixName($r->welTypeID);
                                              } ?>                                               
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">วันที่มีผล</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="rightDate" value="<?php echo !empty($r) ? toBEDate($r->rightDate) : ""; ?>" data-type="text" data-required="1" class="xcrud-input xcrud-datepicker form-control" data-type="date">
                                                <?php }else{
                                                echo toBEDateThai($r->rightDate);
                                              } ?>
                                              </div>
                                          </div>
                                          
                                         
                                           <div class="form-group">
                                              <label class="control-label col-sm-3">ผู้ได้รับสิทธิ์</label>
                                              <div class="col-sm-9">
                                                 <?php if($this->uri->segment('2') != 'view'){ 
                                                        //echo getDropdown(array('1'=>'ตนเอง','2'=>'บิดา-มารดา','3'=>'คู่สมรส','4'=>'บุตร'),'rightPersonType',$r->rightPersonType,'class="form-control"');
                                                  ?>
                                                      <select 
                                                        ng-init="ddlperson = {id: ddl.person}"
                                                        ng-model="ddlperson"
                                                        ng-change="ddl.person = ddlperson.id; change_person() "
                                                        ng-options="value.name for value in persons track by value.id"
                                                        class="form-control"
                                                        name="rightPersonType" id="rightPersonType">
                                                        
                                                      </select>
                                                  <?php
                                                   }else{
                                                      
                                                        switch($r->rightPersonType){
                                                          case '1' : echo 'ตนเอง'.'<br>'; break;
                                                          case '2' : echo 'บิดา-มารดา'.'<br>'; break;
                                                          case '3' : echo 'คู่สมรส'.'<br>'; break;
                                                          case '4' : echo 'บุตร'.'<br>'; break;
                                                        }
                                                      
                                                  } ?>                                              
                                            </div>
                                          </div>
                                          
                                          <div class="form-group">
                                              <label class="control-label col-sm-3">ชื่อ - สกุล</label>
                                              <div class="col-sm-9">
                                                 <div id="divperson"></div>                                             
                                            </div>
                                          </div>
                                          

                                      </div>
                                  </div>
                                  <div class="xcrud-top-actions btn-group">
                                      <?php if($this->uri->segment('2') != 'view'){ ?>
                                      <a class="btn btn-primary"  href="javascript:submitForm();">บันทึกและย้อนกลับ</a>
                                      <?php } ?>
                                      <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>staffwelfareright/welfare/?token=<?php echo !empty($staffID) ? $staffID : ""; ?>'">ย้อนกลับ</a>
                                  </div>
                                  </form>
                                  
                              </div>
                          </section>
                      </div>
                  </div>
            
             
                
        </div>  
      </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css">
<script src="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url(); ?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script>
<script>
  $(function(){
      $('.xcrud-datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            changeMonth: true,
            changeYear: true
          });

      

  })

  function submitForm(){
    $('#welfarerightForm').submit();
  }
  
</script>
<script>
        var myApp = angular.module("baseApp",[]);


        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
             
          }
        }]);

        myApp.controller('welfarerightCtrl', ['$scope', '$location','$http', function($scope, $location,$http) {

         $scope.persons = [
            {id:'1', name:'ตนเอง'},
            {id:'2', name:'บิดา-มารดา'},
            {id:'3', name:'คู่สมรส'},
            {id:'4', name:'บุตร'}
          ];

          $scope.init = function () {
              
              if("<?php echo $this->uri->segment('2') ?>" == "view"){

                $http.get("<?php echo site_url()?>staffperson/getStaffPerson", { params: { "id": '<?php echo (!empty($r)) ? $r->rightPersonType : ""?>',"personID": '<?php echo (!empty($r)) ? $r->personID : ""?>' ,"staffID":$('#staffID').val(),"mode":"view"} }).success(function (data, status, header, config) {
                    $('#divperson').html(data);
                   
                }).error(function (data, status, headers, config) {
                    
                });
                
              }else{

                  $http.get("<?php echo site_url()?>welfaretype/getJsonData").success(function (data, status, header, config) {
                    $scope.myOptions = data;
                    $scope.ddlwelfare.welTypeID = '<?php echo (!empty($r)) ? $r->welTypeID : ""?>';
                    $scope.ddlperson.id = '<?php echo (!empty($r)) ? $r->rightPersonType : ""?>';

                    if($scope.ddlwelfare.welTypeID != ""){
                       $http.get("<?php echo site_url()?>welfaretype/getWelfareFix", { params: { "id": $scope.ddlwelfare.welTypeID } }).success(function (data, status, header, config) {
                          $scope.welfarefix = data;
                         
                      }).error(function (data, status, headers, config) {
                          
                      });

                      
                    }

                    if($scope.ddlperson.id != ""){
                        $http.get("<?php echo site_url()?>staffperson/getStaffPerson", { params: { "id": $scope.ddlperson.id ,"staffID":$('#staffID').val(),"personID":'<?php echo (!empty($r)) ? $r->personID : ""?>'} }).success(function (data, status, header, config) {
                          $('#divperson').html(data);
                         
                       }).error(function (data, status, headers, config) {
                          
                       });
                    }

                    

                   
                }).error(function (data, status, headers, config) {
                    
                });



                


              }
          };

          
           
          $scope.change = function(){
              /*var fruitName = $.grep($scope.Fruits, function (fruit) {
                      return fruit.Id == fruitId;
                  })[0].Name;*/

              $http.get("<?php echo site_url()?>welfaretype/getWelfareFix", { params: { "id": $scope.ddlwelfare.welTypeID } }).success(function (data, status, header, config) {
                  $scope.welfarefix = data;
                 
              }).error(function (data, status, headers, config) {
                  
              });

              

          }


          $scope.change_person = function(){
             
              $http.get("<?php echo site_url()?>staffperson/getStaffPerson", { params: { "id": $scope.ddlperson.id ,"staffID":$('#staffID').val()} }).success(function (data, status, header, config) {
                  $('#divperson').html(data);
                 
              }).error(function (data, status, headers, config) {
                  
              });

              

          }


        }]);

</script>
