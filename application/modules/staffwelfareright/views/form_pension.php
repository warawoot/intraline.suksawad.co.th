<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <div id="div_header"><h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user" id="fastaffname"></i>   <?php echo $staffName; ?></small></h3></div>
                
                 <a href="<?php echo site_url(); ?>staffwelfareright" id="btn_back"  class="btn btn-info">ย้อนกลับหน้ากำหนดสิทธิ์สวัสดิการ</a>
                
               

            </header>
            <div class="panel-body">
                <header class="panel-heading tab-bg-dark-navy-blue">

                   
                  <div id="div_myTabs">

                    
                    <ul class="row" id="myTabs">
                      
                     <li id="li_tab_fund" class="list-group-item col-xs-6"><a href="<?php echo site_url();?>staffwelfareright/fund/?token=<?php echo $staffID; ?>"  id="tab_fund">กองทุนสำรองเลี้ยงชีพ</a></li>
                      <li id="li_tab_pension" class="list-group-item col-xs-6"><a href="<?php echo site_url();?>staffwelfareright/pension/?token=<?php echo $staffID; ?>"  id="tab_pension">กองทุนบำเหน็จ</a></li>
                      

                    </ul>
                 </div>
                </header>
                 <div class="panel-body">
                      <div class="tab-content tasi-tab">
                        <div id="ajax-content" class="tab-pane active">
                               <form class="form-horizontal " id="welfarepensionForm" name="welfarepensionForm" method="post" action="<?php echo site_url(); ?>staffwelfareright/submitPensionCollect" >
                            
                                <input type="hidden" name="welfarePID" value="<?php echo !empty($r) ? $r->welfarePID : ""; ?>">
                                <input type="hidden" name="staffID" value="<?php echo !empty($staffID) ? $staffID : ""; ?>">
                                <div class="xcrud">
                                  <div class="xcrud-container">
                                      <div class="xcrud-view">
                                        <div class="form-horizontal">
                                          
                                          <div class="form-group">
                                            <label class="control-label col-sm-3"></label>
                                            <div class="col-sm-9">
                                                
                                               <?php if($mode != 'view'){ 

                                                  $chk = (!empty($r)) ? $r->isApply : "";
                                                  $chk = ($chk == "1") ? "checked='checked'" : "";
                                                ?>
                                                  <input type="checkbox" name="isApply" value="1" <?php echo $chk; ?>> สมัครกองทุนสำรองเลี้ยงชีพ
                                              <?php }else{
                                                    echo ($r->isApply == "1") ? "สมัครกองทุนสำรองเลี้ยงชีพ" : "";
                                              } ?> 
                                                  
                                            </div>
                                          </div>

                                      </div>
                                  </div>
                                  <div class="xcrud-top-actions btn-group">
                                      <?php if($mode != 'view'){ ?>
                                      <a class="btn btn-primary"  href="javascript:submitForm();">บันทึก</a>
                                      <?php } ?>
                                     </div>
                                </form>



                        </div>
                      </div>
                </div>
            </div>
            <div class="separator"></div>
            
        </section>
    </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css">
<script src="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url(); ?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script>
<script>
  $(function(){
      $('.xcrud-datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            changeMonth: true,
            changeYear: true
          });

      

  })

  function submitForm(){
    $('#welfarepensionForm').submit();
  }
  
</script>
<script type="text/javascript">
 
   $(document).ready(function() {

      
        $('#div_header').show();
        $('#div_header_edit').hide();

        var classt = $('#li_tab_pension').attr("class");
        $('#li_tab_pension').attr("class","active "+classt);


     /*$('#myTabs').click(function(e) {  
        src = $(e.target).attr('href');
        id = $(e.target).attr('id');
        //src = $(paneID).attr('data-src');
        var html = "";
        html ='<iframe class="iframetab " src="' + src + '"></iframe>';

        $("#ajax-content").html(html);

        $("iframe").wrap('<div class="embed-responsive embed-responsive-16by9"/>');
        $("iframe").addClass('embed-responsive-item');

        $("li").removeClass("active");
        var classt = $('#li_'+id).attr("class");
        $('#li_'+id).attr("class","active "+classt);
    });*/
    
  });

  /*jQuery(document).on("xcrudafterrequest",function(event,container){
      
      if(Xcrud.current_task == 'edit'){
        $('#div_myTabs').show();
        $('#btn_back').show();
        $('#div_header_edit').show();
        $('#div_header').hide();
        
        var pk = $('input[name="primary"]').val(); 

         $('#tab_fund').attr('href','<?php echo site_url();?>staffwelfareright/fund/?token='+pk);
        $('#tab_pension').attr('href','<?php echo site_url();?>staffwelfareright/pension/?token='+pk);

        $.post('<?php echo site_url();?>reg/getStaffName',{id: pk},
        function(data, status){
            $('#fastaffname').html(data);
      }
    );

      }else{
        $('#div_myTabs').hide();
        $('#btn_back').hide();
        $('#div_header').show();
        $('#div_header_edit').hide();
      }
      
  });*/


</script>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);


</script>
<style>

 .iframetab {
        border:0px;
        margin:0px;
        position: relative;
    }

    .panel-heading .nav > li > a{
        border-bottom: 1px solid transparent;

    }
  
    .panel-heading{
      margin:20px 0 30px 0;
    }

    .nav-tabs > li > a {
        /*background: #DADADA;*/
        border-radius: 0;
        border: 1px solid #ddd;
    }
    ul, .list-unstyled {
        padding: 0 10px;
    }
    .tab-bg-dark-navy-blue ul > li > a {
        display: block;
        padding: 10px 15px !important;
    }
    .tab-bg-dark-navy-blue li a:hover, .tab-bg-dark-navy-blue li.active a {
        background: #fff none repeat scroll 0 0 !important;
        border-radius: 0 !important;
        color: #337ab7 !important;
    }
    .tab-bg-dark-navy-blue li a:hover, .tab-bg-dark-navy-blue li.active a {
        background: #fff none repeat scroll 0 0 !important;
        border-radius: 0 !important;
        color: #337ab7 !important;
    }
    .list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
        background-color: #fff;
        border: 1px solid #ddd;
        color: #ffffff;
        z-index: 2;
    }

    
</style>