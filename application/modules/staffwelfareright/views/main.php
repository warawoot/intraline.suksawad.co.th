<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3><?php echo !empty($title) ? $title : ""; ?> <?php if(isset($staffID)){ ?>: <small><i class="fa fa-user"></i> <?php echo $staffName; ?></small> <?php } ?></h3>
                
               
                <?php if(isset($staffID)){ ?>
                <a class="btn btn-success"   href="javascript:window.location='<?php echo site_url();?>staffwelfareright/add?token=<?php echo $staffID; ?>'"><i class="glyphicon glyphicon-plus-sign" ></i> เพิ่ม</a>
                <style>
                  .xcrud-search-toggle{
                    position:absolute;
                    top:-35px;
                    left:70px;
                  }
                </style>
                <?php } ?>
            </header>
            <div class="panel-body">
                <?php echo $html; ?>
            </div>
        </section>
    </div>
</div>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);


</script>

 