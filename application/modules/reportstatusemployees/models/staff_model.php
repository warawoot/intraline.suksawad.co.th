<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

        public function getByStatus($month,$year,$status){
            $year -= 543;
        	
            $subq = "(SELECT max(workStartDate) from tbl_staff_work 
                    WHERE workStatusID = '$status' AND ";
            if(!empty($month)){
                 $subq.=" MONTH(workStartDate) >= '$month' AND ";              
            }
            if(!empty($year)){
                $subq.=" YEAR(workStartDate) = '$year' AND ";
            }
             $subq.=" staffID=w.staffID)";
             $sql = "SELECT *
                    FROM tbl_staff_work w 
                    JOIN tbl_staff s ON w.staffID = s.ID
                    JOIN tbl_work_status ws ON w.workStatusID = ws.workStatusID
                    WHERE w.workStartDate = ".$subq;    
            
            /*if(!empty($status) || !empty($month) || !empty($year)){
                $sql .= " WHERE";
                if(!empty($status))
                    $sql .= " w.workStatusID = '$status' AND";
                if(!empty($month))
                    $sql .= " MONTH(w.workStartDate) >= '$month' AND";
                if(!empty($year))
                    $sql .= " YEAR(w.workStartDate) = '$year' AND";

                $sql = substr($sql,0,-4);
            }
            $sql.=" GROUP BY w.staffID";*/
            
        	$r = $this->db->query($sql);
        	return $r->result();
        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/Reportstatusemployees/models/staff_model.php */