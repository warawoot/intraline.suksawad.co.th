
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
          ?>
	<h5 style="text-align:center;">
		<br>
			รายงานสถานภาพการทำงานของพนักงาน ประจำเดือน <?php echo $th_month[$month]; ?>
		
	</h5>
	<br>
	ค้นหา 
	เดือน
	
	<select style="width:80px" id="month">
		<?php foreach($th_month as $key=>$val){ ?>
		<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;สถานภาพ 
	<?php echo getDropdown(listData('tbl_work_status',"workStatusID","workStatusName"),"work_status",$status); ?>

	&nbsp;&nbsp;&nbsp;
	<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>
	<br><br>

	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ลำดับ</th>
			<th >ชื่อ-นามสกุล</th>
			<th >ตำแหน่งระดับ</th>
			<th >ระดับ</th>
			<th >แผนก</th>
			<th >กอง</th>
			<th >ฝ่าย</th>
			<th >สถานะ</th>
			<th >ตั้งแต่วันที่</th>
			<th>ถึงวันที่</th>
			<th>หมายเหตุ</th>
			
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 
				$i++;
				?>
				<tr ><!-- td 14 ตัว-->
					<td><?php echo $i; ?></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo getPositionByWork($row->ID);?></td>
					<td><?php echo getRankByWork($row->ID);?></td>
					<td><?php echo getOrgByWork($row->ID);?></td>
					<td><?php echo getOrg2ByWork($row->ID);?></td>
					<td><?php echo getOrg1ByWork($row->ID);?></td>
					<td><?php echo $row->workStatusName; ?></td>
					<td><?php echo toFullDate($row->workStartDate,'th'); ?></td>
					<td>
						<?php
						$rw = $this->db->from('tbl_staff_work')
										->where(array('staffID'=>$row->ID,'workID >'=>$row->workID))
										->order_by('workID','ASC')
										->get();
						if($rw->num_rows() > 0){
						   list($year,$month,$day) = explode("-",$row->workStartDate);
					       list($year_e,$month_e,$day_e) = explode("-",date("Y-m-d",strtotime("-1day",strtotime($row->workStartDate))));
							return toFullDate($year.'-'.$month.'-'.$day,'th').' - '.toFullDate($year_e.'-'.$month_e.'-'.$day_e,'th');
						}else
							echo "ปัจจุบัน";
					 	?>
					</td>
					<td><?php echo $row->workDetail;?></td>
				</tr>
		<?php  }
		}else{ ?>
			<tr><td colspan="14" style="text-align:center">ไม่พบข้อมูล</td></tr>
		<?php } ?>
		
		
	</table>
	</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);
        /*$('#month').change(function(){
        	window.location='<?php echo site_url();?>reportstatusemployees?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&status='+$('#work_status :selected').val();
        })

        $('#year').change(function(){
        	window.location='<?php echo site_url();?>reportstatusemployees?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&status='+$('#work_status :selected').val();
        })

        $('#work_status').change(function(){
        	window.location='<?php echo site_url();?>reportstatusemployees?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&status='+$('#work_status :selected').val();
        })*/
		function search(){
			window.location='<?php echo site_url();?>reportstatusemployees?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&status='+$('#work_status :selected').val();
		}

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportstatusemployees/print_pdf?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&status='+$('#work_status :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportstatusemployees/print_excel?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&status='+$('#work_status :selected').val());
        }

</script>