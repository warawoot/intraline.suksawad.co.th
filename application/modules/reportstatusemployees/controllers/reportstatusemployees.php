<?php

class Reportstatusemployees extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		$data['status'] = (!empty($_GET['status'])) ? $this->input->get('status') : "1";

		$data['r'] = $this->staff_model->getByStatus($data['month'],$data['year'],$data['status']);

		$data['title'] = "รายงานพนักงานพ้นสภาพแต่ละเดือน";
        $this->template->load("template/admin",'reportstatusemployees', $data);

	}

	public function print_pdf(){

		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		$data['status'] = (!empty($_GET['status'])) ? $this->input->get('status') : "1";

		$data['r'] = $this->staff_model->getByStatus($data['month'],$data['year'],$data['status']);

		$data_r['html'] = $this->load->view('print',$data,true);

		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		$data['status'] = (!empty($_GET['status'])) ? $this->input->get('status') : "1";

		$data['r'] = $this->staff_model->getByStatus($data['month'],$data['year'],$data['status']);

		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportstatusemployees.php */
/* Location: ./application/module/reportstatusemployees/reportstatusemployees.php */