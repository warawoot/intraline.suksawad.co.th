<?php

class Staffmove extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_move')->where('staffID',$id);

		$xcrud->relation('orgID1','tbl_org_chart','orgID','orgName','tbl_org_chart.upperOrgID = \'0\'');
		$xcrud->relation('orgID2','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\'','','','','','upperOrgID','orgID1');
		$xcrud->relation('orgID','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\'','','','','','upperOrgID','orgID2');

		$xcrud->relation('orgID1Old','tbl_org_chart','orgID','orgName','tbl_org_chart.upperOrgID = \'0\'');
		$xcrud->relation('orgID2Old','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\'','','','','','upperOrgID','orgID1Old');
		$xcrud->relation('orgIDOld','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\'','','','','','upperOrgID','orgID2Old');
		
		//// List /////
		$col_name = array(
			'moveNumber' => 'เลขที่คำสั่ง',
			'moveDate' => 'วันที่คำสั่ง',
			'moveWorkDate' => 'วันที่ปฏิบัติงาน',
			'orgID1Old' => 'ฝ่ายเดิม',
			'orgID2Old' => 'กองเดิม',
			'orgIDOld' => 'แผนกเดิม',
			'orgID1' => 'ฝ่ายใหม่',
			'orgID2' => 'กองใหม่',
			'orgID' => 'แผนกใหม่'

		);

		$xcrud->columns('moveNumber,moveDate,orgID1Old,orgID2Old,orgIDOld,orgID1,orgID2,orgID');
		$xcrud->column_callback('orgID1Old','getOrgID1');
		$xcrud->column_callback('orgID2Old','getOrgID2');
		$xcrud->column_callback('orgIDOld','getOrgID');
		$xcrud->column_callback('moveDate','toBDDate');
		$xcrud->label($col_name);
		
		// End List//

		//// Form //////
		$xcrud->pass_var('staffID',$id)->pass_default(array('orgID1Old'=>getStaffByID($id,'orgID1'),'orgID2Old'=>getStaffByID($id,'orgID2'),'orgIDOld'=>getStaffByID($id,'orgID')));

		$xcrud->fields('moveNumber,moveDate,moveWorkDate,orgID1Old,orgID2Old,orgIDOld,orgID1,orgID2,orgID');
		$xcrud->validation_required('moveNumber,moveDate,moveWorkDate,orgID1,orgID2,orgID');
		$xcrud->disabled('orgID1Old,orgID2Old,orgIDOld');
		//$xcrud->change_type('orgID1Old','text')->change_type('orgID2Old','text')->change_type('orgIDOld','text');
		// End Form//
		
		$data['html'] = $xcrud->render();
		$data['title'] = "ประวัติเคลื่อนไหว / โยกย้าย";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file staffmove.php */
/* Location: ./application/module/staffmove/staffmove.php */