<style>
.tab-pane,.panel-heading{
    margin-top:30px;
}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?></h3>
            </header>
            <div class="panel-body">
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs nav-justified ">
                            <li class="active">
                                <a href="#first" data-toggle="tab"> ประเภทสัญญาจ้าง </a>
                            </li>
                            <li>
                                <a href="#second" data-toggle="tab"> ตัวแปร </a>
                            </li>
                             <!--<li>
                                <a href="#third" data-toggle="tab"> วุฒิการศึกษาปัจจุบัน </a>
                            </li>-->
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content tasi-tab">

                            <div id="first" class="tab-pane active">

                                <?php echo $html1; ?>
                            </div>

                           <div id="second" class="tab-pane">
                                <?php echo $html2; ?>
                            </div>

                              <!--<div id="third" class="tab-pane">
                                <?php //echo $html3; ?>
                            </div>-->
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
   /* jQuery(document).on("xcrudbeforerequest",function(event,container){
       
    if(Xcrud.current_task == null)
    {
           
            if (typeof(CKEDITOR) != 'undefined') {
             
                CKEDITOR.editorConfig = function( config ) {
        
                    //config.htmlEncodeOutput=>FALSE;
                    //config.entities=>FALSE;
                    //config.enterMode=>'CKEDITOR.ENTER_BR';
                    //config.allowedContent=>TRUE;
                    config.filebrowserBrowseUrl ='<?php echo site_url();?>kcfinder/browse.php?type=files';
                    config.filebrowserImageBrowseUrl= '<?php echo site_url();?>kcfinder/browse.php?type=images';
                    config.filebrowserFlashBrowseUrl= '<?php echo site_url();?>kcfinder/browse.php?type=flash';
                    config.filebrowserUploadUrl= '<?php echo site_url();?>kcfinder/upload.php?type=files';
                    config.filebrowserImageUploadUrl= '<?php echo site_url();?>kcfinder/upload.php?type=images';
                    config.filebrowserFlashUploadUrl='<?php echo site_url();?>kcfinder/upload.php?type=flash';
                    config.filebrowserWindowWidth  = 800;
                    config.filebrowserWindowHeight = 500;
                };
                /*for (instance in CKEDITOR.instances) {
                    alert('bb');
                    if (jQuery('#' + instance).size()) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                }*/
   /*         }
    }
});
    
    
    /*CKEDITOR.editorConfig = function( config ) {
        
        CKEDITOR.config.htmlEncodeOutput=>FALSE;
        CKEDITOR.config.entities=>FALSE;
        CKEDITOR.config.enterMode=>'CKEDITOR.ENTER_BR';
        CKEDITOR.config.allowedContent=>TRUE;
        CKEDITOR.config.filebrowserBrowseUrl=> '<?php echo site_url();?>kcfinder/browse.php?type=files';
        CKEDITOR.config.filebrowserImageBrowseUrl=> '<?php echo site_url();?>kcfinder/browse.php?type=images';
        CKEDITOR.config.filebrowserFlashBrowseUrl=> '<?php echo site_url();?>kcfinder/browse.php?type=flash';
        CKEDITOR.config.filebrowserUploadUrl=> '<?php echo site_url();?>kcfinder/upload.php?type=files';
        CKEDITOR.config.filebrowserImageUploadUrl=> '<?php echo site_url();?>kcfinder/upload.php?type=images';
        CKEDITOR.config.filebrowserFlashUploadUrl=> '<?php echo site_url();?>kcfinder/upload.php?type=flash';
        CKEDITOR.config.filebrowserWindowWidth  : 800;
        CKEDITOR.config.filebrowserWindowHeight : 500;
    };
    /*CKEDITOR.replace('body', {
        htmlEncodeOutput=>FALSE,
        entities=>FALSE,
        enterMode=>'CKEDITOR.ENTER_BR',
        allowedContent=>TRUE,
        filebrowserBrowseUrl=> '<?php echo site_url();?>kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl=> '<?php echo site_url();?>kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl=> '<?php echo site_url();?>kcfinder/browse.php?type=flash',
        filebrowserUploadUrl=> '<?php echo site_url();?>kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl=> '<?php echo site_url();?>kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl=> '<?php echo site_url();?>kcfinder/upload.php?type=flash',
        filebrowserWindowWidth  : 800,
        filebrowserWindowHeight : 500
    });*/

</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
           
          }
        }]);


</script>