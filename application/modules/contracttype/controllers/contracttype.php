<?php

class contracttype extends MY_Controller {
    
	function __construct(){
		parent::__construct();

		$this->setModel('contract_type_model');
		
		
	}
	
	public function index(){

		Xcrud_config::$editor_url = base_url().'editors/ckeditor/ckeditor.js';


		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();


		$xcrud->table('tbl_contract_type');
		$xcrud->relation('rankID','tbl_rank','rankID','rankName');

		
		//// List /////
		$col_name = array(
			'contracttypeType'=>'ประเภทสัญญาจ้าง',
			'contracttypeYear' => 'จำนวนปี',
			'contracttypeDetail' =>'รายละเอียด',
			'numPerson' => 'เป็นบุคคลค้ำประกันผู้อื่นได้จำนวน',
			'rankID' => 'ระดับ'
			
		);

		
		
		
		$xcrud->columns('contracttypeType,contracttypeYear');
		$xcrud->label($col_name);

		//// Form //////

		$xcrud->fields('contracttypeType,contracttypeYear,contracttypeDetail,numPerson,rankID');


		$xcrud->validation_required('contracttypeType,contracttypeYear');

		
		$data['html1'] = $xcrud->render();


		$xcrud2= xcrud_get_instance();
		$xcrud2->table('tbl_contract_field');	
		
		//// List /////
		$col_name = array(
			'fieldName'=>'รายการ',
			/*'fieldType' => 'การประมวลผล',
			'fieldTable' =>'ความหมายของข้อมูล',
			'fieldValue' =>''*/
			'fieldDescription' => 'ความหมายของข้อมูล'
			
		);
	
		$xcrud2->columns('fieldName,fieldDescription');
		$xcrud2->column_width('fieldTable','20%')->column_width('fieldValue','20%');
		$xcrud2->label($col_name);

		//// Form //////

		$xcrud2->fields('fieldName,fieldDescription');
		/*$xcrud2->change_type('fieldType','select','',array('1'=>'แสดงข้อมูล','2'=>'วันปัจจุบัน','3'=>'อายุ'));
		$xcrud2->change_type('fieldTable','select','',$this->config->item('contract_table'));
		$xcrud2->change_type('fieldValue','select','',$this->config->item('contract_field'));*/
		$xcrud2->unset_add()->unset_edit()->unset_view()->unset_remove();

		$data['html2'] = $xcrud2->render();


		$data['title'] = "ประเภทสัญญาจ้าง";
        $this->template->load("template/admin",'main', $data);

	}

	public function getAge(){
		if(!empty($_GET['id'])){

			$id = $this->input->get('id');

			//$model = $this->getModel();
			echo getContractTypeAge($id).' ปี';
			//$r = $this->contract_type_model->get_by(array('contracttypeID'=>$id),true);

			//echo (!empty($r)) ? $r->contracttypeYear.' ปี' : '';

		}
		
	}

	public function getField(){
		if(!empty($_GET['id'])){

			$id = $this->input->get('id');

			$contract_staff = $this->input->get('contract_staff');

			$model = $this->getModel();

			$r = $this->$model->get_by(array('contracttypeID'=>$id),true);

			$text = (!empty($r)) ? $r->contracttypeDetail : "";
			$rankID = (!empty($r)) ? $r->rankID : "";

			$i = 0;
			$html = '';
			$arr_field = array();
			while(strpos($text,"[",$i)){

				$pos = strpos($text,"[",$i);
				$pos_e = strpos($text,"]",$i);

				if($pos_e){
					$sub_text = substr($text,$pos+1,$pos_e-($pos+1));

					$r = $this->db->get_where('tbl_contract_field',array('fieldName'=>$sub_text));

					if($r->num_rows() > 0){
						if($r->row()->fieldType != '2' && $r->row()->fieldType != '3' && $r->row()->fieldType != '9' && $r->row()->fieldType != '10')
							$arr_field[] = $r->row();
						
					}
				}
				

				$i=$pos_e+1;
				

			}

			$arr_s = array();
			if(!empty($contract_staff)){
				$r_s = $this->db->get_where('tbl_staff_contract',array('contractID'=>$contract_staff));
				$arr_s = ($r_s->num_rows() > 0) ? json_decode($r_s->row()->contractField) : array();
			}
		
			$html='<table class="table table-striped table-hover table-bordered"><thead><tr><td>ความหมายของข้อมูล</td><td>ระบุข้อมูล</td></tr></thead>';
			for($i=0; $i<count($arr_field); $i++){
				 
				if($arr_field[$i]->fieldType == '1'){ // from db
					$html.='<input type="hidden" name="field[]" value="'.$arr_field[$i]->fieldID.'">';
					if(isset($_GET['mode'])){

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listData($arr_field[$i]->fieldTable,getPK($arr_field[$i]->fieldTable),$arr_field[$i]->fieldValue),"fieldvalue[]",$value,"class='form-control' disabled").'</td></tr>';
					}else{
						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listData($arr_field[$i]->fieldTable,getPK($arr_field[$i]->fieldTable),$arr_field[$i]->fieldValue),"fieldvalue[]",$value,"class='form-control'").'</td></tr>';
					}
				}else if($arr_field[$i]->fieldType == '4'){//StaffName
					$html.='<input type="hidden" name="field[]" value="'.$arr_field[$i]->fieldID.'">';
					if(isset($_GET['mode'])){

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listData('tbl_staff','ID','staffPreName,staffFName,staffLName'),"fieldvalue[]",$value,"class='form-control' disabled").'</td></tr>';
					}else{

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listData('tbl_staff','ID','staffPreName,staffFName,staffLName'),"fieldvalue[]",$value,"class='form-control'").'</td></tr>';
					}
				}else if($arr_field[$i]->fieldType == '5'){// Etc
					$html.='<input type="hidden" name="field[]" value="'.$arr_field[$i]->fieldID.'">';
					if(isset($_GET['mode'])){

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.$value.'</td></tr>';
					}else{
						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td><input type="text" name="fieldvalue[]" value="'.$value.'" class="form-control"></td></tr>';
					}
				}else if($arr_field[$i]->fieldType == '6'){ // GuaranteeName
					$html.='<input type="hidden" name="field[]" value="'.$arr_field[$i]->fieldID.'">';
					if(isset($_GET['mode'])){

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$sql = "SELECT w.workID,s.ID,s.staffPreName,s.staffFName,s.staffLName
								FROM tbl_staff_work w 
								JOIN tbl_staff s ON w.staffID = s.ID
								LEFT JOIN tbl_rank r ON r.rankID = w.rankID
								WHERE w.workID = (SELECT max(workID) from tbl_staff_work WHERE staffID=w.staffID) and CONVERT(r.rankName, SIGNED INTEGER) >= '$rankID'";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listDataSql($sql,'ID','staffPreName,staffFName,staffLName'),"fieldvalue[]",$value,"class='form-control' disabled").'</td></tr>';
					}else{

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$sql = "SELECT w.workID,s.ID,s.staffPreName,s.staffFName,s.staffLName
								FROM tbl_staff_work w 
								JOIN tbl_staff s ON w.staffID = s.ID
								LEFT JOIN tbl_rank r ON r.rankID = w.rankID
								WHERE w.workID = (SELECT max(workID) from tbl_staff_work WHERE staffID=w.staffID) and CONVERT(r.rankName, SIGNED INTEGER) >= '$rankID'";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listDataSql($sql,'ID','staffPreName,staffFName,staffLName'),"fieldvalue[]",$value,"class='form-control'").'</td></tr>';
					}
				}else if($arr_field[$i]->fieldType == '7' || $arr_field[$i]->fieldType == '11' || $arr_field[$i]->fieldType == '12' || $arr_field[$i]->fieldType == '13'){ //Date
					$html.='<input type="hidden" name="field[]" value="'.$arr_field[$i]->fieldID.'">';
					if(isset($_GET['mode'])){

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.$value.'</td></tr>';
					}else{
						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td><input type="text" name="fieldvalue[]" value="'.$value.'" class="xcrud-datepicker form-control"></td></tr>';
					}
				}else if($arr_field[$i]->fieldType == '8'){ //HeadName
					$html.='<input type="hidden" name="field[]" value="'.$arr_field[$i]->fieldID.'">';
					if(isset($_GET['mode'])){

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						$sql = "SELECT w.workID,s.ID,s.staffPreName,s.staffFName,s.staffLName
								FROM tbl_staff_work w 
								JOIN tbl_staff s ON w.staffID = s.ID
								WHERE w.workID = (SELECT max(workID) from tbl_staff_work WHERE staffID=w.staffID and w.positionID <= '4' )";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listDataSql($sql,'ID','staffPreName,staffFName,staffLName'),"fieldvalue[]",$value,"class='form-control' disabled").'</td></tr>';
					}else{

						$value = (!empty($arr_s[$i]->value)) ? $arr_s[$i]->value : "";
						//$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listDataJoin('tbl_staff_work','ID','staffPreName,staffFName,staffLName','','tbl_position','tbl_position.positionID = tbl_staff_work.positionID',array('CONVERT(tbl_position.positionID, SIGNED INTEGER) <='=>'4'),'tbl_staff','tbl_staff.ID = tbl_staff_work.staffID'),"fieldvalue[]",$value,"class='form-control'").'</td></tr>';
						$sql = "SELECT w.workID,s.ID,s.staffPreName,s.staffFName,s.staffLName
								FROM tbl_staff_work w 
								JOIN tbl_staff s ON w.staffID = s.ID
								WHERE w.workID = (SELECT max(workID) from tbl_staff_work WHERE staffID=w.staffID and w.positionID <= '4' )";
						$html.='<tr><td>'.$arr_field[$i]->fieldName.'</td><td>'.getDropdown(listDataSql($sql,'ID','staffPreName,staffFName,staffLName'),"fieldvalue[]",$value,"class='form-control'").'</td></tr>';
							
					}
				}
				
			}
			$html.='</table>';
			echo $html;
			
		}
		
	}



	

}
/* End of file staffcontract.php */
/* Location: ./application/module/staffcontract/staffcontract.php */