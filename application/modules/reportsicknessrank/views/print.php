
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h4 style="text-align:center;">
		<br>
			รายงานสถิติการเจ็บป่วยของพนักงาน <?php echo (!empty($day)) ? 'วันที่ '.$day : ""; ?> <?php echo (!empty($month)) ? 'ประจำเดือน '.$th_month[$month] : ""; ?> <?php echo (!empty($year)) ? 'ปี '.$year : ""; ?>
		
	</h4>
	
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ฝ่าย</th>
			<th>กอง</th>
			<th >แผนก</th>
			<th >ชื่อ-สกุล</th>
			<th>วันที่เบิก</th>
			<th>โรค</th>
			<th>วันที่รักษา</th>
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){
				$list = explode("/",$row);

				$rp = $this->staff_welfare_take_model->getAll($list[0]);
				$numrow = ($rp != NULL) ? $rp->num_rows() : 1;
				$numrow = ($numrow > 1) ? $numrow+1 : $numrow;
				/*echo $this->db->last_query(); exti;
				$price = 0;
				if($rp != NULL){
					foreach($rp->result() as $rowp){
						$arrp = json_decode($rowp->takePrice);
						foreach($arrp as $p){
							$price+=$p->request;
						}
						
					}
				}*/



				echo '<tr>';
				if($list[2] == 0 || $list[2] == 1){
					echo '<td colspan="3" rowspan = "'.$numrow.'">'.$list[1].'</td>';
				}else if($list[2] == 2){
					echo '<td rowspan = "'.$numrow.'"></td>';
					echo '<td colspan="2" rowspan = "'.$numrow.'">'.$list[1].'</td>';
				}else if($list[2] == 3){
					echo '<td colspan="2" rowspan = "'.$numrow.'"></td>';
					echo '<td rowspan = "'.$numrow.'">'.$list[1].'</td>';
				}
				if($rp != NULL){
					$j=0;
					foreach($rp->result() as $rowp){
						if($j > 1)
							echo '<tr>';
						echo '<td>'.$rowp->staffFName.' '.$rowp->staffLName.'</td>';
						echo '<td>'.toBEDate($rowp->takeDate).'</td>';

						echo '<td>'.$rowp->diseaseName.'</td>';
						
						echo '<td>'.toBEDate($rowp->takeStartDate).' - '.toBEDate($rowp->takeEndDate).'</td>';
						if($j > 1)
							echo '</tr>';
					}
				}else{
					echo '<td colspan="4"></td>';
				}
				
				//echo '<td colspan="4">'.$list[1].' '.$list[0].' '.$list[2].'</td>';
				echo '</tr>';
			}
		}?>
		
		
	</table>
	</div>