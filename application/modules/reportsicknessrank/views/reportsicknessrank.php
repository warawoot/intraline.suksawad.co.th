
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h5 style="text-align:center;">
		<br>
			รายงานสถิติการเจ็บป่วยของพนักงาน <?php echo $th_month[$month].' ปี '.$year; ?>
		
	</h5>
	<br>
	ค้นหา 
	วัน
	<select style="width:80px" id="day">
		<option value="" <?php echo ($day == "") ? "selected" : ""; ?>>-- กรุณาเลือก --</option>
		<?php $count = $day_arr[intval($month)];

		for($i=1; $i<=$count; $i++){ ?>
			<option value="<?php echo $i; ?>" <?php echo ($i == $day) ? "selected" : ""; ?>><?php echo $i; ?></option>
		<?php } ?>
		
	</select>


	&nbsp;&nbsp;&nbsp;เดือน
	
	<select style="width:80px" id="month">
		<?php foreach($th_month as $key=>$val){ ?>
		<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;
		ถึง 
	วัน
	<select style="width:80px" id="day">
		<option value="" <?php echo ($day == "") ? "selected" : ""; ?>>-- กรุณาเลือก --</option>
		<?php $count = $day_arr[intval($month)];

		for($i=1; $i<=$count; $i++){ ?>
			<option value="<?php echo $i; ?>" <?php echo ($i == $day) ? "selected" : ""; ?>><?php echo $i; ?></option>
		<?php } ?>
		
	</select>


	&nbsp;&nbsp;&nbsp;เดือน
	
	<select style="width:80px" id="month">
		<?php foreach($th_month as $key=>$val){ ?>
		<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
		<?php } ?>
		
	</select>
	&nbsp;&nbsp;&nbsp;ปี พ.ศ. 
	<select style="width:80px" id="year">
		<?php for($i=date("Y");  $i>=(date("Y")-59); $i--){ ?>
		<option value="<?php echo $i+543; ?>" <?php echo (($i+543) == $year) ? "selected" : ""; ?>><?php echo $i+543; ?></option>
		<?php } ?>
		
	</select>&nbsp;&nbsp;&nbsp;
	<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>

	
	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ฝ่าย</th>
			<th>กอง</th>
			<th >แผนก</th>
			<th >ชื่อ-สกุล</th>
			<th>วันที่เบิก</th>
			<th>โรค</th>
			<th>วันที่รักษา</th>
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){
				$list = explode("/",$row);

				$rp = $this->staff_welfare_take_model->getAll($list[0]);
				$numrow = ($rp != NULL) ? $rp->num_rows() : 1;
				$numrow = ($numrow > 1) ? $numrow+1 : $numrow;
				/*echo $this->db->last_query(); exti;
				$price = 0;
				if($rp != NULL){
					foreach($rp->result() as $rowp){
						$arrp = json_decode($rowp->takePrice);
						foreach($arrp as $p){
							$price+=$p->request;
						}
						
					}
				}*/



				echo '<tr>';
				if($list[2] == 0 || $list[2] == 1){
					echo '<td colspan="3" rowspan = "'.$numrow.'">'.$list[1].'</td>';
				}else if($list[2] == 2){
					echo '<td rowspan = "'.$numrow.'"></td>';
					echo '<td colspan="2" rowspan = "'.$numrow.'">'.$list[1].'</td>';
				}else if($list[2] == 3){
					echo '<td colspan="2" rowspan = "'.$numrow.'"></td>';
					echo '<td rowspan = "'.$numrow.'">'.$list[1].'</td>';
				}
				if($rp != NULL){
					$j=0;
					foreach($rp->result() as $rowp){
						if($j > 1)
							echo '<tr>';
						echo '<td>'.$rowp->staffFName.' '.$rowp->staffLName.'</td>';
						echo '<td>'.toBEDate($rowp->takeDate).'</td>';

						echo '<td>'.$rowp->diseaseName.'</td>';
						
						echo '<td>'.toBEDate($rowp->takeStartDate).' - '.toBEDate($rowp->takeEndDate).'</td>';
						if($j > 1)
							echo '</tr>';
						$j++;
					}
				}else{
					echo '<td colspan="4"></td>';
				}
				
				//echo '<td colspan="4">'.$list[1].' '.$list[0].' '.$list[2].'</td>';
				echo '</tr>';
			}
		}?>
		
		
	</table>
	</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function search(){
        	window.location='<?php echo site_url();?>reportsicknessrank?day='+$('#day :selected').val()+'&month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
        }


        $('#month').change(function(){
        	day_arr = ["","31","29","31","30","31","30","31","31","30","31","30","31"];  
   			day = $('#day :selected').val();
        	month = $('#month :selected').val();
        	count = day_arr[parseInt(month)];
        	sel = (day == "") ? 'selected' : '';
        	html = "<option value='' "+sel+">-- กรุณาเลือก --</option>";

			for(i=1; i<=count; i++){ 

				sel = (day == i) ? 'selected' : '';
				html+='<option value="'+i+'" '+sel+'>'+i+'</option>';
			 } 
			 $("#day").html(html);
        })
        

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportsicknessrank/print_pdf?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&day='+$('#day :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportsicknessrank/print_excel?month='+$('#month :selected').val()+'&year='+$('#year :selected').val()+'&day='+$('#day :selected').val());
        }

</script>