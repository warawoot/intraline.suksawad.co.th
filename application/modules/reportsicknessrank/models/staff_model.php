<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

        public function getByBirthday($day,$month,$year){
             if(!empty($day) || !empty($month) || !empty($year)){

                $year -= 543;
            
                 $sql = "SELECT * 
                        FROM tbl_staff
                        WHERE ";
                if(!empty($day)){
                    $sql.=" DAY(staffBirthday) = '$day' AND ";
                }
                if(!empty($month)){
                     $sql.=" MONTH(staffBirthday) = '$month' AND ";
                  
                }
                if(!empty($year)){
                    $sql.=" YEAR(staffBirthday) = '$year' AND ";
                }
                
                $sql = substr($sql,0,-4);

                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r : NULL;
            }
            
        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/Reportstatusemployees/models/staff_model.php */