<?php

class reportsicknessrank extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_welfare_take_model');
		
	}
	
	public function index(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$assignID = getAssignLasted();
        $ida = array();
        $this->getCategoryTree(0, $assignID,$ida); 
		
		$data['r'] = $ida;

        $this->template->load("template/admin",'reportsicknessrank', $data);

	}

	function getCategoryTree($upper = 0 , $assignID=0,&$ida) {
        
        $rows = $this->db
            ->select('t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t1.upperOrgID')
            ->where('t1.upperOrgID', $upper)
            ->where('t1.assignID', $assignID)
            ->order_by('sequence','asc')
            ->get('tbl_org_chart as t1')
            ->result();
         ;
        $tree = '';
         
        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {

                    
                    
                     if(count($ida) > 0){
                        if($row->upperOrgID == 1) {
                            $level = 1;
                        } else{
                            for($i=0; $i<count($ida);$i++){
                                $list = explode('/',$ida[$i]);
                                if($list[0] == $row->upperOrgID){
                                    $level = $list[2]+1;
                                    break;
                                }              
                            }
                          
                        }
                    }else
                        $level = 0;

                        array_push($ida,$row->orgID.'/'.$row->orgName.'/'.$level.'/'.$row->upperOrgID);
                        //echo $row->orgID.' '.$row->orgName.' '.$level.' '.$row->upperOrgID.'<br>';
                    $this->getCategoryTree($row->orgID, $row->assignID,$ida);
            }
        }
        
    }

	public function print_pdf(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$assignID = getAssignLasted();
        $ida = array();
        $this->getCategoryTree(0, $assignID,$ida); 
		
		$data['r'] = $ida;

		$data_r['html'] = $this->load->view('print',$data,true);
		
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$assignID = getAssignLasted();
        $ida = array();
        $this->getCategoryTree(0, $assignID,$ida); 
		
		$data['r'] = $ida;
		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportsicknessrank.php */
/* Location: ./application/module/reportsicknessrank/reportsicknessrank.php */