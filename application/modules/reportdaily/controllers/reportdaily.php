<?php


class Reportdaily extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('reportdaily_model');
	    $this->load->model('organization/organization_model');
	    //date_default_timezone_set("Asia/Bangkok");
	   	echo "<meta charset='utf-8'>";

	}
	
	public function index(){
		
		$agencies = $this->input->get_post('agencies');
		$datestr = $this->input->get_post('datestr');

        if($datestr == "") {
            $datestr = date("d/m/").(date("Y")+543);
        } 

        $data['datestr'] = $datestr;
		$date_start 		= explode('/',$datestr); //สร้างตัวแปรมาเก็บวันที่
		$strYear_s 			= $date_start[2]-543;
		$strMonth_s			= $date_start[1];
		$strDay_s			= $date_start[0];
        $strDate_s 		    = $strYear_s."-".$strMonth_s."-".$strDay_s;  

        $orgIDManage = $this->session->userdata('orgIDManage');
        if(empty($this->input->get('orgID'))){
        	$selected= $orgIDManage;
        	$dataOrg = $this->reportdaily_model->getOrgReg($orgIDManage);
        }else{
        	$selected= $this->input->get('orgID');
        	$dataOrg = $this->reportdaily_model->getOrgReg($this->input->get('orgID'));
        }
        //echo $this->db->last_query();
        
		//พนักงานที่ลาในแต่ละวัน
		if(!empty($dataOrg)) {
			$tableReportOrg = "<table class='table table-striped'>
									<tr>
										<th class='thlight'>พนักงานที่ลา</th>
									</tr>";
			foreach ($dataOrg as $row) {
				$dataLeave = "";
				$tableLeaveOrg = "<tr>
									<td><b>".$row->orgName."</b></td> 
								  </tr>";
	            $ag = $row->orgID.$this->organization_model->getSubOrg($row->orgID);
	            $tableLeaveOrg .= "<tr><td>";

				$dataLeave = $this->reportdaily_model->getLeave($strDate_s,$ag);
				
	            if(!empty($dataLeave)) {
					$tableLeaveOrg .=  "<table class='table table-striped'>
											<tr class='lighttr'>
												<th width='10%'>รหัส</th>
												<th width='20%'>ชื่อ</th>
												<th width='20%'>หน่วยงาน</th>
												<th width='20%'>ประเภทการลา</th>
												<th width='15%'>เวลาเริ่มลา</th>
												<th width='15%'>สิ้นสุดการลา</th>
											</tr>
										";
					foreach ($dataLeave as $rowLeave) {
						if(($rowLeave->status_absence)==2){
							$time_start = '<br>เวลา '.toBETime($rowLeave->absStartDate);
							$time_end 	= '<br>เวลา '.toBETime($rowLeave->absEndDate);
						}else{
							$time_start	= '';
							$time_end	= '';
						}
						
						$tableLeaveOrg .=  "<tr>
												<td>".$rowLeave->staffID."</td> 
												<td>".$rowLeave->staffFName." ".$rowLeave->staffLName."</td>
												<td>".$rowLeave->orgName."</td>
												<td>".$rowLeave->absName."</td>
												<td>".toBEDateThai($rowLeave->absStartDate).$time_start."</td>
												<td>".toBEDateThai($rowLeave->absEndDate).$time_end."</td>
											  </tr>";
					}
					$tableLeaveOrg .=  "</table>";
					$tableLeaveOrg .= "</td></tr>";
					$tableReportOrg .= $tableLeaveOrg;
				}

			}
			$tableReportOrg .= "</table>";
			$data['tableReportOrg'] = $tableReportOrg;
		}      

		//พนักงานมาสาย
		if(!empty($dataOrg)) {
			$tableReportOrgs = "<table class='table table-striped'>
									<tr>
										<th class='thlight'>พนักงานมาสาย</th>
									</tr>";
			foreach ($dataOrg as $row) {
				$dataLate = "";
				$tableLateOrgs =    "<tr>
										<td><b>".$row->orgName."</b></td> 
									  </tr>";
	            $ag = $row->orgID.$this->organization_model->getSubOrg($row->orgID);
	            $tableLateOrgs .= "<tr><td>";

				$dataLate = $this->reportdaily_model->getLate($strDate_s,$ag);
				
		if(!empty($dataLate)) {
			$tableLateOrgs .= "<table class='table table-striped'>
									<tr class='lighttr'>
										<th width='15%'>รหัส</th>
										<th width='25%'>ชื่อ</th>
										<th width='25%'>หน่วยงาน</th>
										<th width='20%'>เวลามาสาย</th>
										<th width='15%'>จัดการ</th>
									</tr>
								";
			foreach ($dataLate as $row) {
				$timedaily = toBETime($row->dateDaily);
				
				$chk = $this->reportdaily_model->chkApprove($row->staffID,$row->dateDaily,1);
				if($chk->num_rows()==0){
					$a_btn = "<a data-toggle='modal' href='#modal_late".$row->staffID."'><button type='button' class='btn btn-success '>A</button></a>"; 
			        $l_url = site_url('absence'); 
			        $l_btn = "<a href='$l_url'><button type='button' class='btn btn-info'>L</button></a>";
			        $btn_group = "<div class='btn-group text-center'> 
					                      ".$a_btn.$l_btn." 
					                      </div> ";
					
			        $strike_open = "";
					$strike_close = "";
					$comment = "";
					$changeTime='';
					$text_comment ='';
					$action = 'insert';
					$chk_radio = '';
					$approveID = '';
					$time2 = $timedaily;
				}else{
					$chkRs = $chk->result();
					$approveID = $chkRs[0]->approveID;
					$conf_del = "onclick='CancelApprove(".$approveID.")'";
					$e_btn = "<a data-toggle='modal' href='#modal_late".$row->staffID."'><button type='button' class='btn btn-warning '><i class='fa fa-edit'></i></button></a>"; 
					$d_btn = "<a ".$conf_del."><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>";
					$btn_group = "<div class='btn-group text-center'> 
					                      ".$e_btn.$d_btn." 
					                      </div> ";
					
					$strike_open = "<strike style='color:red;'>";
					$strike_close = "</strike>";
					
					$text_comment = $chkRs[0]->approveDetail;
					$comment = "<br><span style='color:red;'>( ".$text_comment." )</span>";
					if($timedaily != toBETime($chkRs[0]->approvedTime2)){
						$changeTime = ' '.toBETime($chkRs[0]->approvedTime2);
						$time2 = toBETime($chkRs[0]->approvedTime2);
					}else{
						$changeTime = '';
						$time2 = $timedaily;
					}

					$action = 'update';
					$chk_radio = 'checked';
					
					
				}



				$tableLateOrgs .= " <tr>
										<td>".$strike_open.$row->staffID.$strike_close."</td> 
										<td>".$strike_open.$row->staffFName." ".$row->staffLName.$strike_close."</td>
										<td>".$strike_open.$row->orgName.$strike_close."</td>
										<td>".$strike_open.$timedaily.$strike_close.$changeTime.$comment."     
					                      
					                    </td> 
					                    <td align='center' style='padding-left:3px;padding-right:3px;text-align:center;'>".$btn_group."</td>
									</tr>";
				$tableLateOrgs .= '<div class="modal fade" id="modal_late'.$row->staffID.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
                      <div class="modal-dialog" style="padding-top: 10%;"> 
                        <div class="modal-content"> 
                        <form method="post" action="'.site_url('reportdaily/approve').'"> 
                          <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
                            <h4 class="modal-title">แบบฟอร์มอนุมัติ</h4> 
                          </div> 
                          <div class="modal-body">
                              <div class="form-group"> 
                              	<label><b>หมายเหตุ/เหตุผล</b></label>
                              	<div class="form-group"> 
                              		<input type="radio" name="reson" value="ลืมบัตร" required> ลืมบัตร<br>
                              		<input type="radio" name="reson" value="แสกนไม่ติด" required> แสกนไม่ติด<br>
                              		<input type="radio" name="reson" value="ทำธุระ" required> ทำธุระ<br>
                              		<input type="radio" name="reson" value="" required '.$chk_radio.'> อื่นๆ
                              	</div>
                                  <textarea name="approveDetail" class="form-control" rows="5">'.$text_comment.'</textarea> 
                              </div> 
                              <div class="form-group"> 
                              	<label><b>เวลา</b></label>
                              	<div class="input-group">
             						<input name="edittime" type="time" readonly id="changeTimeLate'.$row->staffID.'" class="form-control" value="'.$time2.'"> 
             						<span class="input-group-btn">
            							<button type="button" onclick="changeTimeLate('.$row->staffID.')" style="line-height: 1.0;" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> แก้ไข</button>
        							</span>
             					</div>
                              
                              </div>
                             
                          </div> 
                          <div class="modal-footer" style="text-align: right"> 
                            <input name ="staffID" hidden value="'.$row->staffID.'"> 
                            <input name ="dateDaily" hidden value="'.$row->dateDaily.'"> 
                            <input name ="uri_segment" hidden value="'."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".'">
                            <input name ="periodType" hidden value="1"> 
                            <input name ="action" hidden value="'.$action.'"> 
                            <input name ="approveID" hidden value="'.$approveID.'"> 
                            <button type="submit" name="approve" class="btn btn-success center">Approve</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
                          </div> 
                        </form> 
                        </div> 
                      </div> 
                    </div>';   
			}
			$tableLateOrgs .= "</table>";
			$tableLateOrgs .= "</td></tr>";	
			$tableReportOrgs .= $tableLateOrgs;
			
			}
				
		}

		$tableReportOrgs .= "</table>";
		$data['tableReportLate'] = $tableReportOrgs;
		}  


	// มาสายเกินเวลาลา
	if(!empty($dataOrg)) {
					$tableReportOrgs = "<table class='table table-striped'>
									<tr>
										<th class='thlight'>พนักงานมาสายเกินเวลาที่ลา</th>
									</tr>";
			foreach ($dataOrg as $row) {
				$dataLate = "";
				$tableLateOrgs =    "<tr>
										<td><b>".$row->orgName."</b></td> 
									  </tr>";
	            $ag = $row->orgID.$this->organization_model->getSubOrg($row->orgID);
	            $tableLateOrgs .= "<tr><td>";

				$dataLate = $this->reportdaily_model->getLateFromAbsenceHour($strDate_s,$ag);
				
		if($dataLate->num_rows > 0) {
			$tableLateOrgs .= "<table class='table table-striped'>
									<tr class='lighttr'>
										<th width='15%'>รหัส</th>
										<th width='15%'>ชื่อ</th>
										<th width='20%'>หน่วยงาน</th>
										<th width='15%'>ช่วงเวลาที่ลา</th>
										<th width='20%'>เวลามาสาย</th>
										<th width='15%'>จัดการ</th>
									</tr>
								";
			foreach ($dataLate->result() as $row) {
				$timedaily = toBETime($row->dateDaily);
				
				$chk = $this->reportdaily_model->chkApprove($row->staffID,$row->dateDaily,1);
				if($chk->num_rows()==0){
					$a_btn = "<a data-toggle='modal' href='#modal_late".$row->staffID."'><button type='button' class='btn btn-success '>A</button></a>"; 
			        $l_url = site_url('absence'); 
			        $l_btn = "<a href='$l_url'><button type='button' class='btn btn-info'>L</button></a>";
			        $btn_group = "<div class='btn-group text-center'> 
					                      ".$a_btn.$l_btn." 
					                      </div> ";
					
			        $strike_open = "";
					$strike_close = "";
					$comment = "";
					$changeTime='';
					$text_comment ='';
					$action = 'insert';
					$chk_radio = '';
					$approveID = '';
					$time2 = $timedaily;
				}else{
					$chkRs = $chk->result();
					$approveID = $chkRs[0]->approveID;
					$conf_del = "onclick='CancelApprove(".$approveID.")'";
					$e_btn = "<a data-toggle='modal' href='#modal_late".$row->staffID."'><button type='button' class='btn btn-warning '><i class='fa fa-edit'></i></button></a>"; 
					$d_btn = "<a ".$conf_del."><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>";
					$btn_group = "<div class='btn-group text-center'> 
					                      ".$e_btn.$d_btn." 
					                      </div> ";
					
					$strike_open = "<strike style='color:red;'>";
					$strike_close = "</strike>";
					
					$text_comment = $chkRs[0]->approveDetail;
					$comment = "<br><span style='color:red;'>( ".$text_comment." )</span>";
					if($timedaily != toBETime($chkRs[0]->approvedTime2)){
						$changeTime = ' '.toBETime($chkRs[0]->approvedTime2);
						$time2 = toBETime($chkRs[0]->approvedTime2);
					}else{
						$changeTime = '';
						$time2 = $timedaily;
					}

					$action = 'update';
					$chk_radio = 'checked';
					
					
				}



				$tableLateOrgs .= " <tr>
										<td>".$strike_open.$row->staffID.$strike_close."</td> 
										<td>".$strike_open.$row->staffFName." ".$row->staffLName.$strike_close."</td>
										<td>".$strike_open.$row->orgName.$strike_close."</td>
										<td>".toBETime($row->absStartDate)." - ".toBETime($row->absEndDate)."</td>
										<td>".$strike_open.$timedaily.$strike_close.$changeTime.$comment."     
					                      
					                    </td> 
					                    <td align='center' style='padding-left:3px;padding-right:3px;text-align:center;'>".$btn_group."</td>
									</tr>";
				$tableLateOrgs .= '<div class="modal fade" id="modal_late'.$row->staffID.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
                      <div class="modal-dialog" style="padding-top: 10%;"> 
                        <div class="modal-content"> 
                        <form method="post" action="'.site_url('reportdaily/approve').'"> 
                          <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
                            <h4 class="modal-title">แบบฟอร์มอนุมัติ</h4> 
                          </div> 
                          <div class="modal-body">
                              <div class="form-group"> 
                              	<label><b>หมายเหตุ/เหตุผล</b></label>
                              	<div class="form-group"> 
                              		<input type="radio" name="reson" value="ลืมบัตร" required> ลืมบัตร<br>
                              		<input type="radio" name="reson" value="แสกนไม่ติด" required> แสกนไม่ติด<br>
                              		<input type="radio" name="reson" value="ทำธุระ" required> ทำธุระ<br>
                              		<input type="radio" name="reson" value="" required '.$chk_radio.'> อื่นๆ
                              	</div>
                                  <textarea name="approveDetail" class="form-control" rows="5">'.$text_comment.'</textarea> 
                              </div> 
                              <div class="form-group"> 
                              	<label><b>เวลา</b></label>
                              	<div class="input-group">
             						<input name="edittime" type="time" readonly id="changeTimeLate'.$row->staffID.'" class="form-control" value="'.$time2.'"> 
             						<span class="input-group-btn">
            							<button type="button" onclick="changeTimeLate('.$row->staffID.')" style="line-height: 1.0;" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> แก้ไข</button>
        							</span>
             					</div>
                              
                              </div>
                             
                          </div> 
                          <div class="modal-footer" style="text-align: right"> 
                            <input name ="staffID" hidden value="'.$row->staffID.'"> 
                            <input name ="dateDaily" hidden value="'.$row->dateDaily.'"> 
                            <input name ="uri_segment" hidden value="'."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".'">
                            <input name ="periodType" hidden value="1"> 
                            <input name ="action" hidden value="'.$action.'"> 
                            <input name ="approveID" hidden value="'.$approveID.'"> 
                            <button type="submit" name="approve" class="btn btn-success center">Approve</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
                          </div> 
                        </form> 
                        </div> 
                      </div> 
                    </div>';   
			}
			$tableLateOrgs .= "</table>";
			$tableLateOrgs .= "</td></tr>";	
			$tableReportOrgs .= $tableLateOrgs;
			
			}
				
		}

		$tableReportOrgs .= "</table>";
		$data['tableReportLateHour'] = $tableReportOrgs;
	}   


	//พนักงานออกก่อนเวลา
		if(!empty($dataOrg)) {
			$tableReportOutTime = "<table class='table table-striped'>
									<tr>
										<th class='thlight'>พนักงานที่ออกก่อนเวลา</th>
									</tr>";
			foreach ($dataOrg as $row) {
				$dataOutTime = "";
				$tableOutTime =    "<tr>
										<td><b>".$row->orgName."</b></td> 
									  </tr>";
	            $ag = $row->orgID.$this->organization_model->getSubOrg($row->orgID);
	            $tableOutTime .= "<tr><td>";

				$dataOutTime = $this->reportdaily_model->getOutTime($strDate_s,$ag);
				
		if(!empty($dataOutTime)) {

			$tableOutTime .= "<table class='table table-striped'>
									<tr class='lighttr'>
										<th width='15%'>รหัส</th>
										<th width='25%'>ชื่อ</th>
										<th width='25%'>หน่วยงาน</th>
										<th width='20%'>เวลากลับ</th>
										<th width='15%'>จัดการ</th>
									</tr>
								";
			foreach ($dataOutTime as $row) {
				$timedaily = toBETime($row->dateDaily);
				$chk = $this->reportdaily_model->chkApprove($row->staffID,$row->dateDaily,3);
				if($chk->num_rows()==0){
					$a_btn = "<a data-toggle='modal' href='#modal_OutTime".$row->staffID."'><button type='button' class='btn btn-success'>A</button></a>"; 
			        $l_url = site_url('absence'); 
			        $l_btn = "<a href='$l_url'><button type='button' class='btn btn-info'>L</button></a>"; 

			        $btn_group = "<div class='btn-group text-center'> 
					                      ".$a_btn.$l_btn." 
					                      </div> ";
			        $strike_open = "";
					$strike_close = "";
					$comment = "";
					$action = 'insert';
					$chk_radio = '';
					$approveID = '';
					$time2 = $timedaily;
				}else{
					$chkRs = $chk->result();
					$approveID = $chkRs[0]->approveID;
					$conf_del = "onclick='CancelApprove(".$approveID.")'";
					$e_btn = "<a data-toggle='modal' href='#modal_OutTime".$row->staffID."'><button type='button' class='btn btn-warning '><i class='fa fa-edit'></i></button></a>"; 
					$d_btn = "<a ".$conf_del."><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>";
					$btn_group = "<div class='btn-group text-center'> 
					                      ".$e_btn.$d_btn." 
					                      </div> ";
					$action = 'update';
					$strike_open = "<strike style='color:red;'>";
					$strike_close = "</strike>";
					
					$text_comment = $chkRs[0]->approveDetail;
					$comment = "<br><span style='color:red;'>( ".$text_comment." )</span>";
					$chk_radio = 'checked';
					if($timedaily != toBETime($chkRs[0]->approvedTime2)){
						$changeTime = ' '.toBETime($chkRs[0]->approvedTime2);
						$time2 = toBETime($chkRs[0]->approvedTime2);
					}else{
						$changeTime = '';
						$time2 = $timedaily;
					}
				}

				

				$tableOutTime .= " <tr>
										<td>".$strike_open.$row->staffID.$strike_close."</td> 
										<td>".$strike_open.$row->staffFName." ".$row->staffLName.$strike_close."</td>
										<td>".$strike_open.$row->orgName.$strike_close."</td>
										<td>".$strike_open.toBETime($row->dateDaily).$strike_close.$changeTime.$comment."     
					                      
					                    </td>
					                    <td align='center' style='padding-left:3px;padding-right:3px;text-align:center;'>".$btn_group."</td> 
									</tr>";
				$tableOutTime .= '<div class="modal fade" id="modal_OutTime'.$row->staffID.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
                      <div class="modal-dialog" style="padding-top: 10%;"> 
                        <div class="modal-content"> 
                        <form method="post" action="'.site_url('reportdaily/approve').'"> 
                          <div class="modal-header"> 
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
                            <h4 class="modal-title">แบบฟอร์มอนุมัติ</h4> 
                          </div> 
                          <div class="modal-body">
                              <div class="form-group"> 
                              	<label><b>หมายเหตุ/เหตุผล</b></label>
                              	<div class="form-group"> 
                              		<input type="radio" name="reson" value="ลืมบัตร" required> ลืมบัตร<br>
                              		<input type="radio" name="reson" value="แสกนไม่ติด" required> แสกนไม่ติด<br>
                              		<input type="radio" name="reson" value="ทำธุระ" required> ทำธุระ<br>
                              		<input type="radio" name="reson" value="" required '.$chk_radio.'> อื่นๆ
                              	</div>
                                  <textarea name="approveDetail" class="form-control" rows="5">'.$text_comment.'</textarea> 
                              </div> 
                              <div class="form-group"> 
                              	<label><b>เวลา</b></label>
                              	<div class="input-group">
             						<input name="edittime" type="time" readonly id="changeTimeLate'.$row->staffID.'" class="form-control" value="'.$time2.'"> 
             						<span class="input-group-btn">
            							<button type="button" onclick="changeTimeLate('.$row->staffID.')" style="line-height: 1.0;" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> แก้ไข</button>
        							</span>
             					</div>
                              
                              </div>
                             
                          </div> 
                          <div class="modal-footer" style="text-align: right"> 
                            <input name ="staffID" hidden value="'.$row->staffID.'"> 
                            <input name ="dateDaily" hidden value="'.$row->dateDaily.'"> 
                            <input name ="uri_segment" hidden value="'."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".'">
                            <input name ="periodType" hidden value="3"> 
                            <input name ="action" hidden value="'.$action.'"> 
                            <input name ="approveID" hidden value="'.$approveID.'"> 
                            <button type="submit" name="approve" class="btn btn-success center">Approve</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
                          </div> 
                        </form> 
                        </div> 
                      </div> 
                    </div>';   
			}
			$tableOutTime .= "</table>";
			$tableOutTime .= "</td></tr>";	
			$tableReportOutTime .= $tableOutTime;
			
			}
				
		}

			$tableReportOutTime .= "</table>";
			$data['tableReportOutTime'] = $tableReportOutTime;
		}  





//พนักงานที่ไม่สแกนนิ้ออก
		
		if(!empty($dataOrg)) 
		{
			$tableReportScanPm = "<table class='table table-striped'>
									<tr>
										<th class='thlight'>พนักงานที่ไม่ลงเวลาตอนเลิกงาน</th>
									</tr>
								";

			foreach ($dataOrg as $row) {

				$tableNotScanPm = " <tr>
										<td><b>".$row->orgName."</b></td> 
								</tr>";
			
				$ag = $row->orgID.$this->organization_model->getSubOrg($row->orgID);
				$tableNotScanPm .= "<tr><td>";				
				$dataScanPm = $this->reportdaily_model->getNotScanPm($strDate_s,$ag);
				
				
				if(!empty($dataScanPm)) {
					$tableNotScanPm .= "<table class='table table-striped'>
									<tr class='lighttr'>
										<th width='15%'>รหัสพนักงาน</th>
										<th width='25%'>ชื่อ</th>
										<th width='25%'>หน่วยงาน</th>
										<th width='20%'>หมายเหตุ</th>
										<th width='15%'>จัดการ</th>
									</tr>
								";
					foreach ($dataScanPm as $row) {

						$chk = $this->reportdaily_model->chkApprove($row->staffID,$strDate_s,3);
						if($chk->num_rows()==0){
							$a_btn = "<a data-toggle='modal' href='#modal_NotScanPM".$row->staffID."'><button type='button' class='btn btn-success'>A</button></a>"; 
					        $l_url = site_url('absence'); 
					        $l_btn = "<a href='$l_url'><button type='button' class='btn btn-info'>L</button></a>"; 
					        $btn_group = "<div class='btn-group text-center'> 
					                      ".$a_btn.$l_btn." 
					                      </div> ";
					        $strike_open = "";
							$strike_close = "";
							$comment = "";
							$action = 'insert';
							$chk_radio = '';
							$approveID = '';
							
							$text_comment ='';
						}else{
							$chkRs = $chk->result();
							$approveID = $chkRs[0]->approveID;
							$conf_del = "onclick='CancelApprove(".$approveID.")'";
							$e_btn = "<a data-toggle='modal' href='#modal_NotScanPM".$row->staffID."'><button type='button' class='btn btn-warning '><i class='fa fa-edit'></i></button></a>"; 
							$d_btn = "<a ".$conf_del."><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>";
							$btn_group = "<div class='btn-group text-center'> 
							                      ".$e_btn.$d_btn." 
							                      </div> ";
							$action = 'update';
							$strike_open = "<strike style='color:red;'>";
							$strike_close = "</strike>";
							
							$text_comment = $chkRs[0]->approveDetail;
							$comment = "<span style='color:red;'>( ".$text_comment." )</span>";
							$chk_radio = 'checked';
							
							
						}
						

						$tableNotScanPm .= " <tr>
											<td>".$strike_open.$row->staffID.$strike_close."</td> 
											<td>".$strike_open.$row->staffFName." ".$row->staffLName.$strike_close."</td>
											<td>".$strike_open.$row->orgName.$strike_close."</td>
											<td>".$strike_open.toBETime($row->dateDaily).$strike_close.$comment."     
						                     

						                    </td> 
						                    <td align='center' style='padding-left:3px;padding-right:3px;text-align:center;'>".$btn_group."</td> 
										</tr>";
						$tableNotScanPm .= '<div class="modal fade" id="modal_NotScanPM'.$row->staffID.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
	                      <div class="modal-dialog" style="padding-top: 10%;"> 
	                        <div class="modal-content"> 
	                        <form method="post" action="'.site_url('reportdaily/approve').'"> 
	                          <div class="modal-header"> 
	                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
	                           <h4 class="modal-title">แบบฟอร์มอนุมัติ</h4> 
                          </div> 
                          <div class="modal-body">
                              <div class="form-group"> 
                              	<label><b>หมายเหตุ/เหตุผล</b></label>
                              	<div class="form-group"> 
                              		<input type="radio" name="reson" value="ลืมบัตร" required> ลืมบัตร<br>
                              		<input type="radio" name="reson" value="แสกนไม่ติด" required> แสกนไม่ติด<br>
                              		<input type="radio" name="reson" value="ทำธุระ" required> ทำธุระ<br>
                              		<input type="radio" name="reson" value="" required '.$chk_radio.'> อื่นๆ
                              	</div>
	                                  <textarea name="approveDetail" class="form-control" rows="5" >'.$text_comment.'</textarea> 
	                              </div> 
	                          </div> 
	                          <div class="modal-footer" style="text-align: right"> 
	                            <input name ="staffID" hidden value="'.$row->staffID.'"> 
	                            <input name ="dateDaily" hidden value="'.$strDate_s.'"> 
	                            <input name ="uri_segment" hidden value="'."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".'">
	                            <input name ="periodType" hidden value="3"> 
	                            <input name ="action" hidden value="'.$action.'"> 
                            	<input name ="approveID" hidden value="'.$approveID.'"> 
	                            <button type="submit" name="approve" class="btn btn-success center">Approve</button> 
	                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
	                          </div> 
	                        </form> 
	                        </div> 
	                      </div> 
	                    </div>';   
					}
					$tableNotScanPm .= "</table>";
					$tableNotScanPm .= "</td></tr>";	
					$tableReportScanPm .= $tableNotScanPm;
				}
			}
			$tableReportScanPm .= "</table>";
			$data['tableReportScanPm'] = $tableReportScanPm; 
			
			
		} 






		//พนักงานที่ไม่สแกนนิ้ว
		
		if(!empty($dataOrg)) 
		{
			$tableReportScan = "<table class='table table-striped'>
									<tr>
										<th class='thlight'>พนักงานที่ไม่ลงเวลาช่วงเช้า</th>
									</tr>
								";

			foreach ($dataOrg as $row) {

				$tableNotScan = " <tr>
										<td><b>".$row->orgName."</b></td> 
								</tr>";
			
				$ag = $row->orgID.$this->organization_model->getSubOrg($row->orgID);
				$tableNotScan .= "<tr><td>";				
				$dataScan = $this->reportdaily_model->getScan($strDate_s,$ag);
				
				
				if(!empty($dataScan)) {
					$tableNotScan .= "<table class='table table-striped'>
									<tr class='lighttr'>
										<th width='15%'>รหัสพนักงาน</th>
										<th width='25%'>ชื่อ</th>
										<th width='25%'>หน่วยงาน</th>
										<th width='20%'>หมายเหตุ</th>
										<th width='15%'>จัดการ</th>
									</tr>
								";
					foreach ($dataScan as $row) {
						$chk = $this->reportdaily_model->chkApprove($row->staffID,$strDate_s,1);
						if($chk->num_rows()==0){
							$a_btn = "<a data-toggle='modal' href='#modal_NotScan".$row->staffID."'><button type='button' class='btn btn-success'>A</button></a>"; 
					        $l_url = site_url('absence'); 
					        $l_btn = "<a href='$l_url'><button type='button' class='btn btn-info'>L</button></a>"; 
					        $btn_group = "<div class='btn-group text-center'> 
					                      ".$a_btn.$l_btn." 
					                      </div> ";
					        $strike_open = "";
							$strike_close = "";
							$comment = "";
							$action = 'insert';
							$chk_radio = '';
							$approveID = '';
							$text_comment ='';
						}else{
						

							$chkRs = $chk->result();
							$approveID = $chkRs[0]->approveID;
							$conf_del = "onclick='CancelApprove(".$approveID.")'";
							$e_btn = "<a data-toggle='modal' href='#modal_NotScan".$row->staffID."'><button type='button' class='btn btn-warning '><i class='fa fa-edit'></i></button></a>"; 
							$d_btn = "<a ".$conf_del."><button type='button' class='btn btn-danger'><i class='fa fa-times'></i></button></a>";
							$btn_group = "<div class='btn-group text-center'> 
							                      ".$e_btn.$d_btn." 
							                      </div> ";
							$action = 'update';
							$strike_open = "<strike style='color:red;'>";
							$strike_close = "</strike>";
							
							$text_comment = $chkRs[0]->approveDetail;
							$comment = "<span style='color:red;'>( ".$text_comment." )</span>";
							$chk_radio = 'checked';
						}
						

						$tableNotScan .= " <tr>
											<td>".$strike_open.$row->staffID.$strike_close."</td> 
											<td>".$strike_open.$row->staffFName." ".$row->staffLName.$strike_close."</td>
											<td>".$strike_open.$row->orgName.$strike_close."</td>
											<td>".$strike_open.toBETime($row->dateDaily).$strike_close.$comment."     
						                   
						                    </td> 
						                    <td align='center' style='padding-left:3px;padding-right:3px;text-align:center;'>".$btn_group."</td> 
										</tr>";
						$tableNotScan .= '<div class="modal fade" id="modal_NotScan'.$row->staffID.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
	                      <div class="modal-dialog" style="padding-top: 10%;"> 
	                        <div class="modal-content"> 
	                        <form method="post" action="'.site_url('reportdaily/approve').'"> 
	                          <div class="modal-header"> 
	                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
	                           <h4 class="modal-title">แบบฟอร์มอนุมัติ</h4> 
                          </div> 
                          <div class="modal-body">
                              <div class="form-group"> 
                              	<label><b>หมายเหตุ/เหตุผล</b></label>
                              	<div class="form-group"> 
                              		<input type="radio" name="reson" value="ลืมบัตร" required> ลืมบัตร<br>
                              		<input type="radio" name="reson" value="แสกนไม่ติด" required> แสกนไม่ติด<br>
                              		<input type="radio" name="reson" value="ทำธุระ" required> ทำธุระ<br>
                              		<input type="radio" name="reson" value="" required '.$chk_radio.'> อื่นๆ
                              	</div>
	                                  <textarea name="approveDetail" class="form-control" rows="5">'.$text_comment.'</textarea> 
	                              </div> 
	                          </div> 
	                          <div class="modal-footer" style="text-align: right"> 
	                            <input name ="staffID" hidden value="'.$row->staffID.'"> 
	                            <input name ="dateDaily" hidden value="'.$strDate_s.'"> 
	                            <input name ="uri_segment" hidden value="'."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".'">
	                            <input name ="periodType" hidden value="1"> 
	                            <input name ="action" hidden value="'.$action.'"> 
                            	<input name ="approveID" hidden value="'.$approveID.'"> 
	                            <button type="submit" name="approve" class="btn btn-success center">Approve</button> 
	                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
	                          </div> 
	                        </form> 
	                        </div> 
	                      </div> 
	                    </div>';   
					}
					$tableNotScan .= "</table>";
					$tableNotScan .= "</td></tr>";	
					$tableReportScan .= $tableNotScan;
				}
			}
			$tableReportScan .= "</table>";
			$data['tableReportScan'] = $tableReportScan; 
			
			
		} 
		$data['assignID'] = $this->reportdaily_model->getLastedDateOrgAssign();
		$data['org_dropdown'] = $this->getDropdownOptionOrg($selected);
		$data['dropdown_selected'] = $selected;
        $this->template->load("template/admin",'reportdaily', $data);
	}


	function getDropdownOptionOrg($selected=''){
		$org_data = $this->reportdaily_model->OrgUper(0);
		foreach ($org_data->result() as $row) {
			if($selected==$row->orgID){
				$sel = 'selected';
			}else{
				$sel = '';
			}
			$option .= '<option value="'.$row->orgID.'"'.$sel.'>-- '.$row->orgName.'</option>';

			$org_data2 = $this->reportdaily_model->OrgUper($row->orgID);
			foreach ($org_data2->result() as $row) {
				if($selected==$row->orgID){
					$sel = 'selected';
				}else{
					$sel = '';
				}
				$option .= '<option value="'.$row->orgID.'"'.$sel.'>---- '.$row->orgName.'</option>';
			}

		}
		return $option;
	}

	public function Approve(){ 
	    $staffID = $this->input->post('staffID'); 
	    $dateDaily = $this->input->post('dateDaily'); 
	    $periodType = $this->input->post('periodType'); 
	    $approveDetail =$this->input->post('approveDetail');
	    $redirect = $this->input->post('uri_segment');
	    if(!empty($redirect)){
	    	$errredirect = "window.location='".$redirect."'";
	    }else{
	    	$errredirect = "history.back()";
	    }
	    $reson = $this->input->post('reson');
	    $edittime = $this->input->post('edittime');
	    $action = $this->input->post('action');
	    $approveID = $this->input->post('approveID');

	    if($edittime != '' && !preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $edittime)){
	    	echo "<script>alert('ฟอร์แมตเวลาไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง');history.back();</script>";
	    	exit();
	    }else{
	    	/*$edittime = explode(':', $edittime);
		    $editdatetime = new DateTime($dateDaily);
			$editdatetime->setTime($edittime[0],$edittime[1]);
			$dateDaily = $editdatetime->format('Y-m-d H:i:s');*/


	    	 
	    }
	   
	 
	    $reson = $reson != null ? $reson.' ' : ''; 

	    if($staffID && $dateDaily && $periodType){ 

	    	if($action=='insert' or empty($action)){
		      $data1 = array( 
		        'staffID'     	=> $staffID, 
		        'dateDaily'    	=> $dateDaily, 
		        'approveBy'		=> $this->session->userdata('userID'),
		        'approveDetail'	=> $reson.$approveDetail,
		        'periodType'	=> $periodType,
		        'createDate'	=> date('Y-m-d H:i:s'),
		        'approvedTime'	=> toBETime($dateDaily),
		        'approvedTime2'	=> $edittime ==null ? toBETime($dateDaily) : $edittime
		 
		      );
		      $result = $this->reportdaily_model->Approve($data1);

			   	if($result){
		      		echo "<script>alert('อนุมัติเรียบร้อยแล้ว');window.location='$redirect';</script>";
		    	}else{
		      		echo "<script>alert('คำสั่งผิดพลาด กรุณาทำรายการใหม่อีกครั้ง');$errredirect;</script>"; 
		      	}
		    }else if($action=='update' && !empty($approveID)){

		      $data2 = array( 
		        //'staffID'     	=> $staffID, 
		        //'dateDaily'    	=> $dateDaily, 
		        'updateBy'		=> $this->session->userdata('userID'),
		        'approveDetail'	=> $reson.$approveDetail,
		        //'periodType'	=> $periodType,
		        'updateDate'	=> date('Y-m-d H:i:s'),
		        //'approvedTime'	=> toBETime($dateDaily),
		        'approvedTime2'	=> $edittime ==null ? toBETime($dateDaily) : $edittime
		 
		      );

		      $result = $this->reportdaily_model->ApproveUpdate($data2,$approveID);
		      	if($result){
		      		echo "<script>alert('อัพเดทข้อมูลเรียบร้อยแล้ว');window.location='$redirect';</script>";
		    	}else{
		      		echo "<script>alert('คำสั่งผิดพลาด กรุณาทำรายการใหม่อีกครั้ง');$errredirect;</script>"; 
		      	}
		    
		    }else{
		    	echo "<script>alert('คำสั่งผิดพลาด กรุณาทำรายการใหม่อีกครั้ง');$errredirect;</script>"; 
		    }
	      

	      
	      
	  
	    }else if(!empty($this->input->get('cancel'))){
	    	$result = $this->reportdaily_model->ApproveCancel($this->input->get('cancel'));
	    	$redirect = $this->input->get('redirect');

		    $directto = $redirect != null ? "window.location='".$redirect."'" : "history.back()"; 
		    if($result){
		      		echo "<script>alert('ยกเลิกรายการเรียบร้อยแล้ว');$directto</script>";
		   	}else{
		      		echo "<script>alert('คำสั่งผิดพลาด กรุณาทำรายการใหม่อีกครั้ง');$errredirect;</script>"; 
		    }
	    }else{
	      	echo "<script>alert('คำสั่งผิดพลาด กรุณาทำรายการใหม่อีกครั้ง');$errredirect;</script>"; 
	    }
	    
  	} 
}
