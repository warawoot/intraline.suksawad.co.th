<?php
class Reportdaily_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = '';
            $this->_pk = '';
        }

        public function getLeave($datestr, $ag){
            if(!empty($datestr)){            
                 $sql = "SELECT 
                        s.staffID,
                        s.staffPreName ,
                        s.staffFName ,
                        s.staffLName ,
                        sa.absStartDate,
                        sa.absEndDate,
                        sa.status_absence,
                        an.absName,
                        os.orgName
                    FROM `tbl_staff` s  
                    INNER JOIN `tbl_org_chart` os ON s.orgID = os.orgID
                    INNER JOIN `tbl_staff_absence` sa  ON sa.staffID = s.ID 
                    INNER JOIN `tbl_absence_type` an ON sa.absTypeID = an.absTypeID 
                    WHERE DATE(sa.absStartDate) <= '$datestr' AND DATE(sa.absEndDate) >= '$datestr' AND sa.statusApprove != 3";
                    if(!empty($ag)){
                       $sql .= " AND s.orgID IN ($ag)"; 
                        }
                       $sql .= "ORDER BY an.absName,sa.status_absence ASC"; //,s.staffID ASC 
                    $r = $this->db->query($sql);
                    return ($r->num_rows() > 0) ? $r->result() : NULL;
            }
            
        }


        public function getLate($datestr ,$ag){
            if(!empty($datestr)){            
                 $sql = "SELECT 
                         A.staffID ,
                         A.staffFName , 
                         A.staffLName ,
                         D.orgName ,
                         B.dateDaily
                        FROM    
                        (SELECT ID, staffID, staffFName , staffLName ,orgID
                        FROM tbl_staff 
                        WHERE status <> 2 AND tbl_staff.orgID IN ($ag)) A 
                        LEFT JOIN 
                        (SELECT DISTINCT staffID , dateDaily
                        FROM tbl_staff_work_daily 
                        WHERE  DATE(dateDaily) = '$datestr' AND dateDaily BETWEEN  '$datestr 08:00:01' AND  '$datestr 10:10:00') B 
                        ON A.staffID = B.staffID
                        LEFT JOIN 
                        (SELECT DISTINCT staffID , absStartDate , absEndDate
                        FROM tbl_staff_absence WHERE DATE(absStartDate) <= '$datestr' AND DATE(absEndDate) >= '$datestr' AND statusApprove !=3) C
                        ON A.ID = C.staffID
                        LEFT JOIN 
                        (SELECT DISTINCT orgID, orgName
                        FROM tbl_org_chart) D
                        ON A.orgID = D.orgID
                        WHERE B.dateDaily is NOT NULL AND C.absStartDate IS NULL  AND C.absEndDate IS NULL
                        ";
                $sql .= "ORDER BY A.orgID, B.dateDaily, A.staffID ASC";
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r->result() : NULL;
            }
            
        }

        public function getLateFromAbsenceHour($datestr ,$ag){
            $sql = "SELECT 
                        A.staffID ,
                        A.staffFName , 
                        A.staffLName ,
                        B.absEndDate, 
                        B.absStartDate,
                        C.dateDaily,
                        D.orgName
        
                        FROM    
                            (SELECT ID, staffID, staffFName , staffLName ,orgID
                        FROM tbl_staff 
                        WHERE status <> 2 AND tbl_staff.orgID IN ($ag)) A 
                        right JOIN 
                            (SELECT staffID, absStartDate, absEndDate
                        FROM tbl_staff_absence
                        WHERE  DATE(absEndDate) = '$datestr' and statusApprove='1' and status_absence='2' ) B
                        ON A.ID = B.staffID
                        LEFT JOIN 
                            (SELECT DISTINCT staffID , dateDaily
                        FROM tbl_staff_work_daily 
                        WHERE  DATE(dateDaily) = '$datestr' GROUP BY staffID ORDER BY dateDaily asc) C
                        ON A.staffID = C.staffID
                        LEFT JOIN
                            (SELECT orgID, orgName
                        FROM tbl_org_chart 
                        WHERE  1) D 
                        ON D.orgID=A.orgID
                                                
                        WHERE C.dateDaily is NOT NULL and B.absEndDate < C.dateDaily
                        ORDER BY A.orgID, C.dateDaily ASC
                    ";
            $query = $this->db->query($sql);
            //echo $this->db->last_query();
            return $query;
        }


         public function getOutTime($datestr ,$ag){
            if(!empty($datestr)){            
                 $sql = "SELECT 
                         A.staffID ,
                         A.staffFName , 
                         A.staffLName ,
                         D.orgName ,
                         B.dateDaily
                        FROM    
                        (SELECT ID, staffID, staffFName , staffLName ,orgID
                        FROM tbl_staff 
                        WHERE status <> 2 AND tbl_staff.orgID IN ($ag)) A 
                        LEFT JOIN 
                        (SELECT DISTINCT staffID , dateDaily
                        FROM tbl_staff_work_daily 
                        WHERE  DATE(dateDaily) = '$datestr' AND dateDaily BETWEEN  '$datestr 15:00:00' AND  '$datestr 16:59:59') B 
                        ON A.staffID = B.staffID
                        LEFT JOIN 
                        (SELECT DISTINCT staffID , absStartDate , absEndDate
                        FROM tbl_staff_absence WHERE DATE(absStartDate) <= '$datestr' AND DATE(absEndDate) >= '$datestr' AND statusApprove !=3) C
                        ON A.ID = C.staffID
                        LEFT JOIN 
                        (SELECT DISTINCT orgID, orgName
                        FROM tbl_org_chart) D
                        ON A.orgID = D.orgID
                        WHERE B.dateDaily is NOT NULL AND C.absStartDate IS NULL  AND C.absEndDate IS NULL
                        ";
                $sql .= "ORDER BY B.dateDaily, A.staffID ASC";     
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r->result() : NULL;
            }
            
        }

        public function getOrg($orgID=''){            
                $sql = "SELECT oc.orgID, oc.orgName
                         FROM tbl_org_chart oc";
                $sql.= " WHERE upperOrgID='$orgID' or orgID = '$orgID' and upperOrgID <> 0 ";

                       
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r->result() : NULL;     
        }

         public function getOrgReg($orgID=''){            
                $sql = "SELECT oc.orgID, oc.orgName
                         FROM tbl_org_chart oc";
                $sql.= " WHERE upperOrgID='$orgID' and upperOrgID <> 0 ";

                       
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r->result() : NULL;     
        }

         

        public function getScan($datestr ,$ag){  
             if(!empty($datestr)){            
                 $sql = "SELECT 
                        A.staffID ,
                        A.staffFName , 
                        A.staffLName ,
                        D.orgName 
                        FROM 
                        (SELECT ID,staffID , staffFName , staffLName  , orgID
                        FROM tbl_staff 
                        WHERE status <> 2 AND tbl_staff.orgID IN ($ag)) A
                        LEFT JOIN 
                        (SELECT DISTINCT staffID , dateDaily
                        FROM tbl_staff_work_daily 
                        WHERE  dateDaily BETWEEN  '$datestr 00:00:00'  AND  '$datestr 10:00:00') B 
                        ON A.staffID = B.staffID 
                        LEFT JOIN 
                        (SELECT DISTINCT orgID, orgName
                        FROM tbl_org_chart) D
                        ON A.orgID = D.orgID
                        LEFT JOIN 
                        (SELECT DISTINCT staffID , absStartDate , absEndDate
                        FROM tbl_staff_absence WHERE DATE(absStartDate) <= '$datestr' AND DATE(absEndDate) >= '$datestr') E
                        ON A.ID = E.staffID
                        WHERE B.staffID IS NULL AND E.staffID IS NULL 
                        ";
                        $sql .= "ORDER BY A.orgID, A.staffID ASC";
                        $r = $this->db->query($sql);
                        return ($r->num_rows() > 0) ? $r->result() : NULL; 

                }    
        }





         public function getNotScanPm($datestr ,$ag){  
         if(!empty($datestr)){            
                 $sql = "SELECT 
                        A.staffID ,
                        A.staffFName , 
                        A.staffLName ,
                        D.orgName 
                        FROM 
                        (SELECT staffID , staffFName , staffLName  , orgID
                        FROM tbl_staff 
                        WHERE status <> 2 AND tbl_staff.orgID IN ($ag)) A
                        LEFT JOIN 
                        (SELECT DISTINCT staffID , dateDaily
                        FROM tbl_staff_work_daily 
                        WHERE  dateDaily BETWEEN  '$datestr 00:00:00'  AND  '$datestr 10:00:00') B 
                        ON A.staffID = B.staffID 
                        LEFT JOIN 
                        (SELECT DISTINCT staffID , dateDaily
                        FROM tbl_staff_work_daily 
                        WHERE  dateDaily BETWEEN  '$datestr 14:00:00' AND  '$datestr 23:59:00') C
                        ON C.staffID = B.staffID 
                        LEFT JOIN 
                        (SELECT DISTINCT orgID, orgName
                        FROM tbl_org_chart) D
                        ON A.orgID = D.orgID
                        WHERE B.staffID is NOT null AND C.staffID is null
                        ";
                        $sql .= "ORDER BY A.orgID, A.staffID ASC";
                        $r = $this->db->query($sql);
                        return ($r->num_rows() > 0) ? $r->result() : NULL; 

                }    
        }





        public function getLastedDateOrgAssign($date=''){
            $sql = "select assignID from tbl_orgchart_assigndate ";
            if(!empty($date)){
                $sql.=" where assignDate <= '$date'";
            }
            $sql.=" order by assignDate DESC LIMIT 1";
            $r = $this->db->query($sql);
            return ($r->num_rows() > 0) ? $r->row()->assignID : "1";
        }

        public function OrgUper($upperOrgID=''){
            $this->db->select('orgID,orgName');
            $this->db->where('upperOrgID',$upperOrgID);
            $query = $this->db->get('tbl_org_chart');
            return $query;

        }


        public function Approve($data=array('')){
            $query = $this->db->insert('tbl_staff_work_daily_approve',$data);
            if($query){
                return $this->db->insert_id(); 
            }else{
                return NULL;
            }
            
        }

        public function ApproveUpdate($data,$approveID){
            $this->db->where('approveID',$approveID);
            return $query = $this->db->update('tbl_staff_work_daily_approve',$data);
        }

        public function ApproveCancel($approveID){
            $this->db->where('approveID',$approveID);
            return $query = $this->db->delete('tbl_staff_work_daily_approve');
        }


        public function chkApprove($staffID, $dateDaily, $periodType){
            $dateDaily = substr($dateDaily, 0, 10);
            $this->db->where('staffID',$staffID);
            $this->db->where('dateDaily', $dateDaily);
            $this->db->where('periodType',$periodType);
            $query = $this->db->get('tbl_staff_work_daily_approve');
            if($query){
                return $query;
            }else{
                return NULL;
            }
            

        }

        

}

/* End of file staff_model.php */
/* Location: ./application/module/Reportstatusemployees/models/staff_model.php */
?>