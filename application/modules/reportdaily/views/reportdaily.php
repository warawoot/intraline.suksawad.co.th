<style type="text/css">
    .table.table-striped > tbody > tr > td {
     vertical-align: top !important;
        }
</style>
<div class="xcrud-container">
    <div class="xcrud-ajax">
        <div class="xcrud-view">
            <form id="formMain" action="<?=base_url('reportdaily') ?>" method="get"  enctype="multipart/form-data" class="form-horizontal">                			 
                <div class="form-group">                   
					<div class="col-sm-2" align="right">
					 	<h5>รายงานประจำวันที่</h5>
					</div>
                    <div class="col-sm-4">
				
						<input 
							data-type="select"
                            data-required="1" 
                            type="text" 
                            style="width:250px;background-color:#FFF" 
                            data-type="date" 
                            name="datestr" 
                            id="datestr"
                            readonly=true
							class="form-control input-small" value="<?=$datestr?>">			 
                    </div>
                    <div class="col-sm-2">
                        <h5>หน่วยงาน</h5>
                    </div>
                    <div class="col-sm-4">
                        
                                <select class="xcrud-input form-control" data-type="select" name="orgID" id="orgID"> 
                                  <option value="">----กรุณาเลือก-----</option>
                                <?php 
                                   //echo $org_dropdown;
                                    $orgIDManage = $this->session->userdata('orgIDManage'); 
                                    echo getDropdownTree($orgIDManage,'',$assignID,$dropdown_selected); 
                                ?>
                                </select>

                    </div>
					<div class="col-sm-12">
							<button class="btn btn-primary xcrud-action" 
								type="submit" >ค้นหา
							</button> 
                    </div>
				</div>
        	</form>
    	</div>
	</div>
</div>

<?=$tableReportOrg?><br>
<?=$tableReportLate?><br>
<?=$tableReportLateHour?><br>
<?=$tableReportOutTime?><br>
<?=$tableReportScan?><br>
<?=$tableReportScanPm?><br>



<script>
     $.datepicker.regional['th'] ={
        changeMonth: true,
        changeYear: true,
        //defaultDate: GetFxupdateDate(FxRateDateAndUpdate.d[0].Day),
        yearOffSet: 543,
        dateFormat: 'dd/mm/yy',
        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
        constrainInput: true,
       
        prevText: 'ก่อนหน้า',
        nextText: 'ถัดไป',
        yearRange: '-1:+0',
        buttonText: 'เลือก',
      
    };
    
    $.datepicker.setDefaults($.datepicker.regional['th']);

    $(function() {
        $( "#datestr" ).datepicker( $.datepicker.regional["th"] ); 
    });
    
    var Holidays;
 
    //On Selected Date
    //Have Check Date
    function CheckDate(date) {
        var day = date.getDate();
        var selectable = true;//ระบุว่าสามารถเลือกวันที่ได้หรือไม่ True = ได้ False = ไม่ได้
        var CssClass = '';
        
        if (Holidays != null) {

            for (var i = 0; i < Holidays.length; i++) {
                var value = Holidays[i];
                if (value == day) {

                    selectable = false;
                    CssClass = 'specialDate';
                    break;
                }
            }
        }
        return [selectable, CssClass, ''];
    }

    //=====================================================================================================
    //On Selected Date
    function SelectedDate(dateText, inst) {
        //inst.selectedMonth = Index of mounth
        //(inst.selectedMonth+1)  = Current Mounth
        var DateText = inst.selectedDay + '/' + (inst.selectedMonth + 1) + '/' + inst.selectedYear;
        //CallGetUpdateInMonth(ReFxupdateDate(dateText));
        //CallGetUpdateInMonth(DateText);
        return [dateText, inst]
    }
    //=====================================================================================================
    //Call Date in month on click image
    function OnBeforShow(input, inst) {
        var month = inst.currentMonth + 1;
        var year = inst.currentYear;
        //currentDay: 10
        //currentMonth: 6
        //currentYear: 2012
        GetDaysShows(month, year); 
       
    }
    //=====================================================================================================
    //On Selected Date
    //On Change Drop Down
    function ChangMonthAndYear(year, month, inst) {

        GetDaysShows(month, year);
    }

    //=====================================
    function GetDaysShows(month, year) {
        //CallGetDayInMonth(month, year); <<เป็น Function ที่ผมใช้เรียก ajax เพื่อหาวันใน DataBase  แต่นี้เป็นเพียงตัวอย่างจึงใช้ Array ด้านล่างแทนการ Return Json
        //อาจใช้ Ajax Call Data โดยเลือกจากเดือนและปี แล้วจะได้วันที่ต้องการ Set ค่าวันไว้คล้ายด้านล่าง
        Holidays = [1,4,6,11]; // Sample Data
    }
    //=====================================
// changeTimeLate
function changeTimeLate(staffID){
    var staffID = staffID;
    $("#changeTimeLate" + staffID).prop("readonly",false);
}


// CancelApprove
function CancelApprove(approveID){
    var urldel = "<?php echo site_url('reportdaily/approve?cancel=');?>" + approveID + "&redirect=" + document.URL;
    if(confirm('ต้องการยกเลิกอนุมัติรายการนี้?')==true){
       window.location= urldel;
    }else{
        return false;
    }
}




 
</script>  