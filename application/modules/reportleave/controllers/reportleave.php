<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReportLeave extends MX_Controller {
	
    function __construct() {
        parent::__construct();

        $this->load->model('reportLeavemodel');
     	$this->load->model('organization/organization_model');               
    }

     function index() { 

        $agencies = $this->input->get_post('agencies');
        $startDate = $this->input->get_post('startDate');
        $endDate = $this->input->get_post('endDate');

        if($startDate == "") {
            $startDate = date("d/m/").(date("Y")+543);
        } 
        
        if($endDate == "") {
            $endDate = date("d/m/").(date("Y")+543);
        }

		$date_start 		= explode('/',$startDate); //สร้างตัวแปรมาเก็บวันที่
		$strYear_s 			= $date_start[2]-543;
		$strMonth_s			= $date_start[1];
		$strDay_s			= $date_start[0];
        $strDate_s 		    = $strYear_s."-".$strMonth_s."-".$strDay_s;        

		$date_end 	  		= explode('/',$endDate); //สร้างตัวแปรมาเก็บวันที่
		$strYear_e 			= $date_end[2]-543;
		$strMonth_e			= $date_end[1];
		$strDay_e			= $date_end[0];
        $strDate_e 		    = $strYear_e."-".$strMonth_e."-".$strDay_e;
        
        if($agencies != "")
        {
            $ag = $agencies.$this->organization_model->getSubOrg($agencies);
            $data['dataList'] = $this->reportLeavemodel->getLeave($ag, $strDate_s,$strDate_e);
            //echo $this->db->last_query();
        } else {
            $ag = "";
        }

        /*
        $orgList2 = $agencies.$this->organization_model->getSubOrg(200);
        $data['dataList2'] = $this->reportLeavemodel->getLeave($orgList2, $startDate,$endDate);

        $orgList3 = $agencies.$this->organization_model->getSubOrg(400);
        $data['dataList3'] = $this->reportLeavemodel->getLeave($orgList3, $startDate,$endDate);

        $orgList4 = $agencies.$this->organization_model->getSubOrg(500);
        $data['dataList4'] = $this->reportLeavemodel->getLeave($orgList4, $startDate,$endDate);

        $orgList5 = $agencies.$this->organization_model->getSubOrg(600);
        $data['dataList5'] = $this->reportLeavemodel->getLeave($orgList5, $startDate,$endDate);

        $orgList6 = $agencies.$this->organization_model->getSubOrg(104);
        $data['dataList6'] = $this->reportLeavemodel->getLeave($orgList6, $startDate,$endDate);
        */
        
        echo "<!--".$this->db->last_query()."-->";
       
        $data['startDate'] = $startDate;
        $data['endDate'] = $endDate;

        $data['dropdown_org_chart'] = ""; 

        if($this->session->userdata('roleID') == 3 || $this->session->userdata('roleID') == 4) {
		    $data['dropdown_org_chart'] = $this->organization_model->getDropdownTree(0 , $prefix = '' , $type='sub' ,$agencies);#Dropdown                  
        } else {
            $rows = $this->organization_model->getOrgByManager($this->session->userdata('userID'));
            foreach($rows as $row) {
		        $data['dropdown_org_chart'] .= $this->organization_model->getDropdownTree($row->orgID , $prefix = '' , $type='' ,$agencies);#Dropdown                                  
            }
        }
        $this->template->load('template/admin', 'reportleave',$data);
    }  

    function dateCountHoliday($startDate,$endDate) {  
        $count = $this->reportLeavemodel->CountHoliday($startDate,$endDate);              
       
        
        while(strtotime($startDate)<=strtotime($endDate)){
            $DayofWeek = date("w",strtotime($startDate));
            if($DayofWeek == 0){
                $intholiday++;  
            }else{
                $intworkday++;
            }
            $startDate = date ("Y-m-d", strtotime("+1 day", strtotime($startDate)));
        }
		$intworkday -= $count;
       
		return $intworkday;   
}

    
    function get_update() {  
        $result =  $this->reportLeavemodel->get_UpdateLeave();        
		foreach($result as $row){
            echo $row->stfAbsID."-";								  
            echo $dateCountHoliday = $this->dateCountHoliday($row->absStartDate,$row->absEndDate)."<br>";	
    		$updateData = array(  
							'absenceCount'=> $dateCountHoliday
					);

            $this->reportLeavemodel->update_leave($row->stfAbsID,$updateData);            
        }			                 
    }

     function get_update_absenceCount_minute() {  
        $result =  $this->reportLeavemodel->get_UpdateLeave_time();        
		foreach($result as $row){           
            echo $row->stfAbsID."-";	
            $date = date_create($row->absenceCount_minute); 
            $Hour = $date->format('H');
            $minute = $date->format('i');
            $second = $date->format('s');	
            $Hours = $Hour * 60 ;
            echo $absenceCount_minute =  $Hours + $minute + $second ."<br>";;           						  
            	
    		$updateData = array(  
							'absenceCount_minute'=> $absenceCount_minute
					);

            $this->reportLeavemodel->update_leave($row->stfAbsID,$updateData);            
        }			                 
    } 


}
?>