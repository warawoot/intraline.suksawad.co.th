<style>
.table th {
text-align: center;				
}
.tdC{
	text-align:center;
}	
.tdR{
	text-align:right;			
}
#th{
	padding-bottom: 25px;
}
#colorG1{
	background:#f2f2f2;
}
#colorG2{
	background:#e6e6e6;
}
.textTD{
	font-weight:bold;
}
.form-control {
	color: #343232;
}
.pf-input{
        float:left;
}

b {
	font-size : 12pt;
}    
.pf-popup{
	margin-top:45px;
}


.ui-datepicker-trigger {
	float:left;  
	margin-left:5px;
	margin-top:10px;
}

.hasDatepicker {
	float:left;
}

.orgName {
	font-size:8pt;
	color:#999;
}	
</style>  
<?php
	if($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==4) {
		$readonly = "";
?>

<section class="panel">      
	<div class="panel-body">
	<div class="xcrud-container">
    <div class="xcrud-ajax">
        <div class="xcrud-view">
            <form id="formMain" action="<?=base_url('reportleave') ?>" method="get"  enctype="multipart/form-data" class="form-horizontal">                
			<div class="form-group" >
				<div class="row">
					<div class="col-md-3 text-right" style="margin-bottom:30px"><b>หน่วยงาน</b></div>
					<div class="col-md-9">
						<select class="form-control form-control" data-type="select" id="agencies" name="agencies" style="width:350px"> 
							<option value=''>กรุณาเลือก</option>
							<?=$dropdown_org_chart?>									
						</select>
					</div>
				</div>            

				<div class="row">
					<div class="col-md-3 text-right"><b>วันที่เริ่มต้น</b></div>
					<div class="col-md-9">				
						<input 
							data-type="select"
                            data-required="1" 
                            type="text" 
                            style="width:250px;background-color:#FFF" 
                            data-type="date" 
                            name="startDate" 
                            id="dp1433735797502"
                            readonly=true
							class="form-control input-small" value="<?=$startDate?>"></div>
				</div>	
					          
				<div class="row">
					<div class="col-md-3 text-right"><b>วันที่สิ้นสุด</b></div>
					<div class="col-md-9">				
						<input  
							data-type="select"
							data-required="1" 
                            style="width:250px;background-color:#FFF" 
                            type="text" 
                            data-type="date"  
                            name="endDate" 
                            id="dp1433735797503" 
                            readonly=true
                            class="form-control input-small" value="<?=$endDate?>"></div>
				</div>

				<div class="row" style="padding:10px">
					<div class="col-md-3">&nbsp;</div>
					<div class="col-md-9">										 
						<button class="btn btn-primary xcrud-action" 
							type="submit" >ค้นหา
						</button> 					 
                    </div>
				</div>
        </form>
    </div>
</div>
<div class="xcrud-overlay" style="display: none;"></div>
</div>
	</div>
</section>
<?php
	} else {
		$readonly = "readonly";		
	} 
?>




 <!--table class="table table-striped">
	<thead>
	<tr>
		<th>รหัสพนักงาน</th>
		<th>ชื่อ-สกุล</th>	
		<th>มาสาย / กลับก่อน</th>
		<th>ลาป่วย</th>
		<th>ลากิจ</th>
		<th>ลาพักร้อน</th>
		<th>ลาหยุดพิเศษ</th>
		<th>อื่น ๆ</th>
		<th>รวม</th>				
	</tr>	
	
	</thead>


		
		<?php		
		/*
		if(!empty($dataList1)){
			foreach($dataList1 as $row){		
				
				$Sicks = $row->Sick;
				if($Sicks == null|| 0){
					$Sick = " ";
				}				
				else{
					$Sick = $Sicks.' วัน ';
				}

				$Sick_absenceCount_minutes = $row->Sick_absenceCount_minute;
				if($Sick_absenceCount_minutes == null|| 0){
					$Sick_absenceCount_minute = " ";
				}				
				else{
					$Sick_absenceCount_minute = $Sick_absenceCount_minutes.' นาที ';
				}				

				$Errands = $row->Errand;
				if($Errands == null || 0){
					$Errand = " ";
				}				
				else{
					$Errand = $Errands.' วัน ';
				}

				$Errand_absenceCount_minutes = $row->Errand_absenceCount_minute;
				if($Errand_absenceCount_minutes == null || 0){
					$Errand_absenceCount_minute = " ";
				}				
				else{
					$Errand_absenceCount_minute = $Errand_absenceCount_minutes.' นาที ';
				}

				
				$Holidays = $row->Holiday;
				if($Holidays == null || 0){
					$Holiday = " ";
				}				
				else{
					$Holiday = $Holidays.' วัน ';
				}		

				$Holiday_absenceCount_minutes = $row->Holiday_absenceCount_minute;
				if($Holiday_absenceCount_minutes == null|| 0){
					$Holiday_absenceCount_minute = " ";
				}				
				else{
					$Holiday_absenceCount_minute = $Holiday_absenceCount_minutes.' นาที ';
				}		

				$Specials = $row->Special;
				if($Specials == null || 0){
					$Special = " ";
				}				
				else{
					$Special = $Specials.' วัน ';
				}

				$Special_absenceCount_minutes = $row->Special_absenceCount_minute;
				if($Special_absenceCount_minutes == null || 0){
					$Special_absenceCount_minute = " ";
				}				
				else{
					$Special_absenceCount_minute = $Special_absenceCount_minutes.' นาที ';
				}

				$Others = $row->Other;
				if($Others == null || 0){
					$Other = " ";
				}				
				else{
					$Other = $Others.' วัน ';
				}

				$Others_absenceCount_minutes = $row->Others_absenceCount_minute;
				if($Others_absenceCount_minutes == null || 0){
					$Others_absenceCount_minute = " ";
				}				
				else{
					$Others_absenceCount_minute = $Others_absenceCount_minutes.' นาที ';
				}

				
				$Summarys = $row->Summary;
				if($Summarys == null || 0){
					$Summary = " ";
				}				
				else{
					$Summary = $Summarys.' วัน ';
				}

				$Summary_absenceCount_minutes = $row->Summary_absenceCount_minute;
				if($Summary_absenceCount_minutes == null || 0){
					$Summary_absenceCount_minute = " ";
				}				
				else{
					$Summary_absenceCount_minute = $Summary_absenceCount_minutes.' นาที ';
				}

				$Summary_lates = $row->Summary_late;
				if($Summary_lates == null || 0){
					$Summary_late = " ";
				}
				else{
					$Summary_late = $Summary_lates.' นาที ';	
				}

				$filename = 'assets/staff/'.$row->staffID.'.jpg';

			?><tbody>
				<tr>
					<td><center><?=$row->staffID ;?></center></td>
					<td>
						<?php 
						if (file_exists($filename))
						{
							$imgEmp = $filename;
						} else {
							$imgEmp = base_url().'./assets/staff/default-user.png';
						}
						?>
						<img src="<?=$imgEmp?>" class="imgRround" style="width:50px;height:50px;margin-right:10px" align="left"><?=$row->staffFName?> <?=$row->staffLName?><br>
						<span class='orgName'><?= $row->orgName?></span>														
					</td>
					<td class="tdC" id="colorG2"><?=$Summary_late?></td>
					<td class="tdC" id="colorG1"><?=$Sick.'<br>'.$Sick_absenceCount_minute;?></td>															
          			<td class="tdC" id="colorG2"><?=$Errand.'<br>'.$Errand_absenceCount_minute?></td>
					<td class="tdC" id="colorG1"><?=$Holiday.'<br>'.$Holiday_absenceCount_minute?></td>					
					<td class="tdC" id="colorG2"><?=$Special.'<br>'.$Special_absenceCount_minute?></td>
					<td class="tdC" id="colorG1"><?=$Other.'<br>'.$Other_absenceCount_minute?></td>					
					<td class="tdC" id="colorG2"> <?=$Summary.'<br>'.$Summary_absenceCount_minute;?></td>                    
					
		<?php  }
		}else{ ?>
			<tr><td colspan="14" style="text-align:center">ไม่พบข้อมูล</td></tr><tbody>
		<?php } */ ?>
		
		
	</table-->
	<div class="panel-body" style="text-align:center;"> 
		<div class="xcrud-list-container">
		<h3 style="float: left;">ข้อมูลการลา</h3>
		<table class="xcrud-list table table-striped table-hover table-bordered">  
			<tr>
			<th width="30%" style="color:#FFF">พนักงาน</th>
			<th width="30%" style="color:#FFF">หน่วยงาน</th>
			<th width="20%" style="color:#FFF">ประเภทการลา</th>
			<th width="20%" style="color:#FFF">วันที่ลา</th>
			</tr>
			<?php 
			if($dataList != NULL){
			foreach($dataList->result() as $row){ 
				?>
				<tr> 
				<td>                  
				<?php echo $row->staffFName." ".$row->staffLName; ?> 
				<?php if ($row->staffNickName != "") echo "(".$row->staffNickName.")"; ?></td>
				<td><?php echo getOrgByID($row->orgID); ?></td>
				<td><?php echo $row->absName;?></td>
				<td>
					<?php echo toFullDate($row->absStartDate,'th','full');?> ถึง <br>
					<?php echo toFullDate($row->absEndDate,'th','full');?>
				</td>
				</tr>
			<?php  } }else{
			echo '<tr><td colspan="5" style="text-align:center">ไม่พบข้อมูล</td></tr>';
			}?>
		</table>
		</div>
	</div>

<div>
	
</div>

<script>
     $.datepicker.regional['th'] ={
        changeMonth: true,
        changeYear: true,
        //defaultDate: GetFxupdateDate(FxRateDateAndUpdate.d[0].Day),
        yearOffSet: 543,
        showOn: "button",
        buttonImage: '<?=base_url()?>assets/images/common_calendar_month_-20.png',
        buttonImageOnly: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
        constrainInput: true,
       
        prevText: 'ก่อนหน้า',
        nextText: 'ถัดไป',
        yearRange: '-20:+0',
        buttonText: 'เลือก',
      
    };
    
    $.datepicker.setDefaults($.datepicker.regional['th']);

    $(function() {
        $( "#dp1433735797502" ).datepicker( $.datepicker.regional["th"] ); 

        $( "#dp1433735797503" ).datepicker( $.datepicker.regional["th"] ); 

    });
    
    var Holidays;
 
    //On Selected Date
    //Have Check Date
    function CheckDate(date) {
        var day = date.getDate();
        var selectable = true;//ระบุว่าสามารถเลือกวันที่ได้หรือไม่ True = ได้ False = ไม่ได้
        var CssClass = '';
        
        if (Holidays != null) {

            for (var i = 0; i < Holidays.length; i++) {
                var value = Holidays[i];
                if (value == day) {

                    selectable = false;
                    CssClass = 'specialDate';
                    break;
                }
            }
        }
        return [selectable, CssClass, ''];
    }

    //=====================================================================================================
    //On Selected Date
    function SelectedDate(dateText, inst) {
        //inst.selectedMonth = Index of mounth
        //(inst.selectedMonth+1)  = Current Mounth
        var DateText = inst.selectedDay + '/' + (inst.selectedMonth + 1) + '/' + inst.selectedYear;
        //CallGetUpdateInMonth(ReFxupdateDate(dateText));
        //CallGetUpdateInMonth(DateText);
        return [dateText, inst]
    }
    //=====================================================================================================
    //Call Date in month on click image
    function OnBeforShow(input, inst) {
        var month = inst.currentMonth + 1;
        var year = inst.currentYear;
        //currentDay: 10
        //currentMonth: 6
        //currentYear: 2012
        GetDaysShows(month, year); 
       
    }
    //=====================================================================================================
    //On Selected Date
    //On Change Drop Down
    function ChangMonthAndYear(year, month, inst) {

        GetDaysShows(month, year);
    }

    //=====================================
    function GetDaysShows(month, year) {
        //CallGetDayInMonth(month, year); <<เป็น Function ที่ผมใช้เรียก ajax เพื่อหาวันใน DataBase  แต่นี้เป็นเพียงตัวอย่างจึงใช้ Array ด้านล่างแทนการ Return Json
        //อาจใช้ Ajax Call Data โดยเลือกจากเดือนและปี แล้วจะได้วันที่ต้องการ Set ค่าวันไว้คล้ายด้านล่าง
        Holidays = [1,4,6,11]; // Sample Data
    }
    //=====================================
 
</script>  
