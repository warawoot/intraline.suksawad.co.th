<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReportLeavemodel extends MY_Model {

    function __construct()
    {
        parent::__construct(); 
        $this->table="";
        $this->pk = ""; 
    }

    function getLeave($agencies,$startDate, $endDate){
        $sql = "SELECT 
                    s.ID, 
                    s.staffID,                   
                    s.staffFName,
                    s.staffLName,
                    s.orgID,       
                    s.staffNickName,
                    sa.absStartDate,
                    sa.absEndDate,
                    ast.absName 
                FROM `tbl_staff` s  
                INNER JOIN `tbl_staff_absence` sa 
                    ON sa.staffID = s.ID 
                INNER JOIN `tbl_absence_type` ast
                    ON sa.absTypeID = ast.absTypeID 
                WHERE 
                    (
                        (DATE(sa.absStartDate) >= '".$startDate."' AND DATE(sa.absStartDate) <= '".$endDate."') OR 
                        (DATE(sa.absEndDate) >= '".$startDate."' AND DATE(sa.absEndDate) <= '".$endDate."') OR
                        (DATE(sa.absStartDate) <= '".$startDate."' AND DATE(sa.absEndDate) >= '".$endDate."')                     
                    )
                ";
                if($agencies != "")
                {
                    $sql .= " AND s.OrgID IN (".$agencies.")";
                }
                $sql .= " ORDER BY sa.absStartDate, sa.absEndDate";
        $r = $this->db->query($sql);
        return ($r->num_rows() > 0) ? $r : NULL;
    } 

    function get_reportLeave($agencies,$startDate,$endDate){
        $sql = "SELECT 
                    S.ID, 
                    S.staffID,                   
                    S.staffFName,
                    S.staffLName,
                    S.orgID,       
                    O.orgName,            
                    0 AS Sick,
                    0 AS Errand,
                    0 AS Holiday,
                    0 AS Special,
                    0 AS Other,
                    0 AS Summary,
                    0 AS Sick_absenceCount_minute,
                    0 AS Errand_absenceCount_minute,
                    0 AS Holiday_absenceCount_minute,
                    0 AS Special_absenceCount_minute,
                    0 AS Other_absenceCount_minute,
                    0 AS Summary_absenceCount_minute,
                    L.Summary_late 
                FROM 
                    tbl_staff S
                INNER JOIN
                    tbl_org_chart O
                    ON S.orgID = O.orgID
                LEFT JOIN                 
                 (
                    SELECT 
                        staffID,    
                        SUM(amtStart + amtEnd) AS Summary_late
                    FROM 
                        tbl_staff_work_late   
                    WHERE dateDaily >= '".$startDate."' and dateDaily <= '".$endDate."'
                    GROUP BY staffID
                ) AS L ON S.ID = L.staffID 
                WHERE 1 ";
                if($agencies != "")
                {
                    $sql .= " AND S.OrgID IN (".$agencies.")";
                }
      
                $query = $this->db->query($sql);
                return $query->result();
    }

    function get_UpdateLeave(){

        $sql = "SELECT stfAbsID,
		            DATE_FORMAT(absStartDate,'%Y-%m-%d') AS absStartDate ,
		            DATE_FORMAT(absEndDate, '%Y-%m-%d') AS absEndDate		            
		        FROM tbl_staff_absence 
                WHERE DATE_FORMAT(absStartDate,'%k:%i:%s') = 00-00-00 
                AND DATE_FORMAT(absEndDate, '%k:%i:%s') = 00-00-00
                ORDER BY stfAbsID ASC";                

                $query = $this->db->query($sql);
                return $query->result();
    }

    function get_UpdateLeave_time(){

        $sql = "SELECT stfAbsID,
                TIMEDIFF(DATE_FORMAT(absEndDate,'%k:%i:%s'),DATE_FORMAT(absStartDate,'%k:%i:%s')) 
                AS absenceCount_minute                   
                FROM tbl_staff_absence 
                WHERE DATE_FORMAT(absStartDate,'%k:%i:%s') != 00-00-00 
                AND DATE_FORMAT(absEndDate, '%k:%i:%s') != 00-00-00 
                ORDER BY stfAbsID ASC";                

                $query = $this->db->query($sql);
                return $query->result();
    }

    function CountHoliday($startDate,$endDate){       
        $sql = "SELECT holiday_date as num FROM tbl_holiday WHERE holiday_date >= '$startDate' AND holiday_date <= '$endDate'";
        $check = $this->db->query($sql);
        $count = count($check->result());	
        return $count;
    }
   
    function update_leave($stfAbsID,$data){
		$sql	=	$this->db->where('stfAbsID', $stfAbsID);
		$sql	=	$this->db->update('tbl_staff_absence', $data); 	
		if($this->db->affected_rows() > 0) {
			return 'data succesfully inserted';
		} else {
			return 'data failed inserted';
		}
    }    

     function getOrgName(){
            $sqlOrg = "SELECT orgName,orgID FROM tbl_org_chart";
            $orgName = $this->db->query($sqlOrg);
            return $orgName->result();
             
    } 
}
/*
        $sql = "SELECT 
                    S.staffID,                   
                    S.staffFName,
                    S.staffLName,
                    S.OrgID,                    
                    Sick,
                    Errand,
                    Holiday,
                    Special,
                    Other,
                    Summary,
                    Sick_absenceCount_minute,
                    Errand_absenceCount_minute,
                    Holiday_absenceCount_minute,
                    Special_absenceCount_minute,
                    Other_absenceCount_minute,
                    Summary_absenceCount_minute,
                    Summary_late
                
                FROM 
                    tbl_staff S 
                LEFT JOIN 
                (
                SELECT 
                    ID,    
                    SUM(absenceCount)  AS  Sick                    				
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID                
                WHERE absTypeID in (1,8) and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Sick ON S.ID = Sick.ID                
                 LEFT JOIN 
                (
                SELECT 
                    ID,    
                    SUM(absenceCount)  AS Errand                     
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID = 2 and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Errand ON S.ID = Errand.ID 
                 LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                    SUM(absenceCount)  AS Holiday                     
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID = 7 and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Holiday ON S.ID = Holiday.ID 
                 LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                    SUM(absenceCount)  AS Special                     
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID  in (9,10,11) and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Special ON S.ID = Special.ID 
                LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                    SUM(absenceCount) AS Other                     
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID  in (3,4,5,6) and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Other ON S.ID = Other.ID
                 LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                     SUM(absenceCount) AS Summary                    
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Summary ON S.ID = Summary.ID          
                LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                     SUM(absenceCount_minute) AS Sick_absenceCount_minute                    
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID  in (1,8) and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Sick_absenceCount_minute ON S.ID = Sick_absenceCount_minute.ID
                LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                     SUM(absenceCount_minute) AS Errand_absenceCount_minute                    
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID in (2) and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Errand_absenceCount_minute ON S.ID = Errand_absenceCount_minute.ID 
                LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                     SUM(absenceCount_minute) AS Holiday_absenceCount_minute                    
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID  in (7) and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Holiday_absenceCount_minute ON S.ID = Holiday_absenceCount_minute.ID 
                LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                     SUM(absenceCount_minute) AS Special_absenceCount_minute                    
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID  in (9,10,11) and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Special_absenceCount_minute ON S.ID = Special_absenceCount_minute.ID 
                LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                     SUM(absenceCount_minute) AS Other_absenceCount_minute                    
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE absTypeID  in (3,4,5,6) and statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Other_absenceCount_minute ON S.ID = Other_absenceCount_minute.ID 
                LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                     SUM(absenceCount_minute) AS Summary_absenceCount_minute                    
                FROM tbl_staff 
                LEFT JOIN tbl_staff_absence ON tbl_staff.ID = tbl_staff_absence.staffID  
                WHERE statusApprove = 1 and absStartDate >= '".$startDate."' AND absEndDate <= '".$endDate."'
                GROUP BY ID
                ) AS Summary_absenceCount_minute ON S.ID = Summary_absenceCount_minute.ID                
                LEFT JOIN                 
                 (
                 SELECT 
                    ID,    
                     SUM(amtStart + amtEnd) AS Summary_late, dateDaily                  
                FROM tbl_staff 
                LEFT JOIN tbl_staff_work_late ON tbl_staff.ID = tbl_staff_work_late.staffID  
                WHERE dateDaily >= '".$startDate."' and dateDaily <= '".$endDate."'
                GROUP BY ID
                ) AS Summary_late ON S.ID = Summary_late.ID
              
                WHERE 1 ";

                 if($agencies != null)
                 {
                     $sql .= " AND S.OrgID IN (".$agencies.")";
                 }
                 $sql .= "ORDER BY Summary DESC,Summary_absenceCount_minute DESC";
*/
?>