<?php
class Staff_check_yearly_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff_check_yearly';
            $this->_pk = 'checkID';
        }

        public function getAll($day,$month,$year){
             if(!empty($day) || !empty($month) || !empty($year)){

                $year -= 543;
            
                 $sql = "SELECT s.*
                        FROM tbl_staff s
                        WHERE s.staffID NOT IN (SELECT StaffID FROM tbl_staff_check_yearly WHERE ";
                if(!empty($day)){
                    $sql.=" DAY(checkDate) = '$day' AND ";
                }
                if(!empty($month)){
                     $sql.=" MONTH(checkDate) = '$month' AND ";
                  
                }
                if(!empty($year)){
                    $sql.=" YEAR(checkDate) = '$year' AND ";
                }
                
                $sql = substr($sql,0,-4).')';

                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r : NULL;
            }
            
        }
        

	
}
/* End of file Staff_check_yearly_model.php */
/* Location: ./application/module/reporthealthcheck/models/Staff_check_yearly_model.php */