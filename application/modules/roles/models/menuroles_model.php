<?php
    class Menuroles_model extends Default_Model{

        function __construct() {
            parent::__construct();
            $this->_table = 'menuroles';
            $this->_pk = 'MenuID';
        }
		
		function getMenuByRole($userID){
			$sql = "SELECT DISTINCT mr.MenuID,m.MenuName,m.MenuLink,m.MenuImage FROM users u
					JOIN usersroles ur ON u.UserID = ur.UserID
					JOIN menuroles mr ON ur.RoleID = mr.RoleID
					JOIN menu m ON m.MenuID = mr.MenuID
					WHERE u.UserID = '$userID'";
			$q = $this->db->query($sql);
			return ($q->num_rows() > 0) ? $q : NULL;
		}
		
		function getPermissionMenu($menuID,$roleID){
			$sql = "SELECT *
					FROM menuroles 
					WHERE RoleID in ($roleID) AND
						  MenuID = '$menuID'";
			$q = $this->db->query($sql);
			return ($q->num_rows()) ? $q : NULL;
		}
   
    }
?>