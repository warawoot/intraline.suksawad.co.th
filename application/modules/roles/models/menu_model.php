<?php
class Menu_model extends Default_Model{

   function __construct() {
        parent::__construct();
        $this->_table = 'menu';
        $this->_pk = 'MenuID';

    }
	
	function getNotMenuByRole($not_in){
		if(!empty($not_in)){
			$sql = "SELECT * FROM menu WHERE MenuID NOT IN ($not_in)";
			$r = $this->db->query($sql);
		}else{
			$r = $this->db->get('menu');
		}

		return ($r->num_rows() > 0) ? $r : NULL;
	}
	
	function getMenuByRole($userID){
		$sql = "SELECT DISTINCT m.MenuID,m.MenuName FROM users u
				LEFT JOIN usersroles ur ON u.UserID = ur.UserID
				LEFT JOIN menuroles mr ON ur.RoleID = mr.RoleID
				LEFT JOIN menu m ON m.MenuID = mr.MenuID
				WHERE u.UserID = '$userID'";
		$q = $this->db->query($sql);
		return ($q->num_rows() > 0) ? $q : 1;
	}

    
}
?>