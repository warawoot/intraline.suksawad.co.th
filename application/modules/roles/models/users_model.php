<?php
	class Users_model extends Default_Model{
	
		function __construct() {    	
			parent::__construct();
			$this->_table = 'tbl_user';
			$this->_pk = 'userID';
                }
		
		function getNotUserByRole($not_in){
			if(!empty($not_in)){
				$sql = "SELECT * FROM tbl_user WHERE userID NOT IN ($not_in)";
				$r = $this->db->query($sql);
			}else{
				$r = $this->db->get('tbl_user');
			}

			return ($r->num_rows() > 0) ? $r : NULL;
		}
		
	}
?>