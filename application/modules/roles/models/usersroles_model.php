<?php
    class Usersroles_model extends Default_Model{

        function __construct() {
            parent::__construct();
            $this->_table = 'tbl_usersroles';
            $this->_pk = 'userID';
        }
		
		function getRoleByUser($user){
			
			$r = $this->db->select('r.roleID,r.roleName')
						  ->from('tbl_roles r')
						  ->join('usersroles ur','ur.roleID=r.roleID')
						  ->where('ur.rserID',mysql_real_escape_string($user))
						  ->get();
						 
			return ($r->num_rows() > 0) ? $r : NULL;
		}
		
		

    }
?>