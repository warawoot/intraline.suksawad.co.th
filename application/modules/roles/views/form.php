<style>
    .error_lbl{
        border-color:red !important;
    }
        .ui-dialog-titlebar-close{
            display:none
        }
        .header-product{
            background-color: pink;
            font-weight: bold;
        }
        .header-product th{
            text-align: center;
            vertical-align: top;
        }
        .btn-none{
            display:none;
        }
        .error_lbl{
          border-color:red;
        }
</style>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
<div class="col-md-12" >

     <div class="col-separator box col-separator-first col-unscrollable">
        <div class="col-table">
                   <div class="row">
                      <div class="col-sm-12">
                          <section class="panel">
                              <header class="panel-heading">
                                  <h3><?php echo !empty($title) ? $title : "DPO"; ?></h3>
                              </header>
                              <div class="panel-body">
                              <form class="form-horizontal " id="rolesForm" name="rolesForm" method="post" action="<?php echo site_url(); ?>roles/submitRole" >
                               
                                <input type="hidden" name="roleID" value="<?php echo !empty($r) ? $r->roleID : ""; ?>">

                                <div class="xcrud">
                                  <div class="xcrud-container">
                                      <div class="xcrud-view">
                                        <div class="form-horizontal">
                                          
                                           <div class="form-group">
                                            <label class="control-label col-sm-3">ชื่อ Role *</label>
                                            <div class="col-sm-9">
                                            <?php if($this->uri->segment('2') != 'view'){ ?>
                                            <input type="text" maxlength="50" name="roleName" id="roleName" value="<?php echo !empty($r) ? $r->roleName : ""; ?>" data-type="text" data-required="1" class="xcrud-input form-control">
                                            <?php }else{
                                              echo $r->roleName;
                                              } ?>
                                            </div>
                                          </div>


                                      </div>
                                  </div>
                                  <div class="xcrud-top-actions btn-group">
                                      <?php if($this->uri->segment('2') != 'vieworg'){ ?>
                                      <a class="btn btn-primary"  href="javascript:submitForm();">บันทึกและย้อนกลับ</a>
                                      <?php } ?>
                                      <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>roles'">ย้อนกลับ</a>
                                  </div>
                                  </form>
                                 
                              </div>
                          </section>
                      </div>
                  </div>
            
             
                
        </div>  
      </div>
</div>

<script>
  

  function submitForm(){
    if($('#rolesForm').valid())
      $('#rolesForm').submit();
  }
  

  $("#rolesForm").validate({
            errorClass: "error_lbl",
            rules: {
                    roleName:"required"
            }, messages: {
                    roleName:""
            }
    });

  
</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/roles'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        

</script>
