
<script>
    $(document).ready(function() {
        $("#dialog-user").dialog({
			height: 150,
			width:220,
			autoOpen: false,
			modal: true,
			buttons: {
				'ตกลง': function() {
		if($('#user :selected').val() != ""){
			$('#portalRolesForm').submit();
		}else{
			
			$('#user').attr('style', 'border-color:red;width:170px;');
		}
			/*$( this ).dialog( "close" );*/
				}
			}
		});
	
	$("#dialog-confirm").dialog({
            height: 140,
            width:220,
            show: "drop",
            hide: "drop",
            autoOpen: false,
            modal: true,
            resizable: false,
            buttons: {
                "Yes": function() {
                    $.ajax({
                        url: "<?php echo site_url(); ?>roles/deleteRole",
                        type:"POST",
                        data:{user_id:$('#delID').val(),
                                  role_id:$('#role_id').val(),
                                  csrf_mict: '<?php echo $this->security->get_csrf_hash(); ?>'
                                 },
                        async:false,
                        dataType:"json",
                        success:function(data){
                            if(data.flag===true){
                                $( "#dialog-confirm" ).dialog( "close" );
                                //loadUserRolesGridData();
                                window.location='<?php echo base_url()?>roles/manageUserRoles?id='+$('#role_id').val();
                            }else
                                $("#dialog-warning").dialog("open");}

                   });	
                },
                "No": function() {
                        $( this ).dialog( "close" );
                }
            }
	});
        
  });
</script>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "SSRU"; ?></h3>

            </header>
            <div class="panel-body">
                
                <div class="xcrud">
                          <div class="xcrud-container">
                            <div class="xcrud-view">
                              <div class="form-horizontal">

                                    <input type="hidden" name="role_id" id="role_id" value="<?php echo $id; ?>" /> 
                                    <div class="form-group">
                                        <label class="control-label col-sm-12" style="text-align:center;"><strong>ชื่อ Role : <?php echo $RoleName; ?></strong></label>
                                       
                                    </div>

                                    <div class="form-group">
                                        <div class="control-label col-sm-12">

                                            <div style="width:80%;margin:auto;">
                                                <input type='hidden' name='delID' id='delID' >  
                                                <div id="roleportalGrid" style="width:370px;height:400px;margin:5px 0;float:left;"></div>
                                                <div id="portalGrid" style="width:370px;height:400px;margin:5px 0;float:right;"></div>
                                                <div style="clear:both;"></div>
                                                
                                            </div>  

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-12" style="text-align:center;"> <input type='button' value='Back' onclick="window.location='<?php echo site_url(); ?>roles'"></label>
                                       
                                    </div>


                              </div>
                            </div>
                        </div>
                    
                
                    
            </div>
            <div id="dialog-user" title="เพิ่ม Portal">
                <p> 
                    <center>
                        <?php echo form_open('roles/submitPortalInRoles','id="portalRolesForm"');?>
                            <?php echo user_ddl('user','','',$id); ?>
                            
                          <?php echo form_close();?>   
                    </center>
                </p>
            </div>

            </div>
        </section>
    </div>
</div>


<div id="dialog-confirm" title="Confirm">
    <p><?php echo confirmDel();?></p>
</div>
<script>
        
        function addPortalRole(r1, r2){
            $.ajax({
                url: "<?php echo site_url(); ?>roles/submitPortalInRoles",
                type:"POST",
                data:{portal:r1,
                      role_id: $('#role_id').val()
                },
                async:false,
                dataType:"json",
                success:function(data){
                    if(data.flag===true){
                        loadRolePortalGridData();
                        loadPortalGridData();   
                    }else
                        $("#dialog-warning").dialog("open"); 
                }
           });
        }
        
        function removePortalRole(r1, r2){
             $.ajax({
                url: "<?php echo site_url(); ?>roles/deletePortalRole",
                type:"POST",
                data:{portal_id:r1,
                      role_id:$('#role_id').val()
                     },
                async:false,
                dataType:"json",
                success:function(data){
                    if(data.flag===true){
                        loadRolePortalGridData();
                        loadPortalGridData();
                    }else
                        $("#dialog-warning").dialog("open");
					}

           });	
        }
        
		var roleportalGrid = new dhtmlXGridObject('roleportalGrid');
        roleportalGrid.setImagePath("<? echo base_url() ?>assets/images/dhtmlxGrid/imgs/");
        roleportalGrid.setHeader("Portal In Role");
        roleportalGrid.setInitWidths("350");
        roleportalGrid.setColAlign("center");
        roleportalGrid.setColTypes("ro");
        roleportalGrid.setColSorting("str");
        roleportalGrid.setSkin("dhx_skyblue");
        roleportalGrid.enableDragAndDrop(true);
        roleportalGrid.init();
        roleportalGrid.attachEvent("onDrag", addPortalRole);
	
		loadRolePortalGridData();
	
        var portalGrid = new dhtmlXGridObject('portalGrid');
        portalGrid.setImagePath("<? echo base_url() ?>assets/images/dhtmlxGrid/imgs/");
        portalGrid.setHeader("Portal");
        portalGrid.setInitWidths("350");
        portalGrid.setColAlign("center");
        portalGrid.setColTypes("ro");
        portalGrid.setColSorting("str");
        portalGrid.setSkin("dhx_skyblue");
        portalGrid.enableDragAndDrop(true);
        portalGrid.init();
        portalGrid.attachEvent("onDrag", removePortalRole);
        
        loadPortalGridData();
        
		function loadRolePortalGridData()
        { 
            roleportalGrid.clearAll();
            try{
                var responseData = $.ajax({    
                    url: "<?php echo site_url(); ?>roles/listAllPortalRoles",
                    data : {id: $('#role_id').val(),csrf_mict: '<?php echo $this->security->get_csrf_hash(); ?>'},
                    type : 'POST',
                    dataType : "json",
                    async:false
                }).responseText ;
                if(responseData != "" && responseData != null){
                    roleportalGrid.parse(eval('(' + responseData + ')'),"json");	
                }
            }catch(e){} 		
       
        } 	
        
        function loadPortalGridData()
        { 
            portalGrid.clearAll();
            try{
                var responseData = $.ajax({    
                    url: "<?php echo site_url(); ?>roles/listAllPortal",
                    data : {id: $('#role_id').val(),csrf_mict: '<?php echo $this->security->get_csrf_hash(); ?>'},
                    type : 'POST',
                    dataType : "json",
                    async:false
                }).responseText ;
                if(responseData != "" && responseData != null){
                    portalGrid.parse(eval('(' + responseData + ')'),"json");	
                }
            }catch(e){} 		
       
        }
        
	function openPopPortal(){
		$("#dialog-user" ).dialog( "open" );
		$("#user" ).removeAttr('style');
	}
	
	function deleteRole(id){
		$('#delID').val(id);
		$("#dialog-confirm" ).dialog( "open" );
	}
	
</script>
