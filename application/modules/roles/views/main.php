<style>
.xcrud-search-toggle{
    position:absolute;
    top:-47px;
    left:90px;
  }
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "DPO"; ?></h3>

            </header>
            <div class="panel-body">
                <a class="btn btn-success" href="javascript:window.location='<?php echo site_url();?>roles/add'"><i class="fa fa-plus"></i> เพิ่ม</a>
                <?php echo $html; ?>   
            </div>
        </section>
    </div>
</div>
<script>
function deleteRow(roleID){
    if(confirm("ต้องการลบข้อมูล ?")){
      $.ajax({
          url: "<?php echo site_url(); ?>roles/delete",
          type:"POST",
          data:{token:roleID,
                csrf_mict: '<?php echo $this->security->get_csrf_hash(); ?>'
           },
          async:false,
          dataType:"json",
          success:function(data){
              if(data.flag===true){
                 window.location='<?php echo site_url()?>roles';
              }else if(data.flag == 'DUP'){
                 alert('มี User ที่ใช้ Role นี้อยู่');
              }else
                 alert('เกิดข้อผิดพลาด');
             
          }
      });
    }
  }
</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/roles'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);


</script>