<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends Backend_Controller {

    function __construct(){
        parent::__construct();
        
        $this->load->model('roles_model');
        $this->load->model('usersroles_model');
        $this->load->model('users_model');
         $this->load->model('menuroles_model');
         $this->load->model('menu_model');
        
    }
    
    public function index(){

        
        $this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();

        
        // select table//
        $xcrud->table('tbl_roles');
        
        
        //// List /////
        $col_name = array(
            'roleName' => 'ชื่อ Roles'

        );

        $xcrud->columns('roleName');
        $xcrud->label($col_name);
        // End List//

        //// Form //////
        $xcrud->pass_var('roleID',$this->roles_model->getLastestID()+1,'create');
        $xcrud->fields('roleName');

        $xcrud->button(site_url().'roles/user/index?token={roleID}','Users','fa fa-users','btn-default'); 
        //$xcrud->button(site_url().'roles/portal/index?token={RoleID}','Portal','fa fa-folder','btn-success');
        //$xcrud->button(site_url().'roles/menu/index?token={roleID}','Menu','fa fa-list','btn-info');

        $xcrud->button(site_url().'roles/edit/index?token={roleID}','แก้ไข','fa fa-edit',array('target'=>'_blank'));

        //$xcrud->button(site_url().'roles/edit/index?token={roleID}','แก้ไข','fa fa-edit','btn-warning');
        $xcrud->button('javascript:;','ลบ','fa fa-remove','btn-danger',array('onclick'=>'javascript:deleteRow(\'{roleID}\')'));

        $xcrud->unset_view()->unset_add()->unset_remove()->unset_edit();

        $data['html'] = $xcrud->render();
        

        $data['title'] = "Roles";
        $this->template->load("template/admin2",'main', $data);

    }


    public function add(){
        $data['title'] = "Add Roles";
        $this->template->load("template/admin2",'form', $data);
    }
     public function edit(){
         if(!empty($_GET['token'])){
            $token = mysql_real_escape_string($this->input->get('token',true));
             $data['title'] = "Edit Roles";
            $data['r'] = $this->roles_model->get($token,true);
            $this->template->load("template/admin2",'form', $data);
        }
       
    }

    public function submitRole(){
        if(!empty($_POST['roleName'])){
            $roleName = mysql_real_escape_string($this->input->post('roleName',true));
            $roleID = mysql_real_escape_string($this->input->post('roleID',true));
            $arr = array( 'roleName'  =>  $roleName );

            ////////start transaction///////////
            $this->db->trans_begin();

            $role_id = $this->roles_model->save($arr,$roleID,false);

            
             if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();  
                redirect('roles','refresh');
           }
           else{
                $this->db->trans_commit();
                redirect('roles','refresh');
           }


        }
    }

    public function delete(){
         if(!empty($_POST['token'])){
            $roleID = mysql_real_escape_string(trim($this->input->post('token',true)));

            ////////start transaction///////////
            $this->db->trans_begin();
            $r = $this->usersroles_model->get_by(array('roleID'=>$roleID));
            
            if(empty($r)){
                $this->roles_model->delete_by(array('roleID'=>$roleID));
                //$this->portalroles_model->delete_by(array('roleID'=>$roleID));

                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();  
                    echo json_encode(array('flag'=>FALSE));
               }
               else{
                    $this->db->trans_commit();
                    echo json_encode(array('flag'=>TRUE));
               }
            }else
                echo json_encode(array('flag'=>'DUP'));
            
        }

    }

    //////////////// user //////////////

    public function user(){
        if(!empty($_GET['token'])){
            $data['id'] = mysql_real_escape_string($this->input->get('token',true));
            $r = $this->roles_model->get_by(array('roleID'=>$data['id']),true);
            if(!empty($r)) {
                 $data['RoleName'] = $r->roleName;
            } else {
                $data['RoleName'] = '';
            }
            $data['title'] = 'User In Roles';
            $this->template->load('template/admin2','form_userinroles',$data);

        } else 
            show_404();
    }

    
    public function listAlluserRoles(){
        if(!empty($_POST['id'])){
            $r = $this->usersroles_model->get_by(array('roleID'=>mysql_real_escape_string($this->input->post('id',true))));
            if(!empty($r)){
                $json = "rows:[";
                foreach($r as $row){
                     $rR = $this->users_model->get_by(array('userID'=>mysql_real_escape_string($row->userID)),true);
                      $json .= '{id:"' . $row->userID . '",
                               data:["' . htmlspecialchars($rR->userName) . '"]},';
               }
               echo "{" . substr($json, 0, -1) . "]}";
            }
        }
    }

    public function listAlluser(){
        if(!empty($_POST['id']))
            $r = $this->usersroles_model->get_by(array('RoleID'=>mysql_real_escape_string($this->input->post('id',true))));

            $not_in = '';
            if(!empty($r)){
                foreach($r as $row){
                    $not_in .= $row->userID.',';
                }
                $not_in = substr($not_in,0,-1);
            } 
            
            $r = $this->users_model->getNotUserByRole($not_in);

            $json = "rows:[";
            if($r != NULL){
                foreach($r->result() as $row){
                 $rR = $this->users_model->get_by(array('userID'=>mysql_real_escape_string($row->userID)),true);
                  $json .= '{id:"' . $row->userID . '",
                           data:["' . htmlspecialchars($rR->userName) . '"]},';
                }
            }
            
           echo "{" . substr($json, 0, -1) . "]}";
        
    }

    public function submitUsersInRoles(){   
       
        if(!empty($_POST['user']) && !empty($_POST['role_id'])){
            $arr = array(
                    'UserID'    => mysql_real_escape_string($this->input->post('user',true)),   
                    'RoleID'    => mysql_real_escape_string($this->input->post('role_id',true)) );
            $id = $this->usersroles_model->save($arr,null);
           echo json_encode(array('flag'=>true));
        }else
           echo json_encode(array('flag'=>false)); 
            
   }

   public function deleteRole(){
        if(!empty($_POST['role_id'])&&(!empty($_POST['user_id']))){
            $user_id = mysql_real_escape_string($this->input->post('user_id',true));
            $role_id = mysql_real_escape_string($this->input->post('role_id',true));
            if($this->usersroles_model->delete_by(array('userID'=> $user_id,'roleID'=> $role_id)) ){
                echo json_encode(array('flag'=>true));
            }else 
                echo json_encode(array('flag'=>false));

        }else
             echo json_encode(array('flag'=>false));
        
    }

    //////////////// end user //////////////

    

    //////////////// menu //////////////
     public function menu(){
        if(!empty($_GET['token'])){
            $data['id'] = htmlspecialchars($this->input->get('token',true));
            $r = $this->roles_model->get_by(array('RoleID'=>$data['id']),true);
            if(!empty($r)) {
                 $data['RoleName'] = $r->RoleName;
            } else {
                $data['RoleName'] = '';
            }
            $data['title'] = 'Menu In Roles';
            $this->template->load('template/admin2','form_menuinroles',$data);

        } else 
            show_404();
    }

        public function listAllMenuRoles(){
            if(!empty($_POST['id'])){
                
                $r = $this->menuroles_model->get_by(array('RoleID'=>mysql_real_escape_string($this->input->post('id',true))));
                
                if(!empty($r)){
                    $json = "rows:[";
                    foreach($r as $row){
                         $rR = $this->menu_model->get_by(array('MenuID'=>mysql_real_escape_string($row->MenuID)),true);
                          $json .= '{id:"' . $row->MenuID . '",
                                   data:["' . htmlspecialchars($rR->MenuName) . '"]},';
                   }
                   echo "{" . substr($json, 0, -1) . "]}";
                }
            }   
        }
        
        public function listAllMenu(){
            if(!empty($_POST['id'])){
                $r = $this->menuroles_model->get_by(array('RoleID'=>mysql_real_escape_string($this->input->post('id',true))));
                $not_in = '';
                if(!empty($r)){
                    foreach($r as $row){
                        $not_in .= $row->MenuID.',';
                    }
                    $not_in = substr($not_in,0,-1);
                } 
               
                $r = $this->menu_model->getNotMenuByRole($not_in);
                if($r != NULL){
                    $json = "rows:[";
                    foreach($r->result() as $row){
                         $rR = $this->menu_model->get_by(array('MenuID'=>mysql_real_escape_string($row->MenuID)),true);
                          $json .= '{id:"' . $row->MenuID . '",
                                   data:["' . htmlspecialchars($rR->MenuName) . '"]},';
                   }
                   echo "{" . substr($json, 0, -1) . "]}";
                }
            }       
        }
        
        public function submitMenuInRoles(){    
                
            if(!empty($_POST['menu']) && !empty($_POST['role_id'])){
                $arr = array(
                        'MenuID'    => htmlspecialchars($this->input->post('menu',true)),   
                        'RoleID'    => htmlspecialchars($this->input->post('role_id',true)) );
                $this->menuroles_model->save($arr,null);
                    echo json_encode(array('flag'=>true));
               
            }else
               echo json_encode(array('flag'=>false)); 
                
       }
       
        public function deleteMenuRole(){
            if(!empty($_POST['role_id'])&&(!empty($_POST['menu_id']))){
                $menu_id = mysql_real_escape_string($this->input->post('menu_id',true));
                $role_id = mysql_real_escape_string($this->input->post('role_id',true));
                if($this->menuroles_model->delete_by(array( 'MenuID'=>$menu_id,'RoleID'=>$role_id)) ){
                    echo json_encode(array('flag'=>true));
                }else 
                    echo json_encode(array('flag'=>false));

            }else
                 echo json_encode(array('flag'=>false));
        
        }

     //////////////// end menu //////////////
    

}

/* End of file roles.php */
/* Location: ./application/controllers/roles.php */
?>