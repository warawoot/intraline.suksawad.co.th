
<style>
	table,th,td{
		border: 1px solid black;
		border-collapse:collapse;
		text-align:center;
	}


</style>
	
	<?php $th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
          ?>
	<h5 style="text-align:center;">
		<!--Client Name--><br>
			อัตราพนักงานว่างประจำเดือน <?php echo $th_month[date("m")].' '.(date("Y")+543); ?>
		
	</h5>
	<br>
	
	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	
	<br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th>ลำดับ</th>
			<th>อัตราที่</th>
			<th >ชื่อ - นามสกุล</th>
			<th >ตำแหน่ง</th>
			<th >ระดับ</th>
			<th>แผนก</th>
			<th>กอง/กลุ่มงาน</th>
			<th>ฝ่าย/สำนักงาน</th>
			<th>หมายเหตุ</th>
			
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 

					$i++; ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td style="text-align:left;"><?php echo $row->segID; ?></td>
						<td>--- ว่าง ---</td>
						<td style="text-align:left;"><?php echo $row->positionName;?></td>
						<td style="text-align:left;"><?php 
						$rankend = (!empty($row->rankend)) ? $row->rankend : ""; 
						if($rankend == $row->rankstart)
							$rankend = "";
						else
							$rankend = " - ".$rankend;
						echo $row->rankstart.$rankend;?>
						</td>
						<td style="text-align:left;"></td>
						<td style="text-align:left;"></td>
						<td></td>
						<td><?php echo $row->seqRemark;?></td>
						
					</tr>
				
		<?php }
		}else{
			echo '<tr><td colspan="9">ไม่พบข้อมูล</td></tr>';
		}

		 ?>
		
	</table>
	</div>

	<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        /*$('#month').change(function(){
        	window.location='<?php echo site_url();?>reportposition?month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
        })

        $('#year').change(function(){
        	window.location='<?php echo site_url();?>reportposition?month='+$('#month :selected').val()+'&year='+$('#year :selected').val();
        })*/
		

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportposition/print_pdf');
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportposition/print_excel');
        }
</script>
