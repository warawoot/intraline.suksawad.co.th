
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
     
   ?>
	<h4 style="text-align:center;">
		<br>
			อัตราพนักงานประจำเดือน <?php echo $th_month[date("m")].' '.(date("Y")+543); ?>
		
	</h4>
	
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th>ลำดับ</th>
			<th>อัตราที่</th>
			<th >ชื่อ - นามสกุล</th>
			<th >ตำแหน่ง</th>
			<th >ระดับ</th>
			<th>แผนก</th>
			<th>กอง/กลุ่มงาน</th>
			<th>ฝ่าย/สำนักงาน</th>
			<th>หมายเหตุ</th>
			
		</tr>
		
		<?php 
		$i=0;
		if(!empty($r)){
			foreach($r as $row){ 

					$i++; ?>
					<tr>
						<td style="text-align:center;"><?php echo $i; ?></td>
						<td><?php echo $row->segID; ?></td>
						<td style="text-align:center;">--- ว่าง ---</td>
						<td><?php echo $row->positionName;?></td>
						<td><?php 
						$rankend = (!empty($row->rankend)) ? $row->rankend : ""; 
						if($rankend == $row->rankstart)
							$rankend = "";
						else
							$rankend = " - ".$rankend;
						echo $row->rankstart.$rankend;?>
						</td>
						<td style="text-align:left;"></td>
						<td style="text-align:left;"></td>
						<td></td>
						<td><?php echo $row->seqRemark;?></td>
						
					</tr>
				
		<?php }
		}else{
			echo '<tr><td colspan="9">ไม่พบข้อมูล</td></tr>';
		}

		 ?>
		
		
	</table>
	</div>