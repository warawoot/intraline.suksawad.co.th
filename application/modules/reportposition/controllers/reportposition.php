<?php

class Reportposition extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		

		//$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		//$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		$data['r'] = $this->staff_model->getbyPosition();
		
		$data['title'] = "รายงานตำแหน่งว่าง";

        $this->template->load("template/admin",'reportposition', $data);

	}

	public function print_pdf(){

		//$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		//$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);

		$data['r'] = $this->staff_model->getbyPosition();
		$data_r['html'] = $this->load->view('print',$data,true);
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		//$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		//$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);

		$data['r'] = $this->staff_model->getbyPosition();
		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportposition.php */
/* Location: ./application/module/reportposition/reportposition.php */