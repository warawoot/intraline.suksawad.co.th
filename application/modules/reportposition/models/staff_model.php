<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

        public function getbyPosition(){
        	$sql = "SELECT s.segID,s.seqRemark,p.positionName,r.rankName as rankstart,r2.rankName as rankend FROM `tbl_seguence` s 
                    JOIN tbl_position p on s.positionID = p.positionID
                    JOIN tbl_rank r on s.rankIDBegin = r.rankID
                    JOIN tbl_rank r2 on s.rankIDEnd = r2.rankID
                    where s.segID NOT IN (select segID from tbl_staff_work WHERE segID != '')";
        	$r = $this->db->query($sql);
        	return $r->result();
        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/reg/models/staff_model.php */