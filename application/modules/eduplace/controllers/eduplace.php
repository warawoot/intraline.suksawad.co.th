<?php

class Eduplace extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		
		// select table//
		$xcrud->table('tbl_edu_place');
		$xcrud->relation('countryID','tbl_country','countryID','countryName');
		$xcrud->relation('eduTypeID','tbl_edu_type','eduTypeID','eduTypeName');


		
		
		//// List /////
		$col_name = array(
			'eduPlaceName' => 'สถานศึกษา',
			'eduTypeID' => 'ประเภทสถานศึกษา',
			'countryID' => 'ประเทศ',
			'isGovSupport' => ''

		);

		$xcrud->columns('eduPlaceName,eduTypeID,countryID');
		$xcrud->label($col_name);
		// End List//

		//// Form //////
		$xcrud->pass_default('countryID','1');
		$xcrud->fields('eduPlaceName,eduTypeID,countryID,isGovSupport');
		$xcrud->change_type('isGovSupport','radio','',array('0'=>'ไม่รับเงินอุดหนุน','1'=>'รับเงินอุดหนุน'));

		$data['html'] = $xcrud->render();
		

		$data['title'] = "สถานศึกษา";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file eduplace.php */
/* Location: ./application/module/eduplace/eduplace.php */