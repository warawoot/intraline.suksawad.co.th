<?php

class Evaluation_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="tbl_eval_date";
        $this->pk = "evalYear";
	}
    
	
	function get_date_edit($evalYear,$evalRound){
		$this->db->select('*');
		$this->db->from('tbl_eval_date');
		$this->db->where('evalYear' ,$evalYear);
		$this->db->where('evalRound' ,$evalRound);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
         
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	    
    function insert_evaluation($data){
		
		$evalYear		= $data['evalYear'];
		$evalRound		= $data['evalRound'];
		$salary			= $data['salary'];
		 
		$this->db->select('evalYear , evalRound');
		$this->db->from('tbl_eval_date as t1');
		$this->db->where('t1.evalYear' ,$evalYear); 
		$this->db->where('t1.evalRound' ,$evalRound); 
  		$query 		= $this->db->get();
 		 
		if($query->num_rows() == 0){
				$sql	=	$this->db->insert("tbl_eval_date",$data);
				//exit();	 
				if($this->db->affected_rows() > 0){
						 return 'succesfully';
				}else{
						 return 'failed';
				}
		}else{
				return 'failed';
		}
 			
 	}
	
	
	function update_data($evalYear,$evalRound,$data){ 
  			
			$this->db->where('evalYear', $evalYear);
			$this->db->where('evalRound', $evalRound);
			$this->db->update('tbl_eval_date',$data); 	
			 
			//if($this->db->affected_rows() > 0){
					 return 'succesfully';
			//}else{
					 //return 'failed';
			//}
  	}

	function get_eva_form_desc($evalRound , $evalYear){
		$this->db->select('t1.evalYear  ,t1.evalRound , t1.endDate ,t1.startDate');
		$this->db->from('tbl_eval_date as t1');
		$this->db->where('t1.evalRound' ,$evalRound);
		$this->db->where('t1.evalYear' ,$evalYear);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
         
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	function get_eva_group($evalRound , $evalYear,$evalFormID){
		$this->db->select(' t2.evalYear , t2.evalRound, t1.startDate , t1.endDate ,t2.evalNameText ,t2.empGroup , t2.startLeval , t2.endLeval , t3.typeName');
		$this->db->from(' tbl_eval_date  as t1 ');
		$this->db->join('tbl_eval_form as t2' , 't1.evalRound = t2.evalRound','inner');
		$this->db->join('tbl_staff_type as t3' , 't2.empGroup = t3.typeID','inner');
		$this->db->where('t2.evalRound' ,$evalRound);
		$this->db->where('t2.evalYear' ,$evalYear);
		$this->db->where('t2.evalFormID' ,$evalFormID);
  	$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
         
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	function insert_eval_form_level($data){
  		
		$this->db->insert("tbl_eval_form_level", $data);
	}
	
	function get_count_eval_form_level($formid){
		 
		$this->db->select_max('levelID');
		$this->db->from('tbl_eval_form_level as t1'); 
		$this->db->where('t1.evalFormID' ,$formid); 
		$query 		= $this->db->get();
		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->levelID+1; 
		}else{
			$data = 1; 
		}
		
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	function get_eva_level($evalFormID , $levelID){
		$this->db->select('*');
		$this->db->from('tbl_eval_form_level as t1');
		$this->db->where('t1.evalFormID' ,$evalFormID);
		$this->db->where('t1.levelID' ,$levelID);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
         
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	function delete_eva_level($evalFormID , $levelID){
		$this->db->where('evalFormID', $evalFormID);
		$this->db->where('levelID', $levelID);
 		$this->db->delete('tbl_eval_form_level'); 
		if($this->db->affected_rows() > 0){
					 return 'succesfully';
			}else{
					 return 'failed';
			}
		
	}	
	
	function update_eva_level($evalFormID , $levelID , $data){
		
		$this->db->where('evalFormID', $evalFormID);
		$this->db->where('levelID', $levelID);
 		$this->db->update('tbl_eval_form_level',$data); 
		if($this->db->affected_rows() > 0){
					 return 'succesfully';
			}else{
					 return 'failed';
			}
		
	}	

//-------> New AJAX Form

	function get_eva_subject_and_group($evalFormID){
		$this->db->where('evalFormID', $evalFormID);
		$query_group =  $this->db->get('tbl_eval_group_subject')->result();
		foreach ($query_group as $group){
			$groupId = $group->evalGroupSubjectID;
			$this->db->where('evalGroupSubjectID', $groupId);
			$query_subject =  $this->db->get(' tbl_eval_subject')->result();
			$group->evalSubjects = $query_subject;			
		}
		return $query_group ;
	}

	function insert_group($data){
		$this->db->insert('tbl_eval_group_subject',$data);	 
		if($this->db->affected_rows() > 0){
			return 'succesfully';
		}else{
			return 'failed';
		}
	}	

	function delete_group($evalGroupSubjectID){
		$this->db->where('evalGroupSubjectID', $evalGroupSubjectID);
		$this->db->delete('tbl_eval_subject');
		$this->db->where('evalGroupSubjectID', $evalGroupSubjectID);
		$this->db->delete('tbl_eval_group_subject'); 
		if($this->db->affected_rows() > 0){
			return 'succesfully';
		}else{
			return 'failed';
		}
	}			

	function update_group($data){
		$evalGroupSubjectID		= $data['evalGroupSubjectID'];
		unset($data['evalGroupSubjectID']);
		$this->db->where('evalGroupSubjectID', $evalGroupSubjectID);
		$this->db->update('tbl_eval_group_subject', $data); 

		if($this->db->affected_rows() > 0){
			return 'succesfully';
		}else{
			return 'failed';
		}
	}

	function insert_subject($data){
		$this->db->insert('tbl_eval_subject',$data);	 
		if($this->db->affected_rows() > 0){
			return 'succesfully';
		}else{
			return 'failed';
		}
	}	


	function update_subject($data){
		$evalSubjectID		= $data['evalSubjectID'];
		unset($data['evalSubjectID']);
		$this->db->where('evalSubjectID', $evalSubjectID);
		$this->db->update('tbl_eval_subject', $data); 

		if($this->db->affected_rows() > 0){
			return 'succesfully';
		}else{
			return 'failed';
		}
	}

	function delete_subject($evalSubjectID){
		$this->db->where('evalSubjectID', $evalSubjectID);
		$this->db->delete('tbl_eval_subject'); 
		if($this->db->affected_rows() > 0){
			return 'succesfully';
		}else{
			return 'failed';
		}
	}		
} 