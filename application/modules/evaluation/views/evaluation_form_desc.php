<style type="text/css">
	.form-control {
 	  color: #343232;
	}
    #ui-datepicker-div{  
		font-size: 0.9em;
 	}  
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
 <div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูล coming soon ...</h3>
            </header>
            <div class="panel-body">
                 
                       
             </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 

 $(document).ready(function () {
	 var dateBefore=null;  
	  $("#dp1433735797502").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  });
	  
	  $("#dp1433735797503").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  }); 
 }); 
 
 function modal_back(){
	location.replace('<?php echo base_url().'evaluation'; ?>');
 }
 
 function modal_ck(){
	 
	var evalYear	= $("#dGJsX2V2YWxfZGF0ZS5ldmFsWWVhcg--").val();
	var evalRound	= $("#dGJsX2V2YWxfZGF0ZS5ldmFsUm91bmQ-").val();
	var startDate	= $("#dp1433735797502").val();
	var endDate		= $("#dp1433735797503").val();
	
	if(evalYear == ''){
		  alert('กรุณาระบุ การประเมินประจำปี ด้วยค่ะ');
		  location.href = '<?php echo base_url().'evaluation/add?token='.$token.'&token1='.$token1; ?>';
	}else if(evalRound == ''){
		  alert('กรุณาระบุ ครั้งที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'evaluation/add?token='.$token.'&token1='.$token1; ?>';
	}else if(startDate ==''){
		  alert('กรุณาระบุ ตั้งแต่วันที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'evaluation/add?token='.$token.'&token1='.$token1; ?>';
	}else if(endDate == ''){	
		  alert('กรุณาระบุ ถึงวันที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'evaluation/add?token='.$token.'&token1='.$token1; ?>';
	}else{
		 
		  $.ajax({
				url: "<?php echo base_url('evaluation/check_add?action='.$actions) ?>",
				type: 'POST',
				data: {
						evalYear: evalYear,
						evalRound: evalRound,
						startDate: startDate,
						endDate: endDate
				}, 
				success: function(response) {
					//Do Something 
						if(response == 'succesfully'){
 							location.replace('<?php echo base_url().'evaluation'?>');
						}else{
 							//var obj = jQuery.parseJSON(response); 
							alert('กรุณาลองใหม่อีกครั้ง');
							location.replace('<?php echo base_url().'evaluation/add' ?>');
						}
				},
				error: function(xhr) {
					//Do Something to handle error
					alert('กรุณาลองใหม่อีกครั้ง');
					location.replace('<?php echo base_url().'evaluation' ?>');
				}
				
		 }); //end $.ajax
		  
	} //end if	
 }
</script>