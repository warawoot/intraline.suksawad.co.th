<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ  / ระดับคะแนน</h3>
            </header>
                    <div class="panel-body">
                        <link href="<?php echo site_url();?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                        <link href="<?php echo site_url();?>xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                        <link href="<?php echo site_url();?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                        <link href="<?php echo site_url();?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                        <div class="xcrud">
                                <div class="xcrud-container">
                                        <div class="xcrud-ajax">
                                        	 
                                            <div class="xcrud-view">
                                			    <div class="form-horizontal">
                                                    <div class="form-group"><label class="control-label col-sm-3">ชื่อระดับ*</label>
                                                    <div class="col-sm-9">
                                                    	<input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="" name="name" id="name" maxlength="255"></div>
                                                     </div>
                                                    <div class="form-group"><label class="control-label col-sm-3">คะแนนต่ำสุด*</label>
                                                    <div class="col-sm-9">
                                                    	 <input class="xcrud-input form-control" data-required="1" type="text" data-type="text" value="" name="dGJsX2V2YWxfZm9ybS5ldmFsTmFtZVRleHQ-" maxlength="255"></div>
                                                     </div>
                                                     <div class="form-group"><label class="control-label col-sm-3">คะแนนสุงสุด</label>
                                                     <div class="col-sm-9">
                                                     	<input class="xcrud-input form-control" type="text" data-type="int" value="" name="maxScore" id="maxScore" data-pattern="integer" maxlength="11"></div>
                                                     </div> 
                                               </div>
                                            </div>
                                            <div class="xcrud-top-actions btn-group">
                                               <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action"  onclick="on_save()">บันทึกและย้อนกลับ</a><a href="javascript:;" data-task="list" class="btn btn-warning xcrud-action"  onclick="on_back()">ย้อนกลับ</a>
                                            </div>
                                    <div class="xcrud-nav"> </div>
                                </div>
                                
                            </div>
                    </div>
                </div>
        </section>
    </div>
</div> 


<script type="text/javascript">
function on_back(){
  		 location.href = '<?php echo base_url('evaluation/score'); ?>';
}
	  		
function on_save(){
 	 	
 		var varname			= $('#name').val();
		var varminScore		= $('#minScore').val(); 
		var varmaxScore		= $('#maxScore').val();
 		
		 if(varname == '' ){
			 alert('กรุณาเลือก อัตราที่ ด้วยค่ะ');
			 document.getElementById("name").focus();
 			 
		 }else{
			  $.ajax({
 					url: "<?php echo base_url('evaluation/check_scores') ?>",
					type: 'POST',
					data: {
							name: varname,
							minScore: varminScore,
							maxScore: varmaxScore ,
							rounds: rounds ,
							year: year 
							 
					},
					success: function(response) {
						//Do Something 
						//alert(response);
						if(response == 'succesfully'){
 							location.replace('<?php //echo base_url('sequence'); ?>');
						}else{
 							location.replace('<?php //echo base_url('sequence/add'); ?>'); 
						}
											 
						 
					},
					error: function(xhr) {
						//Do Something to handle error
						//alert('error');
						alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
 						location.replace('<?php //echo base_url('sequence'); ?>');
					}
				});	 
 		}
				
}
		
	
</script>
