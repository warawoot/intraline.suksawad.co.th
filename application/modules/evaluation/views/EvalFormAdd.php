                  
                    
  <style type="text/css">
	.bs-example:after {
			position: absolute;
			top: 15px;
			left: 15px;
			font-size: 12px;
			font-weight: 700;
			color: #959595;
			text-transform: uppercase;
			letter-spacing: 1px;
			content: "ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ ";
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
             </header>
            <div class="panel-body">
            	<div class="bs-example" data-example-id="horizontal-dl">
                    <dl class="dl-horizontal">
                      <dt>การประเมินผลประจำปี</dt>
                      <dd>2558</dd>
                      <dt>ครั้งที่</dt>
                      <dd>1</dd>
                      <dt>ตั้วแต่วันที่ </dt>
                      <dd>1 สิงหาคม 2558&nbsp;ถึงวันที่&nbsp;31 สิงหาคม 2558</dd>
                    </dl>
                </div>
            </div>
        </section>
    </div>
</div>
<br />
<div class="row">
 	<div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>จัดทำแบบประเมินผล</h3>
            </header>
            <div class="panel-body">
                <link href="http://dpo-ehr.smmms.com/xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="http://dpo-ehr.smmms.com/xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                     <div class="xcrud">
                        <div class="xcrud-container">
                            <div class="xcrud-ajax">
                             
                                <div class="xcrud-view">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">กลุ่มพนักงาน*</label>
                                            <div class="col-sm-9">
											<select name="eval_group" class="form-control">
												<option value="">กรุณาระบุ</option>
												<option value="1">พนักงานประจำ</option>
												<option value="2">พนักงานจ้าง</option>
												<option value="3">ลูกจ้าง</option>
											</select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ชื่อแบบประเมิน*</label>
                                                <div class="col-sm-9">
                                                    <input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="int" value="">
                                                </div>
                                         </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ประเภท *</label>
                                                <div class="col-sm-9">
                                                    <input type="radio" name="formtype" value="1" checked> ประเมินพฤติกรรม<br/>
                                                    <input type="radio" name="formtype" value="2"> ประเมิน KPI/Competency
                                                </div>
                                         </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">น้ำหนักรวม*</label>
                                                <div class="col-sm-9">
                                                    <input class="xcrud-input form-control" style="width:300px" data-required="1" data-unique="" type="text" data-type="int" value="">
													&nbsp;&nbsp;(0-100)
                                                </div>
                                         </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ระดับเริ่มต้น*</label>
                                            <div class="col-sm-9">
											<select name="eval_group" class="form-control">
												<option value="">กรุณาระบุ</option>
												<option value="1">ล.ชั้น 1</option>
												<option value="2">ล.ชั้น 2</option>
												<option value="3">ล.ชั้น 3</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ระดับสิ้นสุด*</label>
                                            <div class="col-sm-9">
											<select name="eval_group" class="form-control">
												<option value="">กรุณาระบุ</option>
												<option value="1">ล.ชั้น 1</option>
												<option value="2">ล.ชั้น 2</option>
												<option value="3">ล.ชั้น 3</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ผู้ประเมิน *</label>
                                            <div class="col-sm-9">
											<select name="eval_group" class="form-control">
												<option value="">ไม่กำหนด</option>
												<option value="1">ผู้บังคับบัญชาระดับแผนก</option>
												<option value="2">ผู้บังคับบัญชาระดับกอง</option>
												<option value="3">ผู้บังคับบัญชาระดับฝ่าย</option>
												<option value="3">รองผู้อำนวยการ</option>
												<option value="3">ผู้อำนวยการ</option>
											</select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ผู้บังคับบัญชาเหนือขึ้นไป *</label>
                                            <div class="col-sm-9">
											<select name="eval_group" class="form-control">
												<option value="">ไม่กำหนด</option>
												<option value="1">ผู้บังคับบัญชาระดับแผนก</option>
												<option value="2">ผู้บังคับบัญชาระดับกอง</option>
												<option value="3">ผู้บังคับบัญชาระดับฝ่าย</option>
												<option value="3">รองผู้อำนวยการ</option>
												<option value="3">ผู้อำนวยการ</option>
											</select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ผู้รับรอง *</label>
                                            <div class="col-sm-9">
											<select name="eval_group" class="form-control">
												<option value="">ไม่กำหนด</option>
												<option value="1">ผู้บังคับบัญชาระดับแผนก</option>
												<option value="2">ผู้บังคับบัญชาระดับกอง</option>
												<option value="3">ผู้บังคับบัญชาระดับฝ่าย</option>
												<option value="3">รองผู้อำนวยการ</option>
												<option value="3">ผู้อำนวยการ</option>
											</select>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                <div class="xcrud-top-actions btn-group">
                                    <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action" onclick="modal_ck()">บันทึกและย้อนกลับ</a><a href="javascript:;" data-task="list" class="btn btn-warning xcrud-action" onclick="modal_back()">ย้อนกลับ</a>
                                </div>
                            </div>
                     </div>
                </div> 
             </div>
		  </div>        
</div> 