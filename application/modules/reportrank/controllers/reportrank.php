<?php

class Reportrank extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		
	}
	
	public function index(){

		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		
		//$data['r'] = $this->staff_model->get($data['month'],$data['year']);
		$data['r'] = NULL;
		$data['title'] = "รายงานผู้มีสิทธิ์เลื่อนระดับชั้น (C)";
        $this->template->load("template/admin",'reportrank', $data);

	}

	public function print_pdf(){

		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		$data['r'] =  NULL;

		$data_r['html'] = $this->load->view('print',$data,true);
		
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		$data['month'] = (!empty($_GET['month'])) ? $this->input->get('month') : date("m");
		$data['year'] = (!empty($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		$data['r'] = NULL;
		
		
		$this->load->view('print_excel',$data);

	}

}
/* End of file reportrank.php */
/* Location: ./application/module/reportrank/reportrank.php */