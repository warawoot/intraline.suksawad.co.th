<?php

$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
      

header('Content-type: application/excel');
$filename = 'รายงานผู้มีสิทธิ์เลื่อนระดับชั้น.xls';
header('Content-Disposition: attachment; filename='.$filename);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $filename; ?></title>
        

    </head>
	<body>

	
		<h5 style="text-align:center;">
		</h5>
		<h5 style="text-align:center;">
			รายงานสถานภาพการทำงานของพนักงาน ประจำเดือน <?php echo $th_month[$month]; ?>
		
	</h5>
	<br>
	
	<p style="font-size:11px;">ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered" style="font-size:11px;">
	
		<tr >
			<th >ลำดับ</th>
			<th >รหัสพนักงาน</th>
			<th >อัตราที่</th>
			<th >ชื่อ-นามสกุล</th>
			<th >ตำแหน่ง</th>
			<th >ระดับปัจจุบัน</th>
			<th >ระดับที่มีสิทธิ์เลื่อนชั้น</th>
			
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
			foreach($r->result() as  $row){ 
					$i++;
				
				?>
				<tr ><!-- td 10 ตัว-->
					<td><?php echo $i; ?></td>
					<td><?php echo $row->staffID;?></td>
					<td></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo getPositionByWork($row->ID);?></td>
					<td><?php echo getRankByWork($row->ID);?></td>
					<td></td>
				</tr>
		<?php  } 
		}else{
			echo '<tr><td colspan="7" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
		
		
	</table>
	</div>
	</body>
</html>
