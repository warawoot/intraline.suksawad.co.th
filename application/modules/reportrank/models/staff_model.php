<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

        public function getByContractDate($month,$year){
            if(!empty($month) && !empty($year)){

                $year -= 543;
            
                 $sql = "SELECT * 
                        FROM tbl_staff_contract c 
                        JOIN tbl_staff s ON c.staffID = s.ID
                        WHERE c.contractID = (SELECT max(contractID) from tbl_staff_contract WHERE staffID=c.staffID)";    
                
                
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r : NULL;
            }
            
        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/Reportstatusemployees/models/staff_model.php */