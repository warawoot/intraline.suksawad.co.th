<?php

class Staffinsignia extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_insignia');
		$xcrud->where('staffID',$id);

		$xcrud->relation("insigniaType","tbl_insignia","insigniaID","insigniaAlias");
		//// List /////
		$col_name = array(
			'insigniaYear' => 'ปี พ.ศ. ที่ได้รับ',
			'insigniaType' => 'ประเภทเครื่องราช',
			'insigniaNo' => 'ครั้งที่พิจารณาให้เครื่องราช'

		);

		$xcrud->columns('staffID,insigniaID',true);
		$xcrud->label($col_name);
		$xcrud->column_callback('insigniaYear','toBDYear');
		$xcrud->column_width("insigniaNo","10%")->column_width("insigniaYear","20%");
		// End List//

		//// Form //////

		$xcrud->pass_var('staffID',$id);
		$xcrud->fields('staffID,insigniaID',true);
		for($i=1; $i<=9; $i++){
			$no[] = $i;
		}
		$xcrud->change_type('insigniaNo','select','',$no);
		for($i=date("Y"); $i>=(date("Y")-60); $i--){
			$year[$i] = $i+543;
		}
		
		$xcrud->change_type('insigniaYear','select','',$year);
		$xcrud->validation_required('insigniaYear,insigniaType,insigniaNo');
		// End Form//
		
		$data['html'] = $xcrud->render();
		$data['title'] = "เครื่องราช";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

}
/* End of file staffinsignia.php */
/* Location: ./application/module/staffinsignia/staffinsignia.php */