
<?php 

	$th_month = array(''=>'-- กรุณาเลือก --','01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฎาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
    $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h4 style="text-align:center;">
		<br>
			รายชื่อพนักงานที่ไม่ตรวจสุขภาพ <?php echo (!empty($day)) ? 'วันที่ '.$day : ""; ?> <?php echo (!empty($month)) ? 'ประจำเดือน '.$th_month[$month] : ""; ?> <?php echo (!empty($year)) ? 'ปี '.$year : ""; ?>
		
	</h4>
	
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ลำดับ</th>
			<th >สถานพยาบาล</th>
			<th >ประเภทสถานพยาบาล</th>
			<th >ประเภทผู้ป่วย</th>
			<th >ยอดการเบิก</th>
			
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
				$hosID_tmp = "";
				$patType_tmp = "";
				$price_tmp = "";
				$flg_end = false;
				foreach($r->result() as  $row){ 
					
					if($hosID_tmp != $row->hosID){
						
						$hosID_tmp = $row->hosID;
						$patType_tmp = $row->patTypeID;

						if($i > 0){
							echo '<td>'.number_format($price_tmp).'</td>';
							echo '</tr>';
							$price_tmp = 0;
						}
						$i++;
						echo '<tr>';
						echo '<td>'.$i.'</td>';
						echo '<td>'.$row->hosName.'</td>';
						echo '<td>'.$row->hosTypeName.'</td>';
						echo '<td>'.$row->patTypeName.'</td>';
						
						//$price=0;
						$arrp = json_decode($row->takePrice);
						foreach($arrp as $rowp){
							$price_tmp+=$rowp->request;
						}

					}else if($hosID_tmp == $row->hosID && $patType_tmp == $row->patTypeID){
						
						$arrp = json_decode($row->takePrice);
						foreach($arrp as $rowp){
							$price_tmp+=$rowp->request;
						}
					}else if($hosID_tmp == $row->hosID && $patType_tmp != $row->patTypeID){
						
						$hosID_tmp = $row->hosID;
						$patType_tmp = $row->patTypeID;

						echo '<td>'.number_format($price_tmp).'</td>';
						echo '</tr>';
						$price_tmp = 0;

						echo '<tr>';
						echo '<td colspan="3"></td>';
						echo '<td>'.$row->patTypeName.'</td>';

						$arrp = json_decode($row->takePrice);
						foreach($arrp as $rowp){
							$price_tmp+=$rowp->request;
						}

					}

				 
		  	} 
		  	echo '<td>'.number_format($price_tmp).'</td>';
			echo '</tr>';

		}else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>