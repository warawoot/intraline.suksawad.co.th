<?php

class Workstatus_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="tbl_work_status";
        $this->pk = "workStatusID";
	}
        
    function max_id(){
	    $this->db->select_max('workStatusID');
		$this->db->from('tbl_work_status');
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->workStatusID; 
 		}else{
			$data 		=  '1'; //  hot เท่ากับ login แต่ status ไม่เท่า 2
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
   function max_data($workStatusID){
	    $this->db->select('*');
		$this->db->from('`tbl_work_status`');
		$this->db->where('`workStatusID`' ,$workStatusID);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        //print_r($data);
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
} 