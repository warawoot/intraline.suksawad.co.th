<?php

class Workstatus extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('workstatus_model');
    }
 

    public function index() {
 		 
        $max 	= $this->workstatus_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
		}else{
 			$max_id  =  $max+1;
 		}
 	 
 		$xcrud	=	xcrud_get_instance();
 		 
        $xcrud->table('tbl_work_status'); 
        $arr		=	array("workStatusName"=>"สถานะพนักงงาน");
   		 
 		// -- validation_required -- 
		$xcrud->validation_required('workStatusName') ;
 		 
		$xcrud->pass_var('workStatusID',$max_id,'create');
 		 
        $xcrud->label($arr);
        $xcrud->columns($arr);  
         $xcrud->fields($arr); 
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
		$data['id']	=   '';
        $this->template->load('template/admin', 'workstatus',$data);
    }
     
   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>