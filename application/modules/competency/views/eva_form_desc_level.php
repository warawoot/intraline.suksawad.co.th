<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ  / ระดับ</h3>
            </header>
            <div class="panel-body">
                <?php echo $data; ?>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">

	$(".xcrud-top-actions").find('.btn-success').html("<i class=\"glyphicon glyphicon-plus-sign\"></i> เพิ่มข้อมูลระดับ"); 
    $(".xcrud-top-actions").find('.btn-group').append('<a href="javascript:;" data-task="list" class="btn btn btn-primary" onclick="modal_back_back()">ย้อนกลับหน้าจัดออกแบบ</a>');
	$(".xcrud-top-actions").find('.btn-success').attr("onClick","add_score()");
	//$(".btn-group").find('.btn-warning').attr("onClick","edit_seq();");
	function add_score(){
 		  location.href = '<?php echo base_url(); ?>evaluation/scores/?formid=<?php echo $formid;?>&year=<?php echo $year;?>&round=<?php echo $round;?>';
		 //location.replace('<?php echo base_url();?>evaluation/scores');
	}
	function edit_seq(){
		//location.replace('<?php echo base_url();?>evaluation/edit');
	}
	
	if (Xcrud.current_task == 'list') {
            $(".xcrud-top-actions").find('.btn-success').html("<i class=\"glyphicon glyphicon-plus-sign\"></i> ย้อนกลับหน้าจัดการ"); 
			$(".xcrud-top-actions").find('.btn-group').append('<a href="javascript:;" data-task="list" class="btn btn btn-primary" onclick="modal_back_back()">ย้อนกลับหน้าจัดออกแบบ</a>');
    };
	
	function modal_back_back(){
        location.href = '<?php echo base_url(); ?>'+'evaluation/eval_desc/?year=<?php echo $year?>&round=<?php echo $round;?>&formid=<?php echo $formid;?>';
    }
	
	function myFunction(formid,level){
	if (confirm("ต้องการลบใช่หรือไม่") == true) {
		location.replace('<?php echo base_url(); ?>evaluation/level_delete/?year=<?php echo $year;?>&formid=<?php echo $formid;?>&round=<?php echo $round;?>&level='+level);
	} else {
		location.replace('<?php echo base_url(); ?>evaluation/eval_desc/?year=<?php echo $year;?>&round=<?php echo $round;?>&level='+level); 
	}
}		
</script>
 