<style type="text/css">
	.form-control {
 	  color: #343232;
	}
    #ui-datepicker-div{  
		font-size: 0.9em;
 	}  
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
 <div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลการประเมิน Competency</h3>
            </header>
            <div class="panel-body">
                <link href="<?php echo base_url()?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo base_url()?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                     <div class="xcrud">
                        <div class="xcrud-container">
                            <div class="xcrud-ajax">
                             
                                <div class="xcrud-view">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">การประเมินประจำปี*</label>
                                            <div class="col-sm-9"><input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="int" value="<?php echo $get_data['evalYear'];?>" name="dGJsX2V2YWxfZGF0ZS5ldmFsWWVhcg--" id="dGJsX2V2YWxfZGF0ZS5ldmFsWWVhcg--" data-pattern="integer" maxlength="11" <?php if($get_data['evalYear'] == ''){echo "";}else{echo "readonly=\"readonly\"";}?>  onkeypress="return isNumberKey(event)">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ครั้งที่*</label>
                                                <div class="col-sm-9">
                                                    <input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="int" value="<?php echo $get_data['evalRound'];?>" name="dGJsX2V2YWxfZGF0ZS5ldmFsUm91bmQ-" id="dGJsX2V2YWxfZGF0ZS5ldmFsUm91bmQ-" data-pattern="integer" maxlength="11"  <?php if($get_data['evalRound'] == ''){echo "";}else{echo "readonly=\"readonly\"";}?> onkeypress="return isNumberKey(event)" >
                                                </div>
                                         </div>
                                         <div class="form-group">
                                            <!--<label class="control-label col-sm-3">ประเมินผล / เลื่อนเงินเดือน*</label>
                                            <div class="col-sm-9"> <input name="salary" id="salary" type="radio" value="1" <?php //if($get_data['salary'] == 1){echo "checked=\"checked\"";} ?>/>ประเมินผล &nbsp;&nbsp;<input name="salary" id="salary" type="radio" value="2"  <?php //if($get_data['salary'] == 2){echo "checked=\"checked\"";} ?>/>เลื่อนเงินเดือน
                                            
                                            </div>-->
                                            <input name="salary" id="salary" type="hidden" value="1" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">ตั้งแต่วันที่*</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control" data-required="1" type="text" data-type="date" value="<?php echo $date_start;?>" name="dp1433735797502" id="dp1433735797502">
                                                </div>
                                       </div>
                                       <div class="form-group">
                                            <label class="control-label col-sm-3">ถึงวันที่*</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" data-required="1" type="text" data-type="date" value="<?php echo $date_end;?>" name="dp1433735797503" id="dp1433735797503">
                                            </div>
                                       </div>
                                       <div class="form-group">
                                            <label class="control-label col-sm-3">วันครบรอบกำหนดการประเมิน*</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" data-required="1" type="text" data-type="date" value="<?php echo $date_due;?>" name="datedue" id="datedue">
                                            </div>
                                       </div>
                                    </div>
                                 </div>
                                <div class="xcrud-top-actions btn-group">
                                    <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action" onclick="modal_ck()">บันทึกและย้อนกลับ</a><a href="javascript:;" data-task="list" class="btn btn-warning xcrud-action" onclick="modal_back()">ย้อนกลับ</a>
                                </div>
                            </div>
                     </div>
                </div> 
             </div>
             
        </section>
    </div>
</div>
<script type="text/javascript"> 

 $(document).ready(function () {
	 var dateBefore=null;  
	  $("#dp1433735797502").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  });
	  
	  $("#dp1433735797503").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  }); 
	  
	   $("#datedue").datepicker({  
			dateFormat: 'dd/mm/yy',  
			/*showOn: 'button',  */
			buttonImageOnly: false,  
			dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
			//monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
			monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
			changeMonth: true,  
			changeYear: true 
	  }); 
 }); 
 
 function modal_back(){
	location.replace('<?php echo base_url().'competency'; ?>');
 }
 
 function modal_ck(){
	 
	var evalYear	= $("#dGJsX2V2YWxfZGF0ZS5ldmFsWWVhcg--").val();
	var evalRound	= $("#dGJsX2V2YWxfZGF0ZS5ldmFsUm91bmQ-").val();
	var startDate	= $("#dp1433735797502").val();
	var endDate		= $("#dp1433735797503").val();
	var dateDue		= $("#datedue").val();
	//alert(dateDue);
	//var salary		= $("#salary").val();
	//var salary		=  document.getElementById("salary").checked = true;
	//alert(salary.value);
	var salary		= $( "input:radio[name=salary]:checked" ).val();
 	 
	if(evalYear == ''){
		  alert('กรุณาระบุ การประเมินประจำปี ด้วยค่ะ');
		  location.href = '<?php echo base_url().'competency/add?token='.$token.'&token1='.$token1; ?>';
	}else if(evalRound == ''){
		  alert('กรุณาระบุ ครั้งที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'competency/add?token='.$token.'&token1='.$token1; ?>';
	}else if(startDate ==''){
		  alert('กรุณาระบุ ตั้งแต่วันที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'competency/add?token='.$token.'&token1='.$token1; ?>';
	}else if(endDate == ''){	
		  alert('กรุณาระบุ ถึงวันที่ ด้วยค่ะ');
		  location.href = '<?php echo base_url().'competency/add?token='.$token.'&token1='.$token1; ?>';
	}else{
		 
		  $.ajax({
				url: "<?php echo base_url('competency/check_add?action='.$actions) ?>",
				type: 'POST',
				data: {
						evalYear: evalYear,
						evalRound: evalRound,
						startDate: startDate,
						endDate: endDate,
						salary: salary,
						dateDue:dateDue
				}, 
				success: function(response) {
					//Do Something 
						if(response == 'succesfully'){
 							location.replace('<?php echo base_url().'competency'?>');
						}else{
  							location.replace('<?php echo base_url().'competency'?>');
						}
				},
				error: function(xhr) {
					//Do Something to handle error
					alert('กรุณาลองใหม่อีกครั้ง');
					location.replace('<?php echo base_url().'competency' ?>');
				}
				
		 }); //end $.ajax
		  
	} //end if	
 }
  function isNumberKey(evt) {
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			// Added to allow decimal, period, or delete
			 
			if (charCode == 110 || charCode == 190 ) 
				return true;
			
			if (charCode > 31 && (charCode < 48 || charCode > 57 )) 
				return false;
			
			return true;
		} // isNumberKey
</script>