<style type="text/css">
	.form-control {
 	  color: #343232;
	}
</style>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลระดับ</h3>
            </header>
            <div class="panel-body">
                        <link href="<?php echo base_url();?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                        <link href="<?php echo base_url();?>xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                        <link href="<?php echo base_url();?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                        <link href="<?php echo base_url();?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                        <div class="xcrud">
                            <div class="xcrud-container">
                            <div class="xcrud-ajax">
                                <div class="xcrud-view">
                                	<div class="form-horizontal">
                                         
                                        <div class="form-group"><label class="control-label col-sm-3">ชื่อระดับ</label>
                                            	<div class="col-sm-9"><?php  echo $levelText; ?></div></div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">level</label><div class="col-sm-9"><?php  echo $level; ?></div>
                                        </div>        
                                        <div class="form-group"><label class="control-label col-sm-3">คะแนนต่ำสุด</label>
                                                <div class="col-sm-9"><?php  echo $minScore;  ?></div>
                                        </div>
                                        <div class="form-group"><label class="control-label col-sm-3">คะแน่นสูงสุด</label>
                                                <div class="col-sm-9"><?php  echo $maxScore; ?></div>
                                        </div>
                                             
                                    </div>
                                </div>
                                <div class="xcrud-top-actions btn-group">
                                    <a href="<?php echo base_url();?>evaluation/score/?year=<?php echo $year;?>&round=<?php echo $round;?>&formid=<?php echo $formid;?>" data-task="list" class="btn btn-warning xcrud-action" data-primary="1">ย้อนกลับ</a>
                                </div>
                    			<div class="xcrud-nav"></div>
                   			 </div>
                            <div class="xcrud-overlay" style="width: 1330px; opacity: 0.6; display: none;"></div>
                        </div>
            	</div>  
            </div>
        </section>
    </div>
</div>
 