<style type="text/css">
	.bs-example:after {
			position: absolute;
			top: 15px;
			left: 15px;
			font-size: 12px;
			font-weight: 700;
			color: #959595;
			text-transform: uppercase;
			letter-spacing: 1px;
			content: "ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ / จัดออกแบบ competency";
	}

  .panel-table {
    background-color: #FFF;
    padding: 10px;
  }
  .panel-table .loading{
    position:absolute; 
    top:0; left:0; right:0; bottom:0;
    height: 100%; 
    background:rgba(255,255,255,0.5);
    z-index:99
  }
  .panel-table .loading div{
    font-size:40px;
    left:50%;
    top:10%;
    color: #717277;
  }  
  .panel-form-subject {
    padding: 5px;
  }
  section{
    margin-bottom: 15px;
  }
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
             </header>
            <div class="panel-body">
            	<div class="bs-example" data-example-id="horizontal-dl">
                    <dl class="dl-horizontal">
                      <dt>การประเมินผลประจำปี </dt>
                      <dd><?php echo $get_evalYear;?></dd>
                      <dt>ครั้งที่</dt>
                      <dd><?php echo $get_evalRound?></dd>
                      <dt>ตั้วแต่วันที่ </dt>
                      <dd><?php echo  $get_startDate;?>&nbsp;ถึงวันที่&nbsp;<?php echo  $get_endDate ;?></dd>
                      
                      <dt>ชื่อประเมิน</dt>
                      <dd><?php echo $get_evalNameText;?></dd>
                      <dt>กลุ่มพนักงาน</dt>
                      <dd><?php echo $get_empGroup?></dd>
                      <dt>ตั้วแต่ระดับ </dt>
                      <dd><?php echo  $get_startLeval;?>&nbsp;ถึงระดับ&nbsp;<?php echo  $get_endLeval ;?></dd>
                      
                    </dl>
                </div>
            </div>
        </section>
    </div>
</div>
<br />
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading"> 
          <h3>ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ  / จัดออกแบบ / หัวข้อกลุ่มประเมิน competency</h3>
      </header>
      <div class="panel-body">
        <div class="panel-table">
          <div class="loading" style="display: none;">
            <div class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></div>
          </div>
          <table id="assessement-table" cellspacing="0" width="100%" class="table table-hover table-condensed table-striped table-evaluation table-no-border">
            <thead>
              <tr>
                <th class="" style="display: none;"></th>
                <th class="" style="display: none;">></th>
                <th class="col-sm-6">หัวข้อปัจจัยที่ประเมิน</th>
                <th class="">น้ำหนักคะแนน</th>
                <th class="">คะแนนต่ำสุด</th>
                <th class="">คะแนนสูงสุด</th>
                <th class=""></th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th class="" colspan="2" style="display: none;"></th>
                <th class=""><input type="text" class="form-control" name="evalGroupSubjectText"></th>
                <th class="" colspan="4"></th>
              </tr>
              <tr>
                <th colspan="6"><input type="button" class="btn btn-success" data-task="createGroup" value="เพิ่มหัวข้อหลัก"/></th>
              </tr>
            </tfoot>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="panel-form-group" style="display: none;">
          <div class="form-horizontal" id="edit-group" />
            <input type="hidden" name="evalGroupSubjectID"/>
            <div class="form-group">
              <label class="control-label col-sm-3">ชื่อกลุ่มหัวข้อประเมิน*</label>
              <div class="col-sm-9">
                <input class="form-control" data-required="1" type="text" data-type="text" name="evalGroupSubjectText" maxlength="255">
              </div>
            </div>
          </div>
          <div class="btn-group" style="margin-left: 50px">
            <a href="javascript:;" data-task="updateGroup" data-after="list" class="btn btn-primary xcrud-action" data-primary="10">บันทึกและย้อนกลับ</a>
            <a href="javascript:;" data-task="cancelEdit" class="btn btn-warning xcrud-action" data-primary="10">ย้อนกลับ</a>
          </div> 
        </div>
        <div class="panel-form-subject" style="display: none;">
          <div class="form-horizontal" id="edit-subject">
            <input type="hidden" name="evalSubjectID" />
            <div class="form-group">
              <label class="control-label col-sm-3">หังข้อประเมิน*</label>
              <div class="col-sm-9">
                <input class="form-control" data-required="1" type="text" data-type="text" name="evalSubjectText" maxlength="255">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3">คะแนนต่ำสุด</label>
              <div class="col-sm-9">
                <input class="form-control" type="text" data-type="int" name="minScore" data-pattern="integer" maxlength="11">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3">คะแนนสูงสุด</label>
              <div class="col-sm-9">
                <input class="form-control" type="text" data-type="int" name="maxScore" data-pattern="integer" maxlength="11">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3">น้ำหนักคะแนน</label>
              <div class="col-sm-9">
                <input class="form-control" type="text" data-type="int" name="evalWeight" data-pattern="integer" maxlength="11">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3">คำอธิบาย</label>
              <div class="col-sm-9">
                <textarea class="form-control" rows="12" name="evalDesc"></textarea>
              </div>
            </div>
          </div>
          <div class="btn-group" style="margin-left: 50px">
            <a href="javascript:;" data-task="updateSubject" data-after="list" class="btn btn-primary xcrud-action" data-primary="10">บันทึกและย้อนกลับ</a>
            <a href="javascript:;" data-task="cancelEdit" class="btn btn-warning xcrud-action" data-primary="10">ย้อนกลับ</a>
          </div> 
        </div>
      </div>
    </section>
  </div>
</div>

<script src="<?php echo base_url(); ?>editors/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
  tinymce.init({selector:'textarea'});

  var groupButtons = '<span class="btn-group" style="min-width:70px">';
    groupButtons += '<a class="btn btn-warning btn-sm" title="แก้ไข" href="javascript:;" data-task="editGroup"><i class="glyphicon glyphicon-edit"></i></a>';
    groupButtons += '<a class="btn btn-danger btn-sm" title="ลบ" href="javascript:;" data-task="deleteGroup" data-confirm="ต้องการลบใช่หรือไม่?"><i class="glyphicon glyphicon-remove"></i></a>';
    groupButtons += '</span>';

  var subjectButtons = '<span class="btn-group" style="min-width:70px">';
    subjectButtons += '<a class="btn btn-warning btn-sm" title="แก้ไข" href="javascript:;" data-task="editSubject"><i class="glyphicon glyphicon-edit"></i></a>';
    subjectButtons += '<a class="btn btn-danger btn-sm" title="ลบ" href="javascript:;" data-task="deleteSubject" data-confirm="ต้องการลบใช่หรือไม่?"><i class="glyphicon glyphicon-remove"></i></a>';
    subjectButtons += '</span>';

  function refreshTable(){
    $(".panel").css("min-height", function(){ 
      return $(this).height();
    }); 
    $(".loading").show();
    $.getJSON( "<?php echo base_url(); ?>competency/group_ajax", {formid:<?php echo $formid;?>}, function( data ) {
      $( "#assessement-table tbody").html("");
      $.each( data, function( key, valGroup ) {
        //group line
        var groupStr = '<th class="" style="display: none;">'+valGroup.evalGroupSubjectID+'</th>';
            groupStr += '<th class="" style="display: none;"></th>';
            groupStr += '<th colspan="4" class="">'+valGroup.evalGroupSubjectText+'</th>';
            groupStr += '<th class="">'+groupButtons+'</th>';
        $( "<tr/>", {html: groupStr}).appendTo( "#assessement-table" );
        //subject line
        $.each( valGroup.evalSubjects, function( key, valSubject ) {
          var subjectStr = '<td class="" style="display: none;">'+valSubject.evalSubjectID+'</td>';
              subjectStr += '<td class="" style="display: none;">'+valSubject.evalDesc+'</td>';
              subjectStr += '<td class="">'+valSubject.evalSubjectText+'</td>';
              subjectStr += '<td class="">'+valSubject.evalWeight+'</td>';
              subjectStr += '<td class="">'+valSubject.minScore+'</td>';
              subjectStr += '<td class="">'+valSubject.maxScore+'</td>';
              subjectStr += '<td>'+subjectButtons+'</td>';
           $( "<tr/>", {html: subjectStr,class: "subitem","data-parentid": valGroup.evalGroupSubjectID}).appendTo( "#assessement-table" );
        });
        //control lines
        var subjectControlStr = '<td class="" colspan="2" style="display: none;"></td>'
            subjectControlStr += '<td class=""><input type="text" class="form-control" name="evalSubjectText"></td>';
            subjectControlStr += '<td class=""><input type="text" class="form-control" name="evalWeight"></td>';
            subjectControlStr += '<td class=""><input type="text" class="form-control" name="minScore"></td>';
            subjectControlStr += '<td class=""><input type="text" class="form-control" name="maxScore"></td>';
            subjectControlStr += '<td></td>';
        $( "<tr/>", {html: subjectControlStr,class: "subitem-control","data-parentid": valGroup.evalGroupSubjectID}).appendTo( "#assessement-table" );
            subjectControlStr = '<td class="" style="display: none;"></td>';
            subjectControlStr += '<td colspan="5"><input type="button" class="btn btn-success" data-task="createSubject" value="เพิ่มหัวข้อหลัก"/></td>';
        $( "<tr/>", {html: subjectControlStr,class: "subitem-control","data-parentid": valGroup.evalGroupSubjectID}).appendTo( "#assessement-table" );
        
      });
      $(".loading").hide();
      $(".panel").css("min-height", 0);
    }); 	 
  }
  refreshTable();
  $('#assessement-table tbody, #assessement-table tfoot, .panel-form-subject .btn-group, .panel-form-group .btn-group').on( 'click', 'a, input:button', function () {
    switch($(this).data('task')) {
      case "deleteSubject":
        var obj = {};
        if(confirm($(this).data('confirm'))){
          obj["evalSubjectID"] = $(this).closest('tr').children('td:first').html();
          $.post( "<?php echo base_url(); ?>competency/delete_subject", $.param(obj), function( data ) {
            if( data == "succesfully"){
              refreshTable();
            }                
          });
        }
        break;
      case "deleteGroup":
        var obj = {};
        if(confirm($(this).data('confirm'))){
          obj["evalGroupSubjectID"] = $(this).closest('tr').children('th:first').html();
          $.post( "<?php echo base_url(); ?>competency/delete_group", $.param(obj), function( data ) {
            if( data == "succesfully"){
              refreshTable();
            }                
          });
        }
        break;
      case "createGroup":
        var obj = {};
        $('#assessement-table tfoot input[type=text]').map(function(){
            obj[this.name] = this.value;
        }).get();
        obj["evalFormID"] = "<?php echo $formid;?>";
        $.post( "<?php echo base_url(); ?>competency/add_group", $.param(obj), function( data ) {
          if( data == "succesfully"){
            $('#assessement-table tfoot input[type=text]').val("");
            refreshTable();
          }                 
        });
        break;
      case "createSubject":
        var obj = {};
        $(this).closest('tr').prev().find('input[type=text]').map(function(){
            obj[this.name] = this.value;
        }).get();
        obj['evalGroupSubjectID'] = $(this).closest('tr').data('parentid');
        obj["evalDesc"] = "";
        $.post( "<?php echo base_url(); ?>competency/add_subject", $.param(obj), function( data ) {
          if(data == "succesfully"){
            refreshTable();
          }                 
        });
        break;        
        case "editSubject":
          $("#edit-subject input[name=evalSubjectID]").val($(this).closest('tr').children('td:first').html());
          $("#edit-subject input[name=evalDesc]").val($(this).closest('tr').children('td:nth-child(2)').html());
          $("#edit-subject input[name=evalSubjectText]").val($(this).closest('tr').children('td:nth-child(3)').html());
          $("#edit-subject input[name=evalWeight]").val($(this).closest('tr').children('td:nth-child(4)').html());
          $("#edit-subject input[name=minScore]").val($(this).closest('tr').children('td:nth-child(5)').html());
          $("#edit-subject input[name=maxScore]").val($(this).closest('tr').children('td:nth-child(6)').html());
          $(".panel-table").hide();
          $(".panel-form-subject").show();
        break;
        //evalGroupSubjectID
        case "editGroup":
          $("#edit-group input[name=evalGroupSubjectID]").val($(this).closest('tr').children('th:first').html());
          $("#edit-group input[name=evalGroupSubjectText]").val($(this).closest('tr').children('th:nth-child(3)').html());
          $(".panel-table").hide();
          $(".panel-form-group").show();
        break;
        case "cancelEdit":
          $(".panel-form-group").hide();
          $(".panel-form-subject").hide();
          $(".panel-table").show();
        break;
        case "updateSubject":
          var obj = {};
          $('#edit-subject input').map(function(){
              obj[this.name] = this.value;
          }).get();
          $.post( "<?php echo base_url(); ?>competency/update_subject", $.param(obj), function( data ) {
            if( data == "succesfully"){
              $(".panel-form-subject").hide();
              refreshTable();
              $(".panel-table").show();
            }                 
          });
        break;        
        case "updateGroup":
          var obj = {};
          $('#edit-group input').map(function(){
              obj[this.name] = this.value;
          }).get();
          $.post( "<?php echo base_url(); ?>competency/update_group", $.param(obj), function( data ) {
            if( data == "succesfully"){
              $(".panel-form-group").hide();
              refreshTable();
              $(".panel-table").show();
            }                 
          });
        break;  
    }
  });  
</script>
 