<style type="text/css">
	.bs-example:after {
			position: absolute;
			top: 15px;
			left: 15px;
			font-size: 12px;
			font-weight: 700;
			color: #959595;
			text-transform: uppercase;
			letter-spacing: 1px;
			content: "ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ / จัดออกแบบ ";
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
             </header>
            <div class="panel-body">
            	<div class="bs-example" data-example-id="horizontal-dl">
                    <dl class="dl-horizontal">
                      <dt>การประเมินผลประจำปี</dt>
                      <dd><?php echo $get_evalYear;?></dd>
                      <dt>ครั้งที่</dt>
                      <dd><?php echo $get_evalRound?></dd>
                      <dt>ตั้วแต่วันที่ </dt>
                      <dd><?php echo  $get_startDate;?>&nbsp;ถึงวันที่&nbsp;<?php echo  $get_endDate ;?></dd>
                      
                      <dt>ชื่อประเมิน</dt>
                      <dd><?php echo $get_evalNameText;?></dd>
                      <dt>กลุ่มพนักงาน</dt>
                      <dd><?php echo $get_empGroup?></dd>
                      <dt>ตั้วแต่ระดับ </dt>
                      <dd><?php echo  $get_startLeval;?>&nbsp;ถึงระดับ&nbsp;<?php echo  $get_endLeval ;?></dd>
                      
                    </dl>
                </div>
            </div>
        </section>
    </div>
</div>
<br /><div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ  / จัดออกแบบ / หัวข้อกลุ่มประเมิน</h3>
            </header>
            <div class="panel-body">
                <?php echo $data; ?>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
 	 
    $(".xcrud-top-actions").find('.btn-success').html("<i class=\"glyphicon glyphicon-plus-sign\"></i> เพิ่มกลุ่มข้อมูลประเมิน"); 
    $(".xcrud-top-actions").find('.btn-group').append('<a href="javascript:;" data-task="list" class="btn btn btn-primary" onclick="modal_back_back()">ย้อนกลับหน้าจัดออกแบบ</a>');
	jQuery(document).on("xcrudafterrequest",function(event,container){
		//primary
 		//var LabelLicenseDoctor 	=  find(x).text();
		if(Xcrud.current_task == 'save')
		{
		   // Xcrud.show_message(container,'WOW!','success');
		   //alert(Xcrud.current_task);
 		   location.href = '<?php echo base_url(); ?>'+'evaluation/group/?year=<?php echo $year?>&round=<?php echo $round;?>&formid=<?php echo $formid;?>';
		}
		if (Xcrud.current_task == 'list') {
            $(".xcrud-top-actions").find('.btn-success').html("<i class=\"glyphicon glyphicon-plus-sign\"></i> ย้อนกลับหน้าจัดการ"); 
			$(".xcrud-top-actions").find('.btn-group').append('<a href="javascript:;" data-task="list" class="btn btn btn-primary" onclick="modal_back_back()">ย้อนกลับหน้าจัดออกแบบ</a>');
        };
	});
	
	 function modal_back_back(){
        location.href = '<?php echo base_url(); ?>'+'evaluation/eval_desc/?year=<?php echo $year?>&round=<?php echo $round;?>&formid=<?php echo $formid;?>';
     }
</script>
 