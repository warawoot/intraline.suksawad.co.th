<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ  / ระดับคะแนน</h3>
            </header>
                <div class="panel-body">
                <link href="<?php echo site_url();?>xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo site_url();?>xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                <link href="<?php echo site_url();?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                <link href="<?php echo site_url();?>xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css"><div class="xcrud">
                <div class="xcrud-container">
                <div class="xcrud-ajax"> 
                <div class="xcrud-view">
        			<div class="form-horizontal">
                       <div class="form-group">
                        	<label class="control-label col-sm-3">ชื่อระดับ</label>
                            <div class="col-sm-9">
                            	<input class="xcrud-input form-control" type="text" data-type="text" name="dGJsX2V2YWxfZm9ybV9sZXZlbC5sZXZlbFRleHQ-" id="dGJsX2V2YWxfZm9ybV9sZXZlbC5sZXZlbFRleHQ-" maxlength="250" value="<?php echo $levelText;?>">
                            </div>
                       </div>
                       <div class="form-group">
                            <label class="control-label col-sm-3">คะแนนต่ำสุด</label>
                            <div class="col-sm-9">
                                <input class="xcrud-input form-control" type="text" data-type="int" value="<?php echo $minScore;?>" name="dGJsX2V2YWxfZm9ybV9sZXZlbC5taW5TY29yZQ--" id="dGJsX2V2YWxfZm9ybV9sZXZlbC5taW5TY29yZQ--" data-pattern="integer" maxlength="11">
                            </div>
                       </div>
                       <div class="form-group">
                       		<label class="control-label col-sm-3">คะแนนสุงสุด</label>
                            <div class="col-sm-9">
                            	<input class="xcrud-input form-control" type="text" data-type="int" value="<?php echo $maxScore;?>" name="dGJsX2V2YWxfZm9ybV9sZXZlbC5tYXhTY29yZQ--" id="dGJsX2V2YWxfZm9ybV9sZXZlbC5tYXhTY29yZQ--" data-pattern="integer" maxlength="11">
                            </div>
                       </div>
                    </div>
               </div>
        <div class="xcrud-top-actions btn-group">
            <a href="#" data-task="save" data-after="list" class="btn btn-primary"  onclick="on_save()">บันทึกและย้อนกลับ</a><a href="#" data-task="list" class="btn btn-warning" onclick="on_back()">ย้อนกลับ</a></div>
        <div class="xcrud-nav">
            </div>
        </div>
                <div class="xcrud-overlay" style="width: 1096px; opacity: 0.6; display: none;"></div>
            </div>
</div>
<script src="<?php echo site_url();?>xcrud/plugins/jquery.min.js"></script>
<script src="<?php echo site_url();?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo site_url();?>xcrud/plugins/jcrop/jquery.Jcrop.min.js"></script>
<script src="<?php echo site_url();?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo site_url();?>editors/tinymce/tinymce.min.js"></script>
<script src="<?php echo site_url();?>xcrud/plugins/xcrud.js"></script>
            <script type="text/javascript">
            <!--
            
           	var xcrud_config = {"url":"http:\/\/dpo-ehr.smmms.com\/xcrud\/xcrud_ajax.php","editor_url":"http:\/\/dpo-ehr.smmms.com\/editors\/tinymce\/tinymce.min.js","editor_init_url":false,"force_editor":false,"date_first_day":1,"date_format":"dd\/mm\/yy","time_format":"HH:mm:ss","lang":{"add":"\u0e40\u0e1e\u0e34\u0e48\u0e21","edit":"\u0e41\u0e01\u0e49\u0e44\u0e02","view":"\u0e14\u0e39\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25","remove":"\u0e25\u0e1a","duplicate":"Duplicate","print":"\u0e1e\u0e34\u0e21\u0e1e\u0e4c","export_csv":"\u0e19\u0e33\u0e2d\u0e2d\u0e01\u0e40\u0e1b\u0e47\u0e19\u0e44\u0e1f\u0e25\u0e4c CSV","search":"\u0e04\u0e49\u0e19\u0e2b\u0e32","go":"\u0e15\u0e01\u0e25\u0e07","reset":"\u0e23\u0e35\u0e40\u0e0b\u0e47\u0e15","save":"\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01","save_return":"\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01\u0e41\u0e25\u0e30\u0e22\u0e49\u0e2d\u0e19\u0e01\u0e25\u0e31\u0e1a","save_new":"\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01\u0e41\u0e25\u0e30\u0e40\u0e1e\u0e34\u0e48\u0e21\u0e43\u0e2b\u0e21\u0e48","save_edit":"\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01\u0e41\u0e25\u0e30\u0e41\u0e01\u0e49\u0e44\u0e02","return":"\u0e22\u0e49\u0e2d\u0e19\u0e01\u0e25\u0e31\u0e1a","modal_dismiss":"\u0e1b\u0e34\u0e14","add_image":"\u0e40\u0e1e\u0e34\u0e48\u0e21\u0e23\u0e39\u0e1b\u0e20\u0e32\u0e1e","add_file":"\u0e40\u0e1e\u0e34\u0e48\u0e21\u0e44\u0e1f\u0e25\u0e4c","exec_time":"Execution time:","memory_usage":"Memory usage:","bool_on":"Yes","bool_off":"No","no_file":"no file","no_image":"no image","null_option":"- none -","total_entries":"\u0e08\u0e33\u0e19\u0e27\u0e19\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25 :","table_empty":"\u0e44\u0e21\u0e48\u0e1e\u0e1a\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25","all":"\u0e17\u0e31\u0e49\u0e07\u0e2b\u0e21\u0e14","deleting_confirm":"\u0e15\u0e49\u0e2d\u0e07\u0e01\u0e32\u0e23\u0e25\u0e1a\u0e43\u0e0a\u0e48\u0e2b\u0e23\u0e37\u0e2d\u0e44\u0e21\u0e48?","undefined_error":"\u0e40\u0e01\u0e34\u0e14\u0e02\u0e49\u0e2d\u0e1c\u0e34\u0e14\u0e1e\u0e25\u0e32\u0e14\u0e1a\u0e32\u0e07\u0e2d\u0e22\u0e48\u0e32\u0e07...","validation_error":"\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25\u0e1a\u0e32\u0e07\u0e1f\u0e34\u0e25\u0e14\u0e4c\u0e44\u0e21\u0e48\u0e16\u0e39\u0e01\u0e15\u0e49\u0e2d\u0e07","image_type_error":"\u0e44\u0e21\u0e48\u0e23\u0e2d\u0e07\u0e23\u0e31\u0e1a\u0e44\u0e1f\u0e25\u0e4c\u0e20\u0e32\u0e1e\u0e19\u0e32\u0e21\u0e2a\u0e01\u0e38\u0e25\u0e19\u0e35\u0e49","unique_error":"Some fields are not unique.","your_position":"Your position","search_here":"Search here...","all_fields":"[\u0e04\u0e49\u0e19\u0e08\u0e32\u0e01\u0e17\u0e38\u0e01\u0e1f\u0e34\u0e25\u0e14\u0e4c]","choose_range":"- \u0e01\u0e23\u0e38\u0e13\u0e32\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e0a\u0e48\u0e27\u0e07 -","next_year":"\u0e1b\u0e35\u0e2b\u0e19\u0e49\u0e32","next_month":"\u0e40\u0e14\u0e37\u0e2d\u0e19\u0e2b\u0e19\u0e49\u0e32","today":"\u0e27\u0e31\u0e19\u0e19\u0e35\u0e49","this_week_today":"\u0e2a\u0e31\u0e1b\u0e14\u0e32\u0e2b\u0e4c\u0e19\u0e35\u0e49\u0e08\u0e19\u0e16\u0e36\u0e07\u0e27\u0e31\u0e19\u0e19\u0e35\u0e49","this_week_full":"\u0e17\u0e31\u0e49\u0e07\u0e2a\u0e31\u0e1b\u0e14\u0e32\u0e2b\u0e4c\u0e19\u0e35\u0e49","last_week":"\u0e2a\u0e31\u0e1b\u0e14\u0e32\u0e2b\u0e4c\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","last_2weeks":"2 \u0e2a\u0e31\u0e1b\u0e14\u0e32\u0e2b\u0e4c\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","this_month":"\u0e40\u0e14\u0e37\u0e2d\u0e19\u0e19\u0e35\u0e49","last_month":"\u0e40\u0e14\u0e37\u0e2d\u0e19\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","last_3months":"3 \u0e40\u0e14\u0e37\u0e2d\u0e19\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","last_6months":"6 \u0e40\u0e14\u0e37\u0e2d\u0e19\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27","this_year":"\u0e20\u0e32\u0e22\u0e43\u0e19\u0e1b\u0e35\u0e19\u0e35\u0e49","last_year":"\u0e1b\u0e35\u0e17\u0e35\u0e48\u0e41\u0e25\u0e49\u0e27"},"rtl":0};
                            
            -->
            </script><script src="<?php echo site_url();?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script><script src="<?php echo site_url();?>xcrud/languages/timepicker/jquery-ui-timepicker-th.js"></script>
            </div>     
        </section>
    </div>
</div> 
<script type="text/javascript">
 

function on_back(){
   		 location.href = '<?php echo base_url(); ?>evaluation/score/?formid=<?php echo $formid;?>&year=<?php echo $year;?>&round=<?php echo $round;?>';
}
	  		
function on_save(){
 	 	 
 		var varname			= $('#dGJsX2V2YWxfZm9ybV9sZXZlbC5sZXZlbFRleHQ-').val();
		var varminScore		= $('#dGJsX2V2YWxfZm9ybV9sZXZlbC5taW5TY29yZQ--').val(); 
		var varmaxScore		= $('#dGJsX2V2YWxfZm9ybV9sZXZlbC5tYXhTY29yZQ--').val();
 		
		 if(varname == '' ){
			 alert('กรุณาเลือก อัตราที่ ด้วยค่ะ');
			 document.getElementById("dGJsX2V2YWxfZm9ybV9sZXZlbC5sZXZlbFRleHQ-").focus();
 			 
		 }else{
			  $.ajax({
 					url: "<?php echo base_url('evaluation/check_scores') ?>",
					type: 'POST',  
					data: {
							name: varname,
							minScore: varminScore,
							maxScore: varmaxScore ,
							formid: <?php echo $formid;?> ,
							year: <?php echo $year;?> ,
							action:'<?php echo $action;?>'   
					},
					success: function(response) {
						//Do Something 
						 
						if(response == 'succesfully'){
 							 location.replace('<?php echo base_url(); ?>evaluation/score/?year=<?php echo $year;?>&formid=<?php echo $formid;?>&round=<?php echo $round;?>');
						}else{
 							 location.replace('<?php echo base_url(); ?>evaluation/score/?year=<?php echo $year;?>&formid=<?php echo $formid;?>&round=<?php echo $round;?>'); 
						}
 						 
					},
					error: function(xhr) {
						//Do Something to handle error
						//alert('error');
						alert('กรุณาลองใหม่อีกครั้ง');
 						  location.replace('<?php echo base_url(); ?>evaluation/eval_desc/?year=<?php echo $year;?>&round=<?php echo $round;?>'); 
					}
				});	 
 		}
				
}

	
</script>
