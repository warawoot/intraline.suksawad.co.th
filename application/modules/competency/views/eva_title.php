<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลประเมินผล / แบบประเมินผลการปฎิบัติ  / จัดออกแบบ / หัวข้อประเมิน</h3>
            </header>
            <div class="panel-body">
                <?php echo $data; ?>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
	 $(".xcrud-top-actions").find('.btn-success').html("<i class=\"glyphicon glyphicon-plus-sign\"></i> เพิ่มข้อมูลประเมิน"); 
	 $(".xcrud-top-actions").find('.btn-group').append('<a href="javascript:;" data-task="list" class="btn btn btn-primary" onclick="modal_back_back()">ย้อนกลับหน้ากลุ่มประเมิน</a>');
	 function modal_back_back(){
        location.href = '<?php echo base_url(); ?>'+'evaluation/group/?year=<?php echo $year?>&round=<?php echo $round;?>&formid=<?php echo $formid;?>';
     }
	jQuery(document).on("xcrudafterrequest",function(event,container){
		//primary
 		//var LabelLicenseDoctor 	=  find(x).text();
		if(Xcrud.current_task == 'save')
		{
		   // Xcrud.show_message(container,'WOW!','success');
		   //alert(Xcrud.current_task);
 		    //location.href = '<?php echo base_url(); ?>'+'evaluation/evalsubject/?year=<?php echo $year?>&round=<?php echo $round;?>&formid=<?php echo $formid;?>'; 
			location.href = '<?php echo base_url(); ?>'+'evaluation/evalsubject/?gsid=<?php echo $gsid;?>&formid=<?php echo $formid;?>&year=<?php echo $year?>&round=<?php echo $round;?>';
		}
		 if (Xcrud.current_task == 'list') {
            $(".xcrud-top-actions").find('.btn-group').append('<a href="javascript:;" data-task="list" class="btn btn btn-primary" onclick="modal_back_back()">ย้อนกลับหน้ากลุ่มประเมิน</a>');
        };
	});
</script>
 