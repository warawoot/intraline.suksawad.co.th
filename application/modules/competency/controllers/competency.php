<?php

class Competency extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('evaluation_model');
		 
		 
    }
  
 	 
    public function index() {
 		
 		$xcrud	=	xcrud_get_instance();
        $xcrud->table('tbl_eval_competency_date'); 
        /*,"salary"=>"ประเมินผล / เลื่อนเงินเดือน"*/
        $arr		=	array("evalYear"=>"การประเมินประจำปี","evalRound"=>"ครั้งที่","salary"=>"ประเมินผล","startDate"=>"ตั้งแต่วันที่","endDate"=>"ถึงวันที่" ,"dateDue"=>"วันครบรอบกำหนดการประเมิน" );
        $xcrud->change_type('salary','radio','',array('values'=>array('1'=>'ประเมินผล' ,'2'=>'เลื่อนเงินเดือน '  )));
 		$xcrud->validation_required('evalYear')->validation_required('evalRound')->validation_required('startDate')->validation_required('endDate')  ;
		
		$xcrud->button(site_url().'competency/eval_desc/?year={evalYear}&round={evalRound}','coming soon ','fa fa-folder-open');  
 		$xcrud->button(site_url().'competency/edit?token={evalYear}&token1={evalRound}&action=edit','','warning glyphicon glyphicon-edit','btn-warning');
 
		$xcrud->column_callback('startDate','DateThai');
		$xcrud->column_callback('endDate','DateThai');
		$xcrud->column_callback('dateDue','DateThai');
		$xcrud->column_callback('salary','eval_date_salary_competency');
		
        $xcrud->label($arr);
        $xcrud->columns($arr); 
		 
        $xcrud->fields($arr); 
         
		$xcrud->hide_button('edit');  
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
   	
        $data['data']	=	$xcrud->render();
 		 
        $this->template->load('template/admin', 'evaluation',$data);
 		 
     }
	
	public function add() {
		
		$data['date_start']	= '';
		$data['date_end']	= '';
		$data['salary']		= '';
		$data['date_due']		= '';
		$evalYear	= $this->input->get('token');
		$evalRound	= $this->input->get('token1');
		$data['actions'] 	= 'add';
 		$data['token']	 	= '';
		$data['data']	 	= '';
		
		
		$data['token']		= $evalYear; 
		$data['token1']		= $evalRound; 
		 
  		$data['get_data'] = $this->evaluation_model->get_date_edit($evalYear,$evalRound); 
        $this->template->load('template/admin', 'evaluation_form',$data);
		
    }
   
	public function edit() {
		
		$evalYear	= $this->input->get('token');
		$evalRound	= $this->input->get('token1');
		
  		$data['get_data'] = $this->evaluation_model->get_date_edit($evalYear,$evalRound);  
		
		$convertStart		= $data['get_data']['startDate'];
		$convertEnd			= $data['get_data']['endDate'];
		$salary				= $data['get_data']['salary'];
		$dateDue			= $data['get_data']['dateDue'];
		
		$date_start 		= explode('-',$convertStart);  
		$date_end 	  		= explode('-',$convertEnd);
		$date_due 	  		= explode('-',$dateDue);
		  
		$strYear_s 			= $date_start[2];
		$strMonth_s			= $date_start[1];
		$strDay_s			= $date_start[0];
		
		$strYear_e 			= $date_end[2];
		$strMonth_e			= $date_end[1];
		$strDay_e			= $date_end[0];
		
		$strYear_d 			= $date_due[2];
		$strMonth_d			= $date_due[1];
		$strDay_d			= $date_due [0];
		
		$date_start			= $strYear_s."/".$strMonth_s."/".$strDay_s;
		$date_end			= $strYear_e."/".$strMonth_e."/".$strDay_e;
		$date_start			= $strYear_s."/".$strMonth_s."/".$strDay_s;
		$date_due			= $strYear_d."/".$strMonth_d."/".$strDay_d;
		
        $data['token']		= '';
		$data['actions'] 	= 'edit';
		$data['data']		= ''; 
		
		$data['token']		= $evalYear; 
		$data['token1']		= $evalRound; 
 		
		$data['date_start']		= $date_start; 
		$data['date_end']		= $date_end; 
		
		$data['date_due']		= $date_due; 
		
        $this->template->load('template/admin', 'evaluation_form',$data);
    } 
	 
	 public function view() {
          
		  
    }
	 
	public function delete() {
  		
        $max 	= $this->organization_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
 		}else{
			$max_id  =  $max+1;
   		}
		
		
		$token	= $this->input->get('token');
		$orgID	= $this->input->get('orgID');
		
		if($token == ''){
			redirect(site_url().'appoint', 'refresh');
			exit();
		} 
		
  		$data['token']	= $token; 
		$data['button_add']	= 'delete'; 
 		$data['data'] = $this->organization_model->delete_orgchart($orgID,$token) ;
		 
        $this->template->load('template/admin', 'organization',$data);
    }
	  
	
	function check_search(){
		
		$s_search	= $this->input->get_post('search_c');
		$orgID		= $this->input->get_post('orgID');
		$assignID	= $this->input->get_post('assignID');
		$fields		= $this->input->get_post('fields');
		//$search = $this->organization_model->search_all($search ,$orgID , $assignID);
		
		$token	= $this->input->get('token');
		 		
 		$data['print_data_tree'] = $this->organization_model->getCategorySearch($level = 0, $prefix = '' , $assignID , $s_search ,$fields);
		 
		echo $data['print_data_tree'];
			
	}
	
    public function check_add() {
		
		$evalYear		= $this->input->get_post("evalYear");
		$evalRound 		= $this->input->get_post("evalRound");
		$startDate		= $this->input->get_post("startDate");
		$endDate		= $this->input->get_post("endDate"); 
		$action 		= $this->input->get_post('action');
		$salary 		= $this->input->get_post('salary');
		$dateDue 		= $this->input->get_post('dateDue');
		// format date : 2015-05-01
		$date_start 		= explode('/',$startDate);  
		$date_end 	  		= explode('/',$endDate);
		$dateDue 	  		= explode('/',$dateDue);
		  
		$strYear_s 			= $date_start[2];
		$strMonth_s			= $date_start[1];
		$strDay_s			= $date_start[0];
		
		$strYear_e 			= $date_end[2];
		$strMonth_e			= $date_end[1];
		$strDay_e			= $date_end[0];
		
		$strYear_d			= $dateDue[2];
		$strMonth_d			= $dateDue[1];
		$strDay_d			= $dateDue[0];
		
		$date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s;
		$date_end			= $strYear_e."-".$strMonth_e."-".$strDay_e;
		$date_due			= $strYear_d."-".$strMonth_d."-".$strDay_d;
		 
  		$data	=	array( 
		
 				"evalYear"=>$evalYear,
				"evalRound"=>$evalRound ,
 				"startDate"=>$date_start,
				"endDate"=>$date_end  ,
				"salary "=>1    ,
				"dateDue "=>$date_due 
		
		);
		
		$data2	=	array( 
		
 				//"evalYear"=>$evalYear,
				//"evalRound"=>$evalRound ,
 				"startDate"=>$date_start,
				"endDate"=>$date_end  ,
				"salary "=>1  ,
				"dateDue "=>$date_due 
		
		);
		
  
		if($action == 'edit'){
			$insert_data = $this->evaluation_model->update_data($evalYear,$evalRound,$data2); 
 		}elseif($action == 'add'){
			$insert_data = $this->evaluation_model->insert_evaluation($data); 	
 		}
  		 
 		 
		if($insert_data == 'succesfully'){
				echo  'succesfully';
		}else{
				echo 'failed';
		}									
         
	}  
 
 	
	/// Add New Menu  //
	
	function eval_desc(){
 		// Get parameter //
		$round	= $this->input->get_post("round");
		$year	= $this->input->get_post("year");
		
		// load model //
		$eval_date		= $this->evaluation_model->get_eva_form_desc($round,$year);
		$data['get_evalYear']	= $eval_date['evalYear'];
		$data['get_evalRound']	= $eval_date['evalRound'];
		$data['get_startDate']	= DateThaiHelper($eval_date['startDate']);
		$data['get_endDate']	= DateThaiHelper($eval_date['endDate']);
 		
	    // load xcrud //
		$xcrud	= xcrud_get_instance();
 		 
        $xcrud->table('tbl_eval_competency_form'); 
		$xcrud->where('evalRound ', $round);
		$xcrud->where('evalYear ', $year);
		$arr_list	=	array( "empGroup"=>"กลุ่มพนักงาน" ,"evalNameText"=>"ชื่อแบบประเมิน" ,"evalFullscore"=>"น้ำหนักรวม","startLeval"=>"ระดับเริ่มต้น","endLeval"=>"ระดับสิ้นสุด");
        $arr		=	array( "empGroup"=>"กลุ่มพนักงาน" ,"evalNameText"=>"ชื่อแบบประเมิน" ,"evalFullscore"=>"น้ำหนักรวม","startLeval"=>"ระดับเริ่มต้น","endLeval"=>"ระดับสิ้นสุด" ,"AssesSor"=>"ผู้ประเมิน","Commander"=>"ผู้บังคับบัญชาเหนือขึ้นไป","Endorsee"=>"ผู้รับรอง");
  		$xcrud->relation('startLeval','tbl_rank','rankID','rankName');
		$xcrud->relation('endLeval','tbl_rank','rankID','rankName');
		$xcrud->relation('empGroup','tbl_staff_type','typeID','typeName');
 		
 		$xcrud->change_type('evalType','radio','',array('values'=>array('1'=>'ประเมินพฤติกรรม' ,'2'=>'ประเมิน KPI/Competency '  )));
		$xcrud->change_type('AssesSor','select','',array('values'=>array('0'=>'ไม่กำหนด' ,'6'=>'ผู้บังคับบัญชาระดับแผนก','7'=>'ผู้บังคับบัญชาระดับกอง','8'=>'ผู้บังคับบัญชาระดับฝ่าย','9'=>'รองผู้อำนวยการ','10'=>'ผู้อำนวยการ' )));
		$xcrud->change_type('Commander','select','',array('values'=>array('0'=>'ไม่กำหนด' ,'6'=>'ผู้บังคับบัญชาระดับแผนก','7'=>'ผู้บังคับบัญชาระดับกอง','8'=>'ผู้บังคับบัญชาระดับฝ่าย','9'=>'รองผู้อำนวยการ','10'=>'ผู้อำนวยการ' )));
		$xcrud->change_type('Endorsee','select','',array('values'=>array('0'=>'ไม่กำหนด' ,'6'=>'ผู้บังคับบัญชาระดับแผนก','7'=>'ผู้บังคับบัญชาระดับกอง','8'=>'ผู้บังคับบัญชาระดับฝ่าย','9'=>'รองผู้อำนวยการ','10'=>'ผู้อำนวยการ' )));
 		
		$xcrud->button(site_url().'competency/group/?year={evalYear}&round={evalRound}&formid={evalFormID}','ชื่อประเมิน','fa fa-folder-open');  
		//$xcrud->button(site_url().'competency/score/?year={evalYear}&round={evalRound}&formid={evalFormID}','ระดับคะแนน ','fa fa-star','btn-success');  
 		//$xcrud->button(site_url().'evaluation/descedit?token={evalYear}&token1={evalRound}&action=edit','','warning glyphicon glyphicon-edit','btn-warning');
 		// -- validation_required -- 
		//$xcrud->validation_required('job_name') ;
		$xcrud->validation_required('empGroup')->validation_required('evalNameText')->validation_required('evalFullscore') ->validation_required('startLeval') ->validation_required('endLeval') ->validation_required('evalType')  ;
		//$xcrud->before_insert('hash_assigndate1', 'functions_eval.php'); // manualy load
		 
 		$xcrud->pass_var('evalRound',$round,'create'); // insert to table
 		$xcrud->pass_var('evalYear',$year,'create');   // insert to table
		$xcrud->pass_var('evalType',1,'create');
		
        $xcrud->label($arr);
        $xcrud->columns($arr_list);//แสดงในตางราง $xcrud->columns('userName');  
 		 
		//$xcrud->before_remove('delete_user_data'); 
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
          
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
       
		$data['round']	=	$round;
		$data['year']	=	$year; 
        
		$data['data']	=	$xcrud->render();
        $this->template->load('template/admin', 'eva_form_desc',$data);	
		//echo "  coming soon  evaluation_form_desc";	
 		
	}

 
	
	function score(){
 		// Get parameter //
		$round	= $this->input->get_post("round");
		 
		$year	= $this->input->get_post("year");
		$formid	= $this->input->get_post("formid");
		// load model //
		$eval_date		= $this->evaluation_model->get_eva_form_desc($round,$year);
		$data['get_evalYear']	= $eval_date['evalYear'];
		$data['get_evalRound']	= $eval_date['evalRound'];
		$data['get_startDate']	= DateThaiHelper($eval_date['startDate']);
		$data['get_endDate']	= DateThaiHelper($eval_date['endDate']);
 		
	    // load xcrud //
		$xcrud	= xcrud_get_instance();
 		 
        $xcrud->table('tbl_eval_form_level'); 
		$xcrud->where('evalFormID ', $formid);
        $arr		=	array( "evalFormID"=>"ชื่อประเมิน" ,"levelID"=>"level" ,"levelText"=>"ชื่อระดับ","minScore"=>"คะแนนต่ำสุด","maxScore"=>"คะแนนสุงสุด");
  		$xcrud->relation('evalFormID','tbl_eval_form','evalFormID','evalNameText');
		// modify button new //
   		$xcrud->button(site_url().'evaluation/level_edit?formid={evalFormID}&level={levelID}&round='.$round.'&year='.$year.'&action=edit','','warning glyphicon glyphicon-edit','btn-warning');
		$xcrud->button(site_url().'evaluation/level_view?formid={evalFormID}&level={levelID}&round='.$round.'&year='.$year.'&action=edit','','glyphicon glyphicon-search','btn-info');
 		$xcrud->button('javascript:myFunction({evalFormID},{levelID});','','danger glyphicon glyphicon-remove','btn-danger');
		
 		// -- validation_required -- 
		//$xcrud->validation_required('job_name') ;
		$xcrud->validation_required('ชื่อระดับ');
		//$xcrud->before_insert('hash_assigndate1', 'functions_eval.php'); // manualy load
		 
 		$xcrud->pass_var('evalFormID',$formid,'create'); // insert to table
 		//$xcrud->pass_var('evalYear',$year,'create');   // insert to table
		
        $xcrud->label($arr);
        $xcrud->columns($arr);//แสดงในตางราง $xcrud->columns('userName');  
 		 
		//$xcrud->before_remove('delete_user_data'); 
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
        $xcrud->hide_button('view'); 
		$xcrud->hide_button('remove');
		$xcrud->hide_button('edit');   
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
       
		$data['round']	=	$round;
		$data['year']	=	$year; 
        $data['formid']	=	$formid	 ; 
		$data['data']	=	$xcrud->render();
        $this->template->load('template/admin', 'eva_form_desc_level',$data);	
 		
	}
	
	public function scores(){
 		// Get parameter //
		$round	= $this->input->get_post("round");
		$year	= $this->input->get_post("year");
		$formid	= $this->input->get_post("formid");
		
		// varname null //
		$data['formid']	= '';
		$data['level']	= '';
		$data['levelText']	= '';
		$data['minScore']	= '';
		$data['maxScore']	= '';
		
		$data['round']	=	$round;
		$data['year']	=	$year;  
		$data['formid']	=	$formid;  
		$data['action']	= 'add';
		$data['data']	=	'';
        $this->template->load('template/admin', 'eva_form_desc_level_save',$data);	
		//echo "  coming soon  evaluation_form_desc";	
 	}
	
	public function level_edit(){
 		// Get parameter //
		$round		= $this->input->get_post("round");
		$year		= $this->input->get_post("year");
		$formid		= $this->input->get_post("formid");
		$level		= $this->input->get_post("level");
 		$action		= $this->input->get_post("action");
		
		$eval_edit		= $this->evaluation_model->get_eva_level($formid, $level);
 		
		if(is_array($eval_edit)){
 			$data['formid']		= $eval_edit['formid'];
			$data['level']		= $eval_edit['levelID'];
			$data['levelText']	= $eval_edit['levelText'];
			$data['minScore']	= $eval_edit['minScore'];
			$data['maxScore']	= $eval_edit['maxScore'];
  		}else{
			$data['formid']		= '';
			$data['level']		= '';
			$data['levelText']	= '';
			$data['minScore']	= '';
			$data['maxScore']	= '';
 		}
 		$data['action']	=	$action;
		$data['round']	=	$round;
		$data['year']	=	$year;  
		$data['formid']	=	$formid;  
		$data['data']	=	'';
        $this->template->load('template/admin', 'eva_form_desc_level_save',$data);	
		//echo "  coming soon  evaluation_form_desc";	
 		
	}
	
	public function level_view(){
 		// Get parameter //
		$round		= $this->input->get_post("round");
		$year		= $this->input->get_post("year");
		$formid		= $this->input->get_post("formid");
		$level		= $this->input->get_post("level");
 		
		$eval_edit		= $this->evaluation_model->get_eva_level($formid, $level);
 
		if(is_array($eval_edit)){
 			$data['formid']		= $eval_edit['formid'];
			$data['level']		= $eval_edit['levelID'];
			$data['levelText']	= $eval_edit['levelText'];
			$data['minScore']	= $eval_edit['minScore'];
			$data['maxScore']	= $eval_edit['maxScore'];
 		}else{
			$data['formid']		= '';
			$data['level']		= '';
			$data['levelText']	= '';
			$data['minScore']	= '';
			$data['maxScore']	= '';
		}
 		
		$data['round']	=	$round;
		$data['year']	=	$year;  
		$data['formid']	=	$formid;  
		$data['data']	=	'';
        $this->template->load('template/admin', 'eval_view',$data);	
		//echo "  coming soon  evaluation_form_desc";	
 		
	}
	
	public function level_delete(){
 		// Get parameter //
		$round		= $this->input->get_post("round");
		$year		= $this->input->get_post("year");
		$formid		= $this->input->get_post("formid");
		$level		= $this->input->get_post("level");
		
 		
		$eval_delete		= $this->evaluation_model->delete_eva_level($formid, $level);
 
		if($eval_delete =='succesfully'){ 
 			 redirect(site_url().'evaluation/score/?year='.$year.'&formid='.$formid.'&round='.$round.'', 'refresh');
			 exit();
 		}elseif($eval_delete =='failed'){
			  redirect(site_url().'evaluation/score/?year='.$year.'&formid='.$formid.'&round='.$round.'', 'refresh');
			 exit();
			 
		}
 		 
 		
	}
	
	
	public function check_scores(){
		
		$name		= $this->input->post("name");
		$minScore	= $this->input->post("minScore");
		$maxScore	= $this->input->post("maxScore");
		$rounds		= $this->input->post("rounds");
		$year		= $this->input->post("year"); 
		$formid		= $this->input->post("formid"); 
		$action		= $this->input->get_post("action");
 		 
		$count_levelID	= $this->evaluation_model->get_count_eval_form_level($formid);
		$data_in		= array("evalFormID"=>$formid,"levelID"=>$count_levelID,"levelText"=>$name,"minScore"=>$minScore,"maxScore"=>$maxScore);
		$data_up		= array("levelText"=>$name,"minScore"=>$minScore,"maxScore"=>$maxScore);
		
 		if($action == 'add'){
			$this->evaluation_model->insert_eval_form_level($data_in);
		}else if($action == 'edit'){
 			$this->evaluation_model->update_eva_level($formid , $count_levelID-1 ,$data_up);
 		}
		
        echo json_encode($action);
 		
	}
	
 	public function group_old(){
		// Get parameter //
		
   		$round	= $this->input->get_post("round");
		$year	= $this->input->get_post("year"); 
		$formid	= $this->input->get_post("formid"); 
 		
		// load model  get_eva_group //
			/*
				function :  array ( key0 => value0, key1 => value1, ..., keyn => valuen );
				return type :  array
				content :  ใช้สร้างตัวแปรแบบ array โดยมีการกำหนดค่าข้อมูลไว้แล้ว
				และมีการกำหนด key ด้วย
					
 				example :  $arr = array ( "domain"=>"bamboo", "protocol"=>"http" );
				echo $arr [ "domain" ];
  			*/
 		//
		$eval_geoup		= $this->evaluation_model->get_eva_group($round , $year ,$formid); // return Array
		
		$data['get_evalYear']	= $eval_geoup['evalYear']; // Get ค่า Array 
		$data['get_evalRound']	= $eval_geoup['evalRound'];
		$data['get_startDate']	= DateThaiHelper($eval_geoup['startDate']);
		$data['get_endDate']	= DateThaiHelper($eval_geoup['endDate']);
		
		$data['get_evalNameText']	= $eval_geoup['evalNameText'];
		$data['get_empGroup']	= $eval_geoup['endLeval'];
		$data['get_startLeval']	= $eval_geoup['startLeval'];
		$data['get_endLeval']	= $eval_geoup['endLeval'];
		
 		// load xcrud //		
		$xcrud	=	xcrud_get_instance();
 		 
    $xcrud->table('tbl_eval_group_subject'); 
		//$xcrud->where('evalRound ', $round);
		//$xcrud->where('evalYear ', $year);
		$xcrud->where('evalFormID ', $formid);
        $arr		=	array( "evalGroupSubjectText"=>"ชื่อกลุ่มหัวข้อประเมิน");
  		$xcrud->column_cut(200,'evalGroupSubjectText'); 
		$xcrud->button(site_url().'evaluation/evalsubject/?gsid={evalGroupSubjectID}&formid={evalFormID}&year='.$year.'&round='.$round.'','coming soon ','fa fa-folder-open');  
 		//$xcrud->button(site_url().'evaluation/editgroup?token={evalYear}&token1={evalRound}&action=edit','','warning glyphicon glyphicon-edit','btn-warning');
 		// -- validation_required -- 
		//$xcrud->validation_required('job_name') ;
 		//$xcrud->pass_var('job_id',$max_id,'create');
		$xcrud->validation_required('evalGroupSubjectText') ;
 		//$xcrud->pass_var('evalRound',$round,'create');
 		//$xcrud->pass_var('evalYear',$year,'create');
		$xcrud->pass_var('evalFormID',$formid,'create');
		//$xcrud->pass_var('formid',$formid,'create');  
        $xcrud->label($arr);
        $xcrud->columns($arr);//แสดงในตางราง $xcrud->columns('userName');  
 		 
		//$xcrud->before_remove('delete_user_data'); 
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
		$data['round']	=	$round;
		$data['year']	=	$year; 
		$data['formid']	=	$formid; 
		
		 
        $data['data']	=	$xcrud->render();
   		
		$this->template->load('template/admin', 'eva_group',$data);
 		
	}

	public function evalsubject(){
		
		$round	= $this->input->get_post("round");
		$year	= $this->input->get_post("year");
		$gsid	= $this->input->get_post("gsid");
		$formid	= $this->input->get_post("formid"); 
		
		$xcrud	=	xcrud_get_instance();
 		 
        $xcrud->table('tbl_eval_subject'); 
		//$xcrud->where('evalRound ', $round);
		//$xcrud->where('evalYear ', $year);
		//$xcrud->where('evalFormID ', $formid);
		$xcrud->where('evalGroupSubjectID ',$gsid);
        $arr		=	array( "evalSubjectText"=>"หังข้อประเมิน","minScore"=>"คะแนนต่ำสุด","maxScore"=>"คะแนนสูงสุด","evalWeight"=>"น้ำหนักคะแนน","evalDesc"=>"คำอธิบาย");
   		$xcrud->validation_required('evalSubjectText') ;
		//$xcrud->pass_var('evalRound',$round,'create');
 		//$xcrud->pass_var('evalYear',$year,'create'); 
		$xcrud->pass_var('evalGroupSubjectID',$gsid,'create'); 
		//$xcrud->pass_var('evalFormID',$formid,'create');
		$xcrud->column_cut(200,'evalSubjectText'); 
		$xcrud->label($arr);
        $xcrud->columns($arr);
		
        $xcrud->fields($arr);
 		
		$data['round']	=	$round;
		$data['year']	=	$year;
		$data['gsid']	=	$gsid;
		$data['formid']	=	$formid;
		
		$data['data']	=	$xcrud->render();
 		$this->template->load('template/admin', 'eva_title',$data);
	}

//-------> New AJAX Form
   public function group(){
   		$round	= $this->input->get_post("round");
		$year	= $this->input->get_post("year"); 
		$formid	= $this->input->get_post("formid"); 

		$data['round']	=	$round;
		$data['year']	=	$year; 
		$data['formid']	=	$formid;
		$eval_geoup		= $this->evaluation_model->get_eva_group($round , $year ,$formid); // return Array
		
		$data['get_evalYear']	= $eval_geoup['evalYear']; // Get ค่า Array 
		$data['get_evalRound']	= $eval_geoup['evalRound'];
		$data['get_startDate']	= DateThaiHelper($eval_geoup['startDate']);
		$data['get_endDate']	= DateThaiHelper($eval_geoup['endDate']);
		
		$data['get_evalNameText']	= $eval_geoup['evalNameText'];
		$data['get_empGroup']	= $eval_geoup['endLeval'];
		$data['get_startLeval']	= $eval_geoup['startLeval'];
		$data['get_endLeval']	= $eval_geoup['endLeval'];
		$this->template->load('template/admin', 'eva_group',$data);
 	}
		// Get parameter //
	public function group_ajax(){	
		$formid	= $this->input->get_post("formid"); 
		echo json_encode($this->evaluation_model->get_eva_subject_and_group($formid));
	}
	public function add_group() {
		$data["evalFormID"] = $this->input->get_post("evalFormID");
		$data["evalGroupSubjectText"] = $this->input->get_post("evalGroupSubjectText"); 
		echo ($this->evaluation_model->insert_group($data));
	}
	public function update_group() {
		$data["evalGroupSubjectID"] = $this->input->get_post("evalGroupSubjectID");
		$data["evalGroupSubjectText"] = $this->input->get_post("evalGroupSubjectText"); 
		echo ($this->evaluation_model->update_group($data));
	}
	public function delete_group() { 
		$evalGroupSubjectID = $this->input->get_post("evalGroupSubjectID");
		echo ($this->evaluation_model->delete_group($evalGroupSubjectID));
	}	
	public function add_subject() { 
		$data["evalDesc"] = $this->input->get_post("evalDesc");
		$data["evalGroupSubjectID"] = $this->input->get_post("evalGroupSubjectID"); 
		$data["evalSubjectText"] = $this->input->get_post("evalSubjectText");
		$data["evalWeight"] = $this->input->get_post("evalWeight"); 
		$data["minScore"] = $this->input->get_post("minScore");
		$data["maxScore"] = $this->input->get_post("maxScore");
		echo ($this->evaluation_model->insert_subject($data));
	}
	public function update_subject() { 
		$data["evalDesc"] = $this->input->get_post("evalDesc");
		$data["evalSubjectID"] = $this->input->get_post("evalSubjectID"); 
		$data["evalSubjectText"] = $this->input->get_post("evalSubjectText");
		$data["evalWeight"] = $this->input->get_post("evalWeight"); 
		$data["minScore"] = $this->input->get_post("minScore");
		$data["maxScore"] = $this->input->get_post("maxScore");
		echo ($this->evaluation_model->update_subject($data));
	}
	public function delete_subject() { 
		$evalSubjectID = $this->input->get_post("evalSubjectID");
		echo ($this->evaluation_model->delete_subject($evalSubjectID));
	}
	
	
	
	public function evalupcondition(){ 
		
		$year	= $this->input->get_post("year"); 
		$round	= $this->input->get_post("round"); 
		
		$xcrud	=	xcrud_get_instance(); 
		
        $xcrud->table('tbl_up_condition'); 
        $xcrud->where('budgetYear' ,$year);
		$xcrud->where('round' ,$round);
		$arr		=	array("chkDateStart"=>"ต้องปฏิบัติงานก่อนวันที่หรือไม่","valDateStart"=>"วันที่ปฏิบัติงาน","chkPernulties"=>"ไม่มีประวัติการลงโทษ","valPernulties"=>"วันที่ถูกลงโทษ","chkTscore"=>"คะแนน T-Score" ,"valTscore"=>"ค่าคะแนน T-Score" ,"chkEducation"=>"ลาศึกษาต่อ");
		
		$arr1		=	array("budgetYear"=>'ปีงบประมาณ','round'=>'รอบ',"chkDateStart"=>"ต้องปฏิบัติงานก่อนวันที่ หรือไม่","valDateStart"=>"วันที่ปฏิบัติงาน","chkPernulties"=>"ไม่มีประวัติการลงโทษ","valPernulties"=>"วันที่ถูกลงโทษ","chkTscore"=>"คะแนน T-Score" ,"valTscore"=>"ค่าคะแนน T-Score" ,"chkEducation"=>"ลาศึกษาต่อ");
        
 		
		$xcrud->column_callback('valDateStart','DateThai');
		$xcrud->column_callback('valPernulties','DateThai'); 
		
 		$xcrud->pass_var('budgetYear',$year,'create');
		$xcrud->pass_var('round',$round,'create');
 		 
        $xcrud->label($arr1);
        $xcrud->columns($arr1);//แสดงในตางราง $xcrud->columns('userName');  
   		 
		//$xcrud->before_remove('delete_user_data'); 
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
		 
		$this->template->load('template/admin', 'EvalUpCondition',$data);	
	}
	
	public function evalcondition(){
		
		$year	= $this->input->get_post("year"); 
		$round	= $this->input->get_post("round"); 
		
		$xcrud	=	xcrud_get_instance(); 
		
        $xcrud->table('tbl_contract_condition'); 
        $xcrud->where('budgetYear' ,$year);
		$xcrud->where('round' ,$round);
		$arr		=	array("chkDateStart"=>"ต้องปฏิบัติงานก่อนวันที่หรือไม่","valDateStart"=>"วันที่ปฏิบัติงาน","chkPernulties"=>"ไม่มีประวัติการลงโทษ","valPernulties"=>"วันที่ถูกลงโทษ","chkTscore"=>"คะแนน T-Score" ,"valTscore"=>"ค่าคะแนน T-Score" );
		
		$arr1		=	array("budgetYear"=>'ปีงบประมาณ','round'=>'รอบ',"chkDateStart"=>"ต้องปฏิบัติงานก่อนวันที่ หรือไม่","valDateStart"=>"วันที่ปฏิบัติงาน","chkPernulties"=>"ไม่มีประวัติการลงโทษ","valPernulties"=>"วันที่ถูกลงโทษ","chkTscore"=>"คะแนน T-Score" ,"valTscore"=>"ค่าคะแนน T-Score" );
        
 		
		$xcrud->column_callback('valDateStart','DateThai');
		$xcrud->column_callback('valPernulties','DateThai'); 
		
 		$xcrud->pass_var('budgetYear',$year,'create');
		$xcrud->pass_var('round',$round,'create');
 		 
        $xcrud->label($arr1);
        $xcrud->columns($arr1);//แสดงในตางราง $xcrud->columns('userName');  
   		 
		//$xcrud->before_remove('delete_user_data'); 
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
		 
        
		$this->template->load('template/admin', 'EvalCondition',$data);	
	}

   
}

?>