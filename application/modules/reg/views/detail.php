<div class="col-md-12">
    <section class="panel">
        <header class="panel-heading tab-bg-dark-navy-blue">
            <ul class="nav nav-tabs nav-justified ">
                <li class="active">
                    <a href="#settings" data-toggle="tab">
                        ข้อมูลการศึกษา
                    </a>
                </li>
                <li>
                    <a href="#settings" data-toggle="tab">
                        การทำผิดวินัย
                    </a>
                </li>
                <li>
                    <a class="contact-map" href="#settings" data-toggle="tab">
                        ประวัติการอบรม
                    </a>
                </li>
                <li>
                    <a href="#settings" data-toggle="tab">
                        การสอน / บรรยาย
                    </a>
                </li>
                <li>
                    <a href="#settings" data-toggle="tab">
                        ความดีความชอบ
                    </a>
                </li>
                <li>
                    <a href="#settings" data-toggle="tab">
                        สิทธิ์สวัสดิการ
                    </a>
                </li>
                 <li>
                    <a href="#settings" data-toggle="tab">
                        สัญญาจ้าง
                    </a>
                </li>
            </ul>
        </header>
        <div class="panel-body">
            <div class="tab-content tasi-tab">
                <div class="tab-pane active" id="overview">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="recent-act">
                                <h1>Recent Activity</h1>
                                <div class="activity-icon terques">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="activity-desk">
                                    <h2>1 Hour Ago</h2>
                                    <p>Purchased new stationary items for head office</p>
                                </div>
                                <div class="activity-icon red">
                                    <i class="fa fa-beer"></i>
                                </div>
                                <div class="activity-desk">
                                    <h2 class="red">2 Hour Ago</h2>
                                    <p>Completed Coffee meeting with <a class="terques" href="#">Stive Martin</a> regarding the Product Promotion</p>
                                </div>
                                <div class="activity-icon purple">
                                    <i class="fa fa-tags"></i>
                                </div>
                                <div class="activity-desk">
                                    <h2 class="purple">today evening</h2>
                                    <p>3 photo Uploaded on facebook product page</p>
                                    <div class="photo-gl">
                                        <a href="#">
                                            <img alt="" src="images/sm-img-1.jpg">
                                        </a>
                                        <a href="#">
                                            <img alt="" src="images/sm-img-2.jpg">
                                        </a>
                                        <a href="#">
                                            <img alt="" src="images/sm-img-3.jpg">
                                        </a>
                                    </div>
                                </div>

                                <div class="activity-icon green">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="activity-desk">
                                    <h2 class="green">yesterday</h2>
                                    <p>Outdoor visit at <a class="blue" href="#">California State Route</a> 85 with <a class="terques" href="#">John Boltana</a> &amp; <a class="terques" href="#">Harry Piterson</a> regarding to setup a new show room.</p>
                                    <div class="loc-map">
                                        location map goes here
                                    </div>
                                </div>

                                <div class="activity-icon yellow">
                                    <i class="fa fa-user-md"></i>
                                </div>
                                <div class="activity-desk">
                                    <h2 class="yellow">12 december 2013</h2>
                                    <p>Montly Regular Medical check up at Greenland Hospital.</p>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="prf-box">
                                <h3 class="prf-border-head">work in progress</h3>
                                <div class=" wk-progress">
                                    <div class="col-md-5">Themeforest</div>
                                    <div class="col-md-5">
                                        <div class="progress  ">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                                <span class="sr-only">70% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">70%</div>
                                </div>
                                <div class=" wk-progress">
                                    <div class="col-md-5">Graphics River</div>
                                    <div class="col-md-5">
                                        <div class="progress ">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                                                <span class="sr-only">57% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">57%</div>
                                </div>
                                <div class=" wk-progress">
                                    <div class="col-md-5">Code Canyon</div>
                                    <div class="col-md-5">
                                        <div class="progress ">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                <span class="sr-only">20% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">20%</div>
                                </div>
                                <div class=" wk-progress">
                                    <div class="col-md-5">Audio Jungle</div>
                                    <div class="col-md-5">
                                        <div class="progress ">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
                                                <span class="sr-only">30% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">30%</div>
                                </div>
                            </div>
                            <div class="prf-box">
                                <h3 class="prf-border-head">performance status</h3>
                                <div class=" wk-progress pf-status">
                                    <div class="col-md-8 col-xs-8">Total Product Sales</div>
                                    <div class="col-md-4 col-xs-4">
                                        <strong>23545</strong>
                                    </div>
                                </div>
                                <div class=" wk-progress pf-status">
                                    <div class="col-md-8 col-xs-8">Total Product Refer</div>
                                    <div class="col-md-4 col-xs-4">
                                        <strong>235</strong>
                                    </div>
                                </div>
                                <div class=" wk-progress pf-status">
                                    <div class="col-md-8 col-xs-8">Total Earn</div>
                                    <div class="col-md-4 col-xs-4">
                                        <strong>235452344$</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="prf-box">
                                <h3 class="prf-border-head">team members</h3>
                                <div class=" wk-progress tm-membr">
                                    <div class="col-md-2 col-xs-2">
                                        <div class="tm-avatar">
                                            <img alt="" src="images/lock_thumb.jpg">
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-xs-7">
                                        <span class="tm">John Boltana</span>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <a class="btn btn-white" href="#">Assign</a>
                                    </div>
                                </div>
                                <div class=" wk-progress tm-membr">
                                    <div class="col-md-2 col-xs-2">
                                        <div class="tm-avatar">
                                            <img alt="" src="images/avatar-mini-2.jpg">
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-xs-7">
                                        <span class="tm">John Boltana</span>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <a class="btn btn-white" href="#">Assign</a>
                                    </div>
                                </div>
                                <div class=" wk-progress tm-membr">
                                    <div class="col-md-2 col-xs-2">
                                        <div class="tm-avatar">
                                            <img alt="" src="images/avatar-mini-3.jpg">
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-xs-7">
                                        <span class="tm">John Boltana</span>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <a class="btn btn-white" href="#">Assign</a>
                                    </div>
                                </div>
                                <div class=" wk-progress tm-membr">
                                    <div class="col-md-2 col-xs-2">
                                        <div class="tm-avatar">
                                            <img alt="" src="images/avatar-mini-4.jpg">
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-xs-7">
                                        <span class="tm">John Boltana</span>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <a class="btn btn-white" href="#">Assign</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="job-history">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="timeline-messages">
                                <h3>Take a Tour</h3>
                                <!-- Comment -->
                                <div class="msg-time-chat">
                                    <div class="message-body msg-in">
                                        <span class="arrow"></span>
                                        <div class="text">
                                            <div class="first">
                                                13 January 2013
                                            </div>
                                            <div class="second bg-terques ">
                                                Join as Product Asst. Manager
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /comment -->

                                <!-- Comment -->
                                <div class="msg-time-chat">
                                    <div class="message-body msg-in">
                                        <span class="arrow"></span>
                                        <div class="text">
                                            <div class="first">
                                                10 February 2012
                                            </div>
                                            <div class="second bg-red">
                                                Completed Provition period and Appointed as a permanent Employee
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /comment -->

                                <!-- Comment -->
                                <div class="msg-time-chat">
                                    <div class="message-body msg-in">
                                        <span class="arrow"></span>
                                        <div class="text">
                                            <div class="first">
                                                2 January 2011
                                            </div>
                                            <div class="second bg-purple">
                                                Selected Employee of the Month
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /comment -->

                                <!-- Comment -->
                                <div class="msg-time-chat">
                                    <div class="message-body msg-in">
                                        <span class="arrow"></span>
                                        <div class="text">
                                            <div class="first">
                                                4 March 2010
                                            </div>
                                            <div class="second bg-green">
                                                Got Promotion and become area manager of California
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /comment -->
                                <!-- Comment -->
                                <div class="msg-time-chat">
                                    <div class="message-body msg-in">
                                        <span class="arrow"></span>
                                        <div class="text">
                                            <div class="first">
                                                3 April 2009
                                            </div>
                                            <div class="second bg-yellow">
                                                Selected the Best Employee of the Year 2013 and was awarded
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /comment -->

                                <!-- Comment -->
                                <div class="msg-time-chat">
                                    <div class="message-body msg-in">
                                        <span class="arrow"></span>
                                        <div class="text">
                                            <div class="first">
                                                23 May 2008
                                            </div>
                                            <div class="second bg-terques">
                                                Got Promotion and become Product Manager and was transper from Branch to Head Office. Lorem ipsum dolor sit amet
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /comment -->
                                <!-- Comment -->
                                <div class="msg-time-chat">
                                    <div class="message-body msg-in">
                                        <span class="arrow"></span>
                                        <div class="text">
                                            <div class="first">
                                                14 June 2007
                                            </div>
                                            <div class="second bg-blue">
                                                Height Sales scored and break all of the previous sales record ever in the company. Awarded
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /comment -->
                                <!-- Comment -->
                                <div class="msg-time-chat">
                                    <div class="message-body msg-in">
                                        <span class="arrow"></span>
                                        <div class="text">
                                            <div class="first">
                                                1 January 2006
                                            </div>
                                            <div class="second bg-green">
                                                Take 15 days leave for his wedding and Honeymoon &amp; Christmas
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /comment -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="contacts">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="prf-contacts">
                                <h2> <span><i class="fa fa-map-marker"></i></span> location</h2>
                                <div class="location-info">
                                    <p>Postal Address<br>
                                        PO Box 16122 Collins Street West<br>
                                        Victoria 8007 Australia</p>
                                    <p>Headquarters<br>
                                        121 King Street, Melbourne<br>
                                        Victoria 3000 Australia</p>
                                </div>
                                <h2> <span><i class="fa fa-phone"></i></span> contacts</h2>
                                <div class="location-info">
                                    <p>Phone  : +61 3 8376 6284 <br>
                                        Cell    : +61 3 8376 6284</p>
                                    <p>Email    : david@themebucket.net<br>
                                        Skype   : david.rojormillan</p>
                                    <p>
                                        Facebook  : https://www.facebook.com/themebuckets <br>
                                        Twitter : https://twitter.com/theme_bucket
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="map-canvas" style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;" class="gm-style"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;" aria-hidden="true"><div style="width: 256px; height: 256px; position: absolute; left: -241px; top: -288px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -241px; top: -32px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 15px; top: -288px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 15px; top: -32px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;" aria-hidden="true"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -241px; top: -288px;"><canvas draggable="false" style="-moz-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;" height="512" width="512"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -241px; top: -32px;"><canvas draggable="false" style="-moz-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;" height="512" width="512"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 15px; top: -288px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 15px; top: -32px;"></div></div></div></div><div style="position: absolute; z-index: 0; left: 0px; top: 0px;"><div style="overflow: hidden;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;" aria-hidden="true"><div style="width: 256px; height: 256px; position: absolute; left: -241px; top: -288px; opacity: 1; transition: opacity 200ms ease-out 0s;"><img style="width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://mts0.googleapis.com/vt?pb=!1m4!1m3!1i15!2i29578!3i20106!2m3!1e0!2sm!3i298000000!3m9!2sth-TH!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2" draggable="false"></div><div style="width: 256px; height: 256px; position: absolute; left: -241px; top: -32px; opacity: 1; transition: opacity 200ms ease-out 0s;"><img style="width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://mts0.googleapis.com/vt?pb=!1m4!1m3!1i15!2i29578!3i20107!2m3!1e0!2sm!3i298000000!3m9!2sth-TH!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2" draggable="false"></div><div style="width: 256px; height: 256px; position: absolute; left: 15px; top: -288px; opacity: 1; transition: opacity 200ms ease-out 0s;"><img style="width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://mts1.googleapis.com/vt?pb=!1m4!1m3!1i15!2i29579!3i20106!2m3!1e0!2sm!3i298000000!3m9!2sth-TH!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2" draggable="false"></div><div style="width: 256px; height: 256px; position: absolute; left: 15px; top: -32px; opacity: 1; transition: opacity 200ms ease-out 0s;"><img style="width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://mts1.googleapis.com/vt?pb=!1m4!1m3!1i15!2i29579!3i20107!2m3!1e0!2sm!3i298000000!3m9!2sth-TH!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2" draggable="false"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a style="position: static; overflow: visible; float: none; display: inline;" target="_blank" href="https://maps.google.com/maps?ll=-37.815207,144.963937&amp;z=15&amp;t=m&amp;hl=th-TH&amp;gl=US&amp;mapclient=apiv3" title="คลิกเพื่อดูพื้นที่นี้ใน Google Maps "><div style="width: 62px; height: 26px; cursor: pointer;"><img style="position: absolute; left: 0px; top: 0px; width: 62px; height: 26px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://maps.gstatic.com/mapfiles/api-3/images/google_white2_hdpi.png" draggable="false"></div></a></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 0px; bottom: 0px; width: 12px;"><div draggable="false" style="-moz-user-select: none;" class="gm-style-cc"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">ข้อมูลแผนที่</a><span style="display: none;"></span></div></div></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto,Arial,sans-serif; color: rgb(34, 34, 34); box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.2); z-index: 10000002; display: none; width: 0px; height: 0px; position: absolute; left: 5px; top: 5px;"><div style="padding: 0px 0px 10px; font-size: 16px;">ข้อมูลแผนที่</div><div style="font-size: 13px;"></div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false"></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto,Arial,sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);"></div></div><div class="gmnoprint gm-style-cc" style="z-index: 1000001; position: absolute; -moz-user-select: none; right: 0px; bottom: 0px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);" href="https://www.google.com/intl/th-TH_US/help/terms_maps.html" target="_blank">ข้อกำหนดในการใช้งาน</a></div></div><div draggable="false" style="-moz-user-select: none; display: none; position: absolute; right: 0px; bottom: 0px;" class="gm-style-cc"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a target="_new" title="รายงานข้อผิดพลาดในแผนที่ถนนหรือภาพถ่ายไปยัง Google" style="font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;" href="https://www.google.com/maps/@-37.815207,144.963937,15z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3">รายงานข้อผิดพลาดของแผนที่</a></div></div><div class="gmnoprint" style="margin: 5px; -moz-user-select: none; position: absolute; left: 0px; top: 0px;" draggable="false" controlwidth="32" controlheight="84"><div controlwidth="32" controlheight="40" style="cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; position: absolute; left: 0px; top: 0px;"><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px;" aria-label="การควบคุมเพ็กแมนสำหรับ Street View"><img style="position: absolute; left: -9px; top: -102px; width: 1028px; height: 214px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout2_hdpi.png" draggable="false"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;" aria-label="ปิดใช้เพ็กแมนอยู่"><img style="position: absolute; left: -107px; top: -102px; width: 1028px; height: 214px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout2_hdpi.png" draggable="false"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;" aria-label="เพ็กแมนอยู่ด้านบนสุดของแผนที่"><img style="position: absolute; left: -58px; top: -102px; width: 1028px; height: 214px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout2_hdpi.png" draggable="false"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;" aria-label="การควบคุมเพ็กแมนสำหรับ Street View"><img style="position: absolute; left: -205px; top: -102px; width: 1028px; height: 214px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout2_hdpi.png" draggable="false"></div></div><div class="gmnoprint" style="opacity: 0.6; display: none; position: absolute;" controlwidth="0" controlheight="0"><div style="width: 22px; height: 22px; overflow: hidden; position: absolute; cursor: pointer;" title="หมุนแผนที่ 90 องศา"><img style="position: absolute; left: -38px; top: -360px; width: 59px; height: 492px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6_hdpi.png" draggable="false"></div></div><div class="gmnoprint" controlwidth="20" controlheight="39" style="position: absolute; left: 6px; top: 45px;"><div style="width: 20px; height: 39px; overflow: hidden; position: absolute;"><img style="position: absolute; left: -39px; top: -401px; width: 59px; height: 492px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;" src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6_hdpi.png" draggable="false"></div><div style="position: absolute; left: 0px; top: 2px; width: 20px; height: 17px; cursor: pointer;" title="ขยาย"></div><div style="position: absolute; left: 0px; top: 19px; width: 20px; height: 17px; cursor: pointer;" title="ย่อ"></div></div></div><div class="gmnoprint gm-style-mtc" style="margin: 5px; z-index: 0; position: absolute; cursor: pointer; text-align: left; width: 85px; right: 0px; top: 0px;"><div style="direction: ltr; overflow: hidden; text-align: left; position: relative; color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 1px 6px; border-radius: 2px; background-clip: padding-box; border: 1px solid rgba(0, 0, 0, 0.15); box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); font-weight: 500;" draggable="false" title="เปลี่ยนลักษณะแผนที่">แผนที่<img src="https://maps.gstatic.com/mapfiles/arrow-down.png" draggable="false" style="-moz-user-select: none; border: 0px none; padding: 0px; margin: -2px 0px 0px; position: absolute; right: 6px; top: 50%; width: 7px; height: 4px;"></div><div style="background-color: white; z-index: -1; padding-top: 2px; background-clip: padding-box; border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgba(0, 0, 0, 0.15) rgba(0, 0, 0, 0.15); -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; border-image: none; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); position: relative; text-align: left; display: none;"><div style="color: black; font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 3px; font-weight: 500;" draggable="false" title="แสดงแผนที่ถนน">แผนที่</div><div style="color: black; font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 3px;" draggable="false" title="แสดงภาพจากดาวเทียม">ดาวเทียม</div><div style="margin: 1px 0px; border-top: 1px solid rgb(235, 235, 235);"></div><div style="color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap;" draggable="false" title="แสดงแผนที่ถนนพร้อมภูมิประเทศ"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img style="position: absolute; left: -52px; top: -44px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; width: 68px; height: 67px;" src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false"></div></span><label style="vertical-align: middle; cursor: pointer;">ภูมิประเทศ</label></div><div style="margin: 1px 0px; border-top: 1px solid rgb(235, 235, 235); display: none;"></div><div style="color: rgb(184, 184, 184); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap; display: none;" draggable="false" title="ขยายเพื่อแสดงมุมมอง 45 องศา"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(241, 241, 241); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img style="position: absolute; left: -52px; top: -44px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; width: 68px; height: 67px;" src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false"></div></span><label style="vertical-align: middle; cursor: pointer;">45°</label></div><div style="color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap; display: none;" draggable="false" title="แสดงภาพพร้อมชื่อถนน"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img style="position: absolute; left: -52px; top: -44px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; width: 68px; height: 67px;" src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false"></div></span><label style="vertical-align: middle; cursor: pointer;">ป้ายกำกับ</label></div></div></div></div></div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="settings">
                    <div class="position-center">
                        <div class="prf-contacts sttng">
                            <h2>  Personal Information</h2>
                        </div>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Avatar</label>
                                <div class="col-lg-6">
                                    <input type="file" class="file-pos" id="exampleInputFile">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Company</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="c-name" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Lives In</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="lives-in" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Country</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="country" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Description</label>
                                <div class="col-lg-10">
                                    <textarea name="" id="" class="form-control" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </form>
                        <div class="prf-contacts sttng">
                            <h2> socail networks</h2>
                        </div>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Facebook</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="fb-name" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Twitter</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="twitter" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Google plus</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="g-plus" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Flicr</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="flicr" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Youtube</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="youtube" placeholder=" ">
                                </div>
                            </div>

                        </form>
                        <div class="prf-contacts sttng">
                            <h2>Contact</h2>
                        </div>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Address 1</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="addr1" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Address 2</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="addr2" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Phone</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="phone" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cell</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="cell" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="email" placeholder=" ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Skype</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="skype" placeholder=" ">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>