<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php echo $staffName; ?></small></h3>
                
                <a href="<?php echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php echo $this->config->item('txt_back')?></a>
            </header>
            <div class="panel-body">
                <?php echo $html; ?>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
$(function(){
    //alert("aa");
    $('.btn-warning').attr("id","btn_back");
    $('.btn-warning').attr("onclick","window.location='../reg'");
    $('.btn-warning').removeAttr("class");
    //$('#btn_back').attr("class","btn btn-warning");

    //$('.btn-primary').attr("id","btn_save");
    $('.btn-primary').removeAttr("data-after");
    $('.btn-primary').attr("data-after","edit");
})

jQuery(document).on("xcrudafterrequest",function(event,container){

    if(Xcrud.current_task == 'save')
    {
        window.location="<?php echo site_url()?>reg";
        
    }
});
</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);


</script>