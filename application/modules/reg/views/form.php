<style>
    .error_lbl{
        border-color:red !important;
    }
        .ui-dialog-titlebar-close{
            display:none
        }
        .header-product{
            background-color: pink;
            font-weight: bold;
        }
        .header-product th{
            text-align: center;
            vertical-align: top;
        }
        .btn-none{
            display:none;
        }
</style>

<div class="col-md-12">

     <div class="col-separator box col-separator-first col-unscrollable">
        <div class="col-table">
                    <div class="heading-buttons innerLR border-bottom">
                        <h4 class="margin-none innerTB pull-left">ทะเบียนประวัติ</h4>

                        <div class="clearfix"></div>
                    </div>

             <div class="col-table-row">

                  <div class="col-app">
                        <div class="col-separator-h box"></div>


                        <div class="innerAll bg-gray">

                            <form class="form-horizontal " id="staffForm" name="staffForm" method="post" action="<?php echo site_url(); ?>reg/submit" >
                                  <div class="col-md-8 ">
                                     <input type="hidden" name="staffID" id="staffID" value="<?php echo (!empty($r)) ? $r->staffID : ""; ?>">
                                    <div class="form-group">
                                       <label class="col-sm-10 col-sm-offset-1">ข้อมูลการทำงาน </label>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-4 control-label">รหัสพนักงาน</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control"disabled readonly value="<?php echo (!empty($r)) ? $r->staffID : ""; ?>" >
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-4 control-label">ชื่อ</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control" name="staffFName" id="staffFName" value="<?php echo (!empty($r)) ? $r->staffFName : ""; ?>" >
                                      </div>
                                    </div>
                                     <div class="form-group">
                                      <label class="col-sm-4 control-label">นามสกุล</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control" name="staffLName" id="staffLName" value="<?php echo (!empty($r)) ? $r->staffLName : ""; ?>" >
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-4 control-label">ระดับ</label>
                                      <div class="col-sm-6">
                                        <?php
                                            <select name="rankID" id="rankID">

                                            </select>
                                             $rankID = (!empty($r)) ? $r->rankID : "";
                                            echo listData('tblrank','rankName','rankID'); ?>
                                      </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-10 col-sm-offset-1">
                                            <button type="button" class="btn btn-info" onclick="submitForm()">บันทึก</button>
                                            <button type="button" class="btn btn-warning" onclick="window.location='<?php echo site_url();?>reg'">กลับ</button>
                                        </div>
                                    </div>

                            </form>

                             <div class="clearfix border-bottom"></div>
                        </div>
                    </div>
                </div>

        </div>
      </div>
</div>
