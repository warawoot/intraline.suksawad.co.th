<div class="row"> 
  <div class="col-md-12"> 
    <section class="panel"> 
      <header class="panel-heading"> 
        <h3>เปลี่ยนรหัสผ่าน</h3> 
        <h5 style="color:#A9A6A6;">
            <i class="fa fa-key" aria-hidden="true"></i>
            <?php echo $staff->staffPreName.$staff->staffFname.' '.$staff->staffLname;?>
        </h5> 
      </header> 
    </section> 
  </div> 
</div> 
<div class="xcrud-container" align="right" >
  <div class="xcrud-ajax">
        <div class="xcrud-view">
            <form id="formMain" action="<?=base_url('reg/passwordreset_submit') ?>" method="post"  enctype="multipart/form-data" class="form-horizontal">       			 
				 <div class="form-group">                   
					<div class="col-sm-4" align="right">
					 	<label>รหัสผ่านใหม่ <span style="color:red">*</span> :</label> 
					</div>
                    <div class="col-sm-4">
				
						<input 
                            data-required="1" 
                            type="password" 
                            style="width:400px;background-color:#FFF" 
                            name="new_pass" 
                            id="pw_new"
                            placeholder="กรุณาป้อนรหัสใหม่..."
							class="form-control input-small" value=""
							required="">			 
                    </div>
				</div>
				 <div class="form-group">                   
					<div class="col-sm-4" align="right">
					 	<label>ยืนยันรหัสผ่าน <span style="color:red">*</span> :</label> 
					</div>
                    <div class="col-sm-4">
				
						<input 
                            data-required="1" 
                            type="password" 
                            style="width:400px;background-color:#FFF" 
                            name="new_pass2" 
                            id="pw_rel_new"
                            placeholder="ยืนยันรหัสใหม่..."
							class="form-control input-small" value=""
							required="">			 
                    </div>

				</div>
				<div class="form-group"> 
		          		<div class="col-sm-4"> 
                        <input type="hidden" name="staffID" value="<?=$staff->staffID;?>">
		          		</div> 
		          		<div class="col-sm-4 text-left"> 
		            		<div class="btn-group"> 
		              			<button class="btn btn-primary xcrud-action"  
		                type="submit" >บันทึกและย้อนกลับ</button>
		              			<button class="btn btn-warning xcrud-action" onclick="window.location='reg';"  
                type="button" >ย้อนกลับ</button>
                			</div>
                		</div>
                </div>  
        	</form>
    	</div>
	</div>
</div>



