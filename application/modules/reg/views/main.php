<!--<div data-ng-view="" id="ng-view" data-ng-app="myApp"></div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular-resource.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular-route.min.js"></script>
<script charset="utf-8" type="text/javascript" src="<?php echo base_url(); ?>assets/js/ui-grid.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ui-grid.min.css">

<script>
var app = angular.module('myApp', ['ngRoute']);

app.config(function ($routeProvider) {
  $routeProvider
   .when('/', {
    templateUrl: '<?php echo site_url();?>reg/page_list',
    controller: 'ListCtrl'
  })
  .otherwise({
    redirectTo:'/' // ให้ไปที่หน้าลิสแสดงรายการ
  });
})


app.controller('ListCtrl', ['$scope', '$http','$location', 'uiGridConstants','myObj', function ($scope, $http, $location, uiGridConstants,myObj) {


  $scope.gridOptions = {
    enableFiltering: false,
    flatEntityAccess: true,
    showGridFooter: true
  };

   $scope.gridOptions.columnDefs = [
      {name:'id', field: 'StaffID' },
      {name:'name', field: 'FName'},
      {name: 'edit', displayName: '', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="grid.appScope.edit(row.entity.id)" class="glyphicon glyphicon-edit red"></a></div>'},
      {name: 'del', displayName: '',cellTemplate:'<div class="ngCellText" ng-class="col.colIndex()"><a ng-click="grid.appScope.delete(row.entity.id)" class="glyphicon glyphicon-remove red"></a></div>'}
  ];

   $http.get('<?php echo site_url();?>reg/get_list')
    .success(function(data) {
    $scope.gridOptions.data = data;
  });


}])


</script>-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <div id="div_header"><h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?></h3></div>
                <div id="div_header_edit"><h3><?php echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user" id="fastaffname"></i> </small></h3></div>
                 <a href="javascript:location.reload();" id="btn_back"  class="btn btn-info"><?php echo $this->config->item('txt_back')?></a>

            </header>
            <div class="panel-body">
                <header class="panel-heading tab-bg-dark-navy-blue">


                  <div id="div_myTabs">

                    <ul class="row" id="myTabs">

                      <li id="li_tab_work" class="list-group-item col-xs-2"><a href="" data-toggle="tab" id="tab_work">ประวัติการทำงาน</a></li>
                      <li id="li_tab_graduation" class="list-group-item col-xs-2"><a href="" data-toggle="tab" id="tab_graduation">การศึกษา</a></li>
                      <li id="li_tab_seminar" class="list-group-item col-xs-2"><a href="" data-toggle="tab" id="tab_seminar">อบรม / สัมมนา</a></li>
                      <li id="li_tab_person" class="list-group-item col-xs-3"><a href="" data-toggle="tab" id="tab_person">คู่สมรส / บุตร / บิดามารดา</a></li>
                      <li id="li_tab_contact" class="list-group-item col-xs-3"><a href="" data-toggle="tab" id="tab_contact">ผู้ติดต่อฉุกเฉิน / ผู้คำ้ประกัน</a></li>

                      <li id="li_tab_mistake" class="list-group-item col-xs-2"><a href="" data-toggle="tab" id="tab_mistake">ความผิด / โทษ</a></li>
                      <li id="li_tab_feat" class="list-group-item col-xs-2"><a href="" data-toggle="tab" id="tab_feat">ความดี / ความชอบ</a></li>
                      <!--<li id="li_tab_insignia" class="list-group-item col-xs-2"><a href="" data-toggle="tab" id="tab_insignia">เครื่องราช</a></li>-->
                      <li id="li_tab_welfare" class="list-group-item col-xs-2"><a href="" data-toggle="tab" id="tab_welfare">สวัสดิการ</a></li>
                      <li id="li_tab_contract" class="list-group-item col-xs-3"><a href="" data-toggle="tab" id="tab_contract">สัญญาจ้าง</a></li>
                      <li id="li_tab_note" class="list-group-item col-xs-3"><a href="" data-toggle="tab" id="tab_note">Note</a></li>

                    </ul>
                 </div>
                </header>
                 <div class="panel-body">
                      <div class="tab-content tasi-tab">
                        <div id="ajax-content" class="tab-pane active">
                         <?php echo $html; ?>
                        </div>
                      </div>
                </div>
            </div>
            <div class="separator"></div>
            <!--<div class="row" id="comment">
                <div class=" col-sm-8 text-left">
                    <p><strong>คำอธิบาย</strong></p>
                      <div class=" col-sm-4 text-left">
                     <span class="inline"><a title="ประวัติการทำงาน" href="javascript:;" class="btn btn-sm btn-warning"><i class="fa fa-folder-open"></i></a>
                       ประวัติการทำงาน</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </div>
                      <div class=" col-sm-4 text-left">
                      <span class="inline"><a title="การศึกษา" href="javascript:;" class="btn btn-sm btn-danger"><i class="fa fa-book"></i></a>
                       การศึกษา</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       </div>
                       <div class=" col-sm-4 text-left">
                       <span class="inline"><a title="อบรม / สัมมนา" href="javascript:;" class="btn btn-sm btn-success"><i class="fa fa-calendar"></i></a>
                       อบรม / สัมมนา</span>&nbsp;&nbsp;
                       </div><br><br>
                       <!--<div class=" col-sm-4 text-left">
                       <span class="inline"><a title="ประวัติเคลื่อนไหว / โยกย้าย" href="javascript:;" class="btn btn-sm btn-primary"><i class="fa fa-repeat"></i></a>
                       ประวัติเคลื่อนไหว / โยกย้าย</span>&nbsp;&nbsp;
                       </div>-->
           <!--            <div class=" col-sm-4 text-left">
                       <span class="inline"><a title="คู่สมรส / บุตร / บิดามารดา" href="javascript:;" class="btn btn-sm btn-primary"><i class="fa fa-users"></i></a>
                       คู่สมรส / บุตร / บิดามารดา</span>&nbsp;&nbsp;
                       </div>
                       <div class=" col-sm-4 text-left">
                       <span class="inline"><a title="ผู้ติดต่อฉุกเฉิน / ผู้คำ้ประกัน" href="javascript:;" class="btn btn-sm btn-default"><i class="fa fa-phone"></i></a>
                       ผู้ติดต่อฉุกเฉิน / ผู้คำ้ประกัน</span>&nbsp;&nbsp;
                       </div>
                       <div class=" col-sm-4 text-left">
                       <span class="inline"><a title="ความผิด / โทษ" href="javascript:;" class="btn btn-sm btn-info"><i class="fa fa-gavel"></i></a>
                      ความผิด / โทษ</span>&nbsp;&nbsp;
                    </div><br><br>
                    <div class=" col-sm-4 text-left">
                      <span class="inline"><a title="ความดี / ความชอบ" href="javascript:;" class="btn btn-sm btn-warning"><i class="fa fa-gift"></i></a>
                      ความดี / ความชอบ</span>&nbsp;&nbsp;
                       </div>
                       <div class=" col-sm-4 text-left">
                        <span class="inline"><a title="เครื่องราช" href="javascript:;" class="btn btn-sm btn-danger"><i class="fa fa-bookmark"></i></a>
                      เครื่องราช</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </div>
                      <div class=" col-sm-4 text-left">
                      <span class="inline"><a title="สวัสดิการ" href="javascript:;" class="btn btn-sm btn-success"><i class="fa fa-plus-square"></i></a>
                      สวัสดิการ</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </div><br><br>
                      <div class=" col-sm-4 text-left">
                      <span class="inline"><a title="สัญญาจ้าง" href="javascript:;" class="btn btn-sm btn-primary"><i class="fa fa-file"></i></a>
                      สัญญาจ้าง</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </div>
                       <div class=" col-sm-4 text-left">
                      <span class="inline"><a title="Note" href="javascript:;" class="btn btn-sm btn-default"><i class="fa fa-file-text-o"></i></a>
                      Note</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </div>
                      <div class=" col-sm-4 text-left">
                      <span class="inline"><a title="พิมพ์ประวัติ" href="javascript:;" class="btn btn-sm btn-danger"><i class="fa fa-print"></i></a>
                      พิมพ์ประวัติ</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </div><br><br>
                      <div class=" col-sm-4 text-left">
                    <span class="inline">
                    <a href="javascript:;" title="ดูข้อมูล" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-search"></i></a>
                       ดูข้อมูล</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <div class=" col-sm-4 text-left">
                      <span class="inline"><a href="javascript:;" title="แก้ไข" class="btn btn-sm  btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                       แก้ไขข้อมูล</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <div class=" col-sm-4 text-left">
                      <span class="inline"><a href="javascript:;" title="ลบ" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                       ลบข้อมูล</span>&nbsp;&nbsp;

                   </div>
                  </div>

              </div>-->
        </section>
    </div>
</div>
<script type="text/javascript">

   $(document).ready(function() {

      $('#div_myTabs').hide();
        $('#btn_back').hide();
        $('#div_header').show();
        $('#div_header_edit').hide();

     $('#myTabs').click(function(e) {
        src = $(e.target).attr('href');
        id = $(e.target).attr('id');
        //src = $(paneID).attr('data-src');
        var html = "";
        html ='<iframe class="iframetab " src="' + src + '"></iframe>';

        $("#ajax-content").html(html);

        $("iframe").wrap('<div class="embed-responsive embed-responsive-16by9"/>');
        $("iframe").addClass('embed-responsive-item');

        $("li").removeClass("active");
        var classt = $('#li_'+id).attr("class");
        $('#li_'+id).attr("class","active "+classt);
    });

  });

  jQuery(document).on("xcrudafterrequest",function(event,container){

      if(Xcrud.current_task == 'edit'){
        $('#div_myTabs').show();
        $('#btn_back').show();
        $('#div_header_edit').show();
        $('#div_header').hide();

        var pk = $('input[name="primary"]').val();

        $('#tab_work').attr('href','<?php echo site_url();?>staffwork/index?token='+pk);
        $('#tab_graduation').attr('href','<?php echo site_url();?>staffgraduation/index?token='+pk);
        $('#tab_seminar').attr('href','<?php echo site_url();?>staffseminar/index?token='+pk);
        $('#tab_person').attr('href','<?php echo site_url();?>staffperson/index?token='+pk);
        $('#tab_contact').attr('href','<?php echo site_url();?>staffcontact/index?token='+pk);
        $('#tab_mistake').attr('href','<?php echo site_url();?>staffmistake/index?token='+pk);
        $('#tab_feat').attr('href','<?php echo site_url();?>stafffeat/index?token='+pk);
        $('#tab_insignia').attr('href','<?php echo site_url();?>staffinsignia/index?token='+pk);
        $('#tab_welfare').attr('href','<?php echo site_url();?>staffwelfare/index?token='+pk);
        $('#tab_contract').attr('href','<?php echo site_url();?>staffcontract/index?token='+pk);
        $('#tab_note').attr('href','<?php echo site_url();?>staffnote/index?token='+pk);

          $.post('<?php echo site_url();?>reg/getStaffName',{id: pk},
          function(data, status){
              $('#fastaffname').html(data);
        }
      );

      }else{
        $('#div_myTabs').hide();
        $('#btn_back').hide();
        $('#div_header').show();
        $('#div_header_edit').hide();
      }
      if(Xcrud.current_task == 'list' || Xcrud.current_task == 'save')
        $('#comment').show();
      else
        $('#comment').hide();
      /*if(Xcrud.current_task == 'save')
      {
          Xcrud.show_message(container,'บันทึกแล้ว','success');
      window.location='<?php echo site_url(); ?>admin_list_request';
      }else if(Xcrud.current_task == 'edit'){
      $('.xcrud-ajax').append('<input type="hidden" value="<?php echo site_url()?>admin_list_request/submit" name="link" class="xcrud-data">');
    }/*else if(Xcrud.current_task == 'list'){
      window.location='<?php echo site_url(); ?>admin_list_request';
    }*/
  });


</script>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {

          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }

              /*var cur_path = $location.path().substr(-path.length);

              if (cur_path == path) {

                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);


</script>
<style>

 .iframetab {
        border:0px;
        margin:0px;
        position: relative;
    }

    .panel-heading .nav > li > a{
        border-bottom: 1px solid transparent;

    }

    .panel-heading{
      margin:20px 0 30px 0;
    }

    .nav-tabs > li > a {
        /*background: #DADADA;*/
        border-radius: 0;
        border: 1px solid #ddd;
    }
    ul, .list-unstyled {
        padding: 0 10px;
    }
    .tab-bg-dark-navy-blue ul > li > a {
        display: block;
        padding: 10px 15px !important;
    }
    .tab-bg-dark-navy-blue li a:hover, .tab-bg-dark-navy-blue li.active a {
        background: #fff none repeat scroll 0 0 !important;
        border-radius: 0 !important;
        color: #337ab7 !important;
    }
    .tab-bg-dark-navy-blue li a:hover, .tab-bg-dark-navy-blue li.active a {
        background: #fff none repeat scroll 0 0 !important;
        border-radius: 0 !important;
        color: #337ab7 !important;
    }
    .list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
        background-color: #fff;
        border: 1px solid #ddd;
        color: #ffffff;
        z-index: 2;
    }


</style>
