<?php

class Reg extends MY_Controller {

	function __construct(){
		parent::__construct();
		//$this->load->add_package_path ( APPPATH . 'third_party/codeignitercrud/' );
		//$this->load->library ( 'CodeigniterCrud' );
		$this->load->model('staff_model');

	}

	public function index(){

		//Xcrud_config::$editor_url = base_url().'editors/ckeditor/ckeditor.js';

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		$xcrud->table('tbl_staff');
		$xcrud->order_by('staffID','asc');
		$xcrud->where('status =', 0);
		//$xcrud->default_tab('ข้อมูลส่วนตัว');
		//$xcrud->join('staffID','tblstaffaddress','staffID');
		$xcrud->relation('positionID','tbl_position','positionID','positionName');
		$xcrud->relation('orgID','tbl_org_chart','orgID','orgName');
		
		$xcrud->relation('rankID','tbl_rank','rankID','rankName');
	/*
		$xcrud->relation('orgID1','tbl_org_chart','orgID','orgName','org_chart.upperOrgID = \'0\' OR tbl_org_chart.upperOrgID = \'1\'','orgID');
		$xcrud->relation('orgID2','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\' AND upperOrgID != \'1\'','orgID','','','','upperOrgID','orgID1');
	*/
		//$xcrud->relation('orgID','tblorgchart','orgID','orgName','orgID != upperOrgID','','','','','upperOrgID','orgID2');
		$xcrud->relation('provinceID','tbl_province','provinceID','provinceName');
		$xcrud->relation('districtID','tbl_district','districtID','districtName','','','','','','provinceID','provinceID');
		$xcrud->relation('subdistrictID','tbl_sub_district','subdistrictID','subdistrictName','','','','','','districtID','districtID');
		$xcrud->relation('provinceIDNow','tbl_province','provinceID','provinceName');
		$xcrud->relation('districtIDNow','tbl_district','districtID','districtName','','','','','','provinceID','provinceIDNow');
		$xcrud->relation('subdistrictIDNow','tbl_sub_district','subdistrictID','subdistrictName','','','','','','districtID','districtIDNow');
		//$xcrud->relation('orgID','tblorgchart','orgID','orgName');
		//$xcrud->relation('staffPreName','tblprefix','prefixID','prefixName');
		//$xcrud->relation('staffPreNameEN','tblprefix','prefixID','prefixName');
		//$xcrud->relation('staffOldPreName','tblprefix','prefixID','prefixName');
		//$xcrud->relation('workStatusID','tblworkstatus','workStatusID','workStatusName');
		//$xcrud->relation('workPlaceID','tblworkplace','workPlaceID','workPlaceName');

		//// List /////
		$col_name = array(
			'staffID' 			=> 'รหัสพนักงาน',
			'status' 			=> 'สถานะการทำงานปัจจุบัน',
			'staffPreName'		=> 'คำนำหน้าชื่อ',
			'staffFName' 		=> 'ชื่อ',
			'staffLName' 		=> 'นามสกุล',
			'staffPreNameEN'	=> 'คำนำหน้าชื่อ (EN)',
			'staffIDCard' 		=> 'เลขประจำตัวประชาชน/หนังสือเดินทาง',
			'staffPreNameEN' 	=> 'คำนำหน้าชื่อ (EN)',
			'staffFNameEN' 		=> 'ชื่อ (EN)',
			'staffLNameEN' 		=> 'นามสกุล (EN)',
			//'staffOldPreName' => 'คำนำหน้าชื่อ (เดิม)',
			'staffOldPreName' 	=> '',
			'staffOldFName' 	=> 'ชื่อ (เดิม)',
			'staffOldLName' 	=> 'นามสกุล (เดิม)',
			'staffBirthday' 	=> 'วันเกิด',
			'staffGender' 		=> 'เพศ',
			'staffBlood' 		=> 'หมู่โลหิต',
			'staffImage' 		=> 'รูปภาพ',
			'rankID' 			=> 'ระดับ',
			'positionID' 		=> 'ตำแหน่ง',
			'orgID'				=> 'แผนก',
			/*'staffDateWork' => 'วันที่บรรจุ',
			'workStatusID' => 'ที่อยู่ตามทะเบียนบ้าน',
			'workPlaceID' => 'ที่อยู่ปัจจุบัน',
			'orgID1'	=> 'ฝ่าย / สำนักงาน',
			'orgID2'	=> 'กอง / กลุ่มงาน',*/
			'staffImage'		=> 'รูป',
			'staffBirthday'	=> 'วันเกิด',
			'staffBirthday'	=> 'วันเกิด',
			'staffHight'	=> 'ส่วนสูง',
			'staffWeight' => 'น้ำหนัก',
			'staffTel'	=> 'เบอร์โทรศัพท์',
			'staffEmail' => 'E-mail',
			'staffPreNameEtc' => 'อื่นๆ',
			'staffPreNameEnEtc' => 'Etc',
			'staffEthnicity' => 'เชื้อชาติ',
			'staffNation'	=> 'สัญชาติ',
			'staffReligion' => 'ศาสนา',
			'staffStatus'	=> 'สถานะภาพ',
			'staffMilitary' => 'สถานะทางการทหาร',
			'staffDriver'  => 'ใบอนุญาติขับขี่',
			'staffTalent'  => 'ความรู้ความสามารถ',
			'staffAddress'  => 'เลขที่/หมู่บ้าน/ซอย (ทะเบียนบ้าน)',
			'provinceID'  => 'จังหวัด (ทะเบียนบ้าน)',
			'districtID'  => 'อำเภอ/เขต (ทะเบียนบ้าน)',
			'subdistrictID'  => 'ตำบล/แขวง (ทะเบียนบ้าน)',
			'zipCode'  => 'รหัสไปรษณีย์ (ทะเบียนบ้าน)',
			'staffAddressNow'  => 'เลขที่/หมู่บ้าน/ซอย (ปัจจุบัน)',
			'provinceIDNow'  => 'จังหวัด (ปัจจุบัน)',
			'districtIDNow'  => 'อำเภอ/เขต (ปัจจุบัน)',
			'subdistrictIDNow'  => 'ตำบล/แขวง (ปัจจุบัน)',
			'zipCodeNow'  => 'รหัสไปรษณีย์ (ปัจจุบัน)',
			'staffIDCardType' => 'ประเภทบัตรประจำตัว',
			'staffDriverDate' => 'วันที่ใบอนุญาตหมดอายุ',
			'staffAddrType' => 'ประเภทที่อยู่',
			'staffIDCardOff' => 'รหัสพนักงาน',
			'salary'  => 'เงินเดือน',
			'wage'  => 'ค่าจ้าง (รายวัน)', 
      		'staffNickName' => 'ชื่อเล่น',
      		'dateIn' => 'วันที่เริ่มงาน',
      		'dateOut' => 'วันที่ลาออก',
      		'orgName' => 'ชื่อแผนก'

		);


		$xcrud->columns('staffID,staffFName,staffNickName,rankID,positionID,orgID');
		$xcrud->label($col_name);
		$xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');

		$xcrud->column_callback('rankID','calRankWork');
		$xcrud->column_callback('positionID','calPositionWork');
		$xcrud->column_callback('orgID','calOrgIDWork');


		$xcrud->column_width('staffID','5%');
		//$xcrud->unset_view()->unset_remove()->unset_edit();
/*		$xcrud->button(site_url().'staffWork/index?token={ID}','ประวัติการทำงาน','fa fa-folder-open','btn-warning');
		$xcrud->button(site_url().'staffgraduation/index?token={ID}','การศึกษา','fa fa-book','btn-danger');
		$xcrud->button(site_url().'staffseminar/index?token={ID}','อบรม / สัมมนา','fa fa-calendar','btn-success');
		//$xcrud->button(site_url().'staffmove/index?token={ID}','ประวัติเคลื่อนไหว / โยกย้าย','fa fa-repeat','btn-default');
		$xcrud->button(site_url().'staffperson/index?token={ID}','คู่สมรส / บุตร / บิดามารดา','fa fa-users','btn-primary');
		$xcrud->button(site_url().'staffcontact/index?token={ID}','ผู้ติดต่อฉุกเฉิน / ผู้คำ้ประกัน','fa fa-phone','btn-default');
		$xcrud->button(site_url().'staffmistake/index?token={ID}','ความผิด / โทษ','fa fa-gavel','btn-info');
		$xcrud->button(site_url().'stafffeat/index?token={ID}','ความดี / ความชอบ','fa fa-gift','btn-warning');
		$xcrud->button(site_url().'staffinsignia/index?token={ID}','เครื่องราช','fa fa-bookmark','btn-danger');
		$xcrud->button(site_url().'staffwelfare/index?token={ID}','สวัสดิการ','fa fa-plus-square','btn-success');
		$xcrud->button(site_url().'staffcontract/index?token={ID}','สัญญาจ้าง','fa fa-file','btn-primary');
		$xcrud->button(site_url().'staffnote/index?token={ID}','Note','fa fa-file-text-o','btn-default');*/
		$xcrud->button(site_url().'staffprint/index?token={ID}','พิมพ์ประวัติ','fa fa-print','btn-default');
		$xcrud->button(site_url().'reg/passwordreset?token={ID}','เปลี่ยนรหัสผ่าน','fa fa-key','btn-green');

		//$xcrud->button('javascript:;','แฟ้มบุคคล','fa fa-folder-open','btn-primary',array('onclick'=>'javascript:showWork(\'{staffID}\')'));
		/*$xcrud->button('javascript:;','ข้อมูลส่วนตัว','fa fa-user','btn-warning',array('onclick'=>'javascript:showUser(\'{staffID}\')'));
		$xcrud->button('javascript:;','การศึกษา','fa fa-book','btn-success',array('onclick'=>'javascript:showGrad(\'{staffID}\')'));
		$xcrud->button('javascript:;','อบรม / สัมมนา','fa fa-calendar','btn-info',array('onclick'=>'javascript:showSeminar(\'{staffID}\')'));
		$xcrud->button('javascript:;','ประวัติเคลื่อนไหว / โยกย้าย','fa fa-repeat','btn-default',array('onclick'=>'javascript:showEdit(\'{staffID}\')'));
		$xcrud->button('javascript:;','คู่สมรส / บุตร / บิดามารดา','fa fa-users','btn-danger',array('onclick'=>'javascript:showEdit(\'{staffID}\')'));
		$xcrud->button('javascript:;','ผู้ติดต่อฉุกเฉิน / ผู้คำ้ประกัน','fa fa-phone','btn-primary',array('onclick'=>'javascript:showEdit(\'{staffID}\')'));
		$xcrud->button('javascript:;','ความผิด / โทษ','fa fa-gavel','btn-warning',array('onclick'=>'javascript:showEdit(\'{staffID}\')'));
		$xcrud->button('javascript:;','ความดี / ความชอบ','fa fa-gift','btn-success',array('onclick'=>'javascript:showEdit(\'{staffID}\')'));
		$xcrud->button('javascript:;','เครื่องราช','fa fa-bookmark','btn-info',array('onclick'=>'javascript:showEdit(\'{staffID}\')'));
		$xcrud->button('javascript:;','สวัสดิการ','fa fa-plus-square','btn-default',array('onclick'=>'javascript:showEdit(\'{staffID}\')'));
		*/

		//// Form //////

		$xcrud->fields('staffImage,staffID,staffIDCardType,staffIDCard,staffPreName,staffPreNameEtc,staffFName,staffLName,staffNickName,staffPreNameEN,staffPreNameEnEtc,staffFNameEN,staffLNameEN,status,dateIn,dateOut,orgID');
		$xcrud->fields('positionID,rankID,salary,wage');
		$xcrud->fields('staffOldPreName',false,false,'edit');
		$xcrud->fields('staffGender,staffBirthday,staffHight,staffWeight,staffBlood,staffEthnicity,staffNation,staffReligion,staffStatus,staffMilitary,staffDriver,staffDriverDate');
		$xcrud->fields('staffTalent,staffTel,staffEmail,staffAddress,provinceID,districtID,subdistrictID,zipCode,staffAddressNow,provinceIDNow,districtIDNow,subdistrictIDNow,zipCodeNow,staffAddrType');
		//$xcrud->subselect('staffFName','{staffFName} {staffLName}');

		$xcrud->change_type('staffImage', 'image', '', array(
	    'width' => 200,
	    'height' => 200,
	    'manual_crop' => true));
	   // $xcrud->change_type('staffID','text');
		$xcrud->change_type('status','radio','',array('0'=>'ปฏิบัติงาน','2'=>'ลาออก'));
		$xcrud->change_type('staffGender','radio','',array('m'=>'ชาย','f'=>'หญิง'));
		$xcrud->change_type('staffBlood','radio','',array('A'=>'A','B'=>'B','AB'=>'AB','O'=>'O'));
		$xcrud->change_type('staffPreName','select','',array('นาย'=>'นาย','นาง'=>'นาง','นางสาว'=>'นางสาว','อื่นๆ'=>'อื่นๆ'));
		$xcrud->change_type('staffPreNameEN','select','',array('Mr.'=>'Mr.','Mrs.'=>'Mrs.','Miss'=>'Miss','Etc'=>'Etc'));
		$xcrud->change_type('staffIDCardType','radio','',array('1'=>'คนไทย','2'=>'ต่างชาติ','3'=>'ประชาชนพื้นที่สูง(ชาวเขา)'));
		$xcrud->change_type('staffAddrType','radio','',array('1'=>'บ้านตนเอง','2'=>'บ้านญาติ','3'=>'บ้านเช่า/หอพัก'));


		//$xcrud->change_type('staffStatus','radio','',array('โสด'=>'ไม่มีข้อมูลคู่สมรส','สมรส'=>'มีข้อมูลคู่สมรสเป็น "สมรส"','หย่า'=>'มีข้อมูลคู่สมรสเป็น "หย่า"','หม้าย'=>'มีข้อมูลคู่สมรสเป็น "เสียชีวิต"'));
		$xcrud->column_pattern('staffStatus','{ID}');
		$xcrud->field_callback('staffStatus','getStaffStatus');
		$xcrud->change_type('staffMilitary','radio','',array('เคยรับราชการทหาร'=>'เคยรับราชการทหาร','ได้รับการยกเว้น'=>'ได้รับการยกเว้น'));
		$xcrud->change_type('staffDriver','multiselect','',array('รถยนต์'=>'รถยนต์','รถจักรยานยนต์'=>'รถจักรยานยนต์','รถ 6 ล้อ'=>'รถ 6 ล้อ'));
		$xcrud->validation_required('staffFName,staffLName')->validation_required('staffIDCard',13);
		$xcrud->validation_pattern('staffIDCard,staffHight,staffWeight,staffTel,zipCode,zipCodeNow','numeric')->validation_pattern('staffEmail','email');


		$xcrud->field_callback('staffOldPreName','getLinkChangeName');

		//$xcrud->field_callback('workStatusID','getNull');
		//$xcrud->field_callback('workPlaceID','getNull');

		//$xcrud->set_attr('workStatusID',array('class'=>'label'));
		//$xcrud->unset_list();
		//$xcrud->hide_button('return');

		//$contactInfo = $xcrud->nested_table('staff','orderNumber','orderdetails','orderNumber'); // 2nd level


		//$staffname = $xcrud->nested_table('ข้อมูลติดต่อ','staffID','tblstaffaddress','staffID'); // 2nd level 2
    	//$staffname->columns('addressNo');
    	//$staffname->default_tab('ข้อมูลติดต่อ');

		$data['html'] = $xcrud->render();
		$data['title'] = "ทะเบียนประวัติ";
        $this->template->load("template/admin",'main', $data);

	}

	public function getStaffName(){
		if(!empty($_POST['id'])){
			echo ' '.getStaffName($this->input->post('id'));
		}
	}

	public function editStaff(){
		if(!empty($_GET['id'])){
			$id = $this->input->get('id');


			$this->load->helper('xcrud_helper');
			$xcrud = xcrud_get_instance();

			$xcrud->table('tbl_staff')->where('ID',$id);
			$xcrud->table('tbl_staff')->where('status =', 0);

			$xcrud->relation('positionID','tbl_position','positionID','positionName');
			$xcrud->relation('provinceID','tbl_province','provinceID','provinceName');
			$xcrud->relation('districtID','tbl_district','districtID','districtName','','','','','','provinceID','provinceID');
			$xcrud->relation('subdistrictID','tbl_sub_district','subdistrictID','subdistrictName','','','','','','districtID','districtID');
			$xcrud->relation('provinceIDNow','tbl_province','provinceID','provinceName');
			$xcrud->relation('districtIDNow','tbl_district','districtID','districtName','','','','','','provinceID','provinceIDNow');
			$xcrud->relation('subdistrictIDNow','tbl_sub_district','subdistrictID','subdistrictName','','','','','','districtID','districtIDNow');

			//// List /////
			$col_name = array(
				'staffID' => 'รหัสพนักงาน',
				'staffPreName' => 'คำนำหน้าชื่อ',
				'staffFName' => 'ชื่อ',
				'staffLName' => 'นามสกุล',
				'status' => 'สถานะการทำงาน',
				'staffIDCard' => 'เลขประจำตัวประชาชน',
				'staffPreNameEN' => 'คำนำหน้าชื่อ (EN)',
				'staffFNameEN' => 'ชื่อ (EN)',
				'staffLNameEN' => 'นามสกุล (EN)',
				'staffOldPreName' => '',
				'staffOldFName' => 'ชื่อ (เดิม)',
				'staffOldLName' => 'นามสกุล (เดิม)',
				'staffBirthday' => 'วันเกิด',
				'staffGender' => 'เพศ',
				'staffBlood' => 'หมู่โลหิต',
				'staffImage' => 'รูปภาพ',
				'rankID' => 'ระดับ',
				'positionID' => 'ตำแหน่ง',
				'orgID'	=> 'ฝ่าย / สำนักงาน',
				'staffImage'	=> 'รูป',
				'staffBirthday'	=> 'วันเกิด',
				'staffBirthday'	=> 'วันเกิด',
				'staffHight'	=> 'ส่วนสูง',
				'staffWeight' => 'น้ำหนัก',
				'staffTel'	=> 'เบอร์โทรศัพท์',
				'staffEmail' => 'E-mail',
				'staffPreNameEtc' => 'อื่นๆ',
				'staffPreNameEnEtc' => 'Etc',
				'staffEthnicity' => 'เชื้อชาติ',
				'staffNation'	=> 'สัญชาติ',
				'staffReligion' => 'ศาสนา',
				'staffStatus'	=> 'สถานะภาพ',
				'staffMilitary' => 'สถานะทางการทหาร',
				'staffDriver'  => 'ใบอนุญาติขับขี่',
				'staffTalent'  => 'ความรู้ความสามารถ',
				'staffAddress'  => 'เลขที่/หมู่บ้าน/ซอย (ทะเบียนบ้าน)',
				'provinceID'  => 'จังหวัด (ทะเบียนบ้าน)',
				'districtID'  => 'อำเภอ/เขต (ทะเบียนบ้าน)',
				'subdistrictID'  => 'ตำบล/แขวง (ทะเบียนบ้าน)',
				'zipCode'  => 'รหัสไปรษณีย์ (ทะเบียนบ้าน)',
				'staffAddressNow'  => 'เลขที่/หมู่บ้าน/ซอย (ปัจจุบัน)',
				'provinceIDNow'  => 'จังหวัด (ปัจจุบัน)',
				'districtIDNow'  => 'อำเภอ/เขต (ปัจจุบัน)',
				'subdistrictIDNow'  => 'ตำบล/แขวง (ปัจจุบัน)',
				'zipCodeNow'  => 'รหัสไปรษณีย์ (ปัจจุบัน)',
				'salary'  => 'เงินเดือน',
				'wage'  => 'ค่าจ้าง (รายวัน)', 
        'staffNickName' => 'ชื่อเล่น'


			);

			$xcrud->columns('staffID,staffFName,rankID,positionID,orgID');
			$xcrud->label($col_name);
			$xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
			$xcrud->column_callback('rankID','calRankWork');
			$xcrud->column_callback('positionID','calPositionWork');
			$xcrud->column_callback('orgID','calOrgIDWork');


			$xcrud->column_width('staffID','5%');
			$xcrud->button(site_url().'staffprint/index?token={ID}','พิมพ์ประวัติ','fa fa-print','btn-danger');

			//// Form //////

			$xcrud->fields('staffImage,staffID,staffIDCard,staffNickName,staffPreName,staffPreNameEtc,staffFName,staffLName,status,staffPreNameEN,staffPreNameEnEtc,staffFNameEN,staffLNameEN');
			$xcrud->fields('positionID,salary,wage');
			$xcrud->fields('staffOldPreName',false,false,'edit');
			$xcrud->fields('staffGender,staffBirthday,staffHight,staffWeight,staffBlood,staffEthnicity,staffNation,staffReligion,staffStatus,staffMilitary,staffDriver');
			$xcrud->fields('staffTalent,staffTel,staffEmail,staffAddress,provinceID,districtID,subdistrictID,zipCode,staffAddressNow,provinceIDNow,districtIDNow,subdistrictIDNow,zipCodeNow');

			$xcrud->change_type('staffImage', 'image', '', array(
		    'width' => 200,
		    'height' => 200,
		    'manual_crop' => true));
			$xcrud->change_type('status','radio','',array('0'=>'ปฏิบัติงาน','2'=>'ลาออก'));
			$xcrud->change_type('staffGender','radio','',array('m'=>'ชาย','f'=>'หญิง'));
			$xcrud->change_type('staffBlood','radio','',array('A'=>'A','B'=>'B','AB'=>'AB','O'=>'O'));
			$xcrud->change_type('staffPreName','select','',array('นาย'=>'นาย','นาง'=>'นาง','นางสาว'=>'นางสาว','อื่นๆ'=>'อื่นๆ'));
			$xcrud->change_type('staffPreNameEN','select','',array('Mr.'=>'Mr.','Mrs.'=>'Mrs.','Miss'=>'Miss','Etc'=>'Etc'));
			$xcrud->column_pattern('staffStatus','{ID}');
			$xcrud->field_callback('staffStatus','getStaffStatus');
			$xcrud->change_type('staffMilitary','radio','',array('เคยรับราชการทหาร'=>'เคยรับราชการทหาร','ได้รับการยกเว้น'=>'ได้รับการยกเว้น'));
			$xcrud->change_type('staffDriver','multiselect','',array('รถยนต์'=>'รถยนต์','รถจักรยานยนต์'=>'รถจักรยานยนต์'));
			$xcrud->validation_required('staffFName,staffLName')->validation_required('staffIDCard',13);
			$xcrud->validation_pattern('staffIDCard,staffHight,staffWeight,staffTel,zipCode,zipCodeNow','numeric')->validation_pattern('staffEmail','email');


			$xcrud->field_callback('staffOldPreName','getLinkChangeName');


			$data['id'] = $id;
			$data['html'] = $xcrud->render('edit',$id);
			$data['title'] = "ทะเบียนประวัติ";
	        $this->template->load("template/admin",'form_edit', $data);


		}
	}

	/*public function workInfo(){



		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		$rs = $this->db->get_where('tbl_staff',array('ID'=>$id));
		$ids = ($rs->num_rows() > 0) ? $rs->row()->staffID : '';

		// select table//
		$xcrud->table('tbl_staff')->where('ID =', $id);


		$xcrud->relation('positionID','tbl_position','positionID','positionName');
		$xcrud->relation('rankID','tbl_rank','rankID','rankName');
		$xcrud->relation('orgID1','tbl_org_chart','orgID','orgName','tbl_org_chart.upperOrgID = \'0\'');
		$xcrud->relation('orgID2','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\'','','','','','upperOrgID','orgID1');
		$xcrud->relation('orgID','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\'','','','','','upperOrgID','orgID2');

		//$xcrud->relation('orgID','tblorgchart','orgID','orgName');
		$xcrud->relation('staffPreName','tbl_prefix','prefixID','prefixName');
		$xcrud->relation('staffPreNameEN','tbl_prefix','prefixID','prefixName');
		$xcrud->relation('staffOldPreName','tbl_prefix','prefixID','prefixName');
		$xcrud->relation('workStatusID','tbl_work_status','workStatusID','workStatusName');
		$xcrud->relation('workPlaceID','tbl_work_place','workPlaceID','workPlaceName');
		//// List /////
		$col_name = array(
			'staffID' => 'รหัสพนักงาน',
			'staffPreName' => 'คำนำหน้าชื่อ',
			'staffFName' => 'ชื่อ',
			'staffLName' => 'นามสกุล',
			'staffIDCard' => 'เลขประจำตัวประชาชน',
			'staffPreNameEN' => 'คำนำหน้าชื่อ (EN)',
			'staffFNameEN' => 'ชื่อ (EN)',
			'staffLNameEN' => 'นามสกุล (EN)',
			'staffOldPreName' => 'คำนำหน้าชื่อ (เดิม)',
			'staffOldFName' => 'ชื่อ (เดิม)',
			'staffOldLName' => 'นามสกุล (เดิม)',
			'staffBirthday' => 'วันเกิด',
			'staffGender' => 'เพศ',
			'staffBlood' => 'หมู่เลือด',
			'staffImage' => 'รูปภาพ',
			'rankID' => 'ระดับ',
			'positionID' => 'ตำแหน่ง',
			'orgID'	=> 'แผนก',
			'staffDateWork' => 'วันที่บรรจุ',
			'workStatusID' => 'สถานะภาพการทำงาน',
			'workPlaceID' => 'สถานที่ปฏิบัติงาน',
			'orgID1'	=> 'ฝ่าย / สำนักงาน',
			'orgID2'	=> 'กอง / กลุ่มงาน',
			'staffImage'	=> 'รูป',
			'staffBirthday'	=> 'วันเกิด',
			'staffBirthday'	=> 'วันเกิด',
			'staffHight'	=> 'ส่วนสูง',
			'staffWeight' => 'น้ำหนัก'

		);

		$xcrud->columns('staffID,staffFName,rankID,positionID,orgID1');
		$xcrud->label($col_name);
		$xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
		$xcrud->column_width('staffID','5%');
		/*$xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
		$xcrud->column_width('StaffID','10%');
		$xcrud->unset_view()->unset_remove();
		$xcrud->button('javascript:;','1','fa fa-print','btn-default',array('onclick'=>'javascript:showWork(\'{StaffID}\')'));
		$xcrud->button('javascript:;','2','fa fa-print','btn-primary',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','3','fa fa-print','btn-success',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','4','fa fa-print','btn-info',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','5','fa fa-print','btn-warning',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','6','fa fa-print','btn-danger',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','7','fa fa-print','btn-default',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','8','fa fa-print','btn-primary',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','9','fa fa-print','btn-success',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','10','fa fa-print','btn-info',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));*/

		//// Form //////
	/*	$xcrud->pass_var('staffID',$ids);
		$xcrud->fields('staffImage,staffID,staffFName,staffLName,rankID,positionID,orgID1,orgID2,orgID,staffDateWork,staffBirthday,workPlaceID,workStatusID');
		//$xcrud->subselect('staffFName','{staffFName} {staffLName}');

		$xcrud->readonly('staffImage,staffID,staffFName,staffLName,staffBirthday');
		$xcrud->disabled('staffImage');
		$xcrud->change_type('staffImage', 'image', '', array(
	    'width' => 200,
	    'height' => 200,
	    'manual_crop' => true));
		$xcrud->change_type('staffBirthday','text');
		$xcrud->validation_required('rankID,positionID,orgID1,orgID2,orgID,staffDateWork');
		$xcrud->field_callback('staffBirthday','getExpiredDate');

		//$xcrud->validation_required('staffID,staffFName,staffLName');

		//$xcrud->unset_list();

		$xcrud->hide_button('return');

		$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_add()->unset_limitlist();

		$data['html'] = $xcrud->render('edit',$id);
		$data['title'] = "แฟ้มบุคคล";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/admin",'child', $data);

	}


	/*public function userInfo(){
		$id = mysql_real_escape_string($this->uri->segment(3));

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		$xcrud->table('tblstaff');
		$xcrud->where('staffID',$id);

		$xcrud->relation('positionID','tblposition','positionID','positionName');
		$xcrud->relation('rankID','tblrank','rankID','rankName');
		$xcrud->relation('orgID1','tblorgchart','orgID','orgName',array('tblorgchart.upperOrgID'=>NULL));
		$xcrud->relation('orgID2','tblorgchart','orgID','orgName','orgID != upperOrgID','','','','','upperOrgID','orgID1');
		$xcrud->relation('orgID','tblorgchart','orgID','orgName','orgID != upperOrgID','','','','','upperOrgID','orgID2');

		//$xcrud->relation('orgID','tblorgchart','orgID','orgName');
		$xcrud->relation('staffPreName','tblprefix','prefixID','prefixName');
		$xcrud->relation('staffPreNameEN','tblprefix','prefixID','prefixName');
		$xcrud->relation('staffOldPreName','tblprefix','prefixID','prefixName');
		$xcrud->relation('workStatusID','tblworkstatus','workStatusID','workStatusName');
		$xcrud->relation('workPlaceID','tblworkplace','workPlaceID','workPlaceName');
		//// List /////
		$col_name = array(
			'staffID' => 'อายุ',
			'staffPreName' => 'คำนำหน้าชื่อ',
			'staffFName' => 'ชื่อ',
			'staffLName' => 'นามสกุล',
			'staffIDCard' => 'เลขประจำตัวประชาชน',
			'staffPreNameEN' => 'คำนำหน้าชื่อ (EN)',
			'staffFNameEN' => 'ชื่อ (EN)',
			'staffLNameEN' => 'นามสกุล (EN)',
			'staffOldPreName' => 'คำนำหน้าชื่อ (เดิม)',
			'staffOldFName' => 'ชื่อ (เดิม)',
			'staffOldLName' => 'นามสกุล (เดิม)',
			'staffBirthday' => 'วันเกิด',
			'staffGender' => 'เพศ',
			'staffBlood' => 'หมูโลหิต',
			'staffImage' => 'รูปภาพ',
			'rankID' => 'ระดับ',
			'positionID' => 'ตำแหน่ง',
			'orgID'	=> 'แผนก',
			'staffDateWork' => 'วันที่บรรจุ',
			'workStatusID' => 'สถานะภาพการทำงาน',
			'workPlaceID' => 'สถานที่ปฏิบัติงาน',
			'orgID1'	=> 'ฝ่าย / สำนักงาน',
			'orgID2'	=> 'กอง / กลุ่มงาน',
			'staffImage'	=> 'รูป',
			'staffBirthday'	=> 'วันเกิด',
			'staffHight'	=> 'ส่วนสูง',
			'staffWeight' => 'น้ำหนัก'

		);

		$xcrud->columns('staffID,staffFName,rankID,positionID,orgID');
		$xcrud->label($col_name);
		/*$xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
		$xcrud->column_width('StaffID','10%');
		$xcrud->unset_view()->unset_remove();
		$xcrud->button('javascript:;','1','fa fa-print','btn-default',array('onclick'=>'javascript:showWork(\'{StaffID}\')'));
		$xcrud->button('javascript:;','2','fa fa-print','btn-primary',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','3','fa fa-print','btn-success',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','4','fa fa-print','btn-info',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','5','fa fa-print','btn-warning',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','6','fa fa-print','btn-danger',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','7','fa fa-print','btn-default',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','8','fa fa-print','btn-primary',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','9','fa fa-print','btn-success',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));
		$xcrud->button('javascript:;','10','fa fa-print','btn-info',array('onclick'=>'javascript:showEdit(\'{StaffID}\')'));*/

		//// Form //////
	/*	$xcrud->fields('staffImage,staffIDCard,staffPreName,staffFName,staffLName,staffPreNameEN,staffFNameEN,staffLNameEN,staffGender,staffBirthday,staffID,staffHight,staffWeight,staffBlood');
		//$xcrud->subselect('staffFName','{staffFName} {staffLName}');
		$xcrud->readonly('staffID');
		$xcrud->change_type('staffImage', 'image', '', array(
	    'width' => 200,
	    'height' => 200,
	    'manual_crop' => true));
	    $xcrud->change_type('staffID','text');
		$xcrud->change_type('staffGender','radio','',array('m'=>'ชาย','f'=>'หญิง'));
		$xcrud->change_type('staffBlood','radio','',array('A'=>'A','B'=>'B','AB'=>'AB','O'=>'O'));
		$xcrud->validation_required('staffFName,staffLName')->validation_required('staffIDCard',13);
		$xcrud->validation_pattern('staffIDCard,staffHight,staffWeight','numeric');

		$xcrud->field_callback('staffID','getAge');

		//$xcrud->unset_list();
		$xcrud->hide_button('return');


		//$contactInfo = $xcrud->nested_table('staff','orderNumber','orderdetails','orderNumber'); // 2nd level

		$data['html'] = $xcrud->render('edit',$id);
		$data['title'] = "ข้อมูลส่วนตัว";
        $this->template->load("template/admin",'main', $data);

	}
	/*public function index(){

		$this->codeignitercrud->table ( 'tbl_staff' );
		$this->codeignitercrud->tableAlias ( 'ข้อมูลพนักงาน' );
		$this->codeignitercrud->theme('bootstrap');
		$this->codeignitercrud->addNoCol ( true );

		$this->codeignitercrud->cols ( array(
			'FName',
			'LName'
		) );

		$this->codeignitercrud->alias('FName','ชื่อ')
							  ->alias('LName','นามสกุล')
							  ->alias('IDCard','เลขประจำตัวประชาชน')
							  ->alias('PreName','คำนำหน้าชื่อ');

		$this->codeignitercrud->hide('excel')
							  ->hide('csv')
							  ->hide('print')
							  ->hide('copy')
							  ->hide('edit')
							  ->hide('delete');

        $html = $this->codeignitercrud->fetch();

		$this->template->load("template/admin","main",array(
			'html'=>$html
		) );

	}


  public function submitLogin(){

        if(!empty($_POST['username']) && !empty($_POST['password'])){
            $username = trim($this->input->post('username',true));
            $password = trim($this->input->post('password',true));
            $ru = $this->user_model->authPassword($username,$password);

            if($ru){

				$this->session->set_userdata("login", true);
                $this->session->set_userdata("userID", trim($ru->row()->userID));
                $this->session->set_userdata("userName", trim($ru->row()->userName));
                $this->session->set_userdata("roleID", trim($ru->row()->roleID));
  		$orderdetails = $xcrud->nested_table('products','orderNumber','orderdetails','orderNumber'); // 2nd level
                redirect('dashboard','refresh');


            }else{
				redirect('auth','refresh');

           }

        }else
			redirect('auth','refresh');


	}*/

/*	public function edit(){
		$id = $this->uri->segment(3);
		$data['r'] = $this->staff_model->get($id);
		$data['rank_ddl'] = listData('tblrank','rankName','rankID');

		$this->template->load("template/admin","form",$data);
	}
	*/

	public function passwordreset(){
		$token = $this->input->get('token');
		if(!empty($token)){
			$staff = $this->staff_model->getStaff($token);
			$data['staff'] = $staff[0];
			$this->template->load("template/admin","passwordreset_form",$data);
		}
	}

	public function passwordreset_submit(){
		$staffID 	= $this->input->post('staffID');
		$new_pass   = $this->input->post('new_pass');
        $new_pass2  = $this->input->post('new_pass2');
        $back_url=site_url('reg');  
        
        if(!empty($staffID) && !empty($new_pass) && !empty($new_pass2)){
	        if($new_pass == $new_pass2){
	        	$data =  array(
	                        'userCredential' => hash('sha256', $new_pass)
	                      );
	        	$updatepass = $this->staff_model->updatepassword($staffID,$data);
	        	if($updatepass){
	                    echo "<script>alert('เปลี่ยนรหัสผ่านสำเร็จ');window.location='$back_url';</script>"; 
	            }else{
	                    echo "<script>alert('คำสั่งผิดพลาด กรุณาทำรายการใหม่อีกครั้ง');history.back();</script>";
	            }
	        }else{
	        	echo "<script>alert('รหัสผ่านที่ท่านป้อนไม่ตรงกัน กรุณาตรวจสอบใหม่อีกครั้ง');history.back();</script>";
	        }
	    }else{
	    	echo "<script>alert('คำสั่งผิดพลาด กรุณาทำรายการใหม่อีกครั้ง');window.location='$back_url';</script>";
	    }
	}

}
/* End of file reg.php */
/* Location: ./application/module/reg/reg.php */
