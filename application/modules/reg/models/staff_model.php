<?php
class Staff_model extends MY_Model{

    function __construct() {    	
        parent::__construct();
        $this->_table = 'tbl_staff';
        $this->_pk = 'staffID';
    }

    function updateSalary($staffID, $salary, $wage='')
    {
        $wage != '' ? $data['wage'] = $wage : '';
        $data['salary'] = $salary;
        $this->db->where('staffID',$staffID);
        $this->db->update('tbl_staff', $data);
    }


    function getStaff($staffID=''){
        $sql="select ID,staffID, staffPreName, staffFname, staffLname from tbl_staff where ID='$staffID'";
        $query = $this->db->query($sql);
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }

    public function updatepassword($userID,$data){
            $this->db->set($data);
            $this->db->where('userID',$userID);
            $query = $this->db->update('tbl_user');
            return $query;
    }
}
/* End of file staff_model.php */
/* Location: ./application/module/reg/models/staff_model.php */