<?php

class Staffcontract extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		$this->setModel('staff_contract_model');

		$this->load->model('contract_type_model');
		$this->load->model('contract_field_model');
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		// select table//
		$xcrud->table('tbl_staff_contract')->where('staffID',$id);
		$xcrud->relation('contractType','tbl_contract_type','contracttypeID','contracttypeType');
		//// List /////
		$col_name = array(
			'contractNumber' => 'เลขที่',
			//'contractDate' => 'วันที่ทำสัญญา',
			'contractType' => 'อายุสัญญา',
			'contractField' => 'ระยะเวลาของสัญญา',
			'contractID'	=> 'อายุสัญญา'
		);

		$xcrud->columns('contractNumber,contractType,contractField');
		//$xcrud->column_pattern('contractField','{staffID}');
		//$xcrud->column_callback('contractDate','toBDDate');
		$xcrud->column_callback('contractType','calAgeContract');
		$xcrud->column_callback('contractField','calTimeContract');
		
		$xcrud->label($col_name);

		$xcrud->button(site_url().'staffcontract/upload/index?token={contractID}','อัพโหลด','fa fa-upload','btn-primary'); 
		$xcrud->button(site_url().'staffcontract/print_contract/index?token={contractID}','พิมพ์','fa fa-print','btn-default'); 
		$xcrud->button(site_url().'staffcontract/view/index?token={contractID}','ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
		$xcrud->button(site_url().'staffcontract/edit/index?token={contractID}','แก้ไข','glyphicon glyphicon-edit','btn-warning'); 
		
		// End List//

		//// Form //////
		//$xcrud->pass_var('staffID',$id);
		//$xcrud->fields('contractNumber,contractDate,contractType');
		//$xcrud->field_callback('contractID','calAgeContract');
		//$xcrud->fields('contractNumber,contractDate,contractStartDate,contractType');
		//$xcrud->validation_required('contractNumber,contractDate');
		
		// End Form//

		$xcrud->unset_add()->unset_view()->unset_edit();
		
		$data['html'] = $xcrud->render();
		$data['title'] = "สัญญาจ้าง";
		$data['staffName'] = getStaffName($id);
		$data['id'] = $id;
        $this->template->load("template/tab",'main', $data);

	}

	public function add(){

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));
		
		$data['title'] = "สัญญาจ้าง";
		$data['staffName'] = getStaffName($id);
		$data['id'] = $id;
        $this->template->load("template/tab",'form', $data);
	}


	public function submit(){

		if(!empty($_POST['staffID'])){

			$staffID				= 	mysql_real_escape_string(trim($this->input->post('staffID',true)));
			$contractID				= 	mysql_real_escape_string(trim($this->input->post('contractID',true)));
			$contractNumber			= 	mysql_real_escape_string(trim($this->input->post('contractNumber',true)));
			//$contractDate			= 	mysql_real_escape_string(trim($this->input->post('contractDate',true)));
			$contractType			= 	mysql_real_escape_string(trim($this->input->post('contractType',true)));
			$field 					= 	$this->input->post('field');
			$fieldvalue				= 	$this->input->post('fieldvalue');

			$guarantee = '';
			$arr_field = array();
			for($i=0; $i<count($field); $i++){
				
				//if(!empty($fieldvalue[$i])){
					$arr_field[] = array('id'=>$field[$i],'value'=>$fieldvalue[$i]);
					if($field[$i] == '14'){
						$guarantee = $fieldvalue[$i];
					}
				//}		
			}
			// validate guarantee //

			$flg = true;
			if(!empty($guarantee)){
				$r = $this->contract_type_model->get_by(array('contracttypeID'=>$contractType),true);
				if(!empty($r)){
					$numPerson = $r->numPerson;
					$rs = $this->staff_contract_model->get_by(array('contractType'=>$contractType));
					$count = 0;
					foreach($rs as $row){
						$arr_f = json_decode($row->contractField);
						foreach($arr_f as $rowf){
							if($rowf->id == '14' && $rowf->value == $guarantee)
								$count++;
						}
						
					}
					if($count >= $numPerson)
						$flg = false;
				}
			}
			
			if($flg){
				$arr = array(
					'staffID'			=> $staffID,
					'contractNumber'	=> $contractNumber,
					//'contractDate'		=> toCEDate($contractDate),
					'contractType'		=> $contractType,
					'contractField'		=> json_encode($arr_field)
				);	

				if($this->save($arr,$contractID)){
					redirect('staffcontract/index?token='.$staffID);
				}
			}else{
				header('Content-Type: text/html; charset=utf-8');
				echo"<script language=\"JavaScript\">";
				echo"alert('ผู้ค้ำประกันค้ำประกันครบแล้ว');";
				echo"window.location='".site_url()."staffcontract/add/index?token=".$staffID."'";
				echo"</script>";
			}
			
			/*$model = getModel();
			if(!empty($contractID)){
				// Update //
				if(!empty($this->$model->save($arr,$contractID))){
					redirect('staffcontract/index?token='.$staffID);
				}
			}else{
				// Insert //
				if(!empty($this->$model->save($arr))){
					redirect('staffcontract/index?token='.$staffID);
				}

			}*/
		}
	}

	public function view(){
		if(!empty($_GET['token'])){

			$contractID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$data['r'] = $this->staff_contract_model->get_by(array('contractID'=>$contractID),true);
			
			$id = (!empty($data['r'])) ? $data['r']->staffID : "";

			$data['title'] = "สัญญาจ้าง";
			$data['staffName'] = getStaffName($id);
			$data['id'] = $id;
			$data['contractID'] = $contractID;
	        $this->template->load("template/tab",'form', $data);
		}	
	}

	public function edit(){
		if(!empty($_GET['token'])){

			$contractID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$data['r'] = $this->staff_contract_model->get_by(array('contractID'=>$contractID),true);
			
			$id = (!empty($data['r'])) ? $data['r']->staffID : "";

			$data['title'] = "สัญญาจ้าง";
			$data['staffName'] = getStaffName($id);
			$data['id'] = $id;
			$data['contractID'] = $contractID;
	        $this->template->load("template/tab",'form', $data);
		}	
	}

	public function upload(){
		if(!empty($_GET['token'])){

			// get id contract staff //
			$contractID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$data['r'] = $this->staff_contract_model->get_by(array('contractID'=>$contractID),true);
			
			$id = (!empty($data['r'])) ? $data['r']->staffID : "";
			

	        $this->load->helper('xcrud_helper');
			$xcrud = xcrud_get_instance();

			// select table//
			$xcrud->table('tbl_staff_contract')->where('contractID',$contractID);

			$col_name = array(
				'contractFile' => 'ไฟล์สัญญาจ้าง',
				'contractType' => 'สถานะปิดงาน'
			);
				
			$xcrud->label($col_name);

			//// Form //////
			$xcrud->fields('contractFile,contractType');
			//$xcrud->change_type("contractStatus",'select','',array('0'=>'รอปิดงาน','1'=>'เรียบร้อย'));
			$xcrud->field_callback('contractType','getContractStatus');
			$xcrud->change_type('contractFile', 'file', '', array('not_rename'=>true));
			
			$xcrud->unset_numbers()->unset_pagination()->unset_search()->unset_add()->unset_limitlist();
			// End Form//

			$data['html'] = $xcrud->render('edit',$contractID);
			$data['title'] = "สัญญาจ้าง";
			$data['staffName'] = getStaffName($id);
			$data['id'] = $id;
			$data['contractID'] = $contractID;
	        $this->template->load("template/tab",'upload', $data);


		}	
	}

	public function print_contract(){
		if(!empty($_GET['token'])){

			// get id contract staff //
			$contractID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$rs = $this->staff_contract_model->get_by(array('contractID'=>$contractID),true);
			
			$id = (!empty($rs)) ? $rs->staffID : "";
			$arr_s = (!empty($rs)) ? json_decode($rs->contractField) : "";
			$contractTypeID = (!empty($rs)) ? $rs->contractType : "";

			$r = $this->contract_type_model->get_by(array('contracttypeID' => $contractTypeID),true);
	        $html = (!empty($r)) ? $r->contracttypeDetail : "";
	        $year = (!empty($r)) ? $r->contracttypeYear : "";
	        $arr_field = array(); 
	        
	        $run = 0;
	        $i=0;

			while(strpos($html,"[",$i)){

				$pos = strpos($html,"[",$i);
				$pos_e = strpos($html,"]",$i);
				
				if($pos_e){
					$sub_text = substr($html,$pos,$pos_e-($pos-1));
					$sub_field = substr($html,$pos+1,$pos_e-($pos+1));
					$rf = $this->contract_field_model->get_by(array('fieldName'=>$sub_field),true);
					$flg_f = false;

					$html1 = substr($html,0,($pos_e+1));
					$html2 = substr($html,($pos_e+1),(strlen($html)-$pos_e));
					
					if(!empty($rf)){
						for($j=$run; $j< count($arr_s); $j++){
							if($arr_s[$j]->id == $rf->fieldID){
								$field = $rf->fieldValue;
								if($rf->fieldType == '1'){								
									$rv = $this->db->select($field.",".getPK($rf->fieldTable))->from($rf->fieldTable)->where(getPK($rf->fieldTable),$arr_s[$j]->value)->get();
									$value = ($rv->num_rows() > 0) ? $rv->row()->$field : "";
									$html1 = str_replace($sub_text, $value, $html1);	
								}else if($rf->fieldType == '4' || $rf->fieldType == '8'){

									$rb = $this->db->get_where('tbl_staff',array('ID'=>$arr_s[$j]->value));
									$value = ($rb->num_rows() > 0) ? $rb->row()->staffPreName.' '.$rb->row()->staffFName.' '.$rb->row()->staffLName : "";
									$html1 = str_replace($sub_text, $value, $html1);

								}else if($rf->fieldType == '5'){									
									$html1 = str_replace($sub_text, $arr_s[$j]->value, $html1);	
								}else if($rf->fieldType == '7'){
									$list = explode('/',$arr_s[$j]->value);	
									if(!empty($list[2]) && !empty($list[1]) && !empty($list[0]))							
										$html1 = str_replace($sub_text, toFullDateContract($list[2].'-'.$list[1].'-'.$list[0],'th'), $html1);								
								}else if($rf->fieldType == '11'){
									$list = explode('/',$arr_s[$j]->value);	
									if(!empty($list[2]) && !empty($list[1]) && !empty($list[0]))							
										$html1 = str_replace($sub_text, toFullDate($list[2].'-'.$list[1].'-'.$list[0],'th','full'), $html1);								
								}else if($rf->fieldType == '12' || $rf->fieldType == '13'){
									$list = explode('/',$arr_s[$j]->value);	
									if(!empty($list[2]) && !empty($list[1]) && !empty($list[0]))							
										$html1 = str_replace($sub_text, toFullDateContract($list[2].'-'.$list[1].'-'.$list[0],'th'), $html1);								
								}
								
								$flg_f = true;
								$run++;
								break;
							}
						} // end for

						if(!$flg_f){
							if($rf->fieldType == '2'){
								$html1 = str_replace($sub_text, toFullDate(date("Y-m-d"),'th','full'), $html1);
							}else if($rf->fieldType == '3'){									
								$rb = $this->db->get_where('tbl_staff',array('ID'=>$id));
								$value = ($rb->num_rows() > 0) ? $rb->row()->staffBirthday : "";
								$html1 = str_replace($sub_text, calAge($value), $html1);
							}if($rf->fieldType == '9'){

								$html1 = str_replace($sub_text, $year, $html1);
							}else if($rf->fieldType == '10'){									
								$rb = $this->db->get_where('tbl_staff',array('ID'=>$id));
								$value = ($rb->num_rows() > 0) ? $rb->row()->staffBirthday : "";
								$html1 = str_replace($sub_text, toFullDateContract($value,'th'), $html1);
							}
						} // end if
						
						
					}

					if(!$flg_f){
						$html1 = str_replace($sub_text, '{'.$sub_field.'}', $html1);	
					}

					$html = $html1.$html2;
					
				}// end pos_e
					
			}
			$html = str_replace("{", "[", $html);
			$html = str_replace("}", "]", $html);

			$data['html'] = $html;
			$data['title'] = "สัญญาจ้าง";
			$data['staffName'] = getStaffName($id);
			$data['id'] = $id;
			$data['contractID'] = $contractID;
	        $this->template->load("template/tab",'print', $data);
		}
	}

	public function print_pdf(){
		if(!empty($_GET['token'])){

			// get id contract staff //
			$contractID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$rs = $this->staff_contract_model->get_by(array('contractID'=>$contractID),true);
			
			$id = (!empty($rs)) ? $rs->staffID : "";
			$arr_s = (!empty($rs)) ? json_decode($rs->contractField) : "";	
			$contractTypeID = (!empty($rs)) ? $rs->contractType : "";

			$r = $this->contract_type_model->get_by(array('contracttypeID' => $contractTypeID),true);
	        $html = (!empty($r)) ? $r->contracttypeDetail : "";
	        $year = (!empty($r)) ? $r->contracttypeYear : "";
	        $arr_field = array(); 
	        
	        $run = 0;
	        $i=0;
			while(strpos($html,"[",$i)){

				$pos = strpos($html,"[",$i);
				$pos_e = strpos($html,"]",$i);
				
				if($pos_e){
					$sub_text = substr($html,$pos,$pos_e-($pos-1));
					$sub_field = substr($html,$pos+1,$pos_e-($pos+1));
					$rf = $this->contract_field_model->get_by(array('fieldName'=>$sub_field),true);
					$flg_f = false;

					$html1 = substr($html,0,($pos_e+1));
					$html2 = substr($html,($pos_e+1),(strlen($html)-$pos_e));

					if(!empty($rf)){
						for($j=$run; $j< count($arr_s); $j++){
							if($arr_s[$j]->id == $rf->fieldID){
								$field = $rf->fieldValue;
								if($rf->fieldType == '1'){
									
									$rv = $this->db->select($field.",".getPK($rf->fieldTable))->from($rf->fieldTable)->where(getPK($rf->fieldTable),$arr_s[$j]->value)->get();
									$value = ($rv->num_rows() > 0) ? $rv->row()->$field : "";
									$html1 = str_replace($sub_text, $value, $html1);	
								}else if($rf->fieldType == '4'){
									$rb = $this->db->get_where('tbl_staff',array('ID'=>$arr_s[$j]->value));
									$value = ($rb->num_rows() > 0) ? $rb->row()->staffPreName.' '.$rb->row()->staffFName.' '.$rb->row()->staffLName : "";
									$html1 = str_replace($sub_text, $value, $html1);
								}else if($rf->fieldType == '5'){								
									$html1 = str_replace($sub_text, $arr_s[$j]->value, $html1);	
								}else if($rf->fieldType == '7'){								
									$list = explode('/',$arr_s[$j]->value);	
									if(!empty($list[2]) && !empty($list[1]) && !empty($list[0]))							
										$html1 = str_replace($sub_text, toFullDateContract($list[2].'-'.$list[1].'-'.$list[0],'th'), $html1);								
								}else if($rf->fieldType == '11'){
									$list = explode('/',$arr_s[$j]->value);	
									if(!empty($list[2]) && !empty($list[1]) && !empty($list[0]))							
										$html1 = str_replace($sub_text, toFullDate($list[2].'-'.$list[1].'-'.$list[0],'th','full'), $html1);								
								}else if($rf->fieldType == '12' || $rf->fieldType == '13'){
									$list = explode('/',$arr_s[$j]->value);		
									if(!empty($list[2]) && !empty($list[1]) && !empty($list[0]))						
										$html1 = str_replace($sub_text, toFullDateContract($list[2].'-'.$list[1].'-'.$list[0],'th'), $html1);								
								}
								
								$flg_f = true;
								$run++;
								break;
							}
						}

						if(!$flg_f){
							if($rf->fieldType == '2'){
								$html1 = str_replace($sub_text, toFullDate(date("Y-m-d"),'th','full'), $html1);
							}else if($rf->fieldType == '3'){									
								$rb = $this->db->get_where('tbl_staff',array('ID'=>$id));
								$value = ($rb->num_rows() > 0) ? $rb->row()->staffBirthday : "";
								$html1 = str_replace($sub_text, calAge($value), $html1);
							}if($rf->fieldType == '9'){

								$html1 = str_replace($sub_text, $year, $html1);
							}else if($rf->fieldType == '10'){									
								$rb = $this->db->get_where('tbl_staff',array('ID'=>$id));
								$value = ($rb->num_rows() > 0) ? $rb->row()->staffBirthday : "";
								$html1 = str_replace($sub_text, toFullDateContract($value,'th'), $html1);
							}
						}
						
						
					}

					if(!$flg_f){
						$html1 = str_replace($sub_text, '{'.$sub_field.'}', $html1);	
					}		

					$html = $html1.$html2;

				}
						
			}

			$html = str_replace("{", "[", $html);
			$html = str_replace("}", "]", $html);

			$data['html'] = $html;
			$this->load->view('print_pdf',$data);


		}	
	}

}
/* End of file staffcontract.php */
/* Location: ./application/module/staffcontract/staffcontract.php */