<style>
    .error_lbl{
        border-color:red !important;
    }
        .ui-dialog-titlebar-close{
            display:none
        }
        .header-product{
            background-color: pink;
            font-weight: bold;
        }
        .header-product th{
            text-align: center;
            vertical-align: top;
        }
        .btn-none{
            display:none;
        }
        .error_lbl{
          border-color:red;
        }
</style>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
<div class="col-md-12" ng-controller="contractCtrl" data-ng-init="init()">

     <div class="col-separator box col-separator-first col-unscrollable">
        <div class="col-table">
                   <div class="row">
                      <div class="col-sm-12">
                          <section class="panel">
                              <header class="panel-heading">
                                  <!--<h3><?php //echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php //echo $staffName; ?></small></h3>

                                   <a href="<?php //echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php //echo $this->config->item('txt_back')?></a>
                                    -->
                              </header>
                              <div class="panel-body">
                              <form class="form-horizontal " id="staffcontractForm" name="staffcontractForm" method="post" action="<?php echo site_url(); ?>staffcontract/submit" >
                                <input type="hidden" name="staffID" value="<?php echo $id; ?>">
                                <input type="hidden" name="contractID" value="<?php echo !empty($contractID) ? $contractID : ""; ?>">
                                <div class="xcrud">
                                  <div class="xcrud-container">
                                      <div class="xcrud-view">
                                        <div class="form-horizontal">
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">เลขที่*</label>
                                            <div class="col-sm-9">
                                            <?php if($this->uri->segment('2') != 'view'){ ?>
                                            <input type="text" maxlength="50" name="contractNumber" id="contractNumber" value="<?php echo !empty($r) ? $r->contractNumber : ""; ?>" data-type="text" data-required="1" class="xcrud-input form-control">
                                            <?php }else{
                                              echo $r->contractNumber;
                                              } ?>
                                            </div>
                                          </div>
                                          <!--<div class="form-group">
                                            <label class="control-label col-sm-3">วันที่ทำสัญญา*</label>
                                              <div class="col-sm-9">
                                              <?php /*($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="contractDate" value="<?php echo !empty($r) ? toBEDate($r->contractDate) : ""; ?>" data-type="text" data-required="1" class="xcrud-input xcrud-datepicker form-control" data-type="date">
                                                <?php }else{
                                                echo toBEDateThai($r->contractDate);
                                              }*/ ?>
                                              </div>
                                          </div>-->
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">ประเภทสัญญา*</label>
                                            <div class="col-sm-9">
                                               <?php //echo contractType_ddl("contractType",'','class="form-control" ng-change="change()"');?>  
                                               <?php if($this->uri->segment('2') != 'view'){ ?>                                           
                                                  <select 
                                                    ng-init="ddlcontract = {id: ddl.type}"
                                                    ng-model="ddlcontract"
                                                    ng-change="ddl.type = ddlcontract.contracttypeID; change() "
                                                    ng-options="value.contracttypeType for value in myOptions track by value.contracttypeID"
                                                    class="form-control"
                                                    name="contractType" id="contractType">
                                                    
                                                  </select>
                                                 <?php }else{
                                                  echo getContractTypeName($r->contractType);
                                                } ?>  

                                                  
                                            </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-sm-3">อายุสัญญา</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              {{age}}
                                              <?php }else{
                                                    echo getContractTypeAge($r->contractType).' ปี';
                                              } ?>                                               
                                            </div>
                                          </div>

                                         <div class="form-group" >
                                          <label class="control-label col-sm-3">รายละเอียด</label>
                                          <div class="col-sm-9" id="fieldDetail">
                                            

                                          </div>
                                         </div>
                                          
                                      </div>
                                  </div>
                                  <div class="xcrud-top-actions btn-group">
                                      <?php if($this->uri->segment('2') != 'view'){ ?>
                                      <a class="btn btn-primary"  href="javascript:submitForm();">บันทึกและย้อนกลับ</a>
                                      <?php } ?>
                                      <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>staffcontract/index?token=<?php echo $id; ?>'">ย้อนกลับ</a>
                                  </div>
                                  </form>
                                  <!--<div class="col-table-row">
                    
                                    <div class="col-app">
                                          <div class="col-separator-h box"></div>
                                          
                                          
                                          <div class="innerAll bg-gray">
                                              
                                              <form class="form-horizontal " id="staffcontractForm" name="staffcontractForm" method="post" action="<?php echo site_url(); ?>staffcontract/submit" >
                                                    <div class="col-md-8 ">
                                                       <input type="hidden" name="staffID" id="staffID" value="<?php echo (!empty($r)) ? $r->staffID : ""; ?>">
                                                      <div class="form-group">
                                                        <label class="control-label col-sm-3">เลขที่*</label>
                                                        <div class="col-sm-9"><input type="text" maxlength="50" name="contractNumber" value="" data-type="text" data-required="1" class="xcrud-input form-control"></div>
                                                      </div>
                                                      <div class="form-group">
                                                        <label class="control-label col-sm-3">วันที่ทำสัญญา*</label>
                                                        <div class="col-sm-9"><input type="text" maxlength="50" name="contractDate" value="" data-type="text" data-required="1" class="xcrud-input form-control"></div>
                                                      </div>

                                                      <div class="form-group">
                                                          <div class="col-sm-10 col-sm-offset-1">
                                                              <button type="button" class="btn btn-info" onclick="submitForm()">บันทึก</button>
                                                              <button type="button" class="btn btn-warning" onclick="window.location='<?php echo site_url();?>reg'">กลับ</button>
                                                          </div>
                                                      </div>
                                                     
                                              </form>
                                                  
                                               <div class="clearfix border-bottom"></div>
                                          </div>
                                      </div>
                                  </div>-->
                              </div>
                          </section>
                      </div>
                  </div>
            
             
                
        </div>  
      </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css">
<script src="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url(); ?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script>
<script>
  $(function(){
      $('.xcrud-datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            changeMonth: true,
            changeYear: true
          });

      

  })

  function submitForm(){
    if($('#staffcontractForm').valid())
      $('#staffcontractForm').submit();
  }
  
  $("#staffcontractForm").validate({
            errorClass: "error_lbl",
            rules: {
                    contractNumber:"required",
                    contractType:"required"
            }, messages: {
                    contractNumber:"",
                    contractType:""
            }
    });
  
</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        myApp.controller('contractCtrl', ['$scope', '$location','$http', function($scope, $location,$http) {

         
          $scope.init = function () {
              //$scope.ddlcontract = <?php echo (!empty($r)) ? $r->contractType : ""?>;
              


              if("<?php echo $this->uri->segment('2') ?>" == "view"){
               
                $http.get("<?php echo site_url()?>contracttype/getField", { params: { "id": '<?php echo (!empty($r)) ? $r->contractType : ""?>', "contract_staff" : '<?php echo (!empty($contractID)) ? $contractID : ""?>',"mode" : "view" } }).success(function (data, status, header, config) {
                        $('#fieldDetail').html(data);
                       
                    }).error(function (data, status, headers, config) {
                        
                    });
              }else{

                  $http.get("<?php echo site_url()?>contracttype/getJsonData").success(function (data, status, header, config) {
                    $scope.myOptions = data;
                    $scope.ddlcontract.contracttypeID = '<?php echo (!empty($r)) ? $r->contractType : ""?>';

                    if($scope.ddlcontract.contracttypeID != ""){
                       $http.get("<?php echo site_url()?>contracttype/getAge", { params: { "id": $scope.ddlcontract.contracttypeID } }).success(function (data, status, header, config) {
                          $scope.age = data;
                         
                      }).error(function (data, status, headers, config) {
                          
                      });

                      $http.get("<?php echo site_url()?>contracttype/getField", { params: { "id": $scope.ddlcontract.contracttypeID, "contract_staff" : '<?php echo (!empty($contractID)) ? $contractID : ""?>' } }).success(function (data, status, header, config) {
                          $('#fieldDetail').html(data);
                          $('.xcrud-datepicker').datepicker({
                            dateFormat: 'dd/mm/yy',
                            firstDay: 1,
                            changeMonth: true,
                            changeYear: true
                          });
                         
                      }).error(function (data, status, headers, config) {
                          
                      });
                    }
                   
                }).error(function (data, status, headers, config) {
                    
                });

              }
          };

          
           
          $scope.change = function(){
              /*var fruitName = $.grep($scope.Fruits, function (fruit) {
                      return fruit.Id == fruitId;
                  })[0].Name;*/

              $http.get("<?php echo site_url()?>contracttype/getAge", { params: { "id": $scope.ddlcontract.contracttypeID } }).success(function (data, status, header, config) {
                  $scope.age = data;
                 
              }).error(function (data, status, headers, config) {
                  
              });

              $http.get("<?php echo site_url()?>contracttype/getField", { params: { "id": $scope.ddlcontract.contracttypeID, "contract_staff" :'<?php echo (!empty($contractID)) ? $contractID : ""?>'} }).success(function (data, status, header, config) {
                  $('#fieldDetail').html(data);
                   $('.xcrud-datepicker').datepicker({
                    dateFormat: 'dd/mm/yy',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true
                  });

              }).error(function (data, status, headers, config) {
                  
              });

          }

        }]);



</script>
