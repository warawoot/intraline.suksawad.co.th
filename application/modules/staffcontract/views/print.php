<style>
.panel-html {
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
    margin: 20px 0;
    min-height:400px;
    padding:20px;

}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <!--<h3><?php //echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php //echo $staffName; ?></small></h3>

                 <a href="<?php //echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php //echo $this->config->item('txt_back')?></a>
                -->
            </header>
            <div class="panel-body">
                <section class="panel">
                    <div class="panel-body panel-html"><?php echo $html; ?></div>

                     <div class="btn-group col-sm-2 col-sm-offset-5">                         
                          <a class="btn btn-primary"  href="#" onclick="window.open('<?php echo site_url();?>staffcontract/print_pdf?token=<?php echo $contractID; ?>')">พิมพ์</a>
                          <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>staffcontract/index?token=<?php echo $id; ?>'">ย้อนกลับ</a>
                      </div>

                </section>
                      
            </div>
        </section>
    </div>
</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);




</script>