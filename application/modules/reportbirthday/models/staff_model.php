<?php
class Staff_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff';
            $this->_pk = 'staffID';
        }

        public function getByBirthday($month,$ag){
             if(!empty($month) || !empty($ag)){

               // $year -= 543;
            
                $sql = "SELECT *
                        FROM tbl_staff 
                        WHERE tbl_staff.status <> 2 and tbl_staff.staffBirthday != '' ";
                /*if(!empty($day)){
                    $sql.=" DAY(staffBirthday) = '$day' AND ";
                }
			    */	

                if(!empty($month)){
                     $sql.="AND MONTH(staffBirthday) = '$month' ";
                  
                }

                if(!empty($ag)){
                     $sql.="and tbl_staff.orgID in ($ag) ";

                }
               
                // and status <> 2  
                //          and tbl_staff.staffBirthday IS NOT NULL
				/*
                if(!empty($year)){
                    $sql.=" YEAR(staffBirthday) = '$year' AND ";
                }
                */
				$sql.= " ORDER BY DAY(staffBirthday),YEAR(staffBirthday)  ASC";
                $sql = substr($sql,0,-4);
                $r = $this->db->query($sql);

                //echo $sql;
                return ($r->num_rows() > 0) ? $r : NULL;
            }
            
        }

         public function getOrg(){            
                 $sql = "SELECT oc.orgID, oc.orgName
                         FROM tbl_org_chart oc
                         WHERE upperOrgID=2";    
                $r = $this->db->query($sql);
                return ($r->num_rows() > 0) ? $r->result() : NULL;     
        }
        

	
}
/* End of file staff_model.php */
/* Location: ./application/module/Reportstatusemployees/models/staff_model.php */