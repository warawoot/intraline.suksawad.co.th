<?php 

	$th_month = array(''=>'-- กรุณาเลือก --',
		'01'=>'มกราคม',
		'02'=>'กุมภาพันธ์',
		'03'=>'มีนาคม',
		'04'=>'เมษายน',
		'05'=>'พฤษภาคม',
		'06'=>'มิถุนายน',
		'07'=>'กรกฎาคม',
		'08'=>'สิงหาคม',
		'09'=>'กันยายน',
		'10'=>'ตุลาคม',
		'11'=>'พฤศจิกายน',
		'12'=>'ธันวาคม');
   // $day_arr = array("","31","29","31","30","31","30","31","31","30","31","30","31");  
   ?>
	<h5 style="text-align:center;">
		<br>
			รายงานพนักงานที่เกิด ประจำเดือน <?php echo $th_month[$month]; ?>
		
	</h5>
	<br>



	
	<div class="row">
	<form id="formMain" action="<?php echo site_url();?>reportbirthday" method="get"  enctype="multipart/form-data" class="form-horizontal">
		<div class="col-md-12" style="margin-bottom: 5px">
		<label>ประจำเดือน</label>
		<select class="form-control" style="max-width: 200px;" id="month" name="month">
			<?php foreach($th_month as $key=>$val){ ?>
			<option value="<?php echo $key; ?>" <?php echo ($key == $month) ? "selected" : ""; ?>><?php echo $val; ?></option>
			<?php } ?>
			
		</select>
		</div>
		<div class="col-md-12" style="margin-bottom: 5px">
		<label>หน่วยงาน</label>
		<select class="xcrud-input form-control" data-type="select" name="orgID" id="orgID"  style="max-width: 200px;"> 

            <option value="">----กรุณาเลือก-----</option>
            <?php 
                echo getDropdownTree('0','','2',$this->input->get('orgID'));
            ?>
        </select>
        </div>
        <div class="col-md-12">
        		<button type="submit" class="btn btn-warning"><i class="fa fa-search"></i> ค้นหา</button>
        		<!--<a class="btn btn-warning"  href="javascript:search();" ><i class="fa fa-search"></i> ค้นหา </a>-->
       	</div>
    </form>
    </div>

	

	
	<a class="btn btn-danger"  href="javascript:print_excel();" style="float:right;"><i class="fa fa-table"></i> Excel </a>
	<a class="btn btn-success"  href="javascript:print_pdf();" style="float:right; margin-right:10px;"><i class="fa fa-print"></i> PDF </a>

	<br><br>
	<p><b>ข้อมูล ณ วันที่ <?php echo toFullDate(date("Y-m-d"),'th');?></b></p>
	<div class="xcrud-list-container">
	<table class="xcrud-list table table-striped table-hover table-bordered">
	
		<tr >
			<th >ลำดับ</th>
			<th >รหัสพนักงาน</th>
			<th >อัตราที่</th>
			<th >ชื่อ-นามสกุล</th>
			<th >ตำแหน่ง</th>
			<th >ระดับ</th>
			<th >แผนก</th>
			<th >กอง</th>
			<th >ฝ่าย</th>
			<th>วันเกิด</th>
			
		</tr>
		
		<?php 
		$i=0;
		if($r != NULL){
			foreach($r->result() as  $row){ 
					$i++;
				?>
				<tr ><!-- td 10 ตัว-->
					<td><?php echo $i; ?></td>
					<td><?php echo $row->staffID;?></td>
					<td><?php echo getSegByWork($row->ID);?></td>
					<td><?php echo $row->staffPreName.' '.$row->staffFName.' '.$row->staffLName; ?></td>
					<td><?php echo getPositionByWork($row->ID);?></td>
					<td><?php echo getRankByWork($row->ID);?></td>
					<td><?php echo getOrgByWork($row->ID); ?></td>
					<td><?php echo getOrg2ByWork($row->ID);?></td>
					<td><?php echo getOrg1ByWork($row->ID);?></td>
					<td><?php echo toFullDate($row->staffBirthday,'th','full'); ?></td>
				</tr>
		<?php  } }else{
		
			echo '<tr><td colspan="10" style="text-align:center">ไม่พบข้อมูล</td></tr>';
		}?>
		
		
	</table>
	</div>



<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function search(){
        	window.location='<?php echo site_url();?>reportbirthday?&month='+$('#month :selected').val();
        }


        $('#month').change(function(){
        	day_arr = ["","31","29","31","30","31","30","31","31","30","31","30","31"];  
   			day = $('#day :selected').val();
        	month = $('#month :selected').val();
        	count = day_arr[parseInt(month)];
        	sel = (day == "") ? 'selected' : '';
        	html = "<option value='' "+sel+">-- กรุณาเลือก --</option>";

			for(i=1; i<=count; i++){ 

				sel = (day == i) ? 'selected' : '';
				html+='<option value="'+i+'" '+sel+'>'+i+'</option>';
			 } 
			 $("#day").html(html);
        })
        

        function print_pdf(){
        	window.open('<?php echo site_url();?>reportbirthday/print_pdf?month='+$('#month :selected').val()+'&orgID='+$('#orgID :selected').val());
        }

        function print_excel(){
        	window.open('<?php echo site_url();?>reportbirthday/print_excel?month='+$('#month :selected').val()+'&orgID='+$('#orgID :selected').val());
        }
   
</script>