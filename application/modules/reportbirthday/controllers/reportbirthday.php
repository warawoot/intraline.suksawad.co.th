<?php

class Reportbirthday extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('staff_model');
		$this->load->model('organization/organization_model');
		
	}
	
	public function index(){

		//$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		//$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);

		$data['orgID'] = $this->input->get('orgID');
		$ag = $this->organization_model->getSubOrg('orgID');
		$agencies = $this->input->get_post('agencies');
		$data['r'] = $this->staff_model->getByBirthday($data['month'],$data['orgID']);
		
		$data['title'] = "รายงานพนักงานครบกำหนดสัญญาจ้าง";
		//echo $ag = $this->db->last_query();
        $this->template->load("template/admin",'reportbirthday', $data);

	}

	public function print_pdf(){

		//$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		//$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		$data['orgID'] = $this->input->get('orgID');
		$ag = $this->organization_model->getSubOrg('orgID');
		$agencies = $this->input->get_post('agencies');
		$data['r'] = $this->staff_model->getByBirthday($data['month'],$data['orgID']);


		$data_r['html'] = $this->load->view('print',$data,true);
		
		$this->load->view('print_pdf',$data_r);

	}

	public function print_excel(){

		//$data['day'] = (isset($_GET['day'])) ? $this->input->get('day') : date("d");
		$data['month'] = (isset($_GET['month'])) ? $this->input->get('month') : date("m");
		//$data['year'] = (isset($_GET['year'])) ? $this->input->get('year') : (date("Y")+543);
		$data['orgID'] = $this->input->get('orgID');
		$agencies = $this->input->get_post('agencies');
		$ag = $this->organization_model->getSubOrg('orgID');
		$data['r'] = $this->staff_model->getByBirthday($data['month'],$data['orgID']);

		
		$this->load->view('print_excel',$data);

	}


}
/* End of file reportbirthday.php */
/* Location: ./application/module/reportbirthday/reportbirthday.php */