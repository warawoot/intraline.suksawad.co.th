<?php

class Export extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		$this->load->model('staff_check_yearly_model');
	}
	
	public function index(){

		
		
		$this->load->helper('xcrud_helper');
        $xcrud = xcrud_get_instance();

        $xcrud->table('tbl_staff');
        $xcrud->relation('positionID','tbl_position','positionID','positionName');

        $col_name = array(
            'staffID' => 'รหัสพนักงาน',  
            'staffFName' => 'ชื่อ',
            'rankID' => 'ระดับ',
            'positionID' => 'ตำแหน่ง',
            'orgID' => 'ฝ่าย / สำนักงาน'
           

        );
        $xcrud->columns('staffID,staffFName,rankID,positionID,orgID');
        $xcrud->label($col_name);
        $xcrud->column_pattern('staffFName','{staffPreName} {value} {staffLName}');
        $xcrud->column_callback('rankID','calRankWork');
        $xcrud->column_callback('positionID','calPositionWork');
        $xcrud->column_callback('orgID','calOrgIDWork');

        $xcrud->column_width('staffID','5%');

        $xcrud->unset_add()->unset_view()->unset_edit()->unset_remove();
        $xcrud->button(site_url().'export/detail/?token={staffID}','export','fa fa-file','btn-success');
       
        $data['html'] = $xcrud->render();
        $data['title'] = "Export รายงานตรวจสุขภาพ";
        $this->template->load('template/admin', 'main',$data);


		//$data['title'] = "Export รายงานตรวจสุขภาพ";
        //$this->template->load("template/admin",'main', $data);

	}

	public function detail(){
		if(!empty($_GET['token'])){
			$token = $this->input->get('token');

			$this->load->helper('xcrud_helper');
        	$xcrud = xcrud_get_instance();

	        $xcrud->table('tbl_staff_check_yearly')->where('staffID',$token);

	        $col_name = array(
	            'checkDate' => 'วันที่ตรวจ',  
	            'staffID' => 'รายการตรวจ'
	           

	        );
	        //$xcrud->columns('staffID,staffFName,rankID,positionID,orgID');
	        $xcrud->label($col_name);
	        $xcrud->column_pattern('staffID','ผล');
	        //$xcrud->column_callback('rankID','calRankWork');
	        //$xcrud->column_callback('positionID','calPositionWork');
	        //$xcrud->column_callback('orgID','calOrgIDWork');

	        //$xcrud->column_width('staffID','5%');

	        $xcrud->unset_add()->unset_view()->unset_edit()->unset_remove();
	        
	        $data['html'] = $xcrud->render();
	        $data['title'] = "Export รายงานตรวจสุขภาพ";
	        $data['staffID'] = $token;
	        $this->template->load('template/admin', 'detail',$data);

		}
	}

	function exportcsv(){
		
		$data['r'] = $this->staff_check_yearly_model->get();

		$data['fields'] = $this->db->list_fields('tbl_staff_check_yearly');

		 

		$this->load->view('print_csv',$data);
	}

	function exportexcel(){
		
		if(!empty($_GET['token'])){
			$token = $this->input->get('token');

			$data['fields'] = $this->db->list_fields('tbl_staff_check_yearly');

			$data['r'] = $this->staff_check_yearly_model->get_by(array('staffID'=>$token));

			$this->load->view('print_excel',$data);

		}
		
	}

}
/* End of file export.php */
/* Location: ./application/module/export/export.php */