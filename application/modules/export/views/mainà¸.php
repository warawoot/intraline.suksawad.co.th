<style>
.tab-pane,.panel-heading{
    margin-top:30px;
}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3><?php echo !empty($title) ? $title : "Import / Export"; ?></h3>

            </header>
            <div class="panel-body">
               <form action="<?php echo site_url();?>import/importcsv" method="post" enctype="multipart/form-data" name="form1">
                <div class="form-group">
                    <label class="control-label col-sm-2">Export</label>
                    <div class="col-sm-10">
                    <input name="btn_export" type="button" id="btn_export" class="btn btn-success" onclick="print_excel();" value="ดาวน์โหลด Template">                                              
                  </div>
                </div>
                 
                 
                </form>
                
            </div>
        </section>
    </div>
</div>

<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/welfare'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        function print_excel(){
          window.open('<?php echo site_url();?>export/exportcsv');
        }
</script>