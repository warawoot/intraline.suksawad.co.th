<?php

class Absence_condition extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		
		// select table//
		$xcrud->table('tbl_absence_condition');
		$xcrud->relation('absTypeID','tbl_absence_type','absTypeID','absName');
		$xcrud->relation('staffTypeID','tbl_staff_type','typeID','typeName');


		
		
		//// List /////
		$col_name = array(
			'absTypeID' => 'ประเภทการลา',
			'staffTypeID' => 'ประเภทพนักงาน',
			'numDay' => 'สามารถลาได้ทั้งหมด(วัน)',
			'gender' => '',
			'minWorkDay'	=> 'ต้องทำงานมาแล้วอย่างน้อย (วัน)'

		);

		$xcrud->columns('absTypeID,staffTypeID,numDay');
		$xcrud->label($col_name);
		// End List//

		//// Form //////
		$xcrud->fields('absTypeID,staffTypeID,numDay,gender,minWorkDay');
		$xcrud->change_type('gender','radio','',array('0'=>'ไม่มี','m'=>'เฉพาะชาย','f'=>'เฉพาะหญิง'));

		$data['html'] = $xcrud->render();
		

		$data['title'] = "กำหนดเงื่อนไขวันลา";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file absence_condition.php */
/* Location: ./application/module/absence_condition/absence_condition.php */