<?php

class line extends MX_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('line_model');
     	$this->load->model('absence/absence_model');
     	$this->load->model('timeattendance/timeattendance_model');
    }
 
    public function index() {
    }

    public function verifyBox() {
      $lineID = $this->input->get('lineID');
      $ver = date("His");
      $data['ip'] = "192.168.1.199";
      $data['lineID'] = $lineID;
      $this->template->load('template/line', 'verify_box',$data);
    }

    public function getTime() {
      date_default_timezone_set('Asia/Bangkok');
      echo date("H:i:s");
    }

    public function resendnoti() {
      $absData 					= $this->absence_model->getStaffAbsenceNotApprove(date("Y-m-d"));
      //echo $this->db->last_query();
      //echo "<pre>";
      $result = $absData->result();
      //print_R($result);
      foreach ($result as $row) {
        if($row->staffNickName != null && $row->staffNickName != "") {
          $staffNickName = " (".$row->staffNickName.")";
        } else {
          $staffNickName = "";
        }

        $dateStr = toBEDateThai($row->absStartDate);
        $dateEnd = toBEDateThai($row->absEndDate);
        if($row->status_absence==2){
          $timeStr = date("H:i",strtotime($row->absStartDate));
          $timeEnd = date("H:i",strtotime($row->absEndDate));
          $leaveDetail 	= "ลาวันที่ $dateStr\nเวลา $timeStr ถึง $timeEnd\n\n";
        }else{
          $leaveDetail 	= "ตั้งแต่วันที่ $dateStr\nถึงวันที่ $dateEnd\n\n";
        }

        $token = $row->lineToken; 
        $str = 	"\n".
            "แจ้งเตือนซ้ำ\n\n".
            "คุณ".$row->staffName.$staffNickName."\n".
            $row->orgName."\n\n".
            "ขอ".$row->absName."\n".
            $leaveDetail.
            "Link ขออนุมัติ ".base_url()."absence/edit?id=".$row->stfAbsID."\n".
            "ผู้อนุมัติ - คุณ".$row->managerName;
        $res = $this->notify_message($str,$token);
        echo $str.$token.":res=".$res."<br><br>";    
        //$this->notify_message($str,$token);
      } 
    }

    public function checkinRange() {
      $lineID = $this->input->get('lineID');
      $ver = date("His");
      $data['ip'] = "192.168.1.199";
      $data['lineID'] = $lineID;
      $this->template->load('template/line', 'check_inrange',$data);
    }

    public function checkoutRange() {
      $lineID = $this->input->get('lineID');
      $ver = date("His");
      $data['ip'] = "192.168.1.199";
      $data['lineID'] = $lineID;
      $this->template->load('template/line', 'check_outrange',$data);
    }

    public function finish() {
      $data['message_head'] = "intraLiNE-Leave";
      $data['message'] = "บันทึกข้อมูลแล้ว กดปิดเพื่อกลับสู่ Line";
      $this->template->load('template/line', 'message',$data);
    }

    public function lineExist() {
      header("Content-Type: application/json; charset=UTF-8");
      $lineID = $this->input->get_post('lineID');
      $matchStaff = $this->line_model->lineExist($lineID);
      if($matchStaff != null) {
        $arrayResult = array(
          "statusCode" => "200",
          "statusMessage" => "line_exist",
          "staffInfo" => $matchStaff
        );
  			echo json_encode($arrayResult);        
      } else {
        $arrayResult = array(
          "statusCode" => "404",
          "statusMessage" => "line_not_exist"
        );
  			echo json_encode($arrayResult);        
      }
    }

    public function checkin() {
      $lineID = $this->input->get_post('lineID');
      $checkinType = $this->input->get_post('checkinType');
      $inRange = $this->input->get_post('inRange');
      $remark = $this->input->get_post('remark');
      $matchStaff = $this->line_model->lineExist($lineID);
      if($matchStaff != null) {
        $input['empID'] = $matchStaff["ID"];
        $input['checkInType'] = $checkinType;
        $input['checkInTime'] = date('Y-m-d H:i:s');
        $input['inRange'] = $inRange;
        $input['remark'] = $remark;
        if($this->timeattendance_model->checkin($input)) {
          $data['message'] = "บันทึกข้อมูลสำเร็จเวลา ".date('H:i:s');
        } else {
          $data['message'] = "มีปัญหาในการบันทึกข้อมูล";
        }
      } else {
          $data['message'] = "ไม่พบข้อมูล Line ID";
      }
      $data['message_head'] = "intraLiNE-TimeAttendance";
      $this->template->load('template/line', 'message',$data);
    }

    public function checkinJSON() {
      header("Content-Type: application/json; charset=UTF-8");
      $lineID = $this->input->get_post('lineID');
      $checkinType = $this->input->get_post('checkinType');
      $inRange = $this->input->get_post('inRange');
      $matchStaff = $this->line_model->lineExist($lineID);
      if($matchStaff != null) {
        $data['empID'] = $matchStaff["ID"];
        $data['checkInType'] = $checkinType;
        $data['checkInTime'] = date('Y-m-d H:i:s');
        $data['inRange'] = $inRange;
        $data['remark'] = "";
        if($this->timeattendance_model->checkin($data)) {
          $arrayResult = array(
            "statusCode" => "200",
            "statusMessage" => "ok"
          );
        } else {
          $arrayResult = array(
            "statusCode" => "500",
            "statusMessage" => "DB Error"
          );
        }
  			echo json_encode($arrayResult);        
      } else {
        $arrayResult = array(
          "statusCode" => "404",
          "statusMessage" => "line_not_exist"
        );
  			echo json_encode($arrayResult);        
      }
    }

    public function register() {
      $lineID = $this->input->get('lineID');
		  $data['lineID'] = $lineID;
      $matchStaff = $this->line_model->lineExist($lineID);
      if($matchStaff != null) {
        $empID = $matchStaff['ID'];
        $staffID = $matchStaff['staffID'];
        $data['message_head'] = "intraLiNE-Leave";
        $data['message'] = "Line นี้มีการเชื่อมต่อกับรหัสพนักงาน ".$staffID." อยู่แล้ว ไม่ต้องลงทะเบียนอีก<br><br>ถ้าไม่ใช่คุณกรุณาติดต่อผู้ดูแลระบบ";
    		$this->template->load('template/line', 'register_result',$data);
      } else {
    		$this->template->load('template/line', 'register',$data);
      }
    }

    public function register_email() {
      $lineID = $this->input->get('lineID');
		  $data['lineID'] = $lineID;
      $matchStaff = $this->line_model->lineExist($lineID);
      if($matchStaff != null) {
        $empID = $matchStaff['ID'];
        $staffName = $matchStaff['staffFName'].' '.$matchStaff['staffLName'];
        $data['message_head'] = "intraLiNE-Leave";
        $data['message'] = "Line นี้มีการเชื่อมต่อกับคุณ ".$staffName." อยู่แล้ว ไม่ต้องลงทะเบียนอีก<br><br>ถ้าไม่ใช่คุณกรุณาติดต่อผู้ดูแลระบบ";
    		$this->template->load('template/line', 'register_result',$data);
      } else {
    		$this->template->load('template/line', 'register_email',$data);
      }
    }

    public function leave() {
      $lineID = $this->input->get('lineID');
      $matchStaff = $this->line_model->lineExist($lineID);
      if($matchStaff == null) {
        redirect("line/register?lineID=".$lineID, 'refresh');
      } else {
        $data['absence_type']		= $this->absence_model->get_dropdown_all('absTypeID' , 'absName' ,'tbl_absence_type');        
        $data['current_task']		= 'add';  
        $data['var_agencies']		= "";
        
        $data['absDetail']			= '';
        $data['lineID']				  = $lineID;
        $data['empID']				  = $empID = $matchStaff['ID'];
        $data['stfAbsID']			  = '';
        $data['date_start_put']	= '';
        $data['date_end_put']		= '';
        $data['absTypeID']			= '';
        $data['staffName'] 		  = $this->session->userdata('fullName');

        $this->template->load("template/line", "leave",$data);
      }
    }

    public function leave_submit(){

      $stfAbsID			= $this->input->post('id');
      $empID				= $this->input->post('empID');
      $lineID				= $this->input->post('lineID');

      $dropdown_staff		= $this->input->post('dropdown_staff');
      $current_task				= $this->input->post('current_task');

      $absence_type		= $this->input->post('absence_type');
      $date_start			= $this->input->post('date_start');
      $date_end			= !empty($_POST['date_end'])?$this->input->post('date_end'):"";
      $absDetail			= $this->input->post('detail');
      $statusApprove	 	= $this->input->post('statusApprove');

      //ลาเต็มวัน หรือ ลาชั่วโมง
      $status_absence	 =  $this->input->post('status_absence');
      $date_start 		= explode('/',$date_start); //สร้างตัวแปรมาเก็บวันที่
      $strYear_s 			= $date_start[2]-543;
      $strMonth_s			= $date_start[1];
      $strDay_s			= $date_start[0];

      $date_end 	  		= explode('/',$date_end); //สร้างตัวแปรมาเก็บวันที่
      $strYear_e 			= $date_end[2]-543;
      $strMonth_e			= $date_end[1];
      $strDay_e			= $date_end[0];

      $minute	 =  $this->input->post('minute');
      $minute2	 =  $this->input->post('minute2');
      $date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s." ".$minute;

      if($status_absence==2){
        $date_end = $strYear_s."-".$strMonth_s."-".$strDay_s." ".$minute2;
        $leaveDetail = "ลาวันที่ $strDay_s/$strMonth_s/$strYear_s\nเวลา $minute ถึง $minute2\n\n";
      }else{
        $minute = "00:00";
        $minute2 = "00:00";
        $date_end = $strYear_e."-".$strMonth_e."-".$strDay_e." ".$minute2;
        $leaveDetail = "ตั้งแต่วันที่ $strDay_s/$strMonth_s/$strYear_s\nถึงวันที่ $strDay_e/$strMonth_e/$strYear_e\n\n";
      }

      $txtdateInput_start = $date_start;
      $txtdateInput_end 	= $date_end;

      $path = './uploads/absenceFileMedical/';
      $random = rand();

      $filename = $_FILES["absenceFileMedical"]["name"];
      $file = explode(".",$filename);
      $filename_change = $file[0].$random.".".$file[1];
      move_uploaded_file($_FILES['absenceFileMedical']['tmp_name'], $path.iconv('UTF-8','windows-874',$filename_change));
      if($filename ==""){
        $filename_change = "";
      }

      $data		=	array(
              'staffID'=>$dropdown_staff	,
              'absTypeID'=>$absence_type	,
              'absStartDate'=>$date_start,
              'absEndDate'=>$date_end	,
              'absDetail'=>$absDetail,
              'status_absence'=>$status_absence,
              'statusApprove'=>$statusApprove,
              'absenceFile'=>$filename_change
      );

      if($_FILES["absenceFileMedical"]["name"] !=''){
        $data1		=	array(

          'absTypeID'=>$absence_type	,
          'absStartDate'=>$date_start,
          'absEndDate'=>$date_end	,
          'absDetail'=>$absDetail,
          'status_absence'=>$status_absence,
          'statusApprove'=>$statusApprove,
          'absenceFile'=>$filename_change
        );
      }else{
        $data1		=	array(

          'absTypeID'=>$absence_type	,
          'absStartDate'=>$date_start,
          'absEndDate'=>$date_end	,
          'absDetail'=>$absDetail,
          'status_absence'=>$status_absence,
          'statusApprove'=>$statusApprove,
        );
      }

      if($current_task == 'add'){
        $this->absence_model->insert_staff_absence($data);
        define('LINE_API',"https://notify-api.line.me/api/notify");

        $insert_id = $this->db->insert_id();
        $absData = $this->absence_model->getAbsenceData($insert_id);
        if($absData["staffNickName"] != null && $absData["staffNickName"] != "") {
          $staffNickName = " (".$absData["staffNickName"].")";
        } else {
          $staffNickName = "";
        }

        $token = $absData["lineToken"]; 
        $str = 	"\n".
            "คุณ".$absData["staffName"].$staffNickName."\n".
            $absData["orgName"]."\n\n".
            "ขอ".$absData["absName"]."\n".
            $leaveDetail.
            "Link ขออนุมัติ ".base_url()."/absence/edit?id=".$insert_id."\n".
            "ผู้อนุมัติ - คุณ".$absData["managerName"];
        $res = $this->notify_message($str,$token);

        $token = "289T2bd9N198Y35ipDuMxryPpbLWv79VQHxP2qQ9yTH"; //Test - ส่งมาที่ Group intraLiNE
        $str = 	"\n".
            "คุณ".$absData["staffName"].$staffNickName."\n".
            $absData["orgName"]."\n\n".
            "ขอ".$absData["absName"]."\n".
            $leaveDetail.
            "Link ขออนุมัติ ".base_url()."/absence/edit?id=".$insert_id."\n".
            "ผู้อนุมัติ - คุณ".$absData["managerName"];

        $res = $this->notify_message($str,$token);

      }elseif($current_task == 'edit'){
        $filename= $_FILES["absenceFileMedical"]["name"];
        $file = explode(".",$filename_);
        $filename_change = $file[0].$random.".".$file[1];
        if($filename !=$this->input->post('remove_file',true)&& $filename !=""){
          @unlink("./uploads/absenceFileMedical/".$_POST["remove_file"]);
          move_uploaded_file($_FILES['absenceFileMedical']['tmp_name'], $path.$filename_change);
        }else{
          $filename_change = $_POST["remove_file"];
        }
        $this->absence_model->update_staff_absence($stfAbsID,$data1) ;
      }
      if($lineID != "") {
        redirect(site_url().'line/finish', 'refresh');
      }
    }

    private function notify_message($message,$token){
      $queryData = array('message' => $message);
      $queryData = http_build_query($queryData,'','&');
      $headerOptions = array(
          'http'=>array(
            'method'=>'POST',
            'header'=> "Content-Type: application/x-www-form-urlencoded\r\n"
                ."Authorization: Bearer ".$token."\r\n"
                ."Content-Length: ".strlen($queryData)."\r\n",
            'content' => $queryData
          ),
      );
      $context = stream_context_create($headerOptions);
      $result = file_get_contents(LINE_API,FALSE,$context);
      $res = json_decode($result);
      return $res;
    }

    public function history() {
      $lineID = $this->input->get('lineID');
      $matchStaff = $this->line_model->lineExist($lineID);
      if($matchStaff == null) {
        redirect("line/register?lineID=".$lineID, 'refresh');
      } else {
        $empID = $matchStaff['ID'];
        $staffID = $matchStaff['staffID'];
        $data['abs'] = $this->absence_model->getStaffAbsenceData(date('Y'),'',$empID);
        $this->template->load("template/line", "history",$data);
      }
    }

    public function quota() {
      $lineID = $this->input->get('lineID');
      $matchStaff = $this->line_model->lineExist($lineID);
      if($matchStaff == null) {
        redirect("line/register?lineID=".$lineID, 'refresh');
      } else {
        $empID = $matchStaff['ID'];
        $staffID = $matchStaff['staffID'];
        $data['ac1'] = $this->absence_model->getAbsenceQuota(1);
        $data['ac2'] = $this->absence_model->getAbsenceQuota(2);
        $data['ac7'] = $this->absence_model->getAbsenceQuota(7);
        $data['sa1'] = $this->absence_model->getStaffAbsence(date('Y'),$empID,1);
        $data['sa2'] = $this->absence_model->getStaffAbsence(date('Y'),$empID,2);
        $data['sa7'] = $this->absence_model->getStaffAbsence(date('Y'),$empID,7);
        $data['sao'] = $this->absence_model->getStaffAbsence(date('Y'),$empID);
        $this->template->load("template/line", "quota",$data);
      }
    }

    public function register_submit() {
      $lineID = $this->input->post("lineID");
      $staffID = $this->input->post("empID");
      $result_message = $this->line_model->saveLineID($staffID, $lineID);
      $data["message_head"] = "ผลการลงทะเบียน";
			if($result_message == "success") {
        $data["message"] = "ลงทะเบียนเรียบร้อยแล้ว คุณสามารถใช้ระบบ intraLiNE จาก Line ของคุณได้แล้ว กรุณาปิดหน้าจอนี้เพื่อกลับสู่ Line";
      } elseif($result_message == 'staff_not_found') {     
        $data["message"] = "ไม่มีพนักงานรหัส ".$staffID." อยู่ในระบบ กรุณาติดต่อผู้ดูแลระบบ";
      } else {
        $data["message"] = "มีปัญหาที่ไม่คาดคิด รหัสปัญหา ".$result_message." กรุณาติดต่อผู้ดูแลระบบ";
      }
      $this->template->load("template/line", "message",$data);
    }

    public function register_submit_email() {
      $lineID = $this->input->post("lineID");
      $emailValue = $this->input->post("emailValue");
      $result_message = $this->line_model->saveLineID_email($emailValue, $lineID);
      $data["message_head"] = "ผลการลงทะเบียน";
			if($result_message == "success") {
        $data["message"] = "ลงทะเบียนเรียบร้อยแล้ว คุณสามารถใช้ระบบ intraLiNE จาก Line ของคุณได้แล้ว กรุณาปิดหน้าจอนี้เพื่อกลับสู่ Line";
      } elseif($result_message == 'email_not_found') {     
        $data["message"] = "ไม่มีพนักงานที่ใช้ E-mail ".$emailValue." อยู่ในระบบ กรุณาติดต่อผู้ดูแลระบบ";
      } else {
        $data["message"] = "มีปัญหาที่ไม่คาดคิด รหัสปัญหา ".$result_message." กรุณาติดต่อผู้ดูแลระบบ";
      }
      $this->template->load("template/line", "message",$data);
    }
       
}
/* End of file  .php */
/* Location: ./application/module/  */
?>