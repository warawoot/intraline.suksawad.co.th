<style>
    .pf-input{
        float:left;
    }
    
    .pf-popup{
        margin-top:45px;
    }
    
    .pf-search-button{
        width: 40px !important;
        height: 40px !important;
        float:left;
        margin-left:5px;
        margin-top:10px;
        border : solid #CCC 1px !important;
        <?php if($this->session->userdata('roleID')==3) { ?>
            display : none;
        <?php }?>
    }
    
    .ui-datepicker-trigger {
        float:left;  
        margin-left:5px;
        margin-top:10px;
    }

    .hasDatepicker {
        float:left;
    }
</style>
<script>
     $.datepicker.regional['th'] ={
        changeMonth: true,
        changeYear: true,
        yearOffSet: 543,
        showOn: "button",
        buttonImage: '<?=base_url()?>assets/images/common_calendar_month_-20.png',
        buttonImageOnly: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
        constrainInput: true,
       
        prevText: 'ก่อนหน้า',
        nextText: 'ถัดไป',
        yearRange: '-20:+0',
        buttonText: 'เลือก',
      
    };
    
    $.datepicker.setDefaults($.datepicker.regional['th']);

    $(function() {
        $( "#date_start" ).datepicker( $.datepicker.regional["th"] ); 

        $( "#date_end" ).datepicker( $.datepicker.regional["th"] ); 

    <?php if($current_task=="add") {?>      
        $( "#date_start" ).datepicker("setDate", new Date()); 
        $( "#date_end" ).datepicker("setDate", new Date()); 
    <?php }?>
    });
    
    //=====================================================================================================
    //On Selected Date
    //On Change Drop Down
    function ChangMonthAndYear(year, month, inst) {

        GetDaysShows(month, year);
    }

    //=====================================
    function GetDaysShows(month, year) {
        //CallGetDayInMonth(month, year); <<เป็น Function ที่ผมใช้เรียก ajax เพื่อหาวันใน DataBase  แต่นี้เป็นเพียงตัวอย่างจึงใช้ Array ด้านล่างแทนการ Return Json
        //อาจใช้ Ajax Call Data โดยเลือกจากเดือนและปี แล้วจะได้วันที่ต้องการ Set ค่าวันไว้คล้ายด้านล่าง
        Holidays = [1,4,6,11]; // Sample Data
    }
    //=====================================
 
</script>    
<div class="container">
        <?php
            $ipaddress = '';
            if (getenv('HTTP_CLIENT_IP'))
                $ipaddress = "HTTP_CLIENT_IP=".getenv('HTTP_CLIENT_IP');
            else if(getenv('HTTP_X_FORWARDED_FOR'))
                $ipaddress = "HTTP_X_FORWARDED_FOR=".getenv('HTTP_X_FORWARDED_FOR');
            else if(getenv('HTTP_X_FORWARDED'))
                $ipaddress = "HTTP_X_FORWARDED=".getenv('HTTP_X_FORWARDED');
            else if(getenv('HTTP_FORWARDED_FOR'))
                $ipaddress = "HTTP_FORWARDED_FOR=".getenv('HTTP_FORWARDED_FOR');
            else if(getenv('HTTP_FORWARDED'))
                $ipaddress = "HTTP_FORWARDED=".getenv('HTTP_FORWARDED');
            else if(getenv('REMOTE_ADDR'))
                $ipaddress = "REMOTE_ADDR=".getenv('REMOTE_ADDR');
            else
                $ipaddress = 'UNKNOWN';
            echo "IP=".$ipaddress;
        ?>        

        <form id="formMain" action="<?=base_url(); ?>line/leave_submit"" method="post" enctype="multipart/form-data" class="form-horizontal">  
            <input type="hidden" value="<?=$empID?>" id="dropdown_staff" name="dropdown_staff">
            <input type="hidden" value="<?=$lineID?>" id="lineID" name="lineID">
            <div class="form-group">
                <label class="control-label col-sm-3">ประเภทการลา <strong style="color:red">*</strong></label>
                <div class="col-sm-3">
                    <?php
                    echo form_dropdown('absence_type',$absence_type ,$absTypeID ,'id ="absence_type" 
                        class="form-control" style="width: 100%" data-required="1" data-unique="" data-type="select"  ');
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-3">
                    <?php 
                    $status =  !empty($status_absence)?$status_absence:"0";
                    ?>
                    <div class="radio">
                        <label><input type="radio" <?php echo ($status==1 || $status == 0)?'checked="checked"':'';?>  name="status_absence"  id="status_absence"  value="1">ลาเต็มวัน</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" <?php echo ($status==2)?'checked="checked"':'';?> name="status_absence" id="status_absence"  value="2">ลาชั่วโมง</label>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-3">วันที่ลา <strong style="color:red">*</strong></label>
                <div class="col-sm-6">
                    <input  
                        data-required="1" 
                        type="text" 
                        style="width:250px;background-color:#FFF" 
                        data-type="date" 
                        name="date_start" 
                        id="date_start"
                        readonly=true
                        class="form-control input-small" 
                        value="<?php echo ($date_start_put !='')?$date_start_put:''; ?>">
                </div>
            </div>
            
            <div class="form-group" id="time" style="display: none;padding-left:10px">
                <div class="row" style="padding-left:10px">
                    <label class="control-label col-sm-3" style="color:#000">ตั้งแต่เวลา <strong style="color:red">*</strong></label>
                    <div class="col-sm-3">
                        <div class="input-group bootstrap-timepicker timepicker datepicker">
                            <input id="minute" name="minute" 
                                value="<?php echo !empty($minute)?$minute:""?>" 
                                type="text" 
                                style="width: 100%;z-index:0 !important" 
                                class="form-control input-small"><span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div> 
                    </div>
                </div>
                <div class="row" style="padding-left:10px">
                    <label class="control-label col-sm-3" style="color:#000">ถึงเวลา <strong style="color:red">*</strong></label>
                    <div class="col-sm-3">
                        <div class="input-group bootstrap-timepicker timepicker datepicker">
                            <input id="minute2" name="minute2" 
                                value="<?php echo !empty($minute2)?$minute2:""?>"  
                                type="text" 
                                style="width: 100%;z-index:0 !important" 
                                class="form-control input-small"><span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div> 
                    </div>
                </div>    
            </div>    

            <div class="form-group" id="dateEnd">
                <label class="control-label col-sm-3">ถึงวันที่ <strong style="color:red">*</strong></label>
                <div class="col-sm-6">
                    <input  data-required="1" 
                        style="width:250px;background-color:#FFF" 
                        type="text" 
                        data-type="date"  
                        name="date_end" 
                        id="date_end" 
                        readonly=true
                        class="form-control input-small" 
                        value="<?php echo ($date_end_put !='')?$date_end_put:''; ?>">
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-3">ข้อความประกอบการลา</label>
                <div class="col-sm-4">
                    <textarea name="detail" 
                        class="xcrud-input form-control" 
                        rows="5"
                        data-type="text"><?php echo ($absDetail !='')?$absDetail:''; ?></textarea>
                </div>
            </div>
            <?php
            if(($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==2) && $current_task=="edit" && ($this->session->userdata('ID') != $staffID)) { 
            ?>
            <div class="form-group">
                <label class="control-label col-sm-3">ผลการพิจารณา</label>
                <div class="col-sm-3">
                    <div class="radio">
                        <label><input type="radio" name="statusApprove" id="statusApprove" <?php echo ($statusApprove==1)?'checked="checked"':'';?> value="1">อนุมัติ</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="statusApprove" id="statusApprove" <?php echo ($statusApprove==2)?'checked="checked"':'';?> value="2">ไม่อนุมัติ</label>
                    </div>
                </div>
            </div>
            <input type="hidden" value="0" id="statusApprove" name="statusApprove">                
            <?php    
            }
            ?>
            <br><br>

        </div>
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="xcrud-top-actions btn-group"> 
                <input id="empID" 
                    name="empID" 
                    type="hidden" 
                    value="<?php echo ($empID!='')?$empID:''; ?>" />
                <input id="current_task" 
                    name="current_task" 
                    type="hidden" 
                    value="<?php echo ($current_task!='')?$current_task:''; ?>" />
                    
                <input id="stfAbsID" name="id" type="hidden"  value="<?php echo ($stfAbsID!='')?$stfAbsID:''; ?>" />
                <button class="btn btn-primary xcrud-action" 
                    id="btsave" 
                    name="btsave" 
                    type="submit" 
                    >บันทึก</button>
                <button class="btn btn-danger xcrud-action"  
                    type="button" 
                    id="btcancel"
                    name="btcancel"
                    style="margin-left:20px">ยกเลิกการลา</button>
            </div>     
        </div>
        <div class="xcrud-nav"></div>
    </form>
</div>
<script>
$(document).ready(function () {

    /*
    jQuery.ajax({
      type: "GET",
      url: "http://192.168.1.199",
      dataType: "text",
      success: function (response) {
        alert("Found RPI");
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert("Not Found RPI");
        alert(xhr.status);
        alert(thrownError);
      }
    });
    */
    /*
    $.ajax({
        url: "http://192.168.1.199",
        type: "GET",
        async: false,
        success: function (data){
            alert("Found RPI");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Not Found RPI");
            alert(xhr.status);
            alert(thrownError);
        }
    });    
    */

    $.ajax({
        type: 'GET',
        url: 'http://192.168.1.199/index.html',
        contentType: 'text/plain',
        xhrFields: {
            withCredentials: false
        },
        headers: {
        },
        success: function() {
            alert("Found RPI");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Not Found RPI");
            alert(xhr.status);
            alert(thrownError);
        }
    });

    if($('#stfAbsID').val()!=""){
        <?php if($status == 1){?>
            $('#time').hide();
            $('#dateEnd').show();
            $('#minute').val('<?php echo $minute ?>');
            $('#minute2').val('00:00');
        <?php }else if($status == 2){ ?>
            $('#time').show();
            $('#dateEnd').hide();
        <?php } ?>
    } else {
        $('#btcancel').hide();        
    }

    $('input[type=radio][name=status_absence]').change(function(){
        if($(this).val()==1){
            <?php if($current_task != 'edit'){?>
                $('#minute').val('00:00');
                $('#minute2').val('00:00');
            <?php } ?>
            $('#time').hide();
            $('#dateEnd').show();
        }else if($(this).val()==2){
            $('#time').show();
            $('#dateEnd').hide();
            <?php if($current_task != 'edit'){?>
            $('#minute').val('08:00');
            $('#minute2').val('13:00');
            <?php } ?>
        }
    });


    $('#btsave').on('click', function () {

		if ($('#absence_type').val() == '') {
			alert('กรุณาเลือกประเภทการลา');
			$('#absence_type').focus();
			return false;
		}
		if ($('#date_start').val() == '') {
			alert('กรุณาเลือกวันที่ลา');
			$('#date_start').focus();
			return false;
		}
		if ($('#detail').val() == '') {
			alert('กรุณากรอกข้อมูล');
			$('#detail').focus();
			return false;
		}

	});

    $('#btcancel').on('click', function () {
        if(confirm("ต้องการยกเลิกการลาใช่หรือไม่")) {
            $('#formMain').attr('action', "<?=base_url(); ?>absence/cancelLeave").submit();
        } else {
            return false;            
        }
	});

 }); 
</script>
<link type="text/css" href="<?=base_url()?>assets/bootstrap/timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"/>
<script type="text/javascript"  src="<?=base_url()?>assets/bootstrap/timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
  $('#minute').timepicker({
          showMeridian : false,
          defaultTime: '08:00'  
    });
  $('#minute2').timepicker({
          showMeridian : false,
          defaultTime: '13:00'  
    });
</script>