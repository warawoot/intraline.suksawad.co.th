<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<script>
function isSiteOnline(url,callback) {
    // try to load favicon
    var d = new Date();
    var n = d.getMilliseconds();

    var timer = setTimeout(function(){
        // timeout after 5 seconds
        callback(false);
    },5000)

    var img = document.createElement("img");
    img.onload = function() {
        clearTimeout(timer);
        callback(true);
    }

    img.onerror = function() {
        clearTimeout(timer);
        callback(false);
    }

    img.src = url+"/checked.png?ver="+n;
}

jQuery(document).ready(function() {
    setTimeout(function() {
        isSiteOnline("http://<?=$ip?>",function(found){
            if(found) {
                window.location.replace("http://smmms.intraline.net/line/checkinRange?lineID=<?=$lineID?>");
            }
            else {
                window.location.replace("http://smmms.intraline.net/line/checkoutRange?lineID=<?=$lineID?>");
            }
        });
    }, 3000);
});

</script>
<div align=center>
    <img src="<?=base_url()?>assets/images/spinner.gif"/><br><br>
    Verify intraLiNE Box...
</div>