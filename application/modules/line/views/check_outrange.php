<script>
jQuery(document).ready(function() {
    setInterval(function() {
        $.ajax({
            type: "POST",
            data:"",
            url: "line/getTime",
            success: function(result){
                $("#time_div").html(result);
            }
        });
    }, 1000);
});
</script>  
<style>
    #time_div {
        font-size:60px;
        font-weight:bold;
        color:#239B56;
    }

    .btn {
        width:150px;
        font-size:12px;
        font-weight:bold;
    }
</style>      
<div align=center>
    <div id="time_div"></div>
    <br>
    ท่านไม่อยู่ในพิกัดที่กำหนด กรุณาแจ้งเหตุผลของการบันทึกเวลาทำงานนอกสถานที่<br>
    <form action="checkin" method="post">
        <input type="hidden" name="lineID"      id="lineID"         value="<?=$lineID?>">
        <input type="hidden" name="inRange"     id="inRange"        value="0">
        <select style="width:90%;height:50px" name="checkinType" id="checkinType">
            <option value="1">เริ่มงาน</option>
            <option value="2">พักกลางวัน</option>
            <option value="3">เลิกพัก</option>
            <option value="4">เลิกงาน</option>
        </select><br><br>
        <textarea style="width:90%;height:100px" name="remark" id="remark"></textarea>
        <br><br>
        <button class="btn btn-success" id="btn-save" style="width:120px">บันทึกเวลาทำงาน</button> 
        <a class="btn btn-warning" id="btn-back" style="width:120px" href="https://services.intraline.net/suksawad/verifyGPS.php?lineID=<?=$lineID?>">หาพิกัดใหม่</a><br>
    </form>
    <br><br>
</div>