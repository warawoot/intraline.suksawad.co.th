<div class="row">
  <div class="col-sm-12 col-xs-12"><h4>ประวัติการลาปี 2560</h4></div>
</div>
<?php 
if($abs != NULL){
    $rowclass = "";
    foreach($abs->result() as  $row){ 

      switch ($row->absTypeID) {
        case 1:
        case 8:
          $rowclass = "danger";
          break;
        case 2:
          $rowclass = "info";
          break;
        case 7:
          $rowclass = "success";
          break;
        default:
          $rowclass = "warning";
        }

      switch ($row->statusApprove) {
        case 1:
          $statusApprove = "อนุมัติแล้ว";
          break;
        case 2:
          $statusApprove = "ไม่อนุมัติ";
          break;
        case 3:
          $statusApprove = "ยกเลิก";
          break;
        default:
          $statusApprove = "รออนุมัติ";
        }

        if($row->statusApprove == 3) {
          $rowclass .= " cancelled";
        }
  ?>
      <div class="row">
        <div class="col-sm-9 col-xs-9 <?=$rowclass?>">
        <strong><?=$row->absName?></strong><br>
        <?=toFullDate($row->absStartDate,'th');?> - <?=toFullDate($row->absEndDate,'th');?><br>
        <?=$statusApprove?><br><br>
        </div>
        <div class="col-sm-3 col-xs-3">
        <?php if($row->statusApprove!=3) {?>
          <a class="btn btn-warning" 
            href="<?=base_url()?>absence/edit?id=<?=$row->stfAbsID?>&amp;empID=<?=$row->staffID?>" 
            title="แก้ไข"><i class="glyphicon glyphicon-edit"></i></a>
        <?php 
        } else {
        ?>
        <a class="btn btn-default" 
          href="javascript:alert('ใบลานี้ยกเลิกไปแล้ว ไม่สามารถแก้ไขได้');"  
          title="แก้ไข"><i class="glyphicon glyphicon-edit"></i></a>
        <?php
        }
        ?>
        </div>
      </div>
	    <?php  
	    } //foreach 
	    }else{?>
	    <div class="col-sm-12 col-xs-12"><h5>ไม่พบข้อมูลการลา</h5></div>
	    <?php
    	}?>
  </div>  
</div>