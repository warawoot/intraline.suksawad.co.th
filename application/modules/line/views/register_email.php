<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <div style="text-align:center"> 
                <h3>ลงทะเบียน</h3>
            </div>
            <div class="panel-body">
                <form id="formMain" action="<?=base_url(); ?>line/register_submit_email" method="post" enctype="multipart/form-data" class="form-horizontal">  
                    <div class="form-group">
                        <h5 style="width:100%;text-align:left">การใช้งานครั้งแรกจะต้องผูกบัญชี Line กับ E-mail พนักงานของคุณก่อน</h5><br>
                        <label class="control-label">E-mail <strong style="color:red">*</strong></label>
                        <input type="text" class="form-control" placeholder="E-mail" autofocus name="emailValue" id="emailValue">
                        <input type="hidden" name="lineID" id="lineID" value="<?=$lineID; ?>">
                    </div>
                    <button id="btnsave" class="btn btn-primary xcrud-action" type="submit">ลงทะเบียน</button>
                </form>
            </div>
        </section>
    </div>
</div>
<script>
    $('#btnsave').on('click', function () {
		if ($('#emailValue').val() == '') {
			alert('กรุณาใส่ E-mail ของท่าน');
			$('#emailValue').focus();
			return false;
		}
	});
</script>