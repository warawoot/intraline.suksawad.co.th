<div class="row">
  <div class="col-md-3">
    <div class="pastel-pink" style="padding:20px">
      <h4>ลาป่วย : <?=$sa1["numDay"]?> วัน</h4>
      <h6>สามารถลาได้ <?=$ac1["numDay"]?> วัน ได้รับค่าจ้างปกติ</h6>
    </div>
    <div class="icon">
      <i class="ion ion-medkit"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-blue" style="padding:20px">
      <h4>ลากิจ : <?=$sa2["numDay"]?> วัน</h4>
      <h6>ไม่จ่ายค่าจ้างตามจำนวนวันลา</h6>
    </div>
    <div class="icon">
      <i class="ion ion-briefcase"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-green" style="padding:20px">
      <h4>ลาพักผ่อน : <?=$sa7["numDay"]?> วัน</h4>
      <h6>จากสิทธิ <?=$ac7["numDay"]?> วัน / การทำงาน 1 ปี</h6>
    </div>
    <div class="icon">
      <i class="ion ion-ios-partlysunny-outline"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-yellow" style="padding:20px">
      <h4>ลาอื่นๆ : <?=$sao["numDay"]?> วัน</h4>
      <h6>&nbsp;</h6>
    </div>
    <div class="icon">
      <i class="ion ion-email"></i>
    </div>
  </div>
</div> 
