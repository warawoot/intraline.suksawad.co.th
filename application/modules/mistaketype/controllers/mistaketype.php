<?php

class Mistaketype extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		

		$xcrud->table('tbl_mistake_type');
		
		
		//// List /////
		$col_name = array(
			'mistakeName'=>'ประเภทการลงโทษ',
			
		);

		
		
		
		$xcrud->columns('mistakeName');
		$xcrud->label($col_name);
		

		//// Form //////

		$xcrud->fields('mistakeName');
		
		$xcrud->validation_required('mistakeName');

		
		
		
		
		$data['html'] = $xcrud->render();
		$data['title'] = "ประเภทการลงโทษ";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file mistaketype.php */
/* Location: ./application/module/mistaketype/mistaketype.php */