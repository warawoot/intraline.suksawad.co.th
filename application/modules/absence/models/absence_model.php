<?php

class Absence_model extends MY_Model {

	function __construct() {
		parent::__construct();
		$this->table="";
		$this->pk = "";
	}

	public function getAbsenceData($ID){
        $sql = "SELECT
                    sa.stfAbsID,
                    sa.staffID,
                    sa.absStartDate,
                    sa.absEndDate,
                    sa.absDetail,
                    sa.absTypeID,
                    sa.statusApprove,
					sa.absenceFile,
					sa.status_absence,
                    abt.absName,
					oc.managerID,
					oc.orgName, 
                    oc.lineToken,
					CONCAT(st.staffFName,' ',st.staffLName) as staffName,
					st.staffNickName, 
					CONCAT(mgr.staffFName,' ',mgr.staffLName) as managerName
                FROM `tbl_staff_absence` sa
				INNER JOIN `tbl_staff`st
					ON sa.staffID = st.ID
                INNER JOIN `tbl_absence_type`abt
                    ON sa.absTypeID = abt.absTypeID
				INNER JOIN tbl_org_chart oc
					ON st.orgID = oc.orgID
 				INNER JOIN `tbl_staff` mgr
					ON mgr.staffID = oc.managerID
				WHERE sa.stfAbsID = ".$ID;

		/*
	    $query = $this->db->query("
					 	SELECT
							t1.*,
							CONCAT(t3.staffFName,' ',t3.staffLName) as staffName ,
							t2.absName
						FROM `tbl_staff_absence` as t1
						LEFT  JOIN tbl_absence_type as t2 ON t1.absTypeID = t2.absTypeID
						INNER JOIN tbl_staff as t3 ON t1.staffID = t3.ID
						INNER JOIN
    					");
        $data       = $query->row_array();
		*/
        $r = $this->db->query($sql);
        return ($r->num_rows() > 0) ? $r->row_array() : NULL;
    }

    public function getAbsenceQuota($absTypeID){
        $query = $this->db->query("
            SELECT
                numDay
            FROM `tbl_absence_condition` ac
            WHERE ac.absTypeID = $absTypeID
            LIMIT 1
        ");
        return $query->row_array();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function getStaffAbsencePartial($y,$ID,$absTypeID=null){
		$sql = "SELECT COALESCE(SUM(time_sec),0) as TotalLeaveSEC,absTypeID FROM 
				(SELECT COALESCE(SUM(TIME_TO_SEC(timediff(absEndDate,absStartDate))),0) AS time_sec,absTypeID 
				  FROM tbl_staff_absence 
				  WHERE staffID = ".$ID." AND YEAR(absStartDate) = ".$y." AND status_absence = 2 AND TIME(absEndDate) <= '12:00:00' and statusApprove = 1 ";
				  if($absTypeID != null){
						$sql .= " AND absTypeID in ($absTypeID) ";
				  }
	    $sql .= " GROUP BY absTypeID
				  UNION ALL
				  SELECT COALESCE(SUM(TIME_TO_SEC(timediff(absEndDate,absStartDate))),0) AS time_sec,absTypeID
				  FROM tbl_staff_absence
				  WHERE staffID = ".$ID." AND YEAR(absStartDate) = ".$y." AND status_absence = 2 AND TIME(absStartDate) <= '12:00:00' AND TIME(absEndDate) >= '13:00:00' and statusApprove = 1 ";
				  if($absTypeID != null){
					$sql .= " AND absTypeID in ($absTypeID) ";
			  	  }
		$sql .= " GROUP BY absTypeID
				  UNION ALL
				  SELECT COALESCE(SUM(TIME_TO_SEC(timediff(absEndDate,absStartDate))),0) AS time_sec,absTypeID
				  FROM tbl_staff_absence
				  WHERE staffID = ".$ID." AND YEAR(absStartDate) = ".$y." AND status_absence = 2 AND TIME(absStartDate) >= '13:00:00' and statusApprove = 1 ";
				  if($absTypeID != null){
					$sql .= " AND absTypeID in ($absTypeID) ";
			  	  }
		$sql .= "GROUP BY absTypeID) AS A
		";
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->row_array();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getStaffAbsence($y,$ID,$absTypeID=null){
        $sql = "SELECT SUM(nDay) as numDay FROM
				(SELECT
    				ABS(DATEDIFF(sa.absEndDate,sa.absStartDate)) + 1 as nDay
				FROM `tbl_staff_absence` sa
				WHERE sa.staffID = ".$ID."
					AND sa.statusApprove = 1";
			if($y) {
				$sql .= " AND (YEAR(sa.absStartDate)= $y OR YEAR(sa.absEndDate)= $y)";
			} //ต้องคิดแค่วันที่อยู่ในปี ตอนนี้จะมีปัญหาตอนคร่อมปี

            if($absTypeID != null) {
                $sql .= " AND sa.absTypeID = ".$absTypeID;
            }
            else {
                $sql .= " AND sa.absTypeID NOT IN(1,2,7)";
            }
		$sql .= ") TBL_NUMDAY";
        $query = $this->db->query($sql);
        //echo $query->row_array();
        return $query->row_array();
    }

   	public function getTotalLeave($y,$ID,$absTypeID=null){ 
		$sql =  "SELECT SUM(DateDiff) as DateDiff, SUM(Sundays) as Sundays, SUM(Holiday) as Holiday, SUM(TotalLeave) as TotalLeave FROM 
		    	(SELECT staffID, 
					DATEDIFF(absEndDate,absStartDate) + 1 as DateDiff, 
					ROUND((
						(unix_timestamp(absEndDate) - unix_timestamp(absStartDate) ) /(24*60*60)
							-7+WEEKDAY(absStartDate)-WEEKDAY(absEndDate)
						)/7)
						+ if(WEEKDAY(absStartDate) <= 6, 1, 0)
						+ if(WEEKDAY(absEndDate) >= 6, 1, 0) as Sundays,
						
					(SELECT COUNT(*) FROM tbl_holiday WHERE holiday_date BETWEEN absStartDate AND absEndDate) as Holiday, 
				
					DATEDIFF(absEndDate,absStartDate) + 1 -  
					(ROUND((
						(unix_timestamp(absEndDate) - unix_timestamp(absStartDate) ) /(24*60*60)
							-7+WEEKDAY(absStartDate)-WEEKDAY(absEndDate)
						)/7)
						+ if(WEEKDAY(absStartDate) <= 6, 1, 0)
						+ if(WEEKDAY(absEndDate) >= 6, 1, 0)) - 
						(SELECT COUNT(*) FROM tbl_holiday WHERE holiday_date BETWEEN absStartDate AND absEndDate) 
						as TotalLeave
				
				FROM tbl_staff_absence as sa
				WHERE status_absence=1 AND staffID=".$ID;
		 	if($y) {
		     	$sql .= " AND (YEAR(sa.absStartDate)= $y OR YEAR(sa.absEndDate)= $y)";
		    } //ต้องคิดแค่วันที
		    if($absTypeID != null) {
		        $sql .= " AND sa.absTypeID in ($absTypeID)";
		    }
		$sql .= ") LeaveTable";
		$query = $this->db->query($sql);
		return $query->row();
	}
   
    public function getStaffAbsenceData($y,$m,$ID,$managerID,$d,$ag=''){
    		 $sql = "SELECT
        			sa.dateCreate,
                    sa.stfAbsID,
                    sa.staffID,
                    sa.absStartDate,
                    sa.absEndDate,
                    sa.absDetail,
                    sa.absTypeID,
                    sa.statusApprove,
					sa.absenceFile,
                    abt.absName,
					oc.managerID,
					oc.orgID,
					oc.orgName,
					CONCAT(st.staffFName,' ',st.staffLName) as staffName
                FROM `tbl_staff_absence` sa
				INNER JOIN `tbl_staff`st
					ON sa.staffID = st.ID
                INNER JOIN `tbl_absence_type`abt
                    ON sa.absTypeID = abt.absTypeID
				INNER JOIN tbl_org_chart oc
					ON st.orgID = oc.orgID
                WHERE 1";
		if($ID) {
			$sql .= " AND sa.staffID = $ID";
		}
		if($managerID) {
			$sql .= " AND oc.managerID = $managerID";
		}
		
			
		if($y) {
			if(!empty($m == 0) && !empty($d == 0)) 
			{
				$sql .= " AND (YEAR(sa.absStartDate)= $y OR  YEAR(sa.absEndDate)= $y) ";
			}
			elseif (!empty($m != 0) && !empty($d == 0))  {
				$sql .= " AND ((MONTH(sa.absStartDate)= $m AND YEAR(sa.absStartDate)= $y ) OR (MONTH(sa.absStartDate)= $m AND YEAR(sa.absEndDate)= $y))  ";
			}
			elseif (!empty($m == 0) && !empty($d != 0))  {
				$sql .= " AND ((DAY(sa.absStartDate)= $d AND YEAR(sa.absStartDate)= $y ) OR (DAY(sa.absStartDate)= $d AND YEAR(sa.absEndDate)= $y))  ";
			}
			else 
			{
				$sql .= "  AND ((DAY(sa.absStartDate)= $d AND MONTH(sa.absStartDate)= $m AND YEAR(sa.absStartDate)= $y) OR (DAY(sa.absEndDate)= $d AND MONTH(sa.absEndDate)= $m AND YEAR(sa.absEndDate)= $y))  ";
			}
		}

		if(!empty($ag)){
             $sql.="and st.orgID in ($ag) ";
        }
			/*if ($d == 0 ) {
				$sql .= " AND (MONTH(sa.absStartDate)= $m AND YEAR(sa.absStartDate)= $y OR MONTH(sa.absStartDate)= $m AND YEAR(sa.absEndDate)= $y)";
			} else {
				$sql .= " AND ((DAY(sa.absStartDate)= $d AND MONTH(sa.absStartDate)= $m AND YEAR(sa.absStartDate)= $y) OR (DAY(sa.absEndDate)= $d AND MONTH(sa.absEndDate)= $m AND YEAR(sa.absEndDate)= $y))";
			}*/
		$sql .= " ORDER BY sa.absStartDate DESC, sa.absEndDate DESC";
        $r = $this->db->query($sql);
        return ($r->num_rows() > 0) ? $r : NULL;
    	
       
    	
    }

    public function getStaffAbsenceNotApprove($dateStart){
        $sql = "SELECT
                    sa.stfAbsID,
                    sa.staffID,
                    sa.absStartDate,
                    sa.absEndDate,
                    sa.statusApprove,
					sa.status_absence,
                    abt.absName,
					oc.managerID,
					oc.orgName, 
                    oc.lineToken,
					CONCAT(st.staffFName,' ',st.staffLName) as staffName,
					st.staffNickName, 
					CONCAT(mgr.staffFName,' ',mgr.staffLName) as managerName
                FROM `tbl_staff_absence` sa
				INNER JOIN `tbl_staff`st
					ON sa.staffID = st.ID
                INNER JOIN `tbl_absence_type`abt
                    ON sa.absTypeID = abt.absTypeID
				INNER JOIN tbl_org_chart oc
					ON st.orgID = oc.orgID
 				INNER JOIN `tbl_staff` mgr
					ON mgr.staffID = oc.managerID
                WHERE sa.statusApprove=0";
				if($dateStart) {
					$sql .= " AND sa.absStartDate >= '".$dateStart."'";
				}
		$sql .= " ORDER BY sa.absStartDate";
        $r = $this->db->query($sql);
        return ($r->num_rows() > 0) ? $r : NULL;
    }

	function edu_staff($ID){
		$this->db->select(' t3.staffPreName , t3.staffFName,t3.staffLName ');
		$this->db->from('`tbl_staff` as t3');
		$this->db->where('t3.`ID`' ,$ID);
		$query 		= $this->db->get();
		$data 		= $query->row_array();
		return ($query->num_rows() > 0) ? $data : NULL;
	}

	function get_dropdown_all($id , $name ,$table){
					$this->db->order_by($name, "asc");
					$sql		= $this->db->get($table);
					$dropdowns	= $sql->result();

					if ($sql->num_rows() > 0){
						foreach($dropdowns as $dropdown){
						$dropdownlist['']	= "- none -";
						$dropdownlist[$dropdown->$id]	= $dropdown->$name;
					}
					}else{
							$dropdownlist['']	= "- none -";
					}

					$finaldropdown	= $dropdownlist;
					return $finaldropdown;
	}

 	function get_dropdown_staff($id , $name , $lastname ,$table,$where){
		if($where !=''){
			 $sql = $this->db->query('
							SELECT * FROM `tbl_staff` as t1 INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID WHERE t2.orgID = '.$where.' '
	     	 );
		}else{
			$sql = $this->db->query('
							SELECT * FROM `tbl_staff` as t1 INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID   '
	     	 );

		}
 			$dropdowns	= $sql->result();
			$staffLName	=	'';
			if ($sql->num_rows() > 0){
				foreach($dropdowns as $dropdown){
  					$dropdownlist['']	= "- none -";
					$dropdownlist[$dropdown->$id]	= $dropdown->staffPreName.$dropdown->$name .'&nbsp;'. $dropdown->staffLName;
				}
			}else{
					$dropdownlist['']	= "- none -";
			}

			$finaldropdown	= $dropdownlist;
			return $finaldropdown;
	}

	function get_dropdown_staff_e($id , $name , $lastname ,$table,$where){


			 $sql = $this->db->query('
							SELECT * FROM `tbl_staff` as t1 INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID WHERE t1.ID = '.$where.' '
	     	 );

 			$dropdowns	= $sql->result();
			$staffLName	=	'';
			if ($sql->num_rows() > 0){
				foreach($dropdowns as $dropdown){
  					$dropdownlist['']	= "- none -";
					$dropdownlist[$dropdown->$id]	= $dropdown->staffPreName.$dropdown->$name .'&nbsp;'. $dropdown->staffLName;
				}
			}else{
					$dropdownlist['']	= "- none -";
			}

			$finaldropdown	= $dropdownlist;
			return $finaldropdown;
	}

  function getDropdownTree($level = 0 , $prefix = '' , $assignID=0 ,$upperOrgID,$var_agencies) {

        $token  = $this->input->get('token');

		$rows = $this->db->query('
						SELECT t1.id, t1.orgID, t1.assignID, t1.upperOrgID, t1.orgShortName, t1.orgName ,t1.upperOrgID FROM tbl_org_chart as t1
						WHERE t1.upperOrgID = '.$level.' and t1.assignID = '.$assignID.'  order by orgID asc
				  '
		);

        $rows       = $rows->result();
        $tree = '';

        if (count($rows) > 0) {
            $i = 1;
            foreach ($rows as $key  =>$row) {//<i class="fa fa-caret-right"></i>
				//echo $row->orgID .'=='. $var_agencies.'<br>';
                    if($row->orgID == $var_agencies){
                        $selected =  "selected='selected'" ;
                    }else{
                        $selected = "";
                    }
                    // Append subcategories
                    if($upperOrgID == ''){
                        $tree  .=  "<option value='".$row->orgID."' ".$selected.">";
                    }else{
                        $tree  .=  "<option value='".$row->orgID."' ".$selected.">";
                    }
                    $tree  .= $prefix .$row->orgName;
                    $tree  .=  "</option>";

                    $tree .= $this->getDropdownTree($row->orgID, $prefix . '--',$row->assignID , $upperOrgID,$var_agencies);
            }
        }
        return $tree;
    }

	function insert_staff_absence($data){
  		$this->db->insert("tbl_staff_absence",$data);
    }

	function view_absence($ID){
	    $query = $this->db->query("
					 	SELECT
							t1.*,
							CONCAT(t3.staffPreName,t3.staffFName,' ',t3.staffLName) as staffName
						FROM `tbl_staff_absence` as t1
						LEFT  JOIN tbl_absence_type as t2 ON t1.absTypeID = t2.absTypeID
						INNER JOIN tbl_staff as t3 ON t1.staffID = t3.ID
						WHERE t1.stfAbsID = ".$ID."
    					");
        $data       = $query->row_array();
        return ($query->num_rows() > 0) ? $data : NULL;
    }

	function update_staff_absence($id,$data){
		 $this->db->where('stfAbsID', $id);
		 $this->db->update('tbl_staff_absence', $data);
     }

	 function delete_staff_absence($id){
		 $this->db->where('stfAbsID', $id);
 		 $this->db->delete('tbl_staff_absence');
     }

	 function getAbsName($absTypeID){
        $query = $this->db->query("
				SELECT
				staffFName,
				staffLName
				FROM db_intra_suksawad.tbl_staff
				WHERE staffID='$empID'");
        if ($query->num_rows() > 0) {
	        $data = $query->row_array();
			return $data["staffFName"]." ".$data["staffLName"];
		} else {
			return "";
		}
     }

	 function list_absence($m){
	    $query = $this->db->query('
					 	SELECT
					 		t1.stfAbsID ,
					 		t1.staffID ,
							(SELECT t1_type.absName FROM tbl_absence_type as t1_type WHERE t1_type.absTypeID =  t1.absTypeID LIMIT 1 ) as absName,
							t1.absStartDate ,
							t1.absEndDate ,
							t1.absDetail ,
							CONCAT(t2.staffPreName,t2.staffFName ," ",t2.staffLName) as name_lastname,
							(SELECT sub_2.orgID FROM tbl_staff as sub_2 WHERE  sub_2.ID = t1.staffID LIMIT 1) as orgID
						FROM `tbl_staff_absence` t1
						INNER JOIN `tbl_staff` t2 ON t1.staffID = t2.ID
						WHERE MONTH(absStartDate)  = '.$m.' and YEAR(absStartDate) = YEAR(NOW())
						ORDER BY CONVERT(t2.staffFName USING tis620) ,CONVERT(t2.staffLName USING tis620)
    			'  );
        $data       = $query->result();
        return ($query->num_rows() > 0) ? $data : NULL;

	}

	function list_org_chart1($orgID){

		$query = $this->db->query('
					 	 SELECT * FROM `tbl_org_chart` as t1
						 WHERE (t1.orgID = '.$orgID.' and t1.assignID = 2)  and ( t1.orgName LIKE "%ฝ่าย%"  or t1.orgName LIKE "%สำนักงาน%"  or t1.orgName LIKE "%รอง%"  )
    			'  );
        $data       = $query->row_array();
        return ($query->num_rows() > 0) ? $data : NULL;

	}

	function list_org_chart11($orgID){

		$query = $this->db->query('
					 	 SELECT * FROM `tbl_org_chart` as t1
						 WHERE (t1.upperOrgID = '.$orgID.' and t1.assignID = 2)
    			'  );
        $data       = $query->row_array();
        return ($query->num_rows() > 0) ? $data : NULL;

	}

	////// end //////

	function list_org_chart2($orgID){

		$query = $this->db->query('
					 	 SELECT * FROM `tbl_org_chart` as t1
						 WHERE (t1.orgID = '.$orgID.' and t1.assignID = 2)   and ( t1.orgName LIKE "%แผนก%" )
    			'  );
        $data       = $query->row_array();
        return ($query->num_rows() > 0) ? $data : NULL;
	}

	function list_org_chart3($orgID){

		$query = $this->db->query('
					 	 SELECT * FROM `tbl_org_chart` as t1
						 WHERE (t1.orgID = '.$orgID.' and t1.assignID = 2)   and ( t1.orgName LIKE "%กอง%" )
    			'  );
        $data       = $query->row_array();
        return ($query->num_rows() > 0) ? $data : NULL;
	}

	function list_org_chart4($orgID){

		$query = $this->db->query('
					 	 SELECT * FROM `tbl_org_chart` as t1
						 WHERE (t1.orgID = '.$orgID.' and t1.assignID = 2)   and ( t1.orgName LIKE "%กลุ่ม%" )
    			'  );
        $data       = $query->row_array();
        return ($query->num_rows() > 0) ? $data : NULL;
	}

	function cancelLeave($id){
		if($id) {
			$sql = "UPDATE tbl_staff_absence SET statusApprove=3 WHERE stfAbsID=".$id;
			$this->db->query($sql);
			return true;
		} else {
			return false;
		}
	}

}
