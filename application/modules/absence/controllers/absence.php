<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absence extends MY_Controller {

    function __construct() {
    parent::__construct();
     	$this->load->model('absence_model');
		$this->load->model('rate/rate_model');
		$this->load->model('sequence/sequence_model');
		$this->load->model('reportbirthday/staff_model');
		$this->load->model('organization/organization_model');
		$this->assingID	=	$this->sequence_model->get_last_assignID();
    }

    public function index() {
		$filter_day		= $this->input->get_post('filter_day');
		$filter_month	= $this->input->get_post('filter_month');
		$filter_year	= $this->input->get_post('filter_year');
		$ag 			= $this->organization_model->getSubOrg('orgID');
		$agencies 		= $this->input->get_post('agencies');

		// $agencies 		= $this->input->get_post('agencies');
		// $data['orgID']	= $this->input->get('orgID');
		// $ag 				= $this->organization_model->getSubOrg('orgID');
		$y 				= ($filter_year !='')	? $filter_year :date("Y");
  		$m				= ($filter_month !='')	? $filter_month:date("m");
  		$d				= ($filter_day !='')	? $filter_day:date("d");

		if($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==4)
		{
			$orgID = "";
		}
		elseif($this->session->userdata('roleID')==2)
		{
			$managerID = $this->session->userdata('userID');
		}
		else
		{
			redirect('dashboard','refresh');
		}

		$data['orgID'] 			= $this->input->get('orgID'); 
		$data['abs'] 			= $this->absence_model->getStaffAbsenceData($y,$m,'',$managerID,$d,$data['orgID']);
        $data['y']				= $y	;
        $data['m']				= $m	;
        $data['d']				= $d	;
		$data['current_task']	= 'list';

		// echo "<pre>";
		// echo $this->db->last_query();
		// echo "</pre>";
		$this->template->load('template/admin','adminview',$data);
    }

	public function add() {

		$var_agencies				= $this->input->get_post('department');
        $data['dropdown_staff']		= $this->absence_model->get_dropdown_staff('ID' , 'staffFName' ,'staffLName' ,'tbl_staff',$var_agencies);#Dropdown
		$data['dropdown_org_chart'] = $this->absence_model->getDropdownTree($level = 0, $prefix = '' ,$this->assingID	 ,$upperOrgID='',$var_agencies );#Dropdown
		$data['absence_type']		= $this->absence_model->get_dropdown_all('absTypeID' , 'absName' ,'tbl_absence_type');

		$data['current_task']		= 'add';
		$data['var_agencies']		= $var_agencies;

		$data['absDetail']			= '';
		$data['empID']				= '';
		$data['stfAbsID']			= '';
		$data['date_start_put']		= '';
		$data['date_end_put']		= '';
		$data['absTypeID']			= '';
		if($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==4) {
			$data['staffName'] 		= "";
		} else {
			$data['staffName'] 		= $this->session->userdata('fullName');
		}
        $this->template->load('template/admin', 'absence_add',$data);
    }

	public function edit() {

		$stfAbsID			= $this->input->get_post('id');
		$data['stfAbsIDs']	= $this->absence_model->getAbsenceData($stfAbsID);
		$empID				= $data['stfAbsIDs']['staffID'];
		$data['ac1'] = $this->absence_model->getAbsenceQuota(1);
		$data['ac2'] = $this->absence_model->getAbsenceQuota(2);
		$data['ac7'] = $this->absence_model->getAbsenceQuota(7);
		$data['sa1'] = $this->absence_model->getStaffAbsence(date('Y'),$empID,1);
		$data['sa2'] = $this->absence_model->getStaffAbsence(date('Y'),$empID,2);
		$data['sa7'] = $this->absence_model->getStaffAbsence(date('Y'),$empID,7);
		$data['sao'] = $this->absence_model->getStaffAbsence(date('Y'),$empID);
		$data['abs'] = $this->absence_model->getStaffAbsenceData(date('Y'),'',$empID);

		echo "<!--".
			 "/login user ID=".$this->session->userdata('ID').
			 "/manager ID=".$data['stfAbsIDs']['managerID'].
			 "/staff ID=".$data['stfAbsIDs']['staffID'].
			 "-->";
		if(
			($this->session->userdata('roleID')==3) || ($this->session->userdata('roleID')==4) ||
			($this->session->userdata('roleID')==2 && $this->session->userdata('userID')==$data['stfAbsIDs']['managerID']) ||
			($this->session->userdata('ID')==$data['stfAbsIDs']['staffID'])
		){

			$date_start_get 	= explode (" ", $data['stfAbsIDs']['absStartDate']);
			$date_start_get1 	= explode ("-", $date_start_get[0]);

			$y_s 	= $date_start_get1[0] + 543;
			$m_s 	= $date_start_get1[1] ;
			$d_s 	= $date_start_get1[2] ;

			$date_start_put	=	$d_s."/".$m_s."/".$y_s;

			$date_end_get 	= explode (" ", $data['stfAbsIDs']['absEndDate']);
			$date_end_get1 	= explode ("-", $date_end_get[0]);

			$y_e 	= $date_end_get1[0] + 543;
			$m_e 	= $date_end_get1[1] ;
			$d_e 	= $date_end_get1[2] ;

			$date_end_put	=	$d_e."/".$m_e."/".$y_e;

			$var_agencies					= $this->input->get_post('department');
			$data['dropdown_staff']			= $this->absence_model->get_dropdown_staff('ID' , 'staffFName' ,'staffLName' ,'tbl_staff',$var_agencies);#Dropdown
			$data['dropdown_org_chart'] 	= $this->absence_model->getDropdownTree($level = 0, $prefix = '' ,$this->assingID	 ,$upperOrgID='',$var_agencies );#Dropdown
			$data['absence_type'] 			= $this->absence_model->get_dropdown_all('absTypeID' , 'absName' ,'tbl_absence_type');

			$data['empID']					= $empID;
			$data['stfAbsID']				= $stfAbsID;
			$data['absDetail']				= $data['stfAbsIDs']['absDetail'];
			$data['absTypeID']				= $data['stfAbsIDs']['absTypeID'];
			$data['current_task']			= 'edit';
			$data['staffID']				= $data['stfAbsIDs']['staffID'];
			$data['var_agencies']			= $var_agencies;
			$data['staffName']            	= $data['stfAbsIDs']['staffName'];
      		$data['absenceFile']			= $data['stfAbsIDs']['absenceFile'];

			$data['date_start_put']			= $date_start_put;
			$data['date_end_put']			= $date_end_put;

			$data['status_absence'] 		= $data['stfAbsIDs']['status_absence'];
			$data['minute'] 				= $date_start_get[1];
			$data['minute2'] 				= $date_end_get[1];

			$data['statusApprove']         	= $data['stfAbsIDs']['statusApprove'];

			$this->template->load('template/admin', 'absence_add',$data);
		}
		else
		{
			echo "<meta charset='UTF-8'>คุณไม่มีสิทธิเข้าถึงใบลานี้<br><a href='/dashboard'>กลับไปยังระบบ intraLiNE</a>";
			//redirect('dashboard','refresh');
		}

	}

	public function submit($current_task){

 		$stfAbsID			= $this->input->post('id');
		$empID				= $this->input->post('empID');

		$dropdown_staff		= $this->input->post('dropdown_staff');

		$absence_type		= $this->input->post('absence_type');
		$dp1433735797502	= $this->input->post('date_start');
		$dp1433735797503	= !empty($_POST['date_end'])?$this->input->post('date_end'):"";
		$absDetail			= $this->input->post('detail');
		$statusApprove	 	= $this->input->post('statusApprove');

		//ลาเต็มวัน หรือ ลาชั่วโมง
		$status_absence	 =  $this->input->post('status_absence');
		$date_start 		= explode('/',$dp1433735797502); //สร้างตัวแปรมาเก็บวันที่
		$strYear_s 			= $date_start[2]-543;
		$strMonth_s			= $date_start[1];
		$strDay_s			= $date_start[0];

		$date_end 	  		= explode('/',$dp1433735797503); //สร้างตัวแปรมาเก็บวันที่
		$strYear_e 			= $date_end[2]-543;
		$strMonth_e			= $date_end[1];
		$strDay_e			= $date_end[0];

		$minute	 			= $this->input->post('minute');
		$minute2	 		= $this->input->post('minute2');
		$date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s." ".$minute;

		if($status_absence==2){
			$date_end 		= $strYear_s."-".$strMonth_s."-".$strDay_s." ".$minute2;
			$leaveDetail 	= "ลาวันที่ $strDay_s/$strMonth_s/$strYear_s\nเวลา $minute ถึง $minute2\n\n";
		}else{
			$minute 		= "00:00";
			$minute2 		= "00:00";
			$date_end 		= $strYear_e."-".$strMonth_e."-".$strDay_e." ".$minute2;
			$leaveDetail 	= "ตั้งแต่วันที่ $strDay_s/$strMonth_s/$strYear_s\nถึงวันที่ $strDay_e/$strMonth_e/$strYear_e\n\n";
		}

		$txtdateInput_start = $date_start;
		$txtdateInput_end 	= $date_end;

		$path = './uploads/absenceFileMedical/';
		$random = rand();

		$filename = $_FILES["absenceFileMedical"]["name"];
		$file = explode(".",$filename);
		$filename_change = $file[0].$random.".".$file[1];
		move_uploaded_file($_FILES['absenceFileMedical']['tmp_name'], $path.iconv('UTF-8','windows-874',$filename_change));
		if($filename ==""){
			$filename_change = "";
		}

		$data		=	array(
						'staffID'=>$dropdown_staff	,
						'absTypeID'=>$absence_type	,
						'absStartDate'=>$date_start,
						'absEndDate'=>$date_end	,
						'absDetail'=>$absDetail,
						'status_absence'=>$status_absence,
						'statusApprove'=>$statusApprove,
						'dateCreate' => date('Y-m-d H:i:s'),
						'absenceFile'=>$filename_change
		);

        if($_FILES["absenceFileMedical"]["name"] !=''){
			$data1		=	array(

				'absTypeID'=>$absence_type	,
				'absStartDate'=>$date_start,
				'absEndDate'=>$date_end	,
				'absDetail'=>$absDetail,
				'status_absence'=>$status_absence,
				'statusApprove'=>$statusApprove,
				'absenceFile'=>$filename_change
			);
		}else{
			$data1		=	array(

				'absTypeID'=>$absence_type	,
				'absStartDate'=>$date_start,
				'absEndDate'=>$date_end	,
				'absDetail'=>$absDetail,
				'status_absence'=>$status_absence,
				'statusApprove'=>$statusApprove,
				'absenceFile'=>$filename_change
			);
		}

		if($current_task == 'add'){
			$this->absence_model->insert_staff_absence($data);
			define('LINE_API',"https://notify-api.line.me/api/notify");

			$insert_id = $this->db->insert_id();
			$absData = $this->absence_model->getAbsenceData($insert_id);
			if($absData["staffNickName"] != null && $absData["staffNickName"] != "") {
				$staffNickName = " (".$absData["staffNickName"].")";
			} else {
				$staffNickName = "";
			}

			$str = 	"\n".
					"คุณ".$absData["staffName"].$staffNickName."\n".
					$absData["orgName"]."\n\n".
					"ขอ".$absData["absName"]."\n".
					$leaveDetail.
					"Link ขออนุมัติ http://hr.suksawad.co.th/absence/edit?id=".$insert_id."\n".
					"ผู้อนุมัติ - คุณ".$absData["managerName"];

			$token = $absData["lineToken"]; //"CYY7MUUXg2u1Eot7JB3bbkCUtsNZ7wdgfhDHIjUx01S"; //Production
			if($token !== null)
			{
				$res = $this->notify_message($str,$token);
			} else {
				$token = "289T2bd9N198Y35ipDuMxryPpbLWv79VQHxP2qQ9yTH"; //Test - ส่งมาที่ Group intraLiNE
				$res = $this->notify_message($str,$token);
			}
		} elseif ($current_task == 'edit'){
			$absData = $this->absence_model->getAbsenceData($stfAbsID);
			$filename= $_FILES["absenceFileMedical"]["name"];
			$file = explode(".",$filename_);
			$filename_change = $file[0].$random.".".$file[1];
			if($filename !=$this->input->post('remove_file',true)&& $filename !=""){
				@unlink("./uploads/absenceFileMedical/".$_POST["remove_file"]);
				move_uploaded_file($_FILES['absenceFileMedical']['tmp_name'], $path.$filename_change);
			}else{
				$filename_change = $_POST["remove_file"];
			}
			if($absData["statusApprove"] == 0 && ($statusApprove == "1" || $statusApprove == "2")) {
				if($statusApprove == "1") {
					$approveResult = "อนุมัติ";
				} elseif($statusApprove == "2") {
					$approveResult = "ไม่อนุมัติ";
				}
				$str = 	"\n".
						"คุณ".$absData["managerName"]." ได้พิจารณาการลาของ".
						"คุณ".$absData["staffName"].$staffNickName."\n".
						$absData["orgName"]."\n\n".
						"ขอ".$absData["absName"]."\n".
						$leaveDetail.
						"\n\nผลการพิจารณา - ".$approveResult;
				$token = $absData["lineToken"]; //"CYY7MUUXg2u1Eot7JB3bbkCUtsNZ7wdgfhDHIjUx01S"; //Production
				if($token !== null)
				{
					$res = $this->notify_message($str,$token);
				} else {
					$token = "289T2bd9N198Y35ipDuMxryPpbLWv79VQHxP2qQ9yTH"; //Test - ส่งมาที่ Group intraLiNE
					$res = $this->notify_message($str,$token);
				}
			}
			$this->absence_model->update_staff_absence($stfAbsID,$data1) ;
	 	}
		if($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==4) {
 			redirect(site_url().'absence', 'refresh');
		} else {
 			redirect(site_url().'dashboard', 'refresh');
		}
	}

	private function notify_message($message,$token){
		$queryData = array('message' => $message);
		$queryData = http_build_query($queryData,'','&');
		$headerOptions = array(
				'http'=>array(
					'method'=>'POST',
					'header'=> "Content-Type: application/x-www-form-urlencoded\r\n"
							."Authorization: Bearer ".$token."\r\n"
							."Content-Length: ".strlen($queryData)."\r\n",
					'content' => $queryData
				),
		);
		$context = stream_context_create($headerOptions);
		$result = file_get_contents(LINE_API,FALSE,$context);
		$res = json_decode($result);
		return $res;
	}

	public function delete(){
		$stfAbsID			= $this->input->get('id');
		$empID				= $this->input->get('empID');
		$data['data']	=	'';
		$this->absence_model->delete_staff_absence($stfAbsID);
        redirect(site_url().'absence', 'refresh');
	}

	public function cancelLeave(){
		$stfAbsID = $this->input->post('id');
		$this->absence_model->cancelLeave($stfAbsID);
        redirect(site_url().'dashboard', 'refresh');
	}

	public function viewf(){

		$stfAbsID			= $this->input->get('id');
		$empID				= $this->input->get('empID');

		$data				= $this->absence_model->view_absence($stfAbsID);

		$date_start			= DateThaiHelper($data['absStartDate']);
		$date_end			= DateThaiHelper($data['absEndDate']);

		$data['empID']					= $empID;
		$data['stfAbsID']				= $stfAbsID;

		$data['staffPreName']			= $data['staffPreName'];
		$data['staffFName']				= $data['staffFName'];
		$data['staffLName']				= $data['staffLName'];

		$data['absDetail']				= $data['absDetail'];
		$data['absTypeID']				= $data['absTypeID'];
		$data['current_task']			= 'edit';
		$data['var_agencies']			= $var_agencies;

		$data['date_start_put']			= $date_start;
		$data['date_end_put']			= $date_end;

		$this->template->load('template/admin', 'absence_view',$data);


	}

	public function peoplefinder(){
		$data['data']	=	'';
        $this->template->load('template/admin', 'peoplefinder',$data);
	}


}
?>
