<style>
  .ui-datepicker {
      width : 100%;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <section class="panel">
      <header class="panel-heading">
        <h3>ระบบใบลา</h3>
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span style="color:#A9A6A6;">ระบบบริการตัวเองของพนักงาน</span>
      </header>
    </section>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="col-md-2"><h5>ประวัติการลา</h5></div>
    <div class="col-md-8" style="padding-right:50px;">
      <form action="<?php echo base_url() ?>absence" method="get">
          <div class="col-md-1"></div>
          <div class="col-md-1"><h6>ปี</h6></div>
      <select name="filter_year" id="filter_year" class="form-control" style="width:100px; float:left"onchange="this.form.submit();">
        <?php
          for ($yi = date("Y")-5; $yi <= date("Y"); $yi++) {
            if($yi == $y){
              $selecty	=	"selected=\"selected\"selected\"";
            }else{
              $selecty	=	'';
            }
            echo '<option value="'.$yi.'"'.$selecty.' > '.($yi+543).' </option>';
          }
        ?>
      </select>
          <div class="col-md-1"><h6>เดือน</h6></div>
      <select name="filter_month" id="filter_month" class="form-control" style="width:170px; float:left" onchange="this.form.submit();">
        <option value="0">แสดงข้อมูลตลอดปี</option>
        <?php
          $strMonthCut = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
          for ($i = 1; $i <= 12; $i++) {
            if($i == $m){
              $select	=	"selected=\"selected\"selected\"";
            }else{
              $select	=	'';
            }
            echo '<option value="'.$i.'"'.$select.' > '.$strMonthCut[$i].' </option>';
          }
        ?>
      </select>
          <div class="col-md-1"><h6>วัน</h6></div>
      <select name="filter_day" id="filter_day" class="form-control" style="width:180px" onchange="this.form.submit();">
        <option value="0">แสดงข้อมูลตลอดเดือน</option>
        <?php
          $strDayCut = array("","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31");
          for ($di = 1; $di <= 31; $di++) {
            if($di == $d){
              $selectd = "selected=\"selected\"selected\"";
            }else{
              $selectd = '';
            }
            echo '<option value="'.$di.'"'.$selectd.' > '.$strDayCut[$di].' </option>';
          }
        ?>
      </select>
      <br>
      <div class="col-md-2"><h6>หน่วยงาน</h6></div>
     <select class="xcrud-input form-control" data-type="select" name="orgID" id="orgID"  style="max-width: 570px;" onchange="this.form.submit();"> 
          <option value="">----กรุณาเลือก-----</option>
          <?php 
              echo getDropdownTree('0','','2',$this->input->get('orgID'));
          ?>
      </select>
      </form>
    </div>
    <div class="col-md-1" style="padding-left:50px;padding-top:5px"><a class="btn btn-success" href="<?=base_url()?>absence/add"><i class="glyphicon glyphicon-plus-sign"></i> สร้างใบลา</a>  </div>
    <br><br><br><br>
  <table class="table">
      <thead>
        <tr>
          <th class="text-center">วันที่สร้างใบลา</th>
          <th colspan="2" class="text-center">วันที่ลา</th>
          <th class="text-center">ผู้ลา</th>
          <th class="text-center">หน่วยงาน</th>
          <th class="text-center">ประเภท</th>
          <th class="text-center">เหตุผล</th>
          <th class="text-center">สถานะ</th>
          <th class="text-center">ไฟล์</th>
          <th class="text-center">รายละเอียด</th>
        </tr>
      </thead>
      <tbody>
      <?php
    	if($abs != NULL){
          $rowclass = "";
      		foreach($abs->result() as  $row){

            switch ($row->absTypeID) {
              case 1:
              case 8:
                $rowclass = "danger";
                break;
              case 2:
                $rowclass = "info";
                break;
              case 7:
                $rowclass = "success";
                break;
              default:
                $rowclass = "warning";
              }

            switch ($row->statusApprove) {
              case 1:
                $statusApprove = "อนุมัติแล้ว";
                break;
              case 2:
                $statusApprove = "ไม่อนุมัติ";
                break;
              case 3:
                $statusApprove = "ยกเลิก";
                break;
              default:
                $statusApprove = "รออนุมัติ";
              }

              if($row->statusApprove == 3) {
                $rowclass .= " cancelled";
              }

              $dateCreate = $row->dateCreate;
              if(empty($dateCreate) or $dateCreate=='0000-00-00 00:00:00'){
                $dateCreate = '-';
              }else{
                $dateCreate = toFullDate($row->dateCreate,'th');
              }

        ?>
        <tr class="<?=$rowclass?>">
          <td style="width:9%;" class="text-center"><?=$dateCreate;?></td>
          <td style="width:9%;" class="text-center"><?=toFullDate($row->absStartDate,'th');?></td>
          <td style="width:9%;" class="text-center"><?=toFullDate($row->absEndDate,'th');?></td>
          <td style="width:20%;"><?=$row->staffName?></td>
          <td style="width:13%;"><?=$row->orgName?></td>
          <td style="width:20%;"><?=$row->absName?></td>
          <td style="width:15%;"><?=$row->absDetail?></td>
          <td style="width:10%;" class="text-center"><?=$statusApprove?></td>
          <td><a href="<?php echo site_url()."uploads/absenceFileMedical/".$row->absenceFile ?>" target="_blank" style="color:blue"><?php if($row->absenceFile != ''){echo '<i class="fa fa-medkit fa-2x" title="ไฟล์"></i>';}?></a></td>
          <td style="width:15%;" class="text-center">
            <?php if($row->statusApprove!=3) {?>
              <a class="btn btn-warning btn-sm"
                href="<?=base_url()?>absence/edit?id=<?=$row->stfAbsID?>&amp;empID=<?=$row->staffID?>"
                title="แก้ไข"><i class="glyphicon glyphicon-edit"></i></a>
            <?php
            } else {
            ?>
            <a class="btn btn-default btn-sm"
              href="javascript:alert('ใบลานี้ยกเลิกไปแล้ว ไม่สามารถแก้ไขได้');"
              title="แก้ไข"><i class="glyphicon glyphicon-edit"></i></a>

            <a class="btn btn-danger btn-sm"
              title="ลบ"
              href="javascript:delete_id('<?=$row->stfAbsID; ?>')"
              data-primary="7"
              data-task="remove"
              data-confirm="ต้องการลบใช่หรือไม่?"><i class="glyphicon glyphicon-remove"></i></a>
            <?php
            }
            ?>
          </td>
        </tr>
	    <?php
	    } //foreach
	    }else{?>
	    <tr><td colspan="9" style="text-align:center">ไม่พบข้อมูล</td></tr>
	    <?php
    	}?>
      </tbody>
    </table>
  </div>
</div>
<script>
function delete_id(id)
{
     if(confirm('ต้องการลบใช่หรือไม่ ?'))
     {
        window.location.href='<?=base_url(); ?>absence/delete?id='+id;
     }
}

$(document).ready(function () {
    $("#calMonthCurrent").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        yearOffSet: 543,
        yearRange: '-20:+0',
        changeMonth: false,
        changeYear: false ,
        showOn: "both",
        buttonImage: "<?=base_url()?>assets/images/common_calendar_month_-20.png",
        buttonImageOnly: false,
        inline: true
    });

    $("#calMonthNext").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        yearOffSet: 543,
        yearRange: '-20:+0',
        changeMonth: false,
        changeYear: false ,
        showOn: "both",
        buttonImage: "<?=base_url()?>assets/images/common_calendar_month_-20.png",
        buttonImageOnly: false,
        inline: true,
        defaultDate: '+1m'
    });

    $("#calMonthPrev").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
        yearOffSet: 543,
        yearRange: '-20:+0',
        changeMonth: false,
        changeYear: false ,
        showOn: "both",
        buttonImage: "<?=base_url()?>assets/images/common_calendar_month_-20.png",
        buttonImageOnly: false,
        inline: true,
        defaultDate: '-1m'
    });
});
</script>
