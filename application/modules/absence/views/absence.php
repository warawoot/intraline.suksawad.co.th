<div class="row">
    <div class="col-sm-12">
        <section class="panel">

        	 <br /><br />
            	<div class="panel-body">
                   <form action="<?php echo base_url() ?>absence" method="post" class="col-md-12" >
                                 <div class="form-group">
                                    <label class="control-label col-sm-3" style="text-align:right;">เดือน</label>
                                    <div class="col-sm-3">
                                        <select name="signup_birth_month" id="signup_birth_month" class="xcrud-input form-control form-control " >
                                        <option value="">Select Month</option>
                                        <?php
											$strMonthCut = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
											for ($i = 1; $i <= 12; $i++) {
												if($i == date("m")){
													$select	=	"selected=\"selected\"";
												}else{
													$select	=	'';
												}
												 echo '<option value="'.$i.'"'.$select.' > '.$strMonthCut[$i].' </option>';
											}
                                         ?>
                                        </select>
                                    </div>
                                </div>
                         </form>
                </div>

         </section>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3>ใบลา</h3>
            </header>
            <div class="panel-body">
                <?php echo $data; ?>
            </div>
        </section>
    </div>
</div>
<div>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/peoplefinder/javascripts/smmms-peopleFinder/smmms-peopleFinder.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/peoplefinder/stylesheets/font-awesome-4.4.0/css/font-awesome.min.css">
<style>
	  #comboA_popup .pf-popup-content-table-col2{
		width: 500px;
	  }
	  #comboA_popup .pf-popup-content-table-col2 p:nth-child(1) {
		font-weight: bold;
	  }
	  #comboA_popup .pf-popup-content-table-col2 p:nth-child(2) {
		font-style: italic;
		font-size: 90%;
	  }
</style>
<!--<input size="30" id="comboA" name="comboA" class="peopleFinder" placeholder="Name or Surname here ..."></input>-->
</div>
<br /><br /><br /><br /><br /><br /><br />
<div>
 </div>
<script src="<?php echo base_url(); ?>assets/peoplefinder/javascripts/smmms-peopleFinder/jquery.smmms-peopleFinder-Xcrud.js"></script>
  <script type="text/javascript">
   /* $(document).ready( function() {

        $('#dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--').smmmsPeopleFinder({
          url: '<?php echo base_url(); ?>peoplefinder',
		  <?php if($current_task == 'edit'){?>
		  preloadFromUrl: '<?php echo base_url(); ?>peoplefinder/edit?id=<?php echo $empID; ?>',
		  <?php } ?>
          idColumn: 'uid',
		  imageFolderUrl: '<?php echo base_url(); ?>uploads/',
          textColumn: 'name',
          displayColumns: [
            {'map': 'image', 'image': true},
            {
              'map': 'name'
            },
            [
              {
                'map': 'department'
              },
              {
                'map': 'position'
              }
            ]
          ],
          rows: 20
        });

    });*/
  </script>
<script type="text/javascript">

  <?php //if($current_task != 'edit'){?>

   /* var x = document.getElementsByName("dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--");
    $(".col-sm-9").find(x).attr("id","dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--");

    $('#dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--').after ("<input size=\"30\" id=\"dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--\" name=\"dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--\" class=\"xcrud-input peopleFinder\"  data-required=\"1\" type=\"text\" placeholder=\"Name or Surname here ...\"></input>");
    $('#dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--').remove ("");*/

	<?php //} ?>

	$(".xcrud-top-actions").find('.btn-success').attr("onClick","save_Job();");

	//$(".btn-group").find('.btn-warning').attr("onClick","edit_Job();");

    function modal_back_back(){
      // location.href = '<?php echo base_url(); ?>'+'evaluation/group/?year=2559&round=1&formid=6';
    }

	function save_Job(){
 		 location.replace('<?php echo base_url(); ?>absence/add');
	}

	function edit_Job(){
		location.href = '<?php echo base_url(); ?>absence/';
	}

	function myFunction(job_id,job_level,line_id){
		if (confirm("ต้องการลบใช่หรือไม่") == true) {
			location.replace('<?php echo base_url()  ?>absence/' );
		} else {
			location.replace('<?php echo base_url()  ?>absence/');
		}
	}

	//var rates			= $('#dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--').val();
	jQuery(document).on("xcrudafterrequest",function(event,container){


        var ids = $('input:hidden[name=primary]').val();
		//var id = $('input:hidden[name=dGJsX3N0YWZmX2Fic2VuY2Uuc3RhZmZJRA--_id]').val();
		//alert(id) ;
		if(Xcrud.current_task == 'save')
		{
			//alert(ids) ;
 		 	location.replace('<?php echo base_url(); ?>absence/');
		}
		if(Xcrud.current_task == 'edit')
		{
 		 	//location.replace('<?php echo base_url(); ?>conedu/');
		}
		if(Xcrud.current_task == 'remove')
		{
 		 	location.replace('<?php echo base_url(); ?>absence/');
		}
        if (Xcrud.current_task == 'list') {
            //$(".xcrud-top-actions").find('.btn-group').append('<a href="javascript:;" data-task="list" class="btn btn btn-primary" onclick="modal_back_back()">ย้อนกลับหน้ากลุ่มประเมิน</a>');
			location.replace('<?php echo base_url(); ?>absence/');
        };
 	});

</script> 
