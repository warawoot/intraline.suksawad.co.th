<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3>ข้อมูลใบลา </h3>
            </header>
         </section>
    </div>
</div>
<style type="text/css">
	.form-control {
 	  color: #343232;
	}
    #ui-datepicker-div{
		font-size: 0.9em;
 	}
 	.ui-datepicker-trigger{
	  opacity: .5;
	}
</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php
		$jquery_ui_v="1.8.5";
		$theme=array(
			"0"=>"base",
			"1"=>"black-tie",
			"2"=>"blitzer",
			"3"=>"cupertino",
			"4"=>"dark-hive",
			"5"=>"dot-luv",
			"6"=>"eggplant",
			"7"=>"excite-bike",
			"8"=>"flick",
			"9"=>"hot-sneaks",
			"10"=>"humanity",
			"11"=>"le-frog",
			"12"=>"mint-choc",
			"13"=>"overcast",
			"14"=>"pepper-grinder",
			"15"=>"redmond",
			"16"=>"smoothness",
			"17"=>"south-street",
			"18"=>"start",
			"19"=>"sunny",
			"20"=>"swanky-purse",
			"21"=>"trontastic",
			"22"=>"ui-darkness",
			"23"=>"ui-lightness",
			"24"=>"vader"
		);
		$jquery_ui_theme=$theme[0];
?>
<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/<?=$jquery_ui_v?>/themes/<?=$jquery_ui_theme?>/jquery-ui.css" />
<style type="text/css">
/* ปรับขนาดตัวอักษรของข้อความใน tabs
สามารถปรับเปลี่ยน รายละเอียดอื่นๆ เพิ่มเติมเกี่ยวกับ tabs
*/
.ui-tabs{
    font-family:tahoma;
    font-size:11px;
}
.ui-widget-header {
    color: #000;
    font-weight: bold;
}

/* Overide css code กำหนดความกว้างของปฏิทินและอื่นๆ */
.ui-datepicker{
    width:220px;
    font-family:tahoma;
    font-size:11px;
    text-align:center;
}
</style>
<div class="xcrud-container">
        <div class="xcrud-ajax">

            <div class="xcrud-view">
                <div class="form-horizontal">


                  <div class="form-group">
                        <label class="control-label col-sm-3">พนักงาน*</label>
                        <div class="col-sm-9">
                            <?php
                                   echo $staffPreName.$staffFName .'&nbsp;'. $staffLName;

                            ?>
                        </div>
                    </div>
                  <div class="form-group">
                        <label class="control-label col-sm-3">ประเภทการลา*</label>
                        <div class="col-sm-9">
                             <?php
                                  echo $absName;
                            ?>
                       </div>
                   </div>
                  <div class="form-group">
                    <label class="control-label col-sm-3">วันที่ลา*</label>
                    <div class="col-sm-9">
                        <?php echo $date_start_put; ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-3">ถึงวันที่*</label>
                        <div class="col-sm-9">
                           <?php echo $date_end_put; ?>
                        </div>
                  </div>                
                  <div class="form-group">
                    <label class="control-label col-sm-3">หมายเหตุ</label>
                        <div class="col-sm-9">
                          <?php echo ($absDetail !='')?$absDetail:''; ?>
                        </div>
                  </div>
                 </div>
                  <div class="xcrud-top-actions btn-group"><a href="<?php echo base_url()?>absence"><input class="btn btn-warning xcrud-action" type="button" value="ย้อนกลับ"></a>
                </div>
                <div class="xcrud-nav">  </div>

           </div>
         </div>
         <div class="xcrud-overlay" style="display: none;"></div>
    </div>
