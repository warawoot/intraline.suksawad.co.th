<style>
    .pf-input{
        float:left;
    }

    .pf-popup{
        margin-top:45px;
    }

    .pf-search-button{
        width: 40px !important;
        height: 40px !important;
        float:left;
        margin-left:5px;
        margin-top:10px;
        border : solid #CCC 1px !important;
        <?php if($this->session->userdata('roleID')==3){ ?>
            display : none;
        <?php }?>
         <?php if($this->session->userdata('roleID')==4){ ?>
            display : none;
        <?php }?>
    }

    .ui-datepicker-trigger {
        float:left;
        margin-left:5px;
        margin-top:10px;
    }

    .hasDatepicker {
        float:left;
    }
</style>
<section class="panel">
<div class="row">
    <div class="col-sm-12">
            <header class="panel-heading">
                <h3>ข้อมูลใบลา : <?=$staffName?></h3>
            </header>
    </div>
</div>
<div class="row">
  <div class="col-md-3">
    <div class="pastel-pink" style="padding:20px">
      <h4>ลาป่วย : <?=$sa1["numDay"]?> วัน</h4>
      <h6>สามารถลาได้ <?=$ac1["numDay"]?> วัน ได้รับค่าจ้างปกติ</h6>
    </div>
    <div class="icon">
      <i class="ion ion-medkit"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-blue" style="padding:20px">
      <h4>ลากิจ : <?=$sa2["numDay"]?> วัน</h4>
      <h6>ไม่จ่ายค่าจ้างตามจำนวนวันลา</h6>
    </div>
    <div class="icon">
      <i class="ion ion-briefcase"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-green" style="padding:20px">
      <h4>ลาพักผ่อน : <?=$sa7["numDay"]?> วัน</h4>
      <h6>จากสิทธิ <?=$ac7["numDay"]?> วัน / การทำงาน 1 ปี</h6>
    </div>
    <div class="icon">
      <i class="ion ion-ios-partlysunny-outline"></i>
    </div>
  </div>
  <div class="col-md-3">
    <div class="pastel-yellow" style="padding:20px">
      <h4>ลาอื่นๆ : <?=$sao["numDay"]?> วัน</h4>
      <h6>&nbsp;</h6>
    </div>
    <div class="icon">
      <i class="ion ion-email"></i>
    </div>
  </div>
</div>
<br><br>
</section>

<div class="xcrud-container">
    <div class="xcrud-ajax">
        <div class="xcrud-view">
            <form id="formMain" action="<?=base_url(); ?>absence/submit/<?php echo $current_task;?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <?php
                if($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==4) {
                    if($current_task=="add") {
                ?>
                <div class="form-group">
                    <label class="control-label col-sm-3">พนักงาน <strong style="color:red">*</strong></label>
                    <div class="col-sm-9">
                        <input
                            data-required="1"
                            type="text"
                            style="width:450px"
                            data-type="date"
                            class="form-control peopleFinder pf-input"
                            name="dropdown_staff"
                            id="dropdown_staff"
                            autocomplete="off"
                            placeholder="ค้นหาชื่อ........"
                            <?php echo ($current_task == 'edit') ? 'disabled="disabled"' : "";?>>
                    </div>
                </div>
                <?php
                    } else {?>
                <div class="form-group">
                    <label class="control-label col-sm-3">พนักงาน <strong style="color:red">*</strong></label>
                    <div class="col-sm-9" style="padding-top:5px;">
                        <label style="font-weight:bold"><?=$staffName?></label>
                        <input type="hidden" value="<?php echo ($empID!='')?$empID:''; ?>" id="dropdown_staff" name="dropdown_staff">
                    </div>
                </div>
                <?php
                    }
                } else {?>
                    <input type="hidden" value="<?=$this->session->userdata('ID')?>" id="dropdown_staff" name="dropdown_staff">
                <?php
                }
                ?>

                <div class="form-group">
                    <label class="control-label col-sm-3">ประเภทการลา <strong style="color:red">*</strong></label>
                    <div class="col-sm-3">
                        <?php
                        echo form_dropdown('absence_type',$absence_type ,$absTypeID ,'id ="absence_type"
                            class="form-control" style="width: 360px" data-required="1" data-unique="" data-type="select" onChange="setvallength(this.value)" ');
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-3">
                        <?php
                        $status =  !empty($status_absence)?$status_absence:"0";
                        ?>
                        <div class="radio">
                            <label><input type="radio" <?php echo ($status==1 || $status == 0)?'checked="checked"':'';?>  name="status_absence"  id="status_absence"  value="1">ลาเต็มวัน</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" <?php echo ($status==2)?'checked="checked"':'';?> name="status_absence" id="status_absence"  value="2">ลาชั่วโมง</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">วันที่ลา <strong style="color:red">*</strong></label>
                    <div class="col-sm-6">
                        <input
                            data-required="1"
                            type="text"
                            style="width:250px;background-color:#FFF"
                            data-type="date"
                            name="date_start"
                            id="date_start"
                            readonly=true
                            class="form-control input-small"
                            value="<?php echo ($date_start_put !='')?$date_start_put:''; ?>">
                    </div>
                </div>

                <div class="form-group" id="time" style="display: none;padding-left:10px">
                    <div class="row" style="padding:10px">
                        <label class="control-label col-sm-3" style="color:#000">ตั้งแต่เวลา <strong style="color:red">*</strong></label>
                        <div class="col-sm-3">
                            <div class="input-group bootstrap-timepicker timepicker datepicker">
                                <input id="minute" name="minute"
                                    value="<?php echo !empty($minute)?$minute:""?>"
                                    type="text"
                                    style="width: 250px;margin-left:-5px;z-index:0 !important"
                                    class="form-control input-small">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding:10px">
                        <label class="control-label col-sm-3" style="color:#000">ถึงเวลา <strong style="color:red">*</strong></label>
                        <div class="col-sm-3">
                            <div class="input-group bootstrap-timepicker timepicker datepicker">
                                <input id="minute2" name="minute2"
                                    value="<?php echo !empty($minute2)?$minute2:""?>"
                                    type="text"
                                    style="width: 250px;margin-left:-5px;z-index:0 !important"
                                    class="form-control input-small">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group" id="dateEnd">
                    <label class="control-label col-sm-3">ถึงวันที่ <strong style="color:red">*</strong></label>
                    <div class="col-sm-6">
                        <input  data-required="1"
                            style="width:250px;background-color:#FFF"
                            type="text"
                            data-type="date"
                            name="date_end"
                            id="date_end"
                            readonly=true
                            class="form-control input-small"
                            value="<?php echo ($date_end_put !='')?$date_end_put:''; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">ข้อความประกอบการลา</label>
                    <div class="col-sm-4">
                        <textarea name="detail"
                            class="xcrud-input form-control"
                            rows="5"
                            data-type="text"><?php echo ($absDetail !='')?$absDetail:''; ?></textarea>
                    </div>
                </div>

                <div class="form-group" id="medical" style="display:none">
                     <label class="control-label col-sm-3">แนบไฟล์ใบรับรองแพทย์</label>
                        <div class="col-sm-9">
                            <input type="file" name="absenceFileMedical"  id="absenceFileMedical" class="form-controll" style="display:none">
                            <?php
                            if($current_task == 'edit'){
                              echo $absenceFile;

                            }

                            ?>
                         </div>
                </div>

                <?php
                if(($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==2 || $this->session->userdata('roleID')==4) && $current_task=="edit" && ($this->session->userdata('ID') != $staffID)) {
                ?>
                <div class="form-group">
                    <label class="control-label col-sm-3">ผลการพิจารณา</label>
                    <div class="col-sm-3">
                        <div class="radio">
                            <label><input type="radio" name="statusApprove" id="statusApprove" <?php echo ($statusApprove==1)?'checked="checked"':'';?> value="1">อนุมัติ</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="statusApprove" id="statusApprove" <?php echo ($statusApprove==2)?'checked="checked"':'';?> value="2">ไม่อนุมัติ</label>
                        </div>
                    </div>
                </div>
                <?php
                } elseif($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==4 && $current_task=="add") {
                ?>
                    <input type="hidden" value="1" id="statusApprove" name="statusApprove">
                <?php
                } else {
                ?>
                    <input type="hidden" value="0" id="statusApprove" name="statusApprove">
                <?php
                }
                ?>
                <br><br>

            </div>

            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="xcrud-top-actions btn-group">
                    <input id="empID"
                        name="empID"
                        type="hidden"
                        value="<?php echo ($empID!='')?$empID:''; ?>" />
                    <input id="lineID"
                        name="lineID"
                        type="hidden"
                        value="" />
                    <input id="stfAbsID" name="id" type="hidden"  value="<?php echo ($stfAbsID!='')?$stfAbsID:''; ?>" />
                    <button class="btn btn-primary xcrud-action"
                        id="btsave"
                        name="btsave"
                        type="submit"
                        >บันทึกและย้อนกลับ</button>
                    <button class="btn btn-warning xcrud-action" type="button" onclick="history.back();">ย้อนกลับ</button>
                    <button class="btn btn-danger xcrud-action"
                        type="button"
                        id="btcancel"
                        name="btcancel"
                        style="margin-left:20px">ยกเลิกการลา</button>
               </div>
            </div>
            <div class="xcrud-nav"></div>
        </form>
    </div>
</div>
<div class="xcrud-overlay" style="display: none;"></div>
</div>
<script>
    var dateToday = new Date();

    $.datepicker.regional['th'] ={
        changeMonth: true,
        changeYear: true,
        yearOffSet: 543,
        showOn: "button",
        buttonImage: '<?=base_url()?>assets/images/common_calendar_month_-20.png',
        buttonImageOnly: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
        constrainInput: true,
        //setDate : new Date(),
        <?php if($this->session->userdata('roleID')!=3) { ?>
        minDate: dateToday,
        <?php }?>

        prevText: 'ก่อนหน้า',
        nextText: 'ถัดไป',
        yearRange: '-5:+1',
        buttonText: 'เลือก',
    };

    $.datepicker.setDefaults($.datepicker.regional['th']);

    $(function() {
        $( "#date_start" ).datepicker( $.datepicker.regional["th"] );

        $( "#date_end" ).datepicker( $.datepicker.regional["th"] );

    <?php if($current_task=="add") {?>
        $( "#date_start" ).datepicker("setDate", new Date());
        $( "#date_end" ).datepicker("setDate", new Date());
    <?php }?>
    });

    //=====================================================================================================
    //On Selected Date
    //On Change Drop Down
    function ChangMonthAndYear(year, month, inst) {

        GetDaysShows(month, year);
    }

    //=====================================
    function GetDaysShows(month, year) {
        //CallGetDayInMonth(month, year); <<เป็น Function ที่ผมใช้เรียก ajax เพื่อหาวันใน DataBase  แต่นี้เป็นเพียงตัวอย่างจึงใช้ Array ด้านล่างแทนการ Return Json
        //อาจใช้ Ajax Call Data โดยเลือกจากเดือนและปี แล้วจะได้วันที่ต้องการ Set ค่าวันไว้คล้ายด้านล่าง
        Holidays = [1,4,6,11]; // Sample Data
    }
    //=====================================

</script>
<script>
$(document).ready(function () {
    if($('#stfAbsID').val()!=""){
        <?php if($status == 1){?>
            $('#time').hide();
            $('#dateEnd').show();
            $('#minute').val('<?php echo $minute ?>');
            $('#minute2').val('00:00');
        <?php }else if($status == 2){ ?>
            $('#time').show();
            $('#dateEnd').hide();
        <?php } ?>
    } else {
        $('#btcancel').hide();
    }

    $('input[type=radio][name=status_absence]').change(function(){
        if($(this).val()==1){
            <?php if($current_task != 'edit'){?>
                $('#minute').val('00:00');
                $('#minute2').val('00:00');
            <?php } ?>
            $('#time').hide();
            $('#dateEnd').show();
        }else if($(this).val()==2){
            $('#time').show();
            $('#dateEnd').hide();
            <?php if($current_task != 'edit'){?>
            $('#minute').val('08:00');
            $('#minute2').val('13:00');
            <?php } ?>
        }
    });


    $('#btsave').on('click', function () {

        /* เฉพาะเป็น Admin
		if ($('#dropdown_staff').val() == '') {
			alert('กรุณากรอกข้อมูล');
			$('#dropdown_staff').focus();
			return false;
		}
        */
		if ($('#absence_type').val() == '') {
			alert('กรุณาเลือกประเภทการลา');
			$('#absence_type').focus();
			return false;
		}
		if ($('#date_start').val() == '') {
			alert('กรุณาเลือกวันที่ลา');
			$('#date_start').focus();
			return false;
		}
		if ($('#detail').val() == '') {
			alert('กรุณากรอกข้อมูล');
			$('#detail').focus();
			return false;
		}

	});

    $('#btcancel').on('click', function () {
        if(confirm("ต้องการยกเลิกการลาใช่หรือไม่")) {
            $('#formMain').attr('action', "<?=base_url(); ?>absence/cancelLeave").submit();
        } else {
            return false;
        }
	});

 });
</script>
<script>

    function setvallength(){

		var index = document.getElementById('absence_type').value;
		if(index == '1'){
            $("#medical").show();
			document.getElementById('absenceFileMedical').style.display='';
			document.getElementById('absenceFileMedical').required=false;
		}else{
            $("#medical").hide();
			document.getElementById('absenceFileMedical').style.display='none';
			document.getElementById('absenceFileMedical').required=false

        }
	}

  window.onload=function(){

        <?php if($current_task == 'edit'):?>
          <?php      if($absenceFile =='' ) : ?>
            var index = document.getElementById('absence_type').value;
        if(index == '1'){
                $("#medical").show();
          document.getElementById('absenceFileMedical').style.display='';
          document.getElementById('absenceFileMedical').required=true;
        }else{
                $("#medical").hide();
          document.getElementById('absenceFileMedical').style.display='none';
          document.getElementById('absenceFileMedical').required=false

            }
        <?php else: ?>
        var index = document.getElementById('absence_type').value;
        if(index == '1'){
                $("#medical").show();
          document.getElementById('absenceFileMedical').style.display='';
          document.getElementById('absenceFileMedical').required=false;
        }else{
                $("#medical").hide();
          document.getElementById('absenceFileMedical').style.display='none';
          document.getElementById('absenceFileMedical').required=false

            }
        <?php endif; ?>
        <?php endif; ?>

    };


</script>

<?php if($this->session->userdata('roleID')==3 || $this->session->userdata('roleID')==4) { ?>
<script>
$(document).ready(function () {
    $('#dropdown_staff').smmmsPeopleFinder({
        url: '<?=base_url(); ?>peoplefinder',
        <?php if($current_task == 'edit'){?>
            preloadFromUrl: '<?=base_url(); ?>peoplefinder/edit?id=<?php echo $empID; ?>',
        <?php } ?>
        idColumn: 'uid',
        imageFolderUrl: '<?=base_url(); ?>assets/staff/',
        textColumn: 'name',
        displayColumns: [
            {'map': 'image', 'image': true},
            {
                'map': 'name'
            },
            [
                {
                'map': 'department'
                },
                {
                'map': 'position'
                }
            ]
        ],
        rows: 20
    });
});
</script>
<!--- people finder -->
<link rel="stylesheet" href="<?=base_url(); ?>assets/peoplefinder/javascripts/smmms-peopleFinder/smmms-peopleFinder.css">
<script src="<?=base_url(); ?>assets/peoplefinder/javascripts/smmms-peopleFinder/jquery.smmms-peopleFinder-Xcrud.js"></script>
<?php } ?>

<link type="text/css" href="<?=base_url()?>assets/bootstrap/timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"/>
<script type="text/javascript"  src="<?=base_url()?>assets/bootstrap/timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
  $('#minute').timepicker({
          showMeridian : false,
          defaultTime: '08:00'
    });
  $('#minute2').timepicker({
          showMeridian : false,
          defaultTime: '13:00'
    });
</script>
