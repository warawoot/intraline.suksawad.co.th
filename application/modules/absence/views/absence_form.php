<div class="row">
    <div class="col-sm-12">
        <section class="panel">

        	 <br /><br />
            	<div class="panel-body">
                   <form action="<?php echo base_url() ?>absence" method="post" class="col-md-12" >
                                 <div class="form-group">
                                    <label class="control-label col-sm-3" style="text-align:right;">เดือน</label>
                                    <div class="col-sm-3">
                                        <select name="signup_birth_month" id="signup_birth_month" class="xcrud-input form-control form-control " onchange="this.form.submit();">
                                        <option value="">Select Month</option>
                                        <?php
											$strMonthCut = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
											for ($i = 1; $i <= 12; $i++) {
												if($i == $m){
													$select	=	"selected=\"selected\"";
												}else{
													$select	=	'';
												}
												 echo '<option value="'.$i.'"'.$select.' > '.$strMonthCut[$i].' </option>';
											}
                                         ?>
                                        </select>
                                    </div>
                                </div>
                         </form>
                </div>

         </section>
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3>ข้อมูลใบลา</h3>
            </header>
            <div class="panel-body">
                <link href="http://localhost/dpo_ehr/xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="http://localhost/dpo_ehr/xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                <link href="http://localhost/dpo_ehr/xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                <link href="http://localhost/dpo_ehr/xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css"><div class="xcrud">
        <div class="xcrud-container">
        <div class="xcrud-ajax">
            <input type="hidden" class="xcrud-data" name="key" value="ddab148b1dd8ba43999118db3e3d28516e11f5e8"><input type="hidden" class="xcrud-data" name="orderby" value=""><input type="hidden" class="xcrud-data" name="order" value="asc"><input type="hidden" class="xcrud-data" name="start" value="0"><input type="hidden" class="xcrud-data" name="limit" value="50"><input type="hidden" class="xcrud-data" name="instance" value="f3cadf03e66b2be8934f0164fe101c8f0117f47c"><input type="hidden" class="xcrud-data" name="task" value="list">        <div class="xcrud-top-actions">
            <div class="btn-group pull-right">
                </div>
            <a href="<?php echo base_url(); ?>absence/add" class="btn btn-success " ><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</a>
            <!--<a class="xcrud-search-toggle btn btn-warning" href="javascript:;">ค้นหา</a>-->
            <span class="xcrud-search form-inline" style="display:none;"><span class="btn-group"><a class="xcrud-action btn btn-primary" href="javascript:;" data-search="1">ตกลง</a>
            </span></span>
            <div class="clearfix"></div>
        </div>
        <div class="xcrud-list-container">

            <table class="xcrud-list table table-striped table-hover table-bordered">
            <thead>
                <tr class="xcrud-th">
                	<th class="xcrud-num">#</th>
                    <th data-order="asc" data-orderby="tbl_staff_absence.pfl" class="xcrud-column xcrud-action">ชื่อ นามสกุล</th>
                    <th class="xcrud-column xcrud-action" data-order="asc" data-orderby="tbl_staff_absence.orgName2">หน่วยงาน</th>
                    <th data-order="asc" data-orderby="tbl_staff_absence.absTypeID" class="xcrud-column xcrud-action">ประเภทการลา</th>
                    <th data-order="asc" data-orderby="tbl_staff_absence.absStartDate" class="xcrud-column xcrud-action" style="width: 160px">วันที่ลา</th>
                    <th data-order="asc" data-orderby="tbl_staff_absence.absDetail" class="xcrud-column xcrud-action">หมายเหตุ</th>
                    <th class="xcrud-actions" style="width: 160px">Action</th>
                 </tr>
            </thead>
            <tbody>
            <?php
			if(is_array($list_absence)){
			foreach ($list_absence as $key => $item) {
			?>
            	 <tr class="xcrud-row xcrud-row-0">
                	<td class="xcrud-current xcrud-num"><?php echo ++$key; ?></td>
                    <td><?php echo $item->name_lastname; ?></td>
                    <td>&nbsp; <?php  //echo $item->orgID;?>
                    	<?php
                        		$list_org_chart1 = $this->absence_model->list_org_chart1($item->orgID);
 								if($list_org_chart1['orgName'] !=''){
									echo $list_org_chart1['orgName']   ;
 								}else{
 									echo '' ;
								}
 						?>
                    	<?php
                        		$list_org_chart2 = $this->absence_model->list_org_chart2($item->orgID);
								if($list_org_chart2['orgName'] !=''){
									echo $list_org_chart2['orgName'] ;

								}else{
									echo '' ;
								}
 						?>
                    	<?php
                        		$list_org_chart3 = $this->absence_model->list_org_chart3($item->orgID);
								if($list_org_chart3['orgName'] !=''){
									echo $list_org_chart3['orgName'];
								}else{
									echo ''.$list_org_chart11['orgName'];
								}
 						?>
                        <?php
                        		$list_org_chart4 = $this->absence_model->list_org_chart4($item->orgID);
								if($list_org_chart4['orgName'] !=''){
									echo $list_org_chart4['orgName'];
								}else{
									echo ''.$list_org_chart11['orgName'];
								}
 						?>


                    </td>
                    <td><?php echo $item->absName; ?></td>
                    <td><?php echo DateThaiHelper($item->absStartDate); ?> - <br><?php echo DateThaiHelper($item->absEndDate); ?></td>
                    <td><?php echo $item->absDetail; ?></td>
                    <td class="xcrud-current xcrud-actions xcrud-fix">
                        <span class="btn-group">
                            <a class="btn btn-default btn-sm btn-warning" href="<?php echo base_url(); ?>absence/edit?id=<?php echo $item->stfAbsID; ?>&empID=<?php echo $item->staffID; ?>" title="แก้ไข"><i class="glyphicon glyphicon-edit"></i></a>

                            <a class=" btn btn-danger btn-sm" title="ลบ" href="javascript:delete_id(<?php echo $item->stfAbsID; ?>)" data-primary="7" data-task="remove" data-confirm="ต้องการลบใช่หรือไม่?"><i class="glyphicon glyphicon-remove"></i></a></span>
                    </td>
                 </tr>
             <?php }
			}else{ ?>
			 <tr class="xcrud-row xcrud-row-0">
                	<td colspan="7" class="xcrud-current xcrud-num">  ไม่พบข้อมูล   </td>
             </tr>
              <?php
			}
			 ?>
             </tbody>
            <tfoot>
           </tfoot>
        </table>
        </div>
        <div class="xcrud-nav">
           <!-- <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio"><button type="button" class="btn btn-default xcrud-action" data-limit="10">10</button><button type="button" class="btn btn-default active xcrud-action" data-limit="50">50</button><button type="button" class="btn btn-default xcrud-action" data-limit="100">100</button><button type="button" class="btn btn-default xcrud-action" data-limit="all">ทั้งหมด</button></div>-->
              </div>
        </div>
        <div class="xcrud-overlay" style="display: none;"></div>
    </div>
</div>
<script type="text/javascript">
function delete_id(id)
{
     if(confirm('ต้องการลบใช่หรือไม่ ?'))
     {
        window.location.href='<?php echo base_url(); ?>absence/delete?id='+id;
     }
}
</script>

    </div>
        </section>
    </div>
</div>
