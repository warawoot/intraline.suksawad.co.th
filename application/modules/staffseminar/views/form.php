<style>
    .error_lbl{
        border-color:red !important;
    }
        .ui-dialog-titlebar-close{
            display:none
        }
        .header-product{
            background-color: pink;
            font-weight: bold;
        }
        .header-product th{
            text-align: center;
            vertical-align: top;
        }
        .btn-none{
            display:none;
        }
</style>

<div class="col-md-12" ng-controller="seminarCtrl" data-ng-init="init()" >

     <div class="col-separator box col-separator-first col-unscrollable">
        <div class="col-table">
                   <div class="row">
                      <div class="col-sm-12">
                          <section class="panel">
                              <header class="panel-heading">
                                  <!--<h3><?php //echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php //echo $staffName; ?></small></h3>

                                   <a href="<?php //echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php //echo $this->config->item('txt_back')?></a>
                                    -->
                              </header>
                              <div class="panel-body">
                              <form class="form-horizontal " id="staffseminarForm" name="staffseminarForm" method="post" action="<?php echo site_url(); ?>staffseminar/submit" >
                                <input type="hidden" name="staffID" value="<?php echo $staffid; ?>">
                                <input type="hidden" name="ID" value="<?php echo !empty($id) ? $id : ""; ?>">
                                <div class="xcrud">
                                  <div class="xcrud-container">
                                      <div class="xcrud-view">
                                        <div class="form-horizontal">
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">หัวข้ออบรม / สัมมนา*</label>
                                            <div class="col-sm-9">
                                            <?php /*if($this->uri->segment('2') != 'view'){ 
                                              echo getDropdown(listData('tbl_seminar',"seminarID","seminarTitle"),'seminarID', '', 'class="form-control"');
                                             }else{
                                              //echo $r->contractNumber;
                                              }*/ ?>

                                              <?php if($this->uri->segment('2') != 'view'){ ?>                                           
                                                  <select 
                                                    ng-init="ddlseminar = {id: ddl.type}"
                                                    ng-model="ddlseminar"
                                                    ng-change="ddl.type = ddlseminar.seminarID; change() "
                                                    ng-options="value.seminarTitle for value in myOptions track by value.seminarID"
                                                    class="form-control"
                                                    name="seminarID" id="seminarID">
                                                    
                                                  </select>
                                                 <?php }else{
                                                  echo $r->seminarTitle;
                                                } ?> 

                                            </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-sm-3">วันที่อบรม</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              {{seminardate}}
                                              <?php }else{
                                                    list($syear,$smonth,$sday) = explode("-", $r->seminarStartDate);
                                                    list($eyear,$emonth,$eday) = explode("-", $r->seminarEndDate);
                                                    echo $sday.'/'.$smonth.'/'.($syear+543).' - '.$eday.'/'.$emonth.'/'.($eyear+543);
                                              } ?>                                               
                                            </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-sm-3">ประเภท</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              {{seminartype}}
                                              <?php }else{
                                                    switch($r->seminarType){
                                                      case 'I' : echo 'อบรมภายใน'; break;
                                                      case 'O' : echo 'อบรมภายนอก'; break;
                                                      case 'L' : echo 'ผู้สอน / บรรยาย'; break;
                                                    }
                                              } ?>                                               
                                            </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-sm-3"></label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                  $chk = '';
                                                  if(!empty($r) && $r->isLecturer == '1')
                                                    $chk = 'checked';
                                                ?>
                                              <input type="checkbox" name="isLecturer" id="isLecturer" value="1" <?php echo $chk; ?>> วิทยากร / ผู้บรรยาย
                                              <?php }else{
                                                    echo ($r->isLecturer == '1') ? '<input type="checkbox" name="isLecturer" id="isLecturer" value="1" disabled checked> วิทยากร / ผู้บรรยาย' : '<input type="checkbox" name="isLecturer" id="isLecturer" value="1" disabled> วิทยากร / ผู้บรรยาย';
                                              } ?>                                               
                                            </div>
                                          </div>
                                          <div class="xcrud-top-actions btn-group">
                                            <?php if($this->uri->segment('2') != 'view'){ ?>
                                            <a class="btn btn-primary"  href="javascript:submitForm();">บันทึกและย้อนกลับ</a>
                                            <?php } ?>
                                            <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>staffseminar/index?token=<?php echo $staffid; ?>'">ย้อนกลับ</a>
                                        </div>
                                        </form>
                              </div>
                          </section>
                      </div>
                  </div>
            
             
                
        </div>  
      </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css">
<script src="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url(); ?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script>
<script>

  function submitForm(){
    $('#staffseminarForm').submit();
  }
  
</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        myApp.controller('seminarCtrl', ['$scope', '$location','$http', function($scope, $location,$http) {

         
          $scope.init = function () {
              

              if("<?php echo $this->uri->segment('2') ?>" != "view"){

                  $http.get("<?php echo site_url()?>staffseminar/getJsonData").success(function (data, status, header, config) {
                    $scope.myOptions = data;
                    $scope.ddlseminar.seminarID = '<?php echo (!empty($r)) ? $r->seminarID : ""?>';

                   $http.get("<?php echo site_url()?>staffseminar/getSeminarDate", { params: { "id": $scope.ddlseminar.seminarID } }).success(function (data, status, header, config) {
                        $scope.seminardate = data;
                       
                    }).error(function (data, status, headers, config) {
                        
                    });

                    $http.get("<?php echo site_url()?>staffseminar/getSeminarType", { params: { "id": $scope.ddlseminar.seminarID} }).success(function (data, status, header, config) {
                        $scope.seminartype = data;
                       
                    }).error(function (data, status, headers, config) {
                        
                    });
                   
                }).error(function (data, status, headers, config) {
                    
                });

              }
          };

          
           
          $scope.change = function(){
              /*var fruitName = $.grep($scope.Fruits, function (fruit) {
                      return fruit.Id == fruitId;
                  })[0].Name;*/

              $http.get("<?php echo site_url()?>staffseminar/getSeminarDate", { params: { "id": $scope.ddlseminar.seminarID } }).success(function (data, status, header, config) {
                  $scope.seminardate = data;
                 
              }).error(function (data, status, headers, config) {
                  
              });

              $http.get("<?php echo site_url()?>staffseminar/getSeminarType", { params: { "id": $scope.ddlseminar.seminarID} }).success(function (data, status, header, config) {
                  $scope.seminartype = data;
                 
              }).error(function (data, status, headers, config) {
                  
              });

          }

        }]);

</script>
