<?php

class staffseminar extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		$this->setModel('staff_seminar_model');
	}
	
	public function index(){

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		
		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));


		$xcrud->table('tbl_staff_seminar')->where('staffID =',$id);
		$xcrud->relation('seminarID','tbl_seminar','seminarID','seminarTitle');

		
		//// List /////
		$col_name = array(
			/*'seminarStartDate'=>'วันที่จัดอบรม',
			'seminarEndDate' => 'วันที่สิ้นสุด',
			'seminarID' =>'หัวข้อ/โครงการ',
			'seminarPlace'=>'สถานที่',
			'seminarPerson'=>'วิทยากร',
			'seminarNum'=>'จำนวนผู้เข้าร่วม',
			'seminorDetail'=>'รายละเอียด',
			'seminarType'=>'ประเภท'*/
			'seminarID' =>'หัวข้ออบรม/สัมมนา',
			'isLecturer' => 'วันที่จัดอบรม'
			

		);

		//$xcrud->pass_var('staffID',$id);
		
		$xcrud->button(site_url().'staffseminar/view/index?token={ID}','ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
		$xcrud->button(site_url().'staffseminar/edit/index?token={ID}','แก้ไข','glyphicon glyphicon-edit','btn-warning'); 
		
		$xcrud->columns('isLecturer,seminarID');
		$xcrud->label($col_name);

		$xcrud->column_callback('isLecturer','show_seminar_date');
		/*$xcrud->column_pattern('seminarStartDate',' {value} {seminarEndDate}');
		$xcrud->change_type("seminarType",'radio','',array('I'=>'อบรมภายใน','O'=>'อบรมภายนอก','L'=>'ผู้สอน / บรรยาย'));
		$xcrud->column_width("seminarTitle","50%")->column_width("seminarType","20%");
		
		$xcrud->column_callback('seminarStartDate','change_column');

		//// Form //////

		$xcrud->fields('seminarStartDate,seminarTitle,seminarPlace,seminarPerson,seminarNum,seminorDetail,seminarType');*/

		$xcrud->unset_add();
		$xcrud->unset_edit();
		$xcrud->unset_view();
		//$xcrud->unset_remove();
		
		$data['id'] = $id;
		$data['html'] = $xcrud->render();
		$data['title'] = "อบรม/ส้มมนา";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

	public function add(){
		// get id staff //
		$staffid = mysql_real_escape_string($this->input->get('token'));
		
		$data['title'] = "อบรม/ส้มมนา";
		$data['staffName'] = getStaffName($staffid);
		$data['staffid'] = $staffid;
        $this->template->load("template/tab",'form', $data);
	}

	public function getJsonData(){
	
    	$data = $this->db->get('tbl_seminar')->result();

		echo json_encode($data);
	}

	public function getSeminarDate(){

		// get id seminar //
		$id = mysql_real_escape_string($this->input->get('id'));

		$r = $this->db->get_where('tbl_seminar',array('seminarID'=>$id))->row();

		if(!empty($r)){

			list($syear,$smonth,$sday) = explode("-", $r->seminarStartDate);
		    list($eyear,$emonth,$eday) = explode("-", $r->seminarEndDate);
		    echo $sday.'/'.$smonth.'/'.($syear+543).' - '.$eday.'/'.$emonth.'/'.($eyear+543);
	    }

	}

	public function getSeminarType(){

		// get id seminar //
		$id = mysql_real_escape_string($this->input->get('id'));

		$r = $this->db->get_where('tbl_seminar',array('seminarID'=>$id))->row();

		if(!empty($r)){
			switch($r->seminarType){
				case 'I' : echo 'อบรมภายใน'; break;
				case 'O' : echo 'อบรมภายนอก'; break;
				case 'L' : echo 'ผู้สอน / บรรยาย'; break;
			}
		}else
			echo "";

	}


	public function submit(){

		if(!empty($_POST['staffID'])){

			$staffID				= 	mysql_real_escape_string(trim($this->input->post('staffID',true)));
			$ID						= 	mysql_real_escape_string(trim($this->input->post('ID',true)));
			$seminarID				= 	mysql_real_escape_string(trim($this->input->post('seminarID',true)));
			$isLecturer				= 	!empty($_POST['isLecturer']) ? mysql_real_escape_string(trim($this->input->post('isLecturer',true))) : "0";

			
			$arr = array(
				'staffID'			=> $staffID,
				'seminarID'			=> $seminarID,
				'isLecturer'		=> $isLecturer
			);	

			if($this->save($arr,$ID)){
				redirect('staffseminar/index?token='.$staffID);
			}
			/*$model = getModel();
			if(!empty($contractID)){
				// Update //
				if(!empty($this->$model->save($arr,$contractID))){
					redirect('staffcontract/index?token='.$staffID);
				}
			}else{
				// Insert //
				if(!empty($this->$model->save($arr))){
					redirect('staffcontract/index?token='.$staffID);
				}

			}*/
		}
	}

	public function view(){
		if(!empty($_GET['token'])){

			$ID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$data['r'] = $this->db->from('tbl_staff_seminar ss')
								->join('tbl_seminar s','s.seminarID = ss.seminarID')
								->where('ss.ID',$ID)
								->get()
								->row();
			
			$staffid = (!empty($data['r'])) ? $data['r']->staffID : "";

			$data['title'] = "อบรม/ส้มมนา";
			$data['staffName'] = getStaffName($staffid);
			$data['id'] = $ID;
			$data['staffid'] = $staffid;
	        $this->template->load("template/tab",'form', $data);
		}	
	}

	public function edit(){
		if(!empty($_GET['token'])){

			$ID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$data['r'] = $this->staff_seminar_model->get_by(array('ID'=>$ID),true);
			
			$staffid = (!empty($data['r'])) ? $data['r']->staffID : "";

			$data['title'] = "อบรม/ส้มมนา";
			$data['staffName'] = getStaffName($staffid);
			$data['id'] = $ID;
			$data['staffid'] = $staffid;
	        $this->template->load("template/tab",'form', $data);
		}	
	}

}
/* End of file staffseminar.php */
/* Location: ./application/module/staffseminar/staffseminar.php */