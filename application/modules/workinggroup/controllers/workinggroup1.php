<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Workinggroup extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('workinggroup_model');
		$this->load->model('employment/employment_model');
      }
 

    public function index() {
		
		$xcrud	=	xcrud_get_instance(); 
        $xcrud->table('tbl_project_member'); 
        $arr  =	array("position_id"=>"ตำแหน่ง","person_id"=>"ผู้รับผิดชอบ" );
 		//t1.project_id,t2.`name`,t3.staffPreName , t3.staffFName ,t3.staffLName 
		$xcrud->relation('position_id','tbl_project_position','id','name'); 
		$xcrud->relation('person_id','tbl_staff','ID',array('staffPreName','staffFName','staffLName')); 
		 
 		//$xcrud->change_type('emp_level_desc','textarea' );
		//$xcrud->change_type('emp_level_knowledge','textarea' );
		// -- validation_required -- 
		//$xcrud->validation_required('emp_level_name') ;
 		//$xcrud->pass_var('emp_level_id',$max_id,'create');
 		//$xcrud->column_pattern('tbl_staff.staffFName','{tbl_staff.staffFName} {value} {tbl_staff.staffLName}');
        $xcrud->label($arr);
        $xcrud->columns($arr);//แสดงในตางราง $xcrud->columns('userName');  
   		 
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		$xcrud->unset_add();
		$xcrud->unset_edit();
 		 
        $data['data']	=	$xcrud->render();
		
		$token	= $this->input->get('token');
  		$data['get_employment'] = $this->employment_model->get_date_edit($token); 	
		
		//get_dropdown_all
		$data['dropdown_position']	= $this->workinggroup_model->get_dropdown_all('id' , 'name' ,'tbl_project_position');#Dropdown 
		$data['dropdown_staff']	= $this->workinggroup_model->get_dropdown_staff('ID' , 'staffFName' ,'staffLName' ,'tbl_staff');#Dropdown 
 		$data['token']	= $token;
		$data['actions'] =  'add';
        $this->template->load('template/admin', 'workinggroup',$data);
 		 
		
	} 
	
	public function add(){
 		
		$data['date_start']	= '';
		$data['date_end']	= '';
		
 		$data['token']	 	= '';
		$data['data']	 	= '';
		$evalYear			= $this->input->get('token');
		$evalRound			= $this->input->get('token1');
		$data['actions'] 	= 'add';
  		
		$data['token']		= $evalYear; 
		$data['token1']		= $evalRound; 
		$data['actions']	= 'add';
		 
  		//$data['get_data'] = $this->project_model->get_date_edit($evalYear,$evalRound); 
        $this->template->load('template/admin', 'employment_form',$data);
		 
	}
	
	public function check_add() {
		
		$project_id			= $this->input->get_post("project_id");
		$position_id		= $this->input->get_post("position_id");
		$org_position_id	= $this->input->get_post("org_position_id");
		$person_id			= $this->input->get_post("person_id"); 
		$external_person	= $this->input->get_post('external_person');
  		$action				= $this->input->get_post('action');
  		$data	=	array( 
  				"project_id"=>$project_id,
				"position_id"=>$position_id ,
 				"org_position_id"=>$org_position_id,
				"person_id"=>$person_id  ,
				"external_person"=>$external_person
 		);
		
		/*$data2	=	array( 
		
  				"startDate"=>$date_start,
				"endDate"=>$date_end  
		
		);*/
		
  
		if($action == 'edit'){
			//$insert_data = $this->evaluation_model->update_data($evalYear,$evalRound,$data2); 
 		}elseif($action == 'add'){
			$insert_data = $this->workinggroup_model->insert_data($data); 	
 		}
  		 
 		 
		if($insert_data == 'succesfully'){
				echo  'succesfully';
		}else{
				echo 'failed';
		}									
         
	}  
   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>