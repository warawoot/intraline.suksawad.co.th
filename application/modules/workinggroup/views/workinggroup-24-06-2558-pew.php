<style type="text/css">
.bs-example:after { 
  content: "งานโครงการ";
}
.modal {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1050;
  display: none;
  overflow: hidden;
  -webkit-overflow-scrolling: touch;
  outline: 0;
}
.modal-backdrop {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1030;
  background-color: #333333;
  opacity:0.8;  
}
#myModal {
     
  top: 10%;
    
}
</style>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>คณะทำงานโครงการ</h3>
            </header>
            <div class="panel-body">
                  <div class="bs-example bs-example-type" data-example-id="simple-headings" >
                        <table class="table">
                          <tbody>
                             
                            <tr>
                              <td> เลขที่คำสั่ง </td>
                              <td class="type-info"><?php echo $get_employment['id'];?></td>
                            </tr>
                            <tr>
                              <td>ชื่อโครงการ</td>
                              <td class="type-info"><?php echo $get_employment['name'];?></td>
                            </tr>
                            <tr>
                              <td>วันที่เริ่มต้น</td>
                              <td class="type-info"><?php echo $get_employment['startDate'];?></td>
                            </tr>
                            <tr>
                              <td>วันที่สิ้นสุด</td>
                              <td class="type-info"><?php echo $get_employment['endDate'];?></td>
                            </tr>
                             
                          </tbody>
                        </table>
                      </div>
       	  </div>
            <div class="panel-body">
            	<!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">เพิ่มคณะทำงานโครงการ</button>
                
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">คณะทำงานโครงการ</h4>
                      </div>
                      <div class="modal-body">
                         <table class="table">
                          <tbody>
                             
                            <tr>
                             <td>&nbsp;</td>
                              <td> ตำแหน่งในโครงการ </td>
                              <td class="type-info">
							  		<?php
 										echo form_dropdown('dropdown_position',$dropdown_position ,'' ,'id ="position" class="xcrud-input form-control form-control" data-required="1" data-unique="" data-type="select"  ');
									 
									?>
                              </td>
                            </tr>
                            <tr>
                              <td> </td>
                              <td>กำหนดโดยตำแหน่ง</td>
                              <td class="type-info"> coming soon ... </td>
                            </tr>
                            <tr>
                              <td> </td>
                              <td>กำหนดโดยชื่อ</td>
                              <td class="type-info">
							  	<?php
 										echo form_dropdown('dropdown_staff',$dropdown_staff ,'' ,'id ="staff" class="xcrud-input form-control form-control" data-required="1" data-unique="" data-type="select"  ');
									 
								?>
                              </td>
                            </tr>
                            <tr>
                              <td> </td>
                              <td>บุคคลภายนอก</td>
                              <td class="type-info"> <input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="int" value="" name="dGJsX3Byb2plY3QuaWQ-" id="dGJsX3Byb2plY3QuaWQ-" data-pattern="integer" maxlength="11"></td>
                            </tr>
                             
                          </tbody>
                        </table>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="modal_ck();">Save changes</button>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <!--<header class="panel-heading"> 
                <h3>ข้อมูลชื่อระดับ</h3>
            </header>-->
            <div class="panel-body">
                <?php echo $data; ?>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
 	 
	/*$(".xcrud-top-actions").find('.btn-success').attr("onClick","save_Job();");
	$(".btn-group").find('.btn-warning').attr("onClick","edit_Job();");
	
	function save_Job(){
 		 location.replace('<?php echo base_url('employment/add'); ?>');
	}
	function edit_Job(){
		location.href = '<?php echo base_url('employment'); ?>';
	}
	 
    function modal_back(){
		location.replace('<?php echo base_url().'employment'; ?>');
 	}*/
 
 function modal_ck(){
	 
		var position	= $("#position").val();
		var staff		= $("#staff").val();
		var outsider	= $("#dGJsX3Byb2plY3QuaWQ-").val();
 		var by_location	= $("by_location").val();
		
		if(position == ''){
			  alert('กรุณาระบุ ตำแหน่งในโครงการ ด้วยค่ะ');
 		}else if(staff == ''){
			  alert('กรุณาระบุ กำหนดโดยชื่อ ด้วยค่ะ');
 		/*}else if(outsider ==''){
			  alert('กรุณาระบุ บุคคลภายนอก ด้วยค่ะ');*/
  		}else{
			 
			  $.ajax({
					url: "<?php echo base_url('workinggroup/check_add?action='.$actions) ?>",
					type: 'POST',
					data: {
							project_id: <?php echo $token; ?>,
							position_id : position,
							org_position_id: by_location,
							person_id: staff,
							external_person: outsider 
					}, 
					success: function(response) {
						//Do Something 
							if(response == 'succesfully'){
								location.replace('<?php echo base_url().'workinggroup/?token='.$token ?>');
							}else{
								//var obj = jQuery.parseJSON(response); 
								alert('กรุณาลองใหม่อีกครั้ง');
								//location.replace('<?php echo base_url().'employment/add' ?>');
							}
					},
					error: function(xhr) {
						//Do Something to handle error
						alert('กรุณาลองใหม่อีกครั้ง');
						//location.replace('<?php echo base_url().'workinggroup/?token='.$token ?>');
					}
					
			 }); //end $.ajax
			  
		} //end if	
	 }	
</script> 

