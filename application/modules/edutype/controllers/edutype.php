<?php

class Edutype extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		
		// select table//
		$xcrud->table('tbl_edu_type');
		
		
		//// List /////
		$col_name = array(
			'eduTypeName' => 'ประเภทสถานศึกษา'

		);

		$xcrud->columns('eduTypeName');
		$xcrud->label($col_name);
		// End List//

		//// Form //////
		$xcrud->fields('eduTypeName');

		$data['html'] = $xcrud->render();
		

		$data['title'] = "ประเภทสถานศึกษา";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file edutype.php */
/* Location: ./application/module/edutype/eduplace.php */