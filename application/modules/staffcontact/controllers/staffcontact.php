<?php

class staffcontact extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		Xcrud_config::$editor_url = base_url().'editors/ckeditor/ckeditor.js';

		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();
		
		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));


		$xcrud->table('tbl_staff_contact')->where('staffID =',$id);
		

		
		//// List /////
		$col_name = array(
			'contactFName' => 'ชื่อ',
			'contactLName' => 'สกุล',
			'contactRelation' =>'เกี่ยวข้องเป็น',
			'contactTel'=>'เบอร์โทรศัพท์',
			'contactAddress'=>'ที่อยู่',
			'contactWork'=>'สถานที่ทำงาน',
			'contactTelWork'=>'เบอร์โทรที่ทำงาน',
			'contactRelationEtc'=>'อื่นๆ'
			

		);

		$xcrud->pass_var('staffID',$id);
		
		
		$xcrud->columns('contactFName,contactLName,contactRelation,contactTel');
		$xcrud->label($col_name);
		//$xcrud->column_pattern('contactFName',' {value} {contactLName}');
		
		

		//// Form //////

		$xcrud->fields('contactFName,contactLName,contactRelation,contactRelationEtc,contactTel,contactAddress,contactWork,contactTelWork');
		$xcrud->change_type('contactRelation', 'select', '', array('บิดา'=>'บิดา','มารดา'=>'มารดา','ญาติ'=>'ญาติ','คู่สมรส'=>'คู่สมรส','อื่นๆ'=>'อื่นๆ'));
		
		
		
		$xcrud->validation_required('contactFName,contactLName,contactRelation,contactTel');
		$xcrud->validation_pattern('contactTel', 'numeric');
		$xcrud->validation_pattern('contactTelWork', 'numeric');	
		
		
		$data['html'] = $xcrud->render();
		$data['title'] = "รายชื่อผู้ติดต่อฉุกเฉิน/ผู้ค้ำประกัน";
		$data['staffName'] = getStaffName($id);
        $this->template->load("template/tab",'main', $data);

	}

}
/* End of file staffname.php */
/* Location: ./application/module/staffname/staffname.php */