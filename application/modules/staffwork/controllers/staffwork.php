<?php

class Staffwork extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		$this->setModel("staff_work_model");

		$this->load->model('orgchart_assigndate_model');
		$this->load->model('staff_new_model');
		
	}
	
	public function index(){

		Xcrud_config::$editor_url = base_url().'editors/ckeditor/ckeditor.js';


		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));

		/*$data['rs'] = $this->db->from('tbl_staff s')
							//->join('tbl_work_status w','s.workStatusID = w.workStatusID','left')
							->where('ID',$id)
							->get();*/
		$data['rs'] = $this->staff_new_model->getStaffNew($id);			
		
		// select table//
		$xcrud->table('tbl_staff_work')->where('staffID',$id);

		$xcrud->relation('positionID','tbl_position','positionID','positionName');
		$xcrud->relation('rankID','tbl_rank','rankID','rankName');
		$xcrud->relation('orgID','tbl_org_chart','orgID','orgName');
		/*$xcrud->relation('orgID1','tbl_org_chart','orgID','orgName','tbl_org_chart.upperOrgID = \'0\' OR tbl_org_chart.upperOrgID = \'1\'','orgID');
		$xcrud->relation('orgID2','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\' AND upperOrgID != \'1\'','orgID','','','','upperOrgID','orgID1');
		$xcrud->relation('orgID','tbl_org_chart','orgID','orgName','orgID != upperOrgID AND upperOrgID != \'0\' AND upperOrgID != \'1\'','orgID','','','','upperOrgID','orgID2');
		*/
		$xcrud->relation('workStatusID','tbl_work_status','workStatusID','workStatusName');
		$xcrud->relation('workPlaceID','tbl_work_place','workPlaceID','workPlaceName');
		$xcrud->relation('staffTypeID','tbl_staff_type','typeID','typeName');


		//// List /////
		$col_name = array(
			'workNumber' => 'เลขที่คำสั่ง',
			'workName' => 'เรื่อง',
			'workDetail' => 'รายละเอียด',
			'workStartDate' => 'วันที่มีผล',
			'workDate' => 'วันที่คำสั่ง',
			'rankID' => 'ระดับ',
			'positionID' => 'ตำแหน่ง',
			//'orgID1'	=> 'ฝ่าย / สำนักงาน',
			//'orgID2'	=> 'กอง / กลุ่มงาน',
			'orgID'	=> 'แผนก',
			'workPlaceID' => 'สถานที่ปฏิบัติงาน',
			'staffTypeID' => 'ประเภทพนักงาน',
			'workStatusID' => 'สถานะภาพการทำงาน',
			'isHeader'	=> 'ฝ่าย / สำนักงาน',
			'isConsult' => 'กอง / กลุ่มงาน'
			//'isHeader'	=> 'หัวหน้า',
			//'isConsult' => 'ที่ปรึกษา'

		);

		$xcrud->button(site_url().'staffwork/view/index?token={workID}','ดูข้อมูล','glyphicon glyphicon-search','btn-info'); 
		$xcrud->button(site_url().'staffwork/edit/index?token={workID}','แก้ไข','glyphicon glyphicon-edit','btn-warning'); 

		$xcrud->columns('workNumber,workStartDate,rankID,positionID,isHeader,isConsult,orgID');
		$xcrud->column_pattern('workStartDate','{value} {staffID}');
		$xcrud->column_callback('workStartDate','calDateWork');
		$xcrud->column_callback('isHeader','getOrgID1');
		$xcrud->column_callback('isConsult','getOrgID2');
		$xcrud->column_callback('orgID','getOrgID');
		/*$xcrud->column_callback('orgID1Old','getOrgID1');
		$xcrud->column_callback('orgID2Old','getOrgID2');
		$xcrud->column_callback('orgIDOld','getOrgID');
		$xcrud->column_callback('moveDate','toBDDate');*/
		$xcrud->label($col_name);
		
		// End List//

		//// Form //////
		$xcrud->pass_var('staffID',$id);/*->pass_default(array('orgID1Old'=>getStaffByID($id,'orgID1'),'orgID2Old'=>getStaffByID($id,'orgID2'),'orgIDOld'=>getStaffByID($id,'orgID')));*/

		$xcrud->fields('workNumber,workName,workDetail,workStartDate,workDate,rankID,positionID,orgID1,orgID2,orgID,workPlaceID,staffTypeID,workStatusID,isHeader,isConsult');
		$xcrud->validation_required('workName,workStartDate,ownerID,rankID,positionID,orgID1,workPlaceID,workStatusID');
		$xcrud->change_type('isHeader','checkboxes','',array('1'=>''))->change_type('isConsult','checkboxes','',array('1'=>''));

		//xcrud->disabled('orgID1Old,orgID2Old,orgIDOld');
		//$xcrud->change_type('orgID1Old','text')->change_type('orgID2Old','text')->change_type('orgIDOld','text');
		// End Form//
		
		$xcrud->unset_view()->unset_add()->unset_edit();

		$data['html'] = $xcrud->render();
		$data['title'] = "ประวัติการทำงาน";
		$data['staffName'] = getStaffName($id);
		$data['id'] = $id;
        $this->template->load("template/tab",'main', $data);

	}

	public function add(){

		// get id staff //
		$id = mysql_real_escape_string($this->input->get('token'));
		
		$data['assignID'] = $this->orgchart_assigndate_model->getLastedDate();

		$data['title'] = "ประวัติการทำงาน";
		$data['staffName'] = getStaffName($id);
		$data['id'] = $id;
        $this->template->load("template/tab",'form', $data);
	}

	public function getDropdownTree(){
		if(!empty($_POST['date'])){
			$date = mysql_real_escape_string($this->input->post('date'));
			$list = explode("/",$date);
			$date = $list[2].'-'.$list[1].'-'.$list[0];
			$assignID = $this->orgchart_assigndate_model->getLastedDate($date);

			echo getDropdownTree('0','',$assignID,'');
		}
	}

	public function submit(){

		if(!empty($_POST['staffID'])){

			$staffID				= 	mysql_real_escape_string(trim($this->input->post('staffID',true)));
			$workID					= 	mysql_real_escape_string(trim($this->input->post('workID',true)));
			$workNumber				= 	mysql_real_escape_string(trim($this->input->post('workNumber',true)));
			$workName				= 	mysql_real_escape_string(trim($this->input->post('workName',true)));
			$workDetail				= 	mysql_real_escape_string(trim($this->input->post('workDetail',true)));
			$workStartDate 			= 	mysql_real_escape_string(trim($this->input->post('workStartDate',true)));
			$workDate				= 	mysql_real_escape_string(trim($this->input->post('workDate',true)));
			$rankID					= 	mysql_real_escape_string(trim($this->input->post('rankID',true)));
			$positionID				= 	mysql_real_escape_string(trim($this->input->post('positionID',true)));
			$orgID					= 	mysql_real_escape_string(trim($this->input->post('orgID',true)));
			$workPlaceID 			= 	mysql_real_escape_string(trim($this->input->post('workPlaceID',true)));
			$staffTypeID			= 	mysql_real_escape_string(trim($this->input->post('staffTypeID',true)));
			$workStatusID			= 	mysql_real_escape_string(trim($this->input->post('workStatusID',true)));
			$segID					= 	mysql_real_escape_string(trim($this->input->post('segID',true)));
			//$isHeader				= 	!empty($_POST['isHeader']) ? "1" : "0";
			//$isConsult				= 	!empty($_POST['isConsult']) ? "1" : "0";
			//$isDeputy				= 	!empty($_POST['isDeputy']) ? "1" : "0";

			if(!empty($segID)){
				$r = $this->staff_work_model->get_by(array('rankID'=>$rankID,'positionID'=>$positionID,'orgID'=>$orgID,'segID IS NOT NULL'=>NULL));
				
				if(!empty($r)){
					foreach($r as $row){
						$arr['segID'] = NULL;
						$this->save($arr,$row->workID);

					}
				}
			}
			
			$arr = array(
				'staffID'			=> $staffID,
				'workNumber'		=> $workNumber,
				'workName'			=> $workName,
				'workDetail'		=> $workDetail,
				'workStartDate'		=> toCEDate($workStartDate),
				'workDate'			=> toCEDate($workDate),
				'rankID'			=> $rankID,
				'positionID'		=> $positionID,
				'orgID'				=> $orgID,
				'workPlaceID'		=> $workPlaceID,
				'staffTypeID'		=> $staffTypeID,
				'workStatusID'		=> $workStatusID,
				'segID'				=> $segID
				//'isHeader'			=> $isHeader,
				//'isConsult'			=> $isConsult,
				//'isDeputy'			=> $isDeputy
			);	

			if($this->save($arr,$workID)){
				$getMaxstaffwork = $this->staff_new_model->getStaff($staffID);
				$arrWork = array(
				'rankID'			=> $getMaxstaffwork[0]->rankID,
				'positionID'		=> $getMaxstaffwork[0]->positionID,
				'orgID'				=> $getMaxstaffwork[0]->orgID,
				'status'		=> $getMaxstaffwork[0]->workStatusID,
					);	
			$saveDateStaff = $this->staff_new_model->save($arrWork,$staffID);
				redirect('staffwork/index?token='.$staffID);
			}
			
			
		}
	}

	public function view(){
		if(!empty($_GET['token'])){

			$workID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$data['r'] = $this->staff_work_model->get_view($workID);
			
			$id = (!empty($data['r'])) ? $data['r']->staffID : "";
			$workStartDate = (!empty($data['r'])) ? $data['r']->workStartDate : "";

			$data['assignID'] = $this->orgchart_assigndate_model->getLastedDate($workStartDate);

			$data['title'] = "ประวัติการทำงาน";
			$data['staffName'] = getStaffName($id);
			$data['id'] = $id;
			$data['workID'] = $workID;
	        $this->template->load("template/tab",'form', $data);
		}	
	}

	public function edit(){
		if(!empty($_GET['token'])){

			$workID = mysql_real_escape_string(trim($this->input->get('token',true)));

			$data['r'] = $this->staff_work_model->get_by(array('workID'=>$workID),true);
			
			$id = (!empty($data['r'])) ? $data['r']->staffID : "";
			$workStartDate = (!empty($data['r'])) ? $data['r']->workStartDate : "";

			$data['assignID'] = $this->orgchart_assigndate_model->getLastedDate($workStartDate);

			$data['title'] = "ประวัติการทำงาน";
			$data['staffName'] = getStaffName($id);
			$data['id'] = $id;
			$data['workID'] = $workID;
	        $this->template->load("template/tab",'form', $data);
		}	
	}

	public function getStaffDetail(){
		if(!empty($_GET['id'])){

			$id = $this->input->get('id');

			$this->load->model('staff_model');

			$r = $this->staff_model->get_by(array('ID'=>$id),true);
			echo (!empty($r)) ? json_encode(array('staffName'=>$r->staffFName.' '.$r->staffLName,'staffType'=>getStaffTypeByWork($id),'staffPosition'=>getPositionByWork($id))) : '';

			
			

		}		
	}



	/*public function updateDetailStaff(){
		$orgID = $this->input->post('orgID');
		$work_status = $this->input->post('work_status');
		$dateIn = $this->input->post('dateIn');
		$dateOut = $this->input->post('dateOut');
		$staffID = $this->input->post('staffID');

		if(!empty($orgID) && !empty($work_status) && !empty($dateIn) && !empty($staffID)){
			$data = array(
				'status' => $work_status,
				'orgID' => $orgID,
				'dateIn' => toCEDate($dateIn)
			);

			if(!empty($dateOut)){
				$data['dateOut'] = toCEDate($dateOut);
			}

			echo "<pre>";
			print_r($data);

			//$result = $this->staff_new_model->updateDetailStaff($data,$staffID);
		}


	}*/

}
/* End of file staffwork.php */
/* Location: ./application/module/staffwork/staffwork.php */