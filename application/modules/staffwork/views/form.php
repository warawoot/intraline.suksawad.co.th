<style>
    .error_lbl{
        border-color:red !important;
    }
        .ui-dialog-titlebar-close{
            display:none
        }
        .header-product{
            background-color: pink;
            font-weight: bold;
        }
        .header-product th{
            text-align: center;
            vertical-align: top;
        }
        .btn-none{
            display:none;
        }
        .error_lbl{
          border-color:red;
        }
</style>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
<div class="col-md-12" >

     <div class="col-separator box col-separator-first col-unscrollable">
        <div class="col-table">
                   <div class="row">
                      <div class="col-sm-12">
                          <section class="panel">
                              <header class="panel-heading">
                                  <!--<h3><?php //echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php //echo $staffName; ?></small></h3>

                                   <a href="<?php //echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php //echo $this->config->item('txt_back')?></a>
                                    -->
                              </header>
                              <div class="panel-body">
                              <form class="form-horizontal " id="staffworkForm" name="staffworkForm" method="post" action="<?php echo site_url(); ?>staffwork/submit" >
                                <input type="hidden" name="staffID" value="<?php echo $id; ?>">
                                <input type="hidden" name="workID" value="<?php echo !empty($workID) ? $workID : ""; ?>">
                                <div class="xcrud">
                                  <div class="xcrud-container">
                                      <div class="xcrud-view">
                                        <div class="form-horizontal">
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">เลขที่คำสั่ง</label>
                                            <div class="col-sm-9">
                                            <?php if($this->uri->segment('2') != 'view'){ ?>
                                            <input type="text" maxlength="50" name="workNumber" value="<?php echo !empty($r) ? $r->workNumber : ""; ?>" data-type="text" data-required="1" class="xcrud-input form-control">
                                            <?php }else{
                                              echo $r->workNumber;
                                              } ?>
                                            </div>
                                          </div>
                                           <div class="form-group">
                                            <label class="control-label col-sm-3">เรื่อง*</label>
                                            <div class="col-sm-9">
                                            <?php if($this->uri->segment('2') != 'view'){ ?>
                                            <input type="text" maxlength="255" name="workName" id="workName"value="<?php echo !empty($r) ? $r->workName : ""; ?>" data-type="text" data-required="1" class="xcrud-input form-control">
                                            <?php }else{
                                              echo $r->workName;
                                              } ?>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">รายละเอียด</label>
                                            <div class="col-sm-9">
                                            <?php if($this->uri->segment('2') != 'view'){ ?>
                                            <textarea class="xcrud-input form-control" name="workDetail"><?php echo !empty($r) ? $r->workDetail : ""; ?></textarea>
                                            <?php }else{
                                              echo $r->workDetail;
                                              } ?>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">วันที่มีผล*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="workStartDate" id="workStartDate" value="<?php echo !empty($r) ? toBEDate($r->workStartDate) : ""; ?>" data-type="text" data-required="1" class="xcrud-input xcrud-datepicker form-control" data-type="date">
                                                <?php }else{
                                                echo toBEDateThai($r->workStartDate);
                                              } ?>
                                              </div>
                                          </div>
                                           <div class="form-group">
                                            <label class="control-label col-sm-3">วันที่คำสั่ง*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                              <input type="text" maxlength="50" name="workDate" id="workDate" value="<?php echo !empty($r) ? toBEDate($r->workDate) : ""; ?>" data-type="text" data-required="1" class="xcrud-input xcrud-datepicker form-control" data-type="date">
                                                <?php }else{
                                                echo toBEDateThai($r->workDate);
                                              } ?>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">ระดับ*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                    $rankID = !empty($r) ? $r->rankID : "";
                                                    echo getDropdown(listData("tbl_rank","rankID","rankName"),"rankID", $rankID, 'class="xcrud-input form-control"');
                                               }else{
                                                    echo $r->rankName;
                                              } ?>
                                              </div>
                                          </div>
                                            <div class="form-group">
                                            <label class="control-label col-sm-3">ตำแหน่ง*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                    $positionID = !empty($r) ? $r->positionID : "";
                                                    echo getDropdown(listData("tbl_position","positionID","positionName"),"positionID", $positionID, 'class="xcrud-input form-control"');
                                               }else{
                                                    echo $r->positionName;
                                              } ?>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">หน่วยงานแม่*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ ?>
                                                <select class="xcrud-input form-control form-control" data-type="select" name="orgID" id="orgID"> 
                                          
                                                    <?php 
                                                     $orgID = !empty($r) ? $r->orgID : "";
                                                     echo getDropdownTree('0','',$assignID,$orgID); ?>
                                                </select>
                                               <?php }else{
                                                    echo $r->orgID;
                                              } ?>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">สถานที่ปฏิบัติงาน*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                     $workPlaceID = !empty($r) ? $r->workPlaceID : "";
                                                    echo getDropdown(listData("tbl_work_place","workPlaceID","workPlaceName"),"workPlaceID", $workPlaceID, 'class="xcrud-input form-control"');
                                               }else{
                                                    echo $r->workPlaceName;
                                              } ?>
                                              </div>
                                          </div>
                                           <div class="form-group">
                                            <label class="control-label col-sm-3">ประเภทพนักงาน</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                    $staffTypeID = !empty($r) ? $r->staffTypeID : "";
                                                    echo getDropdown(listData("tbl_staff_type","typeID","typeName"),"staffTypeID", $staffTypeID, 'class="xcrud-input form-control"');
                                               }else{
                                                    echo $r->typeName;
                                              } ?>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">สถานภาพการทำงาน*</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                    $workStatusID = !empty($r) ? $r->workStatusID : "";
                                                    echo getDropdown(listData("tbl_work_status","workStatusID","workStatusName"),"workStatusID", $workStatusID, 'class="xcrud-input form-control"');
                                               }else{
                                                    echo $r->workStatusName;
                                              } ?>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-3">อัตราที่</label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                    $segID = !empty($r) ? $r->segID : "";
                                                    echo getDropdown(listData("tbl_seguence","segID","segID"),"segID", $segID, 'class="xcrud-input form-control"');
                                               }else{
                                                    echo $r->segID;
                                              } ?>
                                              </div>
                                          </div>
                                           <!--<div class="form-group">
                                            <label class="control-label col-sm-3"></label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                  $isHeader = !empty($r) ? $r->isHeader : "";
                                                  $chk = ($isHeader == '1') ? 'checked' : "";
                                                ?>
                                                <input type="checkbox"  value="1" name="isHeader" <?php echo $chk; ?>> &nbsp;&nbsp;&nbsp;หัวหน้า
                                              <?php }else{
                                                    echo  ($r->isHeader == '1') ? '- หัวหน้า' : "";
                                              } ?>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                            <label class="control-label col-sm-3"></label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                  $isConsult = !empty($r) ? $r->isConsult : "";
                                                  $chk = ($isConsult == '1') ? 'checked' : "";
                                                ?>
                                                <input type="checkbox"  value="1" name="isConsult" <?php echo $chk; ?>> &nbsp;&nbsp;&nbsp;ที่ปรึกษา
                                              <?php }else{
                                                    echo ($r->isConsult == '1') ? '- ที่ปรึกษา' : "";
                                              } ?>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                            <label class="control-label col-sm-3"></label>
                                              <div class="col-sm-9">
                                              <?php if($this->uri->segment('2') != 'view'){ 
                                                  $isDeputy = !empty($r) ? $r->isDeputy : "";
                                                  $chk = ($isDeputy == '1') ? 'checked' : "";
                                                
                                                ?>
                                                <input type="checkbox"  value="1" name="isDeputy" <?php echo $chk; ?>> &nbsp;&nbsp;&nbsp;รักษาการแทน
                                              <?php }else{
                                                    echo ($r->isDeputy == '1') ? '- รักษาการแทน' : "";
                                              } ?>
                                              </div>
                                          </div>-->
                                          
                                      </div>
                                  </div>
                                  <div class="xcrud-top-actions btn-group">
                                      <?php if($this->uri->segment('2') != 'view'){ ?>
                                      <a class="btn btn-primary"  href="javascript:submitForm();">บันทึกและย้อนกลับ</a>
                                      <?php } ?>
                                      <a class="btn btn-warning"  href="javascript:window.location='<?php echo site_url();?>staffwork/index?token=<?php echo $id; ?>'">ย้อนกลับ</a>
                                  </div>
                                  </form>
                                 
                              </div>
                          </section>
                      </div>
                  </div>
            
             
                
        </div>  
      </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>xcrud/themes/bootstrap/xcrud.css">
<script src="<?php echo base_url(); ?>xcrud/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>xcrud/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url(); ?>xcrud/languages/datepicker/jquery.ui.datepicker-th.js"></script>
<script>
  $(function(){
      $('.xcrud-datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            changeMonth: true,
            changeYear: true
          });

      
      /*$('#workStartDate').change(functon(){
          $.ajax({
            url: "<?php echo site_url()?>staffwork/getDropdownTree",
            date: $('#workStartDate').val()
          }).done(function(data) {
            $("#orgID").html(data);
          });
      })*/

  })

  function submitForm(){
    if($('#staffworkForm').valid())
      $('#staffworkForm').submit();
  }
  

  $('#workStartDate').change(function(){
     $.post("<?php echo site_url()?>staffwork/getDropdownTree",
    {
        date: $('#workStartDate').val()
    },
    function(data, status){
        $("#orgID").html(data);
    });
    
  })

  $("#staffworkForm").validate({
            errorClass: "error_lbl",
            rules: {
                    workName:"required",
                    workStartDate:"required",
                    rankID:"required",
                    positionID:"required",
                    orgID:"required",
                    workPlaceID:"required",
                    workStatusID:"required",
                    workDate:"required"
            }, messages: {
                    workName:"",
                    workStartDate:"",
                    rankID:"",
                    positionID:"",
                    orgID:"",
                    workPlaceID:"",
                    workStatusID:"",
                    workDate:""
            }
    });
</script>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);

        

</script>
