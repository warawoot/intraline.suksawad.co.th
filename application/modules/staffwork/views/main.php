<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <!--<h3><?php //echo !empty($title) ? $title : "ทะเบียนประวัติ"; ?> : <small><i class="fa fa-user"></i> <?php //echo $staffName; ?></small></h3>

                 <a href="<?php //echo site_url(); ?>reg" id="btn_back"  class="btn btn-info"><?php //echo $this->config->item('txt_back')?></a>
                 -->
            </header>

            <div class="panel-body">

              <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body profile-information">
                       <div class="col-md-2">
                           <div class="profile-pic text-center">
                               <img alt="" src="<?php echo site_url();?>uploads/<?php echo ($rs->num_rows() > 0) ? $rs->row()->staffImage : ""; ?>">
                           </div>
                       </div>
                       <div class="col-md-5">
                           <div class="profile-desk">
                               <div class="form-horizontal">

                                <?php 
                                    if(($rs->num_rows() > 0)){
                                      $staffID = $rs->row()->staffID;
                                      $staffName = $rs->row()->staffFName.' '.$rs->row()->staffLName;
                                      $expDate = getExpiredDateByWork($rs->row()->staffBirthday);
                                      $expContract = getExpiredDateByContract($rs->row()->ID);
                                      $status = getWorkStatusByWork($rs->row()->ID);
                                      $age = calAge($rs->row()->staffBirthday);
                                      $age = !empty($age) ? $age.' ปี' : $age;
                                    }else{
                                      $staffID = "";
                                      $staffName = "";
                                      $expDate = "";
                                      $expContract = "";
                                      $status = "";
                                      $age = "";
                                    }
                                  ?>
                                  <div class="form-group"><label class="control-label col-sm-6">รหัสพนักงาน</label><div class="col-sm-6"><?php echo $staffID; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">ชื่อ - สกุล</label><div class="col-sm-6"><?php echo $staffName; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">วันที่เกษียณ ฯ</label><div class="col-sm-6"><?php echo $expDate; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">วันที่ครบกำหนดสัญญาจ้าง</label><div class="col-sm-6"><?php echo $expContract; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">สถานภาพการทำงาน</label><div class="col-sm-6"><?php echo $status; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">อายุพนักงาน</label><div class="col-sm-6"><?php echo $age; ?></div></div>
                                  
                                  
                               </div>
                           </div>
                       </div>
                        <div class="col-md-5">
                           <div class="profile-desk">
                               <div class="form-horizontal">
                             

                                <?php 
                                    if(($rs->num_rows() > 0)){
                                      $positonName = getPositionByWork($rs->row()->ID);
                                      $rankName = getRankByWork($rs->row()->ID);
                                      $orgID1 = getOrg1ByWork($rs->row()->ID);
                                      $orgID2 = getOrg2ByWork($rs->row()->ID);
                                      $orgID = getOrgByWork($rs->row()->ID);
                                      $age =  getWorkAgeByWork($rs->row()->ID);
                                      $status = $rs->row()->status;
                                      $orgID = $rs->row()->orgID;
                                      $orgName = $rs->row()->orgName;
                                      $dateIn = $rs->row()->dateIn;
                                      $dateOut = $rs->row()->dateOut;
                                      $dateIn = $dateIn=='0000-00-00' ? '' : $dateIn;
                                      $dateOut = $dateOut=='0000-00-00' ? '' : $dateOut;
                                      if($status==0){
                                        $status = 'ยังปฏิบัติงาน';
                                      }else if($status==2){
                                        $status = 'ลาออก';
                                      }
                                    }else{
                                      $positonName = "";
                                      $rankName = "";
                                      $orgID1 = "";
                                      $orgID2 = "";
                                      $orgID = "";
                                      $age = "";
                                      $status ="";
                                      $orgID = "";
                                      $dateIn = "";
                                      $dateOut = "";
                                      $orgName = "";
                                    }
                                  ?>
                                  <div class="form-group"><label class="control-label col-sm-4">ตำแหน่ง</label><div class="col-sm-8"><?php echo $positonName; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-4">ระดับ</label><div class="col-sm-8"><?php echo $rankName; ?></div></div>
                                  <!--<div class="form-group"><label class="control-label col-sm-6">ฝ่าย / สำนักงาน</label><div class="col-sm-6"><?php echo $orgID1; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">กอง / กลุ่มงาน</label><div class="col-sm-6"><?php echo $orgID2; ?></div></div>
                                  <div class="form-group"><label class="control-label col-sm-6">แผนก</label><div class="col-sm-6"><?php echo $orgID; ?></div></div>-->

                                  <div class="form-group"><label class="control-label col-sm-4">หน่วยงาน</label>
                                    <div class="col-sm-8">
                                        
                                        <?php 
                                                                                    
                                            echo $orgName;
                                        ?>
                                       
                                    </div>
                                  </div>
                                  <div class="form-group"><label class="control-label col-sm-4">สถานะการทำงาน</label>
                                    <div class="col-sm-8"><?php echo $status;?></div>
                                  </div>
                                  <div class="form-group"><label class="control-label col-sm-4">วันที่เริ่มงาน</label>
                                    <div class="col-sm-8"><?php echo toBEDateThai($dateIn)?></div>
                                  </div>
                                  <div class="form-group"><label class="control-label col-sm-4">วันที่ลาออก</label>
                                    <div class="col-sm-8"><?php echo toBEDateThai($dateOut)?></div>
                                  </div>
                                  
                                  <div class="form-group"><label class="control-label col-sm-4">อายุงานทั้งหมด</label><div class="col-sm-8"><?php echo $age; ?></div></div>
                                  
                              
                               </div>
                           </div>
                       </div>
                    </div>
                </section>
            </div>
                 <a class="btn btn-success" href="javascript:window.location='<?php echo site_url();?>staffwork/add/index?token=<?php echo $id;?>'"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</a>
                
                <?php echo $html; ?>
            </div>
        </section>
    </div>
</div>
<script>
        var myApp = angular.module("baseApp",[]);

        /*myApp.config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }]);*/

        myApp.controller('menuCtrl', ['$scope', '$location', function($scope, $location) {
          
          $scope.getClass = function(path) {
              if(path == '/reg'){
                return "active";
              }
              
              /*var cur_path = $location.path().substr(-path.length);
              
              if (cur_path == path) {
                 
                  if($location.path().substr(0).length > 1 && path.length == 1 )
                      return "";
                  else
                      return "active";
              } else {
                  return "";
              }*/
          }
        }]);



      
    $("#dateIn2").datepicker({  
      dateFormat: 'dd/mm/yy',  
      /*showOn: 'button',  
      buttonImageOnly: false, */ 
      dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
      //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
      monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
      changeMonth: true,  
      changeYear: true ,
      showOn: "button",
      buttonImage: "<?php echo base_url()?>assets/images/common_calendar_month_-20.png",
      buttonImageOnly: true
    });
    $("#dateOut2").datepicker({  
      dateFormat: 'dd/mm/yy',  
      /*showOn: 'button',  
      buttonImageOnly: false, */ 
      dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],   
      //monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
      monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
      changeMonth: true,  
      changeYear: true ,
      showOn: "button",
      buttonImage: "<?php echo base_url()?>assets/images/common_calendar_month_-20.png",
      buttonImageOnly: true
    });


</script>
<style>
  .xcrud-search-toggle{
    position:absolute;
    top:-35px;
    left:70px;
  }
</style>