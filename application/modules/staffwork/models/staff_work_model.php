<?php
class Staff_work_model extends MY_Model{

        function __construct() {    	
            parent::__construct();
            $this->_table = 'tbl_staff_work';
            $this->_pk = 'workID';
        }

        public function get_view($id){
        	$sql = "SELECT w.* ,r.rankName,p.positionName,wp.workPlaceName,t.typeName,ws.workStatusName
        			FROM tbl_staff_work w
        			JOIN tbl_rank r ON w.rankID = r.rankID
        			JOIN tbl_position p on w.positionID = p.positionID
        			JOIN tbl_work_place wp on w.workPlaceID = wp.workPlaceID
        			JOIN tbl_staff_type t on w.staffTypeID = t.typeID
        			JOIN tbl_work_status ws on w.workStatusID = ws.workStatusID
        			WHERE workID = ".$id;
        	return  $this->db->query($sql)->row();
        }

        
	
}
/* End of file staff_work_model.php */
/* Location: ./application/module/staffwork/models/staff_work_model.php */