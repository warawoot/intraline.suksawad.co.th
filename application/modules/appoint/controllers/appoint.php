<?php

class Appoint extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('appoint_model');
		$this->load->model('report/report_model');
		$this->load->model('organization/organization_model');
    }
 

    public function index() {
 		 
	    //////////////////////// Step 1  Check Max Id ///////////////////////	 
		 
        $max 	= $this->appoint_model->max_id(); 
		if($max  == NULL){
			$max_id  =  $max+1;
		}else{
 			$max_id  =  $max+1;
 		}
		
		 //////////////////////// Step 2 Check Max Id distinct///////////////////////	 
		
		$distinct_orgId	= $this->appoint_model->distinct_orgchartassigndate($max_id); 
		
		if(is_array($distinct_orgId)){
			redirect(site_url().'appoint', 'refresh');
			exit();
		}
		
		$date_max	= $this->appoint_model->date_max(); 
		
		/*echo "<pre>";
		print_r($date_max);*/
		
		//////////////////////// End Step Check Max Id ///////////////////////
 	 
 		$xcrud	=	xcrud_get_instance();
 		 
        $xcrud->table('tbl_orgchart_assigndate'); 
		
        $arr		=	array("assignNumber"=>"ปี พ.ศ.","assignName"=>"ชื่อข้อบังคับ","assignDate"=>"วันที่เอกสาร","assignFile"=>"สำเนาคำสั่ง");
		//$arr_columns		=	array("assignID"=>"id","assignNumber"=>"เลขที่คำสั่ง","assignName"=>"ชื่อคำสั่ง","assignDate"=>"วันที่เอกสาร" );
		$arr_columns		=	array( "assignNumber"=>"เลขที่คำสั่ง","assignName"=>"ชื่อคำสั่ง" );
		$xcrud->order_by('assignCreateDate','asc');
 		# การอัพโหลดรูปภาพ
 		$xcrud->change_type('assignFile', 'file', '', array('not_rename'=>true));
 		// -- validation_required -- 
		$xcrud->validation_required('assignNumber')->validation_required('assignName')->validation_required('assignDate')  ;
 		//$xcrud->column_pattern('edit', '<a href="#" class="xcrud-action" data-task="edit" data-primary="{assignID}">{value}</a>');
		$xcrud->pass_var('assignID',$max_id,'create');
		
 		$xcrud->button(site_url().'organization/index?token={assignID}','จัดการผังองค์กร','fa fa-folder-open'); 
		 
        $xcrud->label($arr);
        $xcrud->columns($arr_columns);//แสดงในตางราง $xcrud->columns('userName');  
		
		$xcrud->column_cut(150,'assignName'); 
		$xcrud->column_width('assignName','55%');
		
		$xcrud->label('assignDate','วันที่เอกสาร');
		$xcrud->columns('assignDate');//แสดงในตางราง $xcrud->columns('userName');
		//$xcrud->order_by('assignDate', 'desc');
		//$c_assignDate	=	$xcrud->columns('assignDate');
		
		 
		$xcrud->before_insert('hash_assigndate'); 
		 
 		$xcrud->column_callback('assignDate','DateThai');
		//$xcrud->change_type('assignDate', 'date', '2012-10-22 07:01:33'); 
		//$xcrud->change_type('assignDate', 'date', '', array('range_end'=>'end_date')); 
		//$xcrud->change_type('assignDate','text');
		//$xcrud->field_callback('assignDate','DateThai');
		//echo '{assignDate}';
		//$xx		=	$xcrud->columns('assignDate');
		//$xcrud->column_pattern('assignDate','{value}','DateThai');
		$xcrud->before_remove('delete_user_data'); 
        $xcrud->fields($arr);//แสดงและแก้ไขรายละเอียด $xcrud->fields('createDateTime');
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['content']	=	$xcrud->render();
		$data['id']	=   '';
		
		// Report organization // 
		
		$data['actions']	= 'add';
		
		$data['data']		= 'รายงาน / ผังโครงสร้างองค์กร';
		$data['data1']		= 'ค้นหารายงาน / ผังโครงสร้างองค์กร'; 
		$data['data2']		= '';
		$data['assignID']  =  '' ;
		
		// format date : 2015-05-01
		$assignDate			= $this->input->get_post("assignDate"); 
		
		if($assignDate !=''){
			
			$date_start 		= explode('/',$assignDate);  
			  
			$strYear_s 			= $date_start[2];
			$strMonth_s			= $date_start[1];
			$strDay_s			= $date_start[0];
			 
			
			$date_start			= $strYear_s."-".$strMonth_s."-".$strDay_s; 
			
			$get_data			= $this->report_model->check_orgchart_assigndate($date_start); 
			 
			echo $assignID		= $get_data['assignID'];
			echo $assignDate	= $get_data['assignDate'];
			
			
		}else{
			$assignID		=	'';
			$assignDate		=	'';
		}
		$data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , $assignID ,$upperOrgID='' );#Dropdown  $token
		
		
		
        $this->template->load('template/admin', 'appoint',$data);
    }
     
   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>