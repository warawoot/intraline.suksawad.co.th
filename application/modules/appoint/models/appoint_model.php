<?php

class Appoint_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="tbl_orgchart_assigndate";
        $this->pk = "assignID";
	}
        
    function max_id(){
	    $this->db->select_max('assignID');
		$this->db->from('`tbl_orgchart_assigndate`');
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->assignID; 
 		}else{
			$data 		=  '1';  
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
   function max_data($assignID){
	    $this->db->select('*');
		$this->db->from('`tbl_orgchart_assigndate`');
		$this->db->where('`assignID`' ,$assignID);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        //print_r($data);
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
	function distinct_orgchartassigndate($assignID){
		$this->db->select('*');
		$this->db->from('`tbl_orgchart_assigndate`');
		$this->db->where('`assignID`' ,$assignID);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
       
		return ($query->num_rows() > 0) ? $data : NULL;
    }
	
	function date_max(){
   		$this->db->select_max('assignDate');
		$this->db->from('`tbl_orgchart_assigndate`');
		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
       
		return ($query->num_rows() > 0) ? $data : NULL;
    }
	
	// CodeIgniter Active Record insert from one table to another//
	
	function insert_copy_record(){    
		$this->db->insert('table1');
		$this->db->set('to_column');  
		$this->db->select('from_column');
		$this->db->from('table2');
	}
	
	function insert_copy_record1(){    
		$query = $this->db->get('table1');
		foreach ($query->result() as $row) {
			  $this->db->insert('table2',$row);
		}
	}
	
	function insert_copy_record2(){    
		$this->db->select('au_id, au_lname, au_fname');
		$this->db->from('california_authors');
		$this->db->where('state', 'CA');
		$query = $this->db->get();
		
		if($query->num_rows()) {
			$new_author = $query->result_array();
		
			foreach ($new_author as $row => $author) {
				$this->db->insert("authors", $author);
			}           
		}
	}
}
/* End of file  _model.php */
/* Location: ./application/module/ / .php */