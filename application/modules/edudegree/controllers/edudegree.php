<?php

class Edudegree extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		
	}
	
	public function index(){

		
		$this->load->helper('xcrud_helper');
		$xcrud = xcrud_get_instance();

		
		// select table//
		$xcrud->table('tbl_edu_degree');
		
		
		//// List /////
		$col_name = array(
			'eduDegreeName' => 'วุฒิการศึกษา',
			'eduDegreeAlias' => 'อักษรย่อ'

		);

		$xcrud->columns('eduDegreeName,eduDegreeAlias');
		$xcrud->label($col_name);
		// End List//

		//// Form //////

		$xcrud->fields('eduDegreeName,eduDegreeAlias');

		$data['html'] = $xcrud->render();
		

		$data['title'] = "วุฒิการศึกษา";
        $this->template->load("template/admin",'main', $data);

	}

}
/* End of file edudegree.php */
/* Location: ./application/module/edudegree/edudegree.php */