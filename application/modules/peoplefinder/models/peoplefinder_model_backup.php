<?php

class Peoplefinder_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	}
    
	function people_finder($assignID, $key, $limit, $offset){
		if($this->session->userdata('orgID')!=0){
	 		$orgID = $this->session->userdata('orgID');
	 		$upper = $this->getLowerOrg($orgID,$assignID);
				$query = $this->db->query("
				 SELECT  t1.ID as uid ,t1.orgID,
				 CONCAT(t1.staffPreName ,t1.staffFName ,'  ',t1.staffLName) as name 
				  ,t4.positionName  as position,
				 CONCAT( t3.orgName
							 ,' / ',
				(SELECT sub_1.orgName FROM tbl_org_chart as sub_1 WHERE sub_1.upperOrgID = t3.orgID and sub_1.assignID = '$assignID' limit 1 ) ) as department ,
							 t1.staffImage as image,t2.workStartDate
							 
				 FROM `tbl_staff` as t1 
						INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID
						INNER JOIN (
		    				select max(workStartDate) workStartDate,staffID
		    				from tbl_staff_work
		    				group by staffID
		  					) as max on max.staffID = t2.staffID  and max.workStartDate = t2.workStartDate
						INNER JOIN tbl_org_chart as t3 ON t2.orgID = t3.orgID
						INNER JOIN tbl_position as t4 ON t2.positionID = t4.positionID
							
				 WHERE t3.assignID = '$assignID' 
				 and (staffPreName like '$key%' OR  staffFName  like '$key%' OR staffLName like '$key%')  and t2.workStatusID not in(2,3,4,5,6) AND t2.orgID IN ($orgID$upper)
							group by uid
							order by  t2.workStartDate desc 

							Limit ".$offset." ,  ".$limit." 
						  "
				 );
		$data['peopledb'] = $query->result_array();
		$query2 = $this->db->query("
		 			SELECT  count(t1.ID) as ID
					FROM `tbl_staff` as t1 
					INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID
					INNER JOIN tbl_org_chart as t3 ON t2.orgID = t3.orgID
					INNER JOIN tbl_position as t4 ON t2.positionID = t4.positionID
					WHERE t3.assignID = '$assignID' 
					and (staffPreName like '$key%' OR  staffFName  like '$key%' OR staffLName like '$key%')  and t2.workStatusID not in(2,3,4,5,6) AND t2.orgID IN ($orgID$upper) order by t2.workStartDate desc
 
				  "
		 );
		 }else{
		 	$query = $this->db->query("
				 			SELECT  t1.ID as uid ,CONCAT(t1.staffPreName ,t1.staffFName ,'  ',t1.staffLName) as name  ,t4.positionName  as position,
							 CONCAT( t3.orgName
							 ,' / ',(SELECT sub_1.orgName FROM tbl_org_chart as sub_1 WHERE sub_1.upperOrgID = t3.orgID and sub_1.assignID = '$assignID' limit 1 ) ) as department ,
							 t1.staffImage as image,t2.workStartDate
							 
							FROM `tbl_staff` as t1 
							INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID
							INNER JOIN (
		    				select max(workStartDate) workStartDate,staffID
		    				from tbl_staff_work
		    				group by staffID
		  					) as max on max.staffID = t2.staffID  and max.workStartDate = t2.workStartDate
							INNER JOIN tbl_org_chart as t3 ON t2.orgID = t3.orgID
							INNER JOIN tbl_position as t4 ON t2.positionID = t4.positionID
							
							WHERE t3.assignID = '$assignID' 
							and (staffPreName like '$key%' OR  staffFName  like '$key%' OR staffLName like '$key%')  and t2.workStatusID not in(2,3,4,5,6) 
							group by uid
							order by  t2.workStartDate desc 

							Limit ".$offset." ,  ".$limit." 
						  "
				 );

		  $data['peopledb'] = $query->result_array();
		$query2 = $this->db->query("
		 			SELECT  count(t1.ID) as ID
					FROM `tbl_staff` as t1 
					INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID
					INNER JOIN tbl_org_chart as t3 ON t2.orgID = t3.orgID
					INNER JOIN tbl_position as t4 ON t2.positionID = t4.positionID
					WHERE t3.assignID = '$assignID' 
					and (staffPreName like '$key%' OR  staffFName  like '$key%' OR staffLName like '$key%')  and t2.workStatusID not in(2,3,4,5,6) order by t2.workStartDate desc
 
				  "
		 );

		 }		
		$count = $query2->result_array();
		$data['total'] = $count[0]["ID"];
        return $data; 
		
		
	} 
	private function getLowerOrg($orgID,$assignID){
            $strOrg = "";
            $sql = "SELECT orgID FROM tbl_org_chart WHERE upperOrgID=$orgID AND assignID=$assignID";
            $r = $this->db->query($sql);
            $result = $r->result();
            foreach ($result as $key => $row) {
                $strOrg .= ",".$row->orgID.$this->getLowerOrg($row->orgID,$assignID);
            }
            return $strOrg;  
        }
   	function people_finder2($assignID,$staffID){
		 
		$query = $this->db->query("
		 			SELECT  t1.ID as uid ,CONCAT(t1.staffPreName ,t1.staffFName ,'  ',t1.staffLName) as name  ,t4.positionName  as position,
					 CONCAT( t3.orgName
					 ,' / ',(SELECT sub_1.orgName FROM tbl_org_chart as sub_1 WHERE sub_1.upperOrgID = t3.orgID and sub_1.assignID = '$assignID' limit 1 ) ) as department ,
					 t1.staffImage as image
					 
					FROM `tbl_staff` as t1 
					INNER JOIN tbl_staff_work as t2 ON t1.ID = t2.staffID
					INNER JOIN tbl_org_chart as t3 ON t2.orgID = t3.orgID
					INNER JOIN tbl_position as t4 ON t2.positionID = t4.positionID
					WHERE t3.assignID ='$assignID' and  t1.ID = $staffID and t2.workStatusID not in(2,3,4,5,6) order by t2.workStartDate desc
				  " 
		 );
        return $query->result_array(); 
		
		
	} 

   
} 