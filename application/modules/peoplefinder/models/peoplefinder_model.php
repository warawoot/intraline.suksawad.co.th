<?php

class Peoplefinder_model extends MY_Model {

	function __construct() {
		parent::__construct();
        $this->table="";
        $this->pk = "";
	}

	function people_finder($assignID, $key, $limit, $offset){
		$query = $this->db->query(
			"SELECT  
				t1.ID as uid ,
				t1.staffID as staffID , 
				CONCAT(t1.staffFName ,'  ',t1.staffLName) as name  ,
				CONCAT(t1.staffID,'.jpg') as image
			FROM `tbl_staff` as t1
			WHERE 1
				AND (staffFName  like '$key%' OR staffLName like '$key%')
				AND t1.status not in(2,3,4,5,6)
			GROUP BY uid
			ORDER BY CONVERT (name USING tis620)
		    LIMIT ".$offset." ,  ".$limit);

		$data['peopledb'] = $query->result_array();
		
		$query2 = $this->db->query(
			"SELECT  
				count(t1.ID) as ID
			FROM `tbl_staff` as t1
			WHERE 1 
				AND (staffFName  like '$key%' OR staffLName like '$key%')
				AND t1.status not in(2,3,4,5,6)");

		$count = $query2->result_array();
		$data['total'] = $count[0]["ID"];
        return $data;
	}
}
