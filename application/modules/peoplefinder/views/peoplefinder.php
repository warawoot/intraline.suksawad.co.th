<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>people finder</title>
  <meta name="description" content="people finder">
  <meta name="author" content="smmms">
 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/peoplefinder/javascripts/smmms-peopleFinder/smmms-peopleFinder.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/peoplefinder/stylesheets/font-awesome-4.4.0/css/font-awesome.min.css">
  <style>
  #comboA_popup .pf-popup-content-table-col2{
    width: 500px;
  }
  #comboA_popup .pf-popup-content-table-col2 p:nth-child(1) {
    font-weight: bold;
  }
  #comboA_popup .pf-popup-content-table-col2 p:nth-child(2) {
    font-style: italic;
    font-size: 90%;
  }
  </style>


  <!-- jquery 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
  <!-- jquery for DEV                              -->
   
 
</head>
<body>

<div>
<input size="30" id="comboA" name="comboA" class="peopleFinder" placeholder="Name or Surname here ..."></input>
</div>
<br /><br /><br /><br /><br /><br /><br />
<div>
<input size="30" id="comboB" name="comboB" class="peopleFinder" placeholder="Name or Surname here ..."></input>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                <h3>ข้อมูลชื่อตำแหน่ง</h3>
            </header>
            <div class="panel-body">
                <?php echo $data; ?>
            </div>
        </section>
    </div>
</div>
 <script src="<?php echo base_url(); ?>assets/peoplefinder/javascripts/smmms-peopleFinder/jquery.smmms-peopleFinder.js"></script>
  <script type="text/javascript">
    $(document).ready( function() {
        $('#comboA').smmmsPeopleFinder({
          url: '<?php echo base_url(); ?>assets/peoplefinder/test/server.php',
          imageFolderUrl: '',
          idColumn: 'uid',
          textColumn: 'name',
          displayColumns: [
            {'map': 'image', 'image': true},
            [
              {
                'map': 'name'
              },
              {
                'map': 'position'
              }
            ],
            {
              'map': 'department'
            }
          ],
          rows: 10          
        });

        $('#comboB').smmmsPeopleFinder({
          url: '<?php echo base_url(); ?>assets/peoplefinder/test/server.php',
          idColumn: 'uid',
          textColumn: 'name',
          displayColumns: [
            {
              'map': 'name'
            },
            {'map': 'image', 'image': true},            
            [
              {
                'map': 'department'
              },
              {
                'map': 'position'
              }
            ]
          ],
          rows: 10
        });        
    });
  </script>

</body>
</html>