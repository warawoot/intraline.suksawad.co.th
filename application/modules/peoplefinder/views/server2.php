<?php

$searchName = htmlspecialchars($_GET["key"]);

$outputHtml = false;
if(!empty($_GET["html"]) && $_GET["html"] === "true")$outputHtml = true;

$rows = 10;
if(!empty($_GET["rows"]))$rows = intval ($_GET["rows"]);

$page = 0;
if(!empty($_GET["page"]))$page = (intval ($_GET["page"]) - 1) * $rows;

 
 $peopledb = $peopledb;

/*$peopledb=Array
(
    0 => Array(
      'uid' => '0','name' => 'Adam Sandler','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'https://cdn4.iconfinder.com/data/icons/free-social-media-icons/512/User.png'
    ),
    1 => Array(
      'uid' => '1','name' => 'Al Pacino','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'https://upload.wikimedia.org/wikipedia/commons/9/98/Al_Pacino.jpg'
    ),
    2 => Array(
      'uid' => '2','name' => 'Anthony Hopkins','position'=>'','department'=>'Division Name 1 / Department 1','image2' => 'http://ia.media-imdb.com/images/M/MV5BMTg5ODk1NTc5Ml5BMl5BanBnXkFtZTYwMjAwOTI4._V1._SX287_SY400_.jpg'
    ),
    3 => Array(
      'uid' => '3','name' => 'Antonio Banderas','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTUyOTQ3NTYyNF5BMl5BanBnXkFtZTcwMTY2NjIzNQ@@._V1._SX140_CR0,0,140,209_.jpg'
    ),
    4 => Array(
      'uid' => '4','name' => 'Arnold Schwarzenegger','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTI3MDc4NzUyMV5BMl5BanBnXkFtZTcwMTQyMTc5MQ@@._V1._SY209_CR13,0,140,209_.jpg'
    ),
    5 => Array(
      'uid' => '5','name' => 'Ben Affleck','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTI4MzIxMTk0Nl5BMl5BanBnXkFtZTcwOTU5NjA0Mg@@._V1._SX140_CR0,0,140,209_.jpg'
    ),
    6 => Array(
      'uid' => '6','name' => 'Brad Pitt','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMjA1MjE2MTQ2MV5BMl5BanBnXkFtZTcwMjE5MDY0Nw@@._V1._SX140_CR0,0,140,209_.jpg'
    ),
    7 => Array(
      'uid' => '7','name' => 'Bruce Willis','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMjA0MjMzMTE5OF5BMl5BanBnXkFtZTcwMzQ2ODE3Mw@@._V1._SY209_CR19,0,140,209_.jpg'
    ),
    8 => Array(
      'uid' => '8','name' => 'Charles Chaplin','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BNDcwMDc0ODAzOF5BMl5BanBnXkFtZTgwNTY2OTI1MDE@._V1._SX140_CR0,0,140,209_.jpg'
    ),
    9 => Array(
      'uid' => '9','name' => 'Clint Eastwood','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTg3MDc0MjY0OV5BMl5BanBnXkFtZTcwNzU1MDAxOA@@._V1._SY209_CR7,0,140,209_.jpg'
    ),
    10 => Array(
      'uid' => '10','name' => 'Danny DeVito','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTE5MjM5MzM3M15BMl5BanBnXkFtZTYwOTEzOTY0._V1._SY209_CR4,0,140,209_.jpg'
    ),
    11 => Array(
      'uid' => '11','name' => 'Dustin Hoffman','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTc3NzU0ODczMF5BMl5BanBnXkFtZTcwODEyMDY5Mg@@._V1._SY209_CR8,0,140,209_.jpg'
    ),                                
    12 => Array(
      'uid' => '12','name' => 'Eddie Murphy','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTc0NDQzODAwNF5BMl5BanBnXkFtZTYwMzUzNTk3._V1._SY209_CR4,0,140,209_.jpg'
    ),               
    13 => Array(
      'uid' => '13','name' => 'Edward Burns','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTY1MTU0NjIyMl5BMl5BanBnXkFtZTcwODc3OTc1MQ@@._V1._SX140_CR0,0,140,209_.jpg'
    ), 
    14 => Array(
      'uid' => '14','name' => 'George Clooney','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMjEyMTEyOTQ0MV5BMl5BanBnXkFtZTcwNzU3NTMzNw@@._V1._SY209_CR7,0,140,209_.jpg'
    ),                                
    15 => Array(
      'uid' => '15','name' => 'Jackie Chan','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTk4MDM0MDUzM15BMl5BanBnXkFtZTcwOTI4MzU1Mw@@._V1._SY209_CR5,0,140,209_.jpg'
    ),               
    16 => Array(
      'uid' => '16','name' => 'Jason Statham','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTkxMzk2MDkwOV5BMl5BanBnXkFtZTcwMDAxODQwMg@@._V1._SX140_CR0,0,140,209_.jpg'
    ), 
    17 => Array(
      'uid' => '17','name' => 'Jean Reno','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTgzNjA1MjY2M15BMl5BanBnXkFtZTYwMjgzOTk0._V1._SX140_CR0,0,140,209_.jpg'
    ),                                
    18 => Array(
      'uid' => '18','name' => 'Jet Li','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMjAxNjc0MjIyM15BMl5BanBnXkFtZTcwNTM2NDA4MQ@@._V1._SY209_CR16,0,140,209_.jpg'
    ),               
    19 => Array(
      'uid' => '19','name' => 'Johnny Depp','position'=>'Sales Representative','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    20 => Array(
      'uid' => '20','name' => 'User Name 1','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),    
    21 => Array(
      'uid' => '21','name' => 'User Name 2','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ), 
    22 => Array(
      'uid' => '22','name' => 'User Name 3','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    23 => Array(
      'uid' => '23','name' => 'User Name 4','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    24 => Array(
      'uid' => '24','name' => 'User Name 5','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    25 => Array(
      'uid' => '25','name' => 'User Name 6','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    26 => Array(
      'uid' => '26','name' => 'User Name 7','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    27 => Array(
      'uid' => '27','name' => 'User Name 8','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    28 => Array(
      'uid' => '28','name' => 'User Name 9','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    29 => Array(
      'uid' => '29','name' => 'User Name 10','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    30 => Array(
      'uid' => '30','name' => 'User Name 11','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    31 => Array(
      'uid' => '31','name' => 'User Name 12','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    32 => Array(
      'uid' => '32','name' => 'User Name 13','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),
    33 => Array(
      'uid' => '33','name' => 'User Name 14','position'=>'QA Manager','department'=>'Division Name 1 / Department 1','image' => 'http://ia.media-imdb.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1._SY209_CR3,0,140,209_.jpg'
    ),                                        
);*/


// PHP >= 5.5
//$keys = array_keys(array_column($peopledb, 'name', 'lastName'),'J');
//print_r ($keys);

//PHP < 5.5
function searchName($name, $array) {
   $peopleFound = Array();
   foreach ($array as $key => $val) {
    if($name !== ""){   
      if (stripos($val['name'],$name) !== false) {
         //return $key;
         $peopleFound[]=$array[$key];
      }
    }else{
      $peopleFound[]=$array[$key];  
    } 
   }
   return $peopleFound;
}

$listOfPeople = searchName($searchName, $peopledb);

$totalItems = count($listOfPeople);
$firstItem = $page;
$lastItem = $page + $rows;
$itemperpage = $rows;
if($lastItem > $totalItems)$lastItem = $totalItems; 

$listOfPeople = array_slice($listOfPeople, $page, $rows, false);

$response=Array
(
    'total' => $totalItems,
    'items' => $listOfPeople
);

if(!$outputHtml){
  echo json_encode($response);
}else{
  foreach($listOfPeople as $key => $val){
    echo "<div><img src='" . $val['image'] . "' width:20 ><span>" . $val['name'] . " " . $val['lastName'] . "</span></div>" ;
  }
}