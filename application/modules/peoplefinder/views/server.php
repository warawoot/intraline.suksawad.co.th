<?php


$searchName = "";
if(!empty($_GET["key"])){
  $searchName = htmlspecialchars($_GET["key"]);
}
$outputHtml = false;
if(!empty($_GET["html"]) && $_GET["html"] === "true")$outputHtml = true;

$rows = 10;
if(!empty($_GET["rows"]))$rows = intval ($_GET["rows"]);

$page = 0;
if(!empty($_GET["page"]))$page = (intval ($_GET["page"]) - 1) * $rows;

 $peopledb = $peopledb;


if(!empty($_GET["id"])){
  $id = intval ($_GET["id"]);

$listOfPeople = [];
$listOfPeople[] = $peopledb[$id];

  $response=Array
(
    'total' => count($listOfPeople),
    'items' => $listOfPeople
);

  if(!$outputHtml){
    echo json_encode($response);
  }else{
    echo $response;  
  }
    
    exit;

}
// PHP >= 5.5
//$keys = array_keys(array_column($peopledb, 'name', 'lastName'),'J');
//print_r ($keys);

//PHP < 5.5
function searchName($name, $array) {
   $peopleFound = Array();
   foreach ($array as $key => $val) {
    if($name !== ""){   
      if (stripos($val['name'],$name) !== false) {
         //return $key;
         $peopleFound[]=$array[$key];
      }
    }else{
      $peopleFound[]=$array[$key];  
    } 
   }
   return $peopleFound;
}

$listOfPeople = searchName($searchName, $peopledb);

$totalItems = count($listOfPeople);
$firstItem = $page;
$lastItem = $page + $rows;
$itemperpage = $rows;
if($lastItem > $totalItems)$lastItem = $totalItems; 

$listOfPeople = array_slice($listOfPeople, $page, $rows, false);

$response=Array
(
    'total' => $totalItems,
    'items' => $listOfPeople
);

if(!$outputHtml){
  echo json_encode($response);
}else{
  foreach($listOfPeople as $key => $val){
    echo "<div><img src='" . $val['image'] . "' width:20 ><span>" . $val['name'] . " " . $val['lastName'] . "</span></div>" ;
  }
}