<?php
class User_model extends MY_Model{

    function __construct() {    	
        parent::__construct();
        $this->_table = 'tbl_user';
        $this->_pk = 'userID';
    }
    
    public function authPassword($username,$password){
        $sql = "SELECT 
                    u.userID,
                    u.userName, 
                    u.roleID, 
                    s.ID,
                    CONCAT(s.staffPreName,s.staffFName,' ',s.staffLName) as fullName,
                    s.orgID,
                    s.rankID,
                    s.orgIDManage 
                FROM `tbl_user`u 
                INNER JOIN tbl_staff s ON u.userID = s.staffID
                WHERE u.userName='".$username."'
                    AND u.userCredential='".hash('sha256',$password)."'";
        $r = $this->db->query($sql);
        return ($r->num_rows() > 0) ? $r : NULL;
    }		
}
/* End of file user_model.php */
/* Location: ./application/module/user/models/user_model.php */