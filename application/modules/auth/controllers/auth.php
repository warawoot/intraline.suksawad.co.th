<?php

class Auth extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');		
	}
	
	public function index(){
		if ($this->my_usession->logged_in){         
		   redirect('dashboard','refresh');
		}
		if(strrpos($_SERVER['HTTP_REFERER'], "logout")) {
			$data['referer'] = "";
		} else {
			$data['referer'] = $_SERVER['HTTP_REFERER'];
		}
        $this->load->view('login',$data); 
	}
		
  	public function submitLogin(){    
        if(!empty($this->input->post('username')) && !empty($this->input->post('password'))){
            $username = trim($this->input->post('username',true));
            $password = trim($this->input->post('password',true));
			$referer = trim($this->input->post('referer',true));
            $ru = $this->user_model->authPassword($username,$password);
            if(!empty($ru)){
				$this->session->set_userdata("login", true);
                $this->session->set_userdata("ID", trim($ru->row()->ID));
                $this->session->set_userdata("userID", trim($ru->row()->userID));
                $this->session->set_userdata("roleID", trim($ru->row()->roleID));
                $this->session->set_userdata("orgID", trim($ru->row()->orgID));
                $this->session->set_userdata("rankID", trim($ru->row()->rankID));
                //$this->session->set_userdata("userName", trim($ru->row()->userName));
                $this->session->set_userdata("fullName", trim($ru->row()->fullName));
                $this->session->set_userdata("orgIDManage", trim($ru->row()->orgIDManage));
				if($referer != "") {
					//echo 'referer';
                	redirect($referer,'refresh');
				} else {
					//echo 'empty referer';
                	redirect('dashboard','refresh');
				}
				//redirect('dashboard','refresh');
            }else{     
				redirect('auth/loginFail','refresh');		
           }
        }else{
			redirect('auth','refresh');     
        }
	}
	
	public function submitFunc(){
		$func 	= (!empty($_GET['func'])) ? mysql_escape_string($this->input->get('func',true)) : '';
		if($func == 'normal') {
			$this->template->load('template/admin','regist/regist_manage');
		} else if($func == 'json') {
			$this->loginAjax();
		} else {
			show_404();
		}
	}
	
	public function loginFail() {
		if(strrpos($_SERVER['HTTP_REFERER'], "logout")) {
			$data['referer'] = "";
		} else {
			$data['referer'] = $_SERVER['HTTP_REFERER'];
		}
		$data['msg'] = "Username หรือ Password ที่ป้อนไม่ถูกต้อง";
		$this->load->view('login',$data); 
	}
	
	public function logout() {		
		$this->session->unset_userdata('login');
		$this->session->unset_userdata('ID');
		$this->session->unset_userdata('userID');
		$this->session->unset_userdata('userName');	
		$this->session->unset_userdata('roleID');
        $this->session->unset_userdata('fullName');
		$this->session->sess_destroy();
		redirect('auth','refresh');
	}
}

/* End of file auth.php */
/* Location: ./application/module/auth/auth.php */