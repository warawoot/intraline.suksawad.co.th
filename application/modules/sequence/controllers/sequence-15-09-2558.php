<?php

class Sequence extends MY_Controller {
   
    function __construct() {
    	parent::__construct();
     	$this->load->model('sequence_model');
		$this->load->model('organization/organization_model');
		$this->assingID	=	$this->sequence_model->get_last_assignID();
    }
 

    public function index() {
 		
		
		$xcrud	=	xcrud_get_instance();
 		 
        $xcrud->table('tbl_seguence'); 
        $arr		=	array("segID"=>"อัตราที่","positionID"=>"ตำแหน่ง" , "rankIDBegin"=>"ระดับตั้งแต่" ,"rankIDEnd"=>"ระดับสูงสุด", "id"=>"ฝ่าย / กอง / แผนก", "isHeader"=>"หัวหน้า", "isConsult"=>"ที่ปรึกษา", "isActing"=>"รักษาการ" , "seqRemark"=>"หมายเหตุ"  );
   		
		// -- relation -- //
		
		$xcrud->relation('positionID','tbl_position','positionID', 'positionName' ); // 
		$xcrud->relation('id','tbl_org_chart','orgID', 'orgName','tbl_org_chart.assignID = '.$this->assingID); // 
		//$xcrud->relation ( 'id','tbl_org_chart','orgID','orgName','tbl_org_chart.assignID = '.$this->assingID, '', '', '', array('left_key'=>'orgID','level_key'=>'upperOrgID'), '', '' ) ;
		$xcrud->relation('rankIDBegin','tbl_rank','rankID', 'rankName' ); // . array('primary_key'=>'orgID','parent_key'=>'upperOrgID')
		$xcrud->relation('rankIDEnd','tbl_rank','rankID', 'rankName' ); // 
		//$xcrud->button(site_url().'sequence/edit/?token={segsCode}','แก้ไข','xcrud-action btn btn-warning glyphicon glyphicon-edit '); 
		
		$xcrud->button(site_url().'sequence/edit/?token={segsCode}','แก้ไข','glyphicon glyphicon-edit','btn-warning');
		
 		// -- validation_required -- 
		$xcrud->validation_required('segID')->validation_required('positionID')->validation_required('id')->validation_required('rankIDBegin')->validation_required('rankIDEnd') ;
 		 
		$xcrud->hide_button('edit'); 
 		//$xcrud->unset_add(); 
        $xcrud->label($arr);
        $xcrud->columns($arr);  
        $xcrud->fields($arr); 
         
        $xcrud->unset_print();
        $xcrud->unset_title();
        $xcrud->unset_csv();
		 
        $data['data']	=	$xcrud->render();
        $this->template->load('template/admin', 'sequence',$data);
    }
    
	
	public function add() {
		
		$data['get_segsCode'] 		= '';
        $data['get_segID']  		= '';
        $data['get_positionID']  	= '';
        $data['get_id']  			= '';
        $data['get_seqRemark']  	= '';
        $data['get_rankIDBegin']  	= '';
        $data['get_rankIDEnd']  	= '';
		
		$data['get_isHeader']  		= '';
		$data['get_isConsult']  	= '';
		$data['get_isActing']  		= '';
		
		$data['position'] 		= $this->sequence_model->get_dropdown_all('positionID' , 'positionName' ,'tbl_position');#Dropdown
		$data['rankIDBegin'] 	= $this->sequence_model->get_dropdown_all('rankID' , 'rankName' ,'tbl_rank');#Dropdown
		$data['rankIDEnd'] 		= $this->sequence_model->get_dropdown_all('rankID' , 'rankName' ,'tbl_rank');#Dropdown
  		$data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , $this->assingID ,$upperOrgID='' );#Dropdown  
        $data['data']	=	'';
		$data['action']	=	'add';
        $this->template->load('template/admin', 'sequence_add',$data);
    } 
	
	public function edit() {
		
		$token	= $this->input->get('token'); 
		
		$data['edit']	=   $this->sequence_model->get_edit($token);
		
		//echo "<pre>";
		//print_r($data['edit']);
		
        $data['get_segsCode'] 		= $data['edit']['segsCode'];
        $data['get_segID']  		= $data['edit']['segID'];
        $data['get_positionID']  	= $data['edit']['positionID'];
        $data['get_id']  			= $data['edit']['id'];
        $data['get_seqRemark']  	= $data['edit']['seqRemark'];
        $data['get_rankIDBegin']  	= $data['edit']['rankIDBegin'];
        $data['get_rankIDEnd']  	= $data['edit']['rankIDEnd'];
		
		$data['get_isHeader']  			= $data['edit']['isHeader'];
		$data['get_isConsult']  		= $data['edit']['isConsult'];
		$data['get_isActing']  			= $data['edit']['isActing'];
		
		$data['action']	=	'edit';
		$data['position'] 		= $this->sequence_model->get_dropdown_all('positionID' , 'positionName' ,'tbl_position');#Dropdown
		$data['rankIDBegin'] 	= $this->sequence_model->get_dropdown_all('rankID' , 'rankName' ,'tbl_rank');#Dropdown
		$data['rankIDEnd'] 		= $this->sequence_model->get_dropdown_all('rankID' , 'rankName' ,'tbl_rank');#Dropdown
  		$data['dropdown_org_chart'] = $this->organization_model->getDropdownTree($level = 0, $prefix = '' , $this->assingID ,$upperOrgID= $data['get_id'] );#Dropdown  
        $data['data']	=	'';
        $this->template->load('template/admin', 'sequence_add',$data);
    }
	
	
	public function check_add() {
		
		$action		= $this->input->get_post("action"); 
		$sCode		= $this->input->get_post("sCode"); 
		 
		$data	=	array( 
		
				"segID"=>$this->input->get_post("segID"),
				"positionID"=>$this->input->get_post("positionID"),
 				"id"=>addslashes($this->input->get_post("id")),
				"seqRemark"=>addslashes($this->input->get_post("seqRemark")),
				"rankIDBegin"=>addslashes($this->input->get_post("rankIDBegin")),
				"rankIDEnd"=>addslashes($this->input->get_post("rankIDEnd")) ,
				"isHeader"=>addslashes($this->input->get_post("isHeader")) ,
				"isConsult"=>addslashes($this->input->get_post("isConsult")) ,
				"isActing"=>addslashes($this->input->get_post("isActing")) 
		 );
		
		$data2	=	array( 
  				
				"segID"=>$this->input->get_post("segID"),
				"positionID"=>$this->input->get_post("positionID"),
 				"id"=>addslashes($this->input->get_post("id")),
				"seqRemark"=>addslashes($this->input->get_post("seqRemark")),
				"rankIDBegin"=>addslashes($this->input->get_post("rankIDBegin")),
				"rankIDEnd"=>addslashes($this->input->get_post("rankIDEnd")) ,
				"isHeader"=>addslashes($this->input->get_post("isHeader")) ,
				"isConsult"=>addslashes($this->input->get_post("isConsult")) ,
				"isActing"=>addslashes($this->input->get_post("isActing")) 
		 );
		 
		 
		if($action == 'edit'){
			$data = $this->sequence_model->update($sCode,$data2); 
			
		}else{
			$data = $this->sequence_model->insert($data); 	
		} 
  		 
		if($data == 'succesfully'){
				echo  'succesfully';
		}else{
				echo 'failed';
		}									
         
	} 
	
   
}
/* End of file  .php */
/* Location: ./application/module/  */
?>