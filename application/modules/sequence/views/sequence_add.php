<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading"> 
                <h3>ข้อมูลอัตราพนักงาน</h3>
            </header>
            <div class="panel-body">
                <link href="http://dpo-ehr.smmms.com/xcrud/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
                <link href="http://dpo-ehr.smmms.com/xcrud/plugins/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
                <link href="http://dpo-ehr.smmms.com/xcrud/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
                <link href="http://dpo-ehr.smmms.com/xcrud/themes/bootstrap/xcrud.css" rel="stylesheet" type="text/css">
                <div class="xcrud">
                    <div class="xcrud-container">
                    <div class="xcrud-ajax"> 
                    <div class="xcrud-view">
                            <div class="form-horizontal">
                                <div class="form-group"><label class="control-label col-sm-3">อัตราที่*</label>
                                	<div class="col-sm-9">
                                    	<input class="xcrud-input form-control" data-required="1" data-unique="" type="text" data-type="text" value="<?php echo $get_segID;?>" id="dGJsX3NlZ3VlbmNlLnNlZ0lE" name="dGJsX3NlZ3VlbmNlLnNlZ0lE" maxlength="255">
                                    </div>
                                </div>
                                <div class="form-group"><label class="control-label col-sm-3">ตำ่แหน่ง*</label>
                            		<div class="col-sm-9">
                                    	<?php 	echo form_dropdown('positionID',$position , $get_positionID ,'class="form-control" id="positionID"  '); ?>
                                    </div>
                            	</div>
                                <div class="form-group"><label class="control-label col-sm-3">ระดับตั้งแต่*</label>
                            		<div class="col-sm-9">
                                    	<?php 	echo form_dropdown('rankIDBegin',$rankIDBegin ,$get_rankIDBegin ,'class="form-control" id="rankIDBegin" '); ?>
                                    </div>
                            	</div>
                                <div class="form-group"><label class="control-label col-sm-3">ระดับสูงสุด*</label>
                            		<div class="col-sm-9">
                                    	<?php 	echo form_dropdown('rankIDEnd',$rankIDEnd ,$get_rankIDEnd ,'class="form-control" id="rankIDEnd" '); ?>
                                    </div>
                            	</div>
                                <div class="form-group"><label class="control-label col-sm-3">ฝ่าย / กอง / แผนก*</label>
                                    <div class="col-sm-9">
                                    	<select class="xcrud-input form-control form-control" data-required="1" data-type="select" name="agencies" id="agencies" maxlength="11">
                                        	 <?php  
												echo $dropdown_org_chart;
											 ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="control-label col-sm-3">หัวหน้า*  </label>
                                    <div class="col-sm-9">
                                    <label>
                                          <input class="xcrud-input checkbox" type="checkbox" data-type="bool" name="isHeader" id="isHeader" maxlength="1"  <?php if($get_isHeader == 1){ echo "checked=\"checked\"";} ?> >
                                    </label>
                                    </div>
                                </div>
                                 <div class="form-group"><label class="control-label col-sm-3">ที่ปรึกษา*</label>
                                    <div class="col-sm-9">
                                    	<label>
                                    	 <input class="xcrud-input checkbox" type="checkbox" data-type="bool" name="isConsult" id="isConsult" maxlength="1"  <?php if($get_isConsult == 1){ echo "checked=\"checked\"";} ?> >
                                         </label>
                                     </div>
                                </div>
                                 <div class="form-group"><label class="control-label col-sm-3">รักษาการ*</label>
                                    <div class="col-sm-9">
                                    	<label>
                                    	<input class="xcrud-input checkbox" type="checkbox" data-type="bool" name="isActing" id="isActing" maxlength="1"  <?php if($get_isActing == 1){ echo "checked=\"checked\"";} ?> >
                                        </label>
                                     </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-3">หมายเหตุ</label>
                                    <div class="col-sm-9">
                                    <textarea name="dGJsX3NlZ3VlbmNlLnNlcVJlbWFyaw--"  rows="10" class="xcrud-input xcrud-texteditor form-control editor-loaded col-sm-12" id="dGJsX3NlZ3VlbmNlLnNlcVJlbWFyaw--" data-type="texteditor" aria-hidden="true"><?php echo $get_seqRemark;?></textarea>
                                  </div>
                                </div>
                            </div>
            		</div>
                    <div class="xcrud-top-actions btn-group">
                        <a href="javascript:;" data-task="save" data-after="list" class="btn btn-primary xcrud-action"  onclick="on_save()">บันทึกและย้อนกลับ</a><a href="javascript:;" data-task="list" class="btn btn-warning xcrud-action"  onclick="on_back()">ย้อนกลับ</a></div>
                    <div class="xcrud-nav"> </div>
            </div>
                    <div class="xcrud-overlay" style="width: 1313px; opacity: 0.6; display: none;"></div>
                </div>
</div>
	 
                 </div>
        </section>
    </div>
</div>

<script type="text/javascript">
   		
function on_save(){
			
		var rates			= $('#dGJsX3NlZ3VlbmNlLnNlZ0lE').val();
		var positionID		= $('#positionID').val(); 
		var rankIDBegin		= $('#rankIDBegin').val();
		var rankIDEnd		= $('#rankIDEnd').val();
		var agencies		= $('#agencies').val();
	    var deail			= $('#dGJsX3NlZ3VlbmNlLnNlcVJlbWFyaw--').val();
 		  
 		
		var isHeader = document.getElementById ("isHeader");
		var isHeaderChecked = isHeader.checked;
 		isHeaderIsChecked = (isHeaderChecked)? true : false;  
		
		var isConsult = document.getElementById ("isConsult");
		var isConsultChecked = isConsult.checked;
 		isConsultIsChecked = (isConsultChecked)? true: false;  
 		
		var isActing = document.getElementById ("isActing");
		var isActingChecked = isActing.checked;
 		isActingIsChecked = (isActingChecked)? true: false;  		
	
		
		 if(rates == '' ){
			 alert('กรุณาเลือก อัตราที่ ด้วยค่ะ');
			 document.getElementById("dGJsX3NlZ3VlbmNlLnNlZ0lE").focus();
		 }else if(positionID == ''){
			 alert('กรุณาเลือก ตำ่แหน่งงาน ด้วยค่ะ');
			 document.getElementById("positionID").focus();
		 }else if(rankIDBegin == ''){
			 alert('กรุณาเลือก ตำแหน่งเริ่มต้น ด้วยค่ะ');
			 document.getElementById("rankIDBegin").focus();
		}else if(rankIDEnd == ''){
			 alert('กรุณาเลือก ตำแหน่งปัจจุบัน ด้วยค่ะ');
			 document.getElementById("rankIDEnd").focus();
		}else if(agencies == ''){
			 alert('กรุณาเลือก ชื่อหน่วยงาน ด้วยค่ะ');
			 document.getElementById("dGJsX3NlZ3VlbmNlLnNlcVJlbWFyaw--").focus(); 
			 
		 }else{
			  $.ajax({
 					url: "<?php echo base_url('sequence/check_add') ?>?action=<?php echo $action;?>&sCode=<?php echo $get_segsCode;?>",
					type: 'POST',
					data: {
							segID: rates,
							positionID: positionID,
							id: agencies,
							seqRemark: deail,
							rankIDBegin: rankIDBegin,
							rankIDEnd: rankIDEnd  ,
							isHeader: isHeaderIsChecked  ,
							isConsult: isConsultIsChecked  ,
							isActing: isActingIsChecked  
					},
					success: function(response) {
						//Do Something 
						//alert(response);
						if(response == 'succesfully'){
 							location.replace('<?php echo base_url('sequence'); ?>');
						}else{
 							location.replace('<?php echo base_url('sequence/add'); ?>'); 
						}
											 
						 
					},
					error: function(xhr) {
						//Do Something to handle error
						//alert('error');
						alert('มีข้อมูลระดับนี้อยู่ในระบบแล้ว');
 						location.replace('<?php echo base_url('sequence'); ?>');
					}
				});	 
 		}
				
}
		
function on_back(){
			 location.replace('<?php echo base_url().'sequence' ?>');
}
		
</script>
