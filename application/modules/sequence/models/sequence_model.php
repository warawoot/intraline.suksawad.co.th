<?php

class Sequence_model extends MY_Model {
    
  
	function __construct() {
		parent::__construct();
        $this->table="tbl_seguence";
        $this->pk = "segID";
	}
        
    function max_id(){
	    $this->db->select_max('workPlaceID');
		$this->db->from('tbl_work_place');
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->workPlaceID; 
 		}else{
			$data 		=  '1';  
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
   function max_data($workPlaceID){
	    $this->db->select('*');
		$this->db->from('`tbl_work_place`');
		$this->db->where('`workPlaceID`' ,$workPlaceID);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        //print_r($data);
		return ($query->num_rows() > 0) ? $data : NULL;
    }  
	
	function get_last_assignID(){
		$this->db->select('assignID');
		$this->db->from('tbl_orgchart_assigndate');
		$this->db->order_by('assignCreateDate', "desc");
		$this->db->limit(1);
  		$query 		= $this->db->get();
 		if($query->num_rows() > 0){
			$row  = $query->row(); 
			$data = $row->assignID; 
 		}else{
			$data 		=  NULL;  
 		}
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
	function get_dropdown_all($id , $name ,$table){
            
			$this->db->order_by($id, "asc");
            $sql        = $this->db->get($table);
             
            $dropdowns  = $sql->result();
            
            if ($sql->num_rows() > 0){
                foreach($dropdowns as $dropdown){
                //echo $fruit = array_shift($dropdown);
                $dropdownlist['']   = "- none -";
                //if($dropdown->upperOrgID != 0){
                    //$dropdownlist[$dropdown->$id] = '--'.$dropdown->$name;
                //}else{
                    $dropdownlist[$dropdown->$id]   = $dropdown->$name;
                //}
            }
            }else{
                    $dropdownlist['']   = "- none -";
            } 
            
            $finaldropdown  = $dropdownlist;
            return $finaldropdown;
    }  
	
	function insert($data){
 			//$sql	=	$this->db->insert("tbl_job_desc",$data);
  			
			$this->db->set('segID', $data['segID']);
			$this->db->set('positionID', $data['positionID']);
			$this->db->set('id', $data['id']);
			$this->db->set('seqRemark', $data['seqRemark']);
			$this->db->set('rankIDBegin', $data['rankIDBegin']);
			$this->db->set('rankIDEnd', $data['rankIDEnd']); 
			
			$this->db->set('isHeader', $data['isHeader'],false); 
			$this->db->set('isConsult', $data['isConsult'],false); 
			$this->db->set('isActing', $data['isActing'],false); 
			
  			$this->db->insert('tbl_seguence');
			
			if($this->db->affected_rows() > 0){
					 return 'succesfully';
			}else{
					 return 'failed';
			}
			
 	}
	
	function update($segsCode,$data){
		
			$this->db->set('segID', $data['segID']);
			$this->db->set('positionID', $data['positionID']);
			$this->db->set('id', $data['id']);
			$this->db->set('seqRemark', $data['seqRemark']);
			$this->db->set('rankIDBegin', $data['rankIDBegin']);
			$this->db->set('rankIDEnd', $data['rankIDEnd']);
			$this->db->set('isHeader', $data['isHeader'],false); 
			$this->db->set('isConsult', $data['isConsult'],false); 
			$this->db->set('isActing', $data['isActing'],false); 
			
			
			$this->db->where('segsCode', $segsCode); 
			$this->db->update('tbl_seguence'); 	
			 
			if($this->db->affected_rows() > 0){
					 return 'succesfully';
			}else{
					 return 'failed';
			}
  		}
		
	function get_edit($segsCode){
		
		$this->db->select('t1.segsCode , t1.segID , t1.positionID ,t1.id  ,t1.seqRemark ,t1.rankIDBegin ,t1.rankIDEnd 
							,CAST(t1.isHeader AS unsigned integer) AS isHeader
							,CAST(t1.isConsult AS unsigned integer) AS isConsult
							,CAST(t1.isActing AS unsigned integer) AS isActing'
						);
		$this->db->from('tbl_seguence as t1');
		$this->db->where('t1.segsCode', $segsCode); 
		$this->db->limit(1);
  		$query 		= $this->db->get();
 		$data 		= $query->row_array(); 
        //print_r($data);
		return ($query->num_rows() > 0) ? $data : NULL;
		
	}
	
} 