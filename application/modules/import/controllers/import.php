<?php

class Import extends MY_Controller {
    
	function __construct(){
		parent::__construct();
		
		$this->load->model('staff_check_yearly_model');
	}
	
	public function index(){

		
		
		

		$data['title'] = "Import รายงานตรวจสุขภาพ";
        $this->template->load("template/admin",'main', $data);

	}

	public function importcsv(){
			$path = './assets/import/';
			$filename = $_FILES["fileCSV"]["name"];
			move_uploaded_file($_FILES['fileCSV']['tmp_name'], $path.$_FILES['fileCSV']['name']);
			
			$row = 1;
			if (($handle = fopen($path.$filename, "r")) !== FALSE) {
			    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			    	$str = "";
			    	if($row > 1){
			    		 $num = count($data);
				        //echo "<p> $num fields in line $row: <br /></p>\n";
				        $row++;
				        
				        for ($c=0; $c < $num; $c++) {
				            //echo $data[$c] . "<br />\n";
				            $str.="'".$data[$c]."',";
				        }
				        //echo $str;
				        $str = (!empty($str)) ? substr($str,0,-1) : "";
				        $sql = "insert into tbl_staff_check_yearly values($str)";
			        	$this->db->query($sql);
			    	}
			    	
			        $row++;

			    }
			    fclose($handle);
			}
			@unlink($path.$filename);
			header('Content-Type: text/html; charset=utf-8');
			echo"<script language=\"JavaScript\">";
			echo"alert('Import ข้อมูลเรียบร้อยแล้ว');";
			echo"window.location='".site_url()."import'";
			echo"</script>";


			
	}

	/*function exportcsv(){
		$data['r'] = $this->staff_check_yearly_model->get();

		$data['fields'] = $this->db->list_fields('tbl_staff_check_yearly');

		 

		$this->load->view('print_csv',$data);
	}*/

}
/* End of file import.php */
/* Location: ./application/module/import/import.php */